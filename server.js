const express = require('express');
const path = require('path');
const app = express();
let fs = require("fs");
let multer = require("multer");

let upload = multer({ dest: 'uploads/' });

let cors = require('cors');
app.use(cors())

app.use(express.static(__dirname + '/dist/edeliveryAdmin'));

app.post('/admin/update_language_file',upload.single('avtar'), (request_data, response_data) => {
  response_data.json({success: true});
  let jsondata = JSON.stringify(JSON.parse(JSON.parse(request_data.body.string_file)));               
  let user_string_file_path = './src/assets/i18n/'+request_data.body.language+'.json'
  fs.writeFile(user_string_file_path, jsondata, (err) => {});
})


app.get('/admin/get_language_file/:lang',(request_data,response)=>{
    let lang_name = request_data.params.lang;
    if(lang_name){
        let file_string = './src/assets/i18n/'+lang_name+'.json';
        if(fs.existsSync(file_string)){
            response.download(file_string)
        }else{
            response.json({
                success:false,
                message:'File Not Found!!'
            })
        }
    }
});

app.get('/*', (req, res) =>{
  res.sendFile(path.join(__dirname, '/dist/edeliveryAdmin/index.html'));
});

app.listen(3000);