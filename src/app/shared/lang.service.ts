import { Injectable, Renderer2, RendererFactory2 } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

import { getThemeLang, setThemeLang } from 'src/app/utils/util';
import { ProfileService } from '../services/profile.service';
import { Helper } from './helper';
import { BehaviorSubject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Title } from '@angular/platform-browser';


const languageKey = '__lang';

@Injectable({
    providedIn: 'root',
})
export class LangService {
    isSingleLang = false;
    renderer: Renderer2;
    defaultLanguage = getThemeLang();
    supportedLanguages: Language[] = [];
    private languages = new BehaviorSubject<any>(null)
    languagesObservable = this.languages.asObservable();
    IMAGE_URL = environment.languageUrl

    constructor(
        private translate: TranslateService,
        private _profileService: ProfileService,
        private rendererFactory: RendererFactory2,
        private _helper: Helper,
        private http: HttpClient,
        private titleService: Title
    ) {
        this.renderer = this.rendererFactory.createRenderer(null, null);
    }

    set_panel_name() {
        this.titleService.setTitle(this._helper._trans.instant('app-title.admin-panel'))
    }

    configLanguages() {
        return new Promise((resolve, rejects) => {
            this._profileService.get_languages().then(res_data => {
                if (res_data.success) {
                    this._helper.is_use_captcha = res_data.is_use_captcha
                    let obj = res_data.lang.find(list => list.code == localStorage.getItem('theme_lang'));
                    if (!obj) {
                        setThemeLang('en', 'ltr')
                        window.location.reload()
                    }
                    res_data['lang'].forEach(_lang => {
                        let lang_direction = _lang.is_lang_rtl === false ? 'ltr' : 'rtl'
                        this.supportedLanguages.push({ code: _lang.code, direction: lang_direction, label: _lang.name, shorthand: _lang.code })
                    });
                    setTimeout(() => {
                        this.languages.next({})
                    })
                    resolve(true);
                } else {
                    this.supportedLanguages.push({ code: 'en', direction: 'ltr', label: 'English', shorthand: 'en' })
                    this.languages.next({})
                    resolve(true);
                }
            })
        })
    }

    init(): void {
        this.configLanguages().then(() => {
            this.supportedLanguages.forEach(_lang => {
                try {
                    const randomValue = crypto.getRandomValues(new Uint8Array(1))[0] / 256;
                    const randomQueryParam = `random=${randomValue}`;
                    const data_2 = `${this.IMAGE_URL}language/${_lang.code}.json?${randomQueryParam}`;
                    this.http.get(data_2).subscribe((res: any) => {
                        this.translate.setTranslation(_lang.code, res)
                        this.set_panel_name();
                        this._helper.language_is_loading = false;
                    })

                } catch (err) {
                    const data_2 = `${this.IMAGE_URL}language/en.json`
                    this.http.get(data_2).subscribe((res: any) => {
                        this.translate.setTranslation('en', res)
                        this.set_panel_name();
                        this._helper.language_is_loading = false;
                    })

                }
            })
        })

        this.translate.setDefaultLang(this.defaultLanguage);
        this.translate.use(this.defaultLanguage);

        this.renderer.removeClass(document.body, 'ltr');
        this.renderer.removeClass(document.body, 'rtl');
        if (this.translate.currentLang === 'ar') {
            this.renderer.addClass(document.body, 'rtl');
        } else {
            this.renderer.addClass(document.body, 'ltr');
        }

    }

    checkForDirectionChange(): void {

        this.renderer.removeClass(document.body, 'ltr');
        this.renderer.removeClass(document.body, 'rtl');
        this.renderer.addClass(document.body, this.direction);
        this.renderer.setAttribute(document.documentElement, 'direction', this.direction);
    }

    set language(lang: string) {

        let language = lang || getThemeLang();
        const isSupportedLanguage = this.supportedLanguages.map((item) => item.code).includes(language);
        if (!isSupportedLanguage) {
            language = this.defaultLanguage;
        }

        this.translate.use(language);
        this.checkForDirectionChange();
        if (language == 'ar') {
            setThemeLang(language, 'rtl')
        } else {
            setThemeLang(language, 'ltr');
        }
    }

    get language(): string {
        return this.translate.currentLang;
    }

    get languageShorthand(): string {
        let currentLang = this.supportedLanguages.find(item => item.code === this.translate.currentLang);
        this.set_panel_name()
        return currentLang?.shorthand || localStorage.getItem('theme_lang') || 'en';
    }

    get direction(): string {
        let currentDirection = this.supportedLanguages.find(item => item.code === this.translate.currentLang);
        return currentDirection ? currentDirection.direction : 'ltr';
    }

    get languageLabel(): string {
        return this.supportedLanguages.find(item => item.code === this.translate.currentLang).label;
    }

    get selectedlanguageIndex(): number {
        return this.supportedLanguages.findIndex(item => item.code === this.translate.currentLang);
    }

}

export class Language {
    code: string;
    direction: string;
    label: string;
    shorthand: string;
}
