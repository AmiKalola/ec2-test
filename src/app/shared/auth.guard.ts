import {
  CanActivate,
  Router,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  CanActivateChild,
} from '@angular/router';
import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';

@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate, CanActivateChild {

  permissions: Array<any>;


  constructor(private authService: AuthService, private router: Router) { }
  
  async canActivateChild(route: ActivatedRouteSnapshot,state: RouterStateSnapshot): Promise<boolean> {
    let check_permission = route.data['auth'];

    if(this.authService.loginAdminData){
      if(!check_permission || this.authService.is_main_store_login){
        return true;
      }else{
        this.authService.authPermission.subscribe(permissions => {
          this.permissions = permissions;
          if (permissions?.length > 0) {
            if (this.hasPermission(check_permission) && (this.permissions && !this.permissions.some(bid => bid.url === check_permission))) {
              let route = '';
              for (const element of this.permissions) {
                const permissions = element;
                if (permissions.route) {
                  route = permissions.route;
                  break;
                }
              }
              if (route) {
                this.router.navigate([route]);
              }
            }
            return this.hasPermission(check_permission)
          } else {
            return false;
          }
        }) 
      }
    } else {
      return false
    }
  }

  async canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean> {
    let check_permission = route.data['auth'];
    if(this.authService.loginAdminData){
      if(!check_permission || this.authService.is_main_store_login){
        return true;
      }else{
        return this.hasPermission(check_permission)
      }
    } else {
      return false
    }
  }



  hasPermission(check_permission): Promise<boolean> {
    return new Promise(async (resolve, rejects) => {
      if (!this.permissions) {
        this.authService.authPermission.subscribe(permissions => {
          this.permissions = permissions;
        })
      }    
      resolve(this.permissions && this.permissions.some(bid => bid.url === check_permission));
    })
  }


}


