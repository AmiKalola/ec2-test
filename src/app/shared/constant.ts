export let ORDER_STATE = {
    WAITING_FOR_ACCEPT_STORE: 1,
    CANCELED_BY_USER: 101,
    STORE_ACCEPTED: 3,
    STORE_REJECTED: 103,
    STORE_CANCELLED: 104,
    STORE_CANCELLED_REQUEST: 105,
    ADMIN_CANCELLED_REQUEST: 106,
    STORE_PREPARING_ORDER: 5,
    OREDER_READY: 7,
    WAITING_FOR_DELIVERY_MAN: 9,
    NO_DELIVERY_MAN_FOUND: 109,
    DELIVERY_MAN_ACCEPTED: 11,
    DELIVERY_MAN_REJECTED: 111,
    DELIVERY_MAN_CANCELLED: 112,
    DELIVERY_MAN_COMING: 13,
    DELIVERY_MAN_ARRIVED: 15,
    DELIVERY_MAN_PICKED_ORDER: 17,
    DELIVERY_MAN_STARTED_DELIVERY: 19,
    DELIVERY_MAN_ARRIVED_AT_DESTINATION: 21,
    DELIVERY_MAN_COMPLETE_DELIVERY: 23,
    ORDER_COMPLETED: 25,
};

export let EMAIL_TAG_CONSTANT = {
    USER_FIRST_NAME: 'USER_FIRST_NAME',
    USER_LAST_NAME: 'USER_LAST_NAME',
    PROVIDER_FIRST_NAME: 'PARTNER_FIRST_NAME',
    PROVIDER_LAST_NAME: 'PARTNER_LAST_NAME',
    STORE_NAME: 'MERCHANT_NAME',
    OTP: 'OTP',
    ORDER_NO: 'ORDER_NO',
    PASSWORD: 'PASSWORD'
}

export let EMAIL_TAG_SETTING:any[] = [
    {
        EMAIL_UNIQUE_ID: [1,2,3,6],
        ALLOWED_PARAMS: [
            EMAIL_TAG_CONSTANT.USER_FIRST_NAME,
            EMAIL_TAG_CONSTANT.USER_LAST_NAME
        ]
    },
    {
        EMAIL_UNIQUE_ID: [4],
        ALLOWED_PARAMS: [
            EMAIL_TAG_CONSTANT.USER_FIRST_NAME,
            EMAIL_TAG_CONSTANT.USER_LAST_NAME,
            EMAIL_TAG_CONSTANT.OTP
        ]
    },
    {
        EMAIL_UNIQUE_ID: [11,12,13,16],
        ALLOWED_PARAMS: [
            EMAIL_TAG_CONSTANT.PROVIDER_FIRST_NAME,
            EMAIL_TAG_CONSTANT.PROVIDER_LAST_NAME
        ]
    },
    {
        EMAIL_UNIQUE_ID: [14],
        ALLOWED_PARAMS: [
            EMAIL_TAG_CONSTANT.PROVIDER_FIRST_NAME,
            EMAIL_TAG_CONSTANT.PROVIDER_LAST_NAME,
            EMAIL_TAG_CONSTANT.OTP
        ]
    },
    {
        EMAIL_UNIQUE_ID: [21,22,23,26],
        ALLOWED_PARAMS: [
            EMAIL_TAG_CONSTANT.STORE_NAME
        ]
    },
    {
        EMAIL_UNIQUE_ID: [24],
        ALLOWED_PARAMS: [
            EMAIL_TAG_CONSTANT.STORE_NAME,
            EMAIL_TAG_CONSTANT.OTP
        ]
    },
    {
        EMAIL_UNIQUE_ID: [40, 41, 42, 43, 44, 51, 52, 53, 55, 56, 57, 58, 59, 74, 60, 61, 62, 64, 65, 48, 49],
        ALLOWED_PARAMS: [
            EMAIL_TAG_CONSTANT.ORDER_NO,
            EMAIL_TAG_CONSTANT.STORE_NAME,
            EMAIL_TAG_CONSTANT.USER_FIRST_NAME,
            EMAIL_TAG_CONSTANT.USER_LAST_NAME,
        ]
    },
    {
        EMAIL_UNIQUE_ID: [45, 47, 76, 77, 50],
        ALLOWED_PARAMS: [
            EMAIL_TAG_CONSTANT.ORDER_NO,
            EMAIL_TAG_CONSTANT.STORE_NAME,
            EMAIL_TAG_CONSTANT.USER_FIRST_NAME,
            EMAIL_TAG_CONSTANT.USER_LAST_NAME,
        ]
    },
    {
        EMAIL_UNIQUE_ID: [101],
        ALLOWED_PARAMS: [
            EMAIL_TAG_CONSTANT.PASSWORD
        ]
    },
    {
        EMAIL_UNIQUE_ID: [102],
        ALLOWED_PARAMS: [
            EMAIL_TAG_CONSTANT.PASSWORD
        ]
    },
    {
        EMAIL_UNIQUE_ID: [103],
        ALLOWED_PARAMS: [
            EMAIL_TAG_CONSTANT.PROVIDER_FIRST_NAME,
            EMAIL_TAG_CONSTANT.PROVIDER_LAST_NAME,
            EMAIL_TAG_CONSTANT.PASSWORD
        ]
    },
    {
        EMAIL_UNIQUE_ID: [104],
        ALLOWED_PARAMS: [
            EMAIL_TAG_CONSTANT.PROVIDER_FIRST_NAME,
            EMAIL_TAG_CONSTANT.PROVIDER_LAST_NAME,
            EMAIL_TAG_CONSTANT.PASSWORD
        ]
    },
]

export let SMS_TAG_CONSTANT = {
  USER_FIRST_NAME: 'USER_FIRST_NAME',
  USER_LAST_NAME: 'USER_LAST_NAME',
  PROVIDER_FIRST_NAME: 'PARTNER_FIRST_NAME',
  PROVIDER_LAST_NAME: 'PARTNER_LAST_NAME',
  STORE_NAME: 'MERCHANT_NAME',
  OTP: 'OTP',
  ORDER_NO: 'ORDER_NO',
  PASSWORD: 'PASSWORD'
}

export let SMS_TAG_SETTING: any[] = [
  {
    SMS_UNIQUE_ID: [8, 9, 10],
    ALLOWED_PARAMS: [
      SMS_TAG_CONSTANT.USER_FIRST_NAME,
      SMS_TAG_CONSTANT.USER_LAST_NAME
    ]
  },
  {
    SMS_UNIQUE_ID: [1],
    ALLOWED_PARAMS: [
      SMS_TAG_CONSTANT.USER_FIRST_NAME,
      SMS_TAG_CONSTANT.USER_LAST_NAME,
      SMS_TAG_CONSTANT.OTP
    ]
  },
  {
    SMS_UNIQUE_ID: [2],
    ALLOWED_PARAMS: [
      SMS_TAG_CONSTANT.USER_FIRST_NAME,
      SMS_TAG_CONSTANT.USER_LAST_NAME,
      SMS_TAG_CONSTANT.PASSWORD
    ]
  },
  {
    SMS_UNIQUE_ID: [5, 13, 14, 25, 26, 28, 29, 45, 46, 51, 52, 53, 54, 55],
    ALLOWED_PARAMS: [
      SMS_TAG_CONSTANT.USER_FIRST_NAME,
      SMS_TAG_CONSTANT.USER_LAST_NAME,
      SMS_TAG_CONSTANT.PROVIDER_FIRST_NAME,
      SMS_TAG_CONSTANT.PROVIDER_LAST_NAME,
      SMS_TAG_CONSTANT.STORE_NAME,
      SMS_TAG_CONSTANT.ORDER_NO
    ]
  },
  {
    SMS_UNIQUE_ID: [3, 4, 6, 11, 12, 15, 16, 47, 48, 49, 50],
    ALLOWED_PARAMS: [
      SMS_TAG_CONSTANT.USER_FIRST_NAME,
      SMS_TAG_CONSTANT.USER_LAST_NAME,
      SMS_TAG_CONSTANT.STORE_NAME,
      SMS_TAG_CONSTANT.ORDER_NO
    ]
  },
  {
    SMS_UNIQUE_ID: [7],
    ALLOWED_PARAMS: [
      SMS_TAG_CONSTANT.USER_FIRST_NAME,
      SMS_TAG_CONSTANT.USER_LAST_NAME,
      SMS_TAG_CONSTANT.PROVIDER_FIRST_NAME,
      SMS_TAG_CONSTANT.PROVIDER_LAST_NAME,
      SMS_TAG_CONSTANT.STORE_NAME,
      SMS_TAG_CONSTANT.ORDER_NO,
      SMS_TAG_CONSTANT.OTP
    ]
  },
  {
    SMS_UNIQUE_ID: [23, 24, 27],
    ALLOWED_PARAMS: [
      SMS_TAG_CONSTANT.PROVIDER_FIRST_NAME,
      SMS_TAG_CONSTANT.PROVIDER_LAST_NAME
    ]
  },
  {
    SMS_UNIQUE_ID: [21],
    ALLOWED_PARAMS: [
      SMS_TAG_CONSTANT.PROVIDER_FIRST_NAME,
      SMS_TAG_CONSTANT.PROVIDER_LAST_NAME,
      SMS_TAG_CONSTANT.OTP
    ]
  },
  {
    SMS_UNIQUE_ID: [22],
    ALLOWED_PARAMS: [
      SMS_TAG_CONSTANT.PROVIDER_FIRST_NAME,
      SMS_TAG_CONSTANT.PROVIDER_LAST_NAME,
      SMS_TAG_CONSTANT.PASSWORD
    ]
  },

  {
    SMS_UNIQUE_ID: [43, 44],
    ALLOWED_PARAMS: [
      SMS_TAG_CONSTANT.STORE_NAME
    ]
  },
  {
    SMS_UNIQUE_ID: [41],
    ALLOWED_PARAMS: [
      SMS_TAG_CONSTANT.STORE_NAME,
      SMS_TAG_CONSTANT.OTP
    ]
  },
  {
    SMS_UNIQUE_ID: [42],
    ALLOWED_PARAMS: [
      SMS_TAG_CONSTANT.STORE_NAME,
      SMS_TAG_CONSTANT.PASSWORD
    ]
  }
]

export let DOCUMENT_FOR = [
    {VALUE: 2, TITLE: 'label-title.store'},
    {VALUE: 7, TITLE: 'label-title.user'},
    {VALUE: 8, TITLE: 'label-title.delivery-man'},
    {VALUE: 9, TITLE: 'label-title.delivery-man-vehicle'}
]

export let ADMIN_PROFIT_ON_ORDER = [
    { ID: 1, NAME: 'label-title.percentage' },
    { ID: 2, NAME: 'label-title.absolute-price-per-order' },
    { ID: 3, NAME: "label-title.absolute-price-per-item" }
];


export let DEFAULT_IMAGE_PATH = {
    PROMO_CODE: '/assets/img/default_images/promo_code.png',
    USER: '/assets/img/default_images/user.png',
    USER_SQUARE: '/assets/img/default_images/user_square.png',
    CATEGORY: '/assets/img/default_images/category.png',
    DRIVER: '/assets/img/default_images/driver.png',
    DOCUMENT: '/assets/img/default_images/document_default.png',
    PROMO: '/assets/img/default_images/promo_code.png',
    DELIVERY: '/assets/img/default_images/delivery/food.jpg',
    DELIVERY_ICON: '/assets/img/default_images/delivery/food_icon.png',
    LANDING_IMAGE: '/assets/img/default_images/popup_side_img_admin.png',
    ADVERTISE: '/assets/img/default_images/ads_default_square.png',
    DEFAULT_FLAG:'/assets/img/default_images/country.png'
}

export let USER_TYPE = {
    USER: 7,
    PROVIDER: 8,
    STORE: 2
}

export let DELIVERY_TYPE_CONSTANT = {
    STORE: 1,
    COURIER: 2,
    TABLE_BOOKING: 3,
    TAXI: 4,
    SERVICE: 5,
    APPOINMENT: 6,
    ECOMMERCE:7
}

export let WALLET_REQUEST_STATUS = {
    CREATED: 1,
    ACCEPTED: 2,
    TRANSFERED: 3,
    COMPLETED: 4,
    CANCELLED: 5
};

export let DELIVERY_TYPE = [
    { value: 1, title: 'label-title.store'},
    { value: 2, title: 'label-title.courier'},
    { value: 4, title: 'label-title.taxi'},
    { value: 5, title: 'label-title.service'},
    { value: 6, title: 'label-title.appoinment'},
    { value: 7, title: 'label-title.ecommerce'},
]

export let PROMO_FOR_USE = [
    { value: 0, title: 'label-title.deliveries-business'},
    { value: 2, title: 'label-title.store'},
    { value: 20, title: 'label-title.service-fees'},
    { value: 7, title: 'label-title.user'},
    { value: 21, title: 'label-title.product'},
    { value: 22, title: 'label-title.item'},
]

export let ADMIN_PROFIT_ON_DELIVERY_STRING = {
    PERCENTAGE: 'label-title.percentage',
    PER_DELVIERY: 'label-title.per-delivery'
};

export let ADMIN_PROFIT_ON_DELIVERY_ID = {
    PERCENTAGE: 1,
    PER_DELVIERY: 2
};

export let ADMIN_PROFIT_ON_DELIVERYS = [
    {ID: ADMIN_PROFIT_ON_DELIVERY_ID.PERCENTAGE, NAME: ADMIN_PROFIT_ON_DELIVERY_STRING.PERCENTAGE},
    {ID: ADMIN_PROFIT_ON_DELIVERY_ID.PER_DELVIERY, NAME: ADMIN_PROFIT_ON_DELIVERY_STRING.PER_DELVIERY}

];

export let DATE_FORMAT = {
    DD_MM_YYYY_HH_MM_A: 'dd MMM yyyy hh:mm a',
    DD_MM_YYYY: 'dd MMM yyyy',
    HH_MM_A:"hh:mm A",
	medium:'medium',
    MOMENT_DD_MMM_YYYY: "DD MMM YYYY",
    MOMENT_DD_MMM: "DD MMM",
    mediumDate:'mediumDate',
    MMM_YYYY:"MMM-yyyy",
    MMM:"MMM"

}

export let PROMO_FOR_ID = {
    SERVICE: 20,
    DELIVERIES: 0,
    STORE: 2,
    PRODUCT: 21,
    ITEM: 22,
    USER: 7
};

export let PROMO_FOR_STRING = {
    DELIVERIES: "Deliveries",
    STORE: "Store",
    PRODUCT: "Product",
    ITEM: "Item",
    SERVICE: 'Service'
};

export let PROMO_FOR = [
    {ID: PROMO_FOR_ID.STORE, NAME: PROMO_FOR_STRING.STORE},
    {ID: PROMO_FOR_ID.PRODUCT, NAME: PROMO_FOR_STRING.PRODUCT},
    {ID: PROMO_FOR_ID.ITEM, NAME: PROMO_FOR_STRING.ITEM}
];

export let MONTH = [
    "January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
]

export let WEEK = [
    'First',
    'Second',
    'Third',
    'Fourth',
    'Fifth'
]

export let DAY = [
    'Sunday',
    'Monday',
    'Tuesday',
    'Wednesday',
    'Thursday',
    'Friday',
    'Saturday'
]

export let PROMO_RECURSION_ID = {
    NO_RECURSION: 0,
    DAILY_RECURSION: 1,
    WEEKLY_RECURSION: 2,
    MONTHLY_RECURSION: 3,
    ANNUALLY_RECURSION: 4
};
export let PROMO_RECURSION_STRING = {
    NO_RECURSION: 'label-title.no-recursion',
    DAILY_RECURSION: 'label-title.daily-recursion',
    WEEKLY_RECURSION: 'label-title.weekly-recursion',
    MONTHLY_RECURSION: 'label-title.monthly-recursion',
    ANNUALLY_RECURSION: 'label-title.annually-recursion'
};

export let PROMO_RECURSION = [
    {ID: PROMO_RECURSION_ID.NO_RECURSION, NAME: PROMO_RECURSION_STRING.NO_RECURSION},
    {ID: PROMO_RECURSION_ID.DAILY_RECURSION, NAME: PROMO_RECURSION_STRING.DAILY_RECURSION},
    {ID: PROMO_RECURSION_ID.WEEKLY_RECURSION, NAME: PROMO_RECURSION_STRING.WEEKLY_RECURSION},
    {ID: PROMO_RECURSION_ID.MONTHLY_RECURSION, NAME: PROMO_RECURSION_STRING.MONTHLY_RECURSION},
    {ID: PROMO_RECURSION_ID.ANNUALLY_RECURSION, NAME: PROMO_RECURSION_STRING.ANNUALLY_RECURSION}
];


export let ADMIN_PROMO_FOR_ID = {
    DELIVERIES: 0,
    STORE: 2,
    SERVICE: 20
};

export let ADMIN_PROMO_FOR_STRING = {
    DELIVERIES: "Deliveries",
    STORE: "Store",
    SERVICE: "Service",
    PRODUCT: "Product",
    ITEM: "Item",

};
// CONSTANT ARRAY
export let ADMIN_PROMO_FOR = [
    {ID: ADMIN_PROMO_FOR_ID.DELIVERIES, NAME: ADMIN_PROMO_FOR_STRING.DELIVERIES},
    {ID: ADMIN_PROMO_FOR_ID.STORE, NAME: ADMIN_PROMO_FOR_STRING.STORE},
    {ID: ADMIN_PROMO_FOR_ID.SERVICE, NAME: ADMIN_PROMO_FOR_STRING.SERVICE}
];

export let CHARGE_TYPE = {
    PERCENTAGE: 1,
    ABSOLUTE: 2
};


export let PER_PAGE_LIST = [20,50,100]
export let USERS_PER_PAGE_LIST = [15,30,60]

export let ADMIN_URL_ID = {
    DASHBOARD: "/admin/dashboard",
    USER: "/admin/users",
    PROVIDER: "/admin/providers",
    STORES: "/admin/stores",
    STORE_LOCATION: "/admin/store_location",
    PROVIDER_LOCATION: "/admin/provider_location",
    PROVIDER_TRACK: "/admin/location_track",
    BUSINESS_AREA:"/admin/business_area",
    DELIVERY: "/admin/delivery",
    VEHICLE: "/admin/vehicle",
    COUNTRY: "/admin/country",
    CITY: "/admin/city",
    DELIVERIES_PRICES: "/admin/set_pricing",
    ADMIN_SERVICE: "/admin/admin_service",
    ORDERS: "/admin/orders",
    DELIVERIES: "/admin/deliveries",
    HISTORY: "/admin/history",
    ORDER_EARNING: "/admin/order_earning",
    PROVIDER_WEEKLY_EARNING: "/admin/provider_weekly_earning",
    STORE_WEEKLY_EARNING: "/admin/store_weekly_earning",
    ADMIN_SETTINGS: "/setting/basic_setting",
    PROMO_CODE: "/admin/promotions",
    REFERRAL_DETAIL: "/admin/referral_detail",
    WALLET_HISTORY: "/admin/wallet_detail",
    WALLET_REQUEST: "/admin/wallet_request",
    TRANSACTION_HISTORY: "/admin/transaction_history",
    ADS: "/admin/advertise",
    DOCUMENTS: "/admin/document",
    MAIL: "/admin/email",
    SMS: "/admin/sms",
    CANCELLATION_REASON: "/admin/cancellation_reason",
    TABLE_BOOKING:"/admin/table-booking",
    APPOINTMENT:"/admin/appointment",
    DISPATCH:"/admin/dispatch",
    REVIEWS:"/admin/reviews",
    SERVICE:"/admin/services",
    CATEGORY:"/admin/category",
    ITEM_LIST:"/admin/item-list",
    MODIFIER_GROUP:"/admin/modifier-group",
    COURIER_SERVICE:"/admin/courier-service",
    ACTIVITY_LOG:"/admin/activity-logs",
    MASS_NOTIFICATION:"/admin/mass_notification",
    LANGUAGE:"/admin/language",
    TAG:"/admin/seo/tags",
    SCRIPT:"/admin/seo/scripts",
    TERMSCONDITION:"/admin/terms&condition"

};

export let ADMIN_URL_STRING = {
    DASHBOARD: "Dashboard",
    USER: "User",
    PROVIDER: 'Deliveryman',
    STORES: "Store",
    STORE_LOCATION: "Store Location",
    PROVIDER_LOCATION: "Deliveryman Location",
    PROVIDER_TRACK: "Deliveryman Track",
    BUSINESS_AREA:"Business Area",
    DELIVERY: "Delivery",
    VEHICLE: "Vehicle",
    COUNTRY: "Country - City",
    CITY: "City",
    DELIVERIES_PRICES: "Set Fees",
    ADMIN_SERVICE: "Admin Service",
    ORDERS: "Orders",
    DELIVERIES: "Business",
    HISTORY: "History",
    ORDER_EARNING: "Order Earning",
    PROVIDER_WEEKLY_EARNING: "Deliveryman Weekly Earning",
    STORE_WEEKLY_EARNING: "Store Weekly Earning",
    ADMIN_SETTINGS: "Admin Settings",
    PROMO_CODE: "Promo Code",
    REFERRAL_DETAIL: "Referral Detail",
    WALLET_HISTORY: "Wallet History",
    WALLET_REQUEST: "Wallet Request",
    TRANSACTION_HISTORY: "Transaction History",
    ADS: "Ads",
    DOCUMENTS: "Documents",
    MAIL: "Mail",
    SMS: "SMS",
    CANCELLATION_REASON: "Cancellation Reason",
    TABLE_BOOKING:"Table Booking",
    APPOINTMENT:"Appointment",
    DISPATCH:"Dispatch",
    REVIEWS:"Reviews",
    CATEGORY:"Menu Category",
    ITEM_LIST:"Menu Item List",
    MODIFIER_GROUP: "Menu Modifier Group",
    COURIER_SERVICE:"Courier Service",
    SERVICE:"Service",
    ACTIVITY_LOG:"Activity Log",
    MASS_NOTIFICATION:"Mass Notification",
    LANGUAGE:"Language",
    TAG:"Tag",
    SCRIPT:"Script",
    TERMSCONDITION:"Terms & Conditions"
};

export let ADMIN_URL = [
    {actions: "10101", ID: ADMIN_URL_ID.ORDERS, NAME: ADMIN_URL_STRING.ORDERS },
    {actions: "10001", ID: ADMIN_URL_ID.DELIVERIES, NAME: ADMIN_URL_STRING.DELIVERIES },
    {actions: "10001", ID: ADMIN_URL_ID.TABLE_BOOKING, NAME: ADMIN_URL_STRING.TABLE_BOOKING },
    {actions: "10101", ID: ADMIN_URL_ID.SERVICE, NAME: ADMIN_URL_STRING.SERVICE },
    {actions: "10101", ID: ADMIN_URL_ID.APPOINTMENT, NAME: ADMIN_URL_STRING.APPOINTMENT },
    {actions: "10100", ID: ADMIN_URL_ID.DISPATCH, NAME: ADMIN_URL_STRING.DISPATCH },
    {actions: "10001", ID: ADMIN_URL_ID.HISTORY, NAME: ADMIN_URL_STRING.HISTORY },
    {actions: "10001", ID: ADMIN_URL_ID.REVIEWS, NAME: ADMIN_URL_STRING.REVIEWS },
    {actions: "10000", ID: ADMIN_URL_ID.STORE_LOCATION, NAME: ADMIN_URL_STRING.STORE_LOCATION },
    {actions: "10000", ID: ADMIN_URL_ID.PROVIDER_LOCATION, NAME: ADMIN_URL_STRING.PROVIDER_LOCATION },
    {actions: "10000", ID: ADMIN_URL_ID.PROVIDER_TRACK, NAME: ADMIN_URL_STRING.PROVIDER_TRACK },
    {actions: "10000", ID: ADMIN_URL_ID.BUSINESS_AREA, NAME: ADMIN_URL_STRING.BUSINESS_AREA },
    {actions: "10001", ID: ADMIN_URL_ID.ORDER_EARNING, NAME: ADMIN_URL_STRING.ORDER_EARNING },
    {actions: "10001", ID: ADMIN_URL_ID.PROVIDER_WEEKLY_EARNING, NAME: ADMIN_URL_STRING.PROVIDER_WEEKLY_EARNING },
    {actions: "10001", ID: ADMIN_URL_ID.STORE_WEEKLY_EARNING, NAME: ADMIN_URL_STRING.STORE_WEEKLY_EARNING },
    {actions: "10001", ID: ADMIN_URL_ID.WALLET_HISTORY, NAME: ADMIN_URL_STRING.WALLET_HISTORY },
    {actions: "10101", ID: ADMIN_URL_ID.WALLET_REQUEST, NAME: ADMIN_URL_STRING.WALLET_REQUEST },
    {actions: "10001", ID: ADMIN_URL_ID.TRANSACTION_HISTORY, NAME: ADMIN_URL_STRING.TRANSACTION_HISTORY },
    {actions: "11100", ID: ADMIN_URL_ID.CATEGORY, NAME: ADMIN_URL_STRING.CATEGORY },
    {actions: "11110", ID: ADMIN_URL_ID.ITEM_LIST, NAME: ADMIN_URL_STRING.ITEM_LIST },
    {actions: "11100", ID: ADMIN_URL_ID.MODIFIER_GROUP, NAME: ADMIN_URL_STRING.MODIFIER_GROUP },
    {actions: "11100", ID: ADMIN_URL_ID.COURIER_SERVICE, NAME: ADMIN_URL_STRING.COURIER_SERVICE },
    {actions: "10111", ID: ADMIN_URL_ID.USER, NAME: ADMIN_URL_STRING.USER },
    {actions: "10111", ID: ADMIN_URL_ID.PROVIDER, NAME: ADMIN_URL_STRING.PROVIDER },
    {actions: "10111", ID: ADMIN_URL_ID.STORES, NAME: ADMIN_URL_STRING.STORES },
    {actions: "11100", ID: ADMIN_URL_ID.DELIVERY, NAME: ADMIN_URL_STRING.DELIVERY },
    {actions: "11110", ID: ADMIN_URL_ID.COUNTRY, NAME: ADMIN_URL_STRING.COUNTRY },
    {actions: "11100", ID: ADMIN_URL_ID.DELIVERIES_PRICES, NAME: ADMIN_URL_STRING.DELIVERIES_PRICES },
    {actions: "11110", ID: ADMIN_URL_ID.ADMIN_SERVICE, NAME: ADMIN_URL_STRING.ADMIN_SERVICE },
    {actions: "10100", ID: ADMIN_URL_ID.ADMIN_SETTINGS, NAME: ADMIN_URL_STRING.ADMIN_SETTINGS },
    {actions: "11100", ID: ADMIN_URL_ID.DOCUMENTS, NAME: ADMIN_URL_STRING.DOCUMENTS },
    {actions: "11111", ID: ADMIN_URL_ID.LANGUAGE, NAME: ADMIN_URL_STRING.LANGUAGE },
    {actions: "11100", ID: ADMIN_URL_ID.PROMO_CODE, NAME: ADMIN_URL_STRING.PROMO_CODE },
    {actions: "11110", ID: ADMIN_URL_ID.ADS, NAME: ADMIN_URL_STRING.ADS },
    {actions: "10000", ID: ADMIN_URL_ID.REFERRAL_DETAIL, NAME: ADMIN_URL_STRING.REFERRAL_DETAIL },
    {actions: "10100", ID: ADMIN_URL_ID.MAIL, NAME: ADMIN_URL_STRING.MAIL },
    {actions: "10100", ID: ADMIN_URL_ID.SMS, NAME: ADMIN_URL_STRING.SMS },
    {actions: "11000", ID: ADMIN_URL_ID.MASS_NOTIFICATION, NAME: ADMIN_URL_STRING.MASS_NOTIFICATION },
    {actions: "10100", ID: ADMIN_URL_ID.TERMSCONDITION, NAME: ADMIN_URL_STRING.TERMSCONDITION },
    {actions: "11110", ID: ADMIN_URL_ID.CANCELLATION_REASON, NAME: ADMIN_URL_STRING.CANCELLATION_REASON },
    {actions: "10100", ID: ADMIN_URL_ID.TAG, NAME: ADMIN_URL_STRING.TAG },
    {actions: "11000", ID: ADMIN_URL_ID.SCRIPT, NAME: ADMIN_URL_STRING.SCRIPT }
];

export let ADMIN_DATA_ID = {
    ADMIN: 1,
    SUB_ADMIN: 3,
    STORE: 2,
    USER: 7,
    PROVIDER: 8,
    PROVIDER_VEHICLE: 9
};

export let WALLET_COMMENT_ID = {
    SET_BY_ADMIN: 1,
    ADDED_BY_CARD: 2,
    ADDED_BY_REFERRAL: 3,
    ORDER_CHARGED: 4,
    ORDER_REFUND: 5,
    SET_ORDER_PROFIT: 6,
    ORDER_CANCELLATION_CHARGE: 7,
    SET_BY_WALLET_REQUEST: 8,

    SET_WEEKLY_PAYMENT_BY_ADMIN: 9,
};

export let CHARGES_TYPE = [
    {value:1, title: 'label-title.percentage' },
    {value:2, title: 'label-title.absolute' }
]

export let SIZE = {
    METER: 1,
    CENTI_METER: 2
};

export let WEIGHT = {
    KG: 1,
    GRAM: 2
};

export let PERMISSION = {
	VIEW: 0,
	ADD: 1,
	EDIT: 2,
	DELETE: 3,
	EXPORT: 4
}

export let REDEEM_POINT = {
    REFERRAL_REDEEM_POINT_FOR_USER:1,
	REFERRAL_REDEEM_POINT_FOR_USER_FRIEND:2,
	REVIEW_REDEEM_POINT:3,
	ORDER_REDEEM_POINT:4,
	TIP_REDEEM_POINT:5,
	DAILY_ORDER_ACCEPTED_REDEEM_POINT:6,
	DAILY_ORDER_COMPLETED_REDEEM_POINT:7,
	HIGH_RATING_REDEEM_POINT:8,
    WITHDRAW_REDEEM_POINT: 9
};

export let DELIVERY_TYPE_DEFAULT_IMAGE = {
    [DELIVERY_TYPE_CONSTANT.STORE] : 'delivery_type_images/delivery.png',
    [DELIVERY_TYPE_CONSTANT.SERVICE] : 'delivery_type_images/service.png',
    [DELIVERY_TYPE_CONSTANT.APPOINMENT] : 'delivery_type_images/appoinment.png',
    [DELIVERY_TYPE_CONSTANT.COURIER] : 'delivery_type_images/courier.png',
    [DELIVERY_TYPE_CONSTANT.TAXI] : 'delivery_type_images/taxi.png',
    [DELIVERY_TYPE_CONSTANT.ECOMMERCE] : 'delivery_type_images/ecommerce.png',
}

export let SETTING_TYPE = {
    ANDROID_USER : 'ANDROID_USER',
    ANDROID_PARTNER : 'ANDROID_PARTNER',
    ANDROID_STORE : 'ANDROID_STORE',
    IOS_USER : 'IOS_USER',
    IOS_PARTNER : 'IOS_PARTNER',
    IOS_STORE : 'IOS_STORE',
    WEB_PANEL : 'WEB_PANEL'
}


export let UPDATE_LOG_TYPE = {
    ADMIN_SETTINGS: 1,
    CITY_SETTINGS: 2,
    ZONE_SETTINGS: 3,
    COUNTRY_SETTINGS: 4,
    BUSINESS_SETTINGS : 5, 
    SET_FEES_SETTINGS : 6 ,
    VEHICLE_SETTINGS : 7,
    CANCELLATION_REASON: 8,
    DOCUMENT_SETTINGS: 9,
    LANGUAGE_SETTINGS: 10,
    PROMO_SETTINGS: 11,
    EMAIL_SETTINGS: 12,
    SMS_SETTINGS: 13,
    PRIVACY_SETTINGS: 14,
    SUB_ADMIN_SETTINGS: 15,
    ADVERTISE_SETTINGS : 16,
};
 
export let UPDATE_LOG_STRING = {
    [UPDATE_LOG_TYPE.ADMIN_SETTINGS] : 'menu.admin-setting',
    [UPDATE_LOG_TYPE.CITY_SETTINGS] : 'heading-title.city-details',
    [UPDATE_LOG_TYPE.ZONE_SETTINGS] : 'label-title.zone-settings',
    [UPDATE_LOG_TYPE.COUNTRY_SETTINGS] : 'label-title.country-settings',
    [UPDATE_LOG_TYPE.BUSINESS_SETTINGS] : 'label-title.business-details',
    [UPDATE_LOG_TYPE.SET_FEES_SETTINGS] : 'label-title.set-fees-settings',
    [UPDATE_LOG_TYPE.VEHICLE_SETTINGS] : 'label-title.vehicle-settings',
    [UPDATE_LOG_TYPE.CANCELLATION_REASON] : 'label-title.cancellation-reason-settings',
    [UPDATE_LOG_TYPE.DOCUMENT_SETTINGS] : 'label-title.document-settings',
    [UPDATE_LOG_TYPE.LANGUAGE_SETTINGS] : 'label-title.language-settings',
    [UPDATE_LOG_TYPE.PROMO_SETTINGS] : 'label-title.promo-settings',
    [UPDATE_LOG_TYPE.EMAIL_SETTINGS] : 'label-title.email-settings',
    [UPDATE_LOG_TYPE.SMS_SETTINGS] : 'label-title.sms-settings',
    [UPDATE_LOG_TYPE.PRIVACY_SETTINGS] : 'label-title.privacy-settings',
    [UPDATE_LOG_TYPE.SUB_ADMIN_SETTINGS] : 'label-title.sub-admin',
    [UPDATE_LOG_TYPE.ADVERTISE_SETTINGS] : 'label-title.advertise-settings',
    
};
 
export let LOG_TYPE_STRING = {
    ADDED : 'button-title.add',
    UPDATED : 'button-title.update',
    DELETED : 'button-title.delete',
};
 
export let LOG_TYPE_VALUE = {
    ADD: 1,
    UPDATE: 2,
    DELETE: 3
}

export let PRICE_FORMULA = {
    DEFAULT : 0,
    ZONE_TRIP : 1,
    AIRPORT_TRIP : 2, 
    CITY_TRIP : 3
 }