import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Socket } from 'ngx-socket-io';

@Injectable({
  providedIn: 'root'
})
export class SocketService {
  constructor(
    private socket: Socket
  ) { }

  listener(eventName: string): Observable<any> {
    return new Observable((subscriber) => {
      this.socket.on(eventName, (data) => {
        subscriber.next(data);
      });
    });
  }

  removeAllListeners() {
    this.socket.removeAllListeners();
  }

  removeListener(event) {
    this.socket.removeListener(event);
  }
}
