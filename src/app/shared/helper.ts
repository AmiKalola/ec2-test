import { Inject, Injectable, NgZone } from '@angular/core';
import { METHODS, GET_METHOD } from './http_methods';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import {
  DEFAULT_IMAGE_PATH,
  ORDER_STATE,
  DAY,
  WEEK,
  MONTH,
  USER_TYPE,
  ADMIN_PROMO_FOR_ID,
  PROMO_RECURSION,
  DATE_FORMAT,
  DELIVERY_TYPE,
  PROMO_FOR_USE,
  ADMIN_PROFIT_ON_DELIVERYS,
  PROMO_FOR_ID,
  SIZE, WEIGHT,
  ADMIN_PROMO_FOR_STRING,
  ADMIN_PROFIT_ON_ORDER,
  ADMIN_PROFIT_ON_DELIVERY_ID,
  CHARGE_TYPE,
  WALLET_REQUEST_STATUS,
  ADMIN_URL,
  ADMIN_DATA_ID, DELIVERY_TYPE_CONSTANT,
  WALLET_COMMENT_ID,
  PERMISSION,
  REDEEM_POINT
} from 'src/app/shared/constant'
import * as json2csv from 'json2csv'
import { saveAs } from 'file-saver';
import { BehaviorSubject, lastValueFrom, Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment-timezone';
import { NotifiyService } from '../services/notifier.service';
import { DOCUMENT } from '@angular/common';
import { FormArray, FormControl, FormGroup } from '@angular/forms';

export interface IResponse {
  success: boolean,
  message: string,
  data: any
}

@Injectable({ providedIn: "root" })
export class Helper {
  language_is_loading: boolean = true;
  base_url = environment.apiUrl;
  public REDEEM_POINT = REDEEM_POINT;
  public image_url = environment.imageUrl;
  public POST_METHOD = METHODS;
  public ADMIN_DATA_ID = ADMIN_DATA_ID;
  public WALLET_COMMENT_ID = WALLET_COMMENT_ID;
  public GET_METHOD = GET_METHOD;
  public METHODS = METHODS;
  public ORDER_STATE = ORDER_STATE;
  public USER_TYPE = USER_TYPE;
  public DEFAULT_IMAGE_PATH = DEFAULT_IMAGE_PATH;
  public DATE_FORMAT = DATE_FORMAT;
  public DELIVERY_TYPE = DELIVERY_TYPE;
  public PROMO_FOR_USE = PROMO_FOR_USE;
  public DELIVERY_TYPE_CONSTANT = DELIVERY_TYPE_CONSTANT;
  public ADMIN_PROFIT_ON_DELIVERY_ID = ADMIN_PROFIT_ON_DELIVERY_ID;
  public ADMIN_PROFIT_ON_DELIVERYS = ADMIN_PROFIT_ON_DELIVERYS;
  public ADMIN_PROFIT_ON_ORDER = ADMIN_PROFIT_ON_ORDER;
  public CHARGE_TYPE = CHARGE_TYPE;
  public ADMIN_PROMO_FOR_STRING = ADMIN_PROMO_FOR_STRING;
  public PROMO_FOR_ID = PROMO_FOR_ID;
  public ADMIN_PROMO_FOR_ID = ADMIN_PROMO_FOR_ID;
  public PROMO_RECURSION = PROMO_RECURSION;
  public MONTH = MONTH;
  public WEEK = WEEK;
  public DAY = DAY;
  public SIZE = SIZE;
  public WEIGHT = WEIGHT;
  public WALLET_REQUEST_STATUS = WALLET_REQUEST_STATUS
  public height;
  public width;
  public ADMIN_URL = ADMIN_URL
  public _translateService: any
  public isStoremodelOpen: boolean = false;
  public PERMISSION = PERMISSION;
  public is_main_admin = true;

  devliverySetfees: any = ''; // used to redirect admin service page -> servicelist tab 

  selected_store_id = '';
  selected_store;
  is_store_selected: boolean = false
  changeSelectedStore = new BehaviorSubject<any>(null);
  _selected_store_id = this.changeSelectedStore.asObservable();
  is_loading = false
  is_use_captcha: boolean = false;
  delivery_type: number = 1
  is_set_delivery_pricing_for_selected_store: boolean = false;
  is_city_selected: boolean = false;
  selected_city = {
    _id: null,
    city_name: []
  };
  selected_city_id = '';
  admin_currency_code = '';
  admin_lat_long: any = [22.3039, 70.8022];
  public created_at = new BehaviorSubject<any>(null);
  created_date = this.created_at.asObservable();
  to_fixed_number: number = 2;
  public timeZone = new BehaviorSubject<any>(null);
  display_date_timezone = this.timeZone.asObservable();
  moment = moment;
  uploadFile = ["image/jpeg", "image/jpg", "image/png"];
  uploadDocFile = ["image/jpeg", "image/jpg", "image/png", "application/pdf"];
  PDFSIZE = 100000;
  permissions: any[] = [];
  public country_id = new BehaviorSubject<any>(null);
  countryId = this.country_id.asObservable();
  selected_country_id: any;
  public is_rental = false;

  public _bannerChanges = new BehaviorSubject<any>(null);
  _bannerObservable = this._bannerChanges.asObservable()


  constructor(public http: HttpClient,
    private _notifierService: NotifiyService,
    public router: Router,
    public ngZone: NgZone,
    public _trans: TranslateService,
    @Inject(DOCUMENT) private _documentRef: any) {
    this._translateService = this._trans
    this.countryId.subscribe((data) => {
      this.selected_country_id = data;
    })
  }

  async http_post_method_requester(api_name:string, parameter): Promise<any> {
    this.is_loading = true;
    try {
      const data = await lastValueFrom(this.http.post<any>(this.base_url + api_name, parameter));

      setTimeout(() => {
        if (this.is_loading) {
          this.is_loading = false;
        }
      }, 1000);
      
      return data;
    } catch (error) {
      console.error('Error in HTTP POST request:', error);
      this.is_loading = false; 
      throw error;
    }
  }

  get_method_loading: boolean = false;
  async http_get_method_requester(api_name, parameter) {
    this.get_method_loading = true;
    const data = await lastValueFrom(this.http.get<any>(this.base_url + api_name));
    setTimeout(() => {
      if (this.get_method_loading) {
        this.get_method_loading = false;
      }
    }, 1000);
    return data;
  }

  async http_get_method_requester_raw(api_name: string, parameter: any): Promise<any> {
    try {
      const response = await lastValueFrom(this.http.get<any>(api_name));
      return response;
    } catch (error) {
      console.error('Error in HTTP GET request:', error);
      throw error;
    }
  }
  

  changeSelectStore(store, type, is_set_delivery_pricing = false) {
    if (type === 1) {
      this.is_store_selected = true;
    } else {
      this.is_store_selected = false;
    }
    this.selected_store_id = store._id;
    this.selected_store = store
    this.delivery_type = store.delivery_type;
    this.is_set_delivery_pricing_for_selected_store = is_set_delivery_pricing;
    this.changeSelectedStore.next(store._id);
  }

  changeSelectedCity(city, is_reset = false) {
    if (!is_reset) {
      this.selected_city = {
        _id: null,
        city_name: []
      };
      this.is_city_selected = false;
      return;
    }
    this.selected_city = {
      _id: city.city_id,
      city_name: city.city_name
    }
    this.is_city_selected = true;
  }

  phone_number_validation(evt) {
    let charCode = (evt.which) ? evt.which : evt.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  public downloadcsvFile(data: any, fields) {
    if (fields.length !== 0) {
      fields.forEach(field => {
        field.label = this._trans.instant(field.label)
      });
      let csvData = this.convertToCSV(data, fields);
      let file = new Blob([csvData], { type: 'text/csv;charset=utf-8' });
      saveAs(file, "data.csv");
    } else {
      this._notifierService.showNotification('error', this._trans.instant('label-title.no-data-found'));
    }
  }

  public convertToCSV(objArray: any, fields) {
    let json2csvParser = new json2csv.Parser({ fields: fields });
    let csv = json2csvParser.parse(objArray);
    return csv;
  }

  space_validation(evt) {
    if (evt.code == "Space" && evt.target.value.length < 1) {
      return false;
    }
    return true
  }

  nospace_validation(evt) {
    if (evt.code == "Space") {
      return false;
    }
    return true
  }

  number_validation(evt) {
    let charCode = (evt.which) ? evt.which : evt.keyCode
    if (charCode > 95 && charCode < 106) {
      return true
    }
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      if (charCode == 46) {
        return true;
      }
      else {
        return false;
      }
    }
    return true;
  }

  maxLengthValidation(evt, value = 0, maxLength = 0) {
    let charCode = (evt.which) ? evt.which : evt.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      if (charCode == 46) {
        if (value?.toString().length >= maxLength) {
          return false;
        }
        return true;
      }
      else {
        return false;
      }
    }
    if (value?.toString().length >= maxLength) {
      return false;
    }
    return true;
  }

  special_char_validation(event) {
    let k;
    k = event.charCode;
    return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57));
  }


  export_csv(data, fieldname, filename = 'data') {
    if (data.length !== 0) {
      let date_name: any = new Date().toLocaleDateString();
      date_name.replaceAll("/", "_");
      filename = filename + '_' + date_name;
      let csvData = this.convert_to_csv(data, fieldname);
      let blob = new Blob(['\ufeff' + csvData], { type: 'text/csv;charset=utf-8;' });
      let dwldLink = document.createElement("a");
      let url = URL.createObjectURL(blob);
      let isSafariBrowser = navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1;
      if (isSafariBrowser) {
        dwldLink.setAttribute("target", "_blank");
      }
      dwldLink.setAttribute("href", url);
      dwldLink.setAttribute("download", filename + ".csv");
      dwldLink.style.visibility = "hidden";
      document.body.appendChild(dwldLink);
      dwldLink.click();
      document.body.removeChild(dwldLink);
    }
  }

  private convert_to_csv(objArray, headerList) {
    let array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
    let str = '';
    let row = 'SR NO,';
    for (let index in headerList) {
      let dump = headerList[index].replaceAll("_", " ")
      row += dump.toUpperCase() + ',';
    }
    row = row.slice(0, -1);
    str += row + '\r\n';
    for (let i = 0; i < array.length; i++) {
      let line = (i + 1) + '';
      for (let index in headerList) {
        let head = headerList[index];
        if (array[i][head] && array[i][head].toString().includes(",")) {
          line += ',' + array[i][head].toString().replaceAll(',', '');
        } else {
          line += ',' + array[i][head];
        }
      }
      str += line + '\r\n';
    }
    return str;
  }

  get uuid(): string {
    return 'xxxxxxxx-xxxx-xxx-xxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
      let r = crypto.getRandomValues(new Uint8Array(1))[0] % 16;
      let v = c == 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16);
    });
  }

  checkAndCleanFormValues(form: any) {
    Object.keys(form.controls).forEach(controlName => {
      const control = form.controls[controlName];

      if (control instanceof FormControl) {
        let controlValue = control.value;
        if (controlValue) {
          if (typeof controlValue === 'string' && controlValue.trim() === "") {
            controlValue = ''; // Set control value to empty string
          }
        }
        control.setValue(controlValue); // Update control value
      } else if (control instanceof FormGroup || control instanceof FormArray) {
        this.checkAndCleanFormValues(control); // Recursive call for nested FormGroup or FormArray
      }
    });
  }

  set_dynamic_height_width(class_name = '.dynamic_image') {
    this.width = document.querySelector(class_name).clientWidth;
    this.height = document.querySelector(class_name).clientHeight;
  }

  clear_height_width() {
    this.height = this.width = undefined;
  }

  get_image(image_url): string {
    return (this.height && this.width) ? environment.imageUrl + 'resize_image?width=' + this.width + '&height=' + this.height + '&format=webp&image=' + image_url : null;
  }

  getWeeks(startDate) {
    // Convert the start date to a JavaScript Date object
    const start = new Date(startDate);
    const today = new Date();

    // Adjust the start date to the beginning of the week
    start.setDate(start.getDate() - start.getDay());

    const weeks = [];
    let current = new Date(start);

    while (current < today) {
      const weekStart = new Date(current);
      const weekEnd = new Date(current);
      weekEnd.setDate(weekEnd.getDate() + 6);
      weeks.push({ start: weekStart, end: weekEnd });
      current.setDate(current.getDate() + 7);
    }

    return weeks;
  }

  findnearest(value) {
    value = Math.abs(Math.ceil(value));
    let length = value.toString().length;
    if (length === 1) {
      return length;
    } else {
      let test1 = "1";
      for (let index = 0; index < length - 1; index++) {
        test1 = test1 + "0";
      }
      let test2 = value % Number(test1);
      let test3 = value - test2;
      let test4: any = test2.toString().length === length - 1 ? Number(test2.toString()[0]) + 1 : 1;
      for (let index = 0; index < length - 2; index++) {
        test4 = test4 + "0";
      }
      let final = Number(test3) + Number(test4);
      return final
    }

  }

  get_order_status(order_status) {
    switch (order_status) {
      case (1): {
        return 'Waiting For Store To Accept'
      }
      case (101): {
        return 'Cancelled By User'
      }
      case (3): {
        return 'Store Accepted'
      }
      case (103): {
        return 'Store Rejected'
      }
      case (104): {
        return 'Store cancelled'
      }
      case (105): {
        return 'Store Cancelled Reqeust'
      }
      case (5): {
        return 'Store Preparing Order'
      }
      case (7): {
        return 'Oreder Ready'
      }
      case (9): {
        return 'Waiting For Deliveryman'
      }
      case (109): {
        return 'No Deliveryman Found'
      }
      case (111): {
        return 'Deliveryman rejected'
      }
      case (25): {
        return 'Order Completed'
      }
      case (112): {
        return 'Deliveryman Cancelled'
      }
      case (13): {
        return 'Deliveryman Comming'
      }
      case (15): {
        return 'Deliveryman Arrived'
      }
      case (17): {
        return 'Deliveryman Picked Order'
      }
      case (19): {
        return 'Deliveryman Started Delivery'
      }
      case (21): {
        return 'Deliveryman Arrived At Address'
      }
      case (23): {
        return 'Deliveryman Completed Delivery'
      }
      case (113): {
        return 'Store Rejected'
      }
    }
  }

  downloadImage(url: string, fileName: string) {
    const a: any = document.createElement('a');
    a.href = url;
    a.download = fileName;
    document.body.appendChild(a);
    a.style = 'display: none';
    a.click();
    window.URL.revokeObjectURL(url);
    a.remove();
  };
  downloadUrl(url: string) {
    return this.http.get(url, { responseType: 'blob' }).pipe(switchMap(response => this.readFile(response)));
  }

  private readFile(blob: Blob): Observable<string> {
     return new Observable<string>(observer => {
    const reader = new FileReader();
    
    reader.onerror = err => observer.error(err);
    reader.onabort = err => observer.error(err);
    reader.onload = () => observer.next(reader.result as string);
    reader.onloadend = () => observer.complete();
    
    reader.readAsDataURL(blob);
  });
  }

  pushNotification(type , message) {
    if(!type){
      type = 1
    }
    if (type === 1) {
      return this._notifierService.showNotification('success', this._trans.instant(message));
    } else {
      return this._notifierService.showNotification('error', this._trans.instant(message));
    }
  }
  keyUpDown(evt) {
    let charCode = (evt.which) ? evt.which : evt.keyCode
    if (charCode == 38 || charCode == 40 || evt.key == 'ArrowUp' || evt.key == 'ArrowDown') {
      return false;
    }
  }

  nagetiveNumValidation(evt : any, value: number = 0): boolean {
    const charCode = evt.which || evt.keyCode;
    // Check if the input already contains a negative sign
    if (value.toString().includes('-')) {
      return false;
    }
    // Allow negative sign and decimal point
    if (charCode === 45 || charCode === 46) {
      // Check if the input already contains a decimal point
      if (charCode === 46 && value.toString().includes('.')) {
        return false;
      }
      return true;
    }
    // Allow digits 0-9
    if (charCode >= 48 && charCode <= 57) {
      return true;
    }

    return false;
  }

  decimalNum_validation(evt, value = 0) {
    if (evt.key === '.' && value != null && (value.toString().indexOf('.') === value.toString().lastIndexOf('.'))) {
      return true;
    }
    let charCode = (evt.which) ? evt.which : evt.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      if (charCode == 46) {
        return true;
      }
      else {
        return false;
      }
    }
    return true;
  }
  only_number_valid(evt, value = 0) {
    if (evt.key === '.' && value != null && (value.toString().indexOf('.') === value.toString().lastIndexOf('.'))) {
      return false;
    }
    let charCode = (evt.which) ? evt.which : evt.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      if (charCode == 46) {
        return true;
      }
      else {
        return false;
      }
    }
    return true;
  }

  has_permission(type, static_url = null) {
    if (!this.is_main_admin) {
      let url = this.router.url.split('/').pop()
      if (static_url) {
        url = static_url
      }
      let index = this.permissions.findIndex((x) => x.url?.split('/').pop() == url);

      if (index !== -1) {
        let permission = this.permissions[index].permission.split('');
        if (permission[Number(type)] == '1') {
          return true;
        } else {
          return false;
        }
      } else {
        return false;
      }
    } else {
      return true;
    }
  }
  getMonthDay(date, index) {
    if (index == 0) {
      let start_date = moment().format('YYYY-MM-DD').toString()
      return [start_date];
    } else {
      let start_date = moment(date.toString()).add(-6, 'months').format('YYYY-MM-DD').toString()
      return [start_date];
    }
  }

  getFutureDay(date, index) {
    let start_date;
    if (index == 0) {
      start_date = moment(date).format('YYYY-MM-DD').toString();
    } else {
      let todayDate = new Date();
      let todayDate_month = todayDate.getMonth() + 1;
      let todayDate_year = todayDate.getFullYear().toString();
      let compare_date = moment(date.toString()).add(6, 'months').format('DD-M-YYYY').toString()
      let date_year = compare_date.split('-')[2];
      let date_month = compare_date.split('-')[1];

      if (date_year == todayDate_year && date_month <= todayDate_month.toString()) {
        start_date = moment(date.toString()).add(6, 'months').format('YYYY-MM-DD').toString()
      } else if (date_year < todayDate_year) {
        start_date = moment(date.toString()).add(6, 'months').format('YYYY-MM-DD').toString()
      } else {
        start_date = moment(todayDate.toString()).format('YYYY-MM-DD').toString()
      }
    }
    return [start_date];
  }

  getNextMonth(date, index) {
    if (index == 0) {
      const start_date = moment(date).format('YYYY-MM-DD').toString();
      return [start_date];
    } else {
      const start_date = moment(date).add(1, 'months').format('YYYY-MM-DD').toString();
      return [start_date];
    }
  }

  countryData: any;
  deliveryData: any;
  setDeliveryPrice(country, delivery) {
    if (country && delivery) {
      this.countryData = country;
      this.deliveryData = delivery;
    }
  }

  getSixMonthDifference(date) {
    let createdAt = new Date(date); // Replace with your actual response.created_at
    let counter: number = 0;
    const array = [];
    let end_date;

    while (createdAt < new Date() && (end_date ? end_date <= new Date() : true) || counter === 0) {
      createdAt = new Date(createdAt.getFullYear(), createdAt.getMonth(), 1, 0, 0, 0);
      let start_date = new Date(createdAt.getFullYear(), createdAt.getMonth(), 1, 0, 0, 0);

      let offsetInMilliseconds = start_date.getTimezoneOffset() * 60000; // 1 minute = 60000 milliseconds
      let adjustedDate = new Date(start_date.getTime() - offsetInMilliseconds);
      let isoString = adjustedDate.toISOString();
      start_date = new Date(isoString);

      let end_month = start_date.getMonth() + 5;
      end_date = new Date(start_date.getFullYear(), end_month + 1);

      end_date.setUTCHours(23, 59, 59, 999);
      let endDate_offsetInMilliseconds = end_date.getTimezoneOffset() * 60000; // 1 minute = 60000 milliseconds
      let endDate_adjustedDate = new Date(end_date.getTime() + endDate_offsetInMilliseconds);
      let endDate_isoString = endDate_adjustedDate.toISOString();
      end_date = new Date(endDate_isoString);

      // Add 6 months to the start month
      createdAt.setMonth(end_date.getMonth() + 1);
      if (end_date.getMonth() <= 5) {//for end_date.getMonth() value 0 means january
        createdAt.setFullYear(end_date.getFullYear());
      }
      if (createdAt > new Date()) {
        counter++;
      }
      if (end_date >= new Date()) {
        counter++;
      }
      array.push([start_date, end_date]);
    }
    return array;
  }

  loadGoogleScript(url) {
    return new Promise((resolve, reject) => {
      if (!document.querySelector('script[src="' + url + '"]')) {
        const script = this._documentRef.createElement('script');
        script.type = 'text/javascript';
        script.src = url;
        script.text = ``;
        script.async = true;
        script.defer = true;
        script.onload = resolve;
        script.onerror = reject;
        document.body.appendChild(script);
      } else {
        resolve(true);
      }
    })
  }
  //generate recaptcha token
  async getRecaptchaToken(): Promise<any> {
    try {
      const response: any = await this.http_post_method_requester(this.POST_METHOD.GET_SETTING_DETAIL, {});
  
      if (response.success) {
        const is_use_captcha = response.setting.is_use_captcha;
  
        if (is_use_captcha) {
          const recaptcha_site_key = response.setting.captcha_site_key_for_web;
          
          return new Promise((resolve, reject) => {
            grecaptcha.ready(async () => {
              try {
                const token = await grecaptcha.execute(recaptcha_site_key, { action: 'submit' });
                resolve(token);
              } catch (error) {
                reject(error);
              }
            });
          });
        } else {
          return false;
        }
      } else {
        return false;
      }
    } catch (error) {
      console.error('Error getting recaptcha token:', error);
      return false;
    }
  }
}