import { Injectable } from "@angular/core";
import { Helper } from './helper';

@Injectable({
    providedIn: 'root'
})

export class SubAdminUrl {

    public ADMIN_URL = [
        { index:0, actions: "10000", value: '/app/dashboard', NAME: this._helper._trans.instant('menu-title.dashboard'), route:'/app/dashboard' },
        { index:1, actions: "10101", value: '/admin/orders', NAME: this._helper._trans.instant('menu-title.orders'), route:'/app/order/running-orders/orders' },
        { index:2, actions: "10001", value: '/admin/delivery', NAME: this._helper._trans.instant('delivery'), route:'/app/order/running-orders/delivery'  },
        { index:3, actions: "10001", value: '/admin/table-booking', NAME: this._helper._trans.instant('menu-title.table-booking'), route:'/app/order/running-orders/table-booking' },
        { index:4, actions: "10101", value: '/admin/service', NAME: this._helper._trans.instant('menu-title.service'), route:'/app/order/running-orders/service' },
        { index:5, actions: "10101", value: '/admin/appointment', NAME: this._helper._trans.instant('menu-title.appointment'), route:'/app/order/running-orders/service' },
        { index:44, actions: "10101", value: '/admin/ecommerce', NAME: this._helper._trans.instant('label-title.ecommerce-orders'), route:'/app/order/running-orders/ecommerce' },
        { index:6, actions: "10100", value: '/admin/dispatch', NAME: this._helper._trans.instant('menu-title.dispatch'), route:'/app/order/running-orders/dispatch' },
        { index:7, actions: "10001", value: '/admin/history-list', NAME: this._helper._trans.instant('menu-title.history'), route:'/app/order/history/history-list' },
        { index:8, actions: "10001", value: '/admin/reviews', NAME: this._helper._trans.instant('menu-title.review'), route:'/app/order/history/reviews' },
        { index:46, actions: "10001", value: '/admin/report', NAME: this._helper._trans.instant('menu-title.report'), route:'/app/order/reports/report' },
        { index:9, actions: "10000", value: '/admin/store-location', NAME: this._helper._trans.instant('menu-title.store-location'), route:'/app/map-views/store-location' },
        { index:10, actions: "10000", value: '/admin/provider-location', NAME: this._helper._trans.instant('menu-title.provider-location'), route:'/app/map-views/provider-location' },
        { index:11, actions: "10000", value: '/admin/delivery-man-tracking', NAME: this._helper._trans.instant('menu-title.delivery-man-tracking'), route:'/app/map-views/delivery-man-tracking' },
        { index:12, actions: "10000", value: '/admin/business-area', NAME: this._helper._trans.instant('menu-title.business-area'), route:'/app/map-views/business-area' },
        { index:13, actions: "10001", value: '/admin/order-earning', NAME: this._helper._trans.instant('heading-title.order-earning'), route:'/app/earning/order/order-earning' },
        { index:14, actions: "10001", value: '/admin/deliveryman-weekly', NAME: this._helper._trans.instant('heading-title.partner-Earning'), route:'/app/earning/order/deliveryman-weekly' },
        { index:15, actions: "10001", value: '/admin/store-earning', NAME: this._helper._trans.instant('heading-title.store-earning'), route:'/app/earning/order/store-earning' },
        { index:16, actions: "10001", value: '/admin/wallet-history', NAME: this._helper._trans.instant('menu-title.wallet-history'), route:'/app/earning/wallet/wallet-history' },
        { index:17, actions: "10101", value: '/admin/wallet-request', NAME: this._helper._trans.instant('menu-title.wallet-request'), route:'/app/earning/wallet/wallet-request' },
        { index:18, actions: "10001", value: '/admin/transaction-history', NAME: this._helper._trans.instant('menu-title.transaction-history'), route:'/app/earning/wallet/transaction-history' },
        { index:43, actions: "10000", value: '/admin/redeem-history', NAME: this._helper._trans.instant('menu-title.redeem-history'), route:'/app/earning/wallet/redeem-history'  },
        { index:19, actions: "11100", value: '/admin/category', NAME: this._helper._trans.instant('menu-title.category'), route:'/app/menu/category' },
        { index:20, actions: "11110", value: '/admin/item-list', NAME: this._helper._trans.instant('menu-title.item'), route:'app/menu/item-list' },
        { index:21, actions: "11100", value: '/admin/modifier-group', NAME: this._helper._trans.instant('menu-title.modifier-group'), route:'/app/menu/modifier-group' },
        { index:23, actions: "11111", value: '/admin/user', NAME: this._helper._trans.instant('menu-title.user'), route:'/app/users/user' },
        { index:24, actions: "11111", value: '/admin/delivery-boy-list', NAME: this._helper._trans.instant('menu-title.delivery-boy-list'), route:'/app/users/delivery-boy-list' },
        { index:25, actions: "11111", value: '/admin/store', NAME: this._helper._trans.instant('menu-title.store'), route:'/app/users/store' },
        { index:26, actions: "11100", value: '/admin/business', NAME: this._helper._trans.instant('menu-title.delivery'), route:'/app/delivery-info/business' },
        { index:27, actions: "11110", value: '/admin/country-city-info', NAME: this._helper._trans.instant('menu-title.country-city-info'), route:'/app/delivery-info/country-city-info' },
        { index:28, actions: "11110", value: '/admin/d-fees-info', NAME: this._helper._trans.instant('menu-title.d-fees-info'), route:'/app/delivery-info/d-fees-info' },
        { index:29, actions: "11110", value: '/admin/admin-service', NAME: this._helper._trans.instant('menu-title.admin-service'), route:'/app/delivery-info/admin-service' },
        { index:45, actions: "11110", value: '/admin/ecommerce-service', NAME: this._helper._trans.instant('menu-title.ecommerce-service'), route:'/app/delivery-info/ecommerce-service' },
        { index:22, actions: "11100", value: '/admin/courier-service', NAME: this._helper._trans.instant('menu-title.courier-service'), route:'/app/delivery-info/courier-service' },
        { index:30, actions: "10100", value: '/admin/settings/admin', NAME: this._helper._trans.instant('menu-title.admin'), route:'/app/settings/basic-settings/admin' },
        { index:31, actions: "11100", value: '/admin/document', NAME: this._helper._trans.instant('menu-title.document'), route:'/app/settings/basic-settings/document' },
        { index:32, actions: "11111", value: '/admin/language', NAME: this._helper._trans.instant('menu-title.language'), route:'/app/settings/basic-settings/language' },
        { index:48, actions: "11110", value: '/admin/banner', NAME: this._helper._trans.instant('menu-title.banner'), route:'/app/settings/basic-settings/banner' },
        { index:33, actions: "11100", value: '/admin/offer', NAME: this._helper._trans.instant('menu-title.promo-code'), route:'/app/settings/discount/offer' },
        { index:34, actions: "11110", value: '/admin/advertise', NAME: this._helper._trans.instant('menu-title.advertise'), route:'/app/settings/discount/advertise' },
        { index:35, actions: "10000", value: '/admin/referral', NAME: this._helper._trans.instant('menu-title.referral-code'), route:'/app/settings/discount/referral' },
        { index:36, actions: "10100", value: '/admin/email-settings', NAME: this._helper._trans.instant('menu-title.email-settings'), route:'/app/settings/template/email-settings' },
        { index:37, actions: "10100", value: '/admin/sms-settings', NAME: this._helper._trans.instant('menu-title.sms-settings'), route:'/app/settings/template/sms-settings' },
        { index:38, actions: "11000", value: '/admin/mass-notification', NAME: this._helper._trans.instant('menu-title.mass-notification'), route:'/app/settings/template/mass-notification' },
        { index:39, actions: "10100", value: '/admin/legal', NAME: this._helper._trans.instant('menu-title.privacy-terms'), route:'/app/settings/template/legal' },
        { index:40, actions: "11110", value: '/admin/cancellation-reason', NAME: this._helper._trans.instant('menu-title.cancellation-reason'), route:'/app/settings/template/cancellation-reason' },
        { index:47, actions: "10001", value: '/admin/logs', NAME: this._helper._trans.instant('menu-title.logs'), route:'/app/settings/template/logs' },
        { index:41, actions: "10100", value: '/admin/seo/tags', NAME: this._helper._trans.instant('menu-title.tags'), route:'/app/seo/tags' },
        { index:42, actions: "11000", value: '/admin/seo/script', NAME: this._helper._trans.instant('menu-title.script'), route:'/app/seo/script' },
        
    ];

    constructor(private _helper: Helper) { }
}