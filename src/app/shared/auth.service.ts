import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

import { getUserRole } from 'src/app/utils/util';
import { Helper } from './helper';

export interface ISignInCredentials {
    username: string;
    password: string;
    captcha_token: any;
}

export interface ICreateCredentials {
    email: string;
    password: string;
    displayName: string;
}

export interface IPasswordReset {
    code: string;
    newPassword: string;
}

export interface Store {
    _id: string,
    email: string,
    displayName: string,
    country_phone_code: string,
    location: string,
    servertoken: string,
    currency: string
}

@Injectable({ providedIn: 'root' })
export class AuthService {

    loginAdminData: any;
    private loginAdmin = new BehaviorSubject<any>(null);
    loginSession = this.loginAdmin.asObservable();
    private permissions = new BehaviorSubject<any>(null);
    authPermission = this.permissions.asObservable();
    is_main_store_login;
    test = []

    constructor(private helper: Helper) { }

    async signIn(credentials: ISignInCredentials) {
        let captcha_token = await this.helper.getRecaptchaToken()
        credentials.captcha_token = captcha_token
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.ADMIN_LOGIN, credentials).then(async response_data => {
            if (response_data.success) {
                this.loginAdminData = {
                    username: response_data.admin_data.username,
                    displayName: response_data.admin_data.username,
                    _id: response_data.admin_data._id,
                    servertoken: response_data.admin_data.server_token,
                    admin_type: response_data.admin_data.admin_type,
                    jwt_token: response_data.admin_data.jwt_token,
                    is_rental: response_data.is_rental
                }
                this.helper.is_rental = response_data.is_rental;
                if (response_data.admin_data.admin_type === 3) {
                    this.subStoreSignIn(response_data.admin_data.urls)
                    this.is_main_store_login = false;
                    this.helper.is_main_admin = false;
                } else {
                    this.helper.is_main_admin = true;
                    this.is_main_store_login = true;
                    localStorage.setItem('adminData', JSON.stringify(this.loginAdminData));
                    this.loginAdmin.next(this.loginAdminData)

                }
            } else if (this.helper.is_use_captcha) {
                grecaptcha.reset()
            }
        })
    }

    check_subscription(){
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.CHECK_SUBCRIPTION, {})
    }

    create_subscription_session(sessionFormData){
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.CREATE_SUBSCRIPTION_SESSION, sessionFormData)
    }

    subStoreSignIn(urls) {
       
        const permissions = urls
        
        this.permissions.next(permissions)
        this.helper.permissions = permissions;
        this.is_main_store_login = false;
        localStorage.setItem('adminData', JSON.stringify(this.loginAdminData));
        localStorage.setItem('adminPermissions', JSON.stringify(permissions));
        this.loginAdmin.next(this.loginAdminData)
    }


    signOut() {
        return new Promise((resolve, rejects) => {
            localStorage.removeItem('adminData');
            localStorage.removeItem('storeData');
            localStorage.removeItem('adminPermissions');
            localStorage.removeItem('cartId');
            this.permissions.next(null)
            this.helper.permissions = [];
            this.helper.is_main_admin = true;
            this.helper.is_rental = false;
            this.loginAdminData = undefined;
            this.loginAdmin.next(null);
            this.helper.router.navigate(['/admin/login'])
        });
    }

    autologin() {
        this.loginAdminData = JSON.parse(localStorage.getItem('adminData'));
        this.loginAdmin.next(this.loginAdminData);
        if (this.loginAdminData && this.loginAdminData.servertoken) {
            let json = { admin_id: this.loginAdminData._id }
            this.helper.http_post_method_requester(this.helper.POST_METHOD.GET_PERMISSION, json).then((response: any) => {
                if (response.success) {
                    this.helper.is_rental = response.is_rental;
                    if (response.type == 3) {
                        let adminPermissions = response.url_array;
                        localStorage.setItem('adminPermissions', JSON.stringify(adminPermissions))
                        this.is_main_store_login = false;
                        this.helper.is_main_admin = false;
                    } else {
                        this.helper.is_main_admin = true;
                        this.is_main_store_login = true;
                    }
                }

                let adminPermissions = JSON.parse(localStorage.getItem('adminPermissions'));
                if (adminPermissions) {
                    this.is_main_store_login = false;
                    this.permissions.next(adminPermissions)
                    this.helper.permissions = adminPermissions;
                    this.helper.is_main_admin = false;
                } else {
                    this.is_main_store_login = true;
                    this.permissions.next(null)
                    this.helper.permissions = [];
                    this.helper.is_main_admin = true;
                }
            })
        } else {
            this.signOut()
        }
    }

    register(store: any) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.REGISTER, store)
    }

    sendPasswordEmail(json) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.FORGOT_PASSWORD, json)
    }

    resetPassword(json) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.NEW_PASSWORD, json)
    }

    getStore() {
        if (this.loginAdminData) {
            return this.loginAdminData;
        } else {
            this.loginAdminData = JSON.parse(localStorage.getItem('adminData'));
            return this.loginAdminData;
        }
    }

    async getUser() {
        return { displayName: this.loginAdminData.displayName, role: getUserRole() };
    }

    otpVerify(json) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.FORGOT_PASSWORD_VERIFY, json)
    }
}
