import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor, HttpResponse, HttpErrorResponse
} from '@angular/common/http';
import { Observable, of } from "rxjs";
import { tap, catchError } from "rxjs/operators";
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from '../shared/auth.service';
import { NotifiyService } from '../services/notifier.service';
import { Helper } from '../shared/helper';

@Injectable()
export class ResInterceptInterceptor implements HttpInterceptor {

  constructor(
    private _notifierService: NotifiyService,
    private _auth:AuthService,
    private translate: TranslateService,
    private _helper:Helper) { }

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {

    return next.handle(req).pipe(
      tap(evt => {
        if (evt instanceof HttpResponse && evt.body) {
          if (evt.body.success) {
            if (evt.body.status_phrase && req.body?.is_show_success_toast !== false) {
              this.onSuccess(evt.body.status_phrase)
            }
          } else if (evt.body.error_code === 999) {
            this._auth.signOut()
          } else if (evt.body.status_phrase && req.body?.is_show_error_toast !== false) {
            this.onError(evt.body.status_phrase);
          }
        }
      }),
      catchError((err: any) => {
        if (err instanceof HttpErrorResponse) {
          if(err.status != 200){
            console.log(this._helper._trans.instant('error_code.2003'));
          }
        }
        return of(err);
      }));

  }

  onError(status_phrase): void {
    if(status_phrase){
      this._notifierService.showNotification('error', status_phrase);
    }
  }

  onSuccess(status_phrase): void {
    if (status_phrase) {
      this._notifierService.showNotification('success', status_phrase);
    }
  }
}
