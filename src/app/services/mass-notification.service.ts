import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { AuthService } from '../shared/auth.service';
import { Helper } from '../shared/helper';

@Injectable({ providedIn: 'root' })
export class MassNotificationService {
    private _notificationChanges = new BehaviorSubject<any>(null);
    _notificationObservable = this._notificationChanges.asObservable()

    constructor(private helper: Helper, private _auth: AuthService) { }

    fetch(json) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.GET_MASS_NOTIFICATION_LIST, json)
    }

    sendMassNotification(json) {
        this.helper.http_post_method_requester(this.helper.POST_METHOD.CREATE_MASS_NOTIFICATION, json).then(res => {
            if (res.success) {
                this._notificationChanges.next({})
            }
        })
    }

}
