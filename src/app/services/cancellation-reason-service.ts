import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { AuthService } from '../shared/auth.service';
import { Helper } from '../shared/helper';


@Injectable({ providedIn: 'root' })
export class CancellationReasonService {
    private _advertiseChanges = new BehaviorSubject<any>(null);
    _addvertiseObservable = this._advertiseChanges.asObservable();

    constructor(private helper: Helper, private _auth: AuthService) { }

    list(json) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.CANCELLATION_REASON_LIST, json)
    }

    addCancellationReason(form_data) {
        this.helper.http_post_method_requester(this.helper.POST_METHOD.ADD_CANCELLATION_REASON, form_data).then(res => {
            if (res.success) {
                this._advertiseChanges.next({})
            }
        })
    }

    deleteCancellationReason(reason_id) {
        this.helper.http_post_method_requester(this.helper.POST_METHOD.DELETE_CANCELLATION_REASON, { reason_id }).then(res => {
            if (res.success) {
                this._advertiseChanges.next({})
            }
        })
    }

    updateCancellationReason(form_data) {
        this.helper.http_post_method_requester(this.helper.POST_METHOD.UPDATE_CANCELLATION_REASON, form_data).then(res => {
            if (res.success) {
                this._advertiseChanges.next({})
            }
        })
    }

    getCancellationReasonDetail(json) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.GET_CANCELLATION_REASON_DETAIL, json)
    }
}
