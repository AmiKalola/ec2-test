import { Injectable } from '@angular/core';
import { AuthService } from '../shared/auth.service';
import { Helper } from '../shared/helper';

@Injectable({ providedIn: 'root' })
export class AddDetailsService {

    constructor(private helper: Helper, private _auth: AuthService) { }

    get_country_list() {
        return this.helper.http_get_method_requester(this.helper.GET_METHOD.GET_COUNTRY_LIST, {})
    }

    get_delivery_list() {
        return this.helper.http_get_method_requester(this.helper.POST_METHOD.GET_DELIVERY_LIST, {})
    }

    get_provider_list() {
        return this.helper.http_get_method_requester(this.helper.GET_METHOD.GET_PROVIDERS, {})
    }

    get_city_list(json) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.GET_CITY_LIST, json)
    }

    get_store_list_by_delivery(json) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.GET_STORE_LIST_BY_DELIVERY, json)
    }

    add_new_store(json) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.ADD_NEW_STORE, json)
    }

    add_new_user(json) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.ADD_NEW_USER, json)
    }

    add_new_provider(json) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.ADD_NEW_PROVIDER, json)
    }

    add_provider_vehicle_data(json) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.ADD_PROVIDER_VEHICLE_DATA, json)
    }

    update_database(json) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.UPDATE_DATABASE_TABLE, json)
    }

    update_new_item_database(json) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.UPDATE_ITEM_NEW_TABLE, json)
    }

}
