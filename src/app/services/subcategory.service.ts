import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { AuthService } from '../shared/auth.service';
import { Helper } from '../shared/helper';
import { DELIVERY_TYPE_CONSTANT } from '../shared/constant';


export interface SubcategoryModel {
	_id: string,
	name: any,
	sequence_number: number
}


@Injectable({ providedIn: 'root' })
export class SubcategoryService {

	private _subcategoryChanges = new BehaviorSubject<any>(null);
	_subcategoryObservable = this._subcategoryChanges.asObservable()

	constructor(private helper: Helper, private _auth: AuthService) { }

	list(store_id = this.helper.selected_store_id, delivery_type = this.helper.delivery_type) {
		return this.helper.http_post_method_requester(this.helper.POST_METHOD.GET_PRODUCT_LIST_OF_GROUP, { store_id, "type": 1, delivery_type, city_id: this.helper.selected_city._id })
	}

	fetch(id) {
		let json = { store_id: this.helper.selected_store_id, type: "1", product_id: id, delivery_type: this.helper.delivery_type, city_id: this.helper.selected_city._id }
		return this.helper.http_post_method_requester(this.helper.POST_METHOD.GET_PRODUCT_DATA, json)
	}

	add(addSubcategory: SubcategoryModel) {
		let json = {
			...addSubcategory,
			type: "1",
			store_id: this.helper.selected_store_id,
			is_visible_in_store: true,
			city_id: null
		}
		if (this.helper.delivery_type == DELIVERY_TYPE_CONSTANT.ECOMMERCE) {
			json.city_id = this.helper.selected_city_id
		}

		return this.helper.http_post_method_requester(this.helper.POST_METHOD.ADD_PRODUCT, json).then(() => {
			this._subcategoryChanges.next(true)
		})
	}

	update(editSubcategory: SubcategoryModel) {
		let json: any = {
			...editSubcategory,
			product_id: editSubcategory._id,
			type: "1",
			store_id: this.helper.selected_store_id,
		}
		if (this.helper.selected_city) {
			json.city_id = this.helper.selected_city._id
		}
		return this.helper.http_post_method_requester(this.helper.POST_METHOD.UPDATE_PRODUCT, json).then(() => {
			this._subcategoryChanges.next(true)
		})
	}

}
