import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';
import { AuthService } from '../shared/auth.service';
import { Helper } from '../shared/helper';

@Injectable({ providedIn: 'root' })
export class CityService {
    public _cityChanges = new BehaviorSubject<any>(null);
    _cityObservable = this._cityChanges.asObservable()
    public _citySelect = new Subject<any>();
    public _addCity = new Subject<any>();
    public _unselectCity = new Subject<any>();

    constructor(private helper: Helper, private _auth: AuthService) { }

    fetch() {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.CITY_LIST, {})
    }

    addCity(json) {
        this.helper.http_post_method_requester(this.helper.POST_METHOD.ADD_CITY_DATA, json).then((res) => {
            if (res.success) {
                this._cityChanges.next({})
            }
        })
    }

    updateCity(json) {
        this.helper.http_post_method_requester(this.helper.POST_METHOD.UPDATE_CITY_DATA, json).then((res) => {
            if (res.success) {
                if (json.city_id) {
                    this._cityChanges.next(json.city_id)
                }
            }
        })
    }

    checkCity(json) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.CHECK_CITY, json)
    }

    getCityDetail(json) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.GET_CITY_DETAIL, json)
    }

    getEcommerceCityList(json) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.GET_ECOMMERCE_CITY_LIST, json)
    }

    getEcommerceCountryWiseCityList(json) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.GET_ECOM_SERVICE_COUNTRY_WISE_CITYLIST, json)
    }

    getDestinationCityList(json) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.GET_DESTINATION_CITY_LIST, json)
    }

    deleteZone(zone_id, city_id) {
        let parameters = {
            zone_id: zone_id,
            city_id: city_id
        }
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.DELETE_CITY_ZONE, parameters)
    }
    
    fetch_destination_citylist(json){
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.FETCH_DESTINATION_CITIES, json)
	}
    
    fetch_airport(json){
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.CITY_FETCH_AIRPORT_DETAILS, json)
	}

    updateAirportZone(json){
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.CITY_AIRPORTZONE_UPDATE, json)
	}

    fetch_redzone(json){
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.CITY_FETCH_REDZONE_DETAILS, json)
	}

    updateRedZone(json){
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.CITY_REDZONE_UPDATE, json)
	}

}
