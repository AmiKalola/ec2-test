import { Injectable } from '@angular/core';
import { AuthService } from '../shared/auth.service';
import { Helper } from '../shared/helper';

@Injectable({
    providedIn: 'root'
})
export class ReferralService {

    constructor(private helper: Helper, private _auth: AuthService) { }

    getRefferralHistory(json) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.GET_REFERRAL_HISTORY_LIST, json)
    }
}
