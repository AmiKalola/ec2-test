import { Injectable } from '@angular/core';
import { AuthService } from '../shared/auth.service';
import { Helper } from '../shared/helper';
import { BehaviorSubject } from 'rxjs';

export enum user_page_type {
    "approved", "blocked"
}

@Injectable({
    providedIn: 'root'
})
export class UserService {

    public _userChanges = new BehaviorSubject<any>(null);
    _userObservable = this._userChanges.asObservable()

    constructor(private helper: Helper, private _auth: AuthService) { }

    async getUsers(type: user_page_type, page, itemsPerPage, search_field, search_value, filterLists) {
        let json = {
            "number_of_rec": itemsPerPage,
            "page": page,
            "user_page_type": type === user_page_type.approved ? 1 : 2,
            "search_field": search_field,
            "search_value": search_value,
            "filterLists": filterLists
        }
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.USER_LIST_SEARCH_SORT, json);

    }

    getUserDetail(data) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.GET_USER_DETAIL, data)
    }

    getDocumentList(data) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.VIEW_DOCUMENT_LIST, data)
    }

    updateDocumentDetail(data) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.UPLOAD_DOCUMENT, data)
    }

    getReferralUsageList(data) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.GET_REFERRAL_HISTORY, data)
    }

    getReviewList(data) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.GET_USER_REVIEW_HISTORY, data)
    }

    async updateUserDetail(data) {
        const res_data = await this.helper.http_post_method_requester(this.helper.POST_METHOD.UPDATE_USER, data);
        this._userChanges.next({})
        return res_data;
    }

    async approveDeclineUser(data) {
        const res_data = await this.helper.http_post_method_requester(this.helper.POST_METHOD.APPROVE_DECLINE_USER, data);
        this._userChanges.next({})
        return res_data;
    }

    deleteAccount(json) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.DELETE_USER, json)
    }

}
