import { Injectable } from '@angular/core';
import { Helper } from '../shared/helper';
import { AuthService } from '../shared/auth.service';


@Injectable({ providedIn: 'root' })
export class DeliveriesService {

    constructor(private helper: Helper, private _auth: AuthService) { }

    delivery_list_search_sort(json) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.DELIVERY_LIST_SEARCH_SORT, json)
    }

    admin_requests_datail(json) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.ADMIN_REQUESTS_DETAIL, json)
    }
}

