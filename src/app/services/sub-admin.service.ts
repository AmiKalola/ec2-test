import { Injectable } from '@angular/core';
import { Helper } from '../shared/helper';
import { AuthService } from '../shared/auth.service';
import { BehaviorSubject } from 'rxjs';


@Injectable({ providedIn: 'root' })
export class SubAdminService {

    private _subAdminChange = new BehaviorSubject<any>(null);
    _subcategoryObservable = this._subAdminChange.asObservable()

    constructor(private helper: Helper, private _auth: AuthService) {
    }

    getAdminList() {
        return this.helper.http_get_method_requester(this.helper.GET_METHOD.ADMIN_LIST, {})
    }

    getDetails(id) {
        let json = {
            admin_id: id
        }
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.GET_DETAILS, json)
    }

    async addAdmin(json) {
        let captcha_token = await this.helper.getRecaptchaToken()
        json.captcha_token = captcha_token;
        json.device_type = "web";
        let res_data = await this.helper.http_post_method_requester(this.helper.POST_METHOD.ADD_ADMIN, json);
        if (res_data.success) {
            this._subAdminChange.next({})
        }
        return res_data;
    }

    async updateAdmin(json) {
        let res_data = await this.helper.http_post_method_requester(this.helper.POST_METHOD.UPDATE_ADMIN, json);
        if (res_data.success) {
            this._subAdminChange.next({})
        }
        return res_data;
    }

    async deleteAdmin(_json) {
        this.helper.http_post_method_requester(this.helper.POST_METHOD.DELETE, _json).then(res_data => {
            if (res_data.success) {
                this._subAdminChange.next({})
            }
        })
    }
}
