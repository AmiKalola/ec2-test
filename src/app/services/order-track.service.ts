import { Injectable } from '@angular/core';
import { AuthService } from '../shared/auth.service';
import { Helper } from '../shared/helper';

@Injectable({
    providedIn: 'root'
})
export class OrderTrackService {

    constructor(private helper: Helper, private _auth: AuthService) { }

    getProviderListForCity(city_id) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.GET_PROVIDER_LIST_FOR_CITY, { city_id })
    }

    getOrderListForCity(city_id) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.GET_ORDER_LIST_FOR_LOCATION_TRACK, { city_id })
    }

    deliverymanTrack(id, type) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.DELIVERYMAN_TRACK, { id, type })
    }


}
