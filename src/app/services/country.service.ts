import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { AuthService } from '../shared/auth.service';
import { Helper } from '../shared/helper';

@Injectable({ providedIn: 'root' })
export class CountryService {
    public _countryChanges = new BehaviorSubject<any>(null);
    _countryObservable = this._countryChanges.asObservable()

    constructor(private helper: Helper, private _auth: AuthService) { }

    fetch() {
        return this.helper.http_get_method_requester(this.helper.GET_METHOD.GET_ADMIN_COUNTRY_LIST, {})
    }

    addCountry(json) {
        this.helper.http_post_method_requester(this.helper.POST_METHOD.ADD_COUNTRY_DATA, json).then(res => {
            if (res.success) {
                this._countryChanges.next({})
            }
        })
    }

    updateCountry(json) {
        this.helper.http_post_method_requester(this.helper.POST_METHOD.UPDATE_COUNTRY, json).then(res => {
            if (res.success) {
                if (json.country_id) {
                    this._countryChanges.next(json.country_id)
                }
            }
        })
    }

    getAllCountry() {
        return this.helper.http_get_method_requester(this.helper.GET_METHOD.GET_ALL_COUNTRY, {})
    }

    getCountryDetailForAdmin(json) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.COUNTRY_DETAIL_FOR_ADMIN, json)
    }

    getTimezoneList() {
        return this.helper.http_get_method_requester(this.helper.GET_METHOD.GET_TIMEZONE_LIST, {})
    }

    getCountryData(json) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.GET_COUNTRY_DATA, json)
    }

    getServerCountryList() {
        return this.helper.http_get_method_requester(this.helper.GET_METHOD.GET_SERVER_COUNTRY_LIST, {})
    }

    getCountryTimeZones(json) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.GET_COUNTRY_TIMEZONE, json)
    }

    add_tax(json) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.ADD_TAX, json)
    }

    edit_tax(json) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.EDIT_TAX, json)
    }

    getCountryList() {
        return this.helper.http_get_method_requester(this.helper.GET_METHOD.GET_COUNTRY_LIST, {})
    }

}
