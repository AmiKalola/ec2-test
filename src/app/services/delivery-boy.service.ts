import { Injectable } from '@angular/core';
import { AuthService } from '../shared/auth.service';
import { Helper } from '../shared/helper';
import { BehaviorSubject } from 'rxjs';

export enum provider_page_type {
    "online", "approved", "blocked"
}

@Injectable({
    providedIn: 'root'
})
export class ProviderService {

    private _providerChanges = new BehaviorSubject<any>(null);
    _providerObservable = this._providerChanges.asObservable()

    constructor(private helper: Helper, private _auth: AuthService) { }

    async getProviders(options: {
        type: provider_page_type,
        page: number,
        itemsPerPage: number,
        search_field: string,
        search_value: string,
        filterLists: any, // Adjust the type accordingly
        selectedDeliveryType: any, // Adjust the type accordingly
        selectedMerchantType: number,
        selectedDeliveryId: any
    }) {
        let page_type;
        switch (options.type) {
            case provider_page_type.online:
                page_type = 1;
                break;
            case provider_page_type.approved:
                page_type = 2;
                break;
            case provider_page_type.blocked:
                page_type = 3;
                break;
        }
        let json = {
            "number_of_rec": options.itemsPerPage,
            "page": options.page,
            "provider_page_type": page_type,
            "search_field": options.search_field,
            "search_value": options.search_value,
            "filterLists": options.filterLists,
            "delivery_type": options.selectedDeliveryType,
            "partner_type": options.selectedMerchantType,
            "business_id": options.selectedDeliveryId
        }
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.PROVIDER_LIST_SEARCH_SORT, json);

    }

    getProviderDetail(data) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.GET_PROVIDER_DETAIL, data)
    }

    getDocumentList(data) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.VIEW_DOCUMENT_LIST, data)
    }

    updateDocumentDetail(data) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.UPLOAD_DOCUMENT, data)
    }

    getReferralUsageList(data) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.GET_REFERRAL_HISTORY, data)
    }

    getReviewList(data) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.GET_PROVIDER_REVIEW_HISTORY, data)
    }

    async updateProviderDetail(data) {
        let res_data = await this.helper.http_post_method_requester(this.helper.POST_METHOD.UPDATE_PROVIDER, data);
        this._providerChanges.next({})
        return res_data;
    }

    async approveDeclineProvider(data) {
        let res_data = await this.helper.http_post_method_requester(this.helper.POST_METHOD.APPROVE_DECLINE_PROVIDER, data);
        this._providerChanges.next({})
        return res_data;
    }

    getProviderListForMap() {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.PROVIDER_LIST_FOR_MAP, {})
    }

    providerVehicleList(json) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.GET_STORE_PROVIDER_VEHICLE, json)
    }

    providerVehicelUpdate(json) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.UPDATE_STORE_PROVIDER_VEHICLE, json)
    }

    upload_documents(form_data) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.UPLOAD_DOCUMENT, form_data)
    }

    deleteAccount(json) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.DELETE_PROVIDER, json)
    }
}
