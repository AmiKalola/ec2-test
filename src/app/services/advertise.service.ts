import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { AuthService } from '../shared/auth.service';
import { Helper } from '../shared/helper';

@Injectable({ providedIn: 'root' })
export class AdvertiseService {
    private _advertiseChanges = new BehaviorSubject<any>(null);
    _addvertiseObservable = this._advertiseChanges.asObservable();

    constructor(private helper: Helper, private _auth: AuthService) { }

    list(json) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.ADVERTISE_LIST, json)
    }

    addAdvertise(form_data) {
        this.helper.http_post_method_requester(this.helper.POST_METHOD.ADD_ADVERTISE, form_data).then(res => {
            if (res.success) {
                this._advertiseChanges.next({})
            }
        })
    }

    deleteAdvertise(advertise_id) {
        this.helper.http_post_method_requester(this.helper.POST_METHOD.DELETE_ADVERTISE, { advertise_id }).then(res => {
            if (res.success) {
                this._advertiseChanges.next({})
            }
        })
    }

    updateAdvertise(form_data) {
        this.helper.http_post_method_requester(this.helper.POST_METHOD.UPDATE_ADVERTISE, form_data).then(res => {
            if (res.success) {
                this._advertiseChanges.next({})
            }
        })
    }

    getAdvertiseDetails(json) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.GET_ADVERTISE_DETAIL, json)
    }
}
