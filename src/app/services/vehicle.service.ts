import { Injectable } from '@angular/core';
import { AuthService } from '../shared/auth.service';
import { Helper } from '../shared/helper';
import { BehaviorSubject } from 'rxjs';

export enum user_page_type {
    "approved", "blocked"
}

@Injectable({
    providedIn: 'root'
})
export class VehicleService {

    private _vehicleChanges = new BehaviorSubject<any>(null);
    _vehicleObservable = this._vehicleChanges.asObservable()

    constructor(private helper: Helper, private _auth: AuthService) { }

    get_vehicle_list() {
        return this.helper.http_get_method_requester(this.helper.GET_METHOD.VEHICLE_LIST, {})
    }

    add_vehicle_data(json) {
        this.helper.http_post_method_requester(this.helper.POST_METHOD.ADD_VEHICLE_DATA, json).then(() => {
            this._vehicleChanges.next({})
        })
    }

    update_vehicle(json) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.UPDATE_VEHICLE, json).then(() => {
            this._vehicleChanges.next({})
        })
    }

    get_vehicle_details(json) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.GET_VEHICLE_DETAIL, json)
    }

}
