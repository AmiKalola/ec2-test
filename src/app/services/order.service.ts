import { Injectable } from '@angular/core';
import { Helper } from '../shared/helper';
import { AuthService, Store } from '../shared/auth.service';
import { BehaviorSubject } from 'rxjs';
import { DELIVERY_TYPE_CONSTANT } from '../shared/constant';


@Injectable({ providedIn: 'root' })
export class OrderService {

    private store: Store;
    private _orderChanges = new BehaviorSubject<any>(null);
    _orderObservable = this._orderChanges.asObservable()

    constructor(private helper: Helper, private _auth: AuthService) {
        this.store = this._auth.getStore();
    }

    list_history(page = 1, perPage = 10, query = {}, start_date: any = "", end_date: any = "", status: any = "", business_type : any = []) {
        let json = {
            "start_date": start_date,
            "end_date": end_date,
            ...query,
            "created_by": "both",
            "payment_status": "all",
            "pickup_type": "both",
            "order_status_id": status.value,
            "page": page,
            "order_type": "both",
            "no_of_records": perPage,
            "business_type": business_type
        }
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.LIST_ORDERS_HISTORY, json)
    }


    get_order_detail(orderid, new_store_id = null) {
        let json = {
            server_token: this.store.servertoken,
            store_id: new_store_id ? new_store_id : this.store._id,
            order_id: orderid
        }
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.FETCH_ORDER_DETAIL, json)
    }


    list_orders(start_date = null, end_date = null, page = 1, perPage = 10, query = {}, delivery_type = 1) {
        let store_id = null;
        if (delivery_type !== DELIVERY_TYPE_CONSTANT.TAXI) {
            store_id = this.store._id
        }
        let json = {
            server_token: this.store.servertoken,
            store_id: store_id,
            start_date,
            end_date,
            page,
            perPage,
            query,
            delivery_type
        }
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.LIST_ORDERS, json)
    }

    table_list_orders(start_date = null, end_date = null, page = 1, perPage = 10, search_by = 'user', search_value = '', order_id = null) {
        let json = {
            server_token: this.store.servertoken,
            store_id: this.store._id,
            start_date,
            end_date,
            page,
            perPage,
            search_by,
            search_value,
            order_id
        }
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.TABLE_LIST_ORDERS, json)
    }

    list_deliveries(start_date = null, end_date = null, page = 1, perPage = 10, query = {}) {
        let json = {
            server_token: this.store.servertoken,
            admin: this.store._id,
            start_date,
            end_date,
            page,
            perPage,
            query
        }
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.LIST_DELIVERIES, json)
    }

    set_order_status(order_id, order_status) {
        let json = {
            server_token: this.store.servertoken,
            store_id: this.store._id,
            order_status,
            order_id
        }
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.SET_ORDER_STATUS, json).then(() => {
            this._orderChanges.next({})
        })
    }

    dispatcher_order() {
        let json = {
            server_token: this.store.servertoken,
            store_id: this.store._id
        }
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.GET_DISPATCHER_ORDER_LIST, json)
    }
    cancel_request(request_id, provider_id) {
        let json = {
            server_token: this.store.servertoken,
            provider_id: provider_id,
            request_id: request_id,
            store_id: this.store._id
        }
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.CANCEL_REQUEST, json)
    }
    create_request(provider_id, order_id, vehicle_id, request_id) {
        let json = {
            server_token: this.store.servertoken,
            provider_id: provider_id,
            order_id: order_id,
            vehicle_id: vehicle_id,
            request_id: request_id,
            store_id: this.store._id
        }
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.CREATE_REQUEST, json)
    }
    complete_order(order_id) {
        let json = {
            server_token: this.store.servertoken,
            store_id: this.store._id,
            order_id
        }

        return this.helper.http_post_method_requester(this.helper.POST_METHOD.COMPLETE_ORDER, json).then(() => {
            this._orderChanges.next({})
        })
    }

    user_cancle_order(json) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.USER_CANCLE_ORDER, json).then(() => {
            this._orderChanges.next({})
        })
    }
    get_request_data(json) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.GET_REQUEST_DATA, json)
    }

    get_details_country_city_wise_list(json) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.GET_DETAILS_COUNTRY_CITY_WISE, json)
    }

    async get_report_list(json): Promise<any> {
        try {
            const response = await this.helper.http_post_method_requester(this.helper.GET_METHOD.GET_ORDER_REPORT, json)
            if (response.success) {
                return response;
            } else {
                return true
            }
        } catch (error) {
            return false;
        }
    }

}
