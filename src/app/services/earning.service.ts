import { Injectable } from '@angular/core';
import { Helper } from '../shared/helper';
import { AuthService, Store } from '../shared/auth.service';


@Injectable({ providedIn: 'root' })
export class EarningService {

    private store: Store;

    constructor(private helper: Helper, private _auth: AuthService) {
    }

    async list(start_date = null, end_date = null, page = 1, perPage = 10, query = {}, business_type : any = []): Promise<any> {
        return new Promise((resolve, rejects) => {
            this.store = this._auth.getStore();
            let json = {
                server_token: this.store.servertoken,
                store_id: this.store._id,
                start_date,
                end_date,
                page,
                perPage,
                query,
                business_type
            }
            this.helper.http_post_method_requester(this.helper.POST_METHOD.LIST_EARNING, json).then((res_data) => {
                if (res_data['success']) {
                    resolve(res_data['data']);
                } else {
                    resolve({
                        results: [],
                        count: 0
                    })
                }
            })
        })

    }

    async fetch_detail(order_payment_id): Promise<any> {
        let json = {
            order_payment_id: order_payment_id
        }
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.FETCH_EARNING_DETAIL, json)
    }


    list_store_earning(json = { start_date: null, end_date: null, search_field: '', search_value: '', page: 1, number_of_rec: 10 }) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.LIST_STORE_EARNING, json)
    }

    fetch_store_earning(json = { start_date: null, end_date: null, search_field: '', search_value: '', page: 1, type: 1, store_id: null }) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.WEEKLY_EARNING, json)
    }

    list_deliveryman_earning(json = { start_date: null, end_date: null, search_field: '', search_value: '', page: 1 }) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.LIST_DELIVERYMAN_EARNING, json)
    }

    fetch_deliveryman_earning(json = { start_date: null, end_date: null, search_field: '', search_value: '', page: 1, type: 1, provider_id: null }) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.FETCH_DELIVERYMAN_EARNING, json)
    }


}
