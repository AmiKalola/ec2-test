import { Injectable } from '@angular/core';
import { Helper } from '../shared/helper';
import { AuthService } from '../shared/auth.service';


@Injectable({ providedIn: 'root' })
export class DeliveryFeesService {

    constructor(private helper: Helper, private _auth: AuthService) {

    }

    get_service_list() {
        let json = {
            page: 1,
            search_field: "country_details.country_name",
            search_value: "",
            sort_field: "unique_id",
            sort_service: -1
        }
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.SERVICE_LIST, json)
    }

    get_server_country_list() {
        return this.helper.http_get_method_requester(this.helper.GET_METHOD.GET_SERVER_COUNTRY_LIST, {})
    }

    get_server_delivery_types() {
        return this.helper.http_get_method_requester(this.helper.GET_METHOD.GET_DELIVERY_TYPES, {})
    }

    get_city_lists(country_id) {
        let json = {
            country_id: country_id
        }
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.GET_CITY_LISTS, json)
    }

    get_vehicle_list(id, is_business = null) {
        let json = {
            city_id: id,
            delivery_type: "",
            is_business: is_business
        }
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.GET_VEHICLE_LIST, json)
    }

    get_zone_details(id) {
        let json = {
            city_id: id
        }
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.GET_ZONE_DETAIL, json)
    }

    add_zone_price(json) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.ADD_ZONE_PRICE, json)
    }

    update_zone_price(json) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.UPDATE_ZONE_PRICE, json)
    }

    setup_courier_service(json) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.SETUP_COURIER_SERVICE, json)
    }

    add_service_price(json) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.ADD_SERVICE_DATA, json)
    }

    get_service_details(id) {
        let json = {
            service_id: id
        }
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.GET_SERVICE_DETAIL, json)
    }

    update_service(json) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.UPDATE_SERVICE, json)
    }

    copy_item(json) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.COPY_DATA, json)
    }

    get_delivery_service_list(json) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.GET_SERVICE_LIST_FOR_DELIVERY, json)
    }

    check_zone_price_exists(json) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.CHECK_ZONE_PRICE_EXISTS, json)
    }

    get_airport_price_list(json) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.FETCH_AIRPORT_PRICE_LIST, json)
    }

    get_city_price_list(json) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.FETCH_CITY_PRICE_LIST, json)
    }

    update_airport_city_price_list(json) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.UPDATE_AIRPORT_CITY_PRICE, json)
    }

    update_city_to_city_price_list(json) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.UPDATE_CITY_TO_CITY_PRICE, json)
    }

    get_rich_surge_list(json) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.FETCH_RICH_SURGE, json)
    }

    delete_zone_price(json) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.DELETE_ZONE_PRICE, json)
    }
    
}
