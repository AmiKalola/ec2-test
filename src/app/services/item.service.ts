import { Injectable } from '@angular/core';
import { Helper } from '../shared/helper';
import { BehaviorSubject } from 'rxjs';
import { DELIVERY_TYPE_CONSTANT } from '../shared/constant';

export interface SpecificationModel {
    _id: string,
    type: number,
    is_required: boolean,
    max_range: any,
    range: number,
    name: any,
    sequence_number: number,
    list: [
        {
            _id: string,
            name: any,
            sequence_number: number,
            price: number,
            is_default_selected: boolean,
            is_user_selected: boolean
        }
    ]
}

export interface ItemModel {
    _id: string,
    name: any,
    sequence_number: number,
    product_id: string,
    details: any,
    price: string,
    tax: number,
    is_visible_in_store: boolean,
    is_item_in_stock: boolean,
    is_most_popular: boolean,
    specifications: [SpecificationModel]
}

@Injectable({ providedIn: 'root' })
export class ItemService {

    private _itemChanges = new BehaviorSubject<any>(null);
    _itemObservable = this._itemChanges.asObservable()

    constructor(private helper: Helper) { }

    list(subcategory_id, store_id) {
        let json: any = {
            store_id,
            product_id: subcategory_id,
            delivery_type: this.helper.delivery_type
        }
        if (this.helper.selected_city) {
            json.city_id = this.helper.selected_city._id
        }
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.GET_STORE_PRODUCT_ITEM_LIST, json)
    }

    fetch(id) {
        let json = {
            store_id: this.helper.selected_store_id,
            type: '1',
            item_id: id,
            delivery_type: this.helper.delivery_type,
            city_id: this.helper.selected_city._id
        }
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.GET_ITEM_DATA, json)
    }


    add(addItem: ItemModel) {
        let json = {
            store_id: this.helper.selected_store_id,
            type: 1,
            city_id: null,
            ...addItem
        }
        if (this.helper.delivery_type == DELIVERY_TYPE_CONSTANT.ECOMMERCE) {
            json.city_id = this.helper.selected_city._id
        }
        delete json._id;
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.ADD_ITEM, json).then((data) => {
            setTimeout(() => {
                this._itemChanges.next(true)
            }, 3000)
            return data;
        })
    }

    update(editItem: ItemModel) {
        let json = {
            type: 1,
            ...editItem,
            item_id: editItem._id
        }
        if (this.helper.selected_store_id) {
            json['store_id'] = this.helper.selected_store_id
        }
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.UPDATE_ITEM, json).then(() => {
            setTimeout(() => {
                this._itemChanges.next(true)
            }, 3000)
        })
    }

    update_images(formData) {
        formData.append('store_id', this.helper.selected_store_id)
        formData.append('type', 1)
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.UPDATE_ITEM_IMAGE, formData)
    }

    delete_images(item_id, image_url) {
        let json = {
            store_id: this.helper.selected_store_id,
            type: 1,
            image_url,
            _id: item_id
        }
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.DELETE_ITEM_IMAGE, json)
    }

}
