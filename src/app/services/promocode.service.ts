import { Injectable } from '@angular/core';
import { Helper } from '../shared/helper';
import { AuthService, Store } from '../shared/auth.service';
import { BehaviorSubject } from 'rxjs';

export interface PromoModel {
    promo_code_name: string,
    promo_code_value: string,
    promo_details: string
}

export enum promocode_page_type {
    "running", "inactive", "unapproved", "expired", "user"
}

@Injectable({ providedIn: 'root' })
export class PromocodeService {

    private store: Store;
    private _offerChanges = new BehaviorSubject<any>(null);
    _offerObservable = this._offerChanges.asObservable()

    constructor(private helper: Helper, private _auth: AuthService) {
        this.store = this._auth.getStore();
    }

    list(type, currentPage, itemsPerPage, search_field, search_value, promo_for, business_id) {
        let page_type;
        switch (type) {
            case promocode_page_type.running:
                page_type = 1;
                break;
            case promocode_page_type.inactive:
                page_type = 2;
                break;
            case promocode_page_type.unapproved:
                page_type = 3;
                break;
            case promocode_page_type.expired:
                page_type = 4;
                break;
            case promocode_page_type.user:
                page_type = 5;
                break;
        }
        let data = {
            page: currentPage,
            search_field: search_field,
            search_value: search_value,
            sort_field: "unique_id",
            sort_promo_code: -1,
            page_type: page_type,
            number_of_rec: itemsPerPage,
            promo_for: typeof promo_for === 'number' ? promo_for : '',
            business_id: business_id ? business_id : ''
        }
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.PROMO_CODE_LIST, data);
    }

    get_promo_uses_detail(promo_id) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.GET_PROMO_USES_DETAIL, { promo_id: promo_id });
    }


    async add(addPromo) {
        const res_data = await this.helper.http_post_method_requester(this.helper.POST_METHOD.ADD_PROMO, addPromo)
        this._offerChanges.next({});
        return res_data;
    }

    async update(addPromo) {
        const res_data = await this.helper.http_post_method_requester(this.helper.POST_METHOD.UPDATE_PROMO, addPromo)
        this._offerChanges.next({});
        return res_data;

    }

    get get_country_list() {
        return this.helper.http_get_method_requester(this.helper.GET_METHOD.GET_COUNTRY_LIST, {})
    }

    get get_delivery_list() {
        return this.helper.http_get_method_requester(this.helper.GET_METHOD.GET_DELIVERY_LIST, {})
    }

    get_city_list(json) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.GET_CITY_LIST, json)
    }

    fetch(id) {
        let json = {
            "store_id": this.store._id,
            "server_token": this.store.servertoken,
            "promo_id": id
        }
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.GET_PROMO_DETAIL, json)
    }

    update_promo_image(formData) {
        formData.append('store_id', this.store._id)
        formData.append('servertoken', this.store.servertoken)
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.UPDATE_PROMO_IMAGE, formData).then(() => {
            this._offerChanges.next({})
        })
    }

    check_promo(name) {
        let json = {
            "store_id": this.store._id,
            "server_token": this.store.servertoken,
            "promo_code_name": name
        }
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.CHECK_PROMO, json)
    }

    product_for_city_list(json) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.PRODUCT_FOR_CITY_STORE, json)
    }

    item_for_city_list(json) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.ITEM_FOR_CITY_STORE, json)
    }

    get_store_list_for_city(json) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.GET_STORE_LIST_FOR_CITY, json)
    }

    get_user_list(json) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.GET_USER_LIST, json)
    }

}
