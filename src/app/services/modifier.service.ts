import { Injectable } from '@angular/core';
import { Helper } from '../shared/helper';
import { AuthService, Store } from '../shared/auth.service';
import { BehaviorSubject } from 'rxjs';
import { DELIVERY_TYPE_CONSTANT } from '../shared/constant';

export interface ModifierGroupModel {
    _id: string,
    specification_group_name: any,
    sequence_number: number,
    edit_id_modifier: boolean,
    is_visible_in_store: boolean
}

export interface ModifierModel {
    _id: string,
    specification_group_id: string,
    specification_name: [{
        name: any,
        price: string,
        sequence_number: number,
        edit_id_modifier: boolean,
        is_visible_in_store: boolean
    }],
}

@Injectable({ providedIn: 'root' })
export class ModifierService {

    private store: Store;
    private _modifierChanges = new BehaviorSubject<any>(null);
    _modifierObservable = this._modifierChanges.asObservable()

    constructor(private helper: Helper, private _auth: AuthService) {
        this.store = this._auth.getStore();
    }

    list_group() {
        let json = {
            store_id: this.helper.selected_store_id ? this.helper.selected_store_id : null,
            type: 1
        }
        if (!this.helper.selected_store_id) {
            json['delivery_type'] = this.helper.DELIVERY_TYPE_CONSTANT.COURIER
        }
        if (this.helper.delivery_type == DELIVERY_TYPE_CONSTANT.ECOMMERCE) {
            json['delivery_type'] = this.helper.DELIVERY_TYPE_CONSTANT.ECOMMERCE
            json['city_id'] = this.helper.selected_city_id
        }
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.GET_SPECIFICATION_GROUP, json)
    }

    fetch(id) {
        let json = {
            store_id: this.helper.selected_store_id ? this.helper.selected_store_id : null,
            specification_group_id: id,
            type: 1
        }
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.GET_SPECIFICATION_LISTS, json)
    }

    add_group(addModifierGroup: ModifierGroupModel) {
        let json = {
            ...addModifierGroup,
            store_id: this.helper.selected_store_id ? this.helper.selected_store_id : null,
            type: 1
        }
        if (!this.helper.selected_store_id) {
            json['delivery_type'] = this.helper.DELIVERY_TYPE_CONSTANT.COURIER
        }
        if (this.helper.delivery_type == DELIVERY_TYPE_CONSTANT.ECOMMERCE) {
            json['city_id'] = this.helper.selected_city_id
        }

        return this.helper.http_post_method_requester(this.helper.POST_METHOD.ADD_SPECIFICATION_GROUP, json).then(() => {
            this._modifierChanges.next({})
        })
    }

    update_group(editModifierGroup: ModifierGroupModel) {
        let json = {
            ...editModifierGroup,
            sp_id: editModifierGroup._id,
            store_id: this.helper.selected_store_id ? this.helper.selected_store_id : null,
            type: 1
        }
        if (!this.helper.selected_store_id) {
            json['delivery_type'] = this.helper.DELIVERY_TYPE_CONSTANT.COURIER
        }
        if (this.helper.delivery_type == DELIVERY_TYPE_CONSTANT.ECOMMERCE) {
            json['city_id'] = this.helper.selected_city_id
        }

        return this.helper.http_post_method_requester(this.helper.POST_METHOD.UPDATE_SP_NAME, json).then(() => {
            this._modifierChanges.next({})
        })
    }

    add_modifier(addModifier: ModifierModel) {
        let json = {
            ...addModifier,
            store_id: this.helper.selected_store_id ? this.helper.selected_store_id : null,
            type: 1
        }
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.ADD_SPECIFICATION, json)
    }

    update_modifier(editModifier: ModifierModel) {
        let json = {
            name: editModifier.specification_name[0].name,
            sequence_number: editModifier.specification_name[0].sequence_number,
            specification_price: editModifier.specification_name[0].price,
            is_visible_in_store: editModifier.specification_name[0].is_visible_in_store,
            sp_id: editModifier._id,

            store_id: this.helper.selected_store_id ? this.helper.selected_store_id : null,

            specification_group_id: editModifier.specification_group_id,

            type: 1
        }
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.UPDATE_SPECIFICATIONS_NAME, json)

    }
}
