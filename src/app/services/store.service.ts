import { Injectable } from '@angular/core';
import { AuthService } from '../shared/auth.service';
import { Helper } from '../shared/helper';
import { BehaviorSubject } from 'rxjs';

export enum store_page_type {
    "approved", "blocked", "business_off"
}

@Injectable({
    providedIn: 'root'
})
export class StoreService {

    private _storeChanges = new BehaviorSubject<any>(null);
    _storeObservable = this._storeChanges.asObservable()

    constructor(private helper: Helper, private _auth: AuthService) { }

    async getStores(type: store_page_type, page, itemsPerPage, search_field, search_value, filterLists, delivery_type_filter) {
        let page_type;
        switch (type) {
            case store_page_type.approved:
                page_type = 1;
                break;
            case store_page_type.blocked:
                page_type = 2;
                break;
            case store_page_type.business_off:
                page_type = 3;
                break;
        }
        let json = {
            "number_of_rec": itemsPerPage,
            "page": page,
            "store_page_type": page_type,
            "search_field": search_field,
            "search_value": search_value,
            "filterLists": filterLists,
            "delivery_type_filter": delivery_type_filter,
            is_show_success_toast: false
        }
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.STORE_LIST_SEARCH_SORT, json);

    }

    getStoreDetail(data) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.GET_STORE_DETAIL, data)
    }

    getDeliveryList() {
        return this.helper.http_get_method_requester(this.helper.POST_METHOD.ALL_DELIVERY_LIST, {})
    }

    getDocumentList(data) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.VIEW_DOCUMENT_LIST, data)
    }

    updateDocumentDetail(data) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.UPLOAD_DOCUMENT, data)
    }

    getReferralUsageList(data) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.GET_REFERRAL_HISTORY, data)
    }

    getReviewList(data) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.GET_STORE_REVIEW_HISTORY, data)
    }

    async updateStoreDetail(data) {
        const res_data = await this.helper.http_post_method_requester(this.helper.POST_METHOD.UPDATE_STORE, data);
        this._storeChanges.next({})
        return res_data;
    }

    async approveDeclineStore(data) {
        const res_data = await this.helper.http_post_method_requester(this.helper.POST_METHOD.APPROVE_DECLINE_STORE, data);
        this._storeChanges.next({})
        return res_data;
    }


    getStoreListForMap() {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.STORE_LIST_FOR_MAP, {})
    }

    getStoreListForCountry(json) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.GET_STORE_LIST_FOR_COUNTRY, json)
    }
    getStoreListForAds(json) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.GET_STORE_FOR_ADS, json)
    }

    getStoreListForCity(json) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.GET_STORE_LIST_FOR_CITY, json)
    }

    deleteAccount(json) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.DELETE_STORE, json)
    }

    getDeliveryType() {
        return this.helper.http_get_method_requester(this.helper.GET_METHOD.GET_DELIVERY_TYPE, '')
    }
}
