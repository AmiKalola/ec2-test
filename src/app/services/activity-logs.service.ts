import { Injectable } from '@angular/core';
import { Helper } from '../shared/helper';

@Injectable({ providedIn: 'root' })
export class ActivityLogsService {
    constructor(private _helper: Helper){}

    get_activity_logs(json){
        return this._helper.http_post_method_requester(this._helper.POST_METHOD.GET_ACTIVITY_LOGS, json)
    }

    get_country_list(){
        return this._helper.http_get_method_requester(this._helper.GET_METHOD.GET_SERVER_COUNTRY_LIST, {})
    }

    get_city_list(json){
        return this._helper.http_post_method_requester(this._helper.POST_METHOD.GET_CITY_LIST, json)
    }

    get_change_logs(json){
        return this._helper.http_post_method_requester(this._helper.POST_METHOD.GET_CHANGE_LOG , json )
    }
}