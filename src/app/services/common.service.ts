import { Injectable } from '@angular/core';
import { Helper } from '../shared/helper';
import { AuthService, Store } from '../shared/auth.service';
import { BehaviorSubject } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class CommonService {

    private store: Store;
    private _settings;

    constructor(private helper: Helper, private _auth: AuthService) {
        this.store = this._auth.getStore();
    }

    private _providerChanges = new BehaviorSubject<any>(null);
    _providerObservable = this._providerChanges.asObservable()

    fetch_image_settings() {
        this.store = this._auth.getStore();
        let json = {
            store_id: this.store._id,
            server_token: this.store.servertoken
        }
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.GET_IMAGE_SETTING, json)
    }

    order_detail(json) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.ORDER_DETAIL, json)
    }

    last_six_months_earnings(parameters) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.LAST_SIX_MONTH_EARNING_DETAIL, parameters)
    }

    last_fifteen_day_order_detail(parameters) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.LAST_FIFTEEN_DAY_ORDER_DETAIL, parameters)
    }

    last_six_month_payment_detail(parameters) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.LAST_SIX_MONTH_PAYMENT_DETAIL, parameters)
    }

    monthly_payment_detail(parameters) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.MONTHLY_PAYMENT_DETAIL, parameters)
    }

    get_app_name() {
        return this.helper.http_get_method_requester(this.helper.GET_METHOD.GET_APP_NAME, {})
    }

    add_wallet_amount(json) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.ADD_WALLET, json).then(() => {
            this._providerChanges.next({})
        })
    }

    add_language(form_data) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.ADD_NEW_LANGUAGE, form_data)
    }

    edit_language(form_data) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.EDIT_LANGUAGE, form_data)
    }

    delete_language(form_data) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.DELETE_LANGUAGE, form_data)
    }

    getSeoTags(tabType) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.GET_SEO_TAGS, { type: tabType })
    }

    copyData(json) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.COPY_DATA, json)
    }

    addServiceCharges(json) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.ADD_SERVICE_CHARGES, json)
    }

    getServiceCharges(json) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.GET_SERVICE_CHARGES, json)
    }

    updateServiceCharges(json) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.UPDATE_SERVICE_CHARGES, json)
    }

    store_setting_details() {
        return new Promise((resolve, rejects) => {
            this.helper.http_post_method_requester(this.helper.POST_METHOD.GET_SETTING_DETAIL, {}).then((res_data) => {
                this._settings = res_data.setting;
                resolve(this._settings);
            })
        })
    }

    async _initApp(): Promise<boolean> {
        try {
            // App Initialization Start....
            const setting_detail = await this.helper.http_post_method_requester(this.helper.POST_METHOD.GET_SETTING_DETAIL, {})
            if (setting_detail.success) {
                let google_key = setting_detail?.Installation_setting?.admin_panel_google_key;
                this.helper.is_rental = setting_detail?.setting?.is_rental;
                if (google_key) {
                    this.helper.loadGoogleScript("https://maps.googleapis.com/maps/api/js?key=" + google_key + "&libraries=places,drawing")
                }
                let captcha_site_key = setting_detail?.setting?.captcha_site_key_for_web;
                if (captcha_site_key) {
                    this.helper.loadGoogleScript("https://www.google.com/recaptcha/api.js?render=" + captcha_site_key);
                }
            }
            // App Initialization End....
            return true;
        } catch (err) {
            return false;
        }
    }

    get_banner() {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.GET_BANNER_LIST , {})
    }
}
