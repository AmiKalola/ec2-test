import { Injectable } from '@angular/core';
import { Helper } from '../shared/helper';
import { BehaviorSubject } from 'rxjs';
import { DELIVERY_TYPE_CONSTANT } from '../shared/constant';

export interface CategoryModel {
    _id: string,
    name: any,
    sequence_number: number,
    product_ids: any
}


@Injectable({ providedIn: 'root' })
export class CategoryService {

    private _categoryChanges = new BehaviorSubject<any>(null);
    _categoryObservable = this._categoryChanges.asObservable()

    constructor(private helper: Helper) { }

    list(store_id) {
        let json = { "type": 1, store_id, city_id: this.helper.selected_city._id, delivery_type: this.helper.delivery_type }
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.GET_PRODUCT_GROUP_LIST, json)
    }

    fetch(id) {
        let json = { type: 1, product_group_id: id, store_id: this.helper.selected_store_id, delivery_type: this.helper.delivery_type }
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.GET_PRODUCT_GROUP_DATA, json)
    }

    add(addCategoryForm) {
        addCategoryForm.append('store_id', this.helper.selected_store_id)
        addCategoryForm.append('type', "1")
        if (this.helper.delivery_type == DELIVERY_TYPE_CONSTANT.ECOMMERCE) {
            addCategoryForm.append('city_id', this.helper.selected_city_id)
        }
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.ADD_PRODUCT_GROUP_DATA, addCategoryForm).then(() => {
            setTimeout(() => {
                this._categoryChanges.next(true)
            }, 1000)
        })
    }

    update(addCategoryForm) {
        addCategoryForm.append('store_id', this.helper.selected_store_id)
        addCategoryForm.append('type', "1")
        if (this.helper.selected_city && this.helper.selected_city._id != null) {
            addCategoryForm.append('city_id', this.helper.selected_city._id)
        }
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.UPDATE_PRODUCT_GROUP_DATA, addCategoryForm).then(() => {
            setTimeout(() => {
                this._categoryChanges.next(true)
            }, 1000)
        })
    }

    delete(id) {
        let json = {
            store_id: this.helper.selected_store_id,
            type: 1,
            product_group_id: id
        }
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.DELETE_PRODUCT_GROUP_DATA, json).then(() => {
            this._categoryChanges.next({})
        })
    }

}
