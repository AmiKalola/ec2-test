import { Injectable } from '@angular/core';
import { AuthService } from '../shared/auth.service';
import { Helper } from '../shared/helper';

export enum user_page_type {
    "approved", "blocked"
}

@Injectable({
    providedIn: 'root'
})
export class WalletService {

    constructor(private helper: Helper, private _auth: AuthService) { }

    get_wallet_requests(json) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.GET_WALLET_REQUEST_LIST_SEARCH_SORT, json)
    }

    get_wallet_history(json) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.GET_WALLET_DETAIL, json)
    }

    get_redeem_history(json) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.GET_REDEEM_DETAIL, json)
    }

    get_bank_details(json) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.GET_WALLET_REQUEST_BANK_DETAIL, json)
    }

    complete_wallet_request(json) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.COMPLETE_WALLETE_REQUEST_AMOUNT, json)
    }

    approve_wallet_request_amount(json) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.APPROVE_WALLET_REQUEST_AMOUNT, json)
    }

    cancle_wallet_request(json) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.CANCLE_WALLET_REQUEST, json)
    }

    transfer_wallet_request_amount(json) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.TRANSFER_WALLET_REQUEST_AMOUNT, json)
    }

    transaction_history(json) {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.GET_TRANSACTION_HISTORY_LIST, json)
    }

    get_country_list() {
        return this.helper.http_get_method_requester(this.helper.GET_METHOD.GET_COUNTRY_LIST, {})
    }
}
