import { Injectable } from '@angular/core';
import { Helper } from '../shared/helper';
import { AuthService } from '../shared/auth.service';

@Injectable({
    providedIn: 'root'
})

export class EmailService {

    constructor(private helper: Helper, private _auth: AuthService) { }

    getEmailList() {
        return this.helper.http_get_method_requester(this.helper.METHODS.EMAIL_LIST, {})
    }

    getEmailGatewayDetail() {
        return this.helper.http_post_method_requester(this.helper.METHODS.GET_MAIL_GATEWAY_DETAIL, {})
    }

    updateEmail(json) {
        this.helper.http_post_method_requester(this.helper.METHODS.UPDATE_EMAIL, json)
    }

    updateEmailConfiguration(json) {
        this.helper.http_post_method_requester(this.helper.METHODS.UPDATE_EMAIL_CONFIGURATION, json)
    }
}
