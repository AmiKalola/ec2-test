import { Injectable } from '@angular/core';
import { Helper } from '../shared/helper';
import { BehaviorSubject } from 'rxjs';


@Injectable({ providedIn: 'root' })
export class DocumentService {
    private _documentChanges = new BehaviorSubject<any>(null)
    _documentObservable = this._documentChanges.asObservable();

    constructor(private helper: Helper) {
    }

    getCountryServerList(){
        return this.helper.http_get_method_requester(this.helper.GET_METHOD.GET_SERVER_COUNTRY_LIST, {})
    }

    documentList(query){
        let json = {
            page: 1,
            search_field: "document_name",
            search_value: "",
            sort_document: -1,
            sort_field: "unique_id",
            query
          }
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.DOCUMENT_LIST, json)
    }

    addDocument(json){
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.ADD_DOCUMENT_DATA, json)
    }

    updateDocument(json){
        this.helper.http_post_method_requester(this.helper.POST_METHOD.UPDATE_DOCUMENT, json).then(() => {
            this._documentChanges.next({})
        })
    }

}
