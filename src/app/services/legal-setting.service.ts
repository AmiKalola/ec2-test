import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { AuthService } from '../shared/auth.service';
import { Helper } from '../shared/helper';

@Injectable({ providedIn: 'root' })
export class LegalSettingService {
    private _legalSettingChanges = new BehaviorSubject<any>(null);
    _legalSettingObservable = this._legalSettingChanges.asObservable();

    constructor(private helper: Helper, private _auth: AuthService) { }

    fetchLegalSetting() {
        return this.helper.http_post_method_requester(this.helper.POST_METHOD.GET_LEGAL, {})
    }

    updateLegalSetting(json) {
        this.helper.http_post_method_requester(this.helper.POST_METHOD.UPDATE_LEGAL, json).then(res => {
            if (res.success) {
                this._legalSettingChanges.next({})
            }
        })
    }
}
