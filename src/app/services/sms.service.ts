import { Injectable } from '@angular/core';
import { Helper } from '../shared/helper';
import { AuthService } from '../shared/auth.service';

@Injectable({
    providedIn: 'root'
})

export class SmsService {

    constructor(private helper: Helper, private _auth: AuthService) { }

    getSmsList() {
        return this.helper.http_get_method_requester(this.helper.METHODS.SMS_LIST, {})
    }

    getSmsGatewayDetail() {
        return this.helper.http_post_method_requester(this.helper.METHODS.GET_SMS_GATEWAY_DETAIL, {})
    }

    updateSms(json) {
        this.helper.http_post_method_requester(this.helper.METHODS.UPDATE_SMS, json)
    }

    updateSmsConfiguration(json) {
        this.helper.http_post_method_requester(this.helper.METHODS.UPDATE_SMS_CONFIGURATION, json)
    }
}
