import { Injectable } from '@angular/core';
import { Helper } from '../shared/helper';
import { AuthService } from '../shared/auth.service';


@Injectable({ providedIn: 'root' })
export class DispatcherService {

  admin: any;
  constructor(private helper: Helper,private _auth:AuthService) {
    this.admin = JSON.parse(localStorage.getItem('adminData'))
  }

  get_admin_dispatcher_order_list(json) {
    return this.helper.http_post_method_requester(this.helper.POST_METHOD.GET_ORDER_DISPATCHER_ORDER_LIST, json)
  }

  find_nearest_provider_list(json){
      return this.helper.http_post_method_requester(this.helper.POST_METHOD.FIND_NEAREST_PROVIDER, json)
  }

  get_provider_list(service_delivery_type,order_id,delivery_type){
    let json = {
      number_of_rec: 10,
      provider_page_type: 2,
      search_field: "first_name",
      search_value: "",
      sort_field: "unique_id",
      sort_provider: -1,
      service_delivery_type,
      delivery_type,
      order_id

    }
      return this.helper.http_post_method_requester(this.helper.POST_METHOD.PROVIDER_LIST_SEARCH_SORT, json)
  }

  create_request(json){
    return this.helper.http_post_method_requester(this.helper.POST_METHOD.CREATE_REQUEST, json)
  }

  get_store_vehicle_list(city_id, delivery_type){
    let json = {
      store_id: this.admin._id,
      server_token: this.admin.server_token,
      type: 1,
      delivery_type,
      city_id
    }
    return this.helper.http_post_method_requester(this.helper.POST_METHOD.GET_STORE_VEHICLE_LIST, json)
  }

  cancel_request(request_id,provider_id){
    let json = {
      provider_id: provider_id,
      request_id: request_id,
      type: 1
    }

    return this.helper.http_post_method_requester(this.helper.POST_METHOD.CANCEL_REQUEST, json)
  }

}
