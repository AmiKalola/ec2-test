import { Injectable } from '@angular/core';
import { Helper } from '../shared/helper';
import { AuthService } from '../shared/auth.service';
import { BehaviorSubject } from 'rxjs';


@Injectable({ providedIn: 'root' })
export class DeliveryService {
    private _deliveryChanges = new BehaviorSubject<any>(null)
    _deliveryObservable = this._deliveryChanges.asObservable();

    constructor(private helper: Helper, private _auth: AuthService) {

    }

    get_delivery_type() {
        return this.helper.http_get_method_requester(this.helper.GET_METHOD.GET_DELIVERY_TYPE, {})
    }
    update_delivery_type(form_data) {
        this.helper.http_post_method_requester(this.helper.POST_METHOD.UPDATE_DELIVERY_TYPE, form_data).then(res_data => {
            if (res_data.success) {
                this._deliveryChanges.next({})
            }
        })
    }

    get_delivery_list() {
        return this.helper.http_get_method_requester(this.helper.METHODS.ALL_DELIVERY_LIST, {})
    }

    add_delivery_data(form_data) {
        return this.helper.http_post_method_requester(this.helper.METHODS.ADD_DELIVERY_DATA, form_data).then(res_data => {
            if (res_data.success) {
                this._deliveryChanges.next({})
            }
        })
    }

    update_delivery_data(form_data) {
        this.helper.http_post_method_requester(this.helper.POST_METHOD.UPDATE_DELIVERY, form_data).then(res_data => {
            if (res_data.success) {
                this._deliveryChanges.next({})
            }
        })
    }

    get_All_store_list() {
        return this.helper.http_get_method_requester(this.helper.METHODS.GET_ALL_STORE_LIST, {})
    }
}
