import { Component, OnInit, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';
import { AddBannerModalComponent } from 'src/app/containers/pages/add-banner-modal/add-banner-modal.component';
import { Helper } from 'src/app/shared/helper';


interface Banner {
  banner_title: string;
  redirect_url: string;
  action_link: string;
  is_visible: boolean;
  action_text: string;
}

@Component({
  selector: 'app-banner',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.scss']
})

export class BannerComponent implements OnInit {


  banner_list: Array<Banner> = []
  bannerObservable : Subscription;

  @ViewChild('AddBannerModal' , {static : true}) AddBannerModal : AddBannerModalComponent;
  constructor(public _helper: Helper) { }

  ngOnInit(): void {
    this.getBannerList()
  }


  getBannerList(){
    this.bannerObservable = this._helper._bannerObservable.subscribe((res) => {
      this._helper.http_post_method_requester(this._helper.METHODS.GET_BANNER_LIST, {}).then(res_data => {
        if(!res_data.success){
          return
        }
        this.banner_list = res_data?.banners;
      })
    })
  }

  onAdd(isEdit : boolean , banner : any){
    this.AddBannerModal.show(isEdit , banner)
  }

  onDeleteBanner(banner:Banner){
    this._helper.http_post_method_requester(this._helper.METHODS.DELETE_BANNER, banner).then(res_data => {
      if(!res_data.success){
        return  this.banner_list = res_data.banners ;
      }
      this.getBannerList()
    })
  }
}
