import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AboveComponent } from './above.component';
import { AboveRoutingModule } from './above.routing';
import { AdminSettingComponent } from './admin-setting/admin-setting.component';
import { LayoutContainersModule } from 'src/app/containers/layout/layout.containers.module';
import { PagesContainersModule } from 'src/app/containers/pages/pages.containers.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { FormsModule as FormsModuleAngular, ReactiveFormsModule ,FormsModule } from '@angular/forms';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ModalModule } from 'ngx-bootstrap/modal';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { RatingModule } from 'ngx-bootstrap/rating';
import { NgSelectModule } from '@ng-select/ng-select';
import { DocumentComponent } from './document/document.component';
import { HotkeyModule } from 'angular2-hotkeys';
import { ComponentsChartModule } from 'src/app/components/charts/components.charts.module';
import { ProgressbarModule } from 'ngx-bootstrap/progressbar';
import { PipeModule } from 'src/app/pipes/pipe.module';
import { LanguageComponent } from './language/language.component';
import { DirectivesModule } from 'src/app/directives/directives.module';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { BannerComponent } from './banner/banner.component';

@NgModule({
  declarations: [AboveComponent, AdminSettingComponent, DocumentComponent, LanguageComponent, BannerComponent],
  imports: [
    CommonModule,
    AboveRoutingModule,
    FormsModule,
    DirectivesModule,
    CollapseModule,
    HotkeyModule.forRoot(),
    ComponentsChartModule,
    SharedModule,
    ProgressbarModule.forRoot(),
    LayoutContainersModule,
    CommonModule,
    PagesContainersModule,
    FormsModuleAngular,
    NgSelectModule,
    RatingModule.forRoot(),
    ReactiveFormsModule,
    TooltipModule,
    PaginationModule.forRoot(),
    TabsModule.forRoot(),
    ModalModule.forRoot(),
    BsDropdownModule.forRoot(),
    AccordionModule.forRoot(),
    PipeModule
  ]
})
export class AboveModule { }
