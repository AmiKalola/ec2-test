import { Component, OnInit, OnDestroy } from '@angular/core';
import { DOCUMENT_FOR } from 'src/app/shared/constant'
import { UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { DocumentService } from 'src/app/services/document.service';
import { Helper } from 'src/app/shared/helper';
import { Subscription } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
@Component({
  selector: 'app-document',
  templateUrl: './document.component.html',
  styleUrls: ['./document.component.scss']
})
export class DocumentComponent implements OnInit,OnDestroy {
  search_field: any =  { label: 'label-title.user-type', value: 'name' };
  country_field: any =  { label: 'label-title.country', value: 'name' };
  is_clear_disabled:boolean=true;
  isCollapsedAnimated = true;
  isCollapsedAnimated1 = false;
  documentForm: UntypedFormGroup
  editDocumentForm: UntypedFormGroup
  country_list: any = [];
  DOCUMENT_FOR = DOCUMENT_FOR
  documents_list: any = [];
  filtered_document: any = [];
  is_edit: boolean = false;
  selectedDocument: any;
  is_add: boolean = false;
  UserOptions = [
    {name: 'label-title.user', for: 7,checked:false},
    {name: 'label-title.deliveryman', for: 8,checked:false},
    {name: 'label-title.store', for: 2,checked:false},
    {name: 'label-title.provider-vehicle', for: 9,checked:false}
  ];
  countryOptions: any = [];
  selectedCountry: any = {name: '', id: ''};
  document: any;
  document_search:any ='';

  is_mandatory: boolean = false;
  is_visible: boolean = true;
  is_expiry: boolean = false;
  is_unique: boolean = false;
  query: any = {};
  document_subscription: Subscription

  constructor(private _documentService: DocumentService, public _helper: Helper, private translateService: TranslateService) {
  }

  ngOnInit(): void {
    this.documentForm = new UntypedFormGroup({
      name: new UntypedFormControl(null, Validators.required),
      country: new UntypedFormControl(null, Validators.required),
      type: new UntypedFormControl(8, Validators.required),
      is_mandatory: new UntypedFormControl(false, Validators.required),
      is_visible: new UntypedFormControl(false, Validators.required),
      is_expiry: new UntypedFormControl(false, Validators.required),
      is_unique: new UntypedFormControl(false, Validators.required)
    })

    this.editDocumentForm = new UntypedFormGroup({
      name: new UntypedFormControl(null, Validators.required),
      country: new UntypedFormControl(null, Validators.required),
      type: new UntypedFormControl(8, Validators.required),
      is_mandatory: new UntypedFormControl(false, Validators.required),
      is_visible: new UntypedFormControl(true, Validators.required),
      is_expiry: new UntypedFormControl(false, Validators.required),
      is_unique: new UntypedFormControl(false, Validators.required)
    })
    this.document_subscription =  this._documentService._documentObservable.subscribe(() => {
      this.countryOptions = []
      this.getServerCountryList()
      this.getDocumentList()
      this.is_add = false
    })
  }

  onAdd(){
    this.is_add = true
  }

  getDocumentList(){

    this._documentService.documentList(this.query).then(res_data => {
      if(res_data.success){
      this.documents_list = res_data.documents
      this.documents_list.forEach(doc => {
        doc.open = false
        doc.edit = true
      });
      this.filtered_document = this.documents_list
      } else {
        this.filtered_document = [];
      }
    })
  }

  getServerCountryList(){
    this._documentService.getCountryServerList().then(res_data => {
      let country_list = []
      let countryOptions = []
      country_list = res_data.countries
      country_list.forEach(country => {
        countryOptions.push({name: country.country_name, id: country._id})
      });

      this.country_list = country_list
      this.countryOptions = countryOptions

    })
  }

  onSearch(){
    this.query['userType'] = this.UserOptions.filter(x => x.checked)
    this.query['country'] = this.selectedCountry
    this.getDocumentList()
  }

  ngOnDestroy(): void{
    this.document_subscription.unsubscribe()
  }

  onSubmit(){

    this._helper.checkAndCleanFormValues(this.documentForm);
    if(this.documentForm.invalid){
      this.documentForm.markAllAsTouched();
      return;
    }
    let json = {
      country_id: this.documentForm.value.country,
      document_for: this.documentForm.value.type,
      document_name: this.documentForm.value.name,
      is_expired_date: this.documentForm.value.is_expiry,
      is_mandatory: this.documentForm.value.is_mandatory,
      is_visible: this.documentForm.value.is_visible,
      is_show: true,
      is_unique_code: this.documentForm.value.is_unique
    }
    if(this.documentForm.valid){
      this.is_add = false
      this._documentService.addDocument(json).then(()=>{
        this.ngOnInit()
      })
    }
  }

  onEdit(document){
    document.edit = !document.edit
    this.is_edit = true
    this.document = document
    this.selectedDocument = document._id
    if(document.is_mandatory === true ){
      document.is_visible = true;
      document.is_visible_disabled = true;
    }else{
      document.is_visible_disabled = false;
    }
    this.editDocumentForm.patchValue({
      name: document.document_name,
      country: document.country_id,
      type: document.document_for,
      is_expiry: document.is_expired_date,
      is_mandatory: document.is_mandatory,
      is_visible: document.is_visible,
      is_unique: document.is_unique_code
    })
  }
  onUpdate(){
    if(this.document.document_name.trim() === ''){
      return ;
    }
    let json = {
      country_id: this.document.country_id,
      document_for: this.document.document_for,
      document_id: this.selectedDocument,
      document_name: this.document.document_name,
      is_expired_date: this.document.is_expired_date,
      is_mandatory: this.document.is_mandatory,
      is_visible: this.document.is_visible,
      is_unique_code: this.document.is_unique_code
    }
    if(json.document_name == ''){
      return
    }
    this._documentService.updateDocument(json)
    this.is_edit = false
  }

  onVisibility(document){
    document.is_show = !document.is_show
    document.document_id = document._id
    this._documentService.updateDocument(document)
  }

  onChangeOrderBy(item){
    this.selectedCountry = item
    this.onFilter()
    this.is_clear_disabled = false;
  }

  onUserType(filter){
    filter.checked = !filter.checked
    this.onFilter()
    this.is_clear_disabled = false;
  }

  onFilter(){
    this.filtered_document = this.documents_list
    if(this.selectedCountry.id != ""){
      this.filtered_document = this.filtered_document.filter(x => x.country_id === this.selectedCountry.id)
    }
    let data: any = []
    let index = this.UserOptions.findIndex(x => x.checked === true)
    if(index >= 0){
      this.UserOptions.forEach(user => {
        if (user.checked){
          this.filtered_document.forEach(document => {
            if(document.document_for === user.for){
              data.push(document)
            }
          })
        }
      })
    } else {
      data = this.filtered_document
    }
    this.filtered_document = data
  }
  clear_filter(){
    this.UserOptions.forEach((data:any)=>{
      data.checked=false;
    })
    this.selectedCountry = {name: '', id: ''};
    this.document_search='';
    this.onSearch()
    this.is_clear_disabled = true;
  }

  onChangeAddDocumentMandatory(){
    if(this.documentForm.value.is_mandatory){
      this.documentForm.patchValue({
        is_visible : true
      })
      this.documentForm.get('is_visible').disable();
    }else{
      this.documentForm.get('is_visible').enable();
    }
  }

  onChangeEditDocumentMandatory(document){
      if(document.is_mandatory === true ){
        document.is_visible = true;
        document.is_visible_disabled = true;
      }else{
        document.is_visible_disabled = false;
      }
  }

}
