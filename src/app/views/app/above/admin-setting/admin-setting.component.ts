import { Component, OnInit, ViewChild } from '@angular/core';
import { AdminSettingService } from '../../../../services/admin-setting.service';
import { UntypedFormGroup, UntypedFormControl, Validators } from '@angular/forms';
import { GoogleApiKeyModalComponent } from '../../../../containers/pages/google-api-key-modal/google-api-key-modal.component';
import { EditAdminDetailsModalComponent } from '../../../../containers/pages/edit-admin-details-modal/edit-admin-details-modal.component';
import { Helper } from 'src/app/shared/helper';
import { environment } from 'src/environments/environment';
export const admin_logo = {
  logo: 1,
  title_icon: 2,
  user_logo: 3,
  store_logo: 4,
  mail_background_logo: 5,
  mail_title_banner: 6,
  delivery_logo_image:7,
  service_logo_image:8,
  taxi_logo_image:9,
  courier_logo_image:10,
  appoinment_logo_image:11,
  ecommerce_logo_image:12,
  dark_logo: 13,
  dark_title_icon: 14,
  dark_user_logo: 15,
  dark_store_logo: 16,
  dark_mail_background_logo: 17,
  dark_mail_title_banner: 18,
}

export const admin_logo_name = {
  1:'logo_image',
  2:'title_image',
  3:'user_logo_image',
  4:'store_logo_image',
  5:'mail_logo_image',
  6:'mail_title_image',
  7:'delivery_logo_image',
  8:'service_logo_image',
  9:'taxi_logo_image',
  10:'courier_logo_image',
  11:'appoinment_logo_image',
  12:'ecommerce_logo_image',
  13:'dark_logo_image',
  14:'dark_title_image',
  15:'dark_user_logo_image',
  16:'dark_store_logo_image',
  17:'dark_mail_logo_image',
  18:'dark_mail_title_image',
  }
import { AddAppPushKeyDetalisComponent } from 'src/app/containers/pages/add-app-push-key-detalis/add-app-push-key-detalis.component';
import { AddAppCertificateSettingComponent } from 'src/app/containers/pages/add-app-certificate-setting/add-app-certificate-setting.component';
import { NotifiyService } from 'src/app/services/notifier.service';
import { GoogleCaptchaSettingModalComponent } from 'src/app/containers/pages/google-captcha-setting-modal/google-captcha-setting-modal.component';
import { SETTING_TYPE } from 'src/app/shared/constant';

@Component({
  selector: 'app-admin-setting',
  templateUrl: './admin-setting.component.html',
  styleUrls: ['./admin-setting.component.scss']
})
export class AdminSettingComponent implements OnInit {
  manualTrigger = false;
  admin_logo = admin_logo
  admin_logo_name = admin_logo_name

  user_app_settings: UntypedFormGroup;
  provider_app_settings: UntypedFormGroup;
  store_app_settings: UntypedFormGroup;

  is_edit_user_app_settings = false;
  is_use_captcha: boolean = false;
  is_allow_to_cancel_request:boolean = false;
  is_allow_twilio_calling: boolean = false
  is_edit_provider_app_settings = false;
  is_edit_store_app_settings = false;

  app_update_settings: UntypedFormGroup;
  ios_app_update_settings: UntypedFormGroup;
  is_edit_app_update_settings = false;
  is_edit_ios_update_settings = false;

  key_settings: any;

  other_settings: UntypedFormGroup;
  is_edit_other_settings = false;

  admin_details:any

  is_edit_logo_settings = false;
  is_edit_dark_logo_settings = false;
  is_edit_business_logo_settings = false;
  public formData :FormData;
  upload_images = {
    logo_image: '',
    title_image: '',
    user_logo_image: '',
    store_logo_image: '',
    mail_logo_image: '',
    mail_title_image: '',
    delivery_logo_image:'',
    service_logo_image:'',
    taxi_logo_image:'',
    courier_logo_image:'',
    appoinment_logo_image:'',
    ecommerce_logo_image:'',
    dark_logo_image:"",
    dark_title_image:"",
    dark_user_logo_image:"",
    dark_store_logo_image:"",
    dark_mail_logo_image:"",
    dark_mail_title_image:"",
  }
  IMAGE_URL = environment.imageUrl;
  SETTING_TYPE = SETTING_TYPE ;

  @ViewChild('googleApiKeyModal', { static: true }) googleApiKeyModal: GoogleApiKeyModalComponent;
  @ViewChild('editAdminDetailsModal', { static: true }) editAdminDetailsModal: EditAdminDetailsModalComponent;
  @ViewChild('AppPushKeyDetalisModal', { static: true }) AppPushKeyDetalisModal: AddAppPushKeyDetalisComponent;
  @ViewChild('AppCertificateSettingsModal', { static: true }) AppCertificateSettingsModal: AddAppCertificateSettingComponent;
  @ViewChild('googleCaptchaSettingModal', { static: true }) googleCaptchaSettingModal: GoogleCaptchaSettingModalComponent;

  constructor(private _adminSettingService: AdminSettingService,public _helper: Helper,
    private _notifierService: NotifiyService
    ) { }

  ngOnInit(): void {
    this._initForm()
    this._disableForm()
    this._adminSettingService._settingObservable.subscribe(() => {
      this.getAdminSetting()
    })
    this.formData = new FormData()
  }

  _initForm() {
    this.user_app_settings = new UntypedFormGroup({
      is_user_login_by_email: new UntypedFormControl(null, Validators.required),
      is_user_login_by_phone: new UntypedFormControl(null, Validators.required),
      is_user_login_by_social: new UntypedFormControl(null, Validators.required),
      is_referral_to_user: new UntypedFormControl(null, Validators.required),
      is_user_mail_verification: new UntypedFormControl(null, Validators.required),
      is_user_sms_verification: new UntypedFormControl(null, Validators.required),
      is_upload_user_documents: new UntypedFormControl(null, Validators.required),
      is_user_profile_picture_required: new UntypedFormControl(null, Validators.required),
      is_show_optional_field_in_user_register: new UntypedFormControl(null, Validators.required)
    })

    this.provider_app_settings = new UntypedFormGroup({
      is_provider_login_by_email: new UntypedFormControl(null, Validators.required),
      is_provider_login_by_phone: new UntypedFormControl(null, Validators.required),
      is_provider_login_by_social: new UntypedFormControl(null, Validators.required),
      is_referral_to_provider: new UntypedFormControl(null, Validators.required),
      is_provider_sms_verification: new UntypedFormControl(null, Validators.required),
      is_provider_mail_verification: new UntypedFormControl(null, Validators.required),
      is_upload_provider_documents: new UntypedFormControl(null, Validators.required),
      is_provider_profile_picture_required: new UntypedFormControl(null, Validators.required),
      is_show_optional_field_in_provider_register: new UntypedFormControl(null, Validators.required),
      is_provider_earning_add_in_wallet_on_cash_payment: new UntypedFormControl(null, Validators.required),
      is_provider_earning_add_in_wallet_on_other_payment: new UntypedFormControl(null, Validators.required),
    })

    this.store_app_settings = new UntypedFormGroup({
      is_store_login_by_email: new UntypedFormControl(null, Validators.required),
      is_store_login_by_phone: new UntypedFormControl(null, Validators.required),
      is_store_login_by_social: new UntypedFormControl(null, Validators.required),
      is_referral_to_store: new UntypedFormControl(null, Validators.required),
      is_store_mail_verification: new UntypedFormControl(null, Validators.required),
      is_store_sms_verification: new UntypedFormControl(null, Validators.required),
      is_upload_store_documents: new UntypedFormControl(null, Validators.required),
      is_store_profile_picture_required: new UntypedFormControl(null, Validators.required),
      is_show_optional_field_in_store_register: new UntypedFormControl(null, Validators.required),
      is_store_earning_add_in_wallet_on_cash_payment: new UntypedFormControl(null, Validators.required),
      is_store_earning_add_in_wallet_on_other_payment: new UntypedFormControl(null, Validators.required),
    })

    this.app_update_settings = new UntypedFormGroup({
      android_provider_app_version_code: new UntypedFormControl(null, Validators.required),
      android_store_app_version_code: new UntypedFormControl(null, Validators.required),
      android_user_app_version_code: new UntypedFormControl(null, Validators.required),
      // ios_provider_app_version_code: new UntypedFormControl(null, Validators.required),
      // ios_store_app_version_code: new UntypedFormControl(null, Validators.required),
      // ios_user_app_version_code: new UntypedFormControl(null, Validators.required),
      is_android_provider_app_force_update: new UntypedFormControl(null, Validators.required),
      is_android_provider_app_open_update_dialog: new UntypedFormControl(null, Validators.required),
      is_android_store_app_force_update: new UntypedFormControl(null, Validators.required),
      is_android_store_app_open_update_dialog: new UntypedFormControl(null, Validators.required),
      is_android_user_app_force_update: new UntypedFormControl(null, Validators.required),
      is_android_user_app_open_update_dialog: new UntypedFormControl(null, Validators.required)
    })

    this.ios_app_update_settings = new UntypedFormGroup({
      ios_provider_app_version_code: new UntypedFormControl(null, Validators.required),
      ios_store_app_version_code: new UntypedFormControl(null, Validators.required),
      ios_user_app_version_code: new UntypedFormControl(null, Validators.required),
      is_ios_provider_app_force_update: new UntypedFormControl(null, Validators.required),
      is_ios_provider_app_open_update_dialog: new UntypedFormControl(null, Validators.required),
      is_ios_store_app_force_update: new UntypedFormControl(null, Validators.required),
      is_ios_store_app_open_update_dialog: new UntypedFormControl(null, Validators.required),
      is_ios_user_app_force_update: new UntypedFormControl(null, Validators.required),
      is_ios_user_app_open_update_dialog: new UntypedFormControl(null, Validators.required)
    })

    this.other_settings = new UntypedFormGroup({
      is_sms_notification: new UntypedFormControl(null, Validators.required),
      is_mail_notification: new UntypedFormControl(null, Validators.required),
      is_push_notification: new UntypedFormControl(null, Validators.required),
      is_confirmation_code_required_at_pickup_delivery: new UntypedFormControl(null, Validators.required),
      is_confirmation_code_required_at_complete_delivery: new UntypedFormControl(null, Validators.required),
      is_allow_contactless_delivery: new UntypedFormControl(null, Validators.required),
      is_allow_pickup_order_verification: new UntypedFormControl(null, Validators.required),
      is_allow_user_to_give_tip: new UntypedFormControl(null, Validators.required),
      tip_type: new UntypedFormControl(null, Validators.required),
      is_use_captcha: new UntypedFormControl(false, Validators.required),
      is_banner_visible: new UntypedFormControl(false, Validators.required),
      is_allow_bring_change_option: new UntypedFormControl(false, Validators.required),
      is_allow_to_cancel_request:new UntypedFormControl(false , Validators.required),
      is_enable_twilio_call_masking: new UntypedFormControl(false, Validators.required),
      max_courier_stop_limit: new UntypedFormControl(3, Validators.required),
      max_taxi_stop_limit: new UntypedFormControl(3, Validators.required),
      find_nearest_driver_type: new UntypedFormControl(1, [Validators.required]),
      request_send_to_no_of_providers: new UntypedFormControl(null, [Validators.required]),
      courier_booking_timeslot: new UntypedFormControl(30, Validators.required),
      taxi_booking_timeslot: new UntypedFormControl(30, Validators.required),
    })
  }

  _disableForm() {
    this.user_app_settings.disable()
    this.provider_app_settings.disable()
    this.store_app_settings.disable()
    this.app_update_settings.disable()
    this.ios_app_update_settings.disable()
    this.other_settings.disable()
  }

  getAdminSetting() {
    let notification = false
    this._adminSettingService.fetchAdminSetting(notification).then(res => {
      if (res.success) {
        this.user_app_settings.patchValue({ ...res.setting })
        this.provider_app_settings.patchValue({ ...res.setting })
        this.store_app_settings.patchValue({ ...res.setting })
        this.other_settings.patchValue({ ...res.setting })
        this.admin_details = res.setting
        this._helper.timeZone.next(res.setting.admin_panel_timezone);
      }
    })
    this._adminSettingService.fetchInstallationSetting().then(res => {
      if (res.success) {
        this.app_update_settings.patchValue({ ...res.app_keys });
        this.ios_app_update_settings.patchValue({...res.app_keys})
        this.key_settings = res.app_keys
      }
    })
  }

  onClickUserAppSetting() {
    this.is_edit_user_app_settings = !this.is_edit_user_app_settings
    if (this.is_edit_user_app_settings) {
      this.user_app_settings.enable()
    } else {
      this.user_app_settings.disable()
      this._adminSettingService.updateAdminSetting(this.user_app_settings.value)
    }
  }

  onClickProviderAppSetting() {
    this.is_edit_provider_app_settings = !this.is_edit_provider_app_settings
    if (this.is_edit_provider_app_settings) {
      this.provider_app_settings.enable()
    } else {
      this.provider_app_settings.disable()
      this._adminSettingService.updateAdminSetting(this.provider_app_settings.value)
    }
  }

  onClickStoreAppSetting() {
    this.is_edit_store_app_settings = !this.is_edit_store_app_settings
    if (this.is_edit_store_app_settings) {
      this.store_app_settings.enable()
    } else {
      this.store_app_settings.disable()
      this._adminSettingService.updateAdminSetting(this.store_app_settings.value)
    }
  }

  validateLoginByOption(option) {
    if (this.user_app_settings.value.is_user_login_by_phone === false && this.user_app_settings.value.is_user_login_by_email === false) {
      if (option == 'phone') {
        this.user_app_settings.patchValue({ is_user_login_by_email: true })
      } else {
        this.user_app_settings.patchValue({ is_user_login_by_phone: true })
      }
    }
    if (this.provider_app_settings.value.is_provider_login_by_phone === false && this.provider_app_settings.value.is_provider_login_by_email === false) {
      if (option == 'phone') {
        this.provider_app_settings.patchValue({ is_provider_login_by_email: true })
      } else {
        this.provider_app_settings.patchValue({ is_provider_login_by_phone: true })
      }
    }
    if (this.store_app_settings.value.is_store_login_by_phone === false && this.store_app_settings.value.is_store_login_by_email === false) {
      if (option == 'phone') {
        this.store_app_settings.patchValue({ is_store_login_by_email: true })
      } else {
        this.store_app_settings.patchValue({ is_store_login_by_phone: true })
      }
    }
  }

  onClickAppUpdateSetting() {
    this.is_edit_app_update_settings = !this.is_edit_app_update_settings
    if (this.is_edit_app_update_settings) {
      this.app_update_settings.enable()
    } else {
      this.app_update_settings.disable()
      this._adminSettingService.updateInstallationSetting(this.app_update_settings.value)
    }
  }

  onClickIosAppUpdateSetting() {
    this.is_edit_ios_update_settings = !this.is_edit_ios_update_settings
    if (this.is_edit_ios_update_settings) {
      this.ios_app_update_settings.enable()
    } else {
      this.ios_app_update_settings.disable()
      this._adminSettingService.updateInstallationSetting(this.ios_app_update_settings.value)
    }
  }

  driverType() {
    if(this.other_settings.value.find_nearest_driver_type == 1){
      this.other_settings.get('request_send_to_no_of_providers').setValidators([Validators.min(1), Validators.required])
      this.other_settings.patchValue({request_send_to_no_of_providers:1});
    }
    if (this.other_settings.value.find_nearest_driver_type == 2) {
      this.other_settings.get('request_send_to_no_of_providers').setValidators([Validators.min(2), Validators.required])
      this.other_settings.patchValue({request_send_to_no_of_providers:null});
    }
  }

  onClickOtherSetting() {
    if (this.other_settings.value.find_nearest_driver_type == 2 && this.other_settings.value.request_send_to_no_of_providers == 1) {
      this.other_settings.get('request_send_to_no_of_providers').setValidators([Validators.min(2), Validators.required])
      this.other_settings.get('request_send_to_no_of_providers').updateValueAndValidity();
      return;
    }
    this.is_edit_other_settings = !this.is_edit_other_settings
    if (this.is_edit_other_settings) {
      this.other_settings.enable()
    } else {
      this.other_settings.disable()
      this._adminSettingService.updateAdminSetting(this.other_settings.value)
    }
  }

  onClickLogoSetting() {
    this.is_edit_logo_settings = !this.is_edit_logo_settings
    if (this.is_edit_logo_settings) {
      this.formData = new FormData();
    } else {
      this._adminSettingService.updateAdminLogoSetting(this.formData)
    }
  }

  onClickDarkLogoSetting(): void {
    this.is_edit_dark_logo_settings = !this.is_edit_dark_logo_settings;
    if (this.is_edit_dark_logo_settings) {
      this.formData = new FormData();
    } else {
      this._adminSettingService.updateAdminLogoSetting(this.formData);
    }
  }

  onClickBusinessLogoSetting() {
    this.is_edit_business_logo_settings = !this.is_edit_business_logo_settings
    if (this.is_edit_business_logo_settings) {
      this.formData = new FormData();
    } else {
      this._adminSettingService.updateAdminLogoSetting(this.formData)
    }
  }

  onEditGoogleApiKey(type) {
    this.googleApiKeyModal.show(this.key_settings , type)
  }

  onEditAdminDetails() {
    this.editAdminDetailsModal.show(this.admin_details)
  }

  onSelectImageFile(event,type){
    let files = event.target.files;
    if (files.length === 0)
      return;

    const mimeType = files[0].type;
    let fileType=this._helper.uploadFile.filter((element)=> {
      return mimeType==element;
    })
    if (mimeType != fileType) {
      this._notifierService.showNotification('error',this._helper._trans.instant('validation-title.invalid-image-format'));
      return;
    }
    const logo_image = files[0];
    this.formData.append(admin_logo_name[type], logo_image);

    const reader = new FileReader();
    reader.readAsDataURL(files[0]);
    reader.onload = (_event) => {
      this.upload_images[admin_logo_name[type]] = reader.result
    }
  }

  openAppPushKeydetailsModal(){
    this.AppPushKeyDetalisModal.show(this.key_settings);
  }
  openAppCertificateSettingsModal(){
    this.AppCertificateSettingsModal.show(this.key_settings);
  }

  openGoogleCaptchaModal(){
    this.googleCaptchaSettingModal.show(this.admin_details)
  }

}
