import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { AddNewLanguageComponent } from 'src/app/containers/pages/add-language-modal/add-language-modal.component';
import { Helper } from 'src/app/shared/helper';
import { saveAs } from 'file-saver';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { CommonService } from 'src/app/services/common.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-language',
  templateUrl: './language.component.html',
})
export class LanguageComponent implements OnInit {

  constructor(private modalService: BsModalService,public _helper: Helper,private http: HttpClient,private _commonService: CommonService) { }
  confirmModelRef: BsModalRef;
  confirmationModalConfig = {
    backdrop: true,
    ignoreBackdropClick: true,
  };
  languages: any = []
  langForm: UntypedFormGroup
  admin_url: any;
  IMAGE_URL = environment.imageUrl
  selected_language: any = '';

  @ViewChild('addNewModalRef', { static: true }) addNewModalRef: AddNewLanguageComponent;
  @ViewChild('confirmationTemplate', { static: true }) confirmationTemplate: TemplateRef<any>;

  ngOnInit(): void {
    this.get_languages()
    this.langForm = new UntypedFormGroup({
      store_url: new UntypedFormControl(null, Validators.required),
      user_website: new UntypedFormControl(null, Validators.required)
    })
  }

  get_languages() {
    this._helper.http_get_method_requester(this._helper.GET_METHOD.GET_LANGUAGES, {}).then(res_data => {
      this.languages = res_data.lang
    })
  }

  onLangSelected(language) {
    this.selected_language = language
  }

  download_language_file(type) {
    let base_url = ""
    switch (type) {
      case 'admin':
        base_url =  this.IMAGE_URL+'/language/admin_panel/';
        break;
      case 'store':
        base_url =  this.IMAGE_URL+'/language/store_panel/';
        break;
      case 'user':
        base_url = this.IMAGE_URL+'/language/user_panel/';
        break;
      default:
        break;
    }

    if (base_url !== "") {
      let url = base_url  + this.selected_language.code + '.json'
      this._helper.http_get_method_requester_raw(url, {}).then(res_data => {
        if (res_data.success !== false) {
          let data = res_data;
          this.downloadJSON(JSON.stringify(data, null, 2), "application/json")
        }
      })
    }
  }

  showEditModal(language): void {
    this.addNewModalRef.edit(language);
  }

  showAddNewModal(): void {
    this.addNewModalRef.show();
  }

  async downloadJSON(data, fileName) {
    let blob = new Blob([data], { type: '2' });
    saveAs(blob, fileName+'.json')
  }
  downLoadLang(lang){
    if (lang.code) {
      let code = lang.code 
      const data_2 = `${this.IMAGE_URL}language/${code}.json`
      this.http.get(data_2).subscribe((res:any) => {
        this.downloadJSON(JSON.stringify(res, null, 2), code)
      })
    }
  }

  openDeleteLangModal(lang){
    this.selected_language=lang;
    this.confirmModelRef = this.modalService.show(this.confirmationTemplate, this.confirmationModalConfig)
  }
  
  deleteLang(lang){
    let json:any ={ code : lang.code}
    this._commonService.delete_language(json).then(res => {
      if(res.success){
        this.confirmModelRef.hide();
        this.get_languages()
      }
    })
  }
  closeModal(){
    this.get_languages()
  }

}
