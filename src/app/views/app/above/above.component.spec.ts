import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AboveComponent } from './above.component';

describe('AboveComponent', () => {
  let component: AboveComponent;
  let fixture: ComponentFixture<AboveComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AboveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AboveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
