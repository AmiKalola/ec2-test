import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SubStoresComponent } from './sub-stores.component';
import { StoreListComponent } from './store-list/store-list.component';

const routes: Routes = [
  {
    path: '', component: SubStoresComponent,
    children: [
      { path: '', redirectTo: 'list', pathMatch: 'full' },
      { path: 'storeList', component: StoreListComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SubStoresRoutingModule { }
