import { Component, EventEmitter, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { AddNewStoreInfoModalComponent } from 'src/app/containers/pages/add-new-store-info-modal/add-new-store-info-modal.component';
import { StoreService, store_page_type } from '../../../../services/store.service'
import { Helper } from 'src/app/shared/helper';
import { Subscription } from 'rxjs';
import { LangService } from 'src/app/shared/lang.service';
import { Lightbox } from 'ngx-lightbox';
import { CommonService } from 'src/app/services/common.service';
import { USERS_PER_PAGE_LIST } from '../../../../shared/constant'

@Component({
  selector: 'app-store-list',
  templateUrl: './store-list.component.html',
  styleUrls: ['./store-list.component.scss']
})
export class StoreListComponent implements OnInit, OnDestroy {
  album: Array<any>;

  is_clear_disabled:boolean = true;
  search_field: any =  { label: 'label-title.name', value: 'name' };
  search_value: string = '';
  delivery_type_filter: any[] = [];
  deliveries: any[] = [];
  itemOptionsPerPage = USERS_PER_PAGE_LIST
  itemsPerPage = 15;

  itemOptionsOrders = [
    { label: 'label-title.name', value: 'name' , isShow : true},
    { label: 'label-title.phone', value: 'phone' , isShow : true },
    { label: 'label-title.email', value: 'email' , isShow : true},
    { label: 'label-title.country', value: 'country_details.country_name' , isShow : true },
    { label: 'label-title.city', value: 'city_details.city_name' , isShow : true },
    { label: 'label-title.id', value: 'unique_id', isShow : true },
   ];

   filterLists = [
     {lable:'label-title.wallet-negative',value:'is_wallet_negative', checked: false},
     {lable:'label-title.document-expired',value:'is_document_expired', checked: false},
     {lable:'label-title.document-uploaded',value:'is_document_uploaded', checked: false},
     {lable:'label-title.email-verified',value:'is_email_verified', checked: false},
     {lable:'label-title.phone-verified',value:'is_phone_verified', checked: false}
   ]

  stores: any = []
  is_approved = true
  store: any
  store_page_type: any = store_page_type;
  STORE_DEFAULT_IMAGE = this._helper.DEFAULT_IMAGE_PATH.USER;

  count: number = 0;
  currentPage = 1;

  status: any = store_page_type.approved;
  storeObservable: Subscription;
  walletObservable: Subscription

  manualTrigger=false;

  deliveryTypeLists = [
    {lable:'label-title.food-delivery',value:'is_food_delivery', checked: false},
    {lable:'label-title.flower',value:'is_flower', checked: false},
    {lable:'label-title.supermarket',value:'is_supermarket', checked: false},
    {lable:'label-title.grocy',value:'is_grocy', checked: false},
    {lable:'label-title.all',value:'is_all', checked: false}
  ]

  export_data_list: any[] = [
    {label: 'label-title.basic-details', value: ''},
    {label: 'label-title.name', value: 'name', is_checked: false},
    {label: 'label-title.email', value: 'email', is_checked: false},
    {label: 'label-title.phone', value: 'phone', is_checked: false},
    {label: 'label-title.basic-details', value: ''},
    {label: 'label-title.wallet', value: 'wallet', is_checked: false},
    {label: 'label-title.address', value: 'address', is_checked: false},
    {label: 'label-title.website-url', value: 'website_url', is_checked: false},
    {label: 'label-title.phone-verified', value: 'is_phone_verified', is_checked: false},
    {label: 'label-title.email-verified', value: 'is_email_verified', is_checked: false}
  ]
  is_all_selected: boolean = false;

  viewType: string = 'grid';


  itemOrder = { label: 'label-title.name', value: 'name' };
  showSearch = true;
  @ViewChild('storeInfoModal', { static: true }) storeInfoModal: AddNewStoreInfoModalComponent;
  changeOrderBy: EventEmitter<any> = new EventEmitter();

  constructor(public storeService: StoreService,
    public _helper: Helper,
    public _lang: LangService,
    public lightBox: Lightbox,
    private _commonService: CommonService) { }

  ngOnInit(): void {

    this.getStoreList(store_page_type.approved)
    this.storeService.getDeliveryList().then(res_data => {
      this.deliveries = res_data.deliveries || []; 
      this.deliveries = this.deliveries.filter(d=>d.delivery_name.length > 0)
    });
    this.storeObservable = this.storeService._storeObservable.subscribe((data) => {
      if(data){
        this.getStoreList(this.status)
      }
    })
    this.walletObservable = this._commonService._providerObservable.subscribe((data) => {
      if(data){
      this.getStoreList(this.status)
      }
    })
  }
  changeStoreType(status) {
    this.currentPage = 1;
    this.status = status;
    this.getStoreList(status)
  }

  onViewChange(type) {
    this.viewType = type
  }

  onChangedeliverytypefilter(delivery){
    this.is_clear_disabled = false;
    delivery.checked = !delivery.checked;
    let index = this.delivery_type_filter.findIndex((x)=>x.toString() == delivery._id.toString());
    if(index ==  -1){
      this.delivery_type_filter.push(delivery._id.toString());
    } else {
      this.delivery_type_filter.splice(index, 1);
    }
  }

  clear_filter(){
    this.search_field =  { label: 'label-title.name', value: 'name' };
    this.search_value = '';
    this.delivery_type_filter = [];
    this.filterLists.forEach((data:any)=>{
      data.checked=false;
    })
    this.deliveries.forEach((data)=>data.checked=false);
    this.getStoreList(this.status)
    this.is_clear_disabled = true;
  }

  onChangeexportfilter(data){
    data.is_checked = !data.is_checked;
  }

  select_all_export_list(){
    this.is_all_selected = ! this.is_all_selected;
    this.export_data_list.forEach((data)=>{
      if(data.value){
        data.is_checked = this.is_all_selected;
      }
    })
  }

  export_excel_data(){
    let export_data_list = this.export_data_list.filter((x)=> {
      return x.is_checked
    });
    let filterLists = this.filterLists.filter((x)=>x.checked)
    this.storeService.getStores(this.status, 0, 0, this.search_field.value, this.search_value, filterLists, this.delivery_type_filter).then(res_data => {
      res_data.stores.forEach(store => {
        store.is_phone_verified = store.is_phone_number_verified
      });
      this._helper.downloadcsvFile(res_data.stores || [], export_data_list)
    });
  }

  getStoreList(status) {
    let filterLists = this.filterLists.filter((x)=>x.checked)
    this.storeService.getStores(status, this.currentPage, this.itemsPerPage, this.search_field.value, this.search_value, filterLists, this.delivery_type_filter).then(res_data => {
      if(res_data){
        this.itemOptionsOrders.forEach((data) => {
          if(data.value == 'email' && res_data.is_show_email === false){
            data.isShow = false ;
          };
          if(data.value == 'phone' && res_data.is_show_phone === false){
            data.isShow = false ;
          }
        })
      }
      if (res_data.success) {
        this.stores = res_data.stores;
        this.count = res_data.count;
      } else {
        this.stores = [];
        this.count = 0;
      }
    })
  }

  approveDecline(store_id, type){
    let page_type;
    switch(type){
      case store_page_type.approved:
        page_type = 1;
        break;
      case store_page_type.blocked:
        page_type = 2;
        break;
      case store_page_type.business_off:
        page_type = 3;
        break;
    }
    let data = {
      "store_page_type": page_type,
      store_id: store_id
    }
    this.storeService.approveDeclineStore(data).then(res_data => {
      this.getStoreList(this.status)
    })
  }
  pageChanged(event) {
    this.currentPage = event.page;
    this.getStoreList(this.status)
  }

  showAddNewModal(event, store) {
    if(event.target.tagName.toLowerCase() !== 'button'){
      this.storeInfoModal.show(store);
    }
  }

  onChangeOrderBy(item): void  {
    this.search_field = item;
    this.is_clear_disabled = false;
  }

  onChangefilter(filter){
    filter.checked = !filter.checked;
    this.is_clear_disabled = false;
  }

  onChangeItemsPerPage(pages){
    this.itemsPerPage = pages
    this.getStoreList(this.status)
  }

  onLightBox(user){
    if(user.image_url != ''){
      let src = this._helper.image_url + user.image_url
      this.lightBox.open([{ src, thumb: '', downloadUrl: '' }], 0, { centerVertically: true, positionFromTop: 0, disableScrolling: true, wrapAround: true });
    }
  }

  ngOnDestroy(){
    this.walletObservable.unsubscribe()
    this.storeObservable.unsubscribe()

    if (this.storeInfoModal.modalRef){
      this.storeInfoModal.onClose();
    }
  }

  updateData()
  {
    this.getStoreList(this.status) 
  }
  apply(status){
    this.currentPage=1;
    this.getStoreList(status) 
  }
}
