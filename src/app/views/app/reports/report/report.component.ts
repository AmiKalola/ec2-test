import { Component, OnInit } from '@angular/core';
import { CityService } from 'src/app/services/city.service';
import { DeliveryFeesService } from 'src/app/services/delivery-fees.service';
import { OrderService } from 'src/app/services/order.service';
import { PaymentGateWayService } from 'src/app/services/payment-gateway.service';
import { PER_PAGE_LIST } from 'src/app/shared/constant';
import { Helper } from 'src/app/shared/helper';

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.scss']
})
export class ReportComponent implements OnInit {
  todayDate: Date = new Date();
  direction = localStorage.getItem('direction');
  itemSearch = { label: 'label-title.user_name', value: 'user_detail.first_name' };
  itemPayment = { label: 'label-title.both', value: '2' };
  paymentBy = [
    {
      id: 'cash',
      checked: false,
      name: 'label-title.cash'
    }
  ];
  itemOptionsOrders = [
    { label: 'label-title.all', value: '' },
    { label: 'label-title.completed', value: 10 },
    { label: 'label-title.rejected', value: 3 },
    { label: 'label-title.cancelled', value: 2 }];
  itemOrder = { label: 'label-title.all', value: '' };
  itemOptionsPerPage = PER_PAGE_LIST
  country_list: any = [];
  city_list: any = [];
  selectedCountryId: string;
  selectedCityId: string = "all";
  selectedProviderId: string;
  selectedDeliveryType: string;
  selectedBusinessId: string;
  rows = [];
  provider_list: any = [];
  deliveries_type_list: any = [];
  business_type_list: any = [];
  city_business_list: any = [];
  servicetype_list: any = [];
  search_value: string = '';
  itemsPerPage = 20;
  current_page: number = 1;
  total_page: number;
  start_date: string = '';
  end_date: string = '';
  is_clear_disabled: boolean = true;
  is_delivery_business: boolean = false;
  item_bsRangeValue;
  created_date: Date;
  timezone_for_display_date: string = '';

  constructor(public _helper: Helper,
    private _cityService: CityService,
    private _orderService: OrderService,
    private _deliverFeesServcise: DeliveryFeesService,
    private paymentGatewayService: PaymentGateWayService,
  ) { }

  ngOnInit(): void {
    this.getCountryList();
    this.getPaymentGatewayList();
    this.deliveries_type_list = this._helper.DELIVERY_TYPE
    this._helper.created_date.subscribe(data => {
      if (data) {
        let date = new Date(data)
        this.created_date = date;
      }
    })
    this._helper.display_date_timezone.subscribe(data => {
      this.timezone_for_display_date = data;
    })

  }

  getCountryList() {
    this._deliverFeesServcise.get_server_country_list().then(res => {

      if (res.success) {
        this.country_list = res.countries;
        this.selectedCountryId = this.country_list[0]?._id;
        this.business_type_list = res.delivery
        this.clearSelection()
        if (this.selectedCountryId) {
          this.getCityList(this.selectedCountryId);
        }
      } else {
        this.selectedCountryId = null;
        this.country_list = [];
        this.clearSelection()
      }
    })
  }

  get_driver_business_lists() {
    if (this.selectedCityId) {
      let json: any = { country_id: this.selectedCountryId, city_id: this.selectedCityId };
      this._orderService.get_details_country_city_wise_list(json).then(res => {
        if (res.providers.length > 0) {
          this.provider_list = res.providers;
        } else {
          this.provider_list = [];
        }
      })
    } else {
      this.provider_list = [];
    }
  }

  getCountry(countryId) {
    this.selectedCountryId = countryId;
    this.getCityList(countryId);
    this.clearSelection()
  }

  // get city from country
  getCityList(country_Id) {
    this._cityService.getDestinationCityList({ country_id: country_Id }).then(res => {
      if (res.success) {
        this.city_list = res.destination_list;
        if (this.selectedCityId) {
          this.get_driver_business_lists();
        }
      } else {
        this.selectedCityId = null;
        this.city_list = [];

      }
    })
    this.clearSelection()
  }

  // get city to input field
  getCity(cityId) {
    this.selectedCityId = cityId;
    this.get_driver_business_lists();
    this.is_clear_disabled = false;
    this.is_delivery_business = false;
    this.clearSelection()
  }

  getProvider(providerId) {
    this.selectedProviderId = providerId;
    this.is_clear_disabled = false;
  }

  getDelivery(deliveryType) {
    this.selectedDeliveryType = deliveryType
    if (deliveryType == this._helper.DELIVERY_TYPE_CONSTANT.STORE ||
      deliveryType == this._helper.DELIVERY_TYPE_CONSTANT.APPOINMENT ||
      deliveryType == this._helper.DELIVERY_TYPE_CONSTANT.SERVICE) {
      this.is_delivery_business = true
    } else {
      this.is_delivery_business = false
      this.city_business_list = []
    }
    this.getCityBusiness(deliveryType);
    this.selectedBusinessId = null;
    this.is_clear_disabled = false;
  }

  getBusiness(businessType) {
    this.selectedBusinessId = businessType
  }

  getCityBusiness(deliveryType) {
    const cityIndex = this.city_list.findIndex(
      city => city._id === this.selectedCityId
    );
    if (cityIndex !== -1) {
      const city_delivery_ids = this.city_list[cityIndex].deliveries_in_city
      this.city_business_list = this.convertedChekboxList(city_delivery_ids, this.business_type_list)
      this.city_business_list = this.city_business_list.filter(cb => cb.delivery_type == deliveryType)
    } else {
      this.city_business_list = this.getAllBusinessType(deliveryType, this.city_list, this.business_type_list)
    }
  }


  getAllBusinessType(deliveryType?: number, city_list?: any, business_type_list?: any) {
    const uniqueDeliveryIdsSet = new Set();
    if (city_list) {
      for (const city of city_list) {
        for (const deliveryId of city.deliveries_in_city) {
          uniqueDeliveryIdsSet.add(deliveryId);
        }
      }

    }
    let city_business_list = this.convertedChekboxList(
      Array.from(uniqueDeliveryIdsSet), // Convert Set to Array
      business_type_list
    );
    if (deliveryType) {
       city_business_list = city_business_list.filter(cb => cb.delivery_type === deliveryType);
       return city_business_list
    }
    return city_business_list
  }


  convertedChekboxList(obj, array) {
    let checklist = []
    obj.forEach(delivery => {
      let i = array.findIndex(x => x._id === delivery);
      if (i != -1) {
        checklist.push(array[i])
      }
    });
    return checklist
  }

  getPaymentGatewayList() {
    this.paymentGatewayService.getPaymentGatewayList().then(data => {
      if (data && data.success) {
        data.payment_gateway.forEach(element => {
          this.paymentBy.push({
            id: element._id,
            checked: false,
            name: element.name || 'Other'
          })
        });
      }
    });
  }

  onChangeOrderBy(item) {
    this.itemOrder = item
    this.is_clear_disabled = false;
  }

  getList() {
    let json: any = {
    }
    if (this.selectedCityId && this.selectedCityId != "all") {
      json.city_id = this.selectedCityId;
    }
    if (this.selectedCountryId) {
      json.country_id = this.selectedCountryId
    }
    if (this.current_page) {
      json.page = this.current_page
    }
    if (this.itemsPerPage) {
      json.limit = this.itemsPerPage
    }
    if (this.search_value) {
      json.user_name = this.search_value
    }
    if (this.selectedProviderId) {
      json.driver_id = this.selectedProviderId
    }
    if (this.selectedDeliveryType) {
      json.delivery_type = this.selectedDeliveryType
    }
    if (this.itemOrder.value) {
      json.order_status_id = this.itemOrder.value
    }
    if (this.start_date) {
      json.start_date = this.start_date
    }
    if (this.end_date) {
      json.end_date = this.end_date
    }
    if (this.selectedBusinessId) {
      json.business_id = this.selectedBusinessId
    }
    json.payment_by = this.paymentBy.filter(_t => _t.checked)
    this._orderService.get_report_list(json).then((res) => {
      if (res.success) {
        ///     Adding  CityName in frontEnd
        const city_id_name_list = {};

        this.city_list.forEach((city) => {
          city_id_name_list[city._id] = city.city_name;
        });
        let delivery_types;
        if (this.city_list && this.business_type_list) {
          delivery_types = this.getAllBusinessType(null, this.city_list, this.business_type_list)
        }

        if (res?.data?.order_list.length > 0) {
          res.data.order_list.forEach((row) => {
            const city_name = city_id_name_list[row.city_id];
            if (city_name !== undefined) {
              row.city_name = city_name;
            }
            if (row.store_detail) {
              const delivery = delivery_types.find((delivery) => delivery._id === row.store_detail.store_delivery_id);
              row.business_name = delivery ? delivery.delivery_name[0] : '';
            } else {
              if (row.delivery_type == this._helper.DELIVERY_TYPE_CONSTANT.COURIER) {
                row.business_name = this._helper._trans.instant('label-title.courier')
              } else if (row.delivery_type == this._helper.DELIVERY_TYPE_CONSTANT.TAXI) {
                row.business_name = this._helper._trans.instant('label-title.taxi')
              }
            }
          });

          this.rows = res.data.order_list;
          this.total_page = res.total_page;
        } else {
          this.rows = [];
          this.total_page = 0;
        }
      }
    })
  }
  onChangeItemsPerPage(item) {
    if (this.total_page >= this.current_page) {
      this.current_page = 1;
    }
    this.itemsPerPage = item;
    this.getList();
  }

  //when change pagination page
  pageChanged(event) {
    this.current_page = event.page;
    this.getList();
  }

  export() {
    let json: any = {
    }
    if (this.selectedCityId && this.selectedCityId != "all") {
      json.city_id = this.selectedCityId;
    }
    if (this.selectedCountryId) {
      json.country_id = this.selectedCountryId
    }
      json.page = null
      json.limit = null
    if (this.search_value) {
      json.user_name = this.search_value
    }
    if (this.selectedProviderId) {
      json.driver_id = this.selectedProviderId
    }
    if (this.selectedDeliveryType) {
      json.delivery_type = this.selectedDeliveryType
    }
    if (this.itemOrder.value) {
      json.order_status_id = this.itemOrder.value
    }
    if (this.start_date) {
      json.start_date = this.start_date
    }
    if (this.end_date) {
      json.end_date = this.end_date
    }
    if (this.selectedBusinessId) {
      json.business_id = this.selectedBusinessId
    }
    json.payment_by = this.paymentBy.filter(_t => _t.checked)
    this._orderService.get_report_list(json).then((res) => {
      if (res?.data?.order_list.length > 0) {
      let report_data = res.data.order_list;
        report_data.forEach(order => {
          delete order._id
          delete order.provider_detail
          delete order.user_detail
          delete order.store_id
          delete order.store_detail
          delete order.delivery_type
          delete order.city_id
          delete order.city_id
          order.order_status = this._helper.get_order_status(order.order_status)
        });
        this._helper.export_csv(report_data, Object.keys(report_data[0]), 'report')
      }
    })
  }

  apply() {
    if (this.item_bsRangeValue && this.item_bsRangeValue.length) {
      this.is_clear_disabled = false;
      this.start_date = this.item_bsRangeValue[0];
      this.end_date = this.item_bsRangeValue[1];
    }
    this.current_page = 1;
    this.getList();
  }

  clearSelection() {
    this.selectedDeliveryType = null;
    this.selectedProviderId = null;
    this.city_business_list = null;
    this.selectedBusinessId = null;
  }

  clear() {
    this.current_page = 1;
    this.itemsPerPage = 20;
    this.selectedProviderId = null;
    this.selectedDeliveryType = null;
    this.selectedBusinessId = null;
    this.itemOrder = { label: '', value: '' };
    this.paymentBy.forEach((data: any) => {
      data.checked = false;
    })
    this.item_bsRangeValue = '';
    this.start_date = '';
    this.end_date = '';
    this.search_value = '';
    this.getList();
    this.is_clear_disabled = true;
  }

}
