import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReportsRoutingModule } from './reports-routing.module';
import { ReportsComponent } from './reports.component';
import { ReportComponent } from './report/report.component';
import { TranslateModule } from '@ngx-translate/core';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { FormsModule , FormsModule as FormsModuleAngular, ReactiveFormsModule } from '@angular/forms';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { PipeModule } from 'src/app/pipes/pipe.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { LayoutContainersModule } from 'src/app/containers/layout/layout.containers.module';
import { PagesContainersModule } from 'src/app/containers/pages/pages.containers.module';
import { DirectivesModule } from 'src/app/directives/directives.module';

@NgModule({
  declarations: [
    ReportsComponent,
    ReportComponent
  ],
  imports: [
    CommonModule,
    ReportsRoutingModule,
    PipeModule,
    TranslateModule,
    LayoutContainersModule,
    PagesContainersModule,
    BsDatepickerModule,
    FormsModule,
    FormsModuleAngular,
    ReactiveFormsModule,
    NgSelectModule,
    PaginationModule,
    BsDropdownModule.forRoot(),
    AccordionModule.forRoot(),
    DirectivesModule
  ]
})
export class ReportsModule { }
