import { Component, OnInit, ViewChild } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs';
import { AddNewOrderListModalComponent } from 'src/app/containers/pages/add-new-order-list-modal/add-new-order-list-modal.component';
import { OrderDetail } from 'src/app/models/order_detail.model';
import { OrderService } from 'src/app/services/order.service';
import { PaymentGateWayService } from 'src/app/services/payment-gateway.service';
import { Helper } from 'src/app/shared/helper';
import { PER_PAGE_LIST } from 'src/app/views/app/constant';
import { DELIVERY_TYPE_CONSTANT } from '../../../../shared/constant';

export interface Response {
  address: string,
  delivery_type: number,
  order_status: number,
  price: number,
  unique_id: number,
  user_name: string,
  _id: string,
}


@Component({
  selector: 'app-taxi',
  templateUrl: './taxi.component.html',
})
export class TaxiComponent implements OnInit {

  @ViewChild('OrderDetailModal', { static: true }) OrderDetailModal: AddNewOrderListModalComponent;
  order: OrderDetail;
  orders: Response[] = []
  count: number = 0;
  currentPage = 1;
  itemsPerPage = 10;
  itemOptionsPerPage = PER_PAGE_LIST
  orderObservable: Subscription;
  selecteddeliveryTypes = []
  selecteddeliveryStatus = []
  selectedpaymentBy = []
  query = {};
  search_value;
  search_by = 'user';
  deliveryTypes = [
    {name:'label-title.delivery',checked:false},
    {name:'label-title.pickup',checked:false},
    {name:'label-title.schedule',checked:false},
    {name:'label-title.now',checked:false}
  ];
  paymentBy = [
    {
      id:'cash',
      checked:false,
      name:'label-title.cash'
    }
  ];
  timezone_for_display_date:string = '';

  constructor(private _orderService: OrderService,
    public _helper: Helper,
    private paymentGatewayService:PaymentGateWayService,
    private _translateService: TranslateService) { }

  ngOnInit(): void {

    this._helper.display_date_timezone.subscribe(data => {
      this.timezone_for_display_date = data;
    })

    this.paymentGatewayService.getPaymentGatewayList().then(data=>{
      if(data && data.success){
        data.payment_gateway.forEach(element => {
          this.paymentBy.push({
            id:element._id,
            checked:false,
            name:element.name || 'Other'
          })
        });
      }
    });

    // this.loadData()
    this.orderObservable = this._orderService._orderObservable.subscribe(() => {
      this.loadData();
    })
  }

  loadData() {
    this._orderService.list_orders(null, null, this.currentPage, this.itemsPerPage,this.query, DELIVERY_TYPE_CONSTANT.TAXI).then(res_data => {
      if (res_data.success) {
        this.orders = res_data.data.results;
        this.count = res_data.data.count;
      }else{
        this.orders = [];
        this.count = 0;
      }
    })
  }

  onChangeDeliveryType(item){
    item.checked = !item.checked;
  }

  onShowOrderDetails(orderid,event) {
    if(event.target.tagName.toLowerCase() !== "button"){
      this.OrderDetailModal.show(orderid, 1)
    }
  }

  onChangeStatus(orderid: string, status: number) {
    this._orderService.set_order_status(orderid, status)
  }

  onCompleteOrder(orderid: string) {
    this._orderService.complete_order(orderid)
  }

  changeOrderBy(event){
    if(event.value === 'all'){
      this.search_by = undefined;
    }else{
      this.search_by = event.value;
    }
  }


  onSearch(){
    this.currentPage=1;
    this.query['delivery_types'] = this.deliveryTypes.filter(_t=>_t.checked);
    this.query['payment_by'] = this.paymentBy.filter(_t=>_t.checked);

    if(this.query['delivery_types']){
      this.query['delivery_types'].forEach(element => {
        element.name = this._translateService.instant(element.name).toLowerCase()
      });
    }
    if(this.search_by && this.search_value !== '' && this.search_value !== undefined){
      this.query['search_by'] = this.search_by;
      this.query['search_value'] = this.search_value;
    }else{
      this.query['search_by'] = undefined;
      this.query['search_value'] = undefined;
    }
    this.loadData();
  }

  searchKeyUp(value){
    this.search_value = value;
  }

  ngOnDestroy() {
    if (this.orderObservable) {
      this.orderObservable.unsubscribe()
    }
    if(this.OrderDetailModal.modalRef){
      this.OrderDetailModal.onClose()
    }
  }

  itemsPerPageChange(event) {
    this.itemsPerPage = event;
    this.loadData()
  }

  pageChanged(event) {
    this.currentPage = event.page;
    this.loadData()
  }

  onExport(event) {
    this._orderService.list_orders(null, null, null, null,{}).then(res_data => {
      if (res_data && res_data.success) {
        let orders = res_data.data.results;
        orders.forEach(order => {
          delete order._id
          delete order.user_image_url
          order.order_status = this._helper.get_order_status(order.order_status)
        });
        res_data.data.results.user_phone = JSON.stringify(res_data.data.results.user_phone)
        this._helper.export_csv(orders, Object.keys(orders[0]), 'orders')
      }
    })
  }

}
