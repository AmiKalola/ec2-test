import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HistoryListComponent } from '../history/history-list/history-list.component';
import { ReviewsComponent } from '../history/reviews/reviews.component';
import { DispatchComponent } from './dispatch/dispatch.component';
import { OrderComponent } from './order.component';
import { TodayOrdersComponent } from './today-orders/today-orders.component'
import { TomorrowOrdersComponent } from './tomorrow-orders/tomorrow-orders.component';
import { TableBookingComponent } from './table-booking/table-booking.component'
import { ServiceComponent } from './service/service.component';
import { AppointmentComponent } from './appointment/appointment.component';
import { TaxiComponent } from './taxi/taxi.component';
import { EcommerceComponent } from './ecommerce/ecommerce.component';
import { ReportComponent } from '../reports/report/report.component';


const routes: Routes = [
  {
    path: '', component: OrderComponent,
    children: [
      { path: '', redirectTo: 'running-orders', pathMatch: 'full' },
      {
        path: 'running-orders', children: [
          { path: '', redirectTo: 'orders', pathMatch: 'full' },
          { path: 'orders', component: TodayOrdersComponent, data: { auth: '/admin/orders' } },
          { path: 'delivery', component: TomorrowOrdersComponent, data: { auth: '/admin/delivery' } },
          { path: 'dispatch', component: DispatchComponent, data: { auth: '/admin/dispatch' } },
          { path: 'taxi', component: TaxiComponent, data: { auth: '/admin/taxi' } },
          { path: 'service', component: ServiceComponent, data: { auth: '/admin/service' } },
          { path: 'ecommerce', component: EcommerceComponent, data: { auth: '/admin/ecommerce' } },
          { path: 'appointment', component: AppointmentComponent, data: { auth: '/admin/appointment' } },
          { path: 'table-booking', component: TableBookingComponent, data: { auth: '/admin/table-booking' } }
        ]
      },
      {
        path: 'history', children: [
          { path: '', redirectTo: 'history-list', pathMatch: 'full' },
          { path: 'history-list', component: HistoryListComponent, data: { auth: '/admin/history-list' } },
          { path: 'reviews', component: ReviewsComponent,data: { auth: '/admin/reviews' }  },
        ]
      },
      {
        path: 'reports', children: [
          { path: '', redirectTo: 'report', pathMatch: 'full' },
          { path: 'report', component: ReportComponent, data: { auth: '/admin/report' } },
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrderRoutingModule { }
