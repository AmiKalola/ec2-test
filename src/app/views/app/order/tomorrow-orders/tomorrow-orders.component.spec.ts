import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { TomorrowOrdersComponent } from './tomorrow-orders.component';

describe('TomorrowOrdersComponent', () => {
  let component: TomorrowOrdersComponent;
  let fixture: ComponentFixture<TomorrowOrdersComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ TomorrowOrdersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TomorrowOrdersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
