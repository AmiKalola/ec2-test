  import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
  import { Subscription } from 'rxjs';
  import { OrderDetail } from 'src/app/models/order_detail.model';
  import { OrderService } from 'src/app/services/order.service';
  import { PaymentGateWayService } from 'src/app/services/payment-gateway.service';
  import { DeliveriesService } from 'src/app/services/deliveries.service';
  import { Helper } from 'src/app/shared/helper';
import { LangService } from 'src/app/shared/lang.service';
import { DeliveriesDetailModal } from 'src/app/containers/pages/deliveries-details-modal/deliveries-details-modal.component';
import { SocketService } from 'src/app/shared/socket.service';
import { SOCKET_TYPE } from 'src/app/views/app/constant';
import { DELIVERY_TYPE,PER_PAGE_LIST } from '../../../../shared/constant'
import { PaginationComponent } from 'ngx-bootstrap/pagination';

  export interface Response {
    address: string,
    delivery_type: number,
    order_status: number,
    price: number,
    unique_id: number,
    user_name: string,
    _id: string,
  }


  @Component({
    selector: 'app-tomorrow-orders',
    templateUrl: './tomorrow-orders.component.html',
    styleUrls: ['./tomorrow-orders.component.scss']
  })
  export class TomorrowOrdersComponent implements OnInit, OnDestroy {

    @ViewChild('DeliveriesDetailModal', { static: true }) DeliveriesDetailModal: DeliveriesDetailModal;
    is_clear_disabled:boolean=true;

    order: OrderDetail;
    orders: Response[] = []
    count: number = 0;
    currentPage = 1;
    itemsPerPage = 20;
    itemOptionsPerPage = PER_PAGE_LIST
    orderObservable: Subscription;
    selecteddeliveryTypes = []
    selecteddeliveryStatus = []
    selectedpaymentBy = []
    query = {};
    pages = 0;
    search_value = '';
    search_by = 'user';
    deliveryTypes = [
      {name:'label-title.delivery', value:'delivery',checked:false},
      {name:'label-title.pickup', value:'pickup', checked:false},
      {name:'label-title.schedule', value:'schedule', checked:false},
      {name:'label-title.now', value:'now', checked:false}
    ];
    paymentBy = [
      {
        id:'cash',
        checked:false,
        name:'label-title.cash'
      }
    ];
    json = {
      created_by: "both",
      end_date: "",
      no_of_records: this.itemsPerPage,
      order_type: "both",
      page: this.currentPage,
      payment_status: "all",
      pickup_type: "both",
      provider_name: "",
      request_status: "",
      start_date: "",
      store_name: "",
      unique_id: null,
      request_id: null,
      delivery_type: 1,
      user_name: ""
    }
    item_order ={ label: 'label-title.user', value: 'user' }

    DELIVERY_TYPE = DELIVERY_TYPE
    request_delivery_type = DELIVERY_TYPE[0];
    timezone_for_display_date:string = '';

    @ViewChild('listPagination', { static: true }) listPagination: PaginationComponent;
    toastSubscription: Subscription;

    constructor(private _orderService: OrderService,
      private _helper: Helper,
      private paymentGatewayService:PaymentGateWayService,
      private _delivriesService: DeliveriesService,
      public _langService: LangService,
      private _socket: SocketService) { }

    ngOnInit(): void {

      this._helper.display_date_timezone.subscribe(data => {
        this.timezone_for_display_date = data;
      })

      this.paymentGatewayService.getPaymentGatewayList().then(data=>{
        if(data && data.success){
          data.payment_gateway.forEach(element => {
            this.paymentBy.push({
              id:element._id,
              checked:false,
              name:element.name[0] || 'Other'
            })
          });
        }
      });

      this.orderObservable = this._orderService._orderObservable.subscribe(() => {
        this.loadData();
      })
    }

    register_new_order_socket(delivery_type) {
      this._socket.removeAllListeners();

      let listner = "'" + SOCKET_TYPE.DELIVERY + delivery_type + "'";
      this._socket.listener(listner).subscribe(order => {
        this.loadData()
        let toast = this._helper.pushNotification(1, 'label-title.new-delivery-received')
        this.toastSubscription = toast.click.subscribe((event) => {
          this.listPagination.selectPage(1)
          this.toastSubscription.unsubscribe()
        });
      })
    }

    register_order_status_socket(request_id) {
      let listner = "'" + request_id + "'"
      this._socket.listener(listner).subscribe(order => {
        this.fetchNewOrderDetail(order.order_id)
      })
    }

    fetchNewOrderDetail(request_id) {
      this.json.request_id = request_id;
      this._delivriesService.delivery_list_search_sort(this.json).then(res_data => {
        this.json.request_id = null;
        if (res_data.success) {
          let idx = this.orders.findIndex(i => i._id == request_id);
          if (idx != -1) {
            this.orders[idx] = res_data.requests[0];
          }
        } else {
          this.loadData()
        }
      })
    }

    onRequestDeliveryTypeChange(value) {
      this.request_delivery_type = value;
      this.is_clear_disabled=false;
    }

    loadData() {
      this.json.no_of_records = this.itemsPerPage;
      this.json.page = this.currentPage;
      this.json.delivery_type = this.request_delivery_type.value;
      this.register_new_order_socket(this.request_delivery_type.value);

      this._delivriesService.delivery_list_search_sort(this.json).then(res_data => {
        if(res_data.success){
          this.orders = res_data.requests;
          this.pages = res_data.pages

          this.orders.forEach((data) => {
            this.register_order_status_socket(data._id)
          })
        } else {
          this.orders = []
          this.pages = 0
        }
      })
    }

    onChangeDeliveryType(item){
      item.checked = !item.checked;
      this.is_clear_disabled=false;
    }

    onShowOrderDetails(orderid, provider,event) {
      let provider_id = null
      if(provider){
        provider_id = provider._id
      }
      if(event.target.tagName.toLowerCase() !== "button"){
        this.DeliveriesDetailModal.show(orderid, provider_id)
      }
    }

    onChangeStatus(orderid: string, status: number) {
      this._orderService.set_order_status(orderid, status)
    }

    onCompleteOrder(orderid: string) {
      this._orderService.complete_order(orderid)
    }

    changeOrderBy(event){
      this.is_clear_disabled=false;
      if(event.value === 'all'){
        this.search_by = undefined;
      }else{
        this.search_by = event.value;
      }
    }


    onSearch(){
      this.currentPage=1;
      this.is_clear_disabled=false;
      this.json.user_name = ''
      this.json.store_name = ''
      this.json.provider_name = ''
      if(this.search_by === 'user'){
        this.json.user_name = this.search_value
      } else if(this.search_by === 'store'){
        this.json.store_name = this.search_value
      } else if(this.search_by === 'provider'){
        this.json.provider_name = this.search_value
      }

      this.loadData();
    }

    searchKeyUp(value){
      this.search_value = value;
      this.is_clear_disabled=false;
    }



    ngOnDestroy() {
      if (this.orderObservable) {
        this.orderObservable.unsubscribe()
      }

      if(this.DeliveriesDetailModal.modalRef){
        this.DeliveriesDetailModal.onClose()
      }

      if (this.toastSubscription) {
        this.toastSubscription.unsubscribe()
      }
      this._socket.removeAllListeners()
    }

    itemsPerPageChange(event) {
      this.itemsPerPage = event;
      this.loadData()
    }

    pageChanged(event) {
      this.currentPage = event.page;
      this.loadData()
    }

    onExport(event) {
      this._orderService.list_orders(null, null, null, null,{}).then(res_data => {
        if (res_data && res_data.success) {
          let orders = res_data.data.results;
          this._helper.export_csv(orders, Object.keys(orders[0]), 'orders')
        }
      })
    }
    clear_filter(){
      this.json = {
        created_by: "both",
        end_date: "",
        no_of_records: this.itemsPerPage,
        order_type: "both",
        page: this.currentPage,
        payment_status: "all",
        pickup_type: "both",
        provider_name: "",
        request_status: "",
        start_date: "",
        store_name: "",
        unique_id: null,
        request_id: null,
        delivery_type: 1,
        user_name: ""
      }
      this.request_delivery_type = DELIVERY_TYPE[0];
      this.item_order ={ label: 'label-title.user', value: 'user' }
      this.query={};
      this.currentPage = 1;
      this.itemsPerPage  = 20; 
      this.search_value = '';
      this.search_by = 'user';
      this.loadData();
      this.is_clear_disabled = true;
    }
  }
