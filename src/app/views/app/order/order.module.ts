import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OrderRoutingModule } from './order-routing.module';
import { OrderComponent } from './order.component';
import { TodayOrdersComponent } from './today-orders/today-orders.component';
import { TomorrowOrdersComponent } from './tomorrow-orders/tomorrow-orders.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { MenuRoutingModule } from '../menu/menu-routing.module';
import { PagesContainersModule } from 'src/app/containers/pages/pages.containers.module';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ModalModule } from 'ngx-bootstrap/modal';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { RatingModule } from 'ngx-bootstrap/rating';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HotkeyModule } from 'angular2-hotkeys';
import { DispatchComponent } from './dispatch/dispatch.component';
import { LayoutContainersModule } from 'src/app/containers/layout/layout.containers.module';
import { PipeModule } from 'src/app/pipes/pipe.module';
import { DirectivesModule } from 'src/app/directives/directives.module';
import { ProgressbarModule } from 'ngx-bootstrap/progressbar';
import { TableBookingComponent } from './table-booking/table-booking.component';
import { TaxiComponent } from './taxi/taxi.component';
import { ServiceComponent } from './service/service.component';
import { AppointmentComponent } from './appointment/appointment.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { EcommerceComponent } from './ecommerce/ecommerce.component';

@NgModule({
  declarations: [OrderComponent, TodayOrdersComponent, TomorrowOrdersComponent, DispatchComponent, TableBookingComponent, TaxiComponent, ServiceComponent, AppointmentComponent,EcommerceComponent],
  imports: [
    SharedModule,
    PagesContainersModule,
    LayoutContainersModule,
    CommonModule,
    FormsModule,
    MenuRoutingModule,
    DirectivesModule,
    OrderRoutingModule,
    PipeModule,
    NgSelectModule,
    RatingModule.forRoot(),
    ReactiveFormsModule,
    HotkeyModule.forRoot(),
    PaginationModule.forRoot(),
    TabsModule.forRoot(),
    ModalModule.forRoot(),
    BsDropdownModule.forRoot(),
    AccordionModule.forRoot(),
    ProgressbarModule.forRoot()
  ]
})
export class OrderModule { }
