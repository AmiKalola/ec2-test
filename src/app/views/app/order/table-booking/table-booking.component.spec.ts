import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { TableBookingComponent } from './table-booking.component';

describe('TableBookingComponent', () => {
  let component: TableBookingComponent;
  let fixture: ComponentFixture<TableBookingComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ TableBookingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableBookingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
