import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { TableOrderDetailModalComponent } from 'src/app/containers/pages/table-booking-detail-modal/table-booking-detail-modal.component';
import { OrderService } from 'src/app/services/order.service';
import { PaymentGateWayService } from 'src/app/services/payment-gateway.service';
import { Helper } from 'src/app/shared/helper';
import { PaginationComponent } from 'ngx-bootstrap/pagination';
import { Subscription } from 'rxjs';
import { SocketService } from 'src/app/shared/socket.service';
import { DELIVERY_TYPE, SOCKET_TYPE } from 'src/app/views/app/constant';
import { PER_PAGE_LIST } from '../../../../shared/constant'
@Component({
  selector: 'app-table-booking',
  templateUrl: './table-booking.component.html',
  styleUrls: ['./table-booking.component.scss']
})
export class TableBookingComponent implements OnInit, OnDestroy {
  is_clear_disabled:boolean=true;

  deliveryTypes = [
    { name: 'label-title.delivery', checked: false },
    { name: 'label-title.pickup', checked: false },
  ];
  itemOptionsOrders = [
    {label: 'label-title.user', value: 'user'},
    {label: 'label-title.order', value: 'order'}
  ]
  itemOrder = {label: 'label-title.user', value: ''}
  orders: any[] = []
  itemOptionsPerPage = PER_PAGE_LIST
  itemsPerPage: any = 20;
  search_value: string = '';
  search_by: 'user';
  count: number = 0;
  currentPage = 1;
  query: {};
  timezone_for_display_date:string = '';

  @ViewChild('listPagination', { static: true }) listPagination: PaginationComponent;
  toastSubscription: Subscription;

  @ViewChild('TableOrderDetailModalComponent', { static: true }) TableOrderDetailModal: TableOrderDetailModalComponent;

  constructor(private _orderService: OrderService,
    public _helper: Helper,
    private paymentGatewayService:PaymentGateWayService,
    private _translateService: TranslateService,
    private _socket: SocketService) { }

  ngOnInit(): void {
    this._helper.display_date_timezone.subscribe(data => {
      this.timezone_for_display_date = data;
    })
    this.loadData()
  }

  itemsPerPageChange(event) {
    this.itemsPerPage = event;
  }

  searchKeyUp(value) {
    this.search_value = value;
  }

  pageChanged(event) {
    this.currentPage = event.page;
    this.loadData()
  }
  changeOrderBy(event) {
    this.is_clear_disabled=false;
    if (event.value === 'all') {
      this.search_by = undefined;
    } else {
      this.search_by = event.value;
      this.itemOrder = event
    }
  }
  onExport(event) { }

  onSearch() {
    this.currentPage=1;
    this.is_clear_disabled=false;
    this.loadData()
  }

  onShowOrderDetails(orderid,event) {
    if(event.target.tagName.toLowerCase() !== "button"){
      this.TableOrderDetailModal.show(orderid)
    }
  }

  register_new_order_socket() {
    this._socket.removeAllListeners();

    let listner = "'" + SOCKET_TYPE.ORDER + DELIVERY_TYPE.TABLE + "'";
    this._socket.listener(listner).subscribe(order => {
      this.loadData()
      let toast = this._helper.pushNotification(1, 'label-title.new-order-received')
      this.toastSubscription = toast.click.subscribe((event) => {
        this.listPagination.selectPage(1)
        this.toastSubscription.unsubscribe()
      });
    })
  }

  register_order_status_socket(order_id) {
    let listner = "'" + order_id + "'"
    this._socket.listener(listner).subscribe(order => {
      this.fetchNewOrderDetail(order.order_id)
    })
  }

  fetchNewOrderDetail(order_id) {
    this._orderService.table_list_orders(null, null, this.currentPage, this.itemsPerPage, this.search_by, this.search_value, order_id).then(res_data => {
      if (res_data.success) {
        let idx = this.orders.findIndex(i => i._id == order_id);
        if (idx != -1) {
          this.orders[idx] = res_data.data.results[0]
        }
      } else {
        this.loadData()
      }
    })
  }

  loadData() {
    this.register_new_order_socket()
    this._orderService.table_list_orders(null, null, this.currentPage, this.itemsPerPage,this.search_by, this.search_value).then(res_data => {
      if (res_data.success) {
        this.orders = res_data.data.results;
        this.count = res_data.data.count;

        this.orders.forEach((data) => {
          this.register_order_status_socket(data._id)
        })
      }else{
        this.orders = [];
        this.count = 0;
      }
    })
  }

  onChangeOrderBy(item){
    this.itemOrder = item
  }

  onChangeItemsPerPage(value){
    this.itemsPerPage = value
    this.loadData()
  }

  clickonExport(){

    let fields = [
      {label: 'ID', value: 'unique_id'},
      {label: 'User', value: 'user_name'},
      {label: 'Store', value: 'store_name'},
      {label: 'Total',value: 'total'},
      {label: 'Table no', value: 'table_no'},
      {label: 'Person no', value: 'table_no'},
      {label: 'Date', value: 'delivery_date'},
      {label: 'Status', value: 'order_status'},
    ]
    this.loadData()
    let order_data = JSON.parse(JSON.stringify(this.orders))
    order_data.forEach(order => {
      order.order_status = this._helper.get_order_status(order.order_status)
      let date = new Date(order.delivery_date)
      order.delivery_date = date.toLocaleString('en-US');
    });
    this._helper.downloadcsvFile(order_data,fields);
  }

  ngOnDestroy(): void {
    if (this.TableOrderDetailModal.modalRef){
      this.TableOrderDetailModal.onClose();
    }

    if (this.toastSubscription) {
      this.toastSubscription.unsubscribe()
    }
    this._socket.removeAllListeners()
  }
  clear_filter(){
    this.itemOrder ={ label: 'label-title.user', value: 'user' }
    this.currentPage = 1;
    this.itemsPerPage  = 20; 
    this.search_value = '';
    this.search_by = 'user';
    this.loadData();
    this.is_clear_disabled = true;
  }
}
