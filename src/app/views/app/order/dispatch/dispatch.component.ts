import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { DispatcherService } from '../../../../services/dispatcher.service'
import { SocketService } from '../../../../shared/socket.service'
import { environment } from 'src/environments/environment';
import { DEFAULT_IMAGE_PATH, DELIVERY_TYPE_CONSTANT } from 'src/app/shared/constant';
import { Helper } from 'src/app/shared/helper';
import { AutoAssignModelComponent } from 'src/app/containers/pages/auto-assign-model/auto-assign-model.component';
import { DELIVERY_TYPE } from '../../../../shared/constant'
import { SOCKET_TYPE } from 'src/app/views/app/constant';
import { AddNewOrderListModalComponent } from 'src/app/containers/pages/add-new-order-list-modal/add-new-order-list-modal.component';
declare let google;
@Component({
  selector: 'app-dispatch',
  templateUrl: './dispatch.component.html',
  styleUrls: ['./dispatch.component.scss']
})
export class DispatchComponent implements OnInit, OnDestroy {
  
  @ViewChild('OrderDetailModal', { static: true }) OrderDetailModal: AddNewOrderListModalComponent;
  map:any;
  provider_info=[];
  order_info = [];
  markers_tab_num:number = 1;
  order_tab_num:number= 1;

  DELIVERY_TYPE = DELIVERY_TYPE
  IMAGE_URL = environment.imageUrl;
  selectedOrder:any = {_id:null};
  selectedProvider:string = null;
  interval: any =null;
  searchOrderText:string = '';
  searchProviderText:string = '';
  currency_sign:string = '';
  providers = [];
  orders = [];
  order_markers: any[] = [];
  provider_markers: any[] = [];
  USER_DEFAULT_IMAGE = DEFAULT_IMAGE_PATH.USER
  vehicles_list: any = []
  filtered_provider: any = []
  is_assignable: boolean = false;
  selected_vehicle_id: any = null;
  map_center_lat_lng = [22.3039, 70.8022];
  request_delivery_type: number= 1;
  service_delivery_type: number= 1;
  timezone_for_display_date:string = '';
  theme : any;
  orderNo= this._helper._trans.instant('label-title.order-no');

  @ViewChild('AutoAssignModal', { static: true }) autoAssignModal: AutoAssignModelComponent;

  constructor(private dispatcherService: DispatcherService, private _helper: Helper, private _socket: SocketService) { }

  ngOnInit(): void {
    this._helper.display_date_timezone.subscribe(data => {
      this.timezone_for_display_date = data;
    })

    this.getOrderList()
    setTimeout(() => {
      this.map_center_lat_lng = this._helper.admin_lat_long ;
      this._initMap();
    }, 800);
    this.getDeliverymanList()
    let cla = document.getElementsByTagName('main')
    cla[0].style.marginLeft = "60px";
    cla[0].style.marginRight = "60px";

    let element = document.getElementById('app-container')
    element.classList.add("sub-hidden")
    element.classList.add("main-hidden")

    this.register_new_order_socket(this.request_delivery_type)
  }

  register_new_order_socket(delivery_type) {
    this._socket.removeAllListeners();
    let listner = "'" + SOCKET_TYPE.DISPATCH + delivery_type + "'";
    this._socket.listener(listner).subscribe(order => {
      this.fetchNewOrderDetail(order.order_id)
    })
  }

  register_order_status_socket(order_id) {
    let listner = "'" + order_id + "'"
    this._socket.listener(listner).subscribe(order => {
      this.fetchNewOrderDetail(order.order_id)
    })
  }

  fetchNewOrderDetail(order_id) {
    let json = { delivery_type: this.request_delivery_type, order_id }
    this.dispatcherService.get_admin_dispatcher_order_list(json).then((data) => {
      if (data.success) {
        data.requests.forEach((data) => {
          data.unique_id = String(data.unique_id);
          if (!data.provider_detail) {
            data.provider_detail = null;
          }
        })

        let idx = this.orders.findIndex(i => i._id == order_id);
        if (idx != -1) {
          this.orders[idx] = data.requests[0]
        } else {
          this.orders.unshift(data.requests[0])
          this.register_order_status_socket(order_id);
        }
      } else {
        let idx = this.orders.findIndex(i => i._id == order_id);
        if (idx != -1) {
          this.orders.splice(idx, 1)
          let listner = "'" + order_id + "'";
          this._socket.removeListener(listner);
        }
      }
    });
  }

  cancleRequest(request_id , provider_id){
    this.dispatcherService.cancel_request(request_id,provider_id).then(data=>{
      if(data.success){
        this.getOrderList();
      }
    })
  }

  selectedOrderDetail(data,index){
    this.selectedOrder = data;
    if(this.order_markers.length>0) {
      this.order_markers.forEach((marker) => {
        marker.setMap(null);
      })
    }
    let info_window 
    if (data.provider_detail) {
      info_window = data.provider_detail.name + '<br />' +this.orderNo +': ' + data.unique_id
    } else {
      info_window = this.orderNo +': ' + data.unique_id
    }
    this.order_markers = [];
    this.order_info = [];
    this.map_center_lat_lng = [data.destination_addresses[0].location[0], data.destination_addresses[0].location[1]]
    this._initMap()
    this.set_provider_markers(this.markers_tab_num);
    this._helper.ngZone.run(() => {
      this.order_info.push({
        latlon: new google.maps.LatLng(this.map_center_lat_lng[0], this.map_center_lat_lng[1]),
        bearing: data.bearing,
        order_id: data._id,
        draggable: false,
        message: new google.maps.InfoWindow({
          content: info_window,
          maxWidth: 320,
        })
      });
      this.order_info.forEach((loc_data, i) => {
        let marker = new google.maps.Marker({
          position: loc_data.latlon,
          map: this.map,
          draggable: false,
          icon: "/assets/img/pins/order_destination.png",
        });
        this.order_markers.push(marker);
        google.maps.event.addListener(marker, 'click', function (e) {
          loc_data.message.open(this.map, marker);
          setTimeout(function () {
            loc_data.message.close();
          }, 5000);
        });
      });
    })

  }

  providerSelected(data, marker){
    this.selectedProvider = data;
    if(this.provider_markers.length>0) {
      this.provider_markers.forEach((marker) => {
        marker.setMap(null);
      })
    }
    this.provider_markers = [];
    this.provider_info = [];
    this.map_center_lat_lng = [data.location[0], data.location[1]]
    this._initMap()
    let icon
    if(marker == 1) {
      icon = "/assets/img/pins/provider_online.png"
    } else if(marker == 2) {
      icon = "/assets/img/pins/provider_order.png"
    } else {
      icon = "/assets/img/pins/provider_offline.png"
    }
    this.set_markers(this.order_tab_num)
    this._helper.ngZone.run(() => {
      this.provider_info.push({
        latlon: new google.maps.LatLng(this.map_center_lat_lng[0], this.map_center_lat_lng[1]),
        bearing: data.bearing,
        order_id: data._id,
        draggable: false,
        message: new google.maps.InfoWindow({
          content: data.first_name + ' ' + data.last_name,
          maxWidth: 320,
        })
      });
      this.provider_info.forEach((loc_data, i) => {
        let marker = new google.maps.Marker({
          position: loc_data.latlon,
          map: this.map,
          draggable: false,
          icon: icon,
        });
        this.provider_info.push(marker);
        google.maps.event.addListener(marker, 'click', function (e) {
          loc_data.message.open(this.map, marker);
          setTimeout(function () {
            loc_data.message.close();
          }, 5000);
        });
      });
    })
  }

  getDeliverymanList(){
    let order_id = null
    this.dispatcherService.get_provider_list(this.service_delivery_type,order_id,this.request_delivery_type).then(data=>{
      if(data.success){
        this.providers = data.providers;
        this.filtered_provider = this.providers
        if(this.providers.length > 0){
          this.set_provider_markers(this.markers_tab_num);
          setTimeout(() => {
            this.set_markers(this.order_tab_num);
          }, 200);
          this.selectedProvider = this.providers[0]._id;
          this.selectedOrder['is_process'] = false;
        }
      }
    })
  }

  getOrderList(){
    
    let json = { delivery_type: this.request_delivery_type }
    this.dispatcherService.get_admin_dispatcher_order_list(json).then((data) => {

      if(data.success){
        data.requests.forEach((data)=>{
          this.register_order_status_socket(data._id)
          data.unique_id = String(data.unique_id);
          if(!data.provider_detail){
            data.provider_detail = null;
          }
        })
        this.orders = data.requests;
        if(this.orders.length > 0 && this.selectedOrder && this.selectedOrder._id){
          const idx = this.orders.findIndex(_order=>_order._id.toString() === this.selectedOrder._id.toString());
          if(idx > -1){
            this.selectedOrder = this.orders[idx];
          }
        }
      } else if (!data.success && data.error_code === 652){
        this.orders = []
      }
    })
  }

  onRequestDeliveryTypeChange(){
    this.register_new_order_socket(this.request_delivery_type)

    this.getOrderList()
    if(this.request_delivery_type === DELIVERY_TYPE_CONSTANT.SERVICE || this.request_delivery_type === DELIVERY_TYPE_CONSTANT.APPOINMENT){
      this.service_delivery_type = 2
      this.getDeliverymanList()
    } else {
      this.service_delivery_type = 1
      this.getDeliverymanList()
    }
  }

  onAssignProvider(orderid){
    if((orderid.delivery_type==DELIVERY_TYPE_CONSTANT.SERVICE || orderid.delivery_type==DELIVERY_TYPE_CONSTANT.APPOINMENT) && orderid.is_admin_services){
      this.vehicles_list = [];
      this.autoAssignModal.show(orderid,false, this.vehicles_list, orderid.delivery_type, orderid.is_admin_services);
    } else {
      this.dispatcherService.get_store_vehicle_list(orderid.city_id, orderid.delivery_type).then(res_data => {
        this.vehicles_list = res_data.admin_vehicles
        this.autoAssignModal.show(orderid,false, this.vehicles_list, orderid.delivery_type);
      })
    }
  }

  onAssign(data){
    this.selected_vehicle_id = data.selectedVehicle
    let vehicle_id = data.selectedVehicle
    let order_id =  this.selectedOrder.order_id
    if(data.type === 1){
      let json = {
        id: this.selectedOrder.store_id,
        store_id: this.selectedOrder.store_id,
        vehicle_id,
        order_id
      }
      this.dispatcherService.create_request(json).then(res_data => {
      })
    } else {
      this.is_assignable = true
      // this.filtered_provider = this.providers.filter(x => (x.admin_vehicle_id.indexOf(data.selectedVehicle) != -1 && x.city_id === this.selectedOrder.city_id))
      this.dispatcherService.get_provider_list(this.service_delivery_type,order_id,this.request_delivery_type).then(provider_data=>{
        if(provider_data.success){
          this.providers = provider_data.providers;
          this.filtered_provider = this.providers.filter(x => (x.vehicle_id && (x?.vehicle_id?.toString() === data?.selectedVehicle?.toString()) || data.is_admin_services) && x.city_id === this.selectedOrder.city_id)          
        }
      })
    }
  }


  orderAssing(provider){
    this.selectedOrder['is_process'] = true;
    let json = {
      id: this.selectedOrder.store_id,
      provider_id: provider._id,
      order_id: this.selectedOrder._id,
      store_id: this.selectedOrder.store_id,
      vehicle_id: this.selected_vehicle_id,
      request_id: this.selectedOrder.request_id
    }
    this.dispatcherService.create_request(json).then((data)=> {
      this.selectedOrder['is_process'] = false;
      if(data.success){
        this.getDeliverymanList()
        this.getOrderList()
        this.is_assignable = false
        this.selectedOrder = {_id: null}
        this.selected_vehicle_id = null
      }
    })
  }

  set_provider_markers(markers_type){
    this.searchProviderText = '';
    this.markers_tab_num=markers_type;
    if (this.provider_markers.length>0) {
      this.provider_markers.forEach((marker) => {
        marker.setMap(null);
      })
    }
    let markerIcon;
    this.provider_markers = [];
    this.provider_info = [];
    if(markers_type == 1){
      markerIcon = "/assets/img/pins/provider_online.png";
      this.free_providers.forEach((provider_data)=>{
        if(provider_data.location){
        this.provider_info.push({
          latlon: new google.maps.LatLng(provider_data.location[0], provider_data.location[1]),
          bearing: provider_data.bearing,
          order_id: provider_data._id,
          draggable: false,
          message: new google.maps.InfoWindow({
            content: provider_data.first_name+' '+provider_data.last_name,
            maxWidth: 320,
          })
        });
        }
      })
    }else if(markers_type == 2){
      markerIcon = "/assets/img/pins/provider_order.png";
      this.on_job_providers.forEach((provider_data)=>{
        if(provider_data.location){
        this.provider_info.push({
          latlon: new google.maps.LatLng(provider_data.location[0], provider_data.location[1]),
          bearing: provider_data.bearing,
          order_id: provider_data._id,
          draggable: false,
          message: new google.maps.InfoWindow({
            content: provider_data.first_name+' '+provider_data.last_name,
            maxWidth: 320,
          })
        });
        }
      })
    }else{  
      markerIcon = "/assets/img/pins/provider_offline.png";
      this.offline_providers.forEach((provider_data)=>{
        if(provider_data.location){
          this.provider_info.push({
            latlon: new google.maps.LatLng(provider_data.location[0], provider_data.location[1]),
            bearing: provider_data.bearing,
            order_id: provider_data._id,
            draggable: false,
            message: new google.maps.InfoWindow({
              content: provider_data.first_name+' '+provider_data.last_name,
              maxWidth: 320,
            })
          });
        }
      })
    }
    this.provider_info.forEach((loc_data, i) => {
      let marker = new google.maps.Marker({
        position: loc_data.latlon,
        map: this.map,
        draggable: false,
        icon: markerIcon
      });
      this.provider_markers.push(marker);
      google.maps.event.addListener(marker, 'click', function (e) {
        loc_data.message.open(this.map, marker);
        setTimeout(function () {
          loc_data.message.close();
        }, 5000);
      });
    });
  }
  set_markers(markers_type){
    this.searchOrderText = '';
    this.order_tab_num =markers_type;
    if(this.order_markers.length>0) {
      this.order_markers.forEach((marker) => {
        marker.setMap(null);
      })
    }
    this.order_markers = [];
    this.order_info = [];
    if(markers_type == 1){      
      this.new_orders.forEach((provider_data)=>{  
             
        if(provider_data.destination_addresses[0].location){
        this.order_info.push({
          latlon: new google.maps.LatLng(provider_data.destination_addresses[0].location[0], provider_data.destination_addresses[0].location[1]),
          bearing: provider_data.bearing,
          order_id: provider_data._id,
          draggable: false,
          message: new google.maps.InfoWindow({
            content: this.orderNo +' : '+provider_data.unique_id,
            maxWidth: 320,
          })
        });
        }
      })
    }else if(markers_type == 3){
      this.ongoing_orders.forEach((provider_data)=>{
        if(provider_data.destination_addresses[0].location){
        this.order_info.push({
          latlon: new google.maps.LatLng(provider_data.destination_addresses[0].location[0], provider_data.destination_addresses[0].location[1]),
          bearing: provider_data.bearing,
          order_id: provider_data._id,
          draggable: false,
          message: new google.maps.InfoWindow({
            content: `${provider_data.provider_detail.name} <br />${this.orderNo}  : ${provider_data.unique_id}`,
            maxWidth: 320,
          })
        });
        }
      })
    }else{
      this.assign_orders.forEach((provider_data)=>{
        let info_window 
        if (provider_data.provider_detail && provider_data.delivery_status !== 105 && provider_data.delivery_status !== 106) {
          info_window = `${provider_data.provider_detail.name} <br />${this.orderNo}  : ${provider_data.unique_id}`
        } else {
          info_window = this.orderNo +' : '+provider_data.unique_id
        }
        if(provider_data.destination_addresses[0].location){
        this.order_info.push({
          latlon: new google.maps.LatLng(provider_data.destination_addresses[0].location[0], provider_data.destination_addresses[0].location[1]),
          bearing: provider_data.bearing,
          order_id: provider_data._id,
          draggable: false,
          message: new google.maps.InfoWindow({
            content: info_window,
            maxWidth: 320,
          })
        });
        }
      })
    }
    this.order_info.forEach((loc_data, i) => {      
      let marker = new google.maps.Marker({
        position: loc_data.latlon,
        map: this.map,
        draggable: false,
        icon: "/assets/img/pins/order_destination.png"
      });
      this.order_markers.push(marker);
      google.maps.event.addListener(marker, 'click', function (e) {
        loc_data.message.open(this.map, marker);
        setTimeout(function () {
          loc_data.message.close();
        }, 5000);
      });
    });
  }
  
  get new_orders() : Array<any> {
    return this.orders.filter(_order => {
      if (_order.user_detail == null) {
          return false; 
      }
  
      if (_order.delivery_status != null) {
          return false; 
      }
  
      return (
          _order.user_detail.name.search(new RegExp(this.searchOrderText, "i")) != -1 ||
          _order.destination_addresses[0].address.search(new RegExp(this.searchOrderText, "i")) != -1 ||
          _order.unique_id.search(new RegExp(this.searchOrderText, "i")) != -1 ||
          (_order.provider_detail && _order.provider_detail.name.search(new RegExp(this.searchOrderText, "i")) != -1)
      );
  });
  
  }
  get ongoing_orders() : Array<any> {
    return this.orders.filter(_order => {
      if (_order.user_detail == null) {
          return false;
      }
  
      if (!(_order.delivery_status > this._helper.ORDER_STATE.DELIVERY_MAN_ACCEPTED &&
            _order.delivery_status < this._helper.ORDER_STATE.CANCELED_BY_USER)) {
          return false; 
      }
  
      return (
          _order.user_detail.name.search(new RegExp(this.searchOrderText, "i")) != -1 ||
          _order.destination_addresses[0].address.search(new RegExp(this.searchOrderText, "i")) != -1 ||
          _order.unique_id.search(new RegExp(this.searchOrderText, "i")) != -1 ||
          (_order.provider_detail && _order.provider_detail.name.search(new RegExp(this.searchOrderText, "i")) != -1)
      );
  });
  
  }
  get assign_orders() : Array<any> {
    return this.orders.filter(_order=>(_order.user_detail == null ? false :_order.delivery_status == this._helper.ORDER_STATE.DELIVERY_MAN_ACCEPTED || _order.delivery_status == this._helper.ORDER_STATE.WAITING_FOR_DELIVERY_MAN || _order.delivery_status == this._helper.ORDER_STATE.NO_DELIVERY_MAN_FOUND || _order.delivery_status == this._helper.ORDER_STATE.DELIVERY_MAN_REJECTED || _order.delivery_status == this._helper.ORDER_STATE.DELIVERY_MAN_CANCELLED || _order.delivery_status == this._helper.ORDER_STATE.STORE_CANCELLED_REQUEST || _order.delivery_status == this._helper.ORDER_STATE.ADMIN_CANCELLED_REQUEST) && (_order.user_detail.name.search(new RegExp(this.searchOrderText, "i")) != -1 || _order.destination_addresses[0].address.search(new RegExp(this.searchOrderText, "i")) != -1 || _order.unique_id.search(new RegExp(this.searchOrderText, "i")) != -1  || (_order.provider_detail ? _order.provider_detail.name.search(new RegExp(this.searchOrderText, "i")) != -1 : false)));
  }
  get free_providers() : Array<any> {
    if(this.request_delivery_type == DELIVERY_TYPE_CONSTANT.TAXI){
    return this.filtered_provider.filter(_provider=>_provider.is_online &&_provider.is_active_for_job &&_provider?.current_request?.length == 0 &&_provider.delivery_types.includes(this.request_delivery_type) &&(_provider.provider_type_id == null || _provider.is_business === true) && (_provider.first_name.search(new RegExp(this.searchProviderText, "i")) != -1 || _provider.last_name.search(new RegExp(this.searchProviderText, "i")) != -1 || _provider.phone.search(new RegExp(this.searchProviderText, "i")) != -1));
  }
    if(this.request_delivery_type == DELIVERY_TYPE_CONSTANT.SERVICE && this.request_delivery_type == DELIVERY_TYPE_CONSTANT.APPOINMENT){
      return this.filtered_provider.filter(_provider=>_provider.is_online &&_provider.is_active_for_job && (_provider?.current_request?.length == 0 || ( _provider?.current_request?.length>0 && _provider?.current_request[0]?.delivery_type == this.request_delivery_type)) &&_provider.delivery_types.includes(this.request_delivery_type) && (_provider.first_name.search(new RegExp(this.searchProviderText, "i")) != -1 || _provider.last_name.search(new RegExp(this.searchProviderText, "i")) != -1 || _provider.phone.search(new RegExp(this.searchProviderText, "i")) != -1));
    }
    return this.filtered_provider.filter(_provider=>_provider.is_online &&_provider.is_active_for_job &&_provider.delivery_types.includes(this.request_delivery_type) && (_provider?.current_request?.length == 0 || ( _provider?.current_request?.length>0 && _provider?.current_request[0]?.delivery_type == this.request_delivery_type)) && (_provider.first_name.search(new RegExp(this.searchProviderText, "i")) != -1 || _provider.last_name.search(new RegExp(this.searchProviderText, "i")) != -1 || _provider.phone.search(new RegExp(this.searchProviderText, "i")) != -1));
  }

  get on_job_providers() : Array<any> {
    return this.filtered_provider.filter(_provider=>_provider.is_active_for_job && _provider.is_in_delivery && (_provider.first_name.search(new RegExp(this.searchProviderText, "i")) != -1 || _provider.last_name.search(new RegExp(this.searchProviderText, "i")) != -1 || _provider.phone.search(new RegExp(this.searchProviderText, "i")) != -1))
  }

  get offline_providers() : Array<any> {
    return this.filtered_provider.filter(_provider=>(!_provider.is_online || !_provider.is_active_for_job) && (_provider.first_name.search(new RegExp(this.searchProviderText, "i")) != -1 || _provider.last_name.search(new RegExp(this.searchProviderText, "i")) != -1 || _provider.phone.search(new RegExp(this.searchProviderText, "i")) != -1))
  }

  onOrderDetail(order, event){
    if(event.target.tagName.toLowerCase() === "i"){
      this.OrderDetailModal.show(order._id, 1)
    }
  }

  ngOnDestroy() {
    let cla = document.getElementsByTagName('main')
    cla[0].style.marginLeft = null;
    cla[0].style.marginRight = null;
    if(this.interval){
      clearInterval(this.interval);
    }

    if(this.autoAssignModal.modalRef){
      this.autoAssignModal.onClose();
    }

    this._socket.removeAllListeners()
  }
 
  _initMap() {
    let theme  = localStorage.getItem('vien-themecolor');
    if(theme.includes('dark')){
      this.map = new google.maps.Map(document.getElementById('dispatch_map'), {
        zoom: 12,
        streetViewControl: false,
        center: { lat: this.map_center_lat_lng[0], lng: this.map_center_lat_lng[1] },
        styles : [
          {elementType: 'geometry', stylers: [{color: '#242f3e'}]},
          {elementType: 'labels.text.stroke', stylers: [{color: '#242f3e'}]},
          {elementType: 'labels.text.fill', stylers: [{color: '#746855'}]},
          {
            featureType: 'administrative.locality',
            elementType: 'labels.text.fill',
            stylers: [{color: '#d59563'}]
          },
          {
            featureType: 'poi',
            elementType: 'labels.text.fill',
            stylers: [{color: '#d59563'}]
          },
          {
            featureType: 'poi.park',
            elementType: 'geometry',
            stylers: [{color: '#263c3f'}]
          },
          {
            featureType: 'poi.park',
            elementType: 'labels.text.fill',
            stylers: [{color: '#6b9a76'}]
          },
          {
            featureType: 'road',
            elementType: 'geometry',
            stylers: [{color: '#38414e'}]
          },
          {
            featureType: 'road',
            elementType: 'geometry.stroke',
            stylers: [{color: '#212a37'}]
          },
          {
            featureType: 'road',
            elementType: 'labels.text.fill',
            stylers: [{color: '#9ca5b3'}]
          },
          {
            featureType: 'road.highway',
            elementType: 'geometry',
            stylers: [{color: '#746855'}]
          },
          {
            featureType: 'road.highway',
            elementType: 'geometry.stroke',
            stylers: [{color: '#1f2835'}]
          },
          {
            featureType: 'road.highway',
            elementType: 'labels.text.fill',
            stylers: [{color: '#f3d19c'}]
          },
          {
            featureType: 'transit',
            elementType: 'geometry',
            stylers: [{color: '#2f3948'}]
          },
          {
            featureType: 'transit.station',
            elementType: 'labels.text.fill',
            stylers: [{color: '#d59563'}]
          },
          {
            featureType: 'water',
            elementType: 'geometry',
            stylers: [{color: '#17263c'}]
          },
          {
            featureType: 'water',
            elementType: 'labels.text.fill',
            stylers: [{color: '#515c6d'}]
          },
          {
            featureType: 'water',
            elementType: 'labels.text.stroke',
            stylers: [{color: '#17263c'}]
          }
        ]
      });
    }else{
      this.map = new google.maps.Map(document.getElementById('dispatch_map'), {
        zoom: 12,
        center: { lat: this.map_center_lat_lng[0], lng: this.map_center_lat_lng[1] },
        draggable: true,
        zoomControl: true,
        scrollwheel: true,
        disableDoubleClickZoom: false,
        fullscreenControl: true
      });
      
    }
  }
}
