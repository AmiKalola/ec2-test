export let PER_PAGE_LIST = [5,10,15];

export let ORDER_STATE = {
    WAITING_FOR_ACCEPT_STORE: 1,
    CANCELED_BY_USER: 101,
    STORE_ACCEPTED: 3,
    STORE_REJECTED: 103,
    STORE_CANCELLED: 104,
    STORE_CANCELLED_REQUEST: 105,
    ADMIN_CANCELLED_REQUEST: 106,
    STORE_PREPARING_ORDER: 5,
    OREDER_READY: 7,
    WAITING_FOR_DELIVERY_MAN: 9,
    NO_DELIVERY_MAN_FOUND: 109,
    DELIVERY_MAN_ACCEPTED: 11,
    DELIVERY_MAN_REJECTED: 111,
    DELIVERY_MAN_CANCELLED: 112,
    DELIVERY_MAN_COMING: 13,
    DELIVERY_MAN_ARRIVED: 15,
    DELIVERY_MAN_PICKED_ORDER: 17,
    DELIVERY_MAN_STARTED_DELIVERY: 19,
    DELIVERY_MAN_ARRIVED_AT_DESTINATION: 21,
    DELIVERY_MAN_COMPLETE_DELIVERY: 23,
    ORDER_COMPLETED: 25,
    STORE_REJECTED_FOR_ECOMMERCE: 113
};

export let DELIVERY_TYPE = {
    STORE: 1,
    COURIER: 2,
    TABLE:3,
    TAXI:4,
    SERVICE:5,
    APPOINTMENT:6,
    ECOMMERCE: 7
}

export let DEFAULT_IMAGE_PATH = {
    PROMO_CODE: '/assets/img/default_images/promo_code.png',
    USER: '/assets/img/default_images/user.png',
    CATEGORY: '/assets/img/default_images/category.png',
    DRIVER: '/assets/img/default_images/driver.png'
}

export let CONSTANT = {
    EDIT: 1,
    DELETE: 2
}

export let SOCKET_TYPE = {
  ORDER: "admin_get_new_request_",
  DELIVERY: "delivery_get_new_request_",
  DISPATCH: "dispatch_get_new_request_"
}
