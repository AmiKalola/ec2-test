import { Component,  OnDestroy, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { UserDetailModalComponent } from 'src/app/containers/pages/user-detail-modal/user-detail-modal.component';
import { UserService, user_page_type } from '../../../../services/user.service'
import { Helper } from 'src/app/shared/helper';
import { Subscription } from 'rxjs';
import { Lightbox } from 'ngx-lightbox';
import { CommonService } from 'src/app/services/common.service';
import { USERS_PER_PAGE_LIST} from 'src/app/shared/constant';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit, OnDestroy {

  search_field: any =  { label: 'label-title.name', value: 'first_name' };
  search_value: string = '';
  itemOptionsPerPage = USERS_PER_PAGE_LIST
  itemsPerPage = 15
  is_clear_disabled:boolean = true;

  itemOptionsOrders = [
    { label: 'label-title.name', value: 'first_name', isShow : true},
    { label: 'label-title.phone', value: 'phone', isShow : true},
    { label: 'label-title.email', value: 'email', isShow : true},
    { label: 'label-title.country', value: 'country_details.country_name', isShow : true},
    { label: 'label-title.id', value: 'unique_id', isShow : true},
   ];

   filterLists = [
     {lable:'label-title.wallet-negative',value:'is_wallet_negative', checked: false},
     {lable:'label-title.document-expired',value:'is_document_expired', checked: false},
     {lable:'label-title.document-uploaded',value:'is_document_uploaded', checked: false},
     {lable:'label-title.email-verified',value:'is_email_verified', checked: false},
     {lable:'label-title.phone-verified',value:'is_phone_verified', checked: false}
   ]

   export_data_list: any[] = [
    {label: 'label-title.basic-details', value: ''},
    {label: 'label-title.name', value: 'name', is_checked: false},
    {label: 'label-title.email', value: 'email', is_checked: false},
    {label: 'label-title.phone', value: 'phone', is_checked: false},
    {label: 'label-title.basic-details', value: ''},
    {label: 'label-title.wallet', value: 'wallet', is_checked: false},
    {label: 'label-title.address', value: 'address', is_checked: false},
    {label: 'label-title.website-url', value: 'website_url', is_checked: false},
    {label: 'label-title.phone-verified', value: 'is_phone_verified', is_checked: false},
    {label: 'label-title.email-verified', value: 'is_email_verified', is_checked: false}
  ]
   is_all_selected: boolean = false;

  users: any = []
  is_approved = true
  user: any
  user_page_type: any = user_page_type;
  USER_DEFAULT_IMAGE = this._helper.DEFAULT_IMAGE_PATH.USER;
  USER_DEFAULT_SQUARE = this._helper.DEFAULT_IMAGE_PATH.USER_SQUARE;

  manualTrigger = false;

  count: number = 0;
  currentPage = 1;

  status: any = user_page_type.approved;
  userObservable: Subscription;
  walletObservable: Subscription;
  viewType: number= 1;
  approveModalConfig = {
    backdrop: true,
    ignoreBackdropClick: true,
  };
  approveModelRef: BsModalRef;
  selectedUser : any ;
  approvalStatus : any ;
  
  @ViewChild('approveTemplate', { static: true }) approveTemplate: TemplateRef<any>;
  @ViewChild('addNewModalRef', { static: true }) addNewModalRef: UserDetailModalComponent;

  constructor(public userService: UserService,
    public _helper: Helper,
    public lightBox: Lightbox,
    private _commonService: CommonService,private modalService : BsModalService) { }

  ngOnInit(): void {
    this.getUserList(user_page_type.approved)
    this.userObservable = this.userService._userObservable.subscribe((data) => {
      if(data){
        this.getUserList(this.status)
      }
    })
    this.walletObservable = this._commonService._providerObservable.subscribe((data) => {
      if(data){
        this.getUserList(this.status)
      }
    })
  }

  changeUserType(status) {
    this.currentPage = 1;
    this.status = status;
    this.getUserList(status)
  }


  clear_filter(){
    this.search_field =  { label: 'label-title.name', value: 'first_name' };
    this.search_value = '';
    this.filterLists.forEach((data:any)=>{
      data.checked=false;
    })
    this.getUserList(this.status)
    this.is_clear_disabled = true;
  }

  onChangeexportfilter(data){
    data.is_checked = !data.is_checked;
  }

  select_all_export_list(){
    this.is_all_selected = ! this.is_all_selected;
    this.export_data_list.forEach((data)=>{
      if(data.value){
        data.is_checked = this.is_all_selected;
      }
    })
  }

  export_excel_data(){
    let export_data_list = this.export_data_list.filter((x)=> {
      return x.is_checked
    });
    let filterLists = this.filterLists.filter((x)=>x.checked)
    this.userService.getUsers(this.status, 0, 0, this.search_field.value, this.search_value, filterLists).then(res_data => {
      res_data.users.forEach(user => {
        user.name = user.first_name + ' ' + user.last_name
        user.is_phone_verified = user.is_phone_number_verified
        if(user.address === null || user.address === 'null'){
          user.address = ''
        }
      });
      this._helper.downloadcsvFile(res_data.users || [], export_data_list)
    });
  }


  getUserList(status) {
    let filterLists = this.filterLists.filter((x)=>x.checked)

    this.userService.getUsers(status, this.currentPage, this.itemsPerPage, this.search_field.value, this.search_value, filterLists).then(res_data => {
      if(res_data){
        this.itemOptionsOrders.forEach((data) => {
          if(data.value == 'email' && res_data.is_show_email === false){
            data.isShow = false ;
          };
          if(data.value == 'phone' && res_data.is_show_phone === false){
            data.isShow = false ;
          }
        })
      }
      if (res_data.success) {
        this.users = res_data.users;
        this.count = res_data.count;
      } else {
        this.users = [];
        this.count = 0;
      }
    })
  }

  approveDecline(user, type){
    this.selectedUser = user ;
    this.approvalStatus = type ;
    this.approveModelRef = this.modalService.show(this.approveTemplate, this.approveModalConfig);
  }

  approve(){
    let data = {
      "user_page_type": this.approvalStatus === user_page_type.approved ? 1 : 2,
      user_id: this.selectedUser._id
    }
    
    this.userService.approveDeclineUser(data).then(res_data => {
      this.getUserList(this.status)
      this.approveModelRef.hide();
    })
  }

  cancel(){
    this.approveModelRef.hide();
  }

  pageChanged(event) {
    this.currentPage = event.page;
    this.getUserList(this.status)
  }

  showAddNewModal(event, user) {
    if(event.target.tagName.toLowerCase() !== 'button'){
      this.addNewModalRef.show(user);
    }
  }
  onChangeOrderBy(item): void  {
    this.search_field = item;
    this.is_clear_disabled = false;
  }

  onChangefilter(filter){
    filter.checked = !filter.checked;
    this.is_clear_disabled = false;
  }

  onChangeItemsPerPage(page){
    this.itemsPerPage = page
    this.getUserList(this.status)
  }

  onLightBox(user){
    let src = this._helper.image_url + user.image_url
    this.lightBox.open([{ src, thumb: '', downloadUrl: '' }], 0, { centerVertically: true, positionFromTop: 0, disableScrolling: true, wrapAround: true });
  }

  onViewChange(type) {
    this.viewType = type
  }

  ngOnDestroy(){
    this.walletObservable.unsubscribe()
    this.userObservable.unsubscribe()
    if (this.addNewModalRef.modalRef){
      this.addNewModalRef.onClose()
    }
  }

  updatedData(){
    this.getUserList(this.status)
  }
  apply(status){
    this.currentPage=1;
    this.getUserList(status)
  }
}
