import { Component, HostListener, OnDestroy, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import { AddDetailModalComponent } from 'src/app/containers/pages/add-detail-modal/add-detail-modal.component';
import { AddSubAdminModalComponent } from 'src/app/containers/pages/add-sub-admin-modal/add-sub-admin-modal.component';
import { SubAdminService } from 'src/app/services/sub-admin.service'
import { Helper } from 'src/app/shared/helper';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { SubAdminUrl } from 'src/app/shared/sub-admin-url';

@Component({
  selector: 'app-sub-admin',
  templateUrl: './sub-admin.component.html',
  styleUrls: ['./sub-admin.component.scss']
})
export class SubAdminComponent implements OnInit, OnDestroy {
  delete_admin: BsModalRef;
  sub_admin_url: BsModalRef;
  config = {
    backdrop: true,
    ignoreBackdropClick: true,
    class: 'modal-md modal-dialog-centered'
  };
  modalConfig = {
    backdrop: true,
    ignoreBackdropClick: true,
  };
  admin_list: any = []
  _subAdminSubscription: Subscription
  selected_admin: any = '';
  adminUrls: any = [];
  subadmin_url_list: any[] = [];
  admins_count: number = 0;
  itemOrder = { label: 'label-title.name', value: 'username' };
  search_string : string = '';
  is_clear_disabled : boolean = true;
 
  itemOptionsOrders = [
    { label: 'label-title.name', value: 'username' , isShow : true},
    { label: 'label-title.email', value: 'email' , isShow : true},
   ];

   temp_admin_list: any[] = []

  @ViewChild('subAdminModal', {static: true}) subAdminModal: AddSubAdminModalComponent;
  @ViewChild('addDetailModal', {static: true}) addDetailModal: AddDetailModalComponent
  constructor(private modalService: BsModalService,private _subAdminService: SubAdminService, private _helper: Helper,private _subAdminUrl:SubAdminUrl ) {
    this.adminUrls = this._subAdminUrl.ADMIN_URL
  }

  ngOnInit(): void {
    this._subAdminSubscription = this._subAdminService._subcategoryObservable.subscribe(() => {
      this.getAdmin()
    })
  }
 
  @HostListener('document:keyup', ['$event'])
  onKeyUp(event: KeyboardEvent) {
    if (event.key === 'Escape' || event.code === 'Escape') {
      if(this.sub_admin_url){
        this.sub_admin_url.onHidden.subscribe(() => {
          setTimeout(() => {
            this.subadmin_url_list = [];
            this.selected_admin = '';
          }, 100);
        })
      } 
    }
  }

  getAdmin(){
    this._helper.is_loading = true;

    this._subAdminService.getAdminList().then(res_data => {
      if(res_data.success){
        this.admin_list = res_data.admins
        this.temp_admin_list = this.admin_list;
        setTimeout(() => {
          if (this.admin_list) {
            this._helper.is_loading = false;
          }
        }, 1000);
        this.admins_count = this.admin_list.filter(x => x.admin_type === 1).length
      }
    })
  }
  showAddDetailModal(){
    this.addDetailModal.show();
  }
  onEdit(admin){
    this.selected_admin = admin
    this.showAdminModal()
    this.selected_admin = ''
  }

  showAdminModal(){
    this.subAdminModal.show(JSON.parse(JSON.stringify(this.selected_admin)))
  }

  ngOnDestroy(){
    this._subAdminSubscription.unsubscribe()
    if(this.subAdminModal.modalRef) {
      this.subAdminModal.onClose();
    }
    if(this.addDetailModal.modalRef) {
      this.addDetailModal.onClose()
    }

  }
  subAdminUrlModal(modal: TemplateRef<any>, admin) {
    this.sub_admin_url = this.modalService.show(modal, this.config);
    this.selected_admin = admin;
    admin.urls.forEach(url => {      
      this.adminUrls.forEach(admin_url => {
        if(url.url == admin_url.value){
          this.subadmin_url_list.push({
            url:admin_url.NAME,
            permission:url.permission
          })
        }
      })
    })
  }

  closeSubAdminUrlModal(){
    this.sub_admin_url.hide();
    setTimeout(() => {
    this.selected_admin = ''
      this.subadmin_url_list = [];
    }, 500);
  }

  onDelete(modal: TemplateRef<any>, admin) {
    this.delete_admin = this.modalService.show(modal, this.modalConfig);
    this.selected_admin = admin;
  }

  onDeleteAdmin(){
    let json:any = {adminId:this.selected_admin._id}
    this._subAdminService.deleteAdmin(json).then(res => {
      setTimeout(() => {
        this.selected_admin = '';
        this.delete_admin.hide();
      }, 500);
    })
  }


  apply() {
    const regex =  new RegExp(this.search_string, 'i');
    let value = this.itemOrder.value
    this.temp_admin_list = this.admin_list.filter((admin)=>{
      return admin[value].match(regex)
    })
  }

  clear_filter() {
    this.itemOrder = { label: 'label-title.name', value: 'username' };
    this.search_string  = '';
    this.is_clear_disabled = true;
    this.getAdmin();
  }

  onChangeOrderBy(item) {
    this.itemOrder = item
  }
}
