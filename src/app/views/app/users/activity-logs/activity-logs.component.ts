import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivityLogsModalComponent } from 'src/app/containers/pages/activity-logs-modal/activity-logs-modal.component';
import { Helper } from 'src/app/shared/helper';
import { ActivityLogsService } from '../../../../services/activity-logs.service'
import { PER_PAGE_LIST} from 'src/app/shared/constant';
@Component({
  selector: 'app-activity-logs',
  templateUrl: './activity-logs.component.html',
  styleUrls: ['./activity-logs.component.scss']
})
export class ActivityLogsComponent implements OnInit, OnDestroy {
  is_clear_disabled:boolean = true;
  logs: any = []
  search_value = ''
  selected_country = ''
  selected_city = ''
  selected_user_type = ''
  selected_api_type = ''
  selected_api_weight = ''
  selected_status = ''


  countries = [{label: '', value: ''}]
  cities = [{label: '', value: ''}]
  user = [
    {label: 'label-title.user', value: 7},
    {label: 'label-title.store', value: 2},
    {label: 'label-title.provider', value: 8}
  ]

  api_type = [
    {label: 'label-title.post', value: 'POST'},
    {label: 'label-title.get', value: 'GET'}
  ]

  api_weight = [
    {label: 'label-title.high', value: 1},
    {label: 'label-title.medium', value: 2},
    {label: 'label-title.low', value: 3},
  ]

  status = [
    {label: 'label-title.success', value: true},
    {label: 'label-title.error', value: false}
  ]

  itemOptionsPerPage = PER_PAGE_LIST
  itemsPerPage = 20
  currentPage = 1
  count = 0
  timezone_for_display_date:string = '';

  constructor(private activityLogsService: ActivityLogsService,public _helper:Helper) { }

  @ViewChild('activityLogModal', {static: true}) activityLogModal: ActivityLogsModalComponent

  ngOnInit(): void {
    this._helper.display_date_timezone.subscribe(data => {
      this.timezone_for_display_date = data;
    })
    this.getLogs()
  }

  getCountryList(){
    this.activityLogsService.get_country_list().then(res_data => {
      if(res_data.success){
        this.countries = []
        res_data.countries.forEach(country => {
            this.countries.push({label: country.country_name, value: country._id})
        });
      }
    })
  }

  getLogs(){
    let json = {
      number_of_record: this.itemsPerPage,
      page: this.currentPage,
      search_value: this.search_value,
      user_type: this.selected_user_type,
      api_type: this.selected_api_type,
      api_weight: this.selected_api_weight,
      status: this.selected_status
    }
    this.activityLogsService.get_activity_logs(json).then((activity_logs) => {
      this.logs = activity_logs.logs
      this.count = activity_logs.pages
      this.logs.forEach(log => {
        log.caller_type = parseInt(log.caller_type)
      });
    })
  }

  onShowOrderDetails(index){
    this.activityLogModal.show(this.logs[index])
  }

  onChangeItemsPerPage(count){
    this.itemsPerPage = count
    this.getLogs()
  }

  pageChanged(event) {
    this.currentPage = event.page
    this.getLogs()
  }

  onChangeCityType(city){
    this.selected_city = city.value
  }

  onChangeUserType(user){
    this.selected_user_type = user.value
    this.is_clear_disabled = false;
  }

  onChangeApiType(api){
    this.selected_api_type = api.value
    this.is_clear_disabled = false;
  }

  onChangeApiWeight(weight){
    this.selected_api_weight = weight.value
    this.is_clear_disabled = false;
  }

  onChangeStatusType(status){
    this.selected_status = status.value
    this.is_clear_disabled = false;
  }

  onSearch(){
    this.currentPage=1;
    this.is_clear_disabled = false;
    this.getLogs()
  }

  onSelectCountry(country){
    this.selected_country = country.value
    this.activityLogsService.get_city_list({country_id: country.value}).then(res_data => {
      if(res_data.success){
        this.cities = []
        res_data.cities.forEach(city => {
            this.cities.push({label: city.city_name, value: city._id})
        });
      }
    })
  }

  ngOnDestroy(): void {
    if (this.activityLogModal.modalRef) {
      this.activityLogModal.onSave()
    }
  }
  clear_filter(){
    this.search_value = '';
    this.selected_user_type = '';
    this.selected_api_type = '';
    this.selected_api_weight = '';
    this.selected_status = '';
    this.getLogs();
    this.is_clear_disabled = true;
  }
}
