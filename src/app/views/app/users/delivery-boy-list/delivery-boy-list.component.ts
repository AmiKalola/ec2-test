import { Component, EventEmitter, OnDestroy, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { AddDManInfoModalComponent } from 'src/app/containers/pages/add-d-man-info-modal/add-d-man-info-modal.component';
import { ProviderService, provider_page_type } from '../../../../services/delivery-boy.service'
import { Helper } from 'src/app/shared/helper';
import { Subscription } from 'rxjs';
import { CommonService } from 'src/app/services/common.service';
import { USERS_PER_PAGE_LIST} from 'src/app/shared/constant';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { LangService } from 'src/app/shared/lang.service';
import { StoreService } from 'src/app/services/store.service';
interface GetProvidersOptions {
  type: provider_page_type;
  page: number;
  itemsPerPage: number;
  search_field: string;
  search_value: string;
  filterLists: any; 
  selectedDeliveryType: any; 
  selectedMerchantType: number;
  selectedDeliveryId: any;
}
@Component({
  selector: 'app-delivery-boy-list',
  templateUrl: './delivery-boy-list.component.html',
  styleUrls: ['./delivery-boy-list.component.scss']
})



export class DeliveryBoyListComponent implements OnInit, OnDestroy {
  is_clear_disabled:boolean = true;
  search_field: any =  { label: 'label-title.name', value: 'first_name' };
  search_value: string = '';
  is_delivery_business: boolean = false;
  deliveries: any[] = [];
  business_type_list: any = [];

  itemOptionsOrders = [
    { label: 'label-title.name', value: 'first_name' , isShow : true},
    { label: 'label-title.phone', value: 'phone' , isShow : true},
    { label: 'label-title.email', value: 'email' , isShow : true},
    { label: 'label-title.country', value: 'country_details.country_name' , isShow : true},
    { label: 'label-title.city', value: 'city_details.city_name' , isShow : true}
   ];

   merchantFilterList = [
    { label: 'heading-title.all', value: 0},
    { label: 'heading-title.normal', value: 1},
    { label: 'label-title.store-partner', value: 2},
   ]

   filterLists = [
     {lable:'label-title.wallet-negative',value:'is_wallet_negative', checked: false},
     {lable:'label-title.document-expired',value:'is_document_expired', checked: false},
     {lable:'label-title.document-uploaded',value:'is_document_uploaded', checked: false},
     {lable:'label-title.email-verified',value:'is_email_verified', checked: false},
     {lable:'label-title.phone-verified',value:'is_phone_verified', checked: false}
   ]
  providers: any = []
  is_approved = true
  provider: any
  provider_page_type: any = provider_page_type;
  PROVIDER_DEFAULT_IMAGE = this._helper.DEFAULT_IMAGE_PATH.USER;

  count: number = 0;
  currentPage = 1;
  itemsPerPage = 15;
  selectedDeliveryId:any;

  status: any = provider_page_type.online;
  providerObservable: Subscription;
  walletObservable: Subscription;

  manualTrigger = false;

  export_data_list: any[] = [
    {label: 'label-title.basic-details', value: ''},
    {label: 'label-title.first-name', value: 'first_name', is_checked: false},
    {label: 'label-title.last-name', value: 'last_name', is_checked: false},
    {label: 'label-title.email', value: 'email', is_checked: false},
    {label: 'label-title.phone', value: 'phone', is_checked: false},
    {label: 'label-title.basic-details', value: ''},
    {label: 'label-title.wallet', value: 'wallet', is_checked: false},
    {label: 'label-title.phone-verified', value: 'is_phone_verified', is_checked: false},
    {label: 'label-title.email-verified', value: 'is_email_verified', is_checked: false},
  ]
  is_all_selected: boolean = false;

  viewType: string = 'grid';

  itemOrder = { label: 'Name', value: 'name' };
  itemOptionsPerPage = USERS_PER_PAGE_LIST;

  approveModalConfig = {
    backdrop: true,
    ignoreBackdropClick: true,
  };
  approveModelRef: BsModalRef;
  selectedUser : any ;
  approvalStatus : any ;


  selectedDeliveryType: any ;
  selectedMerchantType = { label: 'heading-title.all', value: 0};
  deliveries_type_list: any = [];
  
  @ViewChild('approveTemplate', { static: true }) approveTemplate: TemplateRef<any>;
  @ViewChild('addNewModalRef', { static: true }) addNewModalRef: AddDManInfoModalComponent;
  changeOrderBy: EventEmitter<any> = new EventEmitter();
  constructor(public providerService: ProviderService,
    private _lang: LangService,
    public storeService: StoreService,
    public _helper: Helper,
    private _commonService: CommonService,private modalService : BsModalService) { }

  ngOnInit(): void {
    this.getProviderList(provider_page_type.online)
    this.storeService.getDeliveryList().then(res_data => {
      this.deliveries = res_data.deliveries || []; 
      this.deliveries = this.deliveries.filter(d=>d.delivery_name.length > 0)
    });
    this.deliveries_type_list = this._helper.DELIVERY_TYPE
    
    this.providerObservable = this.providerService._providerObservable.subscribe((data) => {
      if(data){
        this.getProviderList(this.status)
      }
    })
    this.walletObservable = this._commonService._providerObservable.subscribe((data) => {
      if(data){
        this.getProviderList(this.status)
      }
    })
  }

  getDelivery(deliveryType) {
    this.selectedDeliveryId = null;
    this.selectedDeliveryType = deliveryType  
    if (deliveryType == this._helper.DELIVERY_TYPE_CONSTANT.APPOINMENT ||
      deliveryType == this._helper.DELIVERY_TYPE_CONSTANT.SERVICE) {
      this.is_delivery_business = true
      this.getDeliveryBusinessList(deliveryType);
    }else{
      this.is_delivery_business = false
      this.business_type_list = []
    } 
    this.is_clear_disabled = false;
  }

  getDeliveryBusinessList(deliveryType) {
    this.business_type_list = this.deliveries.filter((x) => x.delivery_type == deliveryType);
  }

  onViewChange(type) {
    this.viewType = type
  }

  changeProviderType(status) {
    this.currentPage = 1;
    this.status = status;
    this.getProviderList(status)
  }

  clear_filter(){
    this.search_field =  { label: 'label-title.name', value: 'first_name' };
    this.search_value = '';
    this.selectedDeliveryType = null;
    this.selectedDeliveryId = null;
    this.is_delivery_business = false;
    this.filterLists.forEach((data:any)=>{
      data.checked=false;
    })
    this.getProviderList(this.status)
    this.is_clear_disabled = true;
  }

  onChangeexportfilter(data){
    data.is_checked = !data.is_checked;
  }

  select_all_export_list(){
    this.is_all_selected = ! this.is_all_selected;
    this.export_data_list.forEach((data)=>{
      if(data.value){
        data.is_checked = this.is_all_selected;
      }
    })
  }

  export_excel_data(){
    let export_data_list = this.export_data_list.filter((x)=> {
      return x.is_checked
    });
    let filterLists = this.filterLists.filter((x)=>x.checked);
    const params : GetProvidersOptions = {
      type: this.status,
      page: 0,
      itemsPerPage: 0,
      search_field: this.search_field.value,
      search_value: this.search_value,
      filterLists: filterLists, 
      selectedDeliveryType: this.selectedDeliveryType,
      selectedMerchantType: this.selectedDeliveryType,
      selectedDeliveryId: this.selectedDeliveryId
  }
    this.providerService.getProviders(params).then(res_data => {
      this._helper.downloadcsvFile(res_data.providers || [], export_data_list)
    });
  }

  getProviderList(status) {
    let filterLists = this.filterLists.filter((x)=>x.checked)

    const params : GetProvidersOptions = {
      type: status,
      page: this.currentPage,
      itemsPerPage: this.itemsPerPage,
      search_field: this.search_field.value,
      search_value: this.search_value,
      filterLists: filterLists, 
      selectedDeliveryType: this.selectedDeliveryType,
      selectedMerchantType: this.selectedMerchantType.value,
      selectedDeliveryId: this.selectedDeliveryId
  }

    this.providerService.getProviders(params).then(res_data => {
      if(res_data){
        this.itemOptionsOrders.forEach((data) => {
          if(data.value == 'email' && res_data.is_show_email === false){
            data.isShow = false ;
          };
          if(data.value == 'phone' && res_data.is_show_phone === false){
            data.isShow = false ;
          }
        })
      }
      if (res_data.success) {
        this.providers = res_data.providers;
        this.count = res_data.count;
      } else {
        this.providers = [];
        this.count = 0;
      }
    })
  }

  approveDecline(user, type){
    this.selectedUser = user ;
    this.approvalStatus = type ;
    this.approveModelRef = this.modalService.show(this.approveTemplate, this.approveModalConfig);
  }

  approve(){
    let page_type;
    switch(this.approvalStatus){
      case provider_page_type.online:
        page_type = 1;
        break;
      case provider_page_type.approved:
        page_type = 2;
        break;
      case provider_page_type.blocked:
        page_type = 3;
        break;
    }
    let data = {
      "provider_page_type": page_type,
      provider_id: this.selectedUser._id
    }
    this.providerService.approveDeclineProvider(data).then(res_data => {
      this.getProviderList(this.status)
      this.approveModelRef.hide();
    })
  }

  cancel(){
    this.approveModelRef.hide();
  }

  pageChanged(event) {
    this.currentPage = event.page;
    this.getProviderList(this.status)
  }

  showAddNewModal(event, provider) {
    if(event.target.tagName.toLowerCase() !== 'button'){
      this.addNewModalRef.show(provider, this.deliveries);
    }
  }

  onChangeOrderBy(item): void  {
    this.search_field = item;
    this.is_clear_disabled = false;
  }

  onChangeMerchantBy(item:any){
    this.selectedMerchantType = item
  }

  onChangefilter(filter){
    filter.checked = !filter.checked;
    this.is_clear_disabled = false;
  }

  onChangeItemsPerPage(page){
    this.itemsPerPage = page
    this.getProviderList(this.status)
  }

  ngOnDestroy(){
    this.walletObservable.unsubscribe()
    this.providerObservable.unsubscribe()
    if (this.addNewModalRef.modalRef) {
      this.addNewModalRef.onClose()
    }
  }

  updatedData(){
    this.getProviderList(this.status)
  }
  apply(status){
    this.currentPage=1;
    this.getProviderList(status)
  }
}
