import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UsersComponent } from './users.component';
import { UserComponent } from './user/user.component';
import { DeliveryBoyListComponent } from './delivery-boy-list/delivery-boy-list.component';
import { StoreListComponent } from '../sub-stores/store-list/store-list.component';
import { SubAdminComponent } from './sub-admin/sub-admin.component';
import { ActivityLogsComponent } from './activity-logs/activity-logs.component';


const routes: Routes = [
  {
    path: '', component: UsersComponent,
    children: [
      { path: '', redirectTo: 'user', pathMatch: 'full' },
      { path: 'user', component: UserComponent, data:{auth: '/admin/user'} },
      { path: 'delivery-boy-list', component: DeliveryBoyListComponent, data: {auth: '/admin/delivery-boy-list'} },
      { path: 'store', component: StoreListComponent, data: {auth: '/admin/store'} },
      { path: 'sub-admin', component: SubAdminComponent, data: {auth: '/admin/sub-admin'} },
      { path: 'activity-logs', component: ActivityLogsComponent, data: {auth: '/admin/activity-logs'}}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsersRoutingModule { }
