import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsersRoutingModule } from './users-routing.module';
import { UsersComponent } from './users.component';
import { UserComponent } from './user/user.component';
import { DeliveryBoyListComponent } from './delivery-boy-list/delivery-boy-list.component';
// add for accordian and basic bs class
import { LayoutContainersModule } from 'src/app/containers/layout/layout.containers.module';
import { PagesContainersModule } from 'src/app/containers/pages/pages.containers.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { FormsModule as FormsModuleAngular, ReactiveFormsModule } from '@angular/forms';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ModalModule } from 'ngx-bootstrap/modal';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { RatingModule } from 'ngx-bootstrap/rating';
import { DirectivesModule} from 'src/app/directives/directives.module';
import { SubAdminComponent } from './sub-admin/sub-admin.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { ActivityLogsComponent } from './activity-logs/activity-logs.component';
import { CollapseModule } from 'ngx-bootstrap/collapse';


@NgModule({
  declarations: [UsersComponent, UserComponent, DeliveryBoyListComponent, SubAdminComponent, ActivityLogsComponent],
  imports: [
    CommonModule,
    UsersRoutingModule,
    NgSelectModule,
    DirectivesModule,
    SharedModule,
    LayoutContainersModule,
    CommonModule,
    PagesContainersModule,
    FormsModuleAngular,
    RatingModule.forRoot(),
    ReactiveFormsModule,
    CollapseModule,
    PaginationModule.forRoot(),
    TabsModule.forRoot(),
    ModalModule.forRoot(),
    BsDropdownModule.forRoot(),
    AccordionModule.forRoot(),
  ]
})
export class UsersModule { }
