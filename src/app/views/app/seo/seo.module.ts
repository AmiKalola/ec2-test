import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SeoRoutingModule } from './seo-routing.module';
import { ScriptComponent } from './script/script.component';
import { TagsComponent } from './tags/tags.component';
import { SeoComponent } from './seo.component';
import { PagesContainersModule } from 'src/app/containers/pages/pages.containers.module';
import { LayoutContainersModule } from 'src/app/containers/layout/layout.containers.module';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { SharedModule } from 'src/app/shared/shared.module';
import { TranslateModule } from '@ngx-translate/core';
import { DirectivesModule} from 'src/app/directives/directives.module';
import { TooltipModule } from 'ngx-bootstrap/tooltip';

@NgModule({
  declarations: [ScriptComponent, TagsComponent, SeoComponent],
  imports: [
    CommonModule,
    SeoRoutingModule,
    PagesContainersModule,
    LayoutContainersModule,
    TabsModule.forRoot(),
    TooltipModule,
    SharedModule,
    TranslateModule,
    DirectivesModule,
  ]
})
export class SeoModule { }
