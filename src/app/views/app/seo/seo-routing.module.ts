import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TagsComponent } from './tags/tags.component'
import { ScriptComponent } from './script/script.component'
import { SeoComponent } from './seo.component'

const routes: Routes = [{
  path: '', component: SeoComponent,
  children: [
    { path: '', redirectTo: 'tags', pathMatch: 'full' },
    { path: 'tags', component: TagsComponent, data: { auth: '/admin/seo/tags' } },
    { path: 'script', component: ScriptComponent, data: { auth: '/admin/seo/script' } },
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SeoRoutingModule { }
