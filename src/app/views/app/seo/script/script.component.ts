import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ScriptTagsModalComponent } from 'src/app/containers/pages/script-tags-modal/script-tags-modal.component';
import { AdminSettingService } from 'src/app/services/admin-setting.service';
import { Helper } from 'src/app/shared/helper';
@Component({
  selector: 'app-script',
  templateUrl: './script.component.html',
})
export class ScriptComponent implements OnInit, OnDestroy {
  scripts: Array<any> = []

  constructor(private _adminSettingsService: AdminSettingService, public _helper:Helper) { }

  @ViewChild('ScriptTagsModal', { static: true }) ScriptTagsModal: ScriptTagsModalComponent;

  ngOnInit(): void {
    this.getScripts()
  }

  getScripts(){
    this._adminSettingsService.getScriptTags().then(response => {
      if(response.success){
        this.scripts = response.scripts
      }
    })
  }

  onAddScript(){
    this.ScriptTagsModal.show()
  }

  ngOnDestroy(): void {
    if (this.ScriptTagsModal.modalRef) {
      this.ScriptTagsModal.onClose();
    }
  }

}
