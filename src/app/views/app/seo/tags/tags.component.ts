import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import { OpenGraphModalComponent } from 'src/app/containers/pages/open-graph-modal/open-graph-modal.component';
import { SeoTagsHeadModalComponent } from 'src/app/containers/pages/seo-tags-and-head-modal/seo-tags-head-modal.component';
import { TwitterGraphModalComponent } from 'src/app/containers/pages/twitter-graph-modal/twitter-graph-modal.component';
import { AdminSettingService } from 'src/app/services/admin-setting.service';
import { CommonService } from 'src/app/services/common.service';
import { Helper } from 'src/app/shared/helper';
import { LangService } from 'src/app/shared/lang.service';
@Component({
  selector: 'app-tags',
  templateUrl: './tags.component.html',
})
export class TagsComponent implements OnInit, OnDestroy {

  is_store: boolean = false
  tabType: number= 3;  //1: Home, 2: Store, 3: Info
  seoData: any = {
    seoTitleTag: '',
    deliveryListTitle: [],
    adsTitle: [],
    nearStoreList: [],
    offerList: [],
    ogTitle: [],
    ogType: '',
    ogUrl: '',
    ogImage: '',
    ogDescription: [],
    twitterCard: '',
    twitterUrl: '',
    twitterTitle: [],
    twitterImage: '',
    twitterDescription: ''
  };
  seo_subscription: Subscription
  constructor(private _commonService: CommonService,
    private _adminSettingsService: AdminSettingService,
    public _lang: LangService,
    public _helper:Helper) {
  }

  @ViewChild('SeoTagsHeadModal', { static: true }) SeoTagsHeadModal: SeoTagsHeadModalComponent;
  @ViewChild('OpenGraphModal', { static: true }) OpenGraphModal: OpenGraphModalComponent;
  @ViewChild('TwitterGraphModal', { static: true }) TwitterGraphModal: TwitterGraphModalComponent;

  ngOnInit(): void {
    this.seo_subscription = this._adminSettingsService._seoObservable.subscribe(() => {
      this.getSeoData(this.tabType)
    })
  }

  tabChanged(event){
    if(event === "store"){
      this.tabType = 2
      this.is_store = true
      this.getSeoData(this.tabType)
    } else if (event === "home") {
      this.tabType = 1
      this.is_store = false
      this.getSeoData(this.tabType)
    } else if (event === "info"){
      this.tabType = 3;
      this.is_store = false
      this.getSeoData(this.tabType)
    }
  }

  getSeoData(tabType){
    this._commonService.getSeoTags(tabType).then((result) => {
      if(result.success){
        this.seoData = result.data[0]
      }
    })
  }

  homePageEditSeoTagsAndHeadEdit(){
    this.SeoTagsHeadModal.show(this.seoData, this.tabType)
  }

  homePageOgGraphEdit(){
    this.OpenGraphModal.show(this.seoData, this.tabType)
  }

  homePageTwitterGraphEdit(){
    this.TwitterGraphModal.show(this.seoData, this.tabType)
  }

  ngOnDestroy(): void{
    this.seo_subscription.unsubscribe()
    if(this.SeoTagsHeadModal.modalRef){
      this.SeoTagsHeadModal.onClose();
    }
    if(this.OpenGraphModal.modalRef){
      this.OpenGraphModal.onClose()
    }
    if(this.TwitterGraphModal.modalRef){
      this.TwitterGraphModal.onClose()
    }
  }
}
