import { Component, OnInit, ViewChild } from '@angular/core';
import { AddNewItemModalComponent } from 'src/app/containers/pages/add-new-item-modal/add-new-item-modal.component';
import { AddNewSubCategoryModalComponent } from 'src/app/containers/pages/add-new-sub-category-modal/add-new-sub-category-modal.component';
import { CategoryService } from 'src/app/services/category.service';
import { CityService } from 'src/app/services/city.service';
import { CommonService } from 'src/app/services/common.service';
import { ItemModel, ItemService } from 'src/app/services/item.service';
import { ModifierService } from 'src/app/services/modifier.service';
import { SubcategoryService } from 'src/app/services/subcategory.service';
import { CHARGES_TYPE, DELIVERY_TYPE, DELIVERY_TYPE_CONSTANT } from 'src/app/shared/constant';
import { Helper } from 'src/app/shared/helper';
import { IProduct } from 'src/app/data/api.service';
import { Subscription } from 'rxjs';
import { LangService } from 'src/app/shared/lang.service';
import { environment } from 'src/environments/environment';
import { AddNewCategoryModalComponent } from 'src/app/containers/pages/add-new-category-modal/add-new-category-modal.component';
import { AddNewModifierModalComponent } from 'src/app/containers/pages/add-new-modifier-modal/add-new-modifier-modal.component';
import { CountryService } from 'src/app/services/country.service';

@Component({
  selector: 'app-ecommerce-service',
  templateUrl: './ecommerce-service.component.html',
  styleUrls: ['./ecommerce-service.component.scss']
})
export class EcommerceServiceComponent implements OnInit {
  @ViewChild('addNewSubCategoryRef', { static: true }) addNewSubCategoryModalRef: AddNewSubCategoryModalComponent;
  @ViewChild('addNewItemModalRef', { static: true }) addNewItemModalRef: AddNewItemModalComponent;
  @ViewChild('addNewCategoryRef', { static: true }) addNewCategoryModalRef: AddNewCategoryModalComponent;
  @ViewChild('addNewModifierModalRef', { static: true }) addNewModifierModalRef: AddNewModifierModalComponent;

  countries_list: any = [];
  seletedCountry : any = '';
  displayMode = 'image';
  CHARGES_TYPE = CHARGES_TYPE
  DELIVERY_TYPE_CONSTANT = DELIVERY_TYPE_CONSTANT
  DELIVERY_TYPE = DELIVERY_TYPE
  ITEM_DEFAULT_IMAGE = this._helper.DEFAULT_IMAGE_PATH.CATEGORY;
  CATEGORY_DEFAULT_IMAGE = this._helper.DEFAULT_IMAGE_PATH.CATEGORY;
  IMAGE_URL = environment.imageUrl;
  
  subcategorySubscription: Subscription;
  itemSubscription: Subscription;
  categorySubscription: Subscription;
  modifierSubscription: Subscription;

  citylist: any = [];
  city_search : any;
  selected_city: any;
  sequenceNumber: any[];
  subcategories: any;
  selectedSubcategory: any;
  items: any[];
  productArray: any;
  is_item: boolean = false;
  selected: IProduct[] = [];
  data: IProduct[] = [];
  selectAllState: string;
  modifiers: any;
  categories: any;
  is_store_can_add_category: any;
  selectedTabset: number = 0;

  constructor(public _helper: Helper,
    private _cityService : CityService,
    private _subcategoryService: SubcategoryService,
    private itemService: ItemService,
    private _categoryService: CategoryService,
    private _modifierService: ModifierService,
    private _lang: LangService,
    private _commonService: CommonService,
    private _countryService: CountryService) { }

  ngOnInit(): void {
    let json = {}
    this._helper.delivery_type = DELIVERY_TYPE_CONSTANT.ECOMMERCE
    this.getCountryList();
    this.subcategorySubscription = this._subcategoryService._subcategoryObservable.subscribe(data => {
      if (data) {
        this.loadSubcategoryList(this._helper.selected_store_id, this._helper.delivery_type)
      }
    })
    this.itemSubscription = this.itemService._itemObservable.subscribe((data) => {
      if (data) {
        this.loadItemList(false)
      }
    })
    this.categorySubscription = this._categoryService._categoryObservable.subscribe((data) => {
      if (data) {
        this.getCategoryList(this._helper.selected_store_id)
      }
    })
    this.modifierSubscription = this._modifierService._modifierObservable.subscribe((data) => {
      if (data) {
        this.getModifierList()
      }
    })
  }

  onSelectSubcategory(s): void {
    this.selectedSubcategory = s;
    this.loadItemList(false)
  }

  onSelect(item: IProduct): void {
    if (this.isSelected(item)) {
      this.selected = this.selected.filter(x => x.id !== item.id);
    } else {
      this.selected.push(item);
    }
    this.setSelectAllState();
  }
  setSelectAllState(): void {
    if (this.selected.length === this.data.length) {
      this.selectAllState = 'checked';
    } else if (this.selected.length !== 0) {
      this.selectAllState = 'indeterminate';
    } else {
      this.selectAllState = '';
    }
  }

  onContextMenuClick(action: string, item: ItemModel): void {
    switch (action) {
      case 'copy-item':
        this.addNewItemModalRef.show(false, null, this.selectedSubcategory, item);
        break;
      case 'delete-item':
        item.is_visible_in_store = !item.is_visible_in_store;
        this.itemService.update(item);
        break;
      case 'out-of-stock-item':
        item.is_item_in_stock = !item.is_item_in_stock;
        this.itemService.update(item);
        break;
      case 'delete-category':
        this._categoryService.delete(item._id)
        break;
      default:
        break;
    }
  }
  
  isSubcategorySelected(s): boolean {
    return s._id.toString() === this.selectedSubcategory ? this.selectedSubcategory.toString() : "";
  }
  
  isSelected(p: IProduct): boolean {
    return this.selected.findIndex(x => x.id === p.id) > -1;
  }

  async loadSubcategoryList(store_id, delivery_type) {
    this.sequenceNumber = []
    this._subcategoryService.list(store_id, delivery_type).then(data => {
      if (data.success) {
        this.subcategories = data.product_array;
        if (this.subcategories.length) {
          this.selectedSubcategory = this.subcategories[0]._id;
          this.loadItemList(false)
        } else {
          this.items = []
        }
      }

    })
  }

  loadItemList(boolean) {
    this.itemService.list(this.selectedSubcategory, this._helper.selected_store_id).then(data => {
      this.items = []
      this.productArray = data.products
      if (data.success) {
        if (data.products.length > 0) {
          let index = data.products.findIndex(_x => _x._id === this.selectedSubcategory)
          this.items = data.products[index].items
        } else {
          this.items = []
        }
      } else {
        this.items = []
      }
    })
  }

  // tab wise api calling
  tabSelection(tab){
    this.selectedTabset=tab;
    if(this.selected_city){
      this._initData(this.selected_city);
    }
  }

 _initData(city){
    this.selected_city = city
    this._helper.selected_city = city
    this._helper.selected_city_id = city?._id
   if (this.selected_city) {
    if(this.selectedTabset==0){
      this.loadSubcategoryList(this._helper.selected_store_id, this._helper.delivery_type);
    }else if(this.selectedTabset==1){
      this.getCategoryList(this._helper.selected_store_id);
    }else if(this.selectedTabset==2){
      this.getModifierList()
    }
  }
}

async getCategoryList(store_id) {
  this._categoryService.list(store_id).then(data => {
    if (data.success) {
      this.categories = data.product_groups;
    } else {
      this.categories = []
    }
  })
}

async getModifierList(){
  this._modifierService.list_group().then(data => {
    if (data.success) {
      this.modifiers = data.specification_group
    }
    else {
      this.modifiers = []
    }
  })
}

  showAddNewSubCategoryModal(is_edit = false, edit_id = null): void {
    this.addNewSubCategoryModalRef.show(is_edit, edit_id);
  }

  showAddNewItemModal(is_edit = false, edit_id = null): void {
    if(this._helper.has_permission(this._helper.PERMISSION.ADD) && !is_edit){     
      this.addNewItemModalRef.show(is_edit, edit_id, this.selectedSubcategory);
    }
    if(this._helper.has_permission(this._helper.PERMISSION.EDIT) && is_edit){     
      this.addNewItemModalRef.show(is_edit, edit_id, this.selectedSubcategory);
    }      
    this.is_item = true
  }

  showAddNewCategoryModal(is_edit = false, edit_id = null): void {
    if(this._helper.has_permission(this._helper.PERMISSION.ADD) && !is_edit){     
      this.addNewCategoryModalRef.show(is_edit, edit_id, this.categories.length);
    }
    if(this._helper.has_permission(this._helper.PERMISSION.EDIT) && is_edit && this.is_store_can_add_category){     
      this.addNewCategoryModalRef.show(is_edit, edit_id, this.categories.length);
    }
  }

  showAddNewModifierModal(is_edit = false, edit_id = null): void {
    this.addNewModifierModalRef.show(is_edit, edit_id);
  }
  ngOnDestroy() {
    this._helper.selected_city = {_id:null,city_name:[]}
    this._helper.delivery_type = null
    this._helper.selected_store_id = ''
    this._helper.selected_store = null
    if (this.subcategorySubscription) {
      this.subcategorySubscription.unsubscribe()
    }
    this._helper.clear_height_width();
    this._helper.setDeliveryPrice(null,null);
  }
  
  getCountryList() {
    this._countryService.fetch().then(res => {
      if (res.success) {
        this.countries_list = res.countries;
        if(this.countries_list.length>0){
          if(this.countries_list.length>=2){
            this.seletedCountry = 'All';
          }else{
            this.seletedCountry = this.countries_list[0]._id;
          }
          this.onChangeCountry(this.seletedCountry);
        }
        if(this.countries_list.length==0){
          this.seletedCountry = null;
        }
      }else{
        this.countries_list = []
      }
    })
  }

  onChangeCountry(countryId) {
    this.seletedCountry = countryId
    let json = { country_id: this.seletedCountry }
    this._cityService.getEcommerceCountryWiseCityList(json).then(res => {
      if (res.success) {
        this.selected_city= null;
        this.citylist = res.citylist
        this._helper.selected_store_id = res.delivery_details._id
        this.is_store_can_add_category = res.delivery_details.is_store_can_create_group
      }
    })
  }

}
