import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EcommerceServiceComponent } from './ecommerce-service.component';

describe('EcommerceServiceComponent', () => {
  let component: EcommerceServiceComponent;
  let fixture: ComponentFixture<EcommerceServiceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EcommerceServiceComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EcommerceServiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
