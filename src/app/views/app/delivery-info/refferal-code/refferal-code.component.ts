import { Component, OnInit, ViewChild } from '@angular/core';
import { ReferralService } from '../../../../services/referral.service';
import { USER_TYPE } from '../../../../shared/constant';
import { Helper } from 'src/app/shared/helper';
import { LangService } from 'src/app/shared/lang.service';
import { ReferralCodeModalComponent } from 'src/app/containers/pages/referral-code-modal/referral-code-modal.component';

@Component({
  selector: 'app-refferal-code',
  templateUrl: './refferal-code.component.html',
  styleUrls: ['./refferal-code.component.scss']
})
export class RefferalCodeComponent implements OnInit {

  user_referral_list = []
  provider_referral_list = []
  store_referral_list = []
  USER_TYPE = USER_TYPE
  @ViewChild('referralModalRef', { static: true }) referralModalRef: ReferralCodeModalComponent;

  constructor(private _referralService:ReferralService, public _helper: Helper, public _lang: LangService) { }

  ngOnInit(): void {
    this.getUserReferral()
    this.getProviderReferral()
    this.getStoreReferral()
  }

  getUserReferral(){
    let json = {
      user_type:USER_TYPE.USER
    }
    this._referralService.getRefferralHistory(json).then(res=>{
      if(res.success){
        this.user_referral_list = res.referral_codes.filter(x => x.referred_user_detail[0] != undefined)
        this.user_referral_list.forEach(referral => {
          referral.collapse = true
        });
      }
    })
  }

  getProviderReferral(){
    let json = {
      user_type:USER_TYPE.PROVIDER,
      is_show_error_toast: false
    }
    this._referralService.getRefferralHistory(json).then(res=>{
      if(res.success){
        this.provider_referral_list = res.referral_codes.filter(x => x.referred_provider_detail[0] != undefined)
        this.provider_referral_list.forEach(referral => {
          referral.collapse = true
        });
      }
    })
  }

  getStoreReferral(){
    let json = {
      user_type:USER_TYPE.STORE,
      is_show_error_toast: false
    }
    this._referralService.getRefferralHistory(json).then(res=>{
      if(res.success){
        this.store_referral_list = res.referral_codes  
        this.store_referral_list = this.store_referral_list.filter(x => x.referred_store_detail[0] != undefined)
        this.store_referral_list.forEach(referral => {
          referral.collapse = true
        });
      }
    })
  }

  onCollapse(user_referral){
    user_referral.collapse = !user_referral.collapse
  }

  showReferralModal(referral, type){
    this.referralModalRef.show(referral, type);
  }
}
