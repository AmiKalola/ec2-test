import { Component, OnDestroy, OnInit, ViewChild ,TemplateRef} from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { AddNewWalletRequestComponent } from 'src/app/containers/pages/add-new-wallet-request/add-new-wallet-request.component';
import { WalletService } from 'src/app/services/wallet.service'
import { Helper } from 'src/app/shared/helper';
import { LangService } from 'src/app/shared/lang.service';
import { PER_PAGE_LIST } from 'src/app/shared/constant';
@Component({
  selector: 'app-wallet-request',
  templateUrl: './wallet-request.component.html',
  styleUrls: ['./wallet-request.component.scss']
})
export class WalletRequestComponent implements OnInit, OnDestroy {
  search_field: any =  { label: 'label-title.delivery-man', value: 'provider_detail.first_name' };
  wallet_requests: any = [];
  WALLET_REQUEST_STATUS: any;
  modelRef: BsModalRef;
  amount: number;
  selected_request: any;
  items_per_page = 20;
  item_options_per_page = PER_PAGE_LIST;
  current_page = 1;
  itemOrder = { label: 'label-title.delivery-man', value: 'provider_detail.first_name' }
  search_value: any = ''
  amount_error: boolean = false;
  is_clear_disabled:boolean=true;
  total_page: number = 0;
  timezone_for_display_date:string = '';
  user_Type_List:any
  wallet = 0

  itemOptionsOrders = [
    { label: 'label-title.delivery-man', value: 'provider_detail.first_name' },
    { label: 'label-title.store', value: 'store_detail.name' },
  ];
  @ViewChild('addNewModalRef', { static: true }) addNewModalRef: AddNewWalletRequestComponent;
  @ViewChild('template', {static: true}) template: TemplateRef<any>;   

  constructor(private _walletService: WalletService, 
    public _helper: Helper, 
    private modalService: BsModalService, 
    private _lang: LangService,
    private _trans: TranslateService) { }

  ngOnInit(): void {

    this._helper.display_date_timezone.subscribe(data => {
      this.timezone_for_display_date = data;
    })

    this.WALLET_REQUEST_STATUS = this._helper.WALLET_REQUEST_STATUS
    this.user_Type_List = this._helper.USER_TYPE;
    this.getWalletRequest()
  }

  getWalletRequest(){
    let json = {
      end_date: "",
      page: this.current_page,
      search_field: this.search_field.value,
      search_value: this.search_value,
      sort_field: "unique_id",
      sort_wallet_request: -1,
      start_date: "",
      number_of_rec:this.items_per_page
    }
    this._walletService.get_wallet_requests(json).then(res_data => {
      if(res_data.success){
        this.wallet_requests = res_data.wallet_request
        this.total_page = res_data.pages
      }else{
        this.wallet_requests = [];
        this.total_page = 0;
      }
    })
  }

  acceptWalletRequest(id, status){
    let json = {
      id, 
      wallet_status: status
    }
    this._walletService.approve_wallet_request_amount(json).then(res_data => {
      if(res_data.success){
        this.getWalletRequest()
      }
    })
  }

  cancleWalletRequest(id, status){
    let json = {
      id, 
      wallet_status: status,
      type: 1
    }
    this._walletService.cancle_wallet_request(json).then(res_data => {
      if(res_data.success){
        this.getWalletRequest()
      }
    })
  }

  completeWalletRequest(id, status){
    let json = {
      id, 
      wallet_status: status
    }
    this._walletService.complete_wallet_request(json).then(res_data => {
      if(res_data.success){
        this.getWalletRequest()
      }
    })
  }

  onSend(request){
    this.selected_request = request
    this.amount = request.requested_wallet_amount
    this.modelRef = this.modalService.show(this.template);

  }

  onSave(){
    let json = {
      wallet_request_id: this.selected_request._id,
      approved_requested_wallet_amount: this.amount
    }
    if(!isNaN(this.amount)){
      this.amount_error = false
      this._walletService.transfer_wallet_request_amount(json).then(res_data => {
        if(res_data.success){
          this.getWalletRequest()
          this.modelRef.hide()
        } else if (res_data.wallet) {
          this.wallet = res_data.wallet
        }
      })
    } else {
      this.amount_error = true
    }
  }

  onClose(){
    this.modelRef.hide()
  }

  onChangeOrderBy(item): void  {
    this.search_field = item;
    this.is_clear_disabled=false;
  }
  
  pageChanged(event){
    this.current_page = event.page
    this.getWalletRequest()
  }

  onChangeItemsPerPage(item){
    this.items_per_page = item
    this.getWalletRequest()
  }

  showAddNewModal(id, wallet_request): void {
    this.addNewModalRef.show(id, wallet_request);
  }

  onSearch(){
    this.current_page=1;
    this.is_clear_disabled=false;
    this.getWalletRequest()
  }

  clickonExport(){
    let json = {
      end_date: "",
      page: this.current_page,
      search_field: this.search_field.value,
      search_value: this.search_value,
      sort_field: "unique_id",
      sort_wallet_request: -1,
      start_date: "",
      number_of_rec:this.items_per_page
    }
    let requests = []
    this._walletService.get_wallet_requests(json).then(res_data => {
      if(res_data.success){
        this.wallet_requests = res_data.wallet_request
        this.total_page = res_data.pages

        this.wallet_requests.forEach(request => {
          let name = ''
          let user_type
          if(request.user_type === 8){
            name = request.provider_detail[0].first_name + ' ' + request.provider_detail[0].last_name
            user_type = this._trans.instant('label-title.provider')
          } else if(request.user_type === 2){
            name = request.store_detail[0].name[this._lang.selectedlanguageIndex]
            user_type = this._trans.instant('label-title.store')
          }
          requests.push({
            unique_id : request.unique_id,
            date: request.updated_at,
            currency_code: request.wallet_currency_code,
            name,
            total_wallet_amount: request.total_wallet_amount,
            requested_wallet_amount: request.requested_wallet_amount,
            description: request.description_for_request_wallet_amount,
            wallet_status: request.wallet_status,
            user_type,
            approved_requested_wallet_amount: request.approved_requested_wallet_amount
          })
        });
        this._helper.export_csv(requests, Object.keys(requests[0]), 'wallet requests')
      }
    })
  }

  ngOnDestroy(): void {
    if(this.addNewModalRef.modalRef){
      this.addNewModalRef.onClose()
    }
    if(this.modelRef){
      this.modelRef.hide()
    }
  }
  clear_filter(){
    this.itemOrder = { label: 'label-title.delivery-man', value: 'provider_detail.first_name' }
    this.current_page = 1;
    this.items_per_page  = 20; 
    this.search_value = '';
    this.search_field =  { label: 'label-title.delivery-man', value: 'provider_detail.first_name' };
    this.wallet_requests = []
    this.getWalletRequest()
    this.is_clear_disabled = true;
  }
}
