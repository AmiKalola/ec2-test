import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { WalletRequestComponent } from './wallet-request.component';

describe('WalletRequestComponent', () => {
  let component: WalletRequestComponent;
  let fixture: ComponentFixture<WalletRequestComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ WalletRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WalletRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
