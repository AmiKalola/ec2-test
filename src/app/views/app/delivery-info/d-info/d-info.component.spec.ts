import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DInfoComponent } from './d-info.component';

describe('DInfoComponent', () => {
  let component: DInfoComponent;
  let fixture: ComponentFixture<DInfoComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ DInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
