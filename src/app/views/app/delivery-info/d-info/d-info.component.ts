import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { UntypedFormArray, UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { AddDeliveryTagModalComponent } from 'src/app/containers/pages/add-delivery-tag-modal/add-delivery-tag-modal.component';
import { AddNewDInfoModalComponent } from 'src/app/containers/pages/add-new-d-info-modal/add-new-d-info-modal.component';
import { EditDeliveryTypeComponent } from 'src/app/containers/pages/edit-delivery-type/edit-delivery-type.component';
import { ImageCropModelComponent } from 'src/app/containers/pages/image-crop-model/image-crop-model.component';
import { AdminSettingService } from 'src/app/services/admin-setting.service';
import { CommonService } from 'src/app/services/common.service';
import { DeliveryService } from 'src/app/services/delivery.service'
import { NotifiyService } from 'src/app/services/notifier.service';
import { DELIVERY_TYPE, DELIVERY_TYPE_CONSTANT , DELIVERY_TYPE_DEFAULT_IMAGE } from 'src/app/shared/constant';
import { Helper } from 'src/app/shared/helper';
import { LangService } from 'src/app/shared/lang.service';
import { environment } from 'src/environments/environment';

declare let jQuery: any;

class Delivery {
  created_at: Date = new Date();
  delivery_name: Array<string> = [];
  delivery_type: number = 0;
  description: Array<string> = [];
  famous_products_tags: Array<any> = []
  icon_url: string = ""
  image_url: string = ""
  default_image_url: string = ""
  banner_url:string = ""
  is_business: boolean = false
  is_store_can_create_group:boolean = false
  is_store_can_edit_order: boolean =false
  is_provide_table_booking: boolean =false
  map_pin_url: string =""
  sequence_number:number = 0
  unique_id:number = 7
  is_admin_service_allow :boolean= false
  updated_at: Date = new Date();
  _id:string = "608a8312282359265bdcc37f"
}

@Component({
  selector: 'app-d-info',
  templateUrl: './d-info.component.html',
  styleUrls: ['./d-info.component.scss']
})
export class DInfoComponent implements OnInit, OnDestroy {
  languages: any = [];
  delivery_list: Array<Delivery> = [];
  filter_delivery_list: Array<Delivery> = [];
  IMAGE_URL = environment.imageUrl;
  image_settings: any;
  delivery_image_max_height: any;
  delivery_image_min_height: any;
  delivery_image_max_width: any;
  delivery_image_min_width: any;
  delivery_image_ratio: any;
  delivery_icon_maximum_size: any;
  delivery_icon_minimum_size: any;
  delivery_icon_ratio: any;
  deliveryForm: UntypedFormGroup;
  delivery_type = DELIVERY_TYPE;
  DELIVERY_TYPE_CONSTANT = DELIVERY_TYPE_CONSTANT;
  DELIVERY_TYPE_DEFAULT_IMAGE = DELIVERY_TYPE_DEFAULT_IMAGE;
  favourite_tags: any = [];
  deleted_favourite_tags: any = [];
  image_url: any = '';
  default_image_url: any;
  icon_url: any = '';
  banner_url: any = '';
  is_edit: boolean = false;
  image_type: number;
  delivery_imagefile: any;
  default_imagefile: any;
  banner_imagefile:any;
  icon_imagefile: any;
  form_data: FormData;
  upload_delivery_imageurl: any = '';
  default_delivery_imageurl: any = '';
  upload_icon_imageurl: any = '';
  upload_banner_imageUrl:any = "";
  is_image_updated: boolean = false;
  is_default_image_updated: boolean = false;
  is_banner_image_updated:boolean = false;
  is_icon_updated: boolean = false;
  selected_delivery_id: any;
  DEFAULT_IMAGE_PATH: any;
  delivery_search: any =''
  tag_index:number=null;
  delivery_types:any=[];
  delivery_subscriber: Subscription
  languages_subscription: Subscription

  business_type:number = 0;
  businessTypeList = [
    { type:0, name: 'label-title.all'},
    { type:DELIVERY_TYPE_CONSTANT.STORE, name: 'label-title.store'},
    { type:DELIVERY_TYPE_CONSTANT.COURIER, name: 'label-title.courier'},
    { type:DELIVERY_TYPE_CONSTANT.TAXI, name: 'label-title.taxi'},
    { type:DELIVERY_TYPE_CONSTANT.APPOINMENT, name: 'label-title.appoinment'},
    { type:DELIVERY_TYPE_CONSTANT.SERVICE, name: 'label-title.service'},
    { type:DELIVERY_TYPE_CONSTANT.ECOMMERCE, name: 'label-title.ecommerce'},
  ]

  @ViewChild('cropModel', { static: true }) cropModel: ImageCropModelComponent;
  @ViewChild('addNewModalRef1', { static: true }) addNewModalRef1: AddNewDInfoModalComponent;
  @ViewChild('deliveryTagModal', {static: true}) deliveryTagModal: AddDeliveryTagModalComponent 
  @ViewChild('editDeliveryType', { static: true }) editDeliveryType: EditDeliveryTypeComponent;
  selected_delivery_type: any;

  constructor(private _adminSettingService: AdminSettingService,
    private _deliveryService: DeliveryService,
    private _commonService: CommonService,
    private _lang: LangService,
    public _helper: Helper,
    private notifications: NotifiyService
    ) {
      this.DEFAULT_IMAGE_PATH = this._helper.DEFAULT_IMAGE_PATH
    }

  ngOnInit(): void {
    this.form_data = new FormData;
    this._initForm()
    this.delivery_subscriber = this._deliveryService._deliveryObservable.subscribe(() => {
      this.get_admin_setting()
      this.get_delivery_list()
      this.get_delivery_type()
      this.get_image_settings()
    })
    if(!this._helper.has_permission(this._helper.PERMISSION.EDIT)){
      this.deliveryForm.disable();
    }
  }

  showBusinessType(): void {
    if(!this._helper.has_permission(this._helper.PERMISSION.ADD) && (!this._helper.has_permission(this._helper.PERMISSION.EDIT) || this._helper.has_permission(this._helper.PERMISSION.EDIT))){
      setTimeout(() => {
        let elements :any = document.getElementsByTagName('form')[0]?.elements;
        for(const element of elements){
          if(element.tagName != 'BUTTON'){
            element.setAttribute('disabled' , 'true')
          }
        }
      }, 300);
    }
  }

  
  get_delivery_type(){
    this._deliveryService.get_delivery_type().then(res_data => {
      if(res_data.success){
        this.delivery_types = res_data.delivery_type;
      }
    })
  }
  
  select_delivery_type(deliverytype){
    if(this._helper.has_permission(this._helper.PERMISSION.EDIT)){     
      this.editDeliveryType.show(deliverytype)
    }
  }

  _initForm(){
    this.deliveryForm = new UntypedFormGroup({
      delivery_name: new UntypedFormArray([]),
      description: new UntypedFormArray([]),
      delivery_type: new UntypedFormControl(null, Validators.required),
      sequence_number: new UntypedFormControl(null, Validators.required),
      is_store_can_create_group: new UntypedFormControl(false, Validators.required),
      is_store_can_edit_order: new UntypedFormControl(false, Validators.required),
      is_business: new UntypedFormControl(false, Validators.required),
      is_provide_table_booking: new UntypedFormControl(false, Validators.required),
      famous_products_tags: new UntypedFormControl([]),
      theme_number: new UntypedFormControl(1, Validators.required),
      is_admin_service_allow:new UntypedFormControl(false),
    })
    if(this._lang.supportedLanguages.length == 0){
      this.languages_subscription =  this._lang.languagesObservable.subscribe(() => {
        this._lang.supportedLanguages.forEach(_language=>{
          this.delivery_name.push(new UntypedFormControl("",_language.code === 'en' ? Validators.required : null))
          this.description.push(new UntypedFormControl(""))
        })
      })
    }else{
      this._lang.supportedLanguages.forEach(_language=>{
        this.delivery_name.push(new UntypedFormControl("",_language.code === 'en' ? Validators.required : null))
        this.description.push(new UntypedFormControl(""))
      })
    }

  }

  get_admin_setting(){
    let notification = true
    this._adminSettingService.fetchAdminSetting(notification).then(res_data => {
      if(res_data.success){
        this.languages = res_data.setting.lang
      }
    })
  }

  get_delivery_list(){
    this._deliveryService.get_delivery_list().then(res_data => {
      if(res_data.success){
        this.delivery_list = res_data.deliveries;
        this.filter_delivery_list = res_data.deliveries;
        if(this.selected_delivery_id){
          let index = this.delivery_list.findIndex((x:any) => x._id == this.selected_delivery_id)
          this.onDelivery(res_data.deliveries[index])
        }else{
        if(this.delivery_list.length>0){
          this.onDelivery(res_data.deliveries[0])
        }
        }
        this.delivery_search = '';
        this.business_type = 0;
      }
    })
  }

  get_image_settings(){
    this._commonService.fetch_image_settings().then(res_data => {
      this.image_settings = res_data.image_setting;
      this.delivery_image_max_height = this.image_settings.delivery_image_max_height;
      this.delivery_image_min_height = this.image_settings.delivery_image_min_height;
      this.delivery_image_max_width = this.image_settings.delivery_image_max_width;
      this.delivery_image_min_width = this.image_settings.delivery_image_min_width;
      this.delivery_image_ratio = this.image_settings.delivery_image_ratio;
      this.delivery_icon_maximum_size = this.image_settings.delivery_icon_maximum_size;
      this.delivery_icon_minimum_size = this.image_settings.delivery_icon_minimum_size;
      this.delivery_icon_ratio = this.image_settings.delivery_icon_ratio;
    })
  }

  get delivery_name(){
    return this.deliveryForm.get('delivery_name') as UntypedFormArray
  }

  get description(){
    return this.deliveryForm.get('description') as UntypedFormArray
  }

  onSelectImageFile(event, type) {  //1: Vehicle, 2: Map-pin
    this.image_type = type
    let files = event.target.files;
    let aspectRatio;
    let resizeToWidth;
    if (this.image_type == 3) {
      aspectRatio = 3.2;
      resizeToWidth = this.image_settings.ads_fullscreen_image_max_width;
      if (files.length === 0)
        return;
      const mimeType1 = files[0].type;
      let fileType = this._helper.uploadFile.filter((element) => {
        return mimeType1 == element;
      })
      if (mimeType1 != fileType) {
        this.notifications.showNotification('error',this._helper._trans.instant('validation-title.invalid-image-format'));
        return;
      }
      this.cropModel.imageChangedEvent = event;
      this.cropModel.show(aspectRatio, resizeToWidth);
    }
    else {
      aspectRatio = 1;
      resizeToWidth = this.image_settings.delivery_image_max_width;
      if (files.length === 0)
        return;
      const mimeType = files[0].type;
      let fileType=this._helper.uploadFile.filter((element)=> {
        return mimeType==element;
      })
      if (mimeType != fileType) {
        this.notifications.showNotification('error',this._helper._trans.instant('validation-title.invalid-image-format'));
        return;
      }
      this.cropModel.imageChangedEvent = event;
      this.cropModel.show(aspectRatio, resizeToWidth);
    }

  }

  imageCropped(event) {
    const reader = new FileReader();
    reader.readAsDataURL(event);
    if (this.image_type === 1) {
      this.is_image_updated = true
      this.delivery_imagefile = event;
      this.form_data.append('image_url', this.delivery_imagefile)
      reader.onload = (_event) => {
        this.upload_delivery_imageurl = reader.result
      }
    }
    else if (this.image_type === 4) {
      this.is_default_image_updated = true
      this.default_imagefile = event;
      this.form_data.append('default_image_url', this.default_imagefile)
      reader.onload = (_event) => {
        this.default_delivery_imageurl = reader.result
      }
    }
    else if(this.image_type === 3)
    {
      this.is_banner_image_updated = true
      this.banner_imagefile = event;
      this.form_data.append('banner_url', this.banner_imagefile)
      reader.onload = (_event) => {
        this.upload_banner_imageUrl = reader.result
      }
    } else {
      this.is_icon_updated = true
      reader.onload = (_event) => {
        this.icon_imagefile = event;
        this.form_data.append('icon_url', this.icon_imagefile)
        this.upload_icon_imageurl = reader.result
      }
    }
  }

  showAddNewModal(data, index): void {
    if(data){
      this.tag_index = index;
      this.deliveryTagModal.show(data)
    } else {
      this.tag_index = null;
      this.deliveryTagModal.show(null)
    }

  }
  showAddDInfoModal(): void {
    let NEW_DELIVERY_TYPE = [...DELIVERY_TYPE]
    if(this.delivery_list.findIndex((x)=>x.delivery_type==DELIVERY_TYPE_CONSTANT.COURIER)!==-1){
      let index = NEW_DELIVERY_TYPE.findIndex((x)=>x.value===2)
      NEW_DELIVERY_TYPE.splice(index, 1);
    };
    if(this.delivery_list.findIndex((x)=>x.delivery_type==DELIVERY_TYPE_CONSTANT.TAXI)!==-1){
      let index = NEW_DELIVERY_TYPE.findIndex((x)=>x.value===4)
      NEW_DELIVERY_TYPE.splice(index, 1);
    };
    if(this.delivery_list.findIndex((x)=>x.delivery_type==DELIVERY_TYPE_CONSTANT.ECOMMERCE)!==-1){
      let index = NEW_DELIVERY_TYPE.findIndex((x)=>x.value===7)
      NEW_DELIVERY_TYPE.splice(index, 1);
    };
    this.addNewModalRef1.show(NEW_DELIVERY_TYPE);
  }

  onDelivery(delivery){
    jQuery("#delivery-image").val("");
    this.upload_delivery_imageurl = "";
    this.upload_banner_imageUrl="";
    this.selected_delivery_id = delivery._id
    this.selected_delivery_type = delivery.delivery_type
    this.is_edit = true
    this.is_image_updated = false
    this.is_default_image_updated = false
    this.favourite_tags = delivery.famous_products_tags
    if(this._lang.supportedLanguages.length > 0){
      this.delivery_name.patchValue(delivery.delivery_name)
      this.description.patchValue(delivery.description)
    } else {
      this.languages_subscription = this._lang.languagesObservable.subscribe(() => {
        this.delivery_name.patchValue(delivery.delivery_name)
        this.description.patchValue(delivery.description)
      })
    }
    this.deliveryForm.patchValue({
      is_provide_table_booking:delivery.is_provide_table_booking,
      delivery_type: delivery.delivery_type,
      sequence_number: delivery.sequence_number,
      is_store_can_create_group: delivery.is_store_can_create_group,
      is_admin_service_allow: delivery.is_admin_service_allow || false,
      is_store_can_edit_order: delivery.is_store_can_edit_order,
      is_business: delivery.is_business,
      theme_number: String(delivery.theme_number) || '1'
    })
    this.image_url = delivery.image_url;
    this.default_image_url = delivery.default_image_url;
    this.icon_url = delivery.icon_url;
    this.banner_url = delivery.banner_url;
  }

  tagAdded(event){
    if(this.tag_index!==null){
      this.favourite_tags[this.tag_index] = event.tag
      this.deliveryForm.value.famous_products_tags[this.tag_index] = event.tag
    } else {
      this.favourite_tags.push(event.tag)
      this.deliveryForm.value.famous_products_tags.push(event.tag)
    }
  }

  OnDeleteTag(index){
    this.deleted_favourite_tags.push(this.favourite_tags[index])
    this.favourite_tags.splice(index, 1)
  }

  ngOnDestroy(): void {
    if(this.cropModel.modalRef){
      this.cropModel.onClose();
    }
    if(this.addNewModalRef1.modalRef){
      this.addNewModalRef1.onClose();
    }
    if(this.deliveryTagModal.modalRef){
      this.deliveryTagModal.onClose();
    }
  }

  onUpdate(){
    this._helper.checkAndCleanFormValues(this.deliveryForm)
    if(this.deliveryForm.invalid){
      this.deliveryForm.markAllAsTouched();
      return
    }
    this.form_data.append('delivery_name', JSON.stringify(this.deliveryForm.value.delivery_name));
    this.form_data.append('description', JSON.stringify(this.deliveryForm.value.description))
    this.form_data.append('is_business', this.deliveryForm.value.is_business);
    this.form_data.append('is_store_can_create_group', this.deliveryForm.value.is_store_can_create_group)
    this.form_data.append('is_store_can_edit_order', this.deliveryForm.value.is_store_can_edit_order)
    this.form_data.append('delivery_type', this.deliveryForm.value.delivery_type);
    this.form_data.append('famous_products_tags', JSON.stringify(this.favourite_tags));
    this.form_data.append('deleted_product_tag_array', JSON.stringify(this.deleted_favourite_tags));
    this.form_data.append('sequence_number', this.deliveryForm.value.sequence_number);
    this.form_data.append('delivery_id', this.selected_delivery_id)
    this.form_data.append('is_provide_table_booking', this.deliveryForm.value.is_provide_table_booking);
    this.form_data.append('theme_number', this.deliveryForm.value.theme_number)
    this.form_data.append('is_admin_service_allow' , this.deliveryForm.value.is_admin_service_allow)

    this._deliveryService.update_delivery_data(this.form_data)
    this.form_data = new FormData
    this.is_banner_image_updated = false
  }

  onChangeBusiness(type){
    this.delivery_search = '';
    if(type != 0){
      this.filter_delivery_list = this.delivery_list.filter(value => type  == value.delivery_type)
      if(this.filter_delivery_list.length>0){
        this.onDelivery(this.filter_delivery_list[0])
      }
    }else{
      this.filter_delivery_list = this.delivery_list;
      if(this.filter_delivery_list.length>0){
        this.onDelivery(this.filter_delivery_list[0])
      }
    }
  }

}
