import { Component, Input, OnInit, ViewChild, TemplateRef, ChangeDetectorRef } from '@angular/core';
import { CountryService } from '../../../../services/country.service';
import { Subscription } from 'rxjs';
import { CityService } from '../../../../services/city.service';
import { UntypedFormGroup, UntypedFormControl, Validators, UntypedFormArray, FormBuilder, FormArray } from '@angular/forms';
import { LangService } from 'src/app/shared/lang.service';
import { Helper } from 'src/app/shared/helper';
import { Router } from '@angular/router';
import { CommonService } from 'src/app/services/common.service';
import { DeliveryPriceNotSetModal } from 'src/app/containers/pages/add-charges-modal/delivery-price-not-set-modal.component';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { DELIVERY_TYPE_CONSTANT } from 'src/app/shared/constant';
import { AdminSettingService } from '../../../../services/admin-setting.service';
import { TabsetComponent } from 'ngx-bootstrap/tabs';
import * as $ from "jquery";
import { NotifiyService } from 'src/app/services/notifier.service';

declare let google;
declare let jQuery: any;

@Component({
  selector: 'app-city-info',
  templateUrl: './city-info.component.html',
  styleUrls: ['./city-info.component.scss']
})
export class CityInfoComponent implements OnInit {
  ZONECONST = { // city zones tab constant
    city_radius_page : 1,
    city_zone: 2,
    airport_zone: 3,
    red_zone: 4,
    dest_cities: 5
  }

  is_overlaping_zones: any  = [ 
    {is_overlap:false,index:this.ZONECONST.city_zone},
    {is_overlap:false,index:this.ZONECONST.airport_zone},
    {is_overlap:false,index:this.ZONECONST.red_zone}
  ];

  @Input() addCity = false;

  @ViewChild('DeliveryPriceNotSetModal', { static: true }) DeliveryPriceNotSetModal: DeliveryPriceNotSetModal;
  @ViewChild('deleteZone', { static: true }) deleteZone: TemplateRef<any>;
  @ViewChild('staticTabs', { static: false }) staticTabs?: TabsetComponent;
  @ViewChild('confirmationTemplate', { static: true }) confirmationTemplate: TemplateRef<any>;
  confirmationModalConfig = {
    backdrop: true,
    ignoreBackdropClick: true,
  }
  confirmModelRef: BsModalRef;
  modalRef: BsModalRef
  country_list: any
  deliveries_in_city_list: any;
  payment_gateway_list: any
  country_subscriber: Subscription
  city_subscriber: Subscription
  timezones: any = []
  city_form: UntypedFormGroup
  dest_city_form: UntypedFormGroup

  selected_city: any
  is_use_radius = false;
  radiusRequired: boolean= false;
  city_radius:number = 0;
  city_radius_map: any = '';
  city_radius_circle: any;
  city_radius_drawing_manager: any;
  city_polygon: any;

  zone_map: any = '';
  drawing_manager: any = '';
  city_zone: any = []
  selected_shape: any;
  selected_color: any;
  colors = ['#1E90FF', '#FF1493', '#32CD32', '#FF8C00', '#4B0082'];
  delete_zone: any;
  taxi_and_courier_deliveries_in_city: any;
  store_deliveries_in_city: any;
  appointment_deliveries_in_city: any;
  service_deliveries_in_city: any;
  settingData: any;
  tabType:number;
  save_cityform:boolean=false;
  selected_country : any ;
  city_boundry_required:boolean=false;
  
  polygon_array: any[] = [];
  infoWindowArray: any [] = [];
  is_store_earning_add_in_wallet_on_cash_payments: any;
  is_store_earning_add_in_wallet_on_other_payments: any;
  is_provider_earning_add_in_wallet_on_cash_payments: boolean;
  is_provider_earning_add_in_wallet_on_other_payments: any;
  patch_is_provider_earning_add_in_wallet_on_other_payment:any;
  patch_is_provider_earning_add_in_wallet_on_cash_payment:any;
  patch_is_store_earning_add_in_wallet_on_cash_payment:any;
  patch_is_store_earning_add_in_wallet_on_other_payment:any;

  city_zonelist_data: any = [];
  is_city_taxi_business_on:boolean= false;
  airportcity_zone: any = []
  airportzone_map: any = '';
  airportdrawing_manager: any = '';
  
  redcity_zone: any = []
  redzone_map: any = '';
  reddrawing_manager: any = '';
  destination_city_list: any [] = [];

  constructor(private _countryService: CountryService,
    private _cityService: CityService,
    private _commonService: CommonService,
    public _helper: Helper,
    private _router: Router,
    private modalService: BsModalService,
    private _adminSettingService: AdminSettingService,
    private _notifierService: NotifiyService , 
    private cdr:ChangeDetectorRef,
    private _lang:LangService,
    private fb: FormBuilder) { }

  ngOnInit(): void {

    this.country_subscriber = this._countryService._countryObservable.subscribe(() => {
    })
    this._initCityForm();
    this._initDestCityForm();
    this.city_subscriber = this._cityService._cityObservable.subscribe(()=>{
      setTimeout(() => {
        if(!this.selected_city){
          if(!this._helper.has_permission(this._helper.PERMISSION.ADD)){
            this.city_form.disable();
          }
        }
      }, 300);
    })
    
    let notification = false
    this._adminSettingService.fetchAdminSetting(notification).then(res => {
      if (res.success) {
        this.getServerCountryList();

        this.settingData = res.setting

        this.city_subscriber = this._cityService._citySelect.subscribe(res => {
          if (res.success) {

            this.selected_country = res.city.country_details;
            this.selected_city = JSON.parse(JSON.stringify(res.city))
            let autocompleteElm = <HTMLInputElement>document.getElementById('city_name');
            google.maps.event.clearInstanceListeners(autocompleteElm);
            this.selected_city.city_lat = this.selected_city.city_lat_long[0]
            this.selected_city.city_lng = this.selected_city.city_lat_long[1]
            this.selected_city.city_id = this.selected_city._id
            this.getDestinationCity(); // get DestinationCity list

            let city_taxi = this.taxi_and_courier_deliveries_in_city.findIndex(x=>x.delivery_type == DELIVERY_TYPE_CONSTANT.TAXI)
            if(city_taxi != -1){
              let is_city_taxi_on = this.selected_city?.deliveries_in_city.find((x => x == this.taxi_and_courier_deliveries_in_city[city_taxi]._id))
              if(is_city_taxi_on){
                this.is_city_taxi_business_on = true;
              }else{
                this.is_city_taxi_business_on = false;
              }
            }
            
            this.selected_city.deliveries_in_city = this.convertedChekboxList(this.deliveries_in_city_list, this.selected_city.deliveries_in_city)
            this.selected_city.payment_gateway = this.convertedChekboxList(this.payment_gateway_list, this.selected_city.payment_gateway)

            this.city_form.patchValue({
              ...this.selected_city
            })

            this.patch_is_provider_earning_add_in_wallet_on_other_payment = this.selected_city.is_provider_earning_add_in_wallet_on_other_payment;
            this.patch_is_provider_earning_add_in_wallet_on_cash_payment = this.selected_city.is_provider_earning_add_in_wallet_on_cash_payment;
            this.patch_is_store_earning_add_in_wallet_on_cash_payment = this.selected_city.is_store_earning_add_in_wallet_on_cash_payment;
            this.patch_is_store_earning_add_in_wallet_on_other_payment = this.selected_city.is_store_earning_add_in_wallet_on_other_payment;
          
            setTimeout(() => {
              this.settlementToggle();
            }, 400);

            this.is_use_radius = this.selected_city.is_use_radius
            this.city_radius = this.selected_city.city_radius
            this.city_zone = res.city_zone
            this.city_zonelist_data = res.city_zone

            this._initMap()
            this.setCenter()
            this.clearMap()
            this.drawRadius()
            if (this.is_use_radius === false) {
              this.drawCityPoligon()
            }
            if(this.selected_city){
              setTimeout(() => {
              if((this._helper.has_permission(this._helper.PERMISSION.ADD) && !this._helper.has_permission(this._helper.PERMISSION.EDIT)) || (!this._helper.has_permission(this._helper.PERMISSION.ADD) && !this._helper.has_permission(this._helper.PERMISSION.EDIT))){
                this.city_form.disable();
              }else{
                this.city_form.enable();
                  this.payment_gateway_list.forEach((element, index) => {
                    if (element.name === 'Paypal' && !res.is_paypal_available) {
                      this.payment_gateway.at(index).disable();
                    } else if (element.name === 'Paypal' && res.is_paypal_available) {
                      this.payment_gateway.at(index).enable();
                    }
                    if (element.name === 'Paytabs' && !res.is_paytabs_available) {
                      this.payment_gateway.at(index).disable();
                    } else if (element.name === 'Paytabs' && res.is_paytabs_available) {
                      this.payment_gateway.at(index).enable();
                    }
                  }); 
              }
              }, 300);
            }
            this.save_cityform=false;
            this.cashReceive();
            
            this.is_overlaping_zones.forEach(ele => {
              ele.is_overlap = false;
            });
            if(this.tabType == this.ZONECONST.airport_zone || this.tabType == this.ZONECONST.red_zone || this.tabType == this.ZONECONST.dest_cities){
              this.selectTab(0);
            }
            if(this.tabType == this.ZONECONST.city_zone){
              this.onCityZone();
            }
          } else {
            this.selected_city = undefined;
          }
        })
        this.city_subscriber = this._cityService._addCity.subscribe(city => {
          this.selected_city = null
          this._initCityForm();
          this._initDestCityForm();
          this.getServerCountryList();
          this._initMap()
          this.is_use_radius = false
          this.city_radius = 0
          this.city_zonelist_data = [];
          this.city_zone = [] ;
          this.airportcity_zone = [];
          this.redcity_zone = [];
          this.destination_city_list = [];
          this.save_cityform=false;
          this.is_city_taxi_business_on = false;
          setTimeout(() => {
            this.city_form.enable();
            this.settlementToggle();
          }, 200);
          
          this.is_overlaping_zones.forEach(ele => {
            ele.is_overlap = false;
          });
          this.selectTab(0);
        })

        this._initCityForm()
        this._initDestCityForm();
        this._initMap()
      }
      this.settlementToggle();
    })
  }
  selectTab(tabId: number) {
    this.tabType =tabId;    
    if (this.staticTabs?.tabs[tabId]) {
      this.staticTabs.tabs[tabId].active = true;
    }
  }
  settlementToggle(){
  this.is_store_earning_add_in_wallet_on_cash_payments = this.settingData?.is_store_earning_add_in_wallet_on_cash_payment
  this.is_store_earning_add_in_wallet_on_other_payments = this.settingData?.is_store_earning_add_in_wallet_on_other_payment
  this.is_provider_earning_add_in_wallet_on_cash_payments = this.settingData?.is_provider_earning_add_in_wallet_on_cash_payment
  this.is_provider_earning_add_in_wallet_on_other_payments = this.settingData?.is_provider_earning_add_in_wallet_on_other_payment
  
  if(!this.is_provider_earning_add_in_wallet_on_other_payments){
    this.city_form.get('is_provider_earning_add_in_wallet_on_other_payment').disable();
  }

  if(!this.is_provider_earning_add_in_wallet_on_cash_payments){
    this.city_form.get('is_provider_earning_add_in_wallet_on_cash_payment').disable();
  }

  if (!this.is_store_earning_add_in_wallet_on_cash_payments) {
    this.city_form.get('is_store_earning_add_in_wallet_on_cash_payment').disable();
  }

  if (!this.is_store_earning_add_in_wallet_on_other_payments) {
    this.city_form.get('is_store_earning_add_in_wallet_on_other_payment').disable();
  }
  }

  atLeastOneCheckboxSelectedValidator() {
    return (formArray: FormArray) => {
      const atLeastOneSelected = formArray.controls.some(control => control.value === true);
      return atLeastOneSelected ? null : { noCheckboxSelected: true };
    };
  }

  _initCityForm() {
    this.city_form = new UntypedFormGroup({
      country_id: new UntypedFormControl(null, Validators.required),
      city_name: new UntypedFormControl(null, Validators.required),
      city_code: new UntypedFormControl(null, Validators.required),
      city_id: new UntypedFormControl(null),
      city_lat: new UntypedFormControl(null, Validators.required),
      city_lng: new UntypedFormControl(null, Validators.required),
      is_ads_visible: new UntypedFormControl(false, Validators.required),
      is_business: new UntypedFormControl(true, Validators.required),
      zone_business: new UntypedFormControl(false, Validators.required),
      airport_business: new UntypedFormControl(false, Validators.required),
      city_business: new UntypedFormControl(false, Validators.required),
      is_ask_user_for_fixed_fare: new UntypedFormControl(false, Validators.required),
      is_cash_payment_mode: new UntypedFormControl(true, Validators.required),
      is_check_provider_wallet_amount_for_received_cash_request: new UntypedFormControl(false, Validators.required),
      is_other_payment_mode: new UntypedFormControl(true, Validators.required),
      is_promo_apply: new UntypedFormControl(false, Validators.required),
      is_provider_earning_add_in_wallet_on_cash_payment: new UntypedFormControl(false, Validators.required),
      is_provider_earning_add_in_wallet_on_other_payment: new UntypedFormControl(false, Validators.required),
      is_store_earning_add_in_wallet_on_cash_payment: new UntypedFormControl(false, Validators.required),
      is_store_earning_add_in_wallet_on_other_payment: new UntypedFormControl(false, Validators.required),
      timezone: new UntypedFormControl(null, Validators.required),
      provider_min_wallet_amount_for_received_cash_request: new UntypedFormControl(0),
      deliveries_in_city: new UntypedFormArray([]),
      payment_gateway:   this.fb.array([]),
      city_locations: new UntypedFormControl([])
    })

    this.city_form
      .get('is_other_payment_mode')
      .valueChanges.subscribe((value) => {
        const paymentGatewayControl = this.city_form.get('payment_gateway');
        if (value) {
          paymentGatewayControl.setValidators(
            this.atLeastOneCheckboxSelectedValidator()
          );
        } else {
          paymentGatewayControl.clearValidators();
        }
        paymentGatewayControl.updateValueAndValidity();
      });
      setTimeout(() => {
        this.settlementToggle();
      }, 100);
  }

  _initDestCityForm(){
    this.dest_city_form = new UntypedFormGroup({
      destination_city: new UntypedFormArray([])
    })
  }
  
  cashReceive(){
    if(this.city_form.value.is_check_provider_wallet_amount_for_received_cash_request){
      this.city_form.get('provider_min_wallet_amount_for_received_cash_request').setValidators([Validators.min(0),Validators.required])
    }else{
      this.city_form.get('provider_min_wallet_amount_for_received_cash_request').setValidators([])
      if(!this.city_form.value.provider_min_wallet_amount_for_received_cash_request){
        this.city_form.patchValue({provider_min_wallet_amount_for_received_cash_request:0})
      }
    }
  }

  _initMap() {
    let theme = localStorage.getItem('vien-themecolor');
    if(theme.includes('dark')){
      this.city_radius_map = new google.maps.Map(document.getElementById('city_radius_map'), {
        zoom: 10,
        streetViewControl: false,
        center: { lat: this.settingData.latitude, lng: this.settingData.longitude },
        styles: [
          { elementType: "geometry", stylers: [{ color: "#242f3e" }] },
          { elementType: "labels.text.stroke", stylers: [{ color: "#242f3e" }] },
          { elementType: "labels.text.fill", stylers: [{ color: "#746855" }] },
          {
            featureType: "administrative.locality",
            elementType: "labels.text.fill",
            stylers: [{ color: "#d59563" }],
          },
          {
            featureType: "poi",
            elementType: "labels.text.fill",
            stylers: [{ color: "#d59563" }],
          },
          {
            featureType: "poi.park",
            elementType: "geometry",
            stylers: [{ color: "#263c3f" }],
          },
          {
            featureType: "poi.park",
            elementType: "labels.text.fill",
            stylers: [{ color: "#6b9a76" }],
          },
          {
            featureType: "road",
            elementType: "geometry",
            stylers: [{ color: "#38414e" }],
          },
          {
            featureType: "road",
            elementType: "geometry.stroke",
            stylers: [{ color: "#212a37" }],
          },
          {
            featureType: "road",
            elementType: "labels.text.fill",
            stylers: [{ color: "#9ca5b3" }],
          },
          {
            featureType: "road.highway",
            elementType: "geometry",
            stylers: [{ color: "#746855" }],
          },
          {
            featureType: "road.highway",
            elementType: "geometry.stroke",
            stylers: [{ color: "#1f2835" }],
          },
          {
            featureType: "road.highway",
            elementType: "labels.text.fill",
            stylers: [{ color: "#f3d19c" }],
          },
          {
            featureType: "transit",
            elementType: "geometry",
            stylers: [{ color: "#2f3948" }],
          },
          {
            featureType: "transit.station",
            elementType: "labels.text.fill",
            stylers: [{ color: "#d59563" }],
          },
          {
            featureType: "water",
            elementType: "geometry",
            stylers: [{ color: "#17263c" }],
          },
          {
            featureType: "water",
            elementType: "labels.text.fill",
            stylers: [{ color: "#515c6d" }],
          },
          {
            featureType: "water",
            elementType: "labels.text.stroke",
            stylers: [{ color: "#17263c" }],
          },
        ],
      });
    }else{
      this.city_radius_map = new google.maps.Map(document.getElementById('city_radius_map'), {
        zoom: 10,
        streetViewControl: false,
        center: { lat: this.settingData.latitude, lng: this.settingData.longitude }
      });
    }
    this._initDrawingManager()
  }

  _initDrawingManager() {
    this.city_radius_drawing_manager = new google.maps.drawing.DrawingManager({
      drawingMode: null,
      drawingControl: true,
      drawingControlOptions: {
        position: google.maps.ControlPosition.TOP_CENTER,
        drawingModes: [google.maps.drawing.OverlayType.POLYGON]
      },
      polygonOptions: {
        fillColor: 'black',
        fillOpacity: 0.3,
        strokeWeight: 2,
        clickable: true,
        editable: true,
        zIndex: 1
      }
    });
    this.city_radius_drawing_manager.setMap(this.city_radius_map);

    google.maps.event.addListener(this.city_radius_drawing_manager, 'overlaycomplete', (polygon) => {
      let shape = polygon.overlay;
      this.city_radius_drawing_manager.setDrawingMode(null);
      this.city_radius_drawing_manager.setOptions({
        drawingControl: false,
      });
      let location_array = []
      shape.getPath().getArray().forEach((location) => {
        location_array.push([location.lng(), location.lat()])
      });

      this.city_form.value.city_locations = location_array;
      google.maps.event.addListener(shape.getPath(), 'set_at', (event) => {
        let location_array = []
        shape.getPath().getArray().forEach((location) => {
          location_array.push([location.lng(), location.lat()])
        });
        this.city_form.value.city_locations = location_array;
      });
      google.maps.event.addListener(shape.getPath(), 'insert_at', (event) => {
        let location_array = []
        shape.getPath().getArray().forEach((location) => {
          location_array.push([location.lng(), location.lat()])
        });
        this.city_form.value.city_locations = location_array;
      });
      google.maps.event.addListener(shape.getPath(), 'remove_at', (event) => {
        let location_array = []
        shape.getPath().getArray().forEach((location) => {
          location_array.push([location.lng(), location.lat()])
        });
        this.city_form.value.city_locations = location_array;
      });
    });
  }

  _initZoneMap() {
    let infoWindow = null;

    let theme = localStorage.getItem('vien-themecolor');
    if(theme.includes('dark')){
      this.zone_map = new google.maps.Map(document.getElementById('map'), {
        zoom: 12,
        streetViewControl: false,
        center: { lat: this.settingData.latitude, lng: this.settingData.longitude },
        styles: [
          { elementType: "geometry", stylers: [{ color: "#242f3e" }] },
          { elementType: "labels.text.stroke", stylers: [{ color: "#242f3e" }] },
          { elementType: "labels.text.fill", stylers: [{ color: "#746855" }] },
          {
            featureType: "administrative.locality",
            elementType: "labels.text.fill",
            stylers: [{ color: "#d59563" }],
          },
          {
            featureType: "poi",
            elementType: "labels.text.fill",
            stylers: [{ color: "#d59563" }],
          },
          {
            featureType: "poi.park",
            elementType: "geometry",
            stylers: [{ color: "#263c3f" }],
          },
          {
            featureType: "poi.park",
            elementType: "labels.text.fill",
            stylers: [{ color: "#6b9a76" }],
          },
          {
            featureType: "road",
            elementType: "geometry",
            stylers: [{ color: "#38414e" }],
          },
          {
            featureType: "road",
            elementType: "geometry.stroke",
            stylers: [{ color: "#212a37" }],
          },
          {
            featureType: "road",
            elementType: "labels.text.fill",
            stylers: [{ color: "#9ca5b3" }],
          },
          {
            featureType: "road.highway",
            elementType: "geometry",
            stylers: [{ color: "#746855" }],
          },
          {
            featureType: "road.highway",
            elementType: "geometry.stroke",
            stylers: [{ color: "#1f2835" }],
          },
          {
            featureType: "road.highway",
            elementType: "labels.text.fill",
            stylers: [{ color: "#f3d19c" }],
          },
          {
            featureType: "transit",
            elementType: "geometry",
            stylers: [{ color: "#2f3948" }],
          },
          {
            featureType: "transit.station",
            elementType: "labels.text.fill",
            stylers: [{ color: "#d59563" }],
          },
          {
            featureType: "water",
            elementType: "geometry",
            stylers: [{ color: "#17263c" }],
          },
          {
            featureType: "water",
            elementType: "labels.text.fill",
            stylers: [{ color: "#515c6d" }],
          },
          {
            featureType: "water",
            elementType: "labels.text.stroke",
            stylers: [{ color: "#17263c" }],
          },
        ],
      });
    }else{
      this.zone_map = new google.maps.Map(document.getElementById('map'), {
        zoom: 12,
        streetViewControl: false,
        center: { lat: this.settingData.latitude, lng: this.settingData.longitude }
      });
    }

    this.drawing_manager = new google.maps.drawing.DrawingManager({
      drawingMode: null,
      drawingControl: true,
      drawingControlOptions: {
        position: google.maps.ControlPosition.TOP_CENTER,
        drawingModes: [google.maps.drawing.OverlayType.POLYGON]
      },
      polygonOptions: {
        fillColor: 'black',
        fillOpacity: 0.3,
        strokeWeight: 2,
        clickable: true,
        editable: true,
        zIndex: 1
      }
    });
    this.drawing_manager.setMap(this.zone_map);

    google.maps.event.addListener(this.drawing_manager, 'overlaycomplete', (polygon) => {

      let shape = polygon.overlay;
      shape.type = polygon.type;
      shape.index = this.city_zone.length;
      shape.title = 'Polygon ' + (this.city_zone.length + 1);
      this.drawing_manager.setDrawingMode(null);
      let location_array = []
      shape.getPath().getArray().forEach((location) => {
        location_array.push([location.lng(), location.lat()])
      });
      location_array = this.poligon_line_midpoint(location_array);

      let json = {
        kmlzone: location_array,
        index: this.city_zone.length,
        color: shape.get('fillColor'),
        title: shape.title
      }
      this.city_zone.push(json);
      this.setSelection(shape);
      let submit = this._helper._translateService.instant('button-title.submit')
      let locations = shape.getPath().getArray()
      let html = '<div><input type="text" id="title" name="title"/></div><br>' +
        '<div><input type="color" id="color" name="color"/></div><br>' +
        '<div><button id="submit_title" type="button" style="text-align: center;">'+submit+'</button></div>';

      if (infoWindow) {
        infoWindow.close();
      }

      this.zone_overlaping_check()

      infoWindow = new google.maps.InfoWindow();
      infoWindow.setContent(html);
      infoWindow.setPosition(locations[0]);
      infoWindow.open(this.zone_map, shape);
      setTimeout(() => {
        jQuery('#submit_title').on('click', (event, res_data) => {
          if (this.selected_shape) {
            if (jQuery('#title').val().toString().trim() != '') {
              this.selected_shape.set('title', jQuery('#title').val());
              this.city_zone[this.selected_shape.index].title = jQuery('#title').val();
              shape.title = jQuery('#title').val();
            }
            if (jQuery('#color').val() != '') {
              this.makeColorButton(jQuery('#color').val());
              this.selected_shape.set('color', jQuery('#color').val());
              this.city_zone[this.selected_shape.index].color = jQuery('#color').val();
              shape.color = jQuery('#color').val();
            }
            infoWindow.close();
          }
        });
      }, 1000);

      google.maps.event.addListener(shape, 'click', (e) => {

        if (infoWindow) {
          infoWindow.close();
        }

        this.setSelection(shape);
        let locations = shape.getPath().getArray()
        let html = '<div><input type="text" id="edit_title" name="title"/></div><br>' +
          '<div><input type="color" id="edit_color" name="color"/></div><br>' +
          '<div><button id="update_title" type="button" style="text-align: center;">Update</button></div>'

        infoWindow = new google.maps.InfoWindow();
        infoWindow.setContent(html);
        infoWindow.setPosition(locations[0]);
        infoWindow.open(this.zone_map, shape);
        jQuery('#edit_title').val(shape.title);
        jQuery('#edit_color').val(shape.fill);
        setTimeout(() => {
          jQuery('#update_title').on('click', (event, res_data) => {
            if (this.selected_shape) {
              if (jQuery('#edit_color').val() != '') {
                this.makeColorButton(jQuery('#edit_color').val())
                this.selected_shape.set('color', jQuery('#edit_color').val());
                this.city_zone[this.selected_shape.index].color = jQuery('#edit_color').val();
                shape.color = jQuery('#edit_color').val();
              }
              if (jQuery('#edit_title').val().toString().trim() != '') {
                this.selected_shape.set('title', jQuery('#edit_title').val());
                this.city_zone[this.selected_shape.index].title = jQuery('#edit_title').val();
                shape.title = jQuery('#edit_title').val();
              }
              infoWindow.close();
            }
          });
        }, 1000);
      });

      google.maps.event.addListener(shape.getPath(), 'set_at', (event) => {
        let location_array = []
        shape.getPath().getArray().forEach((location) => {
          location_array.push([location.lng(), location.lat()])
        });
        location_array = this.poligon_line_midpoint(location_array);
        
        this.city_zone[this.selected_shape.index].kmlzone = location_array; 
        this.zone_overlaping_check()
      });
      google.maps.event.addListener(shape.getPath(), 'insert_at', (event) => {
        let location_array = []
        shape.getPath().getArray().forEach((location) => {
          location_array.push([location.lng(), location.lat()])
        });
        location_array = this.poligon_line_midpoint(location_array);

        this.city_zone[this.selected_shape.index].kmlzone = location_array;
        this.zone_overlaping_check()

      });
      google.maps.event.addListener(shape.getPath(), 'remove_at', (event) => {
        let location_array = []
        shape.getPath().getArray().forEach((location) => {
          location_array.push([location.lng(), location.lat()])
        });
        location_array = this.poligon_line_midpoint(location_array);

        this.city_zone[this.selected_shape.index].kmlzone = location_array;
        this.zone_overlaping_check()

      });

    });

    this.buildColorPalette();
  }

  getServerCountryList() {
    this._countryService.getServerCountryList().then((res => {
      if (res.success) {
        this.country_list = res.countries
        this.deliveries_in_city_list = res.delivery
        this.deliveries_in_city_list.forEach(element => {
          this.deliveries_in_city.push(new UntypedFormControl(false))
        });
        this.payment_gateway_list = res.payment_gateway
        this.payment_gateway_list.forEach(element => {
          if(element.name){
            this.payment_gateway.push(new UntypedFormControl(false))
          }
        });
        this.taxi_and_courier_deliveries_in_city = this.deliveries_in_city_list.filter((i, index) => {
          if (i.delivery_type == DELIVERY_TYPE_CONSTANT.TAXI || i.delivery_type == DELIVERY_TYPE_CONSTANT.COURIER  || i.delivery_type == DELIVERY_TYPE_CONSTANT.ECOMMERCE) {
            i.main_index = index
            return i;
          }
        })
        this.store_deliveries_in_city = this.deliveries_in_city_list.filter((i, index) => {
          if (i.delivery_type == DELIVERY_TYPE_CONSTANT.STORE) {
            i.main_index = index
            return i;
          }
        })
        this.appointment_deliveries_in_city = this.deliveries_in_city_list.filter((i, index) => {
          if (i.delivery_type == DELIVERY_TYPE_CONSTANT.APPOINMENT) {
            i.main_index = index
            return i;
          }
        })
        this.service_deliveries_in_city = this.deliveries_in_city_list.filter((i, index) => {
          if (i.delivery_type == DELIVERY_TYPE_CONSTANT.SERVICE) {
            i.main_index = index
            return i;
          }
        })
      }
    }))
  }

  convertedChekboxList(obj, array) {
    let checklist = []
    obj.forEach(delivery => {
      let i;
      if(array){
        i = array.findIndex(x => x === delivery._id);
      }
      if (i < 0) {
        checklist.push(false)
      } else {
        checklist.push(true)
      }
    });
    return checklist
  }

  changes_cash(event){
    if((this.city_form.value.is_cash_payment_mode == 0 || this.city_form.value.is_cash_payment_mode === false) && (this.city_form.value.is_other_payment_mode == 0 || this.city_form.value.is_other_payment_mode === false)){
      this.city_form.patchValue({is_other_payment_mode : true})
    }
  }

  changes_card(event){
    if((this.city_form.value.is_cash_payment_mode == 0 || this.city_form.value.is_cash_payment_mode === false) && (this.city_form.value.is_other_payment_mode == 0 || this.city_form.value.is_other_payment_mode === false)){
      this.city_form.patchValue({is_cash_payment_mode : true})
    }
  }

  async saveCity() {
    let position = this.is_overlaping_zones.findIndex((x)=> x.index == this.ZONECONST.city_zone)

    if(this.is_overlaping_zones[position].is_overlap){
      this._notifierService.showNotification('error', this._helper._trans.instant('validation-title.zones-are-overlapping'));
      return
    }else{
      this.is_overlaping_zones[position].is_overlap = false;
    }

    if(this.city_form.invalid){
      this.save_cityform=true;
      this.city_form.markAllAsTouched();
      return;
    }
    if (this.city_form.valid) {
      this.save_cityform=false;
      this.convertCheckboxValue()
      if (!this.selected_city) {
        if(this.is_use_radius === true && this.city_radius == null){
          this.radiusRequired =true;
         return 
        }else{
          this.radiusRequired =false;
        }
        if(this.is_use_radius === false && this.city_form.value.city_locations.length == 0){
          this.city_boundry_required = true;
          return
        }else{
          this.city_boundry_required = false;
        }
        this._cityService.addCity({
          ...this.city_form.value,
          city_radius: this.city_radius,
          is_use_radius: this.is_use_radius,
          city_zone: this.city_zone
        })
      } else {
        if((this.city_radius == null) && (this.tabType == this.ZONECONST.city_radius_page)){
          this.radiusRequired =true;
         return 
        }else{
          this.radiusRequired =false;
        }
        if(this.is_use_radius === false && this.city_form.value.city_locations.length == 0 && (this.tabType == this.ZONECONST.city_radius_page)){
          this.city_boundry_required = true;
          return
        }else{
          this.city_boundry_required = false;
        }

      const updateObject = {
        ...this.city_form.value,
        city_radius: this.city_radius,
        is_use_radius: this.is_use_radius,
        city_zone: this.city_zone,
      }
    
      if(this.is_city_taxi_business_on){
        await this.convertDestinationCitiesValue();
        updateObject['destination_city'] = this.dest_city_form.value.destination_city
      }
      
      if(!this.is_provider_earning_add_in_wallet_on_other_payments){
        updateObject.is_provider_earning_add_in_wallet_on_other_payment = this.patch_is_provider_earning_add_in_wallet_on_other_payment
      }
    
      if(!this.is_provider_earning_add_in_wallet_on_cash_payments){
        updateObject.is_provider_earning_add_in_wallet_on_cash_payment = this.patch_is_provider_earning_add_in_wallet_on_cash_payment
      }
    
      if (!this.is_store_earning_add_in_wallet_on_cash_payments) {
        updateObject.is_store_earning_add_in_wallet_on_cash_payment = this.patch_is_store_earning_add_in_wallet_on_cash_payment
      }
    
      if (!this.is_store_earning_add_in_wallet_on_other_payments) {
        updateObject.is_store_earning_add_in_wallet_on_other_payment = this.patch_is_store_earning_add_in_wallet_on_other_payment
      }
      this._cityService.updateCity(updateObject)
      }
      this.selected_city = null;
      this._initCityForm()
      this._initDestCityForm();
      this.getServerCountryList()
      this._initMap()
      this.is_use_radius = false
      this.city_radius = 0
      this.city_zone = []
      this.destination_city_list = [];
      this._cityService._unselectCity.next(true)
      this.city_form.enable();
    }
  }

  cityTaxiAirportBusiness(evt){
    let str = evt.target.id;
    let lastChar = str.charAt(str.length - 1);
    if(evt.target.checked && this.taxi_and_courier_deliveries_in_city[lastChar].delivery_type == DELIVERY_TYPE_CONSTANT.TAXI){
      this.is_city_taxi_business_on = true;
    }else if(!evt.target.checked && this.taxi_and_courier_deliveries_in_city[lastChar].delivery_type == DELIVERY_TYPE_CONSTANT.TAXI){
      this.is_city_taxi_business_on = false;
      this.city_form.patchValue({airport_business:false});
      this.city_form.patchValue({city_business:false});
      this.city_form.patchValue({is_ask_user_for_fixed_fare:false});
    }
  }

  convertCheckboxValue() {
    let deliveries_in_city_list = []
    let payment_gateway_list = []
    for (let i = 0; i < this.deliveries_in_city_list.length; i++) {
      if (this.city_form.value.deliveries_in_city[i] === true) {
        deliveries_in_city_list.push(this.deliveries_in_city_list[i]._id)
      }
    }
    for (let i = 0; i < this.payment_gateway_list.length; i++) {
      if (this.city_form.getRawValue().payment_gateway[i] === true) {
        payment_gateway_list.push(this.payment_gateway_list[i]._id)
      }
    }
    this.city_form.value.deliveries_in_city = deliveries_in_city_list
    this.city_form.value.payment_gateway = payment_gateway_list
  }

  async convertDestinationCitiesValue():Promise<any>{
    return new Promise((resolve,rejects) => {
      let destination_cities_list = []
      for (let i = 0; i < this.destination_city_list.length; i++) {
        if (this.dest_city_form.value.destination_city[i] === true) {
          destination_cities_list.push(this.destination_city_list[i]._id)
        }
      }
      this.dest_city_form.value.destination_city = destination_cities_list
      resolve(true);
    })
  }

  getCountryTimeZones(event) {
    let country = this.country_list.filter(value =>  event == value._id)
    this.selected_country = country[0]

    this.city_form.patchValue({
      city_code: null,
      city_name: null,
      city_lat: null,
      city_lng: null,
      timezone: null
    })
    if (this.city_form.value.country_id) {
      this._countryService.getCountryTimeZones({ countryid: this.city_form.value.country_id }).then(res => {
        this.timezones = res.country_timezone
        this.payment_gateway_list.forEach((element, index) => {
          if (element.name === 'Paypal' && !res.is_paypal_available) {
             this.payment_gateway.at(index).disable();
          }else if (element.name === 'Paypal' && res.is_paypal_available){
            this.payment_gateway.at(index).enable();
          }
          if (element.name === 'Paytabs' && !res.is_paytabs_available) {
             this.payment_gateway.at(index).disable();
          }else if (element.name === 'Paytabs' && res.is_paytabs_available) {
            this.payment_gateway.at(index).enable();
          }
        });

        let autocompleteElm = <HTMLInputElement>document.getElementById('city_name');
        google.maps.event.clearInstanceListeners(autocompleteElm);
        let autocomplete = new google.maps.places.Autocomplete((autocompleteElm), { types: ['(cities)'], componentRestrictions: { country: res.country_code } });
       
        $('.search-address').find("#city_name").on("focus click keypress", () => {
          $('.search-address').css({ position: "relative" }).append($(".pac-container"));
        });
        autocomplete.addListener('place_changed', () => {
          let place = autocomplete.getPlace();
          if(place.geometry && place.geometry.location.lat() && place.geometry.location.lng()){

          let lat = place.geometry.location.lat();
          let lng = place.geometry.location.lng();
          let city_code = null;
          for (const element of place.address_components) {
            if (element.types[0] == "administrative_area_level_1") {
              city_code = element;
              city_code = city_code.short_name;
            }
          }

          if (city_code === null) {
            for (const element of place.address_components) {
              if (element.types[0] == "locality") {
                city_code = element;
                city_code = city_code.short_name;
              }
            }
          }

          this.city_form.patchValue({
            city_code: city_code,
            city_name: place.name,
            city_lat: lat,
            city_lng: lng
          })
          this.cdr.detectChanges();
          let json = {
            city_code: city_code,
            city_name: place.name,
            country_id: this.city_form.value.country_id
          }
          this._cityService.checkCity(json).then((res) => {
            if (!res.success) {
              this.city_form.patchValue({
                city_code: null,
                city_name: null,
                city_lat: null,
                city_lng: null,
                timezone: null
              })
            }
          })
          this.setCenter()
        }
        });
      })
    }
  }

  changeinput(data){
    this.city_form.patchValue({
      city_lat: "",
      city_lng: ""
    })
  }
  drawRadius() {
    if(!this.is_use_radius && this.city_radius == null){
      this.city_radius = 0;
    }
    this.city_radius_drawing_manager.setDrawingMode(null);
    this.city_radius_drawing_manager.setOptions({
      drawingControl: false,
    });

    if (this.city_form.value.city_lat && this.city_form.value.city_lng && this.is_use_radius && this.city_radius > 0) {
      if (this.city_radius_circle) {
        this.city_radius_circle.setMap(null);
      }
      this.city_radius_circle = new google.maps.Circle({
        strokeColor: 'blue',
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: 'blue',
        fillOpacity: 0.35,
        map: this.city_radius_map,
        center: { lat: this.city_form.value.city_lat, lng: this.city_form.value.city_lng },
        radius: (this.city_radius * 1000)
      });
    } else {
      if (this.city_radius_circle) {
        this.city_radius_circle.setMap(null);
      }
    }
    
    if(this.is_use_radius === true ){
      if(this.city_polygon){
        this.city_polygon.setMap(null)        
      }
    }else{
      this.drawCityPoligon()
    }
  }

  drawCityPoligon() {
    if(this.is_use_radius === false){
      if (this.city_form.value.city_locations && this.city_form.value.city_locations.length > 0) {
        this.city_radius_drawing_manager.setDrawingMode(null);
          this.city_radius_drawing_manager.setOptions({
            drawingControl: false,
          });
          if(this.city_polygon){
            this.city_polygon.setMap(null)
          }
      } else {
        if(this.is_use_radius === false){
          this.city_radius_drawing_manager.setOptions({
            drawingControl: true,
          });
        }
        if (!this.city_radius_drawing_manager) {
          this._initDrawingManager()
        }
      }

      if (this.city_form.value.city_locations && this.city_form.value.city_locations.length > 0) {
        let array = [];
        this.city_form.value.city_locations.forEach((location) => {
          array.push({ lat: Number(location[1]), lng: Number(location[0]) })
        });

        this.city_polygon = new google.maps.Polygon({
          map: this.city_radius_map,
          paths: array,
          strokeColor: 'blue',
          strokeOpacity: 1,
          strokeWeight: 1.2,
          fillColor: 'blue',
          fillOpacity: 0.3,
          draggable: false,
          geodesic: true,
          editable: true
        });

        if (this.city_form.value.city_locations.length > 0) {
          google.maps.event.addListener(this.city_polygon.getPath(), 'set_at', (event) => {
            let location_array = []
            this.city_polygon.getPath().getArray().forEach((location) => {
              location_array.push([location.lng(), location.lat()])
            });
            this.city_form.value.city_locations = location_array;
          });
          google.maps.event.addListener(this.city_polygon.getPath(), 'insert_at', (event) => {
            let location_array = []
            this.city_polygon.getPath().getArray().forEach((location) => {
              location_array.push([location.lng(), location.lat()])
            });
            this.city_form.value.city_locations = location_array;
          });
          google.maps.event.addListener(this.city_polygon.getPath(), 'remove_at', (event) => {
            let location_array = []
            this.city_polygon.getPath().getArray().forEach((location) => {
              location_array.push([location.lng(), location.lat()])
            });
            this.city_form.value.city_locations = location_array;
          });
        }
      }
    }
  }

  setCenter() {
    if(this.selected_city){
      this.city_radius_map.setCenter({ lat: Number(this.selected_city.city_lat), lng: Number(this.selected_city.city_lng) });
    }else{
      this.city_radius_map.setCenter({ lat: Number(this.city_form.value.city_lat), lng: Number(this.city_form.value.city_lng) });
    }
    if(this.tabType == this.ZONECONST.city_zone){
      this.zone_map.setCenter({ lat: Number(this.city_form.value.city_lat), lng: Number(this.city_form.value.city_lng) });
    }
    if(this.is_city_taxi_business_on && this.tabType == this.ZONECONST.airport_zone){
      this.airportzone_map.setCenter({ lat: Number(this.city_form.value.city_lat), lng: Number(this.city_form.value.city_lng) });
    }
    if(this.is_city_taxi_business_on && this.tabType == this.ZONECONST.red_zone){
      this.redzone_map.setCenter({ lat: Number(this.city_form.value.city_lat), lng: Number(this.city_form.value.city_lng) });
    }
  }

  clearMap() {
    if (this.city_radius_circle) {
      this.city_radius_circle.setMap(null);
    }
  }

  drawCityZone() {
    this.polygon_array = [];

    let infoWindow = null;
    let allCity_zone:any;
    let drawMap:any;
    if(this.is_city_taxi_business_on && this.tabType == this.ZONECONST.airport_zone){
      allCity_zone = this.airportcity_zone;
      drawMap = this.airportzone_map;
    }else if(this.is_city_taxi_business_on && this.tabType == this.ZONECONST.red_zone){
      allCity_zone = this.redcity_zone;
      drawMap = this.redzone_map;
    }else if(this.tabType == this.ZONECONST.city_zone){
      allCity_zone = this.city_zone;
      drawMap = this.zone_map;
    }

    allCity_zone.forEach((city_zone_detail, index) => {
      let zone = [];
      city_zone_detail.kmlzone.forEach((kml) => {
        zone.push({ lat: Number(kml[1]), lng: Number(kml[0]) })
      });

      let polygon2 = new google.maps.Polygon({
        map: drawMap,
        paths: zone,
        strokeColor: city_zone_detail.color,
        strokeOpacity: 1,
        strokeWeight: 1.2,
        fillColor: city_zone_detail.color,
        fillOpacity: 0.3,
        draggable: false,
        geodesic: true
      });

      if (!this.polygon_array.some(item => item._id === city_zone_detail._id)) {
        this.polygon_array.push({ polygon2: polygon2, _id: city_zone_detail._id });
      }

      let shape: any = polygon2
      shape.index = index;

      google.maps.event.addListener(polygon2.getPath(), 'set_at', (event) => {
        let location_array = []
        polygon2.getPath().getArray().forEach((location) => {
          location_array.push([location.lng(), location.lat()])
        });
        location_array = this.poligon_line_midpoint(location_array);
        allCity_zone[this.selected_shape.index].kmlzone = location_array;
        this.zone_overlaping_check()
      });
      google.maps.event.addListener(polygon2.getPath(), 'insert_at', (event) => {
        let location_array = []
        polygon2.getPath().getArray().forEach((location) => {
          location_array.push([location.lng(), location.lat()])
        });
        location_array = this.poligon_line_midpoint(location_array);

        allCity_zone[this.selected_shape.index].kmlzone = location_array;
        this.zone_overlaping_check()
      });
      google.maps.event.addListener(polygon2.getPath(), 'remove_at', (event) => {
        let location_array = []
        polygon2.getPath().getArray().forEach((location) => {
          location_array.push([location.lng(), location.lat()])
        });
        location_array = this.poligon_line_midpoint(location_array);

        allCity_zone[this.selected_shape.index].kmlzone = location_array;
        this.zone_overlaping_check()
      });

      google.maps.event.addListener(shape, 'click', (e) => {

        if (infoWindow) {
          infoWindow.close();
        }

        this.setSelection(shape);
        let update = this._helper._translateService.instant('button-title.update')
        let locations = shape.getPath().getArray()
        let html = '<div><input type="text" id="edit_titles" name="title" value="'+city_zone_detail.title+'" /></div><br>' +
          '<div><input type="color" id="edit_colors" name="color" value="'+city_zone_detail.color+'"/></div><br>' +
          '<div><button id="update_title" type="button" style="text-align: center;">'+update+'</button></div>'

        infoWindow = new google.maps.InfoWindow();
        infoWindow.setContent(html);
        infoWindow.setPosition(locations[0]);
        infoWindow.open(drawMap, shape);
        this.infoWindowArray.push(infoWindow);
        jQuery('#edit_titles').val(shape.title);
        jQuery('#edit_colors').val(shape.fillColor);

        setTimeout(() => {
          jQuery('#update_title').on('click', (event, res_data) => {
            if (this.selected_shape) {
              if (jQuery('#edit_colors').val() != '') {
                this.makeColorButton(jQuery('#edit_colors').val())
                this.selected_shape.set('color', jQuery('#edit_colors').val());
                allCity_zone[this.selected_shape.index].color = jQuery('#edit_colors').val();
                shape.fillColor = jQuery('#edit_colors').val();
              }
              if (jQuery('#edit_titles').val().toString().trim() != '') {
                this.selected_shape.set('title', jQuery('#edit_titles').val());
                allCity_zone[this.selected_shape.index].title = jQuery('#edit_titles').val();
                shape.title = jQuery('#edit_titles').val();
              }
              infoWindow.close();
            }
          });
        }, 1000);
      });
    });
  }

  buildColorPalette() {
    this.selectColor(this.colors[0]);
  }

  setSelection(shape) {
    this.clearSelection();
    shape.setEditable(true);
    this.selectColor(shape.get('fillColor') || shape.get('strokeColor'));
    this.selected_shape = shape;
  }

  clearSelection() {
    if (this.selected_shape) {
      this.selected_shape.setEditable(false);
      this.selected_shape = null;
    }
  }

  selectColor(color) {
    let polygonOptions:any;
    if(this.tabType == this.ZONECONST.city_zone){
      this.selected_color = color;
      polygonOptions = this.drawing_manager.get('polygonOptions');
      polygonOptions.fillColor = color;
      polygonOptions.strokeColor = color;
      this.drawing_manager.set('polygonOptions', polygonOptions);
    }else if(this.is_city_taxi_business_on && this.tabType == this.ZONECONST.airport_zone){
      this.selected_color = color;
      polygonOptions = this.airportdrawing_manager.get('polygonOptions');
      polygonOptions.fillColor = color;
      polygonOptions.strokeColor = color;
      this.airportdrawing_manager.set('polygonOptions', polygonOptions);
    }else if(this.is_city_taxi_business_on && this.tabType == this.ZONECONST.red_zone){
      this.selected_color = color;
      polygonOptions = this.reddrawing_manager.get('polygonOptions');
      polygonOptions.fillColor = color;
      polygonOptions.strokeColor = color;
      this.reddrawing_manager.set('polygonOptions', polygonOptions);
    }
  }

  setSelectedShapeColor(color) {
    if (this.selected_shape) {
      this.selected_shape.set('strokeColor', color);
      this.selected_shape.set('fillColor', color);
      if(this.tabType == this.ZONECONST.city_zone){
        this.city_zone[this.selected_shape.index].color = color;
      }else if(this.is_city_taxi_business_on && this.tabType == this.ZONECONST.airport_zone){
        this.airportcity_zone[this.selected_shape.index].color = color;
      }else if(this.is_city_taxi_business_on && this.tabType == this.ZONECONST.red_zone){
        this.redcity_zone[this.selected_shape.index].color = color;
      }
    }
  }

  makeColorButton(color) {
    this.selectColor(color);
    this.setSelectedShapeColor(color);
  }

  get payment_gateway() {
    return this.city_form.get('payment_gateway') as UntypedFormArray;
  }

  get deliveries_in_city() {
    return this.city_form.get('deliveries_in_city') as UntypedFormArray;
  }

  onSetDeliveryPrice(delivery) {
    this._helper.changeSelectStore(delivery, 2, true)
    this._commonService.copyData({ city_id: this.selected_city._id, store_id: delivery._id }).then(result => {
      if (result.success) {
        this._helper.changeSelectedCity(this.selected_city, true)
        this._helper.setDeliveryPrice(this.selected_country,delivery);
        if(result.is_pricing_added){
          this._router.navigate(['/app/delivery-info/admin-service'])
        }else{
          this._router.navigate(['/app/delivery-info/d-fees-info'])
        }
      } else {
        this._helper.changeSelectedCity(this.selected_city, false)
        this._helper.setDeliveryPrice(null,null);
        this.DeliveryPriceNotSetModal.show({ city: this.selected_city, delivery });
      }
    })
  }
  // delete zone
  deleteZoneConfirm(zone) {
    this.delete_zone = zone
    this.modalRef = this.modalService.show(this.deleteZone)
  }

  DeleteZone(zone_id) {
    if(this.tabType == this.ZONECONST.city_zone){
      this._cityService.deleteZone(zone_id, this.selected_city._id).then((res) => {
        if (res.success) {
          this.modalRef.hide();
          this._cityService.getCityDetail({ city_id: this.selected_city._id }).then((res) => {
            if (res.success) {
              this.selected_city = JSON.parse(JSON.stringify(res.city))
              this.selected_city.city_lat = this.selected_city.city_lat_long[0]
              this.selected_city.city_lng = this.selected_city.city_lat_long[1]
              this.selected_city.city_id = this.selected_city._id
              this.selected_city.deliveries_in_city = this.convertedChekboxList(this.deliveries_in_city_list, this.selected_city.deliveries_in_city)
              this.selected_city.payment_gateway = this.convertedChekboxList(this.payment_gateway_list, this.selected_city.payment_gateway)
              this.city_form.patchValue({
                ...this.selected_city
              })

              this.is_use_radius = this.selected_city.is_use_radius
              this.city_radius = this.selected_city.city_radius
              this.city_zone = res.city_zone

              this._initZoneMap()
              this._initMap()
              this.setCenter()
              this.clearMap()
              this.drawRadius()
              this.drawCityPoligon()
              this.drawCityZone()
            } else {
              this.selected_city = undefined;
            }
          })
        }
      })
    }else if(this.tabType == this.ZONECONST.airport_zone){
      let arr = [];
      arr.push(zone_id);
      let json: any = { cityid: this.selected_city._id, deleted_airport: arr }
      this._cityService.updateAirportZone(json).then(res => {
        if (res.success) {
          arr = null
          this.modalRef.hide();
          this.airportZoneArea();
        }
      })
    }else if(this.tabType == this.ZONECONST.red_zone){
      let arr = [];
      arr.push(zone_id);
      let json: any = { cityid: this.selected_city._id, deleted_red_zone: arr }
      this._cityService.updateRedZone(json).then(res => {
        if (res.success) {
          arr = null
          this.modalRef.hide();
          this.redZoneArea();
        }
      })
    }
  }

  clearPolygon(){
    this.confirmModelRef = this.modalService.show(this.confirmationTemplate, this.confirmationModalConfig);
  }

  closeBoundary(){
    this.confirmModelRef.hide()
  }

  confirm(){
    this.city_form.value.city_locations = [];
    this.confirmModelRef.hide()
  }

  ngOnDestroy() {
    this.country_subscriber.unsubscribe()
    this.city_subscriber.unsubscribe()
  }

  nextpage(){
    if(this.city_form.invalid){
      this.city_form.markAllAsTouched();
    }else{
      this.save_cityform=false;
      this.selectTab(1);
    }
  }
 
  setZoneCenter(zone, type) {
    let map: any = null;

    if (type == 1) {
      map = this.zone_map;
    } else if (type == 2) {
      map = this.airportzone_map;
    } else if (type == 3) {
      map = this.redzone_map;
    }

    let polygon = this.polygon_array.filter(x => x._id == zone._id)
    let polygon2 = polygon[0].polygon2;
    google.maps.event.trigger(polygon2, 'click');

    const bounds = new google.maps.LatLngBounds();
    polygon2.getPath().forEach((latLng) => {
      bounds.extend(latLng);
    });

    // Fit the map to the polygon bounds
    map.fitBounds(bounds);
  }

  // this function check zones are overlap or not 
  zone_overlaping_check(){
    let city_allzone_list:any;
    let type ;
    if(this.tabType == this.ZONECONST.city_zone){
      city_allzone_list = this.city_zone;
      type = this.ZONECONST.city_zone;
    }else if(this.is_city_taxi_business_on && this.tabType == this.ZONECONST.airport_zone){
      city_allzone_list = this.airportcity_zone;
      type = this.ZONECONST.airport_zone;
    }else if(this.is_city_taxi_business_on && this.tabType == this.ZONECONST.red_zone){
      city_allzone_list = this.redcity_zone;
      type = this.ZONECONST.red_zone;
    }

    let overlap_index = this.is_overlaping_zones.findIndex((x)=> x.index == type)
    this.is_overlaping_zones[overlap_index].is_overlap = false;

    for (let element of city_allzone_list) {
      for (let ele of city_allzone_list) {
        if (element !== ele) {
          for (let zone of element.kmlzone) {
            let inside = this.insideZoesPoints(zone, ele.kmlzone);
            if (inside) {
              this.is_overlaping_zones[overlap_index].is_overlap = true;
              this._notifierService.showNotification('error', this._helper._trans.instant('validation-title.zones-are-overlapping'));
              break;
            }
          }
        }
        if (this.is_overlaping_zones[overlap_index].is_overlap) {
          break;
        }
      }
      if (this.is_overlaping_zones[overlap_index].is_overlap) {
        break;
      }
    }
  
  }

  insideZoesPoints(point, vs) {
    let x = point[0], y = point[1];
    let inside = false;
    for (let i = 0, j = vs.length - 1; i < vs.length; j = i++) {
        let xi = vs[i][0], yi = vs[i][1];
        let xj = vs[j][0], yj = vs[j][1];
        
        let intersect = ((yi > y) != (yj > y))
            && (x < (xj - xi) * (y - yi) / (yj - yi) + xi);
        if (intersect) inside = !inside;
    }
    return inside;
  };

  // this function helps to poligon line midpoint value get
  poligon_line_midpoint(location_array_obj){
    let location_array = location_array_obj;
    let mid_array = [] ;
    for (let i = 0; i < location_array.length; i++) {
      const currentPoint = location_array[i];
      const nextPoint = location_array[(i + 1) % location_array.length]; // Wraps around to the first point for the last segment
      // Calculate the middle point
      const middlePoint = [
        (currentPoint[0] + nextPoint[0]) / 2,
        (currentPoint[1] + nextPoint[1]) / 2
      ];
      mid_array.push([middlePoint[0],middlePoint[1]]);
    }
    let mergedArray = []
    for (let i = 0; i < location_array_obj.length; i++) {
      mergedArray.push([location_array_obj[i][0],location_array_obj[i][1]]);     
      mergedArray.push([mid_array[i][0],mid_array[i][1]]);     
    }
    return mergedArray;
  }

  onCityZone(){
    this._initZoneMap();
    if(this.city_form.value.city_lat && this.city_form.value.city_lng){
      this.setCenter()
      this.city_zone = [];
      this.city_zone = this.city_zonelist_data;
      this.drawCityZone()
    }
  }

  // airport zone area map init 
  airportZoneArea() {
    this._initAirportZoneMap()
    
    if(this.selected_city?._id){
      this._cityService.fetch_airport({ cityid: this.selected_city._id }).then(res => {
        if (res.success) {
          this.airportcity_zone = res.airport_details;
          this.setCenter()
          this.drawCityZone()
        } else {
          this.setCenter()
          this.drawCityZone()
        }
      })
    }
  }

  // airport map init 
  _initAirportZoneMap() {
    let infoWindow = null;

    let theme = localStorage.getItem('vien-themecolor');
    if(theme.includes('dark')){
      this.airportzone_map = new google.maps.Map(document.getElementById('airportmap'), {
        zoom: 12,
        streetViewControl: false,
        center: { lat: this.settingData.latitude, lng: this.settingData.longitude },
        styles: [
          { elementType: "geometry", stylers: [{ color: "#242f3e" }] },
          { elementType: "labels.text.stroke", stylers: [{ color: "#242f3e" }] },
          { elementType: "labels.text.fill", stylers: [{ color: "#746855" }] },
          {
            featureType: "administrative.locality",
            elementType: "labels.text.fill",
            stylers: [{ color: "#d59563" }],
          },
          {
            featureType: "poi",
            elementType: "labels.text.fill",
            stylers: [{ color: "#d59563" }],
          },
          {
            featureType: "poi.park",
            elementType: "geometry",
            stylers: [{ color: "#263c3f" }],
          },
          {
            featureType: "poi.park",
            elementType: "labels.text.fill",
            stylers: [{ color: "#6b9a76" }],
          },
          {
            featureType: "road",
            elementType: "geometry",
            stylers: [{ color: "#38414e" }],
          },
          {
            featureType: "road",
            elementType: "geometry.stroke",
            stylers: [{ color: "#212a37" }],
          },
          {
            featureType: "road",
            elementType: "labels.text.fill",
            stylers: [{ color: "#9ca5b3" }],
          },
          {
            featureType: "road.highway",
            elementType: "geometry",
            stylers: [{ color: "#746855" }],
          },
          {
            featureType: "road.highway",
            elementType: "geometry.stroke",
            stylers: [{ color: "#1f2835" }],
          },
          {
            featureType: "road.highway",
            elementType: "labels.text.fill",
            stylers: [{ color: "#f3d19c" }],
          },
          {
            featureType: "transit",
            elementType: "geometry",
            stylers: [{ color: "#2f3948" }],
          },
          {
            featureType: "transit.station",
            elementType: "labels.text.fill",
            stylers: [{ color: "#d59563" }],
          },
          {
            featureType: "water",
            elementType: "geometry",
            stylers: [{ color: "#17263c" }],
          },
          {
            featureType: "water",
            elementType: "labels.text.fill",
            stylers: [{ color: "#515c6d" }],
          },
          {
            featureType: "water",
            elementType: "labels.text.stroke",
            stylers: [{ color: "#17263c" }],
          },
        ],
      });
    }else{
      this.airportzone_map = new google.maps.Map(document.getElementById('airportmap'), {
        zoom: 12,
        streetViewControl: false,
        center: { lat: this.settingData.latitude, lng: this.settingData.longitude }
      });
    }

    this.airportdrawing_manager = new google.maps.drawing.DrawingManager({
      drawingMode: null,
      drawingControl: true,
      drawingControlOptions: {
        position: google.maps.ControlPosition.TOP_CENTER,
        drawingModes: [google.maps.drawing.OverlayType.POLYGON]
      },
      polygonOptions: {
        fillColor: 'black',
        fillOpacity: 0.3,
        strokeWeight: 2,
        clickable: true,
        editable: true,
        zIndex: 1
      }
    });
    this.airportdrawing_manager.setMap(this.airportzone_map);

    google.maps.event.addListener(this.airportdrawing_manager, 'overlaycomplete', (polygon) => {

      let shape = polygon.overlay;
      shape.type = polygon.type;
      shape.index = this.airportcity_zone.length;
      shape.title = 'Polygon ' + (this.airportcity_zone.length + 1);
      this.airportdrawing_manager.setDrawingMode(null);
      let location_array = []
      shape.getPath().getArray().forEach((location) => {
        location_array.push([location.lng(), location.lat()])
      });
      location_array = this.poligon_line_midpoint(location_array);

      let json = {
        kmlzone: location_array,
        index: this.airportcity_zone.length,
        color: shape.get('fillColor'),
        title: shape.title
      }
      this.airportcity_zone.push(json);
      this.setSelection(shape);
      let submit = this._helper._translateService.instant('button-title.submit')
      let locations = shape.getPath().getArray()
      let html = '<div><input type="text" id="title" name="title"/></div><br>' +
        '<div><input type="color" id="color" name="color"/></div><br>' +
        '<div><button id="submit_title" type="button" style="text-align: center;">'+submit+'</button></div>';

      if (infoWindow) {
        infoWindow.close();
      }

      this.zone_overlaping_check()

      infoWindow = new google.maps.InfoWindow();
      infoWindow.setContent(html);
      infoWindow.setPosition(locations[0]);
      infoWindow.open(this.airportzone_map, shape);
      setTimeout(() => {
        jQuery('#submit_title').on('click', (event, res_data) => {
          if (this.selected_shape) {
            if (jQuery('#title').val().toString().trim() != '') {
              this.selected_shape.set('title', jQuery('#title').val());
              this.airportcity_zone[this.selected_shape.index].title = jQuery('#title').val();
              shape.title = jQuery('#title').val();
            }
            if (jQuery('#color').val() != '') {
              this.makeColorButton(jQuery('#color').val());
              this.selected_shape.set('color', jQuery('#color').val());
              this.airportcity_zone[this.selected_shape.index].color = jQuery('#color').val();
              shape.fillColor = jQuery('#color').val();
            }
            infoWindow.close();
          }
        });
      }, 1000);

      google.maps.event.addListener(shape, 'click', (e) => {

        if (infoWindow) {
          infoWindow.close();
        }

        this.setSelection(shape);
        let locations = shape.getPath().getArray()
        let html = '<div><input type="text" id="edit_title" name="title"/></div><br>' +
          '<div><input type="color" id="edit_color" name="color"/></div><br>' +
          '<div><button id="update_title" type="button" style="text-align: center;">Update</button></div>'

        infoWindow = new google.maps.InfoWindow();
        infoWindow.setContent(html);
        infoWindow.setPosition(locations[0]);
        infoWindow.open(this.airportzone_map, shape);
        jQuery('#edit_title').val(shape.title);
        jQuery('#edit_color').val(shape.fillColor);
     
        setTimeout(() => {
          jQuery('#update_title').on('click', (event, res_data) => {
            if (this.selected_shape) {
              if (jQuery('#edit_color').val() != '') {
                this.makeColorButton(jQuery('#edit_color').val())
                this.selected_shape.set('color', jQuery('#edit_color').val());
                this.airportcity_zone[this.selected_shape.index].color = jQuery('#edit_color').val();
                shape.fillColor = jQuery('#edit_color').val();
              }
              if (jQuery('#edit_title').val().toString().trim() != '') {
                this.selected_shape.set('title', jQuery('#edit_title').val());
                this.airportcity_zone[this.selected_shape.index].title = jQuery('#edit_title').val();
                shape.title = jQuery('#edit_title').val();
              }
              infoWindow.close();
            }
          });
        }, 1000);
      });

      google.maps.event.addListener(shape.getPath(), 'set_at', (event) => {
        let location_array = []
        shape.getPath().getArray().forEach((location) => {
          location_array.push([location.lng(), location.lat()])
        });
        location_array = this.poligon_line_midpoint(location_array);
        
        this.airportcity_zone[this.selected_shape.index].kmlzone = location_array; 
        this.zone_overlaping_check()
      });
      google.maps.event.addListener(shape.getPath(), 'insert_at', (event) => {
        let location_array = []
        shape.getPath().getArray().forEach((location) => {
          location_array.push([location.lng(), location.lat()])
        });
        location_array = this.poligon_line_midpoint(location_array);

        this.airportcity_zone[this.selected_shape.index].kmlzone = location_array;
        this.zone_overlaping_check()

      });
      google.maps.event.addListener(shape.getPath(), 'remove_at', (event) => {
        let location_array = []
        shape.getPath().getArray().forEach((location) => {
          location_array.push([location.lng(), location.lat()])
        });
        location_array = this.poligon_line_midpoint(location_array);

        this.airportcity_zone[this.selected_shape.index].kmlzone = location_array;
        this.zone_overlaping_check()

      });

    });

    this.buildColorPalette();
  }

  // save airport zone data
  saveAirportZone() {
    let position = this.is_overlaping_zones.findIndex((x)=> x.index == this.ZONECONST.airport_zone)
    
    if(this.is_overlaping_zones[position].is_overlap){
      this._notifierService.showNotification('error', this._helper._trans.instant('validation-title.zones-are-overlapping'));
      return
    }else{
      this.is_overlaping_zones[position].is_overlap = false;
    }

    let json: any = { cityid: this.selected_city._id, airport_array: this.airportcity_zone }
    this._cityService.updateAirportZone(json).then(res => {
      if (res.success) {
        this.airportcity_zone = [];
        this.airportZoneArea();
      }
    })
  }

  // red zone area map init 
  redZoneArea() {
    this._initRedZoneMap()
    if (this.selected_city?._id) {
      this._cityService.fetch_redzone({ cityid: this.selected_city._id }).then(res => {
        if (res.success) {
          if(res.redzone_details.length){
            res.redzone_details.forEach(zone => {
              zone.is_redzone_active = zone.is_redzone_active === false ? zone.is_redzone_active : true;
            });
          }
          this.redcity_zone = res.redzone_details;
          this.setCenter()
          this.drawCityZone()
        } else {
          this.setCenter()
          this.drawCityZone()
        }
      })
    }
  }

  _initRedZoneMap(){
    let infoWindow = null;

    let theme = localStorage.getItem('vien-themecolor');
    if(theme.includes('dark')){
      this.redzone_map = new google.maps.Map(document.getElementById('redZoneMap'), {
        zoom: 12,
        streetViewControl: false,
        center: { lat: this.settingData.latitude, lng: this.settingData.longitude },
        styles: [
          { elementType: "geometry", stylers: [{ color: "#242f3e" }] },
          { elementType: "labels.text.stroke", stylers: [{ color: "#242f3e" }] },
          { elementType: "labels.text.fill", stylers: [{ color: "#746855" }] },
          {
            featureType: "administrative.locality",
            elementType: "labels.text.fill",
            stylers: [{ color: "#d59563" }],
          },
          {
            featureType: "poi",
            elementType: "labels.text.fill",
            stylers: [{ color: "#d59563" }],
          },
          {
            featureType: "poi.park",
            elementType: "geometry",
            stylers: [{ color: "#263c3f" }],
          },
          {
            featureType: "poi.park",
            elementType: "labels.text.fill",
            stylers: [{ color: "#6b9a76" }],
          },
          {
            featureType: "road",
            elementType: "geometry",
            stylers: [{ color: "#38414e" }],
          },
          {
            featureType: "road",
            elementType: "geometry.stroke",
            stylers: [{ color: "#212a37" }],
          },
          {
            featureType: "road",
            elementType: "labels.text.fill",
            stylers: [{ color: "#9ca5b3" }],
          },
          {
            featureType: "road.highway",
            elementType: "geometry",
            stylers: [{ color: "#746855" }],
          },
          {
            featureType: "road.highway",
            elementType: "geometry.stroke",
            stylers: [{ color: "#1f2835" }],
          },
          {
            featureType: "road.highway",
            elementType: "labels.text.fill",
            stylers: [{ color: "#f3d19c" }],
          },
          {
            featureType: "transit",
            elementType: "geometry",
            stylers: [{ color: "#2f3948" }],
          },
          {
            featureType: "transit.station",
            elementType: "labels.text.fill",
            stylers: [{ color: "#d59563" }],
          },
          {
            featureType: "water",
            elementType: "geometry",
            stylers: [{ color: "#17263c" }],
          },
          {
            featureType: "water",
            elementType: "labels.text.fill",
            stylers: [{ color: "#515c6d" }],
          },
          {
            featureType: "water",
            elementType: "labels.text.stroke",
            stylers: [{ color: "#17263c" }],
          },
        ],
      });
    }else{
      this.redzone_map = new google.maps.Map(document.getElementById('redZoneMap'), {
        zoom: 12,
        streetViewControl: false,
        center: { lat: this.settingData.latitude, lng: this.settingData.longitude }
      });
    }

    this.reddrawing_manager = new google.maps.drawing.DrawingManager({
      drawingMode: null,
      drawingControl: true,
      drawingControlOptions: {
        position: google.maps.ControlPosition.TOP_CENTER,
        drawingModes: [google.maps.drawing.OverlayType.POLYGON]
      },
      polygonOptions: {
        fillColor: 'black',
        fillOpacity: 0.3,
        strokeWeight: 2,
        clickable: true,
        editable: true,
        zIndex: 1
      }
    });
    this.reddrawing_manager.setMap(this.redzone_map);

    google.maps.event.addListener(this.reddrawing_manager, 'overlaycomplete', (polygon) => {

      let shape = polygon.overlay;
      shape.type = polygon.type;
      shape.index = this.redcity_zone.length;
      shape.title = 'Polygon ' + (this.redcity_zone.length + 1);
      this.reddrawing_manager.setDrawingMode(null);
      let location_array = []
      shape.getPath().getArray().forEach((location) => {
        location_array.push([location.lng(), location.lat()])
      });
      location_array = this.poligon_line_midpoint(location_array);

      let json = {
        kmlzone: location_array,
        index: this.redcity_zone.length,
        color: shape.get('fillColor'),
        title: shape.title
      }
      this.redcity_zone.push(json);
      this.setSelection(shape);
      let submit = this._helper._translateService.instant('button-title.submit')
      let locations = shape.getPath().getArray()
      let html = '<div><input type="text" id="title" name="title"/></div><br>' +
        '<div><input type="color" id="color" name="color"/></div><br>' +
        '<div><button id="submit_title" type="button" style="text-align: center;">'+submit+'</button></div>';

      if (infoWindow) {
        infoWindow.close();
      }

      this.zone_overlaping_check()

      infoWindow = new google.maps.InfoWindow();
      infoWindow.setContent(html);
      infoWindow.setPosition(locations[0]);
      infoWindow.open(this.redzone_map, shape);
      setTimeout(() => {
        jQuery('#submit_title').on('click', (event, res_data) => {
          if (this.selected_shape) {
            if (jQuery('#title').val().toString().trim() != '') {
              this.selected_shape.set('title', jQuery('#title').val());
              this.redcity_zone[this.selected_shape.index].title = jQuery('#title').val();
              shape.title = jQuery('#title').val();
            }
            if (jQuery('#color').val() != '') {
              this.makeColorButton(jQuery('#color').val());
              this.selected_shape.set('color', jQuery('#color').val());
              this.redcity_zone[this.selected_shape.index].color = jQuery('#color').val();
              shape.fillColor = jQuery('#color').val();
            }
            infoWindow.close();
          }
        });
      }, 1000);

      google.maps.event.addListener(shape, 'click', (e) => {

        if (infoWindow) {
          infoWindow.close();
        }

        this.setSelection(shape);
        let locations = shape.getPath().getArray()
        let html = '<div><input type="text" id="edit_title" name="title"/></div><br>' +
          '<div><input type="color" id="edit_color" name="color"/></div><br>' +
          '<div><button id="update_title" type="button" style="text-align: center;">Update</button></div>'

        infoWindow = new google.maps.InfoWindow();
        infoWindow.setContent(html);
        infoWindow.setPosition(locations[0]);
        infoWindow.open(this.redzone_map, shape);
        jQuery('#edit_title').val(shape.title);
        jQuery('#edit_color').val(shape.fillColor);
     
        setTimeout(() => {
          jQuery('#update_title').on('click', (event, res_data) => {
            if (this.selected_shape) {
              if (jQuery('#edit_color').val() != '') {
                this.makeColorButton(jQuery('#edit_color').val())
                this.selected_shape.set('color', jQuery('#edit_color').val());
                this.redcity_zone[this.selected_shape.index].color = jQuery('#edit_color').val();
                shape.fillColor = jQuery('#edit_color').val();
              }
              if (jQuery('#edit_title').val().toString().trim() != '') {
                this.selected_shape.set('title', jQuery('#edit_title').val());
                this.redcity_zone[this.selected_shape.index].title = jQuery('#edit_title').val();
                shape.title = jQuery('#edit_title').val();
              }
              infoWindow.close();
            }
          });
        }, 1000);
      });

      google.maps.event.addListener(shape.getPath(), 'set_at', (event) => {
        let location_array = []
        shape.getPath().getArray().forEach((location) => {
          location_array.push([location.lng(), location.lat()])
        });
        location_array = this.poligon_line_midpoint(location_array);
        
        this.redcity_zone[this.selected_shape.index].kmlzone = location_array; 
        this.zone_overlaping_check()
      });
      google.maps.event.addListener(shape.getPath(), 'insert_at', (event) => {
        let location_array = []
        shape.getPath().getArray().forEach((location) => {
          location_array.push([location.lng(), location.lat()])
        });
        location_array = this.poligon_line_midpoint(location_array);

        this.redcity_zone[this.selected_shape.index].kmlzone = location_array;
        this.zone_overlaping_check()

      });
      google.maps.event.addListener(shape.getPath(), 'remove_at', (event) => {
        let location_array = []
        shape.getPath().getArray().forEach((location) => {
          location_array.push([location.lng(), location.lat()])
        });
        location_array = this.poligon_line_midpoint(location_array);

        this.redcity_zone[this.selected_shape.index].kmlzone = location_array;
        this.zone_overlaping_check()

      });

    });

    this.buildColorPalette();
  }

  // save airport zone data
  saveRedZone() {
    let position = this.is_overlaping_zones.findIndex((x)=> x.index == this.ZONECONST.red_zone)

    if(this.is_overlaping_zones[position].is_overlap){
      this._notifierService.showNotification('error', this._helper._trans.instant('validation-title.zones-are-overlapping'));
      return
    }else{
      this.is_overlaping_zones[position].is_overlap = false;
    }
    
    let json: any = { cityid: this.selected_city._id, red_zone_array: this.redcity_zone }
    this._cityService.updateRedZone(json).then(res => {
      if (res.success) {
        this.redcity_zone = [];
        this.redZoneArea();
      }
    })
  }

  get destination_city() {
    return this.dest_city_form.get('destination_city') as UntypedFormArray;
  }

  // get country wise destination city list
  getDestinationCity() {
    this._initDestCityForm();
    if (this.selected_country?._id && this.selected_city?._id) {
      let json: any = { country_id: this.selected_country?._id, city_id: this.selected_city?._id}
      this._cityService.getDestinationCityList(json).then(res => {
        if (res.success) {
          this.destination_city_list = res.destination_list;
          this.destination_city_list.forEach(element => {
            this.destination_city.push(new UntypedFormControl(false))
          });
          if(this.selected_city?.destination_city){
            this.selected_city.destination_city = this.convertedChekboxList(this.destination_city_list, this.selected_city?.destination_city)
            this.dest_city_form.patchValue({
              destination_city: this.selected_city.destination_city
            })
          }
        } else {
          this.destination_city_list = []
        }
      })
    }
  }

}
