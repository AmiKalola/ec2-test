import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Helper } from 'src/app/shared/helper';
import { WalletService } from '../../../../services/wallet.service';
import { PER_PAGE_LIST } from '../../../../shared/constant'

@Component({
  selector: 'app-transaction-history',
  templateUrl: './transaction-history.component.html',
})
export class TransactionHistoryComponent implements OnInit {

  transaction_list = []
  total_page = 0;
  current_page = 1;
  itemsPerPage = 20
  itemOptionsPerPage = PER_PAGE_LIST;
  timezone_for_display_date:string = '';

  constructor(private _walletService: WalletService,
    private _trans: TranslateService,
    public _helper: Helper) { }

  ngOnInit(): void {

    this._helper.display_date_timezone.subscribe(data => {
      this.timezone_for_display_date = data;
    })

    this.getTransactionList()
  }

  getTransactionList() {
    let json = {
      page: this.current_page,
      end_date: "",
      search_field: "provider_detail.email",
      search_value: "",
      sort_field: "unique_id",
      sort_transaction_history: -1,
      start_date: "",
      number_of_rec: this.itemsPerPage
    }
    this._walletService.transaction_history(json).then(res => {
      this.transaction_list = []
      this.total_page = 0;
      if (res.success) {
        this.transaction_list = res.transfer_history
        this.total_page = res.pages
      }
    })
  }
  pageChanged(event) {
    this.current_page = event.page
    this.getTransactionList()
  }

  clickonExport(){
    let json = {
      page: this.current_page,
      end_date: "",
      search_field: "provider_detail.email",
      search_value: "",
      sort_field: "unique_id",
      sort_transaction_history: -1,
      start_date: "",
      number_of_rec: this.itemsPerPage
    }
    let all_transactions = [];
    this._walletService.transaction_history(json).then(res => {
      this.transaction_list = []
      this.total_page = 0;
      if (res.success) {
        this.transaction_list = res.transfer_history
        this.transaction_list.forEach(transactions => {
          let user_type;
          let email;
          let currency;
          let transfer_status;
          if(transactions.user_type === 8){
            user_type = this._trans.instant('label-title.provider');
            email = transactions.provider_detail[0].email;
            currency = transactions.provider_detail[0].wallet_currency_code;
          } else if(transactions.user_type === 2){
            user_type = this._trans.instant('label-title.store');
            email = transactions.store_detail[0].email;
            currency = transactions.provider_detail[0].wallet_currency_code;
          }
          if(transactions.transfer_status === 1){
            transfer_status = this._trans.instant('label-title.transfer')
          } else if (transactions.transfer_status === 0){
            transfer_status = this._trans.instant('label-title.not-transfer')
          }


          all_transactions.push({
            user_type,
            email,
            date: transactions.created_at,
            country: transactions.country_details.country_name,
            currency,
            amount: transactions.amount,
            transfer_status,
          })
          this._helper.export_csv(all_transactions, Object.keys(all_transactions[0]), 'transaction history')
        })
      }
    })
  }

  onChangeItemsPerPage(page){
    this.itemsPerPage = page
  }
}
