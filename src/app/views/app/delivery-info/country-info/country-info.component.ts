import { Component, OnInit, ViewChild } from '@angular/core';
import { CountryService } from '../../../../services/country.service';
import { UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { Helper } from '../../../../shared/helper';
import { CityService } from '../../../../services/city.service';
import { AddNewTaxModalComponent } from 'src/app/containers/pages/add-new-tax-modal/add-new-tax-modal.component';
import { LangService } from 'src/app/shared/lang.service';
import { TabsetComponent } from 'ngx-bootstrap/tabs';
declare let google;

@Component({
  selector: 'app-country-info',
  templateUrl: './country-info.component.html',
  styleUrls: ['./country-info.component.scss']
})
export class CountryInfoComponent implements OnInit {
  country= true;
  city ;
  addCity :boolean =false;
  is_next_clicked:boolean = false;

  all_countries = []
  country_form: UntypedFormGroup
  redeem_form:UntypedFormGroup
  country_subscriber: Subscription
  city_subscriber: Subscription
  city_select_subscriber: Subscription
  countries_list: any
  city_list: any = []
  selected_country: any = null
  selected_city: any = null
  city_search_text: ""
  country_search_text: ""
  taxes: any = [];
  taxes_id: any = [];
  edit = false;
  is_tax_edit_index: any = null
  updateCityId:string;
  updateCountryId:string;

  @ViewChild('countryTabs', { static: false }) countryTabs?: TabsetComponent;
  @ViewChild('AddNewTaxModal', {static: true}) AddNewTaxModal: AddNewTaxModalComponent;
  
  is_add: boolean = true;
  DEFAULT_FLAG = this._helper.DEFAULT_IMAGE_PATH.DEFAULT_FLAG;

  constructor(private _countryService: CountryService,
    public _helper: Helper,
    private _cityService: CityService,
    public _lang: LangService) { }

  ngOnInit(): void {
    this.country_subscriber = this._countryService._countryObservable.subscribe((data) => {
      if(data){
        this.updateCountryId=data;
        this.getCountryList()
      }
    })
    this.getCountryList()
    this._countryService.getAllCountry().then((res => {
      this.all_countries = res.country_list
    }))
    this.city_subscriber = this._cityService._cityObservable.subscribe((data) => {
      if(data){
        this.updateCityId=data;
        this.getCityList()
      }
    })
    this.city_select_subscriber = this._cityService._unselectCity.subscribe(() => {
      this.selected_city = null
    })
    this._initCountryForm()
    this.onChangeDriverReq();
    this.getCityList()
    this.taxes = []
    setTimeout(() => {
      if(!this._helper.has_permission(this._helper.PERMISSION.ADD)){
        this.country_form.disable();
        this.redeem_form.disable();
      }
    }, 200);
  }

  _initCountryForm() {
    let numericRegex = /^[a-zA-Z0-9]+$/
    let numericValidation = [
      Validators.required,
      Validators.pattern(numericRegex),
      Validators.max(9999999999)
    ]
  
    this.country_form = new UntypedFormGroup({
      country_name: new UntypedFormControl(null, Validators.required),
      country_code: new UntypedFormControl(null, Validators.required),
      country_code_2: new UntypedFormControl(null),
      country_id: new UntypedFormControl(null),
      country_phone_code: new UntypedFormControl(null, Validators.required),
      country_timezone: new UntypedFormControl(null),
      currency_code: new UntypedFormControl(null, Validators.required),
      currency_rate: new UntypedFormControl(null, Validators.required),
      currency_sign: new UntypedFormControl(null, Validators.required),
  
      is_ads_visible: new UntypedFormControl(false, Validators.required),
      is_auto_transfer_for_deliveryman: new UntypedFormControl(false, Validators.required),
      is_auto_transfer_for_store: new UntypedFormControl(false, Validators.required),
      is_business: new UntypedFormControl(true, Validators.required),
      is_distance_unit_mile: new UntypedFormControl(false, Validators.required),
      is_referral_user: new UntypedFormControl(false, Validators.required),
      is_referral_provider: new UntypedFormControl(false, Validators.required),
      is_referral_store: new UntypedFormControl(false, Validators.required),

      auto_transfer_day_for_deliveryman: new UntypedFormControl(0),
      auto_transfer_day_for_store: new UntypedFormControl(0),
      no_of_user_use_referral: new UntypedFormControl(0, numericValidation),
      referral_bonus_to_user: new UntypedFormControl(0, numericValidation),
      referral_bonus_to_user_friend: new UntypedFormControl(0, numericValidation),
      no_of_provider_use_referral: new UntypedFormControl(0, numericValidation),
      referral_bonus_to_provider: new UntypedFormControl(0, numericValidation),
      referral_bonus_to_provider_friend: new UntypedFormControl(0, numericValidation),
      no_of_store_use_referral: new UntypedFormControl(0, numericValidation),
      referral_bonus_to_store: new UntypedFormControl(0, numericValidation),
      referral_bonus_to_store_friend: new UntypedFormControl(0, numericValidation),
      country_lat_long: new UntypedFormControl([0,0]),
      taxes: new UntypedFormControl([]),
      allow_next_trip_when_near_destination: new UntypedFormControl(false, Validators.required),
      next_trip_when_near_destination_radius: new UntypedFormControl(0, Validators.required),
      is_send_money_for_user: new UntypedFormControl(true, Validators.required),
      is_send_money_for_provider: new UntypedFormControl(true, Validators.required),
    })

    this.redeem_form = new UntypedFormGroup({
      is_user_redeem_point_reward_on: new UntypedFormControl(false),
      order_redeem_point: new UntypedFormControl(0, numericValidation),
      tip_redeem_point: new UntypedFormControl(0, numericValidation),
      referring_redeem_point_to_user: new UntypedFormControl(0, numericValidation),
      referring_redeem_point_to_users_friend: new UntypedFormControl(0, numericValidation),
      provider_review_redeem_point_for_user: new UntypedFormControl(0, numericValidation),
      store_review_redeem_point_for_user:new UntypedFormControl(0, numericValidation),
      user_minimum_point_require_for_withdrawal:new UntypedFormControl(0 ,numericValidation),
      user_redeem_point_value: new UntypedFormControl(0,[Validators.required, Validators.max(99999999)]),

      is_provider_redeem_point_reward_on: new UntypedFormControl(false),
      daily_accepted_order_count_for_redeem_point: new UntypedFormControl(0, numericValidation),
      daily_accepted_order_redeem_point: new UntypedFormControl(0, numericValidation),
      daily_completed_order_count_for_redeem_point: new UntypedFormControl(0, numericValidation),
      daily_completed_order_redeem_point: new UntypedFormControl(0, numericValidation),
      rating_average_for_redeem_point: new UntypedFormControl(0, numericValidation),
      high_rating_redeem_point: new UntypedFormControl(0, numericValidation),
      user_review_redeem_point_for_provider: new UntypedFormControl(0, numericValidation),
      provider_minimum_point_require_for_withdrawal:new UntypedFormControl(0 ,numericValidation),
      provider_redeem_point_value: new UntypedFormControl(0,[Validators.required, Validators.max(99999999)]),
      
      is_store_redeem_point_reward_on: new UntypedFormControl(false),
      daily_accepted_order_count_for_store_redeem_point: new UntypedFormControl(0, numericValidation),
      daily_accepted_order_redeem_point_for_store: new UntypedFormControl(0, numericValidation),
      daily_completed_order_count_for_store_redeem_point : new UntypedFormControl(0, numericValidation),
      daily_completed_order_redeem_point_for_store: new UntypedFormControl(0, numericValidation),
      high_rating_redeem_point_for_store: new UntypedFormControl(0, numericValidation),
      rating_average_for_store_redeem_point: new UntypedFormControl(0, numericValidation),
      user_review_redeem_point_for_store: new UntypedFormControl(0, numericValidation),
      store_minimum_point_require_for_withdrawal:new UntypedFormControl(0 ,numericValidation),
      store_redeem_point_value: new UntypedFormControl(0,[Validators.required, Validators.max(99999999)])
    })
    this.autoDay();
  }
  getCountryList() {
    this._countryService.fetch().then(res => {
      if (res.success) {
        this.countries_list = res.countries
        if(this.updateCountryId){
          let index = this.countries_list.findIndex((x:any) => x._id == this.updateCountryId)
          this.onSelectCountry(res.countries[index])
        }
      }else{
        this.countries_list = []
      }
    })
  }

  getCityList() {
    this._cityService.fetch().then(res => {
      if (res.success) {
        this.city_list = res.cities
        if(this.updateCityId){
          let index = this.city_list.findIndex((x:any) => x._id == this.updateCityId)
          this.onSelectCity(res.cities[index])
        }    
      }else{
        this.city_list = []
      }
    })
  }

  CountryShow(): void {
    this.selected_city = null;
    this.addCity=false;
    this.country = true;
    this.city = false;
  }
  CityShow(): void {
    this.addCity=true;
    this.city = true;
    this.country = false;
  }
  AddCountry(): void {
    this.edit = false;
    this.selected_country = null
    this._initCountryForm()
    this.is_next_clicked = false;
    setTimeout(() => {
      this.country_form.enable();
      this.redeem_form.enable();
    }, 200);
  }
  AddCity(): void {
    this.addCity = true;
    this.selected_city = null
    this._cityService._addCity.next(true)
    this.is_add = false;
    setTimeout(() => {
      this.is_add = true
    }, 1000)
  }

  nextPage(tabNumber: number): void {
    if (this.country_form.invalid) {
      this.country_form.markAllAsTouched();
    }else{
      this.is_next_clicked = false;
      this.selectTab(tabNumber);
    }
  }

  async saveCountry(): Promise<void> {
    if(this.country_form.invalid){
      this.is_next_clicked = true;
      this.country_form.markAllAsTouched();
      return ;
    }
    if(this.redeem_form.invalid){
      this.redeem_form.markAllAsTouched();
      return;
    }
    const invalid = [];
    const controls = this.country_form.controls;
    for (const name in controls) {
      if (controls[name].invalid) {
        invalid.push(name);
      }
    }
    console.log(invalid);
    console.clear()
    if (this.country_form.value.country_lat_long[0] == 0 || this.country_form.value.country_lat_long[1] == 0) {
      let geocoder = new google.maps.Geocoder();
      let request = { address: this.country_form.value.country_name };
       await geocoder.geocode(request, (results, status) => {
        if (status == google.maps.GeocoderStatus.OK) {
          let countryLatLong = [0, 0]
          countryLatLong[0] = results[0].geometry.location.lat()
          countryLatLong[1] = results[0].geometry.location.lng()
          this.country_form.patchValue({ country_lat_long: countryLatLong })
        }
      });
    }
    if(this.country_form.valid || this.redeem_form.valid){
      this.is_next_clicked = false;
      if (this.country_form.value.country_id) {
        this._countryService.updateCountry({...this.country_form.value,...this.redeem_form.value})
      } else {
        this._countryService.addCountry({...this.country_form.value,...this.redeem_form.value})
      }
      this._initCountryForm();
      this.selected_country = null
      this.is_tax_edit_index = null
      this.taxes = []
    }
  }

  onChangeCountry() {
    this.country_form.patchValue({
      currency_sign: null,
      country_timezone: null,
      currency_code: null,
      country_phone_code: null,
      country_code: null,
      country_code_2: null,
    })
    if (this.country_form.value.country_name) {
      this._countryService.getCountryData({ countryname: this.country_form.value.country_name }).then(res => {
        if (res.success) {
          this.country_form.patchValue({
            currency_sign: res.data.currency_symbol,
            country_timezone: res.data.timezone,
            currency_code: res.data.currencies[0],
            country_phone_code: res.data.countryCallingCodes[0],
            country_code: res.data.alpha2,
            country_code_2: res.data.alpha3,
          })
        } else {
          this._initCountryForm()
        }
      })
    }
  }

  onSelectCountry(country) {
    this._initCountryForm();
    this.edit = true
    this.selected_country = country;
    this.country_form.patchValue({ ...this.selected_country, country_id: this.selected_country?._id })
    if(this.selected_country?.provider_redeem_settings?.length>0){
      this.redeem_form.patchValue({...this.selected_country?.provider_redeem_settings[0]})
    }
    if(this.selected_country?.user_redeem_settings?.length>0){
      this.redeem_form.patchValue({...this.selected_country?.user_redeem_settings[0]})
    }
    if(this.selected_country?.store_redeem_settings?.length>0){
      this.redeem_form.patchValue({...this.selected_country?.store_redeem_settings[0]})
    }
    this.taxes = country.taxes_details
    this.taxes_id = this.country_form.value.taxes
    setTimeout(() => {
      if(this.selected_country){
        if((this._helper.has_permission(this._helper.PERMISSION.ADD) && !this._helper.has_permission(this._helper.PERMISSION.EDIT)) || (!this._helper.has_permission(this._helper.PERMISSION.ADD) && !this._helper.has_permission(this._helper.PERMISSION.EDIT))){
          this.country_form.disable();
          this.redeem_form.disable();
        }else{
          this.country_form.enable();
          this.redeem_form.enable();
        }
      }else{
        this.country_form.enable();
        this.redeem_form.enable();
      }
      this.onChangeDriverReq();
    }, 200);
    this.autoDay();
  }

  onSelectCity(city) {
    this.selected_city = city
    this._cityService.getCityDetail({ city_id: city._id }).then((res) => {
      if (res.success) {
        this._helper.selected_city_id = res.city._id
        this._cityService._citySelect.next(res)
      }
    })
  }

  minMaxValidation(evt) {
    if (this.country_form.value.minimum_phone_number_length > this.country_form.value.maximum_phone_number_length) {
      this.country_form.patchValue({
        maximum_phone_number_length: null
      })
    }
  }

  showAddNewTax() {
    this.AddNewTaxModal.show(null, this.selected_country._id)
  }

  taxAdded(event) {
    if (this.is_tax_edit_index !== null) {
      this.taxes[this.is_tax_edit_index] = event
    } else {
      this.taxes.push(event)
      this.taxes_id.push(event._id)
    }
    this.country_form.patchValue({ taxes: this.taxes })
  }

  onEdit(index) {
    this.AddNewTaxModal.show(this.taxes[index], this.selected_country._id)
    this.is_tax_edit_index = index
  }

  ngOnDestroy(): void {
    if (this.AddNewTaxModal.modalRef) {
      this.AddNewTaxModal.onClose()
    }
    this.country_subscriber.unsubscribe()
    this.city_subscriber.unsubscribe()
    this.city_select_subscriber.unsubscribe()
    this._countryService._countryChanges.next(null)
    this._cityService._cityChanges.next(null)
  }

  // auto transfer day inputs conditions
  autoDay(){
    if(this.country_form.value.is_auto_transfer_for_deliveryman){
      this.country_form.get('auto_transfer_day_for_deliveryman').setValidators([Validators.min(1),Validators.max(366),Validators.required])
      if(this.country_form.value.auto_transfer_day_for_deliveryman == 0){
        this.country_form.patchValue({auto_transfer_day_for_deliveryman:0})
      }
    }else{
      this.country_form.get('auto_transfer_day_for_deliveryman').setValidators([])
      if(!this.country_form.value.auto_transfer_day_for_deliveryman){
        this.country_form.patchValue({auto_transfer_day_for_deliveryman:0})
      }
    }
    if(this.country_form.value.is_auto_transfer_for_store){
      this.country_form.get('auto_transfer_day_for_store').setValidators([Validators.min(1),Validators.max(366),Validators.required])
      if(this.country_form.value.auto_transfer_day_for_store == 0){
        this.country_form.patchValue({auto_transfer_day_for_store:0})
      }
    }else{
      this.country_form.get('auto_transfer_day_for_store').setValidators([]);
      if(!this.country_form.value.auto_transfer_day_for_store){
        this.country_form.patchValue({auto_transfer_day_for_store:0})
      }
    }
  }

  selectTab(tabId: number): void {
    if (this.countryTabs?.tabs[tabId]) {
      this.countryTabs.tabs[tabId].active = true;
    }
  }

  onChangeDriverReq(){
    if (!this.country_form.value.allow_next_trip_when_near_destination) {
      this.country_form.patchValue({ next_trip_when_near_destination_radius: 0 });
      this.country_form.get('next_trip_when_near_destination_radius').disable();
    }else{
      this.country_form.get('next_trip_when_near_destination_radius').enable();
    }
  }
}
