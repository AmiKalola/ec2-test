import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, FormsModule as FormsModuleAngular, ReactiveFormsModule } from '@angular/forms';
import { ContextMenuModule } from '@perfectmemory/ngx-contextmenu';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ModalModule } from 'ngx-bootstrap/modal';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { RatingModule } from 'ngx-bootstrap/rating';
import { NgSelectModule } from '@ng-select/ng-select';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { DeliveryInfoRoutingModule } from './delivery-info.routing';
import { DeliveryInfoComponent } from './delivery-info.component';
import { DInfoComponent } from './d-info/d-info.component';
import { TimepickerModule } from 'ngx-bootstrap/timepicker';
import { LayoutContainersModule } from 'src/app/containers/layout/layout.containers.module';
import { PagesContainersModule } from 'src/app/containers/pages/pages.containers.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { CountryInfoComponent } from './country-info/country-info.component';
import { CityInfoComponent } from './city-info/city-info.component';
import { DFeesInfoComponent } from './d-fees-info/d-fees-info.component';
import { PromoCodeComponent } from './promo-code/promo-code.component';
import { AdvertiseComponent } from './advertise/advertise.component';
import { WalletHistoryComponent } from './wallet-history/wallet-history.component';
import { WalletRequestComponent } from './wallet-request/wallet-request.component';
import { MassNotificationComponent } from './mass-notification/mass-notification.component';
import { RefferalCodeComponent } from './refferal-code/refferal-code.component';
import { PipeModule } from 'src/app/pipes/pipe.module';
import { TransactionHistoryComponent } from './transaction-history/transaction-history.component';
import { AdminServiceComponent } from './admin-service/admin-service.component';
import { DirectivesModule } from 'src/app/directives/directives.module';
import { RedeemHistoryComponent } from './redeem-history/redeem-history.component';
import { EcommerceServiceComponent } from './ecommerce-service/ecommerce-service.component';
import { HotkeyModule } from 'angular2-hotkeys';

@NgModule({
  declarations: [DeliveryInfoComponent, DInfoComponent, CountryInfoComponent, CityInfoComponent, DFeesInfoComponent, PromoCodeComponent, AdvertiseComponent, WalletHistoryComponent, WalletRequestComponent, MassNotificationComponent, RefferalCodeComponent, TransactionHistoryComponent, AdminServiceComponent, RedeemHistoryComponent, EcommerceServiceComponent],
  imports: [
    CommonModule,
    DeliveryInfoRoutingModule,
    FormsModule,
    CollapseModule,
    SharedModule,
    LayoutContainersModule,
    CommonModule,
    PagesContainersModule,
    FormsModuleAngular,
    NgSelectModule,
    RatingModule.forRoot(),
    ReactiveFormsModule,
    PaginationModule.forRoot(),
    TabsModule.forRoot(),
    BsDatepickerModule,
    ModalModule.forRoot(),
    BsDropdownModule.forRoot(),
    AccordionModule.forRoot(),
    TimepickerModule.forRoot(),
    ContextMenuModule,
    TooltipModule,
    HotkeyModule.forRoot(),
    PipeModule,
    DirectivesModule,
  ]
})
export class DeliveryInfoModule { }
