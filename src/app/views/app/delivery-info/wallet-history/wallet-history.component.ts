import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Helper } from 'src/app/shared/helper';
import { LangService } from 'src/app/shared/lang.service';
import { WalletService } from '../../../../services/wallet.service';
import { PER_PAGE_LIST } from 'src/app/shared/constant';
@Component({
  selector: 'app-wallet-history',
  templateUrl: './wallet-history.component.html',
  styleUrls: ['./wallet-history.component.scss']
})
export class WalletHistoryComponent implements OnInit {
  direction = localStorage.getItem('direction');

  search_field: any =  { label: '', value: '' };
  user_field: any = { label: '', value: '' };
  comment_field: any = { label: '', value: '' };
  is_clear_disabled:boolean=true;
  wallet_history:any = []
  items_per_page = 20;
  item_options_per_page = PER_PAGE_LIST;
  total_page = 0;
  current_page = 1;
  USER_TYPE:any
  itemOptionsOrders: any = []
  userOption: any = []
  commentOption: any = []
  user_type = 0;
  comment_type = 0;
  search: any = '';
  item_bsRangeValue: any;
  start_date = "";
  end_date = "";
  created_date:Date;
  todayDate:Date=new Date();
  timezone_for_display_date:string = '';

  constructor(private _walletService: WalletService,
    public _helper: Helper, 
    public _lang: LangService,
    public _trans: TranslateService) {
    this.itemOptionsOrders = [
      {label: 'label-title.user', value: 'user_detail.first_name'},
      {label: 'label-title.deliveryman', value: 'provider_detail.first_name'},
      {label: 'label-title.store', value: 'store_detail.name'},
      {label: 'label-title.comment', value: 'wallet_description'},
      {label: 'label-title.id', value: 'unique_id'},
    ];

    this.userOption = [
      {label: 'label-title.all', value: 0},
      {label: 'label-title.user', value: _helper.ADMIN_DATA_ID.USER},
      {label: 'label-title.deliveryman', value: _helper.ADMIN_DATA_ID.PROVIDER},
      {label: 'label-title.store', value: _helper.ADMIN_DATA_ID.STORE}
    ]

    this.commentOption = [
      {label: 'label-title.all', value: 0},
      {label: 'label-title.set-by-admin', value: _helper.WALLET_COMMENT_ID.SET_BY_ADMIN},
      {label: 'label-title.added-by-card', value: _helper.WALLET_COMMENT_ID.ADDED_BY_CARD},
      {label: 'label-title.added-by-referral', value: _helper.WALLET_COMMENT_ID.ADDED_BY_REFERRAL},
      {label: 'label-title.order-charged', value: _helper.WALLET_COMMENT_ID.ORDER_CHARGED},
      {label: 'label-title.order-refund', value: _helper.WALLET_COMMENT_ID.ORDER_REFUND},
      {label: 'label-title.set-order-profit', value: _helper.WALLET_COMMENT_ID.SET_ORDER_PROFIT},
      {label: 'label-title.order-cancellation-charge', value: _helper.WALLET_COMMENT_ID.ORDER_CANCELLATION_CHARGE},
      {label: 'label-title.set-by-wallet-request', value: _helper.WALLET_COMMENT_ID.SET_BY_WALLET_REQUEST}
    ]
  }

  ngOnInit(): void {
    this._helper.created_date.subscribe(data => {
      if(data){
        let date = new Date(data)
        this.created_date = date;        
      }
    })

    this._helper.display_date_timezone.subscribe(data => {
      this.timezone_for_display_date = data;
    })

    this.USER_TYPE = this._helper.USER_TYPE;
    this.list();
  }

  list(){
    let json = {
      end_date: this.end_date,
      page: this.current_page,
      search_field: this.search_field.value,
      search_value: this.search,
      start_date: this.start_date,
      user_type: this.user_field.value,
      wallet_comment_id: this.comment_field.value,
      number_of_rec:this.items_per_page
    }
    this._walletService.get_wallet_history(json).then(res=>{
      if(res.success){
        this.total_page = res.pages
        this.wallet_history = res.wallet
      } else {
        this.wallet_history = []
        this.total_page = res.pages
      }
    })
  }
  onChangeOrderBy(item): void  {
    this.search_field = item;
    this.is_clear_disabled = false;
  }

  onChangeUserField(item): void {
    this.user_field = item
    this.is_clear_disabled = false;
  }

  onChangeCommentField(item): void {
    this.comment_field = item
    this.is_clear_disabled = false;
  }

  onChangeItemsPerPage(item){
    this.items_per_page = item
    this.list()
  }

  pageChanged(event){
    this.current_page = event.page
    this.list()
  }

  onSearch(){
    this.is_clear_disabled = false;
    this.current_page=1;
    if(this.item_bsRangeValue){
      this.start_date = this.item_bsRangeValue[0];
      this.end_date = this.item_bsRangeValue[1];
    }
    this.list()
  }

  clickonExport(){
    let json = {
      end_date: this.end_date,
      page: this.current_page,
      search_field: this.search_field.value,
      search_value: this.search,
      start_date: this.start_date,
      user_type: this.user_field.value,
      wallet_comment_id: this.comment_field.value,
      number_of_rec:this.items_per_page
    }
    this._walletService.get_wallet_history(json).then(res=>{
      if(res.success){
        res.wallet.forEach(history => {
          delete history._id
          if(history.user_detail){
            history.name = history.user_detail.first_name + " " + history.user_detail.last_name
            history.user_type = this._trans.instant('label-title.user')
            delete history.user_detail
          } else if (history.provider_detail){
            history.name = history.provider_detail.first_name + " " + history.provider_detail.last_name
            delete history.provider_detail
            history.user_type = this._trans.instant('label-title.provider')
          } else if (history.store_detail){
            history.name = history.store_detail.name[this._lang.selectedlanguageIndex]
            delete history.store_detail
            history.user_type = this._trans.instant('label-title.store')
          }
        });
        this._helper.export_csv(res.wallet, Object.keys(res.wallet[0]), 'wallet history')
      }
    })
  }
  clear_filter(){
    this.item_bsRangeValue = '';
    this.start_date = '';
    this.end_date = '';
    this.current_page = 1;
    this.items_per_page  = 20; 
    this.search = '';
    this.search_field =  { label: '', value: '' };
    this.user_field = { label: '', value: '' };
    this.comment_field = { label: '', value: '' };
    this.list()
    this.is_clear_disabled = true;
  }
}
