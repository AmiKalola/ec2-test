import { Component, OnInit, ViewChild } from '@angular/core';
import { UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { DELIVERY_TYPE, DELIVERY_TYPE_CONSTANT, CHARGES_TYPE } from 'src/app/shared/constant';
import { Helper } from 'src/app/shared/helper';
import { DeliveryFeesService } from 'src/app/services/delivery-fees.service';
import { DeliveryService } from 'src/app/services/delivery.service';
import { AddNewItemModalComponent } from 'src/app/containers/pages/add-new-item-modal/add-new-item-modal.component';
import { AddNewSubCategoryModalComponent } from 'src/app/containers/pages/add-new-sub-category-modal/add-new-sub-category-modal.component';
import { AddNewCategoryModalComponent } from 'src/app/containers/pages/add-new-category-modal/add-new-category-modal.component';
import { AddNewModifierModalComponent } from 'src/app/containers/pages/add-new-modifier-modal/add-new-modifier-modal.component';
import { Subscription } from 'rxjs';
import { SubcategoryService } from 'src/app/services/subcategory.service';
import { LangService } from 'src/app/shared/lang.service';
import { ItemModel, ItemService } from 'src/app/services/item.service';
import { environment } from 'src/environments/environment';
import { IProduct } from 'src/app/data/api.service';
import { CategoryService } from 'src/app/services/category.service';
import { Category } from 'src/app/models/category.model';
import { ModifierService } from 'src/app/services/modifier.service';
import { CommonService } from 'src/app/services/common.service';
import { DeliveryPriceNotSetModal } from 'src/app/containers/pages/add-charges-modal/delivery-price-not-set-modal.component';
import { ContextMenuComponent } from '@perfectmemory/ngx-contextmenu';

@Component({
  selector: 'app-admin-service',
  templateUrl: './admin-service.component.html',
})

export class AdminServiceComponent implements OnInit {
  displayMode = 'image';
  CHARGES_TYPE = CHARGES_TYPE
  DELIVERY_TYPE_CONSTANT = DELIVERY_TYPE_CONSTANT
  DELIVERY_TYPE = DELIVERY_TYPE
  ITEM_DEFAULT_IMAGE = this._helper.DEFAULT_IMAGE_PATH.CATEGORY;
  CATEGORY_DEFAULT_IMAGE = this._helper.DEFAULT_IMAGE_PATH.CATEGORY;
  IMAGE_URL = environment.imageUrl;
  selected: IProduct[] = [];
  data: IProduct[] = [];
  fromState = [
    { "id": 11, "title": "button-title.accepted" },
    { "id": 13, "title": "button-title.coming" },
    { "id": 15, "title": "button-title.arrived" }
  ]

  toState = [
    { "id": 13, "title": "button-title.coming" },
    { "id": 15, "title": "button-title.arrived" },
    { "id": 19, "title": "button-title.started" }
  ]
  temptoStoreStatues = this.toState
  tempfromStoreStatues = this.fromState
  serviceForm: UntypedFormGroup;
  delivery_fee_search: any;
  service_list: any = [];
  delivery_search: any = '';
  delivery_list: any = [];
  delivery_subscriber: Subscription
  languages_subscription: Subscription;
  delivery_name: any;
  description: any;
  selected_delivery_id: any;
  sequenceNumber: any = [];
  subcategories: any = [];
  categories: Category[] = [];
  selectedSubcategory: any;
  items: any = [];
  productArray: any = [];
  delivery_type: any;
  modifiers: any = [];
  is_store_can_add_category: boolean = true
  addDfee = true;
  DeliveryBtn = true;
  VehicalBtn;
  countries: any = [];
  country_id: any;
  cities: any = [];
  city_id: any;
  deliveries_type_list: any = [];
  vehicle: any = '';
  vehicles: any = [];
  vehicle_id: any;
  is_business: boolean = false;
  is_use_distance_calculation: boolean = false;
  zones: any = [];
  to_zone: any;
  from_zone: any;
  zone_price: any;
  zone_to_zone_price: any = [];
  admin_profit_mode_list: any = [];
  admin_profit_mode_on_delivery: any;
  admin_profit_value_on_delivery: any;
  base_price_distance: any
  base_price: any;
  price_per_unit_distance: any;
  price_per_unit_time: any;
  waiting_time_start_after_minute: any;
  service_tax: any;
  min_fare: any;
  is_add_distance_price: boolean = false;
  is_distance_unit_mile: boolean = false;
  from_distance: any;
  to_distance: any;
  delivery_fee: any;
  delivery_price_setting: any = [];
  is_distance_calculation_price_added: boolean = false;
  is_to_distance_gt_from_distance: boolean = false;
  is_edit: boolean = false;
  service_id: any;
  vehicle_list: any = [];
  vehicle_search: any;
  selected_delivery_fees: any;
  is_invalid: boolean = false;
  _vehicleSubscription: Subscription
  all_vehicles: any = []
  services: any = []
  currency_sign: any = '$'
  is_edit_zone: boolean = false
  edit_zone_id: any = ''
  is_default: boolean = false;
  can_set_default_price: boolean = true
  taxes: Array<any> = []
  service_taxes: Array<any> = []
  tax_ids: Array<any> = []
  waiting_time_start_after: number= 0;
  waiting_time_charge: number= 0;
  cancellation_charge: number= 0;
  cancellation_charge_type;
  want_to_set_waiting_time: boolean = false;
  want_to_set_cancellation_charges: boolean = false;
  is_distance_alreay_exist: boolean = false;
  is_Invalid_distance_and_price: boolean = false
  edit_service_charge_id: any;
  services_list: Array<any> = [];
  temp_services_list: Array<any> = [];
  service_type_id: any = null;
  service_charge_id: any = null;
  selectAllState = '';
  is_add: boolean = false;
  city_detail: any = {
    city_id: '',
    city_name: ''
  }
  subcategorySubscription: Subscription;
  itemSubscription: Subscription;
  submitForm:boolean=false;
  selected_country:any

  @ViewChild('basicMenu') public basicMenu: ContextMenuComponent<any>;
  @ViewChild('DeliveryPriceNotSetModal', { static: true }) DeliveryPriceNotSetModal: DeliveryPriceNotSetModal;
  @ViewChild('addNewItemModalRef', { static: true }) addNewItemModalRef: AddNewItemModalComponent;
  @ViewChild('addNewSubCategoryRef', { static: true }) addNewSubCategoryModalRef: AddNewSubCategoryModalComponent;
  @ViewChild('addNewCategoryRef', { static: true }) addNewCategoryModalRef: AddNewCategoryModalComponent;
  @ViewChild('addNewModifierModalRef', { static: true }) addNewModifierModalRef: AddNewModifierModalComponent;
  @ViewChild('tabset', { static: true }) tabset: any;
  @ViewChild('sevice_city_tab', { static: true }) sevice_city_tab: any;
  cancellation_setting: boolean = false;
  is_item: boolean = false;
  categorySubscription: Subscription;
  selectedTabset: number = 0;


  constructor(public _helper: Helper,
    private _deliverFeesServcise: DeliveryFeesService,
    private _deliveryService: DeliveryService,
    private _lang: LangService,
    private _subcategoryService: SubcategoryService,
    private itemService: ItemService,
    private _categoryService: CategoryService,
    private _modifierService: ModifierService,
    private _commonService: CommonService) { }

  ngOnInit(): void {
  
    this.subcategorySubscription = this._subcategoryService._subcategoryObservable.subscribe(data => {
      if (data) {
        this.loadSubcategoryList(this._helper.selected_store_id, this._helper.delivery_type)
      }
    })
    this.itemSubscription = this.itemService._itemObservable.subscribe((data) => {
      if (data) {
        this.loadItemList(false)
      }
    })
    this.categorySubscription = this._categoryService._categoryObservable.subscribe((data) => {
      if (data) {
        this.getCategoryList(this._helper.selected_store_id)
      }
    })
    this.itemSubscription = this._modifierService._modifierObservable.subscribe((data) => {
      if (data) {
        this.getModifierList()
      }
    })  

    this.get_service_list()
    this.get_delivery_list()
    this.get_country_list()
    this._initForm()
    this._initData()

    if(this._helper.devliverySetfees){
      this.onDelivery(this._helper.devliverySetfees)
      setTimeout(() => {
        let elmnt: any = document.getElementById(
          this._helper.devliverySetfees._id
        );
        elmnt.scrollIntoView({ block: "start", inline: "nearest" });
      }, 500);
    }

    if (this._helper.selected_city && this._helper.selected_store && !this._helper.devliverySetfees) {
      this.sevice_city_tab.tabs[1].active = true
      setTimeout(() => {
        this._deliverFeesServcise.get_service_list().then(res_data => {
          this.service_list = res_data.service.filter(type => (type.delivery_type === DELIVERY_TYPE_CONSTANT.SERVICE || type.delivery_type === DELIVERY_TYPE_CONSTANT.APPOINMENT) && type.delivery_details)
          this.selected_delivery_fees = this.service_list[0]._id
          let index = this.service_list.findIndex(x => x.service_type_id === this._helper.selected_store._id && x.delivery_type === this._helper.selected_store.delivery_type && x.city_id === this._helper.selected_city._id)
          if (index >= 0) {
            this.AddDFee(this.service_list[index])

            setTimeout(() => {
              let elmnt: any = document.getElementById(
                this.service_list[index]._id
              );
              elmnt.scrollIntoView({ block: "start", inline: "nearest" });

            }, 500);

          }
          else {
            this.AddDFee(null)
          }

        })
      }, 100)

    }
  }

  ngOnDestroy() {
    this._helper.devliverySetfees = null;
    this._helper.delivery_type = null
    this._helper.selected_store_id = '';
    this._helper.selected_store = null
    this._helper.selected_city = null;
    this._helper.countryData = null;
    this._helper.changeSelectedCity(this.city_id, false)
    if (this.subcategorySubscription) {
      this.subcategorySubscription.unsubscribe()
    }
    this._helper.clear_height_width();
    this._helper.setDeliveryPrice(null,null);
  }

  _initForm() {
    this.serviceForm = new UntypedFormGroup({
      admin_profit_type: new UntypedFormControl(1, Validators.required),
      admin_profit: new UntypedFormControl(0, Validators.required),
      cancellation_charge_type: new UntypedFormControl(1, Validators.required),
      cancellation_charge: new UntypedFormControl(0, Validators.required),
      cancellation_charge_apply_from: new UntypedFormControl(11, Validators.required),
      cancellation_charge_apply_till: new UntypedFormControl(19, Validators.required),
      is_business: new UntypedFormControl(false),
    })
  }

  get_country_list() {
    this._deliverFeesServcise.get_server_country_list().then(res_data => {
      this.countries = res_data['countries']
    })
  }

  country_selected() {
    let country = this.countries.filter(value =>  this.country_id == value._id)
    this.selected_country= country[0];
    
    this.delivery_type = null
    this.city_id = null
    this.vehicle_id = null
    this._deliverFeesServcise.get_city_lists(this.country_id).then(res_data => {
      this.cities = res_data.cities
    })
  }

  city_selected() {
    this.delivery_type = null
    this.vehicle_id = null

    this.vehicles = []
    this._deliverFeesServcise.get_vehicle_list(this.city_id, 1).then(res_data => {
      this.services = res_data.services
      this.all_vehicles = res_data.vehicles
    })
    this.deliveries_type_list = JSON.parse(JSON.stringify(this._helper.DELIVERY_TYPE.filter(d => d.value == 5 || d.value == 6)));
  }

  get_delivery_service_list() {
    this.service_type_id = null
    this.services_list = []
    let json = {
      delivery_type: this.delivery_type
    }
    this.temp_services_list = []
    this._deliverFeesServcise.get_delivery_service_list(json).then(res => {
      this.services_list = res.deliveries;

      this.services_list.forEach(service => {
        let index = this.service_list.findIndex(x => x.service_type_id === service._id && x.delivery_type === this.delivery_type && x.city_id === this.city_id)
        if (index === -1) {
          this.temp_services_list.push(service)
        }
      })
    })
  }

  get_service_list() {
    this._deliverFeesServcise.get_service_list().then(res_data => {
      this.service_list = res_data.service.filter(type => (type.delivery_type === DELIVERY_TYPE_CONSTANT.SERVICE || type.delivery_type === DELIVERY_TYPE_CONSTANT.APPOINMENT) && type.delivery_details)
    })
  }

  get_delivery_list() {
    this._deliveryService.get_delivery_list().then(res_data => {
      if (res_data.success) {
        this.delivery_list = res_data.deliveries.filter(i => i.delivery_type === DELIVERY_TYPE_CONSTANT.SERVICE || i.delivery_type === DELIVERY_TYPE_CONSTANT.APPOINMENT)
      }
    })
  }

  // tab wise api calling
  tabSelection(tab){
    this.selectedTabset=tab;
    this._initData();
  }
  
  _initData() {
    if (this.selected_delivery_id) {
      if(this.selectedTabset==0){
        this.loadSubcategoryList(this.selected_delivery_id, this.delivery_type);
      }else if(this.selectedTabset==1){
        this.getCategoryList(this.selected_delivery_id);
      }else if(this.selectedTabset==2){
        this.getModifierList()
      }
    }
  }

  async getModifierList(){
    this._modifierService.list_group().then(data => {
      if (data.success) {
        this.modifiers = data.specification_group
      }
      else {
        this.modifiers = []
      }
    })
  }

  async getCategoryList(store_id) {
    this._categoryService.list(store_id).then(data => {
      if (data.success) {
        this.categories = data.product_groups;
      } else {
        this.categories = []
      }
    })
  }

  async loadSubcategoryList(store_id, delivery_type) {
    this.sequenceNumber = []
    this._subcategoryService.list(store_id, delivery_type).then(data => {
      if (data.success) {
        this.subcategories = data.product_array;
        if (this.subcategories.length) {
          this.selectedSubcategory = this.subcategories[0]._id;
          this.loadItemList(false)
        } else {
          this.items = []
        }
      }

    })
  }

  loadItemList(boolean) {
    this.itemService.list(this.selectedSubcategory, this.selected_delivery_id).then(data => {
      this.items = []
      this.productArray = data.products
      if (data.success) {
        if (data.products.length > 0) {
          let index = data.products.findIndex(_x => _x._id === this.selectedSubcategory)
          this.items = data.products[index].items
        } else {
          this.items = []
        }
      } else {
        this.items = []
      }
    })
  }
  isSelected(p: IProduct): boolean {
    return this.selected.findIndex(x => x.id === p.id) > -1;
  }

  isSubcategorySelected(s): boolean {
    return s._id.toString() === this.selectedSubcategory ? this.selectedSubcategory.toString() : "";
  }

  onSelectSubcategory(s): void {
    this.selectedSubcategory = s;
    this.loadItemList(false)
  }

  onSelect(item: IProduct): void {
    if (this.isSelected(item)) {
      this.selected = this.selected.filter(x => x.id !== item.id);
    } else {
      this.selected.push(item);
    }
    this.setSelectAllState();
  }
  setSelectAllState(): void {
    if (this.selected.length === this.data.length) {
      this.selectAllState = 'checked';
    } else if (this.selected.length !== 0) {
      this.selectAllState = 'indeterminate';
    } else {
      this.selectAllState = '';
    }
  }

  showAddNewItemModal(is_edit = false, edit_id = null): void {
    if(this._helper.has_permission(this._helper.PERMISSION.ADD) && !is_edit){     
      this.addNewItemModalRef.show(is_edit, edit_id, this.selectedSubcategory);
    }
    if(this._helper.has_permission(this._helper.PERMISSION.EDIT) && is_edit){     
      this.addNewItemModalRef.show(is_edit, edit_id, this.selectedSubcategory);
    }      
    this.is_item = true
  }
  showAddNewCategoryModal(is_edit = false, edit_id = null): void {
    if(this._helper.has_permission(this._helper.PERMISSION.ADD) && !is_edit){     
      this.addNewCategoryModalRef.show(is_edit, edit_id, this.categories.length);
    }
    if(this._helper.has_permission(this._helper.PERMISSION.EDIT) && is_edit && this.is_store_can_add_category){     
      this.addNewCategoryModalRef.show(is_edit, edit_id, this.categories.length);
    }
  }

  showAddNewSubCategoryModal(is_edit = false, edit_id = null): void {
    this.addNewSubCategoryModalRef.show(is_edit, edit_id);
  }

  showAddNewModifierModal(is_edit = false, edit_id = null): void {
    this.addNewModifierModalRef.show(is_edit, edit_id);
  }

  onContextMenuClick(action: string, item: ItemModel): void {
    switch (action) {
      case 'copy-item':
        this.addNewItemModalRef.show(false, null, this.selectedSubcategory, item);
        break;
      case 'delete-item':
        item.is_visible_in_store = !item.is_visible_in_store;
        this.itemService.update(item);
        break;
      case 'out-of-stock-item':
        item.is_item_in_stock = !item.is_item_in_stock;
        this.itemService.update(item);
        break;
      case 'delete-category':
        this._categoryService.delete(item._id)
        break;
      default:
        break;
    }
  }

  onDelivery(delivery) {
    this.selected_delivery_id = delivery._id
    this.delivery_type = delivery.delivery_type
    this._helper.delivery_type = this.delivery_type
    this._helper.selected_store_id = this.selected_delivery_id
    this.is_store_can_add_category = delivery.is_store_can_create_group
    this.serviceForm.value.is_business = delivery.is_business;
    this._initData()
  }


  AddDFee(delivery) {
    if(delivery){
      this.selected_country = delivery?.country_details
    }
    this.submitForm=false;
    if (delivery !== null) {
      this.is_edit = true
      this.is_add = false
      this.selected_delivery_id = delivery.delivery_details._id
      this.delivery_type = delivery.delivery_type
      this._helper.delivery_type = this.delivery_type
      this.selected_delivery_fees = delivery._id
      this._helper.selected_store_id = this.selected_delivery_id
      this.city_detail.city_id = delivery.city_details._id
      this.city_detail.city_name = delivery.city_details.city_name
      this.tabset.tabs[0].active = true
      this._commonService.copyData({ city_id: delivery.city_details._id, store_id: this.selected_delivery_id }).then(result => {
        if (result.success) {
          this._helper.changeSelectedCity(this.city_detail, true)
          this._deliverFeesServcise.get_service_details(delivery._id).then(res_data => {
            this.service_id = res_data.service._id
            this.serviceForm.patchValue({
              is_business: res_data.service.is_business,
              ...res_data.service_charges
            })
            this.country_id = res_data.service.country_details?._id;
            this.city_id = res_data.service.city_details?._id;
            this.city_detail.city_id = this.city_id
            this.city_detail.city_name = res_data.service.city_details?.city_name
            this._helper.changeSelectedCity(this.city_detail, true)
            this.service_charge_id = res_data.service_charges?._id;
            this.service_type_id = res_data.service_charges?.delivery_id;
            this._initData()
          })
        } else {
          this.selected_delivery_id = '';
          this._helper.changeSelectedCity(this.city_detail, true)
          if(this._helper.has_permission(this._helper.PERMISSION.EDIT)){
            this.DeliveryPriceNotSetModal.show({ city: delivery.city_details, delivery, route: "admin" });
          }
        }

      })
    }
    else {
      this.is_edit = false
      this.selected_delivery_fees = null
      this.items = []
      this.modifiers = []
      this.categories = []
      this.subcategories = []
      this.serviceForm.reset()
      this.is_add = true
      this.country_id = ''
      this.city_id = ''
      this.delivery_type = ''
      this.service_type_id = ''
      this.tabset.tabs = this.tabset.tabs.filter(x => x.heading == "PROFIT AND CANCELLATION SETTINGS")
      this.tabset.tabs[0].active = true
    }
    if(this.is_edit){
      if(!this._helper.has_permission(this._helper.PERMISSION.EDIT)){
        this.serviceForm.disable();
      }
    }else{
      this.serviceForm.enable();
    }
  }

  add_service_charges() {
    this.submitForm=true;
    if(this.serviceForm.invalid){
      this.serviceForm.markAllAsTouched();
      return;
    }
    if(!this.service_type_id || !this.city_id || !this.country_id || !this.delivery_type){
      return
    }
   
    let json2 = {
      country_id: this.country_id,
      city_id: this.city_id,
      delivery_type: this.delivery_type,
      is_business: this.serviceForm.value.is_business,
      service_type_id: this.service_type_id,
      service_id: this.service_id
    }
    let json = {
      country_id: this.country_id,
      city_id: this.city_id,
      delivery_id: this.service_type_id,
      ...this.serviceForm.value
    }
    if (!this.is_edit) {

      this._commonService.addServiceCharges(json).then(result => {
        if (result.success) {
          this.serviceForm.reset()
          this.selected_delivery_fees = null
        }
      })
      this._deliverFeesServcise.add_service_price(json2).then(res_data => {
        if (res_data.success) {
          this.get_service_list()
          this.is_invalid = false
          this.serviceForm.reset()
          this.country_id = ''
          this.city_id = ''
          this.delivery_type = ''
          this.service_type_id = ''
        }
      })
    } else {
      json._id = this.service_charge_id
      this._commonService.updateServiceCharges(json).then(result => {
        if (result.success) {
          this.serviceForm.reset();
          this.selected_delivery_fees = null
        }
      })
      this._deliverFeesServcise.update_service(json2).then(res_data => {
        if (res_data.success) {
          this.get_service_list()
          this.is_edit = false
          this.is_invalid = false
          this.serviceForm.reset()
          this.country_id = ''
          this.city_id = ''
          this.delivery_type = ''
          this.service_type_id = ''
        }
      })
    }
    this.submitForm=false;
  }

  onChange(e) {
    if (e.id == "city-price") {
      this.cancellation_setting = true
      this.selected_delivery_fees = ''
      this.selected_delivery_id = ''
      setTimeout(() => {
      this.tabset.tabs[0].active = true
      }, 200);
    }
    else {
      this.is_add = false
      this._helper.changeSelectedCity(this.city_id, false)
      this.selected_delivery_fees = ''
      this.selected_delivery_id = ''
      this.cancellation_setting = false
      this.tabset.tabs[0].active = true
    }
  }

}
