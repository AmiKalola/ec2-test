import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { AddOfferAdvanceSettingModalComponent } from 'src/app/containers/pages/add-offer-advance-setting-modal/add-offer-advance-setting-modal.component';
import { PromocodeService, promocode_page_type } from '../../../../services/promocode.service'
import { Helper } from 'src/app/shared/helper';
import { AddNewPromoInfoModalComponent } from 'src/app/containers/pages/add-new-promo-info-modal/add-new-promo-info-modal.component';
import { PER_PAGE_LIST, PROMO_FOR_ID } from '../../../../shared/constant';
import { DeliveryService } from 'src/app/services/delivery.service';
import { LangService } from 'src/app/shared/lang.service';
@Component({
  selector: 'app-promo-code',
  templateUrl: './promo-code.component.html',
  styleUrls: ['./promo-code.component.scss']
})
export class PromoCodeComponent implements OnInit, OnDestroy {
  manualTrigger=false;

  count: number = 0;
  currentPage = 1;
  items_per_page = 20;
  search_field: any =  { label: 'label-title.name', value: 'promo_code_name' };
  itemOptionsOrders = [
    { label: 'label-title.name', value: 'promo_code_name' }
   ];
  item_options_per_page = PER_PAGE_LIST;
  search_value: string = '';
  promo_codes: any[] = [];
  promocode_page_type: any = promocode_page_type;
  promo_for: any;
  status: any = promocode_page_type.running;
  is_delivery_business: boolean = false;
  selectedBusinessId: string;
  is_store_business:boolean = false ; 
  promo_for_list: { value: number; title: string; }[];
  selectedDeliveryType: any;
  delivery_list: any;
  deliverytype: any;
  PROMO_FOR_ID = PROMO_FOR_ID;

  @ViewChild('addNewModalRef', { static: true }) addNewModalRef: AddOfferAdvanceSettingModalComponent;
  @ViewChild('addNewModalRef1', { static: true }) addNewModalRef1: AddNewPromoInfoModalComponent;
  constructor(public offerService: PromocodeService, public _helper: Helper, private _deliveryService: DeliveryService ,   private _lang: LangService, ) { }

  ngOnInit(): void {
    this.offerService._offerObservable.subscribe(()=>{
      this.get_promocode_list(this.status);
    })
    this.promo_for_list = this._helper.PROMO_FOR_USE

  }

  get_promocode_list(promocode_page_type){
    this.offerService.list(promocode_page_type, this.currentPage, this.items_per_page, this.search_field.value, this.search_value , this.selectedDeliveryType , this.selectedBusinessId).then(res_data => {
      if (res_data.success) {
        this.promo_codes = res_data.promo_codes;
        this.count = res_data.page;
      } else {
        this.promo_codes = [];
      }
    })
  }
  apply(status){
    this.currentPage=1;
    this.get_promocode_list(status)
  }

  clear_filter(){
    this.search_field =  { label: 'Name', value: 'promo_code_name' };
    this.search_value = '';
    this.selectedBusinessId = null; 
    this.promo_for =null;
    this.delivery_list = null
    this.selectedDeliveryType = null;
    this.is_delivery_business = false ;
    this.is_store_business = false ; 
    this.get_promocode_list(this.status)
  }

  changeUserType(promocode_page_type) {
    this.currentPage = 1;
    this.status = promocode_page_type;
    this.get_promocode_list(promocode_page_type)
  }

  pageChanged(event) {
    this.currentPage = event.page;
    this.get_promocode_list(this.status)
  }

  onChangeItemsPerPage(item) {
    this.currentPage=1;
    this.items_per_page = item
    this.get_promocode_list(this.status)
  }

  onChangeOrderBy(item): void {
    this.search_field = item;
  }
  showAddNewModal(promo_id): void {
    this.addNewModalRef1.show(promo_id);
  }
  showPromoInfoModal(promo_id = null): void {
    this.addNewModalRef.show(promo_id);
  }

  getDelivery(data){
    this.selectedDeliveryType = data
    if (data == this._helper.PROMO_FOR_ID.DELIVERIES) {
      this.is_delivery_business = true
    } else {
      this.is_delivery_business = false
      // this.city_business_list = []
    }
    if( data == this._helper.PROMO_FOR_ID.STORE){
      this.is_store_business = true 
    }else{
      this.is_store_business = false 
    }
    this.getCityBusiness(data);
    this.selectedBusinessId = null;
  }

  getCityBusiness(deliveryType) {
    if(deliveryType == this._helper.PROMO_FOR_ID.DELIVERIES){
      this._deliveryService.get_delivery_list().then(res => {
        if (res.success) {
          this.delivery_list = res.deliveries
          
        }
      })
    }

    if(deliveryType == this._helper.PROMO_FOR_ID.STORE){
      this._deliveryService.get_All_store_list().then(res => {
        if (res.success) {
          this.delivery_list = res.stores
        }
      })
    }
    this.delivery_list = []
  }


  getBusiness(business_id){
    this.selectedBusinessId = business_id
  }

  ngOnDestroy(): void {
    if(this.addNewModalRef.modalRef){
      this.addNewModalRef.onClose();
    }
  }
}
