import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DFeesInfoComponent } from './d-fees-info.component';

describe('DFeesInfoComponent', () => {
  let component: DFeesInfoComponent;
  let fixture: ComponentFixture<DFeesInfoComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ DFeesInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DFeesInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
