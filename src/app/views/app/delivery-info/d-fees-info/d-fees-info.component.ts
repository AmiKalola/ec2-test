import { ChangeDetectorRef, Component, ElementRef, OnInit, QueryList, TemplateRef, ViewChild, ViewChildren } from '@angular/core';
import { UntypedFormControl, UntypedFormGroup, NgForm, Validators } from '@angular/forms';
import { NgSelectConfig } from '@ng-select/ng-select';
import { TranslateService } from '@ngx-translate/core';
import {  Subscription } from 'rxjs';
import { AddNewVehicleComponent } from 'src/app/containers/pages/add-new-vehicle/add-new-vehicle.component';
import { DeliveryFeesService } from 'src/app/services/delivery-fees.service'
import { VehicleService } from 'src/app/services/vehicle.service';
import { Helper } from 'src/app/shared/helper';
import { DELIVERY_TYPE, DELIVERY_TYPE_CONSTANT, CHARGES_TYPE , CHARGE_TYPE } from 'src/app/shared/constant';
import { CommonService } from 'src/app/services/common.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { AddNewItemModalComponent } from 'src/app/containers/pages/add-new-item-modal/add-new-item-modal.component';
import { StoreTimeSettingsModalComponent } from 'src/app/containers/pages/store-time-settings-modal/store-time-settings-modal.component';
import { TabsetComponent } from 'ngx-bootstrap/tabs';
import { NotifiyService } from 'src/app/services/notifier.service';

@Component({
  selector: 'app-d-fees-info',
  templateUrl: './d-fees-info.component.html',
  styleUrls: ['./d-fees-info.component.scss']
})
export class  DFeesInfoComponent implements OnInit {
  @ViewChild('adminProfitInput') adminProfitInput: ElementRef;

  btnDisable: boolean = false;
  deleteZonePriceModalRef: BsModalRef;
  confirmationModalConfig = {
    backdrop: true,
    ignoreBackdropClick: true,
  };

  deleteZoneData: any;
  modalRef: BsModalRef;
  addDfee = true;
  DeliveryBtn = true;
  VehicalBtn;
  countries: any = [];
  country_id: any;
  cities: any = [];
  city_id: any;
  deliveries_type_list: any = [];
  delivery_type: any;
  vehicle: any = '';
  vehicles: any = [];
  vehicle_id: any;
  is_business: boolean = false;
  is_use_distance_calculation: boolean = false;
  zones: any = [];
  to_zone: any;
  from_zone: any;
  zone_price: any;
  zone_to_zone_price: any = [];
  admin_profit_mode_list: any = [];
  admin_profit_mode_on_delivery: any = this._helper.ADMIN_PROFIT_ON_DELIVERY_ID.PERCENTAGE;
  admin_profit_value_on_delivery: any;
  base_price_distance: any
  base_price: any;
  price_per_unit_distance: any;
  price_per_unit_time: any;
  waiting_time_start_after_minute: any;
  service_tax: any;
  DELIVERY_TYPE=DELIVERY_TYPE;
  DELIVERY_TYPE_CONSTANT=DELIVERY_TYPE_CONSTANT;
  CHARGES_TYPE = CHARGES_TYPE
  CHARGE_TYPE = CHARGE_TYPE
  min_fare: any;
  is_add_distance_price: boolean = false;
  is_add_ecommerce_price: boolean = false;
  is_distance_unit_mile: boolean = false;
  from: any;
  to: any;
  price: any;
  delivery_price_setting: any = [];
  is_distance_calculation_price_added: boolean = false;
  is_to_distance_gt_from_distance: boolean = false;
  service_list: any = [];
  is_edit: boolean = false;
  service_id: any;
  vehicle_list: any = [];
  delivery_fee_search: any;
  vehicle_search: any;
  selected_delivery_fees: any;
  is_invalid: boolean = false;
  _vehicleSubscription: Subscription
  all_vehicles: any = []
  services: any = []
  currency_sign: any = '$'
  is_edit_zone: boolean = false
  edit_zone_id: any = ''
  is_default: boolean = false;
  can_set_default_price: boolean = true
  taxes: Array<any> = []
  service_taxes: Array<any> = []
  tax_ids: Array<any> = []
  waiting_time_start_after: number = null;
  waiting_time_charge: number = null;
  cancellation_charge: number = 0;
  cancellation_charge_apply_from: number = 11;
  cancellation_charge_apply_till: number = 19;
  want_to_set_waiting_time: boolean = false;
  want_to_set_cancellation_charges: boolean = false;
  is_distance_alreay_exist: boolean = false;
  is_Invalid_distance_and_price : boolean = false

  ecommerce_delivery_price_setting: any = [];
  min_order_price: any;
  max_order_price: any;
  delivery_price: any;
  is_invalid_price: boolean = false;
  is_min_price_gt_from_max_price: boolean = false;
  is_price_alreay_exist: boolean = false;
  ecommerce_price_added: boolean = false;

  serviceForm: UntypedFormGroup;
  edit_service_charge_id: any;
  services_list: Array<any> = [];
  temp_services_list: Array<any> = [];
  is_surge_hours: boolean = false;
  is_surge_on_night: boolean = false;
  night_surge_hours: any[] = [{
    start_time: '',
    end_time: '',
    surge_multiplier: null
  }];
  fromState = [
    { "id": 11, "title": "button-title.accepted" },
    { "id": 13, "title": "button-title.coming" },
    { "id": 15, "title": "button-title.arrived" }
  ]

  toState = [
    { "id": 13, "title": "button-title.coming" },
    { "id": 15, "title": "button-title.arrived" },
    { "id": 19, "title": "button-title.started" }
  ]
  temptoStoreStatues = this.toState
  tempfromStoreStatues = this.fromState
  service_type_id: any = null;
  service_charge_id: any = null;

  is_round_trip:boolean = false;
  round_trip_charge:number = null;
  additional_stop_price:number = 0;
  is_waiting_time_every_stop:boolean = false;
  over_sized_item_charge:number = 0;
  is_cancellation_charge:boolean = false;
  cancellation_charge_type:number = this._helper.ADMIN_PROFIT_ON_DELIVERY_ID.PERCENTAGE;
  cancellation_charge_value:number=null;
  surge_hour: any[] = [];
  surge_hours: any[] = [
    {
      "is_surge_hours": false,
      "is_surge_on_night": true,
      "day": 0,
      "day_time": []
    },
    {
      "is_surge_hours": false,
      "is_surge_on_night": true,
      "day": 1,
      "day_time": []
    },
    {
      "is_surge_hours": false,
      "is_surge_on_night": true,
      "day": 2,
      "day_time": []
    },
    {
      "is_surge_hours": false,
      "is_surge_on_night": true,
      "day": 3,
      "day_time": []
    },
    {
      "is_surge_hours": false,
      "is_surge_on_night": true,
      "day": 4,
      "day_time": []
    },
    {
      "is_surge_hours": false,
      "is_surge_on_night": true,
      "day": 5,
      "day_time": []
    },
    {
      "is_surge_hours": false,
      "is_surge_on_night": true,
      "day": 6,
      "day_time": []
    }
  ]
  @ViewChild('deleteZonePriceTemplate', { static: true }) deleteZonePriceTemplate: TemplateRef<any>;
  @ViewChild('staticTabs', { static: false }) staticTabs?: TabsetComponent;
  @ViewChild('storeTimeSettingsRef', { static: true }) storeTimeSettingsRef: StoreTimeSettingsModalComponent;
  @ViewChild('add_delivery_price_form') form: NgForm;
  @ViewChild('addNewItemModalRef', { static: true }) addNewItemModalRef: AddNewItemModalComponent;
  item_sequence_number:any=1;
  tabType:number;
  addClicked:boolean=false;
  updateDfeesId:any;
  permissionDisabled:boolean=false;

  @ViewChild('addNewModalRef', { static: true }) addNewModalRef: AddNewVehicleComponent;

  airportPriceErrorShow:boolean = false;
  cityPriceErrorShow:boolean = false;
  richsurgePriceError: boolean = false;
  airport_data:any []= [];
  city_data:any []= [];
  city_price: any = [];
  airport_price: any = [];
  rich_area_surge :any = [];
  rich_area_surge_list: any = [];

  @ViewChildren('richinputElements') richinputElements: QueryList<ElementRef>;
  @ViewChildren('cityinputElements') cityinputElements: QueryList<ElementRef>;
  @ViewChildren('airportinputElements') airportinputElements: QueryList<ElementRef>;

  constructor(public _helper: Helper,
    private _deliverFeesServcise: DeliveryFeesService,
    private _vehicelService: VehicleService,
    private config: NgSelectConfig,
    private _trans: TranslateService,
    private _commonService: CommonService ,
    private _notifierService: NotifiyService,
    private modalService: BsModalService, 
    private cdr:ChangeDetectorRef,
  ) {
    this.config.notFoundText = this._trans.instant('label-title.no-data-found');
  }

  ngOnInit(): void {
    this._vehicleSubscription = this._vehicelService._vehicleObservable.subscribe(() => {
      this.get_vehicle_list()
    })
    this.deliveries_type_list = this._helper.DELIVERY_TYPE    
    this._deliverFeesServcise 
      .get_server_delivery_types()
      .then(async (res) => {
        this.deliveries_type_list =  this._helper.DELIVERY_TYPE.filter((delivery)=> res?.delivery_types.includes(delivery.value))
      });
    
    this.admin_profit_mode_list = this._helper.ADMIN_PROFIT_ON_DELIVERYS
    this.calSurgeTime(this.surge_hours);
    this.get_country_list()
    this.get_service_list()
    this.get_vehicle_list()
    this._initForm()
    if(!this._helper.has_permission(this._helper.PERMISSION.ADD)){
      setTimeout(() => {
        this.permissionDisabled=true;
        let elements : any = document.getElementsByTagName('form')[0]?.elements;
        for(const element of elements){
          if(element.tagName != 'BUTTON'){
            element.setAttribute('disabled' , 'true')
          }
        }
      }, 300);
    }
    if(this._helper?.countryData){
      setTimeout(() => {
          this.country_id = this._helper.countryData._id;
          this.country_selected(true);
          this.city_selected(true);
          this.delivery_type = this._helper.deliveryData.delivery_type
          this.get_delivery_service_list();
          this.city_id = this._helper.selected_city._id ;
          this.service_type_id = this._helper.deliveryData._id;
      }, 1800);
    }
  }
  selectTab(tabId: number) {
    this.addClicked =false;
    this.tabType =tabId;
    if (this.staticTabs?.tabs[tabId]) {
      this.staticTabs.tabs[tabId].active = true;
    }
  }
  showStoreTimeSettingModal(delivery_type): void {
    this.storeTimeSettingsRef.show(delivery_type);
  }

  get_service_list(){
    this._deliverFeesServcise.get_service_list().then(res_data =>{
      this.service_list = res_data.service || []
      this.service_list.forEach((service) => {
        let deliveryTypeObj = DELIVERY_TYPE.find(type => type.value === service.delivery_type);
        if (deliveryTypeObj) {
          service.delivery_type_title = deliveryTypeObj.title;
        }
        if (service.delivery_type === DELIVERY_TYPE_CONSTANT.STORE || service.delivery_type === DELIVERY_TYPE_CONSTANT.TAXI || service.delivery_type === DELIVERY_TYPE_CONSTANT.COURIER || service.delivery_type === DELIVERY_TYPE_CONSTANT.ECOMMERCE) {
          if (service.vehicle_detail) {
            service.service_vehicle_name = service.vehicle_detail.vehicle_name;
          } else {
            service.service_vehicle_name = 'All';
          }
        }
        if (service.delivery_type === DELIVERY_TYPE_CONSTANT.SERVICE || service.delivery_type === DELIVERY_TYPE_CONSTANT.APPOINMENT) {
          if (service.delivery_details) {
            service.service_vehicle_name = service.delivery_details.delivery_name[0];
          } else {
            service.service_vehicle_name = 'All';
          }
        }
      })
    })
    if(this.updateDfeesId){
      this.edit_service(this.updateDfeesId);
    }
  }

  _initForm() {
    this.serviceForm = new UntypedFormGroup({
      admin_profit_type: new UntypedFormControl(1, Validators.required),
      admin_profit: new UntypedFormControl(null, Validators.required),
      cancellation_charge_type: new UntypedFormControl(1, Validators.required),
      cancellation_charge: new UntypedFormControl(0, Validators.required),
      cancellation_charge_apply_from: new UntypedFormControl(11, Validators.required),
      cancellation_charge_apply_till: new UntypedFormControl(19, Validators.required)
    })
  }

  get_country_list() {
    this._deliverFeesServcise.get_server_country_list().then(res_data => {
      this.countries = res_data['countries']
    })
  }

  get_vehicle_list(){
    this._vehicelService.get_vehicle_list().then(res_data => {
      this.vehicle_list = res_data.vehicles
    })
  }

  country_selected(is_change) {
    if(is_change){
      this.delivery_type = null
      this.city_id = null
      this.vehicle_id = null
    }
    this.get_currency_sign()
    this._deliverFeesServcise.get_city_lists(this.country_id).then(res_data => {
      this.cities = res_data.cities
      this.cdr.detectChanges();
    })
  }

  pad2(number) {
    return (number < 10 ? '0' : '') + number
  }

  get_currency_sign(){
    this.tax_ids = []
    let index = this.countries.findIndex(x => x._id === this.country_id)
    
    if(index !== -1){
      this.currency_sign = this.countries[index].currency_sign
      this.taxes = this.countries[index].taxes_details
      this.service_taxes.forEach(tax => {
        this.tax_ids.push(tax._id)
      })
    }
  }

  city_selected(is_change) { 
    if(is_change){
      this.delivery_type = null
      this.vehicle_id = null
    }
    this.vehicles = []
    this._deliverFeesServcise.get_vehicle_list(this.city_id, null).then(res_data => {
      this.services = res_data.services
      this.all_vehicles = res_data.vehicles
      if(this.vehicle_id){
        this.delivery_type_selected(false);
      }
      if(this.delivery_type==DELIVERY_TYPE_CONSTANT.SERVICE || this.delivery_type == DELIVERY_TYPE_CONSTANT.APPOINMENT){
        this.get_delivery_service_list()
      }
    })
    this.deliveries_type_list = JSON.parse(JSON.stringify(this.deliveries_type_list));
    this.zone_details()
  }

  delivery_type_selected(is_change){
    this.service_type_id = null;
    this.addClicked=false;
    if(is_change){
      this.vehicle_id = null
      this.vehicles = []
      this.all_vehicles.forEach(vehicle => {
        let index = this.services.findIndex(x => x.vehicle_id === vehicle._id && x.delivery_type === this.delivery_type)
        if(index === -1){
          this.vehicles.push(vehicle)
        }
      });
    }
    else{
      this.vehicles = JSON.parse(JSON.stringify(this.all_vehicles));
    }
    this.get_delivery_service_list()
  }

  get_delivery_service_list(){
    this.services_list = []
    let json = {
      delivery_type: this.delivery_type
    }
    this.temp_services_list = []
    this._deliverFeesServcise.get_delivery_service_list(json).then(async res => {
      this.services_list = res.deliveries;
      if(this.selected_delivery_fees){
        this.temp_services_list= this.services_list;
      }else{
         this.services_list.forEach(service => {
            const index = this.service_list.findIndex(x => x.service_type_id === service._id && x.delivery_type === this.delivery_type && x.city_id === this.city_id)
            if(index === -1){
              this.temp_services_list.push(service)
            }
          })
          setTimeout(async () => {
            const cityIndex = await this.cities.findIndex(city => city._id.toString() == this.city_id.toString());
            const deliveriesInCity = cityIndex !== -1 ? this.cities[cityIndex].deliveries_in_city : [];
            let filteredDeliveries = this.temp_services_list.filter(delivery => deliveriesInCity.includes(delivery._id));
            this.temp_services_list = filteredDeliveries
            this.cdr.detectChanges();
          }, 1200);
      }
    })
    }

  zone_details() {
    this._deliverFeesServcise.get_zone_details(this.city_id).then(res_data => {
      this.zones = res_data.city_zone
    })
  }

  add_zone() {
    if((this.from_zone == null) || (this.to_zone == null) || (!this.zone_price && this.zone_price != 0)){
      this.addClicked = true;
      return;
    }
    if(this.from_zone == this.to_zone){
      this._notifierService.showNotification('error', this._helper._trans.instant('validation-title.same-zone-not-allowed'));
      return
    }
    let json = {
      city_id: this.city_id,
      delivery_type: this.delivery_type,
      from_zone_id: this.from_zone,
      to_zone_id: this.to_zone,
      price: this.zone_price,
      vehicle_id: this.vehicle_id
    }

    this._deliverFeesServcise.check_zone_price_exists(json).then(res => {
      if (res.success === true) {
        this._deliverFeesServcise.add_zone_price(json).then(res_data => {
          if (res_data.success) {
            let from_zone_index = this.zones.findIndex((x) => x._id === json.from_zone_id)
            let to_zone_index = this.zones.findIndex((x) => x._id === json.to_zone_id)

            let zone_json = res_data.zone_value
            zone_json.is_checked = false
            zone_json.from_zone_detail = {
              title: this.zones[from_zone_index].title
            }

            zone_json.to_zone_detail = {
              title: this.zones[to_zone_index].title
            }
            this.zone_to_zone_price.push(zone_json)
            this.addClicked = false;
            this.from_zone = null;
            this.to_zone = null;
            this.zone_price = null;
          }
        })
      }

    })
  }

  add_distance_price() {
    this.is_add_distance_price = !this.is_add_distance_price
    this.from = 0;
    this.to = 0;
    this.price = 0;
    this.is_to_distance_gt_from_distance =false;
    this.is_distance_alreay_exist =false;
  }

  add_ecommerce_price() {
    this.is_add_ecommerce_price = !this.is_add_ecommerce_price
    this.min_order_price = 0;
    this.max_order_price = 0;
    this.delivery_price = 0;
    this.is_min_price_gt_from_max_price =false;
    this.is_price_alreay_exist =false;
    this.is_invalid_price = false;
  }

  onDeleteDistaceCalculatio(index){
    this.delivery_price_setting.splice(index, 1)
  }

   onDeleteEcommercePriceCalculatio(index){
    this.ecommerce_delivery_price_setting.splice(index, 1)
  }
  submit_distance_price() {
    if(this.to > 0){
      this.is_Invalid_distance_and_price = false
      if(this.from < this.to){
        this.is_to_distance_gt_from_distance = false
      for(const element of this.delivery_price_setting){
        if((this.from >= element.from &&
           this.from <= element.to) ||
           (this.to > element.from &&
            this.to <= element.to)
             ){
              this.is_distance_alreay_exist = true
              break;
            }
           else {
            if((this.from < element.from) && (this.to > element.from)){
              this.is_distance_alreay_exist = true
              break;
            }
            else{
              this.is_distance_alreay_exist = false
              continue;
            }
           }
      }
    }
    else{
      this.is_to_distance_gt_from_distance = true
    }
  }
  else{
    this.is_Invalid_distance_and_price = true
  }
    if(!this.is_to_distance_gt_from_distance && !this.is_distance_alreay_exist && !this.is_Invalid_distance_and_price){

        this.is_to_distance_gt_from_distance = false
        this.is_distance_calculation_price_added = true
        this.is_add_distance_price = false
        this.delivery_price_setting.push({
          from: this.from,
          to: this.to,
          price: this.price
        })
        this.from = 0;
        this.to = 0;
        this.price = 0;
      }
  }

  submit_ecommerce_price() {
    this.is_min_price_gt_from_max_price = false
    this.is_price_alreay_exist = false
    this.is_invalid_price = false
    if(this.max_order_price > 0){
      this.is_invalid_price = false
      if(this.min_order_price < this.max_order_price){
        this.is_min_price_gt_from_max_price = false
      for(const element of this.ecommerce_delivery_price_setting){
        if((this.min_order_price >= element.min_order_price &&
           this.min_order_price <= element.max_order_price) ||
           (this.max_order_price > element.min_order_price &&
            this.max_order_price <= element.max_order_price)
             ){
              this.is_price_alreay_exist = true
              break;
            }
           else {
            if((this.from < element.from) && (this.to > element.from)){
              this.is_price_alreay_exist = true
              break;
            }
            else{
              this.is_price_alreay_exist = false
              continue;
            }
           }
      }
    }
    else{
      this.is_min_price_gt_from_max_price = true
    }
  }
  else{
    this.is_invalid_price = true
  }
    if(!this.is_min_price_gt_from_max_price && !this.is_price_alreay_exist && !this.is_invalid_price){

        this.is_min_price_gt_from_max_price = false
        this.ecommerce_price_added = true
        this.is_add_ecommerce_price = false
        this.ecommerce_delivery_price_setting.push({
          min_order_price: this.min_order_price,
          max_order_price: this.max_order_price,
          delivery_price: this.delivery_price
        })
        this.min_order_price = 0;
        this.max_order_price = 0;
        this.delivery_price = 0;
      }
  }

  businessToggel(event) {
    this.is_business = event.target.checked
    if(this.is_business === false && this.is_default){
      this.is_default = false;
    }
  }

  distanceCalculationToggle(event) {
    this.is_use_distance_calculation = event.target.checked
  }
  waitingTimeToggle(event){
    this.want_to_set_waiting_time = event.target.checked
  }

  add_service_charges(){
    if(!this.is_edit){
      let json = {
        country_id: this.country_id,
        city_id: this.city_id,
        delivery_id: this.service_type_id,
        ...this.serviceForm.value,
        service_id:this.service_id,
        cancellation_charge:this.cancellation_charge_value,
        cancellation_charge_apply_from:this.cancellation_charge_apply_from,
        cancellation_charge_apply_till:this.cancellation_charge_apply_till,
        cancellation_charge_type:this.cancellation_charge_type
      }
      this._commonService.addServiceCharges(json).then(result => {
        if(result.success){
          this.serviceForm.reset()
        }
      })
    } else {
      let json = {
        _id: this.service_charge_id,
        country_id: this.country_id,
        delivery_id: this.service_type_id,
        city_id: this.city_id,
        ...this.serviceForm.value,
        service_id:this.service_id,
        cancellation_charge:this.cancellation_charge_value,
        cancellation_charge_apply_from:this.cancellation_charge_apply_from,
        cancellation_charge_apply_till:this.cancellation_charge_apply_till,
        cancellation_charge_type:this.cancellation_charge_type
      }
      this._commonService.updateServiceCharges(json).then(result => {
        if(result.success){
          this.serviceForm.reset();
        }
      })
    }
  }

  async set_service_charges(value) {
    if (this.delivery_type == DELIVERY_TYPE_CONSTANT.SERVICE || this.delivery_type == DELIVERY_TYPE_CONSTANT.APPOINMENT || this.delivery_type == DELIVERY_TYPE_CONSTANT.TAXI || this.delivery_type == DELIVERY_TYPE_CONSTANT.COURIER) {
      this.add_service_charges()
      value.service_type_id = this.service_type_id;
    } else {
      delete value.service_type_id;
    }
  }


  add_service(value) {
    this.distance_price_parameters_init();
    this.ecom_price_parameters_init();
   
    this.service_tax = 0
    this.service_taxes = []

    if (this.delivery_type === this._helper.DELIVERY_TYPE_CONSTANT.ECOMMERCE && this.ecommerce_delivery_price_setting.length === 0) {
      this.is_invalid = true;
      this._notifierService.showNotification('error', this._helper._trans.instant('validation-title.please-add-delivery-fees'));
      return 
    }else{
      this.is_invalid = false;
    }
    
    if (this.delivery_type === this._helper.DELIVERY_TYPE_CONSTANT.SERVICE || this.delivery_type === this._helper.DELIVERY_TYPE_CONSTANT.APPOINMENT) {
      if(!this.serviceForm.value.admin_profit_type || !this.serviceForm.value.admin_profit){
        this.serviceForm.get('admin_profit_type').markAsTouched();
        this.serviceForm.get('admin_profit').markAsTouched();
        if(this.is_edit){
          this.adminProfitInput.nativeElement.focus();
        }
        return
      }
    }

    if(value.is_cancellation_charge && !value.cancellation_charge_value){
      this.is_invalid = true;
      return
    }else{
      this.is_invalid = false;
    }

    if (value.is_business === null) {
      value.is_business = false
    }
    if (value.is_surge_hours === null) {
      value.is_surge_hours = false
    }
    if (value.is_default === null) {
      value.is_default = false
    }
    if (value.is_use_distance_calculation === null) {
      value.is_use_distance_calculation = false
    }
    if (value.want_to_set_waiting_time === null){
      value.want_to_set_waiting_time = false
    }
    if(this.is_waiting_time_every_stop === null){
      value.is_waiting_time_every_stop = false
    }
    if (value.want_to_set_cancellation_charges === null){
      value.want_to_set_cancellation_charges = false
    }
    if(value.is_round_trip === null){
      value.is_round_trip = false
    }
    if(value.is_cancellation_charge === null){
      value.is_cancellation_charge = false
    }
    this.tax_ids.forEach(tax => {
      let index = this.taxes.findIndex(x => x._id === tax)
      if (index !== -1) {
        this.service_taxes.push(this.taxes[index])
      }
    })
    value.service_taxes = this.service_taxes
    this.service_taxes.forEach(tax => {
      this.service_tax = this.service_tax + tax.tax
    })
    value.service_tax = this.service_tax
    value.delivery_price_setting = this.delivery_price_setting;
    value.ecommerce_delivery_price_setting = this.ecommerce_delivery_price_setting

    this.is_invalid = false
    if (value.is_use_distance_calculation) {
      if (this.delivery_price_setting.length === 0 || this.admin_profit_mode_on_delivery === null) {
        this.is_invalid = true
      }
    } else {
      let count = 0
      Object.keys(value).forEach(function (k) {
        if (value[k] === "" || value[k] === undefined || value[k] === null) {
          count++
        }
      });
      if (count > 0) {
        this.is_invalid = true
      }
    }
    if (this.delivery_type == this._helper.DELIVERY_TYPE_CONSTANT.COURIER || this.delivery_type == this._helper.DELIVERY_TYPE_CONSTANT.TAXI) {
      let night_surge_hours = [];
    
      if (this.is_cancellation_charge === true) {
        if (!this.cancellation_charge_type || !this.cancellation_charge_value) {
          return this.form.submitted
        }
      }
      if (this.delivery_type == this._helper.DELIVERY_TYPE_CONSTANT.COURIER && this.is_round_trip === true) {
        if (!this.round_trip_charge) {
          return this.form.submitted
        }
      }
      if (this.want_to_set_waiting_time === true) {
        if (!this.waiting_time_start_after || !this.waiting_time_charge) {
          return this.form.submitted
        }
      }
      if (value.is_surge_on_night && this.delivery_type == this._helper.DELIVERY_TYPE_CONSTANT.COURIER) {
        if (this.night_surge_hours.length == 0 || !this.night_surge_hours[0].surge_multiplier || !this.night_surge_hours[0].start_time || !this.night_surge_hours[0].end_time) {
          return this.form.submitted;
        }
        if (new Date(this.night_surge_hours[0].start_time.getTime()) >= new Date(this.night_surge_hours[0].end_time.getTime())) {
          return this.form.submitted;
        }
        this.night_surge_hours.forEach((time) => {
          let start_time = this.pad2((new Date(time.start_time.getTime())).getHours()) + ':' + this.pad2((new Date(time.start_time.getTime())).getMinutes());
          let end_time = this.pad2((new Date(time.end_time.getTime())).getHours()) + ':' + this.pad2((new Date(time.end_time.getTime())).getMinutes());
          night_surge_hours.push({
            start_time: start_time,
            end_time: end_time,
            surge_multiplier: time.surge_multiplier
          })
        })
        value.night_surge_hours = night_surge_hours;
      }

    value.surge_hours = this.surge_hours;


    this.service_tax = 0
    this.service_taxes = []

    if (value.is_surge_hours === null) {
      value.is_surge_hours = false
    }
    if (value.is_surge_on_night == null) {
      value.is_surge_on_night = false;
    }

    if(value.is_round_trip == null){
      value.is_round_trip = false
    }
    }
    if (this.delivery_type === DELIVERY_TYPE_CONSTANT.TAXI || this.delivery_type === DELIVERY_TYPE_CONSTANT.COURIER) {
      value.want_to_set_waiting_time = this.want_to_set_waiting_time
      value.want_to_set_cancellation_charges = this.want_to_set_cancellation_charges
      value.waiting_time_start_after = this.waiting_time_start_after
      value.waiting_time_charge = this.waiting_time_charge
      value.cancellation_charge_type = this.cancellation_charge_type
      value.cancellation_charge = this.cancellation_charge
    }
    value.is_default = this.is_default
    value.delivery_price_setting = this.delivery_price_setting
    value.ecommerce_delivery_price_setting = this.ecommerce_delivery_price_setting
    if(this.form.controls.admin_profit_value_on_delivery?.errors?.max || this.form.controls.round_trip_charge?.errors?.max
      || this.form.controls.cancellation_charge_value?.errors?.max || this.form.controls.admin_profit_value_on_delivery?.errors?.min || this.form.controls.round_trip_charge?.errors?.min
      || this.form.controls.cancellation_charge_value?.errors?.min){
      return
    }

    if (!this.is_invalid && !this.is_edit) {
      this._deliverFeesServcise.add_service_price(value).then(async(res_data) => {
        if (res_data.success) {
          this.service_id = res_data.service_id;
          await this.set_service_charges(value)
          this.get_service_list()
          this.is_invalid = false
          this.form.resetForm()
          this.taxes = []
          this.service_taxes = []
          this.tax_ids = []
        }
      })
    } else if (!this.is_invalid && this.is_edit) {      
      value.service_id = this.service_id
      value.is_default = this.is_default
      this._deliverFeesServcise.update_service(value).then(async(res_data) => {
        if (res_data.success) {
          await this.set_service_charges(value)
          this.is_edit = false
          this.is_invalid = false
          this.get_service_list()
          this.form.resetForm()
          this.taxes = []
          this.service_taxes = []
          this.tax_ids = []
        }
      })
    }
    this.selected_delivery_fees =null;
    this.addDfee = true;
  }
  
  edit_service(id){    
    this.rich_area_surge_list = [];
    this.selectTab(0);
    this.distance_price_parameters_init()
    this.ecom_price_parameters_init();
    if(!this._helper.has_permission(this._helper.PERMISSION.EDIT)){
      setTimeout(() => {
        this.permissionDisabled=true;
        let elements : any = document.getElementsByTagName('form')[0]?.elements;
        for(const element of elements){
          element.setAttribute('disabled' , 'false')
        }
      }, 300);
    }else{
      setTimeout(() => {
        let elements :any = document.getElementsByTagName('form')[0]?.elements;
        for(const element of elements){
          element.removeAttribute('disabled')
        }
      }, 300);
    }
    this.updateDfeesId=id;
    this.service_taxes = []
    this.service_tax = 0
    this.from_zone = null
    this.to_zone = null
    this.zone_price = null
    this.addDfee = false
    this.selected_delivery_fees = id
    this.is_edit = true
    this._deliverFeesServcise.get_service_details(id).then(res_data => {
      if(res_data.success){
        let service = res_data.service
        this.service_id = service._id
        this.is_business = service.is_business
        this.is_surge_hours = service.is_surge_hours
        this.is_surge_on_night = service.is_surge_on_night;
        if(res_data.service?.rich_area_surge){
          this.rich_area_surge = res_data.service?.rich_area_surge;
        }
        if (service.night_surge_hours && service.night_surge_hours.length > 0) {
          service.night_surge_hours.forEach(element => {
            let start_date = new Date();
            let start_time = element.start_time.split(':');
            element.start_time = new Date(start_date.setHours(start_time[0], start_time[1], 0, 0));
            let end_date = new Date();
            let end_time = element.end_time.split(':');
            element.end_time = new Date(end_date.setHours(end_time[0], end_time[1], 0, 0));
          });
          this.night_surge_hours = service.night_surge_hours;
        } else {
          this.night_surge_hours = [{
            start_time: '',
            end_time: '',
            surge_multiplier: null
          }];
        }
        if (service.surge_hours && service.surge_hours.length > 0) {
          this.surge_hours = service.surge_hours;
        } else {
          this.surge_hours = [
            {
              "is_surge_hours": false,
              "is_surge_on_night": false,
              "day": 0,
              "day_time": []
            },
            {
              "is_surge_hours": false,
              "is_surge_on_night": false,
              "day": 1,
              "day_time": []
            },
            {
              "is_surge_hours": false,
              "is_surge_on_night": false,
              "day": 2,
              "day_time": []
            },
            {
              "is_surge_hours": false,
              "is_surge_on_night": false,
              "day": 3,
              "day_time": []
            },
            {
              "is_surge_hours": false,
              "is_surge_on_night": false,
              "day": 4,
              "day_time": []
            },
            {
              "is_surge_hours": false,
              "is_surge_on_night": false,
              "day": 5,
              "day_time": []
            },
            {
              "is_surge_hours": false,
              "is_surge_on_night": false,
              "day": 6,
              "day_time": []
            }
          ]
        }
        this.calSurgeTime(this.surge_hours);
        this.is_default = service.is_default
        setTimeout(() => {  // is_default and is_business on then both toggele disabled.
          if (this.delivery_type !== this._helper.DELIVERY_TYPE_CONSTANT.SERVICE && this.delivery_type !== this._helper.DELIVERY_TYPE_CONSTANT.APPOINMENT) {
            const element_is_default: HTMLInputElement | null = document.getElementById('is_default') as HTMLInputElement;
            const element_is_business: HTMLInputElement | null = document.getElementById('is_business') as HTMLInputElement;
            if (this.is_business === false && element_is_default && !this.is_default) {
              element_is_default.disabled = true;
            }
            if (element_is_default && this.is_default) {
              element_is_default.disabled = true;
              element_is_business.disabled = true;
            }
          }
        }, 1000);
        this.is_use_distance_calculation = service.is_use_distance_calculation
        this.country_id = service.country_id
        this.city_id = service.city_id
        this.vehicle_id = service.vehicle_id
        this.delivery_type = service.delivery_type
        this.delivery_price_setting = service.delivery_price_setting
        this.ecommerce_delivery_price_setting = service.ecommerce_delivery_price_setting || []
        this.is_distance_unit_mile = service.country_details.is_distance_unit_mile;
        this.admin_profit_mode_on_delivery = service.admin_profit_mode_on_delivery
        this.admin_profit_value_on_delivery = service.admin_profit_value_on_delivery
        this.base_price_distance = service.base_price_distance
        this.base_price = service.base_price
        this.service_tax = service.service_tax
        this.min_fare = service.min_fare
        this.price_per_unit_distance = service.price_per_unit_distance
        this.price_per_unit_time = service.price_per_unit_time
        this.waiting_time_start_after_minute = service.waiting_time_start_after_minute
        this.zone_to_zone_price = res_data.zone_price
        this.service_taxes = service.service_taxes
        this.taxes = service.service_taxes
        this.cancellation_charge = service.cancellation_charge
        this.cancellation_charge_type = service.cancellation_charge_type == 0 ? 1 : service.cancellation_charge_type
        this.waiting_time_charge = service.waiting_time_charge
        this.waiting_time_start_after = service.waiting_time_start_after
        this.want_to_set_cancellation_charges = service.want_to_set_cancellation_charges
        this.want_to_set_waiting_time = service.want_to_set_waiting_time
        this.service_type_id = service.service_type_id

        this.is_round_trip = service.is_round_trip
        this.round_trip_charge = service.round_trip_charge
        this.additional_stop_price = service.additional_stop_price
        this.want_to_set_waiting_time = service.want_to_set_waiting_time
        this.is_waiting_time_every_stop = service.is_waiting_time_every_stop
        this.waiting_time_start_after = service.waiting_time_start_after
        this.waiting_time_charge = service.waiting_time_charge
        this.over_sized_item_charge = service.over_sized_item_charge
        this.is_cancellation_charge=service.is_cancellation_charge
        this.cancellation_charge_value = service.cancellation_charge_value


        if(service.delivery_type === DELIVERY_TYPE_CONSTANT.SERVICE || service.delivery_type === DELIVERY_TYPE_CONSTANT.APPOINMENT || service.delivery_type === DELIVERY_TYPE_CONSTANT.TAXI || service.delivery_type === DELIVERY_TYPE_CONSTANT.COURIER){
          if(res_data?.service_charges){
            this.service_charge_id = res_data?.service_charges?._id;
            this.service_type_id = res_data?.service_charges?.delivery_id;
            setTimeout(() => { // settime added value some time not patch proper
              this.serviceForm.patchValue({
                ...res_data.service_charges
              })
            }, 200);
            this.cancellation_charge_apply_from = res_data.service_charges.cancellation_charge_apply_from;
            this.cancellation_charge_apply_till = res_data.service_charges.cancellation_charge_apply_till;
            this.onFromStateSelect(res_data.service_charges.cancellation_charge_apply_from)
            this.onToStateSelect(res_data.service_charges.cancellation_charge_apply_till)
          }
        }
        if(this.delivery_type == DELIVERY_TYPE_CONSTANT.SERVICE || this.delivery_type == DELIVERY_TYPE_CONSTANT.APPOINMENT){
          this.selectTab(0);
        }
        if(this.delivery_type == DELIVERY_TYPE_CONSTANT.TAXI && this.tabType == 1){
          this.checkPricesList();
        }
        this.get_currency_sign()
        this.zone_details()
        this.check_default_price_available()
        this.country_selected(false);
        this.city_selected(false)
      }
    })
    this.is_edit_zone=false;
  }

  updateStoreTime(event) {
    this.surge_hours = event;
    this.calSurgeTime(event);
  }

  calSurgeTime(data) {
    this.surge_hour = []
    data.forEach((store_time) => {
      this.surge_hour.push({
        "is_surge_hours": store_time.is_surge_hours,
        "is_surge_on_night": store_time.is_surge_on_night,
        "day": store_time.day,
        "day_time": [],
      });
      store_time.day_time.forEach((day_time) => {

        let store_open_string = this.pad2(Math.floor(day_time.start_time / 60)) + ":" + this.pad2(Math.floor(day_time.start_time % 60));
        let store_close_string = this.pad2(Math.floor(day_time.end_time / 60)) + ":" + this.pad2(Math.floor(day_time.end_time % 60));

        if(Math.floor(day_time.start_time/60) > 12){
          store_open_string = this.pad2(Number(store_open_string.split(":")[0]) -12)+':'+store_open_string.split(":")[1]+' PM';
        }else if(Math.floor(day_time.start_time/60) == 12){
          store_open_string = this.pad2(Number(store_open_string.split(":")[0]))+':'+store_open_string.split(":")[1]+' PM';
        }else if(Math.floor(day_time.start_time/60) == 0){
          store_open_string = this.pad2(Number(store_open_string.split(":")[0]) +12)+':'+store_open_string.split(":")[1]+' AM';
        }else{
          store_open_string = store_open_string+' AM';
        }
        if(Math.floor(day_time.end_time/60) > 12){
          store_close_string = this.pad2(Number(store_close_string.split(":")[0]) -12)+':'+store_close_string.split(":")[1]+' PM';
        }else if(Math.floor(day_time.end_time/60) == 12){
          store_close_string = this.pad2(Number(store_close_string.split(":")[0]))+':'+store_close_string.split(":")[1]+' PM';
        }else if(Math.floor(day_time.end_time/60) == 0){
          store_close_string = this.pad2(Number(store_close_string.split(":")[0]) +12)+':'+store_close_string.split(":")[1]+' AM';
        }else{
          store_close_string = store_close_string+' AM';
        }

        this.surge_hour[this.surge_hour.length - 1].day_time.push({
          "start_time": store_open_string,
          "end_time": store_close_string,
          "surge_multiplier": day_time.surge_multiplier
        });
      })
    })
  }

  check_default_price_available(){
    this.can_set_default_price = true
    let index = this.service_list.findIndex(x => x.city_id === this.city_id && x.delivery_type === this.delivery_type && x.is_default === true)
    if(index !== -1){
      this.can_set_default_price = false
    }

  }

  onTaxSelected(event){
    this.tax_ids = event
  }

  onDefault(event){
    if (event?.target) {
      let index = this.service_list.findIndex(x => x._id === this.service_id)
      this.service_list[index].is_default = event.target.checked;
      this.is_default = event.target.checked;
      this.check_default_price_available();
    }
  }

  select_vehicle(vehicle){
    if(this._helper.has_permission(this._helper.PERMISSION.EDIT)){
      this.addNewModalRef.show(vehicle)
    }
    this.vehicle = vehicle
  }

  edit_zone(index){
    this.is_edit_zone = true
    this.edit_zone_id = this.zone_to_zone_price[index]._id
    this.from_zone = this.zone_to_zone_price[index].from_zone_id
    this.to_zone = this.zone_to_zone_price[index].to_zone_id
    this.zone_price = this.zone_to_zone_price[index].price
  }

  update_zone(){
    if((this.from_zone == null) || (this.to_zone == null) || (!this.zone_price && this.zone_price != 0)){
      this.addClicked = true;
      return;
    }
    let json = {
      price: this.zone_price,
      _id: this.edit_zone_id
    }
    this._deliverFeesServcise.update_zone_price(json).then(res_data => {
      if(res_data.success){
        let index = this.zone_to_zone_price.findIndex(x => x._id === this.edit_zone_id)
        this.zone_to_zone_price[index].price = this.zone_price
        this.from_zone = null;
        this.to_zone = null;
        this.zone_price = null;
        this.is_edit_zone = false
        this.addClicked = false;
      }
    })
  }

  showAddNewModal(): void {
    this.addNewModalRef.show('');
    this.vehicle = ''
  }

  AddDFee(): void {
    this.rich_area_surge_list = [];
    this.selectTab(0)
    this._helper.setDeliveryPrice(null,null);
    this._helper.countryData = null;

    this.updateDfeesId=null;
    this.addClicked = false;
    this.is_edit = false
    this.addDfee = true;
    if (this.selected_delivery_fees != null) {
      this.form.resetForm();
      this.selected_delivery_fees = null;
    }
    this.country_id = null
    this.delivery_type = null
    this.city_id = null
    this.vehicle_id = null
    this.is_edit_zone = false
    this.zone_to_zone_price = []
    this.service_tax = null
    this.service_taxes = []
    this.taxes = []
    this.tax_ids = []
    this.zones = []
    this.zone_price = "";
    this.to_zone = "";
    this.from_zone = "";
    this.is_round_trip = false;
    this.round_trip_charge = null;
    this.additional_stop_price = null;
    this.want_to_set_waiting_time = false;
    this.is_waiting_time_every_stop = false;
    this.waiting_time_start_after = null;
    this.waiting_time_charge = null;
    this.over_sized_item_charge = null;
    this.is_cancellation_charge = false;
    this.cancellation_charge_type = this._helper.ADMIN_PROFIT_ON_DELIVERY_ID.PERCENTAGE;
    this.cancellation_charge_value = null;
    this.delivery_price_setting = [];
    this.ecommerce_delivery_price_setting = []
    this.admin_profit_mode_on_delivery = this._helper.ADMIN_PROFIT_ON_DELIVERY_ID.PERCENTAGE;

    this.ngOnInit()
    if(this._helper.has_permission(this._helper.PERMISSION.ADD)){
      setTimeout(() => {
        this.permissionDisabled=false;
        let elements :any = document.getElementsByTagName('form')[0]?.elements;
        for(const element of elements){
          element.removeAttribute('disabled')
        }
      }, 300);
    }
  }
  showDeliveryFees(): void {
    this.DeliveryBtn = true;
    this.VehicalBtn = false;
    this.addDfee = true;
    if(!this._helper.has_permission(this._helper.PERMISSION.ADD) && (!this._helper.has_permission(this._helper.PERMISSION.EDIT) || this._helper.has_permission(this._helper.PERMISSION.EDIT))){
      setTimeout(() => {
        this.permissionDisabled=true;
        let elements : any = document.getElementsByTagName('form')[0]?.elements;
        for(const element of elements){
          if(element.tagName != 'BUTTON'){
            element.setAttribute('disabled' , 'true')
          }
        }
      }, 300);
    }
  }
  showVehicle(): void {
    this.DeliveryBtn = false;
    this.VehicalBtn = true;
    this.addDfee = false;
  }

  onFromStateSelect(event){
    this.toState = this.temptoStoreStatues.filter(x => x.id > event)
  }

  onToStateSelect(event){
    // this.fromState = this.tempfromStoreStatues.filter(x => x.id < event)
  }

  ngOnDestroy(){
    if(this.addNewModalRef.modalRef){
      this.addNewModalRef.onClose();
    }
    this.updateDfeesId=null;
    this._helper.setDeliveryPrice(null,null);
    this._helper.countryData = null;
  }

  setup_service(){
    let city_index = this.cities.findIndex((x)=>x._id==this.city_id);
    let vehicle_index = this.vehicle_list.findIndex((x)=>x._id==this.vehicle_id);
    let json = {
      store_id: this.service_id,
      name: this.cities[city_index].city_name + ' - ' + this.vehicle_list[vehicle_index].vehicle_name
    }
    this._deliverFeesServcise.setup_courier_service(json).then(res_data => {
      if(res_data.success){
        this.addNewItemModalRef.show(true, res_data.item_id, null, null, true);
      }
    });
  }

  // this function helpes to initials to disatance price parameters
  distance_price_parameters_init(){
    this.is_to_distance_gt_from_distance = false;
    this.is_distance_alreay_exist = false;
    this.is_add_distance_price=false;
    this.from = 0;
    this.to = 0;
    this.price = 0;
  }

  // this function helpes to initials to Ecommerce price parameters
  ecom_price_parameters_init(){
    this.is_min_price_gt_from_max_price = false;
    this.is_price_alreay_exist = false;
    this.is_invalid_price=false;
    this.min_order_price = 0;
    this.max_order_price = 0;
    this.delivery_price = 0;
  } 

  checkPricesList(){
    if(this.delivery_type == 4 && this.city_id && this.vehicle_id){
      this.getAirportList(); 
      this.getCityList();
    }
  }

  // get airport price list
  getAirportList(){
    if (this.city_id && this.vehicle_id) {
      let josn: any = { cityid: this.city_id, vehicle_id: this.vehicle_id }
      this._deliverFeesServcise.get_airport_price_list(josn).then(res => {
        if (!res.success){
          this.airport_data = []
          return
        }
          if(res.airport_value.length>0){
            this.airport_data = res.airport_value;
            if(res.airport_value.length != res.airport_list.length) {
              for(let data of res.airport_list) {
                let obj = this.airport_data.find((obj) => obj.airport_id == data._id)               
                if(!obj){
                  this.airport_data.push(data)
                } 
              }
            }
          } else {
            this.airport_data = res.airport_list;
          }
      })
    }
  }

  // get city price list 
  getCityList(){
    if (this.city_id && this.vehicle_id) {
      let josn: any = { cityid: this.city_id, vehicle_id: this.vehicle_id }
      this._deliverFeesServcise.get_city_price_list(josn).then(res => {
        if (!res.success) {
          this.city_data = [];
          return
        }
        if (res.city_value.length > 0 && res.city_list.length > 0) {
          const filtered_city_list = res.city_list;
          const cityValueArrayMap = new Map();
          for (const element of res.city_value) {
            cityValueArrayMap.set(element.destination_name._id, element);
          }
          for (let i = 0; i < filtered_city_list.length; i++) {
            const destinationId = filtered_city_list[i].destination_name._id;
            if (cityValueArrayMap.has(destinationId)) {
              filtered_city_list[i] = cityValueArrayMap.get(destinationId);
            }
          }
          this.city_data = filtered_city_list;
        } else {
          this.city_data = res.city_list;
        }
      })
    }
  }
  
  // all airport price update
  airportPriceUpdate(data) {
    if (data) {
      let blankInput = data.find(ele => ele.price == null);
      if (blankInput) {
        let index = data.indexOf(blankInput);
        this.airportinputElements.toArray()[index].nativeElement.focus();
        this.airportPriceErrorShow = true;
        return;
      }else{
        this.airportPriceErrorShow = false
      }
      
      for (const datas of data) {
        if (datas.airport_id) {
          this.airport_price.push({ airport_id: datas.airport_id, price: datas.price })
        } else {
          this.airport_price.push({ airport_id: datas._id, price: datas.price })
        }
      }
      let json: any = { cityid: this.city_id, vehicle_id: this.vehicle_id, airport_price: this.airport_price }
      this._deliverFeesServcise.update_airport_city_price_list(json).then(res => { 
        this.airport_price = []
        if (res.success) {
          this.getAirportList();
        }
      })
    }
  }
 
  // all city price update
  cityPriceUpdate(data) {
    if (data) {
      let blankInput = data.find(ele => ele.price == null);
      if (blankInput) {
        let index = data.indexOf(blankInput);
        this.cityinputElements.toArray()[index].nativeElement.focus();
        this.cityPriceErrorShow = true;
        return;
      }else{
        this.cityPriceErrorShow = false
      }
      
      for (const datas of data) {
        this.city_price.push({ destination_city_id: datas.destination_name._id, price: datas.price })
      }
      let json: any = { cityid: this.city_id, vehicle_id: this.vehicle_id, city_price: this.city_price }
      this._deliverFeesServcise.update_city_to_city_price_list(json).then(res => {      
        this.city_price = []
        if (res.success) {
          this.getCityList();
        }
      })
    }
  }

  // rich surge area list
  richAreaList() {
    this.rich_area_surge_list = [];
    if (this.zones.length>0) {
      for (const iterator of this.zones) {
        if (this.rich_area_surge?.length > 0) {
          let surge_index = this.rich_area_surge.findIndex((x) => x.id == iterator._id)
          let zone_index = this.zones.findIndex((x) => x._id == iterator._id)
          let surge_multiplier = 1;
          if (surge_index != -1) {
            this.rich_area_surge_list.push({ surge_multiplier: this.rich_area_surge[surge_index].surge_multiplier, title: this.zones[zone_index].title, id: this.zones[zone_index]._id })
          } else {
            this.rich_area_surge_list.push({ surge_multiplier: surge_multiplier, title: iterator.title, id: iterator._id })
          }
        } else {
          this.rich_area_surge_list.push({ surge_multiplier: null, title: iterator.title, id: iterator._id })
        }
      }
    }
  }

  updateRichAreaSurge(data){
    if (data) {
      let blankInput = data.find(ele => ele.surge_multiplier == null);
      if (blankInput) {
        let index = data.indexOf(blankInput);
        this.richinputElements.toArray()[index].nativeElement.focus();
        this.richsurgePriceError = true;
        return;
      } else {
        this.richsurgePriceError = false
      }
      let josn: any = { rich_surge_price: data, cityid: this.city_id, service_id: this.service_id}
      this._deliverFeesServcise.update_service(josn).then(async (res_data) => {
        if(res_data.success){
          this.rich_area_surge_list = [];
          this._deliverFeesServcise.get_service_details(this.selected_delivery_fees).then(res_data => {
            if(res_data.success && res_data.service?.rich_area_surge){
              this.rich_area_surge = res_data.service?.rich_area_surge;
              this.richAreaList();
            }
          });
        }
      })
    }
  }

  // open model delete zone to zone price
  deleteSignalZoneModel(data) {
    this.addClicked = false;
    this.deleteZoneData = data;   
    this.deleteZonePriceModalRef = this.modalService.show(this.deleteZonePriceTemplate, this.confirmationModalConfig);
  }

  closeDeleteZoneModel() {
    this.deleteZonePriceModalRef.hide();
  }

  // delete SignalZone price 
  deleteSignalZone(data) {
    if (data._id) {
      this.btnDisable = true;
      let json: any = { _id: data._id }
      this._deliverFeesServcise.delete_zone_price(json).then(res => {
        if (res.success) {
          this.closeDeleteZoneModel();
          let newzonearray = this.zone_to_zone_price.filter((zone)=> zone._id != res?.zone_id)
          this.zone_to_zone_price = newzonearray;
          setTimeout(() => {
            this.btnDisable = false;
          }, 500);
        } else {
          this.btnDisable = false;
        }
      })
    }
  }

}
