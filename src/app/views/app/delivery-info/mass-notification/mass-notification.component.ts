import { Component, EventEmitter, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { AddNewMassNotificationModalComponent } from 'src/app/containers/pages/add-new-mass-notification-modal/add-new-mass-notification-modal.component';
import { MassNotificationService } from 'src/app/services/mass-notification.service';
import { Helper } from 'src/app/shared/helper';
import { USER_TYPE } from 'src/app/shared/constant';
import { Subscription } from 'rxjs';
import { LangService } from 'src/app/shared/lang.service';

@Component({
  selector: 'app-mass-notification',
  templateUrl: './mass-notification.component.html',
  styleUrls: ['./mass-notification.component.scss']
})
export class MassNotificationComponent implements OnInit,OnDestroy {
  itemOrder = { label: 'label-title.all', value: '' };
  itemOptionsOrders = [
    { label: 'label-title.all', value: '' },
    { label: 'label-title.user', value: USER_TYPE.USER },
    { label: 'label-title.store', value: USER_TYPE.STORE },
    { label: 'label-title.deliveryman', value: USER_TYPE.PROVIDER },
  ];
  changeOrderBy: EventEmitter<any> = new EventEmitter();

  notification_list = []
  USER_TYPE = USER_TYPE
  total_page = 0;
  current_page = 1;
  notification_subscriber: Subscription
  timezone_for_display_date:string = '';

  @ViewChild('openMassNotificationModal', { static: true }) openMassNotificationModal: AddNewMassNotificationModalComponent;
  constructor(private _massNotificationService: MassNotificationService, public _lang: LangService,public _helper:Helper) { }

  ngOnInit(): void {
    this._helper.display_date_timezone.subscribe(data => {
      this.timezone_for_display_date = data;
    })
    this.notification_subscriber = this._massNotificationService._notificationObservable.subscribe(()=>{
      this.getNotificationList()
    })
  }

  getNotificationList() {
    let json = {
      "page": this.current_page,
      "user_type": this.itemOrder.value
    }
    this._massNotificationService.fetch(json).then(res => {
      this.notification_list = []
      this.total_page = 0;
      if (res.success) {
        this.notification_list = res.mass_notification_list
        this.total_page = res.pages
      }
    })
  }
  onChangeOrderBy(item): void {
    this.itemOrder = item;
    this.getNotificationList()
  }
  showAddMassNotificationModal(): void {
    this.openMassNotificationModal.show();
  }

  pageChanged(event) {
    this.current_page = event.page
    this.getNotificationList()
  }

  ngOnDestroy(): void {
    this.notification_subscriber.unsubscribe()
    if(this.openMassNotificationModal.modalRef){
      this.openMassNotificationModal.onClose()
    }
  }
}
