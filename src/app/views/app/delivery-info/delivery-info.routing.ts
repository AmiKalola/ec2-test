import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CountryInfoComponent } from './country-info/country-info.component';
import { DFeesInfoComponent } from './d-fees-info/d-fees-info.component';
import { AdminServiceComponent } from './admin-service/admin-service.component';
import { EcommerceServiceComponent } from './ecommerce-service/ecommerce-service.component';
import { DInfoComponent } from './d-info/d-info.component';
import { DeliveryInfoComponent } from './delivery-info.component';
import { RefferalCodeComponent } from './refferal-code/refferal-code.component';
import { CourierServiceComponent } from '../menu/courier-service/courier-service.component';

const routes: Routes = [
    {
        path: '', component: DeliveryInfoComponent,
        children: [
            { path: '', pathMatch: 'full', redirectTo: 'business' },
            { path: 'business', component: DInfoComponent, data: {auth: '/admin/business'} },
            { path: 'country-city-info', component: CountryInfoComponent, data: {auth: '/admin/country-city-info'} },
            { path: 'd-fees-info', component: DFeesInfoComponent, data: {auth: '/admin/d-fees-info'} },
            { path: 'admin-service', component: AdminServiceComponent, data: {auth: '/admin/admin-service'} },
            { path: 'ecommerce-service', component: EcommerceServiceComponent, data: {auth: '/admin/ecommerce-service'} },
            { path: 'refferal-code', component: RefferalCodeComponent, data: {auth: '/admin/referral_detail'}},
            { path: 'courier-service', component: CourierServiceComponent, data: {auth: '/admin/courier-service'} },
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})

export class DeliveryInfoRoutingModule { }
