import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { CountryService } from 'src/app/services/country.service';
import { DeliveryFeesService } from 'src/app/services/delivery-fees.service';
import { AddNewAdvertiseModalComponent } from 'src/app/containers/pages/add-new-advertise-modal/add-new-advertise-modal.component';
import { AdvertiseService } from '../../../../services/advertise.service';
import {  Subscription } from 'rxjs';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { Helper } from 'src/app/shared/helper';
import { LangService } from 'src/app/shared/lang.service';
import { DELIVERY_TYPE_CONSTANT } from 'src/app/shared/constant';
import { DeliveryService } from 'src/app/services/delivery.service';

@Component({
  selector: 'app-advertise',
  templateUrl: './advertise.component.html',
  styleUrls: ['./advertise.component.scss']
})
export class AdvertiseComponent implements OnInit,OnDestroy {
  approved_advertise = []
  filter_approved_advertise = []
  unapproved_advertise = []
  filter_unapproved_advertise = []
  expired_advertise = []
  filter_expired_advertise = []
  advertise_subscriper:Subscription

  country_list = []
  city_list = [];
  delivery_type_list :any = [
    { value: 1, title: 'label-title.store' },
    { value: 5, title: 'label-title.service' },
    { value: 6, title: 'label-title.appoinment' }
  ]
  delivery_list :any = []
  temp_delivery_list = []
  
  country_name: any = '';
  city_name: any = '';
  delivery_type_name: any = 'label-title.all';
  delivery_name: any = 'label-title.all';
  
  selected_country:any = '000000000000000000000000';
  selected_city = '000000000000000000000000';
  selected_delivery_type = '000000000000000000000000';
  selected_delivery = '000000000000000000000000';
  DELIVERY_TYPE_CONSTANT = DELIVERY_TYPE_CONSTANT;
  ads_type : number = 0


  @ViewChild('addNewModalRef', { static: true }) addNewModalRef: AddNewAdvertiseModalComponent;
  constructor(private _advertiseService:AdvertiseService,
    private _countryService: CountryService,
    private _deliveryService: DeliveryService,
    private _deliveryFeesService: DeliveryFeesService,
    public _helper: Helper,
    public _lang: LangService) { }

  ngOnInit(): void {
    this.getCountryList()
    this.advertise_subscriper = this._advertiseService._addvertiseObservable.subscribe(()=>{
      setTimeout(() => {
        this.getApprovedAds()
      }, 2000)
    })

    this._deliveryFeesService.get_city_lists(this.selected_country).then(res => {
      this.city_list = []
      if (res && res.success) {
        this.city_list = res.cities
        if (this.city_list.length == 1) {
          this.selected_city = this.city_list[0]._id
          this.city_name = this.city_list[0].city_name;
        }
      }
    })

    this.getDeliveryList()
  }

  addAdvertiseModal(): void {
    this.addNewModalRef.show();
  }

  editAdvertiseModal(advertise_id) {
    this.addNewModalRef.show(advertise_id)
  }

  getApprovedAds(){
    this.ads_type = 0;
    this._advertiseService.list({
      is_approved: true,
      country_id: this.selected_country,
      city_id: this.selected_city,
      delivery_type: this.selected_delivery_type,
      delivery_type_id: this.selected_delivery
    }).then(res => {
      if(res.success){
        this.approved_advertise = res.advertise
        this.filter_approved_advertise = JSON.parse(JSON.stringify(this.approved_advertise))
        this.filter()
      } else {
        this.approved_advertise = []
        this.filter_approved_advertise = JSON.parse(JSON.stringify(this.approved_advertise))
        this.filter()
      }
    })
  }

  getDeliveryList(){
    this._deliveryService.get_delivery_list().then(res_data => {
      if(res_data.success){
        this.delivery_list = res_data.deliveries;        
        this.temp_delivery_list = res_data.deliveries;
      }
    })
  }

  getExpiredAds(){
    this.ads_type = 1;
    this._advertiseService.list({
      is_expired: true, country_id: this.selected_country,
      city_id: this.selected_city,
      delivery_type: this.selected_delivery_type,
      delivery_type_id: this.selected_delivery
    }).then(res => {
      if(res.success){
        this.expired_advertise = res.advertise
        this.filter_expired_advertise = JSON.parse(JSON.stringify(this.expired_advertise))
        this.filter()
      } else {
        this.expired_advertise = []
        this.filter_expired_advertise = JSON.parse(JSON.stringify(this.expired_advertise))
        this.filter()
      }
    })
  }



  ngOnDestroy(): void {
    this.advertise_subscriper.unsubscribe()
    if(this.addNewModalRef.modalRef)(
      this.addNewModalRef.close()
    )
  }

  onChangeCountry(country): void {
    if(this.selected_country != country){
      this.selected_country = country
      
      if(this.selected_country=='000000000000000000000000'){
        this.country_name = '';
        this.city_list = [];
        this.city_name = ''
        this.selected_city = '000000000000000000000000'
      } else {
        let index = this.country_list.findIndex(x => x._id === this.selected_country)
        this.country_name = this.country_list[index].country_name
        this.getCityList()
      }
      this.filter()
    }
  }

  onChangeCity(city): void {
    if(this.selected_city != city){
      this.selected_city = city
      let index = this.city_list.findIndex(x => x._id === this.selected_city)
      if(index != -1){
        this.city_name = this.city_list[index].city_name
      }
      this.filter()
    }
  }

  onChangeDeliveryList(delivery_type:any) : void{
    if(delivery_type == "000000000000000000000000"){
      this.delivery_type_name = 'label-title.all';
    }
    if(this.selected_delivery_type != delivery_type){
      this.selected_delivery_type = delivery_type
      let index = this.delivery_type_list.findIndex((x:any) => x.value == this.selected_delivery_type)
      if(index != -1){
        this.delivery_type_name = this.delivery_type_list[index].title
      }
      this.selected_delivery = "000000000000000000000000"
      this.delivery_name = 'label-title.all';      
    }
  }
  
  onChangeDelivery(delivery:any){
    if(delivery == "000000000000000000000000"){
      this.delivery_name = 'label-title.all';
    }   
    if(this.selected_delivery != delivery?._id){
      this.selected_delivery = delivery?._id;
      let index = this.delivery_list.findIndex((x:any) => x._id == this.selected_delivery)
      if(index != -1){
        this.delivery_name = this.delivery_list[index]?.delivery_name[this._lang.selectedlanguageIndex]
      }
    }    
  }

  apply(){

    if(this.ads_type == 0){
      this.getApprovedAds();
    }else if(this.ads_type == 1){
      this.getExpiredAds();
    }
  }

  clear_filter(){
    this.selected_country = '000000000000000000000000';
    this.selected_city = '000000000000000000000000';
    this.selected_delivery_type = '000000000000000000000000';
    this.selected_delivery = "000000000000000000000000";
    this.delivery_name = 'label-title.all';
    if(this.ads_type == 0){
      this.getApprovedAds();
    }else if(this.ads_type == 1){
      this.getExpiredAds();
    }
  }

  getCountryList() {
    this._countryService.fetch().then(res => {
      if (res.success) {
        this.country_list = res.countries
        this.selected_country = '000000000000000000000000'
        this.country_name = ''
        if(this.country_list.length==1){
          this.selected_country=this.country_list[0]._id
          this.country_name=this.country_list[0].country_name;
        }
        this.getCityList()
      }
    })
  }

  getCityList() {
    this.city_name = ''
    this.selected_city = '000000000000000000000000'
    if (this.selected_country != '000000000000000000000000') {
      this._deliveryFeesService.get_city_lists(this.selected_country).then(res => {
        this.city_list = []
        if (res && res.success) {
          this.city_list = res.cities
          if (this.city_list.length == 1) {
            this.selected_city = this.city_list[0]._id
            this.city_name = this.city_list[0].city_name;
          }
        }
      })
    }
  }
  

  filter() {
    this._helper.ngZone.run(() => {
      this.filter_approved_advertise = this.approved_advertise.filter((ads) => {
        if ((ads.country_id == this.selected_country || this.selected_country == '000000000000000000000000') && (ads.city_id == this.selected_city || this.selected_city == '000000000000000000000000')) {
          return true;
        }
      })

      this.filter_expired_advertise = this.expired_advertise.filter((ads) => {
        if ((ads.country_id == this.selected_country || this.selected_country == '000000000000000000000000') && (ads.city_id == this.selected_city || this.selected_city == '000000000000000000000000')) {
          return true;
        }
      })
    })
  }

  onDelete(advertise_id) {
    let title = this._helper._trans.instant('label-title.are-you-sure-want-to-remove')
    let text = this._helper._trans.instant('label-title.you-will-not-be-able-to-recover')
    let confirmButtonText = this._helper._trans.instant('label-title.yes-delete-it')
    let cancelButtonText = this._helper._trans.instant('label-title.no-keep-it')
    let advertiseDelete = this._helper._trans.instant('label-title.your-advertise-deleted')
    let deleted = this._helper._trans.instant('label-title.deleted')
    let advertiseSafe = this._helper._trans.instant('label-title.your-advertise-safe')
    let cancelled = this._helper._trans.instant('heading-title.cancelled')
    Swal.fire({
      title: title,
      text: text,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: confirmButtonText,
      cancelButtonText: cancelButtonText
    }).then((result) => {
      if (result.value) {
        Swal.fire(
          deleted,
          advertiseDelete,
          'success'
        )
        this._advertiseService.deleteAdvertise(advertise_id)
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        Swal.fire(
          cancelled,
          advertiseSafe,
          'error'
        )
      }
    })
  }
}
