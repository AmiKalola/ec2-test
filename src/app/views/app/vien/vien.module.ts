import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { StartComponent } from './start/start.component';
import { VienComponent } from './vien.component';
import { VienRoutingModule } from './vien.routing';
import { SharedModule } from 'src/app/shared/shared.module';
import { LayoutContainersModule } from 'src/app/containers/layout/layout.containers.module';
import { ComponentsCardsModule } from 'src/app/components/cards/components.cards.module';
import { DashboardsContainersModule } from 'src/app/containers/dashboards/dashboards.containers.module'
import { ComponentsChartModule } from 'src/app/components/charts/components.charts.module';
import { PipeModule } from 'src/app/pipes/pipe.module';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { NgSelectModule } from '@ng-select/ng-select';

@NgModule({
  declarations: [VienComponent, StartComponent],
  imports: [
    SharedModule,
    LayoutContainersModule,
    VienRoutingModule,
    ComponentsCardsModule,
    DashboardsContainersModule,
    ComponentsChartModule,
    PipeModule,
    BsDropdownModule.forRoot(),
    BsDatepickerModule.forRoot(),
    FormsModule,
    NgSelectModule
  ]
})
export class VienModule { }
