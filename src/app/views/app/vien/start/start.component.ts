import { Component, OnInit, ViewChild } from '@angular/core';
import { BarChartComponent } from 'src/app/components/charts/bar-chart/bar-chart.component';
import { ChartService } from 'src/app/components/charts/chart.service';
import { PaymentBarChartComponent } from 'src/app/components/charts/payment-bar-chart/payment-bar-chart.component';
import { PieChartComponent } from 'src/app/components/charts/pie-chart/pie-chart.component';
import { AdminSettingService } from 'src/app/services/admin-setting.service';
import { CommonService } from 'src/app/services/common.service';
import { Helper } from 'src/app/shared/helper';
import * as moment from 'moment';
import {
  barChartData,
  polarAreaChartData,
  pieChartData
} from '../../../../data/charts';
import { TranslateService } from '@ngx-translate/core';
import { CountryService } from 'src/app/services/country.service';

@Component({
  selector: 'app-start',
  templateUrl: './start.component.html'
})
export class StartComponent implements OnInit {
  barChartData = barChartData;
  polarAreaChartData = polarAreaChartData;
  pieChartData = pieChartData
  last_six_month_earning: any;
  country_list = [];
  country_name: any = '';
  currency_sign: any = '';
  selected_country = {
    country_id: '000000000000000000000000',
    currency_sign: ''
  };
  list = {
    total_cities: 0,
    total_countries: 0,
    total_providers: 0,
    total_store: 0,
    total_users: 0,
  };
  order_detail1 = {
    cancelled_order: 0,
    total_deliveries: 0,
    total_orders: 0
  }
  order_detail = {
    admin_earn_per: 0,
    admin_earning: 0,
    cash_payment: 0,
    completed_order: 0,
    delivery_payment: 0,
    order_payment: 0,
    other_payment: 0,
    promo_payment: 0,
    provider_earn_per: 0,
    provider_earning: 0,
    provider_payment_pre_earning: 0,
    store_earn_per: 0,
    store_earning: 0,
    store_payment_pre_earning: 0,
    total_item_sold: 0,
    total_payments: 0,
    wallet_payment: 0,
  }

  itemOptions = [
    { label: this.translate.instant('label-title.all'), value: '0' },
    { label: this.translate.instant('button-title.today'), value: '1' },
    { label: this.translate.instant('label-title.yesterday'), value: '2' },
    { label: this.translate.instant('label-title.this-week'), value: '3' },
    { label: this.translate.instant('label-title.this-month'), value: '4' },
    { label: this.translate.instant('label-title.this-year'), value: '5' },
    { label: this.translate.instant('label-title.custom'), value: '6' },
  ];

  itemSelected = { label: 'label-title.all', value: '0' };
  chartDataConfig: any;
  start_date: any = '';
  end_date: any = '';
  created_date: Date;
  setting_created_date: Date;
  todayDate: Date = new Date();
  direction = localStorage.getItem('direction');
  item_bsRangeValue: any;

  bar_chart_item_bsRangeValue = [];
  week_days = [];
  selected_end_month: any;
  selected_start_month: any;
  created_at: Date = null;
  @ViewChild('barChartComponent', { static: true }) barChartComponent: PaymentBarChartComponent;

  /* Pie chart letiables */
  pie_chart_item_bsRangeValue = [];
  pie_week_days = [];
  pie_selected_end_month: any;
  pie_selected_start_month: any;
  pie_created_at: Date = null;
  @ViewChild('pieChartComponent', { static: true }) pieChartComponent: PieChartComponent;

  /* Pie chart variables end */

  /* Order chart variables */
  order_chart_item_bsRangeValue = [];
  order_week_days = [];
  order_selected_end_month: any;
  order_selected_start_month: any;
  order_created_at: Date = null;
  @ViewChild('orderChartComponent', { static: true }) orderChartComponent: BarChartComponent;

  /* Order chart variables end */

  constructor(private _chartService: ChartService, private _commonService: CommonService,public _helper:Helper,
    public _adminSettingService: AdminSettingService, private translate: TranslateService,
    private _countryService: CountryService) {
    this.chartDataConfig = this._chartService;

   }

  ngOnInit(){
    this._helper.created_date.subscribe(data => {
      let date = new Date(data)
      this.created_date = date;
    })
    this._adminSettingService.getSettingDetail().then((response) => {
      if (response.success && response.setting) {
        this.setting_created_date = response.setting.created_at
        this.configMonthList();
        this.pieConfigMonthList();
        this.orderChartMonthList();
      }
    })
    this.getList()
    this.getCountryList()
    this._helper.country_id.next(this.selected_country);
  }
  configMonthList() {
    this.created_at = this.setting_created_date;
    if (this.created_at) {
      this.week_days = this._helper.getSixMonthDifference(this.created_at);
      this.week_days?.reverse();
      this.selected_start_month = this.week_days[0][0];
      const networkPayloadDate = new Date(this.week_days[0][1]);
      networkPayloadDate.setUTCDate(networkPayloadDate.getUTCDate() - 1);
      const formattedUTCDate = networkPayloadDate.toISOString();
      this.selected_end_month = formattedUTCDate;
      this.bar_chart_item_bsRangeValue[0] = new Date(this.selected_start_month);
      this.bar_chart_item_bsRangeValue[1] = new Date(this.selected_end_month);
      this.barChartComponent.parentCalled();
    } else{
      let months;
      let d2 = new Date();
      let d1 = new Date(this.created_at);
      months = (d2.getFullYear() - d1.getFullYear()) * 12;
      months -= d1.getMonth();
      months += d2.getMonth();
      let bet_months = months <= 0 ? 0 : months;
      let months_between = Math.ceil(bet_months/6) + 1;

      for (let index = 0; index < months_between; index++) {
        let date = this.week_days[index - 1] ? this.week_days[index - 1] : new Date();
        this.week_days.push(this._helper.getMonthDay(date, index).toString());
      }
      if (this.week_days.length == 8) {
        this.week_days = [...new Set(this.week_days)];
        this.selected_start_month = this.week_days[0];
        this.selected_end_month = this.week_days[1];
        this.bar_chart_item_bsRangeValue[0] = new Date(this.selected_start_month);
        this.bar_chart_item_bsRangeValue[1] = new Date(this.selected_end_month);
        this.barChartComponent.parentCalled();
      }
      this.selected_start_month = this.week_days[1];
      this.selected_end_month = this.week_days[0];
      this.bar_chart_item_bsRangeValue[0] = new Date(this.selected_start_month);
      this.bar_chart_item_bsRangeValue[1] = new Date(this.selected_end_month);
      this.barChartComponent.parentCalled();
    }
  }

  changeMonts(selected_start_month, selected_end_month) {
    this.selected_start_month = selected_start_month;
    const networkPayloadDate = new Date(selected_end_month);
    networkPayloadDate.setUTCDate(networkPayloadDate.getUTCDate() - 1);
    const formattedUTCDate = networkPayloadDate.toISOString();
    this.selected_end_month = formattedUTCDate;
    this.bar_chart_item_bsRangeValue[0] = new Date(this.selected_start_month);
    this.bar_chart_item_bsRangeValue[1] = new Date(this.selected_end_month);
    this.barChartComponent.parentCalled();
  }

  pieConfigMonthList() {
    this.pie_created_at = this.setting_created_date

    if (this.pie_created_at) {
      this.pie_week_days = this._helper.getSixMonthDifference(this.pie_created_at);
      this.pie_week_days?.reverse();
      this.pie_selected_start_month = this.pie_week_days[0][0];
      const networkPayloadDate = new Date(this.pie_week_days[0][1]);
      networkPayloadDate.setUTCDate(networkPayloadDate.getUTCDate() - 1);
      const formattedUTCDate = networkPayloadDate.toISOString();
      this.pie_selected_end_month = formattedUTCDate;
      this.pie_chart_item_bsRangeValue[0] = new Date(this.pie_selected_start_month);
      this.pie_chart_item_bsRangeValue[1] = new Date(this.pie_selected_end_month);
      this.pieChartComponent.parentCalled();
    } else{
      let months;
      let d2 = new Date();
      let d1 = new Date(this.pie_created_at);
      months = (d2.getFullYear() - d1.getFullYear()) * 12;
      months -= d1.getMonth();
      months += d2.getMonth();
      let bet_months = months <= 0 ? 0 : months;
      let months_between = Math.ceil(bet_months/6) + 1;

      for (let index = 0; index < months_between; index++) {
        let date = this.pie_week_days[index - 1] ? this.pie_week_days[index - 1] : new Date();
        this.pie_week_days.push(this._helper.getMonthDay(date, index).toString());
      }
      if (this.pie_week_days.length == 8) {
        this.pie_week_days = [...new Set(this.pie_week_days)];
        this.pie_selected_start_month = this.pie_week_days[0];
        this.pie_selected_end_month = this.pie_week_days[1];
        this.pie_chart_item_bsRangeValue[0] = new Date(this.pie_selected_start_month);
        this.pie_chart_item_bsRangeValue[1] = new Date(this.pie_selected_end_month);
        this.pieChartComponent.parentCalled();
      }
      this.pie_selected_start_month = this.pie_week_days[1];
      this.pie_selected_end_month = this.pie_week_days[0];
      this.pie_chart_item_bsRangeValue[0] = new Date(this.pie_selected_start_month);
      this.pie_chart_item_bsRangeValue[1] = new Date(this.pie_selected_end_month);
      this.pieChartComponent.parentCalled();
    }
  }

  pieChangeMonts(pie_selected_start_month, pie_selected_end_month) {
    this.pie_selected_start_month = pie_selected_start_month;
    const networkPayloadDate = new Date(pie_selected_end_month);
    networkPayloadDate.setUTCDate(networkPayloadDate.getUTCDate() - 1);
    const formattedUTCDate = networkPayloadDate.toISOString();
    this.pie_selected_end_month = formattedUTCDate;
    this.pie_chart_item_bsRangeValue[0] = new Date(this.pie_selected_start_month);
    this.pie_chart_item_bsRangeValue[1] = new Date(this.pie_selected_end_month);
    this.pieChartComponent.parentCalled();
  }

  orderChartMonthList() {
    this.order_created_at = this.setting_created_date;
    let d2 = new Date();
    let d1 = new Date(this.order_created_at);
    let months = (d2.getFullYear() - d1.getFullYear()) * 12;
    months += d2.getMonth() - d1.getMonth();
    if (d2.getMonth() >= d1.getMonth()) {
      months += 1;
    }
    let months_between = months < 0 ? 0 : months; // Ensure non-negative result
    for (let index = 0; index < months_between; index++) {
      if (this.order_created_at) {
        let date = this.order_week_days[index - 1] ? this.order_week_days[index - 1] : this.order_created_at;
        if (date == this.order_week_days[index - 2]) {
          this.order_week_days.pop();
          if (this.order_created_at) {
            this.order_selected_start_month = this.order_week_days[0];
            this.order_chart_item_bsRangeValue[0] = new Date(this.order_selected_start_month);
            this.orderChartComponent.parentCalled();
          } else {
            if (this.order_week_days.length == 8) {
              this.order_selected_start_month = this.order_week_days[0];
              this.order_chart_item_bsRangeValue[0] = new Date(this.order_selected_start_month);
              this.orderChartComponent.parentCalled();
            }

          }
          return;
        } else {

          this.order_week_days.push(this._helper.getNextMonth(date, index).toString())
        }
      }
    }

    if (this.order_created_at) {
      if ((this.order_week_days.length) == months_between){
        this.order_week_days.reverse();
        this.order_selected_start_month = this.order_week_days[0];
        this.order_chart_item_bsRangeValue[0] = new Date(this.order_selected_start_month);
        this.orderChartComponent.parentCalled();
      }
    }
  }

  orderChangeMonts(order_selected_start_month) {
    this.order_selected_start_month = order_selected_start_month;
    this.order_chart_item_bsRangeValue[0] = new Date(this.order_selected_start_month);
    this.orderChartComponent.parentCalled();
  }

  getList() {
    let json = {
      country_id: this.selected_country.country_id,
      start_date: this.start_date,
      end_date: this.end_date,

    }
    this._commonService.order_detail(json).then((res_data: any) => {
      if(res_data.success){
        if(res_data.order_detail){
          this.order_detail = res_data.order_detail
        }
        if(res_data.list){
          this.list = res_data.list
        }
        if(res_data.order_detail1){
          this.order_detail1 = res_data.order_detail1
        }
      }
    })
  }

  onChangeSearchBy(item): void {
    this.itemSelected = item;
    let data = Number(item.value)
    let start_date = "";
    let end_date = moment().format('YYYY-MM-DD');
    if (data == 1) {
      start_date = moment().format('YYYY-MM-DD');
      end_date = moment().add(1, 'days').format('YYYY-MM-DD').toString()
    } else if (data == 2) {
      start_date = moment().add(-2, 'days').format('YYYY-MM-DD').toString();
      end_date = moment().subtract({ hours: 23, minutes: 59 }).format('YYYY-MM-DD').toString();
    }
    else if (data == 3) {
      start_date = moment().startOf('isoWeek').format('YYYY-MM-DD').toString();
    }
    else if (data == 4) {
      start_date = moment().startOf('month').format('YYYY-MM-DD').toString();
    }
    else if (data == 5) {
      start_date = moment().startOf('year').format('YYYY-MM-DD').toString();
    } else {
      start_date = '';
      end_date = '';
    }
    if (data !== 6) {
      this.start_date = new Date(start_date)
      this.end_date = new Date(end_date)
      this.getList();
    }
  }

  apply() {
    if (this.item_bsRangeValue && this.item_bsRangeValue.length) {
      this.start_date = this.item_bsRangeValue[0];
      this.end_date = this.item_bsRangeValue[1];
    }
    this.getList();
  }

  getCountryList() {
    this._countryService.fetch().then(res => {
      if (res.success) {
        this.country_list = res.countries
        this.selected_country.country_id = '000000000000000000000000'
        this.country_name = ''
        this.currency_sign = ''
        if(this.country_list.length==1){

          this.selected_country.country_id =this.country_list[0]._id
          this.country_name=this.country_list[0].country_name;
          this.selected_country.currency_sign = this.country_list[0].currency_sign
          this.currency_sign = this.country_list[0].currency_sign;
        }
      }
    })
  }

  onChangeCountry(country): void {
    if(this.selected_country.country_id != '000000000000000000000000'){
      this.selected_country.country_id = country
      if(this.selected_country.country_id =='000000000000000000000000'){
        this.country_name = '';
        this.currency_sign = '';
      } else {
        let index = this.country_list.findIndex(x => x._id === country)
        this.country_name = this.country_list[index].country_name;
        this.currency_sign = this.country_list[index].currency_sign;
        this.selected_country.currency_sign = this.country_list[index].currency_sign
      }
      this._helper.country_id.next(this.selected_country);
    }
    this.barChartComponent.parentCalled();
    this.orderChartComponent.parentCalled();
    this.pieChartComponent.parentCalled();
    this.getList()
  }

}
