import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { AddNewOrderListModalComponent } from 'src/app/containers/pages/add-new-order-list-modal/add-new-order-list-modal.component';
import { TableOrderDetailModalComponent } from 'src/app/containers/pages/table-booking-detail-modal/table-booking-detail-modal.component';
import { OrderService } from 'src/app/services/order.service';
import { PER_PAGE_LIST } from 'src/app/shared/constant';
import { Helper } from 'src/app/shared/helper';

@Component({
  selector: 'app-history-list',
  templateUrl: './history-list.component.html',
  styleUrls: ['./history-list.component.scss']
})
export class HistoryListComponent implements OnInit, OnDestroy {
  direction = localStorage.getItem('direction');
  @ViewChild('OrderDetailModal', { static: true }) OrderDetailModal: AddNewOrderListModalComponent;
  @ViewChild('TableOrderDetailModalComponent', { static: true }) TableOrderDetailModal: TableOrderDetailModalComponent;
  is_clear_disabled:boolean=true;

  order: any = []
  orders = []
  business_type = []
  currentPage = 1;
  itemsPerPage = 20;
  count = 0;
  itemOptionsPerPage = PER_PAGE_LIST;
  itemOptionsOrders = [
    { label: 'label-title.completed', value: 10 },
    { label: 'label-title.rejected', value: 3 },
    { label: 'label-title.cancelled', value: 2 }];
    itemOrder = { label: '', value: '' };

  orderByList = [
    { label: 'label-title.user', value: 'user' },
    { label: 'label-title.store', value: 'store' },
    { label: 'label-title.deliveryman', value: 'deliveryman' },
    { label: 'label-title.order', value: 'order' }
  ];

  businessTypes = [
    { checked: false,type:1, name: 'label-title.store'},
    { checked: false,type:2, name: 'label-title.courier'},
    { checked: false,type:4,name: 'label-title.taxi'},
    { checked: false,type:5, name: 'label-title.service'},
    { checked: false,type:6, name: 'label-title.appoinment'},
    { checked: false,type:7, name: 'label-title.ecommerce'},
  ]

  selectedOrderBy = { label: 'label-title.user', value: 'user' };
  search_by = 'user';
  search_value = '';
  query = {
    "unique_id": null,
    "user_name": "",
    "store_name": "",
    "provider_name": "",
  };
  item_bsRangeValue;
  start_date = new Date(0);
  end_date = new Date();
  created_date:Date;
  todayDate:Date=new Date();
  timezone_for_display_date:string = '';

  constructor( private _orderService:OrderService,public _helper:Helper) { }

  ngOnInit(): void {
    this._helper.created_date.subscribe(data => {
      if(data){
        let date = new Date(data)
        this.created_date = date;        
      }
    })

    this._helper.display_date_timezone.subscribe(data => {
      this.timezone_for_display_date = data;
    })

    this.loadData()
  }

  loadData(){
    this._orderService.list_history(this.currentPage,this.itemsPerPage,this.query,this.start_date,this.end_date, this.itemOrder,this.business_type,).then(data=>{
      if(data.success){
        this.business_type=[]
        this.orders = data['orders'];
        this.count = data['count'];
      }else{
        this.business_type=[]
        this.orders = [];
        this.count = 0;
      }
    })
  }

  pageChanged(event) {
    this.query['business_type'] = this.businessTypes.filter(_t=>_t.checked);
    this.query['business_type'].forEach(business => {
      business.name = this._helper._translateService.instant(business.name)
      this.business_type.push(business.type)
    })
    this.currentPage = event.page;
    this.loadData()
  }

  itemsPerPageChange(event) {
    this.query['business_type'] = this.businessTypes.filter(_t=>_t.checked);
    this.query['business_type'].forEach(business => {
      business.name = this._helper._translateService.instant(business.name)
      this.business_type.push(business.type)
    })
    this.itemsPerPage = event;
    this.loadData()
  }


  onShowOrderDetails(orderid, delivery_type) {
    if(delivery_type == this._helper.DELIVERY_TYPE_CONSTANT.TABLE_BOOKING){
      this.TableOrderDetailModal.show(orderid)
    } else {
      this.OrderDetailModal.show(orderid, null , true)
    }
  }

  changeOrderBy(event){
    if(event.value === 'all'){
      this.search_by = undefined;
    }else{
      this.search_by = event.value;
    }
    this.is_clear_disabled = false;
  }


  searchKeyUp(value){
    this.search_value = value;
  }

  onSearch(){
    this.currentPage=1;
    this.is_clear_disabled = false;
    this.query['business_type'] = this.businessTypes.filter(_t=>_t.checked);
    this.query['business_type'].forEach(business => {
      business.name = this._helper._translateService.instant(business.name)
      this.business_type.push(business.type)
    })  

    if(this.item_bsRangeValue && this.item_bsRangeValue.length){
      this.start_date = this.item_bsRangeValue[0];
      this.end_date = this.item_bsRangeValue[1];
    }
    this.query = {
      "unique_id": null,
      "user_name": "",
      "store_name": "",
      "provider_name": "",
    };
    switch (this.search_by) {
      case 'user':
        this.query.user_name = this.search_value;
        break;
      case 'store':
        this.query.store_name = this.search_value;
        break;
      case 'deliveryman':
        this.query.provider_name = this.search_value;
        break;
      case 'order':
        this.query.unique_id = this.search_value;
        break;
      default:
        break;
    }
    this.loadData();
  }

  onExport(){
    this._orderService.list_history(1,this.count, this.query,this.start_date,this.end_date, this.itemOrder,this.business_type).then(data=>{
      if(data.success){
        data.orders.forEach(order => {
          if(order.delivery_type === 1){
            order.delivery = this._helper._translateService.instant('label-title.delivery')
          } else {
            order.delivery = this._helper._translateService.instant('label-title.courier')
          }
        });
        let exportFields = [
          { label:'ID',value:'unique_id'},
          { label:'Delivery Type',value:'delivery'},
          { label:'User',value:'user_detail.name'},
          { label:'Delivery Boy',value:'provider_detail.name'},
          { label:'Store',value:'store_detail.name[0]'},
          { label:'Completed At',value:'completed_at'},
          { label:'Total',value:'total'}
        ]
        this._helper.downloadcsvFile(data['orders'],exportFields);
      }
    })
  }

  onChangeOrderBy(item){
    this.itemOrder = item
    this.is_clear_disabled = false;
  }

  ngOnDestroy(): void {
    if(this.OrderDetailModal.modalRef){
      this.OrderDetailModal.onClose();
    }

    if(this.TableOrderDetailModal.modalRef){
      this.TableOrderDetailModal.onClose();
    }
  }
  clear_filter(){
    this.businessTypes.forEach((data:any)=>{
      data.checked =false;
    })
    this.selectedOrderBy ={ label: 'label-title.user', value: 'user' }
    this.itemOrder = { label: '', value: '' };
    this.item_bsRangeValue='';
    this.start_date = undefined;
    this.end_date = undefined;
    this.currentPage = 1;
    this.itemsPerPage  = 20; 
    this.search_value = '';
    this.query = {
      "unique_id": null,
      "user_name": "",
      "store_name": "",
      "provider_name": "",
    };
    this.search_by = 'user';
    this.loadData();
    this.is_clear_disabled = true;
  }
}
