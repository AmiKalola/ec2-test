import { Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import { UserReviewModalComponent } from 'src/app/containers/pages/user-review-modal/user-review-modal.component';
import { ProfileService } from 'src/app/services/profile.service';
import { PER_PAGE_LIST } from 'src/app/shared/constant';
import { UntypedFormGroup, UntypedFormControl } from '@angular/forms';
import { Helper } from 'src/app/shared/helper';

@Component({
  selector: 'app-reviews',
  templateUrl: './reviews.component.html',
  styleUrls: ['./reviews.component.scss']
})
export class ReviewsComponent implements OnInit, OnDestroy {
  direction = localStorage.getItem('direction');

  is_clear_disabled:boolean=true;

  reviews = []
  rating = [5,4,3,2,1]
  order_rating = {
    rating:[0,0,0,0,0],
    total:0,
    percentage:[0,0,0,0,0]
  };
  provider_rating = {
    rating:[0,0,0,0,0],
    total:0,
    percentage:[0,0,0,0,0]
  };
  total_provider_reviews = 0;

  count:number = 0;
  currentPage = 1;
  itemsPerPage = 20;
  query = {
    delivery_types: [],
    deliveryman_max_rating: 0,
    deliveryman_min_rating: 0,
    order_max_rating: 0,
    order_min_rating: 0
  };
  search_by = 'user';
  search_value= '';
  itemOptionsPerPage = PER_PAGE_LIST


  orderByList = [
    { label: 'label-title.user', value: 'user' },
    { label: 'label-title.store', value: 'store' },
    { label: 'label-title.deliveryman', value: 'deliveryman' },
    { label: 'label-title.order', value: 'order' }
  ];
  selectedOrderBy = { label: 'label-title.user', value: 'user' };

  deliveryTypes = [
    { name: 'label-title.delivery', checked: false },
    { name: 'label-title.pickup', checked: false },
  ];
  orderTypes = [
    { name: 'label-title.schedule', checked: false },
    { name: 'label-title.now', checked: false },
  ]

  businessTypes = [
    { checked: false, name: 'label-title.store'},
    { checked: false, name: 'label-title.courier'},
    { checked: false, name: 'label-title.taxi'},
    { checked: false, name: 'label-title.service'},
    { checked: false, name: 'label-title.appoinment'},
    { checked: false, name: 'label-title.ecommerce'},
  ]

  deliverySliderFormGroup: UntypedFormGroup;
  orderSliderFormGroup: UntypedFormGroup;
  start_date;
  end_date;
  selectedWeek;
  bsRangeValue: any;
  week_days = [];
  created_date:Date;
  todayDate:Date=new Date();
  timezone_for_display_date:string = '';

  @ViewChild('userReviewModalRef', { static: true }) userReviewModalRef: UserReviewModalComponent;

  constructor(private _profileService:ProfileService,public _helper:Helper) { }

  ngOnInit(): void {
    this._helper.created_date.subscribe(data => {
      if(data){
        let date = new Date(data)
        this.created_date = date;        
      }
    })

    this._helper.display_date_timezone.subscribe(data => {
      this.timezone_for_display_date = data;
    })

    this.configWeekDayList();
    this.bsRangeValue = [new Date(new Date().setDate(new Date().getDate() - 30)),new Date()]
    this._initData()
    this.deliverySliderFormGroup = new UntypedFormGroup({
      range: new UntypedFormControl([0, 0]),
    });
    this.orderSliderFormGroup = new UntypedFormGroup({
      range: new UntypedFormControl([0, 0]),
    });
  }


  configWeekDayList(){
    for (let index = 0; index < 30; index++) {
      let date = this.week_days[index - 1] ? new Date(new Date(this.week_days[index - 1][0]).setDate(new Date(this.week_days[index - 1][0]).getDate()-1)) : new Date();
      this.week_days.push(this._helper.getWeeks(date))
    }
    this.selectedWeek = this.week_days[0];
  }

  _initData(){
    this.resetReview()
    this._profileService.list_reviews(this.bsRangeValue[0], this.bsRangeValue[1], this.currentPage, this.itemsPerPage,this.query).then(res_data => {
      if(res_data.success){
        this.reviews = res_data.reviews;

        // Order Rating
        this.rating.forEach((_rating,idx) => {
          this.order_rating.rating[idx] = this.reviews.filter(_t=>_t.user_rating_to_store===_rating).length;
        });
        this.order_rating.rating.forEach(_rating => {
          this.order_rating.total += _rating
        });

        this.order_rating.rating.forEach((_rating,idx) => {
          this.order_rating.percentage[idx] = _rating === 0 ? 0 : ((_rating / this.order_rating.total) * 100)
        });

        // Provider Rating
        this.rating.forEach((_rating,idx) => {
          this.provider_rating.rating[idx] = this.reviews.filter(_t=>_t.user_rating_to_provider===_rating).length;
        });
        this.total_provider_reviews = this.reviews.filter(_t=>!_t.is_user_pick_up_order).length;
        this.provider_rating.rating.forEach(_rating => this.provider_rating.total +=  _rating);
        this.provider_rating.rating.forEach((_rating,idx) => {
          this.provider_rating.percentage[idx] = _rating === 0 ? 0 : ((_rating / this.provider_rating.total) * 100)
        });
      }
    })
  }

  showUserReviewModal(review): void {
    this.userReviewModalRef.show({
      created_date:new Date(review.completed_at),
      order_id:review.unique_id,
      order_rating:review.user_rating_to_store || 0,
      provider_rating:review.user_rating_to_provider || 0,
      user_name:review.user_name,
      user_phone:review.user_phone,
      user_image:review.user_image,
      provider_image:review.provider_image,
      provider_name:review.provider_name,
      provider_phone:review.provider_phone,
      store_image:review.store_image,
      store_name:review.store_name,
      store_phone:review.store_phone,
      order_comment:"",
      provider_comment:'',

      number_of_users_dislike_store_comment: review.number_of_users_dislike_store_comment,
      number_of_users_like_store_comment: review.number_of_users_like_store_comment,
      store_rating_to_user: review.store_rating_to_user,
      provider_rating_to_user:review.provider_rating_to_user,

      store_review_to_provider: review.store_review_to_provider,
      store_review_to_user: review.store_review_to_user,
      user_review_to_provider: review.user_review_to_provider,
      user_review_to_store: review.user_review_to_store,
      provider_review_to_store:review.provider_review_to_store,
      provider_review_to_user:review.provider_review_to_user,
      provider_rating_to_store:review.provider_rating_to_store,
      store_rating_to_provider:review.store_rating_to_provider,
      user_rating_to_provider:review.user_rating_to_provider,
      user_rating_to_store:review.user_rating_to_store,

    });
  }

  itemsPerPageChange(event){
    console.log("1");
    
  }
  
  pageChanged(event){
    console.log("2");
  }

  resetReview(){
    this.reviews = []
    this.rating = [5,4,3,2,1]
    this.order_rating = {
      rating:[0,0,0,0,0],
      total:0,
      percentage:[0,0,0,0,0]
    };
    this.provider_rating = {
      rating:[0,0,0,0,0],
      total:0,
      percentage:[0,0,0,0,0]
    };
  }

  changeOrderBy(event){
    if(event.value === 'all'){
      this.search_by = undefined;
    }else{
      this.search_by = event.value;
    }
    this.is_clear_disabled = false;
  }

  searchKeyUp(value){
    this.search_value = value;
  }

  onSearch(){
    this.is_clear_disabled = false;
    this.query['business_type'] = this.businessTypes.filter(_t=>_t.checked);
    this.query['business_type'].forEach(business => {
      business.name = this._helper._translateService.instant(business.name)
    })

    this.query['delivery_types'] = this.deliveryTypes.filter(_t=>_t.checked);
    this.query['delivery_types'].forEach(delivery => {
      delivery.name = this._helper._translateService.instant(delivery.name)
    })
    this.orderTypes.forEach(order => {
      order.name = this._helper._translateService.instant(order.name)
    })
    this.orderTypes.forEach(_t=>{if(_t.checked){this.query['delivery_types'].push(_t)}})

    if (this.search_by && this.search_value !== '' && this.search_value !== undefined) {
      this.query['search_by'] = this.search_by;
      this.query['search_value'] = this.search_value;
    } else {
      this.query['search_by'] = undefined;
      this.query['search_value'] = undefined;
    }

    this.query['deliveryman_min_rating'] = this.deliverySliderFormGroup.controls.range.value[0];
    this.query['deliveryman_max_rating'] = this.deliverySliderFormGroup.controls.range.value[1];
    this.query['order_min_rating'] = this.orderSliderFormGroup.controls.range.value[0];
    this.query['order_max_rating'] = this.orderSliderFormGroup.controls.range.value[1];

    this._initData();
  }

  onExport() {
    if (this.reviews.length) {
      this._helper.export_csv(this.reviews, Object.keys(this.reviews[0]), 'reviews')
    }
  }

  ngOnDestroy(): void {
    if(this.userReviewModalRef.modalRef){
      this.userReviewModalRef.onClose();
    }
  }
  clear_filter(){
    this.deliverySliderFormGroup = new UntypedFormGroup({
      range: new UntypedFormControl([0, 0]),
    });
    this.orderSliderFormGroup = new UntypedFormGroup({
      range: new UntypedFormControl([0, 0]),
    });
    this.query = {
      delivery_types: [],
      deliveryman_max_rating: 0,
      deliveryman_min_rating: 0,
      order_max_rating: 0,
      order_min_rating: 0
    };
   
    this.deliveryTypes.forEach((data:any)=>{
      data.checked =false;
    })
    this.orderTypes.forEach((data:any)=>{
      data.checked =false;
    })
    this.selectedOrderBy ={ label: 'label-title.user', value: 'user' }
    this.bsRangeValue = [new Date(new Date().setDate(new Date().getDate() - 30)),new Date()]
    this.start_date = undefined;
    this.end_date = undefined;
    this.currentPage = 1;
    this.itemsPerPage  = 20; 
    this.search_value = '';
    this.search_by = 'user';
    this._initData();
    this.is_clear_disabled = true;
  }
}
