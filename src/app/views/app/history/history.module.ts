import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HistoryRoutingModule } from './history-routing.module';
import { HistoryListComponent } from './history-list/history-list.component';
import { HistoryComponent } from './history.component';
import { LayoutContainersModule } from 'src/app/containers/layout/layout.containers.module';
import { PagesContainersModule } from 'src/app/containers/pages/pages.containers.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { FormsModule as FormsModuleAngular, ReactiveFormsModule } from '@angular/forms';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ModalModule } from 'ngx-bootstrap/modal';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { RatingModule } from 'ngx-bootstrap/rating';
import { ReviewsComponent } from './reviews/reviews.component';
import { ProgressbarModule } from 'ngx-bootstrap/progressbar';
import { PipeModule } from 'src/app/pipes/pipe.module';
import { DirectivesModule } from 'src/app/directives/directives.module';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { NouisliderModule } from 'ng2-nouislider';

@NgModule({
  declarations: [HistoryListComponent, HistoryComponent, ReviewsComponent],
  imports: [
    CommonModule,
    HistoryRoutingModule,
    BsDatepickerModule,
    SharedModule,
    LayoutContainersModule,
    CommonModule,
    PipeModule,
    DirectivesModule,
    PagesContainersModule,
    FormsModuleAngular,
    RatingModule.forRoot(),
    ReactiveFormsModule,
    PaginationModule.forRoot(),
    TabsModule.forRoot(),
    ModalModule.forRoot(),
    BsDropdownModule.forRoot(),
    AccordionModule.forRoot(),
    ProgressbarModule.forRoot(),
    NouisliderModule
  ]
})
export class HistoryModule { }
