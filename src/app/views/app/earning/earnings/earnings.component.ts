import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { EarningService } from 'src/app/services/earning.service';
import { PER_PAGE_LIST } from 'src/app/shared/constant';
import { Helper } from 'src/app/shared/helper';
import { AddNewOrderListModalComponent } from 'src/app/containers/pages/add-new-order-list-modal/add-new-order-list-modal.component';
import { PaymentGateWayService } from 'src/app/services/payment-gateway.service';
import { TranslateService } from '@ngx-translate/core';

export interface EarningResponse {
  _id:string,
  id:string,
  type:number,
  completed_date:Date,
  total:number,
  earning:number,
  deliveryman_earning:number,
  store_earning:number
}

@Component({
  selector: 'app-earnings',
  templateUrl: './earnings.component.html',
  styleUrls: ['./earnings.component.scss']
})
export class EarningsComponent implements OnInit, OnDestroy {
  direction = localStorage.getItem('direction');

  is_clear_disabled:boolean=true;
  earnings:EarningResponse[] = [];
  count:number = 0;
  currentPage = 1;
  itemsPerPage = 20;
  itemOptionsPerPage = PER_PAGE_LIST
  search_by = 'user';
  search_value = '';
  orderByList = [
    { label: 'label-title.user', value: 'user' },
    { label: 'label-title.store', value: 'store' },
    { label: 'label-title.deliveryman', value: 'deliveryman' },
    { label: 'label-title.order', value: 'order' }
  ];
  selectedOrderBy = { label: 'label-title.user', value: 'user' };
  deliveryTypes = [
    {name:'label-title.delivery',checked:false},
    {name:'label-title.pickup',checked:false},
    {name:'label-title.schedule',checked:false},
    {name:'label-title.now',checked:false}
  ];
  paymentBy = [
    {
      id:'cash',
      checked:false,
      name:'label-title.cash'
    }
  ];
  typeFilter = [
    {
      id:'free_delivery',
      checked:false,
      name:'label-title.free-delivery'
    },
    {
      id:'promo_applied',
      checked:false,
      name:'label-title.promo-applied'
    }
  ];

  businessTypes = [
    { checked: false,type:this._helper.DELIVERY_TYPE_CONSTANT.STORE, name: 'label-title.store'},
    { checked: false,type:this._helper.DELIVERY_TYPE_CONSTANT.COURIER, name: 'label-title.courier'},
    { checked: false,type:this._helper.DELIVERY_TYPE_CONSTANT.TAXI,name: 'label-title.taxi'},
    { checked: false,type:this._helper.DELIVERY_TYPE_CONSTANT.SERVICE, name: 'label-title.service'},
    { checked: false,type:this._helper.DELIVERY_TYPE_CONSTANT.APPOINMENT, name: 'label-title.appoinment'},
    { checked: false,type:this._helper.DELIVERY_TYPE_CONSTANT.ECOMMERCE, name: 'label-title.ecommerce'},
  ]

  query = {};
  item_bsRangeValue;
  start_date = new Date(0);
  end_date = new Date();
  created_date:Date;
  todayDate:Date=new Date();
  
  timezone_for_display_date:string = '';

  @ViewChild('addNewModalRef', { static: true }) addNewModalRef: AddNewOrderListModalComponent;
  business_type = [];

  constructor(private _earningService:EarningService,
    public _helper:Helper, 
    private _paymentGatewayService: PaymentGateWayService,
    private _translateService: TranslateService) { }


  ngOnInit(): void {
    this._helper.created_date.subscribe(data => {
      if(data){
        let date = new Date(data)
        this.created_date = date;        
      }
    })

    this._helper.display_date_timezone.subscribe(data => {
      this.timezone_for_display_date = data;
    })

    this._initData();
    this._paymentGatewayService.getPaymentGatewayList().then(data=>{
      if(data && data.success){
        data.payment_gateway.forEach(element => {
          this.paymentBy.push({
            id:element._id,
            checked:false,
            name:element.name || 'Other'
          })
        });
      }
    });
  }

  async _initData(){
    let res_data = await this._earningService.list(this.start_date,this.end_date,this.currentPage,this.itemsPerPage,this.query,this.business_type);
    this.earnings = res_data['results'];
    this.count = res_data['count'];
    this.business_type= []
  }

  onSearch(){
    this.currentPage=1;
    this.is_clear_disabled = false;
    this.query['delivery_types'] = this.deliveryTypes.filter(_t=>_t.checked);
    this.query['payment_by'] = this.paymentBy.filter(_t=>_t.checked);

    this.query['business_type'] = this.businessTypes.filter(_t=>_t.checked);
    this.query['business_type'].forEach(business => {
      business.name = this._helper._translateService.instant(business.name)
      this.business_type.push(business.type)
    })

    if(this.query['delivery_types']){
      this.query['delivery_types'].forEach(element => {
        element.name = this._translateService.instant(element.name).toLowerCase()
      });
    }

    if(this.search_by && this.search_value !== '' && this.search_value !== undefined){
      this.query['search_by'] = this.search_by;
      this.query['search_value'] = this.search_value;
    }else{
      this.query['search_by'] = undefined;
      this.query['search_value'] = undefined;
    }

    if(this.item_bsRangeValue && this.item_bsRangeValue.length){
      this.start_date = this.item_bsRangeValue[0];
      this.end_date = this.item_bsRangeValue[1];
    }
    this._initData();
  }

  itemsPerPageChange(event){
    this.query['business_type'] = this.businessTypes.filter(_t=>_t.checked);
    this.query['business_type'].forEach(business => {
      this.business_type.push(business.type)
    })
    this.itemsPerPage = event;
    this._initData()
  }

  changeOrderBy(event){
    this.search_by = event.value;
    this.selectedOrderBy = event
    this.is_clear_disabled = false;
  }

  pageChanged(event){
    this.query['business_type'] = this.businessTypes.filter(_t=>_t.checked);
    this.query['business_type'].forEach(business => {
      this.business_type.push(business.type)
    })
    this.currentPage = event.page;
    this._initData()
  }

  onDetail(order_id){
    this.addNewModalRef.show(order_id);
  }


  onExport(){

    let fields = [
      {label: 'ID', value: 'unique_id'},
      {label: 'Store', value: 'store_name'},
      {label: 'Provider', value: 'provider_name'},
      {label: 'User', value: 'user_name'},
      {label: 'Total',value: 'total'},
      {label: 'Deliveryman Earning',value: 'deliveryman_earn'},
      {label: 'Store Earning',value: 'store_earn'},
      {label: 'Admin Earning',value: 'admin_earn'},
      {label: 'Currency', value: 'order_currency_code'},
      {label: 'Completed At', value: 'completed_date_in_city_timezone'},
      {label: 'Payment By', value: 'payment_by'}
    ]
    this._earningService.list(new Date(0),new Date(),1,0,{},this.business_type).then(data=>{
      data.results.forEach(order => {
        if(order.is_paid_from_wallet){
          order.payment_by = 'Walet'
        } else if(order.is_payment_mode_cash){
          order.payment_by = 'Cash'
        } else {
          order.payment_by = 'Card'
        }
      });
      this._helper.downloadcsvFile(data['results'],fields);
    })
  }

  ngOnDestroy(): void {
    if (this.addNewModalRef.modalRef){
      this.addNewModalRef.onClose()
    }
  }
  clear_filter(){
    this.businessTypes.forEach((data:any)=>{
      data.checked =false;
    })
    this.deliveryTypes.forEach((data:any)=>{
      data.checked =false;
    })    
    this.paymentBy.forEach((data:any)=>{
      data.checked =false;
    })
    this.item_bsRangeValue = '';
    this.start_date = undefined;
    this.end_date = undefined;
    this.currentPage = 1;
    this.itemsPerPage = 20; 
    this.query= {};
    this.business_type = [];
    this.search_value = '';
    this.selectedOrderBy = { label: 'label-title.user', value: 'user' };
    this._initData();
    this.is_clear_disabled = true;
  }
}
