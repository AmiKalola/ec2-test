import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EarningRoutingModule } from './earning-routing.module';
import { EarningComponent } from './earning.component';
import { EarningsComponent } from './earnings/earnings.component';
import { LayoutContainersModule } from 'src/app/containers/layout/layout.containers.module';
import { PagesContainersModule } from 'src/app/containers/pages/pages.containers.module';
import { PipeModule } from 'src/app/pipes/pipe.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { FormsModule as FormsModuleAngular, ReactiveFormsModule } from '@angular/forms';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ModalModule } from 'ngx-bootstrap/modal';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { RatingModule } from 'ngx-bootstrap/rating';
import { CurrentWeekComponent } from './current-week/current-week.component';
import { DeliverymanEarningComponent } from './deliveryman-earning/deliveryman-earning.component';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { DirectivesModule } from 'src/app/directives/directives.module';

@NgModule({
  declarations: [EarningComponent, EarningsComponent, CurrentWeekComponent, DeliverymanEarningComponent],
  imports: [
    CommonModule,
    EarningRoutingModule,
    BsDatepickerModule,
    SharedModule,
    PipeModule,
    DirectivesModule,
    LayoutContainersModule,
    CommonModule,
    PagesContainersModule,
    PipeModule,
    FormsModuleAngular,
    RatingModule.forRoot(),
    ReactiveFormsModule,
    PaginationModule.forRoot(),
    TabsModule.forRoot(),
    ModalModule.forRoot(),
    BsDropdownModule.forRoot(),
    AccordionModule.forRoot(),
  ]
})
export class EarningModule { }
