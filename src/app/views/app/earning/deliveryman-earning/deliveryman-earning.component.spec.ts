import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DeliverymanEarningComponent } from './deliveryman-earning.component';

describe('DeliverymanEarningComponent', () => {
  let component: DeliverymanEarningComponent;
  let fixture: ComponentFixture<DeliverymanEarningComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ DeliverymanEarningComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeliverymanEarningComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
