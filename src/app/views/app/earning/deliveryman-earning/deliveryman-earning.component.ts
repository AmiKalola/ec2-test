import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { DeliverymanEarningDetailComponent } from '../../../../containers/pages/deliveryman-earning-detail/deliveryman-earning-detail.component';
import { EarningService } from 'src/app/services/earning.service';
import { Helper } from 'src/app/shared/helper';
import { PER_PAGE_LIST } from 'src/app/shared/constant';
@Component({
  selector: 'app-deliveryman-earning',
  templateUrl: './deliveryman-earning.component.html',
  styleUrls: ['./deliveryman-earning.component.scss']
})
export class DeliverymanEarningComponent implements OnInit, OnDestroy {

  is_clear_disabled:boolean=true;
  itemsPerPage = 20;
  itemOptionsPerPage = PER_PAGE_LIST;
  earnings = [];
  orderByList = [
    { label: 'label-title.name', value: 'provider_detail.first_name', isShow:true },
    { label: 'label-title.email', value: 'provider_detail.email' ,isShow:true }
  ];
  selectedOrderBy = { label: 'label-title.name', value: 'provider_detail.first_name' };
  search_value = ""
  start_date:any;
  end_date:any;
  item_bsRangeValue;
  selectedWeek:any = [];
  week_days = [];
  order_total: any= ''
  pages: number = 0
  created_date:Date;

  @ViewChild('addNewModalRef', { static: true }) addNewModalRef: DeliverymanEarningDetailComponent;

  constructor(private earningService:EarningService,public _helper:Helper) { }

  ngOnInit(): void {
    this._helper.created_date.subscribe(data => {
      if(data){
        let date = new Date(data)
        this.created_date = date;
        this.configWeekDayList();
      }
    })
  }

  configWeekDayList() {
    const startDate = new Date(this.created_date);
    const weeks = this._helper.getWeeks(startDate);
    this.week_days = weeks;
    this.week_days.reverse();
    this.selectedWeek = this.week_days[0];
    this.start_date = this.selectedWeek.start;
    this.end_date = this.selectedWeek.end;
    this._initData();
  }

  _initData(){
    let json = {
      start_date: new Date(this.start_date),
      end_date: new Date(this.end_date),
      search_field: this.selectedOrderBy.value,
      search_value: this.search_value,
      page: 1,
      number_of_rec: this.itemsPerPage
    };

    this.earningService.list_deliveryman_earning(json).then(data=>{
      if(data){
        this.orderByList.forEach((search) => {
          if(search.value == 'provider_detail.email' && data.is_show_email === false){
            search.isShow = false ;
          };
        })
      }
      if(data.success){
        this.earnings = data.provider_weekly_earnings;
        this.order_total = data.order_total
        this.pages = data.page
      }else{
        this.earnings = [];
      }
    })
  }


  changeOrderBy(item){
    this.selectedOrderBy = item;
    this.is_clear_disabled = false;
  }

  onSearch(){
    this.start_date = this.selectedWeek.start;
    this.end_date = this.selectedWeek.end;
    this.is_clear_disabled = false;
    this._initData();
  }

  onDetail(provider_id){
    this.addNewModalRef.show(provider_id,this.start_date,this.end_date);
  }

  onExport(){
    let export_data_list: any[] = [
      {label:"ID",value:"provider_detail.unique_id"},
      {label:"Deliveryman",value:"provider_detail.first_name"},
      {label:"Deliveryman Earn",value:"total_provider_earning"},
      {label:"Have Cash",value:"total_provider_have_cash_payment"},
      {label:"Wallet Deduct",value:"total_deduct_wallet_income"},
      {label:"Added In Wallet",value:"total_added_wallet_income"},
      {label:"Transferred",value:"total_transferred_amount"},
      {label:"Pay Deliveryman",value:"total_pay_to_provider"},
    ];
    this._helper.downloadcsvFile(this.earnings,export_data_list)
  }

  onChangeItemsPerPage(page){
    this.itemsPerPage = page
  }

  ngOnDestroy(): void {
    if(this.addNewModalRef.modalRef){
      this.addNewModalRef.closeModel()
    }
  }
  clear_filter(){
    this.selectedWeek = this.week_days[0];
    this.start_date = this.selectedWeek.start;
    this.end_date = this.selectedWeek.end;
    this.selectedOrderBy = { label: 'label-title.name', value: 'provider_detail.first_name'  };
    this.search_value ='';
    this.pages= 0;
    this.itemsPerPage=20;
    this._initData();
    this.is_clear_disabled = true;
  }

}
