import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { StoreEarningDetailComponent } from 'src/app/containers/pages/store-earning-detail/store-earning-detail.component';
import { EarningService } from 'src/app/services/earning.service';
import { Helper } from 'src/app/shared/helper';
import { LangService } from 'src/app/shared/lang.service';
import { PER_PAGE_LIST } from 'src/app/shared/constant';
@Component({
  selector: 'app-current-week',
  templateUrl: './current-week.component.html',
  styleUrls: ['./current-week.component.scss']
})
export class CurrentWeekComponent implements OnInit, OnDestroy {

  is_clear_disabled:boolean=true;
  itemsPerPage = 20;
  itemOptionsPerPage = PER_PAGE_LIST;
  earnings = [];
  orderByList = [
    { label: 'label-title.name', value: 'store_detail.name',isShow:true },
    { label: 'label-title.email', value: 'store_detail.email',isShow:true }
  ];
  selectedOrderBy = { label: 'label-title.name', value: 'store_detail.name' };
  search_value = ""
  start_date:any;
  end_date:any;
  item_bsRangeValue;
  selectedWeek:any = [] ;
  week_days = [];
  pages: number = 0;
  created_date:Date;

  @ViewChild('addNewModalRef', { static: true }) addNewModalRef: StoreEarningDetailComponent;

  constructor(private earningService:EarningService,public _helper:Helper, private _lang: LangService) { }

  ngOnInit(): void {
    this._helper.created_date.subscribe(data => {
      if(data){
        let date = new Date(data)
        this.created_date = date;
        this.configWeekDayList();
      }
    })
  }
  
  configWeekDayList() {
    const startDate = new Date(this.created_date);
    const weeks = this._helper.getWeeks(startDate);
    this.week_days = weeks;
    this.week_days.reverse();
    this.selectedWeek = this.week_days[0];
    this.start_date = this.selectedWeek.start;
    this.end_date = this.selectedWeek.end;
    this._initData();
  }

  _initData(){
    this.earningService.list_store_earning({
      start_date: new Date(this.start_date),
      end_date: new Date(this.end_date),
      search_field: this.selectedOrderBy.value,
      search_value: this.search_value,
      page: 1,
      number_of_rec: this.itemsPerPage
    }).then(data=>{
      if(data){
        this.orderByList.forEach((search) => {
          if(search.value == 'store_detail.email' && data.is_show_email === false){
            search.isShow = false ;
          };
        })
      }

      if(data.success){
        this.earnings = data.store_weekly_earnings;
        this.pages = data.pages;
      }else{
        this.earnings = [];
      }
    })
  }


  onSearch(){
    this.start_date = this.selectedWeek.start;
    this.end_date = this.selectedWeek.end;
    this.is_clear_disabled = false;
    this._initData();
  }

  onDetail(store_id){
    this.addNewModalRef.show(store_id,this.start_date,this.end_date);
  }

  changeOrderBy(item){
    this.selectedOrderBy = item;
    this.is_clear_disabled = false;
  }

  onExport(){
    let export_data_list: any[] = [
      {label: "ID",value:"store_detail.unique_id"},
      {label: "Name",value:"store_detail.name"},
      {label: "Email",value:"store_detail.email"},
      {label: "Total Orders",value:"total_order"},
      {label: "Store Earning",value:"total_store_earning"},
      {label: "Have Order Payment",value:"store_have_order_payment"},
      {label: "Deduct Wallet Income",value:"total_deduct_wallet_income"},
      {label: "Added Wallet Income",value:"total_added_wallet_income"},
      {label: "Total Transferred Amount",value:"total_transferred_amount"},
      {label: "Pay To Store",value:"total_pay_to_store"}
    ];
    this._helper.downloadcsvFile(this.earnings,export_data_list)
  }

  onChangeItemsPerPage(page){
    this.itemsPerPage = page
  }

  ngOnDestroy(): void {
    if(this.addNewModalRef.modalRef){
      this.addNewModalRef.closeModel();
    }
  }
  clear_filter(){
    this.selectedWeek = this.week_days[0];
    this.start_date = this.selectedWeek.start;
    this.end_date = this.selectedWeek.end;
    this.selectedOrderBy = { label: 'label-title.name', value: 'store_detail.name' };
    this.search_value ='';
    this.pages= 0;
    this.itemsPerPage=20;
    this._initData();
    this.is_clear_disabled = true;
  }
}
