import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WalletHistoryComponent } from '../delivery-info/wallet-history/wallet-history.component';
import { WalletRequestComponent } from '../delivery-info/wallet-request/wallet-request.component';
import { DeliverymanEarningComponent } from '../earning/deliveryman-earning/deliveryman-earning.component';
import { CurrentWeekComponent } from '../earning/current-week/current-week.component';
import { EarningComponent } from './earning.component'
import { EarningsComponent } from './earnings/earnings.component'
import { TransactionHistoryComponent } from '../delivery-info/transaction-history/transaction-history.component';
import { RedeemHistoryComponent } from '../delivery-info/redeem-history/redeem-history.component';


const routes: Routes = [
  {
    path: '', component: EarningComponent,
    children: [
      { path: '', redirectTo: 'order', pathMatch: 'full' },
      {
        path: 'order', children: [
          { path: '', redirectTo: 'order-earning', pathMatch: 'full' },
          { path: 'order-earning', component: EarningsComponent, data: {auth: '/admin/order-earning'} },
          { path: 'deliveryman-weekly', component: DeliverymanEarningComponent, data: {auth: '/admin/deliveryman-weekly'} },
          { path: 'store-earning', component: CurrentWeekComponent, data: {auth: '/admin/store-earning'} },
        ]
      },
      {
        path: 'wallet', children: [
          { path: '', redirectTo: 'wallet-history', pathMatch: 'full' },
          { path: 'wallet-history', component: WalletHistoryComponent, data: {auth: '/admin/wallet-history'} },
          { path: 'wallet-request', component: WalletRequestComponent, data: {auth: '/admin/wallet-request'} },
          { path: 'transaction-history', component: TransactionHistoryComponent, data: {auth: '/admin/transaction-history'} },
          { path: 'redeem-history', component: RedeemHistoryComponent , data: {auth: '/admin/redeem-history'} },
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EarningRoutingModule { }
