import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { SidebarService, ISidebar } from 'src/app/containers/layout/sidebar/sidebar.service';
import { AdminSettingService } from 'src/app/services/admin-setting.service';
import { CommonService } from 'src/app/services/common.service';

@Component({
  selector: 'app-app',
  templateUrl: './app.component.html'
})
export class AppComponent implements OnInit, OnDestroy {
  sidebar: ISidebar;
  subscription: Subscription;
  banners: any[] = [];
  is_banner_visible: boolean = false;
  constructor(private sidebarService: SidebarService, private commonService: CommonService ,  private _adminSettingService: AdminSettingService) {
  }

  ngOnInit(): void {
    this.subscription = this.sidebarService.getSidebar().subscribe({
      next: res => {
        this.sidebar = res;
      },
      error: err => {
        console.error(`An error occurred: ${err.message}`);
      }
    });
    let notification = false
    this._adminSettingService.fetchAdminSetting(notification).then(res => {
      if (res.success) {
        this.is_banner_visible = res?.setting?.is_banner_visible
        if (this.is_banner_visible) {
          this.getBanners();
        }
      }
    });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  getBanners() {
    this.commonService.get_banner().then((res) => {
      this.banners = res.banners.filter(banner => banner.is_visible);     
    });
  }

}
