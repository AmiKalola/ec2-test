import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HotkeyModule } from 'angular2-hotkeys';
import { MenuRoutingModule } from './menu-routing.module';
import { MenuComponent } from './menu.component';
import { CategoryComponent } from './category/category.component';
import { ItemListComponent } from './item-list/item-list.component';
import { ModifierGroupComponent } from './modifier-group/modifier-group.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { LayoutContainersModule } from 'src/app/containers/layout/layout.containers.module';
import { PagesContainersModule } from 'src/app/containers/pages/pages.containers.module';
import { FormsModule as FormsModuleAngular, ReactiveFormsModule } from '@angular/forms';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ModalModule } from 'ngx-bootstrap/modal';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { RatingModule } from 'ngx-bootstrap/rating';
import { PipeModule } from 'src/app/pipes/pipe.module';
import { DirectivesModule } from 'src/app/directives/directives.module';
import { CourierServiceComponent } from './courier-service/courier-service.component';
import { ContextMenuModule } from '@perfectmemory/ngx-contextmenu';
@NgModule({
  declarations: [MenuComponent, CategoryComponent, ItemListComponent, ModifierGroupComponent, CourierServiceComponent],
  imports: [
    SharedModule,
    LayoutContainersModule,
    CommonModule,
    PipeModule,
    DirectivesModule,
    MenuRoutingModule,
    PagesContainersModule,
    FormsModuleAngular,
    RatingModule.forRoot(),
    ReactiveFormsModule,
    HotkeyModule.forRoot(),
    PaginationModule.forRoot(),
    TabsModule.forRoot(),
    ModalModule.forRoot(),
    BsDropdownModule.forRoot(),
    AccordionModule.forRoot(),
    ContextMenuModule
  ]
})
export class MenuModule { }
