import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { AddNewCategoryModalComponent } from 'src/app/containers/pages/add-new-category-modal/add-new-category-modal.component';
import { SetStoreModalComponent } from 'src/app/containers/pages/set-store-id-model/set-store-id-model.component';
import { HotkeysService, Hotkey } from 'angular2-hotkeys';
import { CategoryService } from 'src/app/services/category.service';
import { IProduct } from 'src/app/data/api.service';
import { Category } from 'src/app/models/category.model';
import { environment } from 'src/environments/environment';
import { Subscription } from 'rxjs';
import { Helper } from 'src/app/shared/helper';
import { DELIVERY_TYPE } from '../../constant';
import { ContextMenuComponent, ContextMenuService } from '@perfectmemory/ngx-contextmenu';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class CategoryComponent implements OnInit, OnDestroy {
  displayMode = 'image';
  selectAllState = '';
  selected: IProduct[] = [];
  data: IProduct[] = [];
  currentPage = 1;
  itemsPerPage = 8;
  search = '';
  orderBy = '';
  isLoading: boolean;
  endOfTheList = false;
  totalItem = 0;
  totalPage = 0;
  DEFAULT_IMAGE = this._helper.DEFAULT_IMAGE_PATH.CATEGORY;
  search_string: '';
  search_area: 'name';
  categories: Category[] = [];
  IMAGE_URL = environment.imageUrl;
  changeStoreSubscription: Subscription;
  changeCategorySubscription: Subscription;
  is_store_can_add_category: boolean = false;

  @ViewChild('basicMenu') public basicMenu: ContextMenuComponent<any>;
  @ViewChild('addNewModalRef', { static: true }) addNewModalRef: AddNewCategoryModalComponent;
  @ViewChild('storeModalRef', { static: true }) storeModalRef: SetStoreModalComponent;
  city_id: any;
  isInitDataCalled:boolean = false;

  constructor(private hotkeysService: HotkeysService, private _categoryService: CategoryService, private _helper: Helper,private _contextMenuService: ContextMenuService<any>) {
    this.hotkeysService.add(new Hotkey('ctrl+a', (event: KeyboardEvent): boolean => {
      this.selected = [...this.data];
      return false;
    }));
    this.hotkeysService.add(new Hotkey('ctrl+d', (event: KeyboardEvent): boolean => {
      this.selected = [];
      return false;
    }));
  }

  ngOnInit(): void {
      this.changeStoreSubscription = this._helper._selected_store_id.subscribe((data) => {
        if(data){
          this._helper.selected_store_id = data;
          this.triggerInitData();
        }else{
          this.triggerInitData();
        }
      })
      this.changeCategorySubscription = this._categoryService._categoryObservable.subscribe((data) => {
        if (data != null) {
          this.triggerInitData();
        }
      })
      this._helper.changeSelectedStore.subscribe(() => {
        if (this._helper.selected_store) {
          this.is_store_can_add_category = this._helper.delivery_type !== DELIVERY_TYPE.STORE ? this._helper.selected_store.is_store_can_create_group  : this._helper.selected_store.delivery_details.is_store_can_create_group;
          this.is_store_can_add_category = this._helper.selected_store.delivery_details?.is_store_can_create_group
        }
      })
  }

  triggerInitData() {
    if (!this.isInitDataCalled) {
      this.isInitDataCalled = true;
      this._initData();
      setTimeout(() => {
        this.isInitDataCalled = false;
      }, 1000);
    }
  }

  _initData() {
    if (this._helper.is_city_selected) {
      this.city_id = this._helper.selected_city._id
    }
    else {
      this.city_id = null
    }

    if (this._helper.selected_store_id) {
      this.getCategoryList(this._helper.selected_store_id, this.city_id);
    } else if (!this._helper.isStoremodelOpen) {
      this.storeModalRef.show()
    }
  }

  async getCategoryList(store_id, city_id) {
    this._categoryService.list(store_id).then(data => {
      if (data.success) {
        this.categories = data.product_groups;
      } else {
        this.categories = []
      }
    })
  }

  changeDisplayMode(mode): void {
    this.displayMode = mode;
  }

  showAddNewModal(is_edit = false, edit_id = null): void {
    if(this._helper.has_permission(this._helper.PERMISSION.ADD) && !is_edit){     
      this.addNewModalRef.show(is_edit, edit_id, this.categories.length);
    }
    if(this._helper.has_permission(this._helper.PERMISSION.EDIT) && is_edit && this.is_store_can_add_category){     
      this.addNewModalRef.show(is_edit, edit_id, this.categories.length);
    }
  }


  isSelected(p: IProduct): boolean {
    return this.selected.findIndex(x => x.id === p.id) > -1;
  }

  onSelect(item: IProduct): void {
    if (this.isSelected(item)) {
      this.selected = this.selected.filter(x => x.id !== item.id);
    } else {
      this.selected.push(item);
    }
    this.setSelectAllState();
  }

  setSelectAllState(): void {
    if (this.selected.length === this.data.length) {
      this.selectAllState = 'checked';
    } else if (this.selected.length !== 0) {
      this.selectAllState = 'indeterminate';
    } else {
      this.selectAllState = '';
    }
  }

  selectAllChange($event): void {
    if ($event.target.checked) {
      this.selected = [...this.data];
    } else {
      this.selected = [];
    }
    this.setSelectAllState();
  }

  onContextMenuClick(action: string, item: any): void {
    if(action == 'delete'){
      this._categoryService.delete(item._id)
    }
  }

  pageChanged(event: any): void { 
    console.log(event);
  }

  itemsPerPageChange(perPage: number): void { 
    console.log(perPage);
  }

  searchKeyUp(event): void {
    const val = event.target.value.toLowerCase().trim();
    this.search_string = val;
  }

  ngOnDestroy() {
    if (this.changeStoreSubscription) {
      this.changeStoreSubscription.unsubscribe()
    }
    if (this.changeCategorySubscription) {
      this.changeCategorySubscription.unsubscribe()
    }
    if (this.storeModalRef.modalRef) {
      this.storeModalRef.onClose();
    }
    if(this.basicMenu){
      this._contextMenuService.closeAll();
    }
  }

}
