import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MenuComponent } from './menu.component';
import { CategoryComponent } from './category/category.component';
import { ItemListComponent } from './item-list/item-list.component';
import { ModifierGroupComponent } from './modifier-group/modifier-group.component';

const routes: Routes = [
  {
    path: '', component: MenuComponent,
    children: [
      { path: '', redirectTo: 'category', pathMatch: 'full' },
      { path: 'category', component: CategoryComponent, data: {auth: '/admin/category'} },
      { path: 'item-list', component: ItemListComponent, data: {auth: '/admin/item-list'} },
      { path: 'modifier-group', component: ModifierGroupComponent, data: {auth: '/admin/modifier-group'} }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MenuRoutingModule { }
