import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { AddNewModifierModalComponent } from 'src/app/containers/pages/add-new-modifier-modal/add-new-modifier-modal.component';
import { HotkeysService, Hotkey } from 'angular2-hotkeys';
import { IProduct } from 'src/app/data/api.service';
import { ModifierService } from 'src/app/services/modifier.service';
import { Subscription } from 'rxjs';
import { Helper } from 'src/app/shared/helper';
import { SetStoreModalComponent } from 'src/app/containers/pages/set-store-id-model/set-store-id-model.component';
import { DELIVERY_TYPE_CONSTANT } from 'src/app/shared/constant';

@Component({
  selector: 'app-courier-service',
  templateUrl: './courier-service.component.html',
  styleUrls: ["./courier-service.component.scss"]
})
export class CourierServiceComponent implements OnInit,OnDestroy {
  displayMode = 'image';
  selectAllState = '';
  selected: IProduct[] = [];
  data: IProduct[] = [];
  currentPage = 1;
  itemsPerPage = 8;
  search = '';
  orderBy = '';
  isLoading: boolean;
  endOfTheList = false;
  totalItem = 0;
  totalPage = 0;

  modifiers = [];
  modifierSubscription:Subscription;
  changeStoreSubscription:Subscription;
  search_string = ''
  isInitDataCalled:boolean = false;

  @ViewChild('addNewModalRef', { static: true }) addNewModalRef: AddNewModifierModalComponent;
  @ViewChild('storeModalRef', { static: true }) storeModalRef: SetStoreModalComponent;

  constructor( private hotkeysService: HotkeysService,public _helper:Helper,
    private _modifierService:ModifierService) {
    this.hotkeysService.add(new Hotkey('ctrl+a', (event: KeyboardEvent): boolean => {
      this.selected = [...this.data];
      return false;
    }));
    this.hotkeysService.add(new Hotkey('ctrl+d', (event: KeyboardEvent): boolean => {
      this.selected = [];
      return false;
    }));
  }


  ngOnInit(): void {
    this._helper.selected_store_id = '';
    this._helper.delivery_type = DELIVERY_TYPE_CONSTANT.COURIER
    
    this.changeStoreSubscription = this._helper.changeSelectedStore.subscribe((data)=>{
      this.triggerInitData();
    })
    this.modifierSubscription = this._modifierService._modifierObservable.subscribe((data)=>{
      if(data){
        this.triggerInitData();
      }
    })
  }

  triggerInitData() {
    if (!this.isInitDataCalled) {
      this.isInitDataCalled = true;
      this._initData();
      setTimeout(() => {
        this.isInitDataCalled = false;
      }, 2000);
    }
  }
  
  _initData(){
      this._modifierService.list_group().then(data=>{
        if(data.success){
          this.modifiers = data.specification_group
          for (const iterator of this.modifiers) {
            iterator.list.sort((a, b) => parseFloat(a.sequence_number) - parseFloat(b.sequence_number));
          }
        }
      })
  }

  changeDisplayMode(mode): void {
    this.displayMode = mode;
  }

  showAddNewModal(is_edit = false,edit_id = null): void {
    this.addNewModalRef.show(is_edit,edit_id);
  }  

  isSelected(p: IProduct): boolean {
    return this.selected.findIndex(x => x.id === p.id) > -1;
  }
  onSelect(item: IProduct): void {
    if (this.isSelected(item)) {
      this.selected = this.selected.filter(x => x.id !== item.id);
    } else {
      this.selected.push(item);
    }
    this.setSelectAllState();
  }

  setSelectAllState(): void {
    if (this.selected.length === this.data.length) {
      this.selectAllState = 'checked';
    } else if (this.selected.length !== 0) {
      this.selectAllState = 'indeterminate';
    } else {
      this.selectAllState = '';
    }
  }

  selectAllChange($event): void {
    if ($event.target.checked) {
      this.selected = [...this.data];
    } else {
      this.selected = [];
    }
    this.setSelectAllState();
  }

  pageChanged(event: any): void {
    //notused
  }
  
  itemsPerPageChange(perPage: number): void {
    //notused
  }
  
  changeOrderBy(item: any): void {
    //notused
  }

  searchKeyUp(event): void {
    const val = event.target.value.toLowerCase().trim();
    this.search_string = val;
  }

  onContextMenuClick(action: string, item: IProduct): void {
  }

  ngOnDestroy(){
    if(this.modifierSubscription){
      this.modifierSubscription.unsubscribe()
    }
    if(this.changeStoreSubscription){
      this.changeStoreSubscription.unsubscribe()
    }
  }
}
