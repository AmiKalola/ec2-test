import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { AddNewItemModalComponent } from 'src/app/containers/pages/add-new-item-modal/add-new-item-modal.component';
import { AddNewSubCategoryModalComponent } from 'src/app/containers/pages/add-new-sub-category-modal/add-new-sub-category-modal.component';
import { HotkeysService, Hotkey } from 'angular2-hotkeys';
import { SubcategoryService } from 'src/app/services/subcategory.service';
import { IProduct } from 'src/app/data/api.service';
import { Subscription } from 'rxjs';
import { LangService } from 'src/app/shared/lang.service';
import { ItemModel, ItemService } from 'src/app/services/item.service';
import { environment } from 'src/environments/environment';
import { Helper } from 'src/app/shared/helper';
import { SetStoreModalComponent } from 'src/app/containers/pages/set-store-id-model/set-store-id-model.component';
import { ContextMenuComponent, ContextMenuService } from '@perfectmemory/ngx-contextmenu';

@Component({
  selector: 'app-item-list',
  templateUrl: './item-list.component.html',
  styleUrls: ['./item-list.component.scss'],
})
export class ItemListComponent implements OnInit, OnDestroy {
  displayMode = '';
  selectAllState = '';
  selected: IProduct[] = [];
  data: IProduct[] = [];
  productArray: any = [];
  sequenceNumber: any = [];
  maxSequenceNumber: any = 0;
  currentPage = 1;
  itemsPerPage = 9;
  search = '';
  orderBy = '';
  isLoading: boolean;
  endOfTheList = false;
  totalItem = 0;
  totalPage = 0;
  DEFAULT_IMAGE = this._helper.DEFAULT_IMAGE_PATH.CATEGORY;

  subcategories: any = []
  items = []
  itemSubscription: Subscription;
  subcategorySubscription: Subscription;
  selectedSubcategory;
  IMAGE_URL = environment.imageUrl;
  search_area = 'name'
  search_string = ''
  changeStoreSubscription: Subscription;

  @ViewChild('basicMenu') public basicMenu: ContextMenuComponent<any>;
  @ViewChild('addNewItemModalRef', { static: true }) addNewItemModalRef: AddNewItemModalComponent;
  @ViewChild('addNewSubCategoryRef', { static: true }) addNewSubCategoryModalRef: AddNewSubCategoryModalComponent;
  @ViewChild('storeModalRef', { static: true }) storeModalRef: SetStoreModalComponent;

  constructor(private hotkeysService: HotkeysService,
    private itemService: ItemService,
    public _helper: Helper,
    private _subcategoryService: SubcategoryService,
    public _lang: LangService,
    private _contextMenuService: ContextMenuService<any>) {
    this.hotkeysService.add(new Hotkey('ctrl+a', (event: KeyboardEvent): boolean => {
      this.selected = [...this.data];
      return false;
    }));
    this.hotkeysService.add(new Hotkey('ctrl+d', (event: KeyboardEvent): boolean => {
      this.selected = [];
      return false;
    }));
  }


  ngOnInit(): void {
      this.changeStoreSubscription = this._helper.changeSelectedStore.subscribe((data) => {
        this._initData();
      })
      this.subcategorySubscription = this._subcategoryService._subcategoryObservable.subscribe(data => {
        if (data) {
          this.loadSubcategoryList(this._helper.selected_store_id, this._helper.delivery_type)
        }
      })
      this.itemSubscription = this.itemService._itemObservable.subscribe(data => {
        if (data) {
          setTimeout((t)=>{
             this.loadItemList(false);
          },100)
        }
      })
  } 

  _initData() {
    if (this._helper.selected_store_id) {
      this.loadSubcategoryList(this._helper.selected_store_id, this._helper.delivery_type);
    } else if (!this._helper.isStoremodelOpen) {
      this.storeModalRef.show()
    }
  }


  async loadSubcategoryList(store_id, delivery_type) {
    this.sequenceNumber = []
    this._subcategoryService.list(store_id, delivery_type).then(data => {
      if (data.success) {
        this.subcategories = data.product_array;
        if (this.subcategories.length) {
          this.selectedSubcategory = this.subcategories[0]._id;
          this.loadItemList(false)
        } else {
          this.items = []
        }
      }

    })
  }

  loadItemList(boolean) {
    this.itemService.list(this.selectedSubcategory, this._helper.selected_store_id).then(data => {
      this.items = []
      this.productArray = data.products
      if (data.success) {
        if (data.products.length > 0) {
          let index = data.products.findIndex(_x => _x._id === this.selectedSubcategory)
          this.items = data.products[index].items
        } else {
          this.items = []
        }
      } else {
        this.items = []
      }
    })
  }

  changeDisplayMode(mode): void {
    this.displayMode = mode;
  }

  showAddNewModal(is_edit = false, edit_id = null): void {
    if(this._helper.has_permission(this._helper.PERMISSION.ADD) && !is_edit){     
      this.addNewItemModalRef.show(is_edit, edit_id, this.selectedSubcategory);
    }
    if(this._helper.has_permission(this._helper.PERMISSION.EDIT) && is_edit){     
      this.addNewItemModalRef.show(is_edit, edit_id, this.selectedSubcategory);
    }
  }

  showAddNewSubCategoryModal(is_edit = false, edit_id = null): void {
    this.addNewSubCategoryModalRef.show(is_edit, edit_id);
  }

  isSelected(p: IProduct): boolean {
    return this.selected.findIndex(x => x.id === p.id) > -1;
  }

  isSubcategorySelected(s): boolean {
    return s._id.toString() === this.selectedSubcategory ? this.selectedSubcategory.toString() : "";
  }

  onSelectSubcategory(s): void {
    this.selectedSubcategory = s;
    this.loadItemList(false)
  }

  onSelect(item: IProduct): void {
    if (this.isSelected(item)) {
      this.selected = this.selected.filter(x => x.id !== item.id);
    } else {
      this.selected.push(item);
    }
    this.setSelectAllState();
  }

  setSelectAllState(): void {
    if (this.selected.length === this.data.length) {
      this.selectAllState = 'checked';
    } else if (this.selected.length !== 0) {
      this.selectAllState = 'indeterminate';
    } else {
      this.selectAllState = '';
    }
  }

  selectAllChange($event): void {
    if ($event.target.checked) {
      this.selected = [...this.data];
    } else {
      this.selected = [];
    }
    this.setSelectAllState();
  }

  onContextMenuClick(action: string, item: ItemModel): void {
    switch (action) {
      case 'copy':
        this.addNewItemModalRef.show(false, null, this.selectedSubcategory, item);
        break;
      case 'delete':
        item.is_visible_in_store = !item.is_visible_in_store;
        this.itemService.update(item);
        break;
      case 'out-of-stock':
        item.is_item_in_stock = !item.is_item_in_stock;
        this.itemService.update(item);
        break;
      default:
        break;
    }
  }

  pageChanged(event: any): void {
    //nouse in this file
  }
  
  itemsPerPageChange(perPage: number): void {
    //nouse in this file
  }

  changeOrderBy(item: any): void {
    this.search_area = item.value;
  }

  searchKeyUp(event): void {
    const val = event.target.value.toLowerCase().trim();
    this.search_string = val;
  }

  ngOnDestroy() {
    if (this.addNewItemModalRef.modalRef) {
      this.addNewItemModalRef.onClose()
    }
    if (this.subcategorySubscription) {
      this.subcategorySubscription.unsubscribe()
    }
    if (this.changeStoreSubscription) {
      this.changeStoreSubscription.unsubscribe()
    }
    if (this.storeModalRef.modalRef) {
      this.storeModalRef.onClose();
    }
    if(this.basicMenu){
      this._contextMenuService.closeAll();
    }
  }
}

