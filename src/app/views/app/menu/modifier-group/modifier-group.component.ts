import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { AddNewModifierModalComponent } from 'src/app/containers/pages/add-new-modifier-modal/add-new-modifier-modal.component';
import { HotkeysService, Hotkey } from 'angular2-hotkeys';
import { IProduct } from 'src/app/data/api.service';
import { ModifierService } from 'src/app/services/modifier.service';
import { Subscription } from 'rxjs';
import { Helper } from 'src/app/shared/helper';
import { SetStoreModalComponent } from 'src/app/containers/pages/set-store-id-model/set-store-id-model.component';

@Component({
  selector: 'app-modifier-group',
  templateUrl: './modifier-group.component.html',
  styleUrls: ["./modifier-group.component.scss"]
})
export class ModifierGroupComponent implements OnInit, OnDestroy {
  displayMode = 'image';
  selectAllState = '';
  selected: IProduct[] = [];
  data: IProduct[] = [];
  currentPage = 1;
  itemsPerPage = 8;
  search = '';
  orderBy = '';
  isLoading: boolean;
  endOfTheList = false;
  totalItem = 0;
  totalPage = 0;

  modifiers = [];
  modifierSubscription: Subscription;
  changeStoreSubscription: Subscription;
  search_string = ''
  isInitDataCalled:boolean = false;

  @ViewChild('addNewModalRef', { static: true }) addNewModalRef: AddNewModifierModalComponent;
  @ViewChild('storeModalRef', { static: true }) storeModalRef: SetStoreModalComponent;

  constructor(private hotkeysService: HotkeysService, private _helper: Helper,
    private _modifierService: ModifierService) {
    this.hotkeysService.add(new Hotkey('ctrl+a', (event: KeyboardEvent): boolean => {
      this.selected = [...this.data];
      return false;
    }));
    this.hotkeysService.add(new Hotkey('ctrl+d', (event: KeyboardEvent): boolean => {
      this.selected = [];
      return false;
    }));
  }

  ngOnInit(): void {
      this.changeStoreSubscription = this._helper._selected_store_id.subscribe((data) => {
        if(data){
          this._helper.selected_store_id = data;
          this.triggerInitData();
        }else{
          this.triggerInitData();
        }
      })
      this.modifierSubscription = this._modifierService._modifierObservable.subscribe((data) => {
        if (data) {
          this.triggerInitData();
        }
      })
  }

  triggerInitData() {
    if (!this.isInitDataCalled) {
      this.isInitDataCalled = true;
      this._initData();
      setTimeout(() => {
        this.isInitDataCalled = false;
      }, 1000);
    }
  }

  _initData() {
    if (this._helper.selected_store_id) {
      this._modifierService.list_group().then(data => {
        if (data.success) {
          this.modifiers = data.specification_group
        }else{
          this.modifiers = []
        }
      })
    } else if (!this._helper.isStoremodelOpen) {
      this.storeModalRef.show()
    }
  }

  changeDisplayMode(mode): void {
    this.displayMode = mode;
  }

  showAddNewModal(is_edit = false, edit_id = null): void {
    this.addNewModalRef.show(is_edit, edit_id);
  }

  isSelected(p: IProduct): boolean {
    return this.selected.findIndex(x => x.id === p.id) > -1;
  }
  onSelect(item: IProduct): void {
    if (this.isSelected(item)) {
      this.selected = this.selected.filter(x => x.id !== item.id);
    } else {
      this.selected.push(item);
    }
    this.setSelectAllState();
  }

  setSelectAllState(): void {
    if (this.selected.length === this.data.length) {
      this.selectAllState = 'checked';
    } else if (this.selected.length !== 0) {
      this.selectAllState = 'indeterminate';
    } else {
      this.selectAllState = '';
    }
  }

  selectAllChange($event): void {
    if ($event.target.checked) {
      this.selected = [...this.data];
    } else {
      this.selected = [];
    }
    this.setSelectAllState();
  }

  pageChanged(event: any): void {
    //no use in file in html use for
  }
  
  itemsPerPageChange(perPage: number): void {
    //no use in file in html use for
  }
  
  changeOrderBy(item: any): void {
    //no use in file in html use for
  }

  searchKeyUp(event): void {
    const val = event.target.value.toLowerCase().trim();
    this.search_string = val;
  }

  onContextMenuClick(action: string, item: IProduct): void {
  }

  ngOnDestroy() {
    if (this.modifierSubscription) {
      this.modifierSubscription.unsubscribe()
    }
    if (this.changeStoreSubscription) {
      this.changeStoreSubscription.unsubscribe()
    }
  }
}
