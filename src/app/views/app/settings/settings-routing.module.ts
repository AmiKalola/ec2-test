import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SettingsComponent } from './settings.component';
import { EmailSettingsComponent } from './email-settings/email-settings.component';
import { SmsSettingsComponent } from './sms-settings/sms-settings.component';
import { AdminSettingComponent } from '../above/admin-setting/admin-setting.component';
import { DocumentComponent } from '../above/document/document.component';
import { PromoCodeComponent } from '../delivery-info/promo-code/promo-code.component';
import { AdvertiseComponent } from '../delivery-info/advertise/advertise.component';
import { MassNotificationComponent } from '../delivery-info/mass-notification/mass-notification.component';
import { RefferalCodeComponent } from '../delivery-info/refferal-code/refferal-code.component';
import { LanguageComponent } from '../above/language/language.component';
import { LegalComponent } from './legal/legal.component';
import { CancellationReasonComponent } from './cancellation-reason/cancellation-reason.component';
import { LogsComponent } from './logs/logs.component';
import { BannerComponent } from '../above/banner/banner.component';

const routes: Routes = [
  {
    path: '', component: SettingsComponent,
    children: [
      { path: '', redirectTo: 'basic-settings/admin', pathMatch: 'full' },
      {
        path: 'basic-settings', children: [
          { path: '', redirectTo: 'admin', pathMatch: 'full' },
          { path: 'admin', component: AdminSettingComponent, data: {auth: '/admin/settings/admin'} },
          { path: 'document', component: DocumentComponent, data: { auth: '/admin/document' } },
          { path: 'language', component: LanguageComponent, data: { auth: '/admin/language' } },
          { path: 'banner', component: BannerComponent, data: { auth: '/admin/banner' } }
        ]
      },
      {
        path: 'discount', children: [
          { path: '', redirectTo: 'offer', pathMatch: 'full' },
          { path: 'offer', component: PromoCodeComponent, data: { auth: '/admin/offer' } },
          { path: 'advertise', component: AdvertiseComponent, data: { auth: '/admin/advertise' } },
          { path: 'referral', component: RefferalCodeComponent, data: { auth: '/admin/referral' } }
        ]
      },
      {
        path: 'template', children: [
          { path: '', redirectTo: 'email-settings', pathMatch: 'full' },
          { path: 'email-settings', component: EmailSettingsComponent, data: { auth: '/admin/email-settings' } },
          { path: 'sms-settings', component: SmsSettingsComponent, data: { auth: '/admin/sms-settings' } },
          { path: 'mass-notification', component: MassNotificationComponent, data: { auth: '/admin/mass-notification' } },
          { path: 'legal', component: LegalComponent, data: { auth: '/admin/legal' } },
          { path: 'cancellation-reason', component: CancellationReasonComponent, data: { auth: '/admin/cancellation-reason' } },
          { path: 'logs', component: LogsComponent, data: { auth: '/admin/logs' } },
        ]
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SettingsRoutingModule { }
