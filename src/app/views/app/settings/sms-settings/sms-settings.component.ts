import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { NgSelectConfig } from '@ng-select/ng-select';
import { TranslateService } from '@ngx-translate/core';
import { SmsConnectModalComponent } from 'src/app/containers/pages/sms-connect-modal/sms-connect-modal.component';
import { SmsService } from 'src/app/services/sms.service';
import { SMS_TAG_SETTING } from 'src/app/shared/constant';
import * as Quill from 'quill';
import { Helper } from 'src/app/shared/helper';

@Component({
  selector: 'app-sms-settings',
  templateUrl: './sms-settings.component.html',
  styleUrls: ['./sms-settings.component.scss']
})
export class SmsSettingsComponent implements OnInit, OnDestroy {
  sms_list = []
  selected_sms = {
    is_send: false,
    sms_content: "",
    sms_unique_title: "",
    unique_id: "",
    _id: null
  }
  sms_tags: any[] = [];

  editor: any;
  @ViewChild('Editor', { static: false })
  editorElementRef: ElementRef;
  cursor_position: number = -1;
  subadmin_readonly:boolean = false;

  @ViewChild('smsConnectRef', { static: true }) smsConnectRef: SmsConnectModalComponent;

  constructor(public _helper:Helper,private _smsService: SmsService, private config: NgSelectConfig, private _trans: TranslateService) {
    this.config.notFoundText = this._trans.instant('label-title.no-data-found');
  }

  ngAfterViewInit() {
    this.editor = new Quill.default(this.editorElementRef.nativeElement, {
      modules: {
        toolbar: [
          ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
          ['blockquote', 'code-block'],

          [{ 'header': 1 }, { 'header': 2 }],               // custom button values
          [{ 'list': 'ordered' }, { 'list': 'bullet' }],
          [{ 'script': 'sub' }, { 'script': 'super' }],      // superscript/subscript
          [{ 'indent': '-1' }, { 'indent': '+1' }],          // outdent/indent
          [{ 'direction': 'rtl' }],                         // text direction

          [{ 'size': ['small', false, 'large', 'huge'] }],  // custom dropdown
          [{ 'header': [1, 2, 3, 4, 5, 6, false] }],

          [{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme
          [{ 'font': [] }],
          [{ 'align': [] }],                                    // remove formatting button

          ['link', 'image', 'video']                         // link and image, video
        ]
      },
      theme: 'snow',
      sanitize: true
    });
  }

  ngOnInit(): void {
    this.getList()
    setTimeout(() => {
      if(!this._helper.has_permission(this._helper.PERMISSION.EDIT)){
        this.subadmin_readonly = true;
        this.editor.disable();
      }else{
        this.editor.enable();
      }
    }, 200);
  }

  showSmsConnectModal(): void {
    this.smsConnectRef.show();
  }

  getList() {
    this._smsService.getSmsList().then(res => {
      if (res.success) {
        this.sms_list = res.sms_details
      }
    })
  }

  onSelectSms(event) {
    if (event) {
      this.selected_sms = event
      let index = SMS_TAG_SETTING.findIndex((x) => x.SMS_UNIQUE_ID.indexOf(event.unique_id) !== -1);
      if (index !== -1) {
        this.sms_tags = SMS_TAG_SETTING[index].ALLOWED_PARAMS;
      } else {
        this.sms_tags = [];
      }
    } else {
      this.selected_sms = {
        is_send: false,
        sms_content: "",
        sms_unique_title: "",
        unique_id: "",
        _id: null
      }
    }
    this.editor.pasteHTML(this.selected_sms.sms_content)
  }


  mouseLeave() {
    const selection = this.editor.getSelection();
    if (selection) {
      this.cursor_position = selection.index;
    }
  }

  select_tag(sms_tag) {
    this.editor.pasteHTML(this.cursor_position, ' <b> [' + sms_tag + '] </b> ');
  }

  save() {
    this.selected_sms.sms_content = this.editor.root.innerHTML;
    if (this.selected_sms._id) {
      let json = {
        is_send: this.selected_sms.is_send,
        sms: this.selected_sms.sms_unique_title,
        sms_content: this.selected_sms.sms_content,
        sms_id: this.selected_sms._id
      }
      this._smsService.updateSms(json)
    }
  }

  ngOnDestroy(): void {
    if (this.smsConnectRef.modalRef) {
      this.smsConnectRef.onClose();
    }
  }


}
