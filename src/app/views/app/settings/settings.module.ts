import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SettingsRoutingModule } from './settings-routing.module';
import { SettingsComponent } from './settings.component';
import { EmailSettingsComponent } from './email-settings/email-settings.component';
import { SmsSettingsComponent } from './sms-settings/sms-settings.component';
// add for accordian and basic bs class
import { LayoutContainersModule } from 'src/app/containers/layout/layout.containers.module';
import { PagesContainersModule } from 'src/app/containers/pages/pages.containers.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { FormsModule as FormsModuleAngular, ReactiveFormsModule } from '@angular/forms';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ModalModule } from 'ngx-bootstrap/modal';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { RatingModule } from 'ngx-bootstrap/rating';
import { NgSelectModule } from '@ng-select/ng-select';
import { QuillModule } from 'ngx-quill';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { DirectivesModule} from 'src/app/directives/directives.module';
import { LegalComponent } from './legal/legal.component';
import { CancellationReasonComponent } from './cancellation-reason/cancellation-reason.component';
import { LogsComponent } from './logs/logs.component';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { TooltipModule } from 'ngx-bootstrap/tooltip';

@NgModule({
  declarations: [SettingsComponent, EmailSettingsComponent, CancellationReasonComponent, SmsSettingsComponent, LegalComponent, LogsComponent],
  imports: [
    CommonModule,
    SettingsRoutingModule,
    SharedModule,
    DirectivesModule,
    LayoutContainersModule,
    CommonModule,
    CollapseModule,
    PagesContainersModule,
    FormsModuleAngular,
    RatingModule.forRoot(),
    ReactiveFormsModule,
    PaginationModule.forRoot(),
    TabsModule.forRoot(),
    ModalModule.forRoot(),
    BsDropdownModule.forRoot(),
    AccordionModule.forRoot(),
    NgSelectModule,
    TooltipModule,
    QuillModule.forRoot(),
    BsDatepickerModule,
  ]
})
export class SettingsModule { }
