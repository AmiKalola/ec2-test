import { Component, OnInit } from '@angular/core';
import { LegalSettingService } from 'src/app/services/legal-setting.service';
import { Helper } from 'src/app/shared/helper';

@Component({
  selector: 'app-legal',
  templateUrl: './legal.component.html',
})
export class LegalComponent implements OnInit {
  legal_info: any;
  subadmin_readonly:boolean = false;

  constructor(public _helper:Helper,private _legalSettingService: LegalSettingService) { }

  ngOnInit(): void {
    this.getData()
    if(!this._helper.has_permission(this._helper.PERMISSION.EDIT)){
      this.subadmin_readonly = true;
    }
  }

  save() {
    this._legalSettingService.updateLegalSetting(this.legal_info)
  }

  getData() {
    this._legalSettingService.fetchLegalSetting().then((res) => {
      if (res.success) {
        this.legal_info = res.legal;
      }
    })
  }
}
