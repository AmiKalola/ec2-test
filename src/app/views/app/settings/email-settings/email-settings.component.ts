import { Component, OnDestroy, OnInit, ViewChild, ElementRef } from '@angular/core';
import { NgSelectConfig } from '@ng-select/ng-select';
import { TranslateService } from '@ngx-translate/core';
import { MailConnectModalComponent } from 'src/app/containers/pages/mail-connect-modal/mail-connect-modal.component';
import { EmailService } from 'src/app/services/email.service';
import { EMAIL_TAG_SETTING } from 'src/app/shared/constant';
import * as Quill from 'quill';
import { Helper } from 'src/app/shared/helper';

@Component({
  selector: 'app-email-settings',
  templateUrl: './email-settings.component.html',
  styleUrls: ['./email-settings.component.scss']
})
export class EmailSettingsComponent implements OnInit, OnDestroy {
  email_list = []
  selected_email = {
    is_send: false,
    email_admin_info: "",
    email_content: "",
    email_title: "",
    email_unique_title: "",
    template_unique_id: "",
    unique_id: "",
    _id: null
  }
  email_tags:any[]= [];
  filtered_email_list = [];

  editor: any;
  @ViewChild('Editor', {static: false})
  editorElementRef: ElementRef;
  cursor_position:number=-1;
  subadmin_readonly:boolean = false;

  @ViewChild('mailConnectRef', { static: true }) mailConnectRef: MailConnectModalComponent;

  constructor(public _helper:Helper,private _emailService: EmailService,private config: NgSelectConfig, private _trans: TranslateService) {
    this.config.notFoundText = this._trans.instant('label-title.no-data-found');
  }

  ngAfterViewInit() {
    this.editor = new Quill.default(this.editorElementRef.nativeElement, {
      modules: {
        toolbar: [
          ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
          ['blockquote', 'code-block'],

          [{ 'header': 1 }, { 'header': 2 }],               // custom button values
          [{ 'list': 'ordered'}, { 'list': 'bullet' }],
          [{ 'script': 'sub'}, { 'script': 'super' }],      // superscript/subscript
          [{ 'indent': '-1'}, { 'indent': '+1' }],          // outdent/indent
          [{ 'direction': 'rtl' }],                         // text direction

          [{ 'size': ['small', false, 'large', 'huge'] }],  // custom dropdown
          [{ 'header': [1, 2, 3, 4, 5, 6, false] }],

          [{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme
          [{ 'font': [] }],
          [{ 'align': [] }],                                    // remove formatting button

          ['link', 'image', 'video']                         // link and image, video
        ]
      },
      theme: 'snow',
      sanitize: true
    });

  }

  ngOnInit(): void {
    this.getList();
    setTimeout(() => {
      if(!this._helper.has_permission(this._helper.PERMISSION.EDIT)){
        this.subadmin_readonly = true;
        this.editor.disable();
      }else{
        this.editor.enable();
      }
    }, 300);
  }

  getList() {
    this._emailService.getEmailList().then(res => {
      if (res.success) {
        this.email_list = res.email_details
        this.showEmailBy('general');
      }
    })
  }

  onSelectMail(event) {
    if(event){
      this.selected_email = event;
      let index = EMAIL_TAG_SETTING.findIndex((x)=>x.EMAIL_UNIQUE_ID.indexOf(event.unique_id)!==-1);
      if(index!==-1){
        this.email_tags = EMAIL_TAG_SETTING[index].ALLOWED_PARAMS;
      } else {
        this.email_tags = [];
      }
    }else{
      this.selected_email = {
        is_send: false,
        email_admin_info: "",
        email_content: "",
        email_title: "",
        email_unique_title: "",
        template_unique_id: "",
        unique_id: "",
        _id: null
      }
      this.email_tags = [];
    }
    this.editor.pasteHTML(this.selected_email.email_content)
  }


  mouseLeave(){
    const selection = this.editor.getSelection();
    if(selection){
      this.cursor_position = selection.index;
    }
  }

  select_tag(email_tag){
    this.editor.pasteHTML(this.cursor_position, ' <b> [' + email_tag + '] </b> ');
  }

  save() {
    this.selected_email.email_content = this.editor.root.innerHTML;
    if(this.selected_email._id){
      let json = {
        email:this.selected_email.email_unique_title,
        email_admin_info:this.selected_email.email_admin_info,
        email_content:this.selected_email.email_content,
        email_id:this.selected_email._id,
        email_title:this.selected_email.email_title,
        is_send:this.selected_email.is_send
      }
      this._emailService.updateEmail(json)
    }
  }

  showMailConnectModal(): void {
    this.mailConnectRef.show();
  }

  ngOnDestroy(): void {
    if(this.mailConnectRef.modalRef){
      this.mailConnectRef.onClose()
    }
  }

  showEmailBy(category){
    if(category === 'general'){
      this.filtered_email_list = this.email_list.filter(email => email.template_unique_id == 1 || email.template_unique_id == 2)
    }
    if(category === 'delivery'){
      this.filtered_email_list = this.email_list.filter(email => email.template_unique_id == 5 || email.template_unique_id == 4)
    }
    if(category === 'taxi'){
      this.filtered_email_list = this.email_list.filter(email => email.template_unique_id == 9)
    }
  }

}
