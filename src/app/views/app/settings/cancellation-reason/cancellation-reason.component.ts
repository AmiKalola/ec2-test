import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { AddNewCancellationReasonComponent } from 'src/app/containers/pages/add-new-cancellation-reason-modal/add-new-cancellation-reason-modal.component';
import { CancellationReasonService } from '../../../../services/cancellation-reason-service';
import { Subscription } from 'rxjs';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { Helper } from 'src/app/shared/helper';
import { LangService } from 'src/app/shared/lang.service';

@Component({
  selector: 'app-cancellation-reason',
  templateUrl: './cancellation-reason.component.html',
  styleUrls: ['./cancellation-reason.component.scss']
})
export class CancellationReasonComponent implements OnInit, OnDestroy {
  approved_advertise = []
  filter_approved_advertise = []
  cancellation_reasons = []
  filter_unapproved_advertise = []
  advertise_subscriper:Subscription

  user_type: any = this._helper.ADMIN_DATA_ID.USER;
  displayOptionsCollapsed = false;

  deliveries_type_list: any = [];
  delivery_type_filter = { value: 0, title: 'label-title.all' };
  delivery_type_all = { value: 0, title: 'label-title.all' };
  delivery_type: number= 0;

  @ViewChild('addNewModalRef', { static: true }) addNewModalRef: AddNewCancellationReasonComponent;
  constructor(private _cancellationReasonService:CancellationReasonService,
    public _helper: Helper,
    public _lang: LangService) { }

  ngOnInit(): void {
    this.advertise_subscriper = this._cancellationReasonService._addvertiseObservable.subscribe(()=>{
      // setTimeout(() => {
        this.getCancellationReasons()
      // }, 2000)
    });
    this.deliveries_type_list = JSON.parse(JSON.stringify(this._helper.DELIVERY_TYPE));
  }

  addAdvertiseModal(): void {
    this.addNewModalRef.show(null, this.user_type);
  }

  editAdvertiseModal(reason_id) {
    this.addNewModalRef.show(reason_id, this.user_type)
  }

  getCancellationReasons(){
    this._cancellationReasonService.list({user_type:this.user_type, delivery_type: this.delivery_type , is_show_error_toast: false, is_show_success_toast: false}).then(res=>{
      if(res.success){
        this.cancellation_reasons = res.cancellation_reasons
      } else {
        this.cancellation_reasons  = []
      }
    })
  }

  ngOnDestroy(): void {
    this.advertise_subscriper.unsubscribe()
    if(this.addNewModalRef.modalRef) {
      this.addNewModalRef.close();
    }
  }

  onDelete(reason_id) {
    let title = this._helper._trans.instant('label-title.are-you-sure-want-to-remove')
    let text = this._helper._trans.instant('label-title.you-will-not-be-able-to-recover')
    let confirmButtonText = this._helper._trans.instant('label-title.yes-delete-it')
    let cancelButtonText = this._helper._trans.instant('label-title.no-keep-it')
    let advertiseDelete = this._helper._trans.instant('label-title.your-advertise-deleted')
    let deleted = this._helper._trans.instant('label-title.deleted')
    let advertiseSafe = this._helper._trans.instant('label-title.your-advertise-safe')
    let cancelled = this._helper._trans.instant('heading-title.cancelled')
    Swal.fire({
      title: title,
      text: text,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: confirmButtonText,
      cancelButtonText: cancelButtonText,
    }).then((result) => {
      if (result.value) {
        Swal.fire(
          deleted,
          advertiseDelete,
          'success'
        )
        this._cancellationReasonService.deleteCancellationReason(reason_id)
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        Swal.fire(
          cancelled,
          advertiseSafe,
          'error'
        )
      }
    })
  }

  changeUserType(user_type){
    this.user_type = user_type
    this.getCancellationReasons();
  }

  changeDeliveryTypeBy(delivery_type){
    if(delivery_type.value === 0){
      this.delivery_type_filter = { value: 0, title: 'label-title.all' };
    }
    if(delivery_type.value !== 0){
      this.delivery_type_filter = delivery_type;
    }
    this.delivery_type = delivery_type.value;
  }

}
