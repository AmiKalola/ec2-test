import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule as FormsModuleAngular, ReactiveFormsModule } from '@angular/forms';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ModalModule } from 'ngx-bootstrap/modal';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { RatingModule } from 'ngx-bootstrap/rating';
import { NgSelectModule } from '@ng-select/ng-select';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { LayoutContainersModule } from 'src/app/containers/layout/layout.containers.module';
import { PagesContainersModule } from 'src/app/containers/pages/pages.containers.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { MapViewsRoutingModule } from './map-views-routing.module';
import { MapViewsComponent } from './map-views.component';
import { StoreLocationComponent } from './store-location/store-location.component';
import { DeliverymanLocationComponent } from './deliveryman-location/deliveryman-location.component';
import { DeliverymanTrackComponent } from './deliveryman-track/deliveryman-track.component';
import { AllCitiesComponent } from './all-cities/all-cities.component'

@NgModule({
  declarations: [MapViewsComponent, StoreLocationComponent, DeliverymanLocationComponent, DeliverymanTrackComponent, AllCitiesComponent],
  imports: [
    CommonModule,
    CollapseModule,
    SharedModule,
    LayoutContainersModule,
    CommonModule,
    PagesContainersModule,
    FormsModuleAngular,
    NgSelectModule,
    RatingModule.forRoot(),
    ReactiveFormsModule,
    PaginationModule.forRoot(),
    TabsModule.forRoot(),
    BsDatepickerModule,
    ModalModule.forRoot(),
    BsDropdownModule.forRoot(),
    AccordionModule.forRoot(),
    MapViewsRoutingModule
  ]
})
export class MapViewsModule { }
