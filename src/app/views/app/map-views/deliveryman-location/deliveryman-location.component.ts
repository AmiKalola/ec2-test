import { Component, OnInit } from '@angular/core';
import { CountryService } from 'src/app/services/country.service';
import { ProviderService } from 'src/app/services/delivery-boy.service';
import { AdminSettingService } from 'src/app/services/admin-setting.service';
import { Helper } from 'src/app/shared/helper';
import { DeliveryFeesService } from 'src/app/services/delivery-fees.service';
declare let google;

@Component({
  selector: 'app-deliveryman-location',
  templateUrl: './deliveryman-location.component.html',
  styleUrls: ['./deliveryman-location.component.scss']
})
export class DeliverymanLocationComponent implements OnInit {

  map: any
  provider_list = []
  filtered_provider_list = []
  country_list = []
  city_list = []

  selected_country: any;
  selected_city: any = 'All';
  provider_markers: any[] = [];
  is_online_selected = true
  is_offline_selected = true
  is_online_with_order_selected = true
  is_offline_with_order_selected = true
  settingData: any;
  address: string;
  cityLatLong: number[] = [0, 0];

  constructor(private _countryService: CountryService, private _providerService: ProviderService, private _adminSettingService: AdminSettingService, private _helper: Helper
    , private _deliveryFeesService: DeliveryFeesService) { }

  ngOnInit(): void {
    let notification = false;
    this._adminSettingService.fetchAdminSetting(notification).then(res => {
      if (res.success) {
        this.settingData = res.setting;
        this.cityLatLong = [this.settingData.latitude, this.settingData.longitude]
      }
      this._initMap()
      this.getCountryList()
      this.getProviderList()
    })
  }

  _initMap() {
    let theme = localStorage.getItem('vien-themecolor');
    if (theme.includes('dark')) {
      this.map = new google.maps.Map(document.getElementById('deliveryman_location_map'), {
        zoom: 2,
        streetViewControl: false,
        center: { lat: Number(this.cityLatLong[0]), lng: Number(this.cityLatLong[1]) },
        styles: [
          { elementType: "geometry", stylers: [{ color: "#242f3e" }] },
          { elementType: "labels.text.stroke", stylers: [{ color: "#242f3e" }] },
          { elementType: "labels.text.fill", stylers: [{ color: "#746855" }] },
          {
            featureType: "administrative.locality",
            elementType: "labels.text.fill",
            stylers: [{ color: "#d59563" }],
          },
          {
            featureType: "poi",
            elementType: "labels.text.fill",
            stylers: [{ color: "#d59563" }],
          },
          {
            featureType: "poi.park",
            elementType: "geometry",
            stylers: [{ color: "#263c3f" }],
          },
          {
            featureType: "poi.park",
            elementType: "labels.text.fill",
            stylers: [{ color: "#6b9a76" }],
          },
          {
            featureType: "road",
            elementType: "geometry",
            stylers: [{ color: "#38414e" }],
          },
          {
            featureType: "road",
            elementType: "geometry.stroke",
            stylers: [{ color: "#212a37" }],
          },
          {
            featureType: "road",
            elementType: "labels.text.fill",
            stylers: [{ color: "#9ca5b3" }],
          },
          {
            featureType: "road.highway",
            elementType: "geometry",
            stylers: [{ color: "#746855" }],
          },
          {
            featureType: "road.highway",
            elementType: "geometry.stroke",
            stylers: [{ color: "#1f2835" }],
          },
          {
            featureType: "road.highway",
            elementType: "labels.text.fill",
            stylers: [{ color: "#f3d19c" }],
          },
          {
            featureType: "transit",
            elementType: "geometry",
            stylers: [{ color: "#2f3948" }],
          },
          {
            featureType: "transit.station",
            elementType: "labels.text.fill",
            stylers: [{ color: "#d59563" }],
          },
          {
            featureType: "water",
            elementType: "geometry",
            stylers: [{ color: "#17263c" }],
          },
          {
            featureType: "water",
            elementType: "labels.text.fill",
            stylers: [{ color: "#515c6d" }],
          },
          {
            featureType: "water",
            elementType: "labels.text.stroke",
            stylers: [{ color: "#17263c" }],
          },
        ],
      });
    } else {
      this.map = new google.maps.Map(document.getElementById('deliveryman_location_map'), {
        zoom: 2,
        streetViewControl: false,
        center: { lat: Number(this.cityLatLong[0]), lng: Number(this.cityLatLong[1]) }
      });
    }

    if (this.cityLatLong[0] != 0) {
      this.map.setZoom(12)
    } else {
      this.map.setZoom(2)
    }
  }

  _initAutocomplete() {

    let country = this.country_list.filter((country) => country._id == this.selected_country);
    let country_code = null;
    if (country.length) {
      country_code = country[0].country_code_2;
    }

    let autocompleteElm = <HTMLInputElement>document.getElementById('deliverman_address');
    google.maps.event.clearInstanceListeners(autocompleteElm);
    let autocomplete;
    if (country_code) {
      autocomplete = new google.maps.places.Autocomplete((autocompleteElm), { types: ['(cities)'], componentRestrictions: { country: country_code } });
    } else {
      autocomplete = new google.maps.places.Autocomplete((autocompleteElm), { types: ['(cities)'] });
    }
    autocomplete.addListener('place_changed', () => {
      let place = autocomplete.getPlace();
      if (place.geometry && place.geometry.location.lat() && place.geometry.location.lng()) {

        let lat = place.geometry.location.lat();
        let lng = place.geometry.location.lng();
        this.address = place['formatted_address'];
        this.cityLatLong = [lat, lng];
        this._initMap();
        this.filter();
      }
    });
    $('.search-address').find("#deliverman_address").on("focus click keypress", () => {
      $('.search-address').css({ position: "relative" }).append($(".pac-container"));
    });
  }

  getCountryList() {
    this._countryService.fetch().then(res => {
      if (res.success) {
        this.country_list = res.countries
        if (this.country_list.length > 0) {
          this.selected_country = this.country_list[0]._id;
          if (this.selected_country) {
            this.countryChange(this.selected_country)
          }
        } else {
          this.selected_city = null;
        }
      }
    })
  }

  countryChange(id) {
    this._deliveryFeesService.get_city_lists(this.selected_country).then(res => {
      this.city_list = []
      if (res.success) {
        this.city_list = res.cities
      }
      if (this.city_list.length >= 2) {
        this.selected_city = 'All';
      } else if (this.city_list.length > 0) {
        this.selected_city = this.city_list[0]._id;
        this.onCitySelected(this.selected_city);
      } else {
        this.selected_city = null;
      }
    })
    this.selectedCountryMap(id);
  }

  selectedCountryMap(id) {
    let i = this.country_list.findIndex((x: any) => x._id == id)
    if (this.country_list[i]?.country_lat_long[0]) {
      this.selected_country = id;
      let theme = localStorage.getItem('vien-themecolor');
      if (theme.includes('dark')) {
        this.map = new google.maps.Map(document.getElementById('deliveryman_location_map'), {
          zoom: 4,
          streetViewControl: false,
          center: { lat: Number(this.country_list[i].country_lat_long[0]), lng: Number(this.country_list[i].country_lat_long[1]) },
          styles: [
            { elementType: "geometry", stylers: [{ color: "#242f3e" }] },
            { elementType: "labels.text.stroke", stylers: [{ color: "#242f3e" }] },
            { elementType: "labels.text.fill", stylers: [{ color: "#746855" }] },
            {
              featureType: "administrative.locality",
              elementType: "labels.text.fill",
              stylers: [{ color: "#d59563" }],
            },
            {
              featureType: "poi",
              elementType: "labels.text.fill",
              stylers: [{ color: "#d59563" }],
            },
            {
              featureType: "poi.park",
              elementType: "geometry",
              stylers: [{ color: "#263c3f" }],
            },
            {
              featureType: "poi.park",
              elementType: "labels.text.fill",
              stylers: [{ color: "#6b9a76" }],
            },
            {
              featureType: "road",
              elementType: "geometry",
              stylers: [{ color: "#38414e" }],
            },
            {
              featureType: "road",
              elementType: "geometry.stroke",
              stylers: [{ color: "#212a37" }],
            },
            {
              featureType: "road",
              elementType: "labels.text.fill",
              stylers: [{ color: "#9ca5b3" }],
            },
            {
              featureType: "road.highway",
              elementType: "geometry",
              stylers: [{ color: "#746855" }],
            },
            {
              featureType: "road.highway",
              elementType: "geometry.stroke",
              stylers: [{ color: "#1f2835" }],
            },
            {
              featureType: "road.highway",
              elementType: "labels.text.fill",
              stylers: [{ color: "#f3d19c" }],
            },
            {
              featureType: "transit",
              elementType: "geometry",
              stylers: [{ color: "#2f3948" }],
            },
            {
              featureType: "transit.station",
              elementType: "labels.text.fill",
              stylers: [{ color: "#d59563" }],
            },
            {
              featureType: "water",
              elementType: "geometry",
              stylers: [{ color: "#17263c" }],
            },
            {
              featureType: "water",
              elementType: "labels.text.fill",
              stylers: [{ color: "#515c6d" }],
            },
            {
              featureType: "water",
              elementType: "labels.text.stroke",
              stylers: [{ color: "#17263c" }],
            },
          ],
        });
      }
    } else {
      this.map = new google.maps.Map(document.getElementById('deliveryman_location_map'), {
        zoom: 4,
        streetViewControl: false,
        center: { lat: Number(this.country_list[i].country_lat_long[0]), lng: Number(this.country_list[i].country_lat_long[1]) }
      });
    }
    this.filter();
  }

  onCitySelected(id) {
    if (id != 'All') {
      let index = this.city_list.findIndex(x => x._id === this.selected_city)
      this.cityLatLong = [this.city_list[index].city_lat_long[0], this.city_list[index].city_lat_long[1]];
      this._initMap();
      this.filter();
    } else {
      this.selectedCountryMap(this.selected_country);
    }
  }

  getProviderList() {
    this._providerService.getProviderListForMap().then(res => {
      if (res.success) {
        this.provider_list = res.providers
        this.filter()
      }
    })
  }

  filter() {
    this.filtered_provider_list = JSON.parse(JSON.stringify(this.provider_list));

    this.filtered_provider_list = this.filtered_provider_list.filter((provider_data) => {
      return (this.selected_country == 'All' || this.selected_country == provider_data.country_id);
    });

    this.filtered_provider_list = this.filtered_provider_list.filter((provider_data) => {
      return (this.selected_city == 'All' || this.selected_city == provider_data?.city_id);
    });

    let provider_list = []
    this.filtered_provider_list.forEach(provider => {
      let is_push = false
      if (this.is_online_selected) {
        if (provider.is_online && Number(provider.has_order) != 1) {
          is_push = true
        }
      }
      if (this.is_offline_selected) {
        if (!provider.is_online && Number(provider.has_order) != 1) {
          is_push = true
        }
      }
      if (this.is_online_with_order_selected) {
        if (provider.is_online && Number(provider.has_order) == 1) {
          is_push = true
        }
      }
      if (this.is_offline_with_order_selected) {
        if (!provider.is_online && Number(provider.has_order) == 1) {
          is_push = true
        }
      }
      if (is_push) {
        provider_list.push(provider)
      }
    });
    this.createMarker(provider_list)
  }

  createMarker(provider_list) {
    this.provider_markers.forEach(function (marker) {
      marker.setMap(null);
    });
    this.provider_markers = [];
    let to_fixed_number = this._helper.to_fixed_number;
    for (const element of provider_list) {
      let data = element;
      let icon_url = 'assets/img/map_pin_images/Deliveryman/';
      let icon_name = ''
      if (data.is_online) {
        icon_name = 'deliveryman_online.png';
        if (Number(data.has_order) == 1) {
          icon_name = 'deliveryman_online_with_orders.png';
        }
      } else {
        icon_name = 'deliveryman_offline.png';
        if (Number(data.has_order) == 1) {
          icon_name = 'deliveryman_offline_with_orders.png';
        }
      }

      icon_url = icon_url + icon_name;

      if (data.location != undefined) {
        let marker = new google.maps.Marker({
          position: { lat: Number(data.location[0]), lng: Number(data.location[1]) },
          map: this.map,
          icon: icon_url
        });
        let infoWindow = new google.maps.InfoWindow();

        this.provider_markers.push(marker);
        let name = this._helper._trans.instant('heading-title.name');
        let email = this._helper._trans.instant('heading-title.email');
        let rating = this._helper._trans.instant('label-title.rating');
        //Attach click event to the marker.
        (function (marker, data) {
          google.maps.event.addListener(marker, "click", function (e) {

            infoWindow.setContent('<p><b>' + name + '</b>: ' + data.first_name + ' ' + data.last_name +
              '<br><b>' + email + '</b>: ' + data.email +
              '<br><b>' + rating + '</b>: ' + data.user_rate.toFixed(to_fixed_number) +
              '</p>');
            infoWindow.open(this.map, marker);
          });
        })(marker, data);

      }
    };
  }
}
