import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DeliverymanLocationComponent } from './deliveryman-location.component';

describe('DeliverymanLocationComponent', () => {
  let component: DeliverymanLocationComponent;
  let fixture: ComponentFixture<DeliverymanLocationComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ DeliverymanLocationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeliverymanLocationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
