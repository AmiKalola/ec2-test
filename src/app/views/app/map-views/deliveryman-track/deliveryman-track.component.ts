import { Component, OnInit } from '@angular/core';
import { AdminSettingService } from 'src/app/services/admin-setting.service';
import { CountryService } from 'src/app/services/country.service';
import { DeliveryFeesService } from 'src/app/services/delivery-fees.service';
import { NotifiyService } from 'src/app/services/notifier.service';
import { OrderTrackService } from 'src/app/services/order-track.service';
import { Helper } from 'src/app/shared/helper';
declare let google;

@Component({
  selector: 'app-deliveryman-track',
  templateUrl: './deliveryman-track.component.html',
})
export class DeliverymanTrackComponent implements OnInit {

  map: any
  country_list = []
  city_list = []
  provider_list = []
  order_list = []
  selected_country: any
  selected_city: any
  selected_provider_id: any
  selected_provider: any
  selected_order: any
  selected_order_id: any
  type = 0

  provider_markers_order = new google.maps.Marker({});
  old_lat_lng: any[];
  new_lat_lng: any[];
  latitude: number;
  longitude: number;
  zoom: number;
  numDeltas: number;
  delay: number; //milliseconds
  i: number;
  id: any;
  interval: any;
  is_changed: boolean = false;
  is_provider_first_time_selected: boolean = true;
  is_order_first_time_selected: boolean = true;
  settingData: any;
  cityLatLong: number[] = [0, 0];

  is_destroyed: boolean = false;

  constructor(private _countryService: CountryService,
    private _deliveryFeesService: DeliveryFeesService,
    private _orderTrackService: OrderTrackService,
    private _helper: Helper,
    public _notifierService: NotifiyService,
    private _adminSettingService: AdminSettingService
  ) { }

  ngOnInit(): void {
    this.zoom = 4;
    this.numDeltas = 100;
    this.delay = 100;
    const notification = false;
    this._adminSettingService.fetchAdminSetting(notification).then(res => {
      if (res.success && res.setting) {
        this.settingData = res.setting;
        this.cityLatLong = [this.settingData.latitude, this.settingData.longitude]
      }
      this._initMap()
    })
    this.getCountryList()
  }

  _initMap(lat = this.cityLatLong[0], lng = this.cityLatLong[1]) {
    let theme = localStorage.getItem('vien-themecolor');
    if (theme.includes('dark')) {
      this.map = new google.maps.Map(document.getElementById('deliveryman_track_map'), {
        zoom: 12,
        streetViewControl: false,
        center: { lat, lng },
        styles: [
          { elementType: "geometry", stylers: [{ color: "#242f3e" }] },
          { elementType: "labels.text.stroke", stylers: [{ color: "#242f3e" }] },
          { elementType: "labels.text.fill", stylers: [{ color: "#746855" }] },
          {
            featureType: "administrative.locality",
            elementType: "labels.text.fill",
            stylers: [{ color: "#d59563" }],
          },
          {
            featureType: "poi",
            elementType: "labels.text.fill",
            stylers: [{ color: "#d59563" }],
          },
          {
            featureType: "poi.park",
            elementType: "geometry",
            stylers: [{ color: "#263c3f" }],
          },
          {
            featureType: "poi.park",
            elementType: "labels.text.fill",
            stylers: [{ color: "#6b9a76" }],
          },
          {
            featureType: "road",
            elementType: "geometry",
            stylers: [{ color: "#38414e" }],
          },
          {
            featureType: "road",
            elementType: "geometry.stroke",
            stylers: [{ color: "#212a37" }],
          },
          {
            featureType: "road",
            elementType: "labels.text.fill",
            stylers: [{ color: "#9ca5b3" }],
          },
          {
            featureType: "road.highway",
            elementType: "geometry",
            stylers: [{ color: "#746855" }],
          },
          {
            featureType: "road.highway",
            elementType: "geometry.stroke",
            stylers: [{ color: "#1f2835" }],
          },
          {
            featureType: "road.highway",
            elementType: "labels.text.fill",
            stylers: [{ color: "#f3d19c" }],
          },
          {
            featureType: "transit",
            elementType: "geometry",
            stylers: [{ color: "#2f3948" }],
          },
          {
            featureType: "transit.station",
            elementType: "labels.text.fill",
            stylers: [{ color: "#d59563" }],
          },
          {
            featureType: "water",
            elementType: "geometry",
            stylers: [{ color: "#17263c" }],
          },
          {
            featureType: "water",
            elementType: "labels.text.fill",
            stylers: [{ color: "#515c6d" }],
          },
          {
            featureType: "water",
            elementType: "labels.text.stroke",
            stylers: [{ color: "#17263c" }],
          },
        ],
      });
    } else {
      this.map = new google.maps.Map(document.getElementById('deliveryman_track_map'), {
        zoom: 12,
        streetViewControl: false,
        center: { lat, lng }
      });
    }
  }

  getCountryList() {
    this._countryService.fetch().then(res => {
      if (res.success) {
        this.country_list = res.countries
      }
    })
  }

  getCityList() {
    this._deliveryFeesService.get_city_lists(this.selected_country).then(res => {
      this.city_list = []
      this.provider_list = []
      this.order_list = []
      this.selected_city = null;
      this.selected_provider_id = null;
      this.selected_order_id = ''
      if (res.success) {
        this.city_list = res.cities
      }
    })
    if (this.selected_country && this.country_list.length > 0) {
      let i = this.country_list.findIndex((x: any) => x._id == this.selected_country)
      let theme = localStorage.getItem('vien-themecolor');
      if (theme.includes('dark')) {
        if (this.country_list[i]?.country_lat_long[0]) {
          this.map = new google.maps.Map(document.getElementById('deliveryman_track_map'), {
            zoom: 4,
            streetViewControl: false,
            center: { lat: this.country_list[i].country_lat_long[0], lng: this.country_list[i].country_lat_long[1] },
            styles: [
              { elementType: "geometry", stylers: [{ color: "#242f3e" }] },
              { elementType: "labels.text.stroke", stylers: [{ color: "#242f3e" }] },
              { elementType: "labels.text.fill", stylers: [{ color: "#746855" }] },
              {
                featureType: "administrative.locality",
                elementType: "labels.text.fill",
                stylers: [{ color: "#d59563" }],
              },
              {
                featureType: "poi",
                elementType: "labels.text.fill",
                stylers: [{ color: "#d59563" }],
              },
              {
                featureType: "poi.park",
                elementType: "geometry",
                stylers: [{ color: "#263c3f" }],
              },
              {
                featureType: "poi.park",
                elementType: "labels.text.fill",
                stylers: [{ color: "#6b9a76" }],
              },
              {
                featureType: "road",
                elementType: "geometry",
                stylers: [{ color: "#38414e" }],
              },
              {
                featureType: "road",
                elementType: "geometry.stroke",
                stylers: [{ color: "#212a37" }],
              },
              {
                featureType: "road",
                elementType: "labels.text.fill",
                stylers: [{ color: "#9ca5b3" }],
              },
              {
                featureType: "road.highway",
                elementType: "geometry",
                stylers: [{ color: "#746855" }],
              },
              {
                featureType: "road.highway",
                elementType: "geometry.stroke",
                stylers: [{ color: "#1f2835" }],
              },
              {
                featureType: "road.highway",
                elementType: "labels.text.fill",
                stylers: [{ color: "#f3d19c" }],
              },
              {
                featureType: "transit",
                elementType: "geometry",
                stylers: [{ color: "#2f3948" }],
              },
              {
                featureType: "transit.station",
                elementType: "labels.text.fill",
                stylers: [{ color: "#d59563" }],
              },
              {
                featureType: "water",
                elementType: "geometry",
                stylers: [{ color: "#17263c" }],
              },
              {
                featureType: "water",
                elementType: "labels.text.fill",
                stylers: [{ color: "#515c6d" }],
              },
              {
                featureType: "water",
                elementType: "labels.text.stroke",
                stylers: [{ color: "#17263c" }],
              },
            ],
          });
        }
      } else {
        this.map = new google.maps.Map(document.getElementById('deliveryman_track_map'), {
          zoom: 4,
          streetViewControl: false,
          center: { lat: this.country_list[i].country_lat_long[0], lng: this.country_list[i].country_lat_long[1] }
        });
      }
    }
  }

  onCitySelected() {
    let index = this.city_list.findIndex(x => x._id === this.selected_city)
    this._initMap(this.city_list[index].city_lat_long[0], this.city_list[index].city_lat_long[1])
    this.selected_provider_id = null;
    this.getProviderList()
    this.getOrderList()
  }

  onProviderSelected() {
    if (!this.is_provider_first_time_selected) {
      this.is_changed = true
    } else {
      this.is_provider_first_time_selected = false
    }

    this._orderTrackService.deliverymanTrack(this.selected_provider_id, this.type).then((res) => {
      if (res.success) {
        this.id = this.selected_provider_id;
        this.selected_provider = res.provider;
        if (res.provider.location) {
          this.latitude = res.provider.location[0];
          this.longitude = res.provider.location[1];
        }
        this.zoom = 12;
        this.old_lat_lng = [this.latitude, this.longitude];

        this.markProviderPin(this.map);
        this.providerLocationTrack();
      }
    })
  }

  onOrderSelected() {
    if (!this.is_order_first_time_selected) {
      this.is_changed = true
    } else {
      this.is_order_first_time_selected = false
    }

    this._orderTrackService.deliverymanTrack(this.selected_order_id, this.type).then((res) => {
      if (res.success) {
        this.id = this.selected_order_id;
        this.selected_provider = res.provider;
        if (res.provider.location) {
          this.latitude = res.provider.location[0];
          this.longitude = res.provider.location[1];
        }
        this.zoom = 12;
        this.old_lat_lng = [this.latitude, this.longitude];
        this.markProviderPin(this.map);
        this.providerLocationTrack();
      } else {
        this.provider_markers_order.setMap(null)
        this._notifierService.showNotification('error', this._helper._trans.instant('label-title.details-not-found'));
      }
    })
  }

  getProviderList() {
    this._orderTrackService.getProviderListForCity(this.selected_city).then((res) => {
      this.provider_list = []
      if (res.success) {
        this.provider_list = res.providers
      }
    })
  }

  getOrderList() {
    this._orderTrackService.getOrderListForCity(this.selected_city).then((res) => {
      this.order_list = []
      if (res.success) {
        this.order_list = res.orders
      }
    })
  }

  markProviderPin(map) {
    this.checkProviderStatus(this.selected_provider, map);
  }

  checkProviderStatus(provider, map) {
    let icon_url = 'assets/img/map_pin_images/Deliveryman/';
    if (this.type == 1) {
      if (provider.is_online) {
        this.createMarker(provider, icon_url + 'deliveryman_online_with_orders.png', map);
      } else {
        this.createMarker(provider, icon_url + 'deliveryman_offline_with_orders.png', map);
      }
    } else if (this.type == 0) {
      if (provider.is_online) {
        this.createMarker(provider, icon_url + 'deliveryman_online.png', map);
      }
      else {
        this.createMarker(provider, icon_url + 'deliveryman_offline.png', map);
      }
    }
  }

  createMarker(provider, icon_url, map) {
    this.provider_markers_order.setMap(null)
    if (provider.location) {
      let marker = new google.maps.Marker({
        position: { lat: provider.location[0], lng: provider.location[1] },
        map: this.map,
        icon: icon_url
      });

      this.provider_markers_order = marker;
      let name = this._helper._trans.instant('heading-title.name');
      let email = this._helper._trans.instant('heading-title.email');
      let rating = this._helper._trans.instant('label-title.rating');
      let contentString =
        '<p><b>' + name + '</b>: ' + provider.first_name + ' ' + provider.last_name +
        '<br><b>' + email + '</b>: ' + provider.email +
        '<br><b>' + rating + '</b>: ' + provider.user_rate.toFixed(this._helper.to_fixed_number) +
        '</p>';

      let message = new google.maps.InfoWindow({
        content: contentString,
        maxWidth: 350,
      });
      google.maps.event.addListener(marker, 'click', function (e) {
        message.open(map, marker);
        setTimeout(function () { message.close(); }, 5000);
      });
    }

  }

  moveMarker(lat, lng) {
    if (this.is_destroyed) {
      return; // ondestory -> function calling stop
    }
    if (!this.is_changed) {
      if (this.i !== this.numDeltas) {
        this.i++;
        this.latitude += lat;
        this.longitude += lng;
        let latlng = new google.maps.LatLng(this.latitude, this.longitude);
        this.provider_markers_order.setPosition(latlng);
        this.map.panTo(latlng);
        setTimeout(() => {
          this.moveMarker(lat, lng)
        }, 100);
      } else {
        this.providerLocationTrack();
      }
    } else {
      if (this.latitude && this.longitude) {
        let latlng = new google.maps.LatLng(this.latitude, this.longitude);
        this.provider_markers_order.setPosition(latlng);
        this.map.panTo(latlng);
      }
      this.is_changed = false
    }
  }

  providerLocationTrack() {
    let lat;
    let lng;
    this._orderTrackService.deliverymanTrack(this.id, this.type).then((res) => {
      if (res.success) {
        this.i = 0;
        this.new_lat_lng = res.provider.location;
        this.selected_provider = res.provider
        this.latitude = JSON.parse(JSON.stringify(this.old_lat_lng[0]));
        this.longitude = JSON.parse(JSON.stringify(this.old_lat_lng[1]));

        if (this.new_lat_lng[0] == this.old_lat_lng[0] && this.new_lat_lng[1] == this.old_lat_lng[1]) {
          lat = 0;
          lng = 0;
          this.moveMarker(lat, lng);
        }
        else {
          lat = (this.new_lat_lng[0] - this.old_lat_lng[0]) / this.numDeltas;
          lng = (this.new_lat_lng[1] - this.old_lat_lng[1]) / this.numDeltas;
          this.moveMarker(lat, lng);
          this.old_lat_lng = JSON.parse(JSON.stringify(this.new_lat_lng));
        }
      }
    });
  }

  ngOnDestroy() {
    this.is_destroyed = true;
  }
}
