import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DeliverymanTrackComponent } from './deliveryman-track.component';

describe('DeliverymanTrackComponent', () => {
  let component: DeliverymanTrackComponent;
  let fixture: ComponentFixture<DeliverymanTrackComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ DeliverymanTrackComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeliverymanTrackComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
