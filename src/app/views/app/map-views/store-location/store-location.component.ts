import { Component, OnInit } from '@angular/core';
import { CountryService } from 'src/app/services/country.service';
import { StoreService } from 'src/app/services/store.service';
import { Helper } from 'src/app/shared/helper';
import { AdminSettingService } from 'src/app/services/admin-setting.service';
import * as $ from "jquery";
import { DeliveryFeesService } from 'src/app/services/delivery-fees.service';
declare let google;

@Component({
  selector: 'app-store-location',
  templateUrl: './store-location.component.html',
})
export class StoreLocationComponent implements OnInit {

  map: any
  store_list = []
  country_list = []
  city_list = []
  delivery_list = []
  store_markers = []
  filtered_store_list = []

  selected_country: any;
  selected_city: any = 'All';
  selected_delivery = "All"
  is_open_selected = true
  is_close_selected = true
  is_business_off_selected = true
  selected_delivery_type: any = "Merhant"
  deliveries_type_list: any = [];
  settingData: any;
  address: string;
  cityLatLong: number[] = [0, 0];

  constructor(private _countryService: CountryService, private _storeService: StoreService, public _helper: Helper, private _adminSettingService: AdminSettingService
    , private _deliveryFeesService: DeliveryFeesService) { }

  ngOnInit(): void {
    let notification = false;
    this._adminSettingService.fetchAdminSetting(notification).then(res => {
      if (res.success) {
        this.settingData = res.setting
        this.cityLatLong = [this.settingData.latitude, this.settingData.longitude]
      }
      this._initMap()
      this.getCountryList()
      this.getStoreList()
      this.getDeliveryList()
      this.deliveries_type_list = this._helper.DELIVERY_TYPE.filter(x => x.value != this._helper.DELIVERY_TYPE_CONSTANT.COURIER);
    })
  }

  _initMap() {
    let theme = localStorage.getItem('vien-themecolor');
    if (theme.includes('dark')) {
      this.map = new google.maps.Map(document.getElementById('store_location_map'), {
        zoom: 2,
        streetViewControl: false,
        center: { lat: Number(this.cityLatLong[0]), lng: Number(this.cityLatLong[1]) },
        styles: [
          { elementType: "geometry", stylers: [{ color: "#242f3e" }] },
          { elementType: "labels.text.stroke", stylers: [{ color: "#242f3e" }] },
          { elementType: "labels.text.fill", stylers: [{ color: "#746855" }] },
          {
            featureType: "administrative.locality",
            elementType: "labels.text.fill",
            stylers: [{ color: "#d59563" }],
          },
          {
            featureType: "poi",
            elementType: "labels.text.fill",
            stylers: [{ color: "#d59563" }],
          },
          {
            featureType: "poi.park",
            elementType: "geometry",
            stylers: [{ color: "#263c3f" }],
          },
          {
            featureType: "poi.park",
            elementType: "labels.text.fill",
            stylers: [{ color: "#6b9a76" }],
          },
          {
            featureType: "road",
            elementType: "geometry",
            stylers: [{ color: "#38414e" }],
          },
          {
            featureType: "road",
            elementType: "geometry.stroke",
            stylers: [{ color: "#212a37" }],
          },
          {
            featureType: "road",
            elementType: "labels.text.fill",
            stylers: [{ color: "#9ca5b3" }],
          },
          {
            featureType: "road.highway",
            elementType: "geometry",
            stylers: [{ color: "#746855" }],
          },
          {
            featureType: "road.highway",
            elementType: "geometry.stroke",
            stylers: [{ color: "#1f2835" }],
          },
          {
            featureType: "road.highway",
            elementType: "labels.text.fill",
            stylers: [{ color: "#f3d19c" }],
          },
          {
            featureType: "transit",
            elementType: "geometry",
            stylers: [{ color: "#2f3948" }],
          },
          {
            featureType: "transit.station",
            elementType: "labels.text.fill",
            stylers: [{ color: "#d59563" }],
          },
          {
            featureType: "water",
            elementType: "geometry",
            stylers: [{ color: "#17263c" }],
          },
          {
            featureType: "water",
            elementType: "labels.text.fill",
            stylers: [{ color: "#515c6d" }],
          },
          {
            featureType: "water",
            elementType: "labels.text.stroke",
            stylers: [{ color: "#17263c" }],
          },
        ],
      });
    } else {
      this.map = new google.maps.Map(document.getElementById('store_location_map'), {
        zoom: 2,
        streetViewControl: false,
        center: { lat: Number(this.cityLatLong[0]), lng: Number(this.cityLatLong[1]) }
      });
    }

    if (this.cityLatLong[0] != 0) {
      this.map.setZoom(12)
    } else {
      this.map.setZoom(2)
    }
  }

  _initAutocomplete() {
    let country = this.country_list.filter((country) => country._id == this.selected_country);
    let country_code = null;
    if (country.length) {
      country_code = country[0].country_code_2;
    }

    let autocompleteElm = <HTMLInputElement>document.getElementById('store_address');
    let autocomplete;
    google.maps.event.clearInstanceListeners(autocompleteElm);
    if (country_code) {
      autocomplete = new google.maps.places.Autocomplete((autocompleteElm), { types: ['(cities)'], componentRestrictions: { country: country_code } });
    } else {
      autocomplete = new google.maps.places.Autocomplete((autocompleteElm), { types: ['(cities)'] });
    }
    autocomplete.addListener('place_changed', () => {
      let place = autocomplete.getPlace();
      if (place.geometry && place.geometry.location.lat() && place.geometry.location.lng()) {

        let lat = place.geometry.location.lat();
        let lng = place.geometry.location.lng();
        this.address = place['formatted_address'];
        this.cityLatLong = [lat, lng];
        this._initMap();
        this.filter();
      }
    });
    $('.search-address').find("#store_address").on("focus click keypress", () => {
      $('.search-address').css({ position: "relative" }).append($(".pac-container"));
    });
  }

  getStoreList() {
    this._storeService.getStoreListForMap().then(res => {
      if (res.success) {
        this.store_list = res.stores
        this.checkOpen(new Date())
      }
    })
  }

  getCountryList() {
    this._countryService.fetch().then(res => {
      if (res.success) {
        this.country_list = res.countries
        if (this.country_list.length > 0) {
          this.selected_country = this.country_list[0]._id;
          if (this.selected_country) {
            this.countryChange(this.selected_country)
          }
        } else {
          this.selected_city = null;
        }
      }
    })
  }

  countryChange(id) {
    this._deliveryFeesService.get_city_lists(this.selected_country).then(res => {
      this.city_list = []
      if (res.success) {
        this.city_list = res.cities
      }
      if (this.city_list.length >= 2) {
        this.selected_city = 'All';
      } else if (this.city_list.length > 0) {
        this.selected_city = this.city_list[0]._id;
        this.onCitySelected(this.selected_city);
      } else {
        this.selected_city = null;
      }
    })
    this.selectedCountryMap(id);
  }

  selectedCountryMap(id) {
    let i = this.country_list.findIndex((x: any) => x._id == id)
    if (this.country_list[i]?.country_lat_long[0]) {
      this.selected_country = id;
      let theme = localStorage.getItem('vien-themecolor');
      if (theme.includes('dark')) {
        this.map = new google.maps.Map(document.getElementById('store_location_map'), {
          zoom: 4,
          streetViewControl: false,
          center: { lat: Number(this.country_list[i].country_lat_long[0]), lng: Number(this.country_list[i].country_lat_long[1]) },
          styles: [
            { elementType: "geometry", stylers: [{ color: "#242f3e" }] },
            { elementType: "labels.text.stroke", stylers: [{ color: "#242f3e" }] },
            { elementType: "labels.text.fill", stylers: [{ color: "#746855" }] },
            {
              featureType: "administrative.locality",
              elementType: "labels.text.fill",
              stylers: [{ color: "#d59563" }],
            },
            {
              featureType: "poi",
              elementType: "labels.text.fill",
              stylers: [{ color: "#d59563" }],
            },
            {
              featureType: "poi.park",
              elementType: "geometry",
              stylers: [{ color: "#263c3f" }],
            },
            {
              featureType: "poi.park",
              elementType: "labels.text.fill",
              stylers: [{ color: "#6b9a76" }],
            },
            {
              featureType: "road",
              elementType: "geometry",
              stylers: [{ color: "#38414e" }],
            },
            {
              featureType: "road",
              elementType: "geometry.stroke",
              stylers: [{ color: "#212a37" }],
            },
            {
              featureType: "road",
              elementType: "labels.text.fill",
              stylers: [{ color: "#9ca5b3" }],
            },
            {
              featureType: "road.highway",
              elementType: "geometry",
              stylers: [{ color: "#746855" }],
            },
            {
              featureType: "road.highway",
              elementType: "geometry.stroke",
              stylers: [{ color: "#1f2835" }],
            },
            {
              featureType: "road.highway",
              elementType: "labels.text.fill",
              stylers: [{ color: "#f3d19c" }],
            },
            {
              featureType: "transit",
              elementType: "geometry",
              stylers: [{ color: "#2f3948" }],
            },
            {
              featureType: "transit.station",
              elementType: "labels.text.fill",
              stylers: [{ color: "#d59563" }],
            },
            {
              featureType: "water",
              elementType: "geometry",
              stylers: [{ color: "#17263c" }],
            },
            {
              featureType: "water",
              elementType: "labels.text.fill",
              stylers: [{ color: "#515c6d" }],
            },
            {
              featureType: "water",
              elementType: "labels.text.stroke",
              stylers: [{ color: "#17263c" }],
            },
          ],
        });
      } else {
        this.map = new google.maps.Map(document.getElementById('store_location_map'), {
          zoom: 4,
          streetViewControl: false,
          center: { lat: Number(this.country_list[i].country_lat_long[0]), lng: Number(this.country_list[i].country_lat_long[1]) }
        });
      }
    }
    this.filter();
  }

  onCitySelected(id) {
    if (id != 'All') {
      let index = this.city_list.findIndex(x => x._id === this.selected_city)
      this.cityLatLong = [this.city_list[index].city_lat_long[0], this.city_list[index].city_lat_long[1]];
      this._initMap();
      this.filter();
    } else {
      this.selectedCountryMap(this.selected_country);
    }
  }

  getDeliveryList() {
    if (this.selected_delivery_type == "Merhant") {
      this.selected_delivery_type = 1;
    }
    this._storeService.getDeliveryList().then(res => {
      if (res.success) {
        let delivery = res.deliveries
        this.delivery_list = delivery.filter(d => d.delivery_type == this.selected_delivery_type)
        if (this.delivery_list[0].delivery_type == this._helper.DELIVERY_TYPE_CONSTANT.TAXI || this.delivery_list[0].delivery_type == this._helper.DELIVERY_TYPE_CONSTANT.ECOMMERCE) {
          if (this.delivery_list.length == 1) {
            this.selected_delivery = this.delivery_list[0]._id;
          } else {
            this.selected_delivery = 'All'
          }
        } else {
          this.selected_delivery = 'All'
        }
        this.filter();
      }
    })
  }

  createMarker(store_list) {
    this.store_markers.forEach(function (marker) {
      marker.setMap(null);
    });
    this.store_markers = [];
    let to_fixed_number = this._helper.to_fixed_number;

    for (const element of store_list) {
      let store_data = element;
      let icon_url = 'assets/img/map_pin_images/Store/';
      if (!store_data.is_business) {
        icon_url = icon_url + 'store_business_off.png';
      } else if (store_data.close) {
        icon_url = icon_url + 'store_close.png';
      } else if (!store_data.close) {
        icon_url = icon_url + 'store_open.png';
      }
      if (store_data.location != undefined) {
        let marker = new google.maps.Marker({
          position: { lat: Number(store_data.location[0]), lng: Number(store_data.location[1]) },
          map: this.map,
          icon: icon_url
        });
        let infoWindow = new google.maps.InfoWindow();

        this.store_markers.push(marker);
        let name = this._helper._trans.instant('heading-title.name');
        let rating = this._helper._trans.instant('label-title.rating');
        (function (marker, store_data) {
          google.maps.event.addListener(marker, "click", function (e) {
            infoWindow.setContent('<p><b>' + name + '</b>: ' + store_data.name +
              '<br><b>' + rating + '</b>: ' + store_data.user_rate.toFixed(to_fixed_number) +
              '</p>');
            infoWindow.open(this.map, marker);
          });
        })(marker, store_data);
      }
    };
  }

  checkOpen(server_date) {
    this.store_list.forEach((store) => {
      let date: any = server_date;
      date = new Date(date).toLocaleString("en-US", { timeZone: store.city_detail.timezone })
      date = new Date(date);
      let weekday = date.getDay();
      let current_time = date.getTime();
      store.close = true;

      let week_index = -1
      if (store.store_time) {
        week_index = store.store_time.findIndex((x) => x.day == weekday)
      }

      if (week_index !== -1) {
        let day_time = store.store_time[week_index].day_time;

        if (store.store_time[week_index].is_store_open_full_time) {
          store.close = false;
        }
        else {
          if (store.store_time[week_index].is_store_open) {
            if (day_time.length == 0) {
              store.close = true;
            } else {
              day_time.forEach((store_time, index) => {
                let open_time = store_time.store_open_time;
                let open_date_time = date.setHours(open_time[0], open_time[1], 0, 0)
                open_date_time = new Date(open_date_time);
                open_date_time = open_date_time.getTime();

                let close_time = store_time.store_close_time;
                let close_date_time = date.setHours(close_time[0], close_time[1], 0, 0)
                close_date_time = new Date(close_date_time);
                close_date_time = close_date_time.getTime();

                if (current_time > open_date_time && current_time < close_date_time) {
                  store.close = false;
                }

              });
            }
          } else {
            store.close = true;
          }
        }
      }
    });
    this.filter()
  }

  filter() {
    this.filtered_store_list = JSON.parse(JSON.stringify(this.store_list));
    this.filtered_store_list = this.filtered_store_list.filter((store_data) => {
      return (this.selected_country == 'All' || this.selected_country == store_data.country_id);
    });

    this.filtered_store_list = this.filtered_store_list.filter((store_data) => {
      return (this.selected_city == 'All' || this.selected_city == store_data.city_id);
    });

    this.filtered_store_list = this.filtered_store_list.filter((store_data) => {
      return (this.selected_delivery_type == store_data.delivery_type);
    });

    this.filtered_store_list = this.filtered_store_list.filter((store_data) => {
      return (this.selected_delivery == 'All' || (this.selected_delivery == store_data.store_delivery_id));
    });

    let store_list = []
    this.filtered_store_list.forEach(store => {
      let is_push = false
      if (this.is_open_selected) {
        if (!store.close && store.is_business) {
          is_push = true
        }
      }
      if (this.is_close_selected) {
        if (store.close) {
          is_push = true
        }
      }
      if (this.is_business_off_selected) {
        if (!store.is_business) {
          is_push = true
        }
      }
      if (is_push) {
        store_list.push(store)
      }
    });

    this.createMarker(store_list);
  }
}
