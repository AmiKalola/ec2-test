import { Component, OnInit } from '@angular/core';
import * as $ from "jquery";
import { AdminSettingService } from 'src/app/services/admin-setting.service';
import { CountryService } from 'src/app/services/country.service';
import { DeliveryFeesService } from 'src/app/services/delivery-fees.service';
declare let google: any;

@Component({
  selector: 'app-all-cities',
  templateUrl: './all-cities.component.html',
})
export class AllCitiesComponent implements OnInit {

  map: any;
  city_circles: any[] = [];
  infoWindow = null;
  cityLatLong: number[] = [0, 0];
  settingData: any;
  country_name = '';
  country_list: any;
  selected_country: any;
  selected_country_id: any;
  city_list: any[]=[];
  selected_city = "All";
  selected_country_detail: any;
  selected_country_code: any;
  city_name: any;

  constructor(private _adminSettingService: AdminSettingService, private _countryService: CountryService, private _deliveryFeesService: DeliveryFeesService) { }

  ngOnInit(): void {
    let notification = false;
    this._adminSettingService.fetchAdminSetting(notification).then(res => {
      if (res.success) {
        this.settingData = res.setting;
        this.cityLatLong = [this.settingData.latitude, this.settingData.longitude];
        this.country_name = res.setting.admin_country;
      }
      this._initMap()
      this.getCountryList()
    })
  }
  //initialize map
  _initMap() {

    let theme = localStorage.getItem('vien-themecolor');
    if(theme.includes('dark')){
      this.map = new google.maps.Map(document.getElementById('city_map'), {
        zoom: 2.5,
        streetViewControl: false,
        center: { lat: this.cityLatLong[0], lng: this.cityLatLong[1] },
        styles: [
          { elementType: "geometry", stylers: [{ color: "#242f3e" }] },
          { elementType: "labels.text.stroke", stylers: [{ color: "#242f3e" }] },
          { elementType: "labels.text.fill", stylers: [{ color: "#746855" }] },
          {
            featureType: "administrative.locality",
            elementType: "labels.text.fill",
            stylers: [{ color: "#d59563" }],
          },
          {
            featureType: "poi",
            elementType: "labels.text.fill",
            stylers: [{ color: "#d59563" }],
          },
          {
            featureType: "poi.park",
            elementType: "geometry",
            stylers: [{ color: "#263c3f" }],
          },
          {
            featureType: "poi.park",
            elementType: "labels.text.fill",
            stylers: [{ color: "#6b9a76" }],
          },
          {
            featureType: "road",
            elementType: "geometry",
            stylers: [{ color: "#38414e" }],
          },
          {
            featureType: "road",
            elementType: "geometry.stroke",
            stylers: [{ color: "#212a37" }],
          },
          {
            featureType: "road",
            elementType: "labels.text.fill",
            stylers: [{ color: "#9ca5b3" }],
          },
          {
            featureType: "road.highway",
            elementType: "geometry",
            stylers: [{ color: "#746855" }],
          },
          {
            featureType: "road.highway",
            elementType: "geometry.stroke",
            stylers: [{ color: "#1f2835" }],
          },
          {
            featureType: "road.highway",
            elementType: "labels.text.fill",
            stylers: [{ color: "#f3d19c" }],
          },
          {
            featureType: "transit",
            elementType: "geometry",
            stylers: [{ color: "#2f3948" }],
          },
          {
            featureType: "transit.station",
            elementType: "labels.text.fill",
            stylers: [{ color: "#d59563" }],
          },
          {
            featureType: "water",
            elementType: "geometry",
            stylers: [{ color: "#17263c" }],
          },
          {
            featureType: "water",
            elementType: "labels.text.fill",
            stylers: [{ color: "#515c6d" }],
          },
          {
            featureType: "water",
            elementType: "labels.text.stroke",
            stylers: [{ color: "#17263c" }],
          },
        ],
      });
    }else{
      this.map = new google.maps.Map(document.getElementById('city_map'), {
        zoom: 2.5,
        streetViewControl: false,
        center: { lat: this.cityLatLong[0], lng: this.cityLatLong[1] }
      });
    }

    if (this.cityLatLong[0] != 0) {
      this.map.setZoom(12)
    } else {
      this.map.setZoom(2)
    }
  }


  //google place autocomplete for searchbox
  _initAutocomplete(event) {
    if(event == 'All'){
      this.cityList(this.selected_country);
      return;
    }
    let index = this.city_list.findIndex(e => e._id === event)
    this.selected_city = event;
    this.cityLatLong = this.city_list[index].city_lat_long
    let cityName = this.city_list[index].city_name
    if (this.city_list[index].city_lat_long[0]) {
      let bounds = new google.maps.LatLngBounds();
      this.map.fitBounds(bounds);
      this.cityLatLong = this.city_list[index].city_lat_long
      this.city_name = cityName;
      this._initMap()
      this.getCity();
    } else {
      this.map.setZoom(2)
    }
    $('.search-address').find("#address").on("focus click keypress", () => {
      $('.search-address').find("#address").parent(".input-wrp").css({ position: "relative" }).append($(".pac-container"));
    });
  }
  
  //get all country
  getCountryList() {
    this._countryService.fetch().then(res => {
      if (res.success) {
        this.country_list = res.countries;
        const country = this.country_list.find((c) => c.country_name === this.country_name);
        this.selected_country = country._id;
        this.cityList(this.selected_country);
      }
    })
  }

  //get all cities
  cityList(event) {
    let index = this.country_list.findIndex(e => e._id === event)
    this.selected_country_code = this.country_list[index].country_code_2
    this.selected_country = event;
    this.cityLatLong = this.country_list[index].country_lat_long
    if (this.country_list[index].country_lat_long[0]) {
      this.cityLatLong = this.country_list[index].country_lat_long
      this._initMap()
      this.map.setZoom(5)
    } else {
      this.map.setZoom(2)
    }
    this._deliveryFeesService.get_city_lists(this.selected_country).then(res => {
      if (res.success) {
        this.city_list = res.cities;
        if(this.city_list.length>=2){
          this.selected_city = 'All';
        }else{
          this.selected_city = null;
        }
        this.city_list.forEach((city) => {
          this.drawRadius(city)
        })
      }else{
        this.city_list = []
        this.selected_city = null;
      }
    })
  }

  getCity() {
    let index = this.city_list.findIndex(e => e.city_name == this.city_name)
    this.drawRadius(this.city_list[index])
    this.map.setZoom(10)
  }

  //draw radius of the cities
  drawRadius(city) {

    this.city_circles.forEach((circle) => {
      circle.setMap(null);
    })

    if (city.city_lat_long) {
      if (city.is_use_radius === true) {
        let color: string;
        if (city.is_business == 0) {
          color = 'red'
        }
        if (city.is_business == 1) {
          color = 'green'
        }

        let city_radius_circle = new google.maps.Circle({
          strokeColor: color,
          strokeOpacity: 0.8,
          strokeWeight: 2,
          fillColor: color,
          fillOpacity: 0.35,
          map: this.map,
          center: { lat: Number(city.city_lat_long[0]), lng: Number(city.city_lat_long[1]) },
          radius: (city.city_radius * 1000)
        });
        google.maps.event.addListener(city_radius_circle, 'click', (event) => {
          if (this.infoWindow) {
            this.infoWindow.close();
          }

          this.infoWindow = new google.maps.InfoWindow({
            content: city.city_name,
            maxWidth: 320
          });

          this.infoWindow.setPosition(event.latLng);
          this.infoWindow.open(this.map, city_radius_circle);
        });
      } else {
        let array = [];

        city.city_locations.forEach((location) => {
          array.push({ lat: Number(location[1]), lng: Number(location[0]) })
        });
        let color: string;
        if (city.is_business == 0) {
          color = 'red'
        }
        if (city.is_business == 1) {
          color = 'green'
        }

        let city_polygon = new google.maps.Polygon({
          map: this.map,
          paths: array,
          strokeColor: color,
          strokeOpacity: 1,
          strokeWeight: 1.2,
          fillColor: color,
          fillOpacity: 0.3,
          draggable: false,
          geodesic: true,
          editable: false
        });
        google.maps.event.addListener(city_polygon, 'click', (event) => {
          if (this.infoWindow) {
            this.infoWindow.close();
          }

          this.infoWindow = new google.maps.InfoWindow({
            content: city.city_name,
            maxWidth: 320
          });

          this.infoWindow.setPosition(event.latLng);
          this.infoWindow.open(this.map, city_polygon);
        });
      }

    }
  }

}
