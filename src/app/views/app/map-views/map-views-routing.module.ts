import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MapViewsComponent } from './map-views.component';
import { StoreLocationComponent } from './store-location/store-location.component';
import { DeliverymanLocationComponent } from './deliveryman-location/deliveryman-location.component';
import { DeliverymanTrackComponent } from './deliveryman-track/deliveryman-track.component';
import { AllCitiesComponent } from './all-cities/all-cities.component'


const routes: Routes = [
  {
    path: '', component: MapViewsComponent,
    children: [
      { path: '', redirectTo: 'store-location', pathMatch: 'full' },
      { path: 'store-location', component: StoreLocationComponent, data: {auth: '/admin/store-location'}},
      { path: 'provider-location', component: DeliverymanLocationComponent, data: {auth: '/admin/provider-location'} },
      { path: 'delivery-man-tracking', component: DeliverymanTrackComponent, data: {auth: '/admin/delivery-man-tracking'} },
      { path: 'business-area', component: AllCitiesComponent, data: {auth: '/admin/business-area'} },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MapViewsRoutingModule { }
