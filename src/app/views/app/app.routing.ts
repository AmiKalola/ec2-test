import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { BlankPageComponent } from './blank-page/blank-page.component';

const routes: Routes = [
    {
        path: '', component: AppComponent,
        children: [
            { path: '', pathMatch: 'full', redirectTo: 'dashboard' },
            { path: 'dashboard', loadChildren: () => import('./vien/vien.module').then(m => m.VienModule) },
            { path: 'menu', loadChildren: () => import('./menu/menu.module').then(m => m.MenuModule) },
            { path: 'order', loadChildren: () => import('./order/order.module').then(m => m.OrderModule) },
            { path: 'history', loadChildren: () => import('./history/history.module').then(m => m.HistoryModule) },
            { path: 'report', loadChildren: () => import('./reports/reports.module').then(m => m.ReportsModule) },
            { path: 'users', loadChildren: () => import('./users/users.module').then(m => m.UsersModule) },
            { path: 'settings', loadChildren: () => import('./settings/settings.module').then(m => m.SettingsModule) },
            { path: 'seo', loadChildren: () => import('./seo/seo.module').then(m => m.SeoModule) },
            { path: 'sub-stores', loadChildren: () => import('./sub-stores/sub-stores.module').then(m => m.SubStoresModule) },
            { path: 'earning', loadChildren: () => import('./earning/earning.module').then(m => m.EarningModule) },
            { path: 'delivery-info', loadChildren: () => import('./../app/delivery-info/delivery-info.module').then(m => m.DeliveryInfoModule) },
            { path: 'above', loadChildren: () => import('./above/above.module').then(m => m.AboveModule) },
            { path: 'map-views', loadChildren: () => import('./map-views/map-views.module').then(m => m.MapViewsModule) },
            { path: 'blank-page', component: BlankPageComponent },
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
