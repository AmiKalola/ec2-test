import { Component, OnInit, OnDestroy } from '@angular/core';
import { Helper } from 'src/app/shared/helper';
import { environment } from 'src/environments/environment';
import { getThemeColor } from 'src/app/utils/util';

@Component({
  selector: 'app-error',
  templateUrl: './error.component.html'
})
export class ErrorComponent implements OnInit, OnDestroy {
  adminRoot = environment.adminRoot;
  isDarkModeActive: boolean = false;

  constructor(public helper: Helper) { 
    this.isDarkModeActive = getThemeColor().indexOf('dark') > -1;
  }

  ngOnInit(): void {
    document.body.classList.add('background');
  }

  ngOnDestroy(): void {
    document.body.classList.remove('background');
  }

}
