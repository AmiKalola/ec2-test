import { Component, TemplateRef, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/shared/auth.service';
import { Helper } from 'src/app/shared/helper';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { NotifiyService } from 'src/app/services/notifier.service';
import { getThemeColor } from 'src/app/utils/util';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
})
export class ForgotPasswordComponent {
  @ViewChild('passwordForm') passwordForm: NgForm;
  buttonDisabled = false;
  buttonState = '';
  email_otp: any
  modelRef: BsModalRef;
  isLoading: boolean =  false
  is_invalid_otp = false
  config = {
    backdrop: true,
    keyboard: false,
    ignoreBackdropClick: true,
    class: 'modal-popup'
  };
  isDarkModeActive: boolean = false;

  @ViewChild('otpTemplate', {static: true}) otpTemplate: TemplateRef<any>;
  
  constructor(private authService: AuthService, 
    private _notifierService: NotifiyService, 
    private modalService: BsModalService,
    private router: Router, 
    public helper: Helper) {
      this.isDarkModeActive = getThemeColor().indexOf('dark') > -1;
    }



  async onSubmit(): Promise<void> {
    let captcha_token = await this.helper.getRecaptchaToken()
        if (!this.passwordForm.valid || this.buttonDisabled) {
          return;
        }
        this.buttonDisabled = true;
        this.buttonState = 'show-spinner';
        let json = {
          email: this.passwordForm.value.email,
          type: 1,
          device_type: 'web',
          captcha_token
        }
        if(captcha_token){
          json.captcha_token = captcha_token 
        }
        this.authService.sendPasswordEmail(json).then((res_data) => {
          if (res_data.success) {
            setTimeout(() => {
              this.modelRef = this.modalService.show(this.otpTemplate, this.config)
            }, 3000);
          } else {
            this.isLoading = false;
            this.buttonState = '';
            this.buttonDisabled = false;
          }

        }).catch((error) => {
          setTimeout(() => {
            this._notifierService.showNotification('error',error.message);
            this.buttonDisabled = false;
            this.isLoading = false
            this.buttonState = '';
          }, 3000);
        });
      // })
  }

  async verify_otp() {
    let captcha_token = await this.helper.getRecaptchaToken()
        if (this.email_otp.length === 6) {
          let json = {
            otp: this.email_otp,
            email: this.passwordForm.value.email,
            phone: '',
            captcha_token,
            device_type: 'web'
          }
          if(captcha_token){
              json.captcha_token = captcha_token
          }
          this.authService.otpVerify(json).then(res_data => {
            if (res_data.success) {
              this.modelRef.hide()
              this.email_otp = ''
              let json = {
                _id: res_data.id,
                servertoken: res_data.server_token
              }
              localStorage.setItem('admindata', JSON.stringify(json))
              this.router.navigate(['admin/reset-password'])
            }
          })
        } else {
          this.is_invalid_otp = true
        }
  }

  hideOtpModel() {
    this.modelRef.hide()
    this.buttonDisabled = false;
    this.buttonState = '';
    this.email_otp = ''
  }

}
