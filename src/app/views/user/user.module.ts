import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { UserComponent } from './user.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { UserRoutingModule } from './user.routing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { ComponentsStateButtonModule } from 'src/app/components/state-button/components.state-button.module';
import { ModalModule } from 'ngx-bootstrap/modal';
import { RecaptchaModule, RecaptchaFormsModule, RecaptchaV3Module, RECAPTCHA_V3_SITE_KEY } from "ng-recaptcha";
import { SubscriptionComponent } from './subscription/subscription.component';

@NgModule({
  declarations: [LoginComponent, RegisterComponent, ForgotPasswordComponent, UserComponent, ResetPasswordComponent, SubscriptionComponent],
  imports: [
    CommonModule,
    UserRoutingModule,
    NgSelectModule,
    ReactiveFormsModule,
    FormsModule,
    SharedModule,
    ComponentsStateButtonModule,
    RecaptchaV3Module,
    RecaptchaModule,
    RecaptchaFormsModule,
    ModalModule.forRoot(),
  ],
  providers: [{ provide: RECAPTCHA_V3_SITE_KEY, useValue: "6Lcgww4hAAAAADYQet8_04Hhk7PqN0cWcDBd0oIK" }]
})
export class UserModule { }
