import { Component, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/shared/auth.service';
import { Helper } from 'src/app/shared/helper';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
})
export class ResetPasswordComponent  {
  @ViewChild('resetForm') resetForm: NgForm;
  emailModel = 'demo@vien.com';
  passwordModel = 'demovien1122';

  buttonDisabled = false;
  buttonState = '';
  admindata: any;
  is_password_miss_match: boolean = false
  newshowPassword: boolean = false; // password eye icon boolean
  showPassword: boolean = false; // password eye icon boolean

  constructor(
    private authService: AuthService,
    public _helper: Helper,
    private router: Router
  ) {}


  onSubmit(): void {
    if (!this.resetForm.invalid) {
      if (this.resetForm.value.code === this.resetForm.value.newPassword) {

        this.admindata = JSON.parse(localStorage.getItem('admindata'))
        let json = {
          id: this.admindata._id,
          server_token: this.admindata.servertoken,
          password: this.resetForm.value.newPassword,
          type: 1
        }
        this.buttonDisabled = true;
        this.buttonState = 'show-spinner';

        this.authService.resetPassword(json).then(res_data => {
          if (res_data.success) {
            this.router.navigate(['admin/'])

          }
        })
      } else {
        this.is_password_miss_match = true
      }
    }
  }
}
