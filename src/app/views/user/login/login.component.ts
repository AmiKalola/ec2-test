import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AuthService } from 'src/app/shared/auth.service';
import { Subscription } from 'rxjs';
import { Helper } from 'src/app/shared/helper';
import { environment } from 'src/environments/environment';
import { CommonService } from 'src/app/services/common.service';
import { getThemeColor } from 'src/app/utils/util';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html'
})
export class LoginComponent implements OnDestroy,OnInit {
  @ViewChild('loginForm') loginForm: NgForm;
  usernameModel = '';
  passwordModel = '';
  buttonDisabled = false;
  buttonState = '';
  StoreType = [{ id: 0, name: 'Store' }, { id: 1, name: 'SubStore' }]
  loginSubscription:Subscription;
  app_name: any = 'eSuper';

  isCaptchaVerified: boolean = false
  captchaToken: any = '';
  is_use_captcha: boolean= false;
  showPassword: boolean = false; // password eye icon boolean
  isDarkModeActive: boolean = false;

  constructor(private authService: AuthService, public helper:Helper, public commonService: CommonService,) {
    this.isDarkModeActive = getThemeColor().indexOf('dark') > -1;
   }

  addTagFn = addedName => ({ name: addedName, tag: true });


  ngOnInit(){
    this.app_name = localStorage.getItem('appName')
    this.loginSubscription = this.authService.loginSession.subscribe(sessionData=>{      
      if(sessionData && sessionData !== null){
        if(this.helper.is_rental){
          this.authService.check_subscription().then((response)=>{
            if(response.success){
              this.helper.router.navigate([environment.adminRoot])
            }else{
              this.helper.router.navigate(['admin/check-subscription'])
            }
          })
        }else{
          this.helper.router.navigate([environment.adminRoot])
        }
      }
    })
    this.storeSettingDetail();
  }

  resolved(captchaResponse: string) {
    this.isCaptchaVerified = false
    this.captchaToken = captchaResponse
  }

  async onSubmit() {
    if (!this.loginForm.valid || this.buttonDisabled) {
      return;
    }

    this.buttonDisabled = true;
    this.buttonState = 'show-spinner';
    let login = {
      username: this.loginForm.value.username,
      password: this.loginForm.value.password,
      device_type: 'web',
      captcha_token : '',
    }
    await this.authService.signIn(login)
    this.buttonDisabled = false;
    this.buttonState = '';
    setTimeout(()=>{
      this.captchaToken=''
    },1000)
  }

  storeSettingDetail()
  {
    this.commonService.store_setting_details().then((store_data:any)=>{
      this.is_use_captcha = store_data.is_use_captcha
    });
  }

  ngOnDestroy(){
    this.loginSubscription.unsubscribe()
  }


}