import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { AuthService } from 'src/app/shared/auth.service';
import { UntypedFormControl, UntypedFormGroup, NgForm, Validators } from '@angular/forms';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  @ViewChild('registerForm') registerForm: NgForm;

  RegisterForm:UntypedFormGroup;

  name: any;
  email: any;
  phone: any;
  address: any;
  password: any;
  buttonDisabled = false;
  buttonState = '';

  modalRef: BsModalRef | null;
  modalRef2: BsModalRef;
  changeTimeZone=false
  constructor(private authService: AuthService,
              private modalService: BsModalService) { }


  ngOnInit(){
    this._initForm()
  }


  _initForm(){
    this.RegisterForm = new UntypedFormGroup({
      email:new UntypedFormControl(null,Validators.required),
      name:new UntypedFormControl(null,Validators.required),
      phone:new UntypedFormControl(null,[Validators.required,Validators.minLength(5),Validators.maxLength(10)]),
      address:new UntypedFormControl(null,Validators.required),
      password:new UntypedFormControl(null,Validators.required),
    })
  }

  async onSubmit() {

    if(this.RegisterForm.invalid){
      this.RegisterForm.markAllAsTouched()
    }else{
      this.buttonDisabled = true;
      this.buttonState = 'show-spinner';
      await this.authService.register(this.RegisterForm.value).then(res_data => {
        if(!res_data.success){
          grecaptcha.reset()
        }
      })
      this.buttonDisabled = false;
      this.buttonState = '';
    }
  }

  openModal(template: TemplateRef<any>): void {
    this.modalRef = this.modalService.show(template, { class: 'modal-lg' });
  }
  openModal2(template: TemplateRef<any>): void {
    this.changeTimeZone = false;
    this.modalRef.hide();
    this.modalRef2 = this.modalService.show(template, { class: 'second' });
  }
  closeFirstModal(): void {
    if (!this.modalRef) {
      return;
    }

    this.modalRef.hide();
    this.modalRef = null;
  }
}
