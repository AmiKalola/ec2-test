import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/shared/auth.service';
import { Helper } from 'src/app/shared/helper';
import { environment } from 'src/environments/environment';
import { getThemeColor } from 'src/app/utils/util';

@Component({
  selector: 'app-subscription',
  templateUrl: './subscription.component.html'
})
export class SubscriptionComponent implements OnInit {
  isDarkModeActive: boolean = false;

  constructor(private authService: AuthService, public helper:Helper) { 
    this.isDarkModeActive = getThemeColor().indexOf('dark') > -1;
  }

  ngOnInit(): void {
    if(this.helper.is_rental){
      this.authService.check_subscription().then((response)=>{
        if(response.success){
          this.helper.router.navigate([environment.adminRoot])
        }
      })
    }else{
      this.helper.router.navigate([environment.adminRoot])
    }
  }

  onClickAddCard(){
    
    let host = `${window.location}`
    
    this.authService.create_subscription_session({ url : host }).then((response)=>{
      if(response.success === true){
        window.open(response.url, '_self');
      }
    })

  }

}
