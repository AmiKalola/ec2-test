import { Component, OnInit, Renderer2, AfterViewInit , Injectable } from '@angular/core';
import { LangService } from './shared/lang.service';
import { environment } from 'src/environments/environment';
import { AuthService } from './shared/auth.service';
import { CommonService } from './services/common.service';
import { AdminSettingService } from './services/admin-setting.service';
import { Helper } from './shared/helper';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})

@Injectable()
export class AppComponent implements OnInit, AfterViewInit {
  isMultiColorActive = environment.isMultiColorActive;
  constructor(private langService: LangService, 
    private renderer: Renderer2,
    private authService:AuthService,
    private commonService: CommonService,
    public _adminSettingService: AdminSettingService,
    public _helper : Helper
    ) {
    this.getAdminData()
  }

  ngOnInit(): void {
    this.authService.autologin()
    this.langService.init();
    if(this._helper.is_rental){
      this.authService.check_subscription().then((response)=>{
        if(response.success){
          this._helper.router.navigate([environment.adminRoot])
        }else{
          this._helper.router.navigate(['admin/check-subscription'])
        }
      })
    }
  }

  ngAfterViewInit(): void {
    setTimeout(() => {
      this.renderer.addClass(document.body, 'show');
    }, 1000);
    setTimeout(() => {
      this.renderer.addClass(document.body, 'default-transition');
    }, 1500);
  }

  getAdminData(){
    let app_name = localStorage.getItem('appName')
    if(!app_name){
      this.commonService.get_app_name().then(res_data => {
        localStorage.setItem('appName', res_data.app_name)
      })
    }

    document.getElementById('fav-icon').setAttribute('href', environment.imageUrl + 'web_images/title_image.png');
  }
}
