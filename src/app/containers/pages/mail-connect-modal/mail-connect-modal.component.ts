import { Component, TemplateRef,  ViewChild, OnInit } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { UntypedFormGroup, UntypedFormControl, Validators } from '@angular/forms';
import { EmailService } from '../../../services/email.service';

@Component({
  selector: 'app-mail-connect-modal',
  templateUrl: './mail-connect-modal.component.html',
  styleUrls: ['./mail-connect-modal.component.scss']
})
export class MailConnectModalComponent implements OnInit {
  showPassword: boolean = false; // password eye icon boolean

  modalRef: BsModalRef;
  config = {
    backdrop: true,
    ignoreBackdropClick: true,
    class: 'modal-right'
  };

  mailConfigForm:UntypedFormGroup

  @ViewChild('template', { static: true }) template: TemplateRef<any>;

  constructor(private modalService: BsModalService,private _emailService: EmailService) { }

  ngOnInit(): void {
    this._initForm()
  }

  show(): void {
    this.showPassword =false;
    this.getEmailConfigDetails()
    this.modalRef = this.modalService.show(this.template, this.config);
  }

  _initForm(){
    this.mailConfigForm = new UntypedFormGroup({
      email: new UntypedFormControl(null,[Validators.required, Validators.email]),
      password: new UntypedFormControl(null,Validators.required),
      domain: new UntypedFormControl("gmail",Validators.required),
      smtp_port: new UntypedFormControl(null),
      smtp_host: new UntypedFormControl(null),
      email_admin_info: new UntypedFormControl(null,Validators.required)
    })
  }

  getEmailConfigDetails(){
    this._emailService.getEmailGatewayDetail().then((res)=>{
      if(res.success){
        this.mailConfigForm.patchValue({ ...res.setting, email_admin_info: res.email_admin_info })
      }
    })
  }

  onSave(){
    this.mailConfigForm.markAllAsTouched();
    if(this.mailConfigForm.valid){
      this._emailService.updateEmailConfiguration(this.mailConfigForm.value)
      this.onClose()
    }
  }

  onClose() {
    this.modalRef.hide()
  }
}
