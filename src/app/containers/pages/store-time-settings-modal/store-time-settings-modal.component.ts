import { Component, EventEmitter, Input, Output, TemplateRef,  ViewChild } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { Helper } from 'src/app/shared/helper';
@Component({
  selector: 'store-time-settings-modal',
  templateUrl: './store-time-settings-modal.component.html',
  styleUrls: ['./store-time-settings-modal.component.scss']
})
export class StoreTimeSettingsModalComponent  {
  openTime: any;
  closeTime: any;
  private _surge_hours: any[] = [];

  @Input() set surge_hours(value: any[]) {
    // Create a deep copy of the input array
    this._surge_hours = JSON.parse(JSON.stringify(value));
  }

  get surge_hours(): any[] {
    return this._surge_hours;
  }
  @Output() updateStoreTime = new EventEmitter<any>();

  modalRef: BsModalRef;
  config = {
    backdrop: true,
    ignoreBackdropClick: true,
    class: 'modal-right'
  };
  basicTime = new Date();
  store_time_error = 0;
  surge_multiplier = 1;
  addSlotTime = {
    0: false,
    1: false,
    2: false,
    3: false,
    4: false,
    5: false,
    6: false
  }
  errorStartTime: boolean = false;
  errorEndTime: boolean = false;
  errorSurgeMultiplier: boolean = false;
  delivery_type:any;

  @ViewChild('template', { static: true }) template: TemplateRef<any>;

  constructor(private modalService: BsModalService,public _helper: Helper) { }


  show(delivery_type): void {
    this.delivery_type = delivery_type;
    this.modalRef = this.modalService.show(this.template, this.config);
    this.addSlotTime = {
      0: false,
      1: false,
      2: false,
      3: false,
      4: false,
      5: false,
      6: false
    }
  }

  pad2(number) {
    return (number < 10 ? '0' : '') + number
  }

  saveSlot(index) {
    if (!this.openTime || !this.closeTime || this.surge_multiplier === null) {
      if (!this.openTime) {
        this.errorStartTime = true;
      }
      if (!this.closeTime) {
        this.errorEndTime = true;
      }
      if (this.surge_multiplier === null) {
        this.errorSurgeMultiplier = true;
      }
      return;
    } else {
      this.errorStartTime = false;
      this.errorEndTime = false;
      this.errorSurgeMultiplier = false;
    }

    if ((this.openTime < this.closeTime) && this.closeTime && this.openTime) {
      let openHour = this.pad2((new Date(this.openTime.getTime())).getHours());
      let closeHour = this.pad2((new Date(this.closeTime.getTime())).getHours());
      let openMinute = this.pad2((new Date(this.openTime.getTime())).getMinutes());
      let closeMinute = this.pad2((new Date(this.closeTime.getTime())).getMinutes());
      let open_am_pm = ""
      let close_am_pm = ""

      if ((new Date(this.openTime.getTime())).getHours() == 12) {
        open_am_pm = this.pad2((new Date(this.openTime.getTime())).getHours()) + ':' + openMinute + " PM";
      }else if ((new Date(this.openTime.getTime())).getHours() > 12) {
        open_am_pm = this.pad2((new Date(this.openTime.getTime())).getHours() - 12) + ':' + openMinute + " PM";
      }else if ((new Date(this.openTime.getTime())).getHours() == 0) {
        open_am_pm = this.pad2((new Date(this.openTime.getTime())).getHours() + 12) + ':' + openMinute + " AM";
      } else {
        open_am_pm = openHour + ':' + openMinute + " AM";
      }

      if ((new Date(this.closeTime.getTime())).getHours() == 12) {
        close_am_pm = this.pad2((new Date(this.closeTime.getTime())).getHours()) + ':' + closeMinute + " PM";
      }else if ((new Date(this.closeTime.getTime())).getHours() > 12) {
        close_am_pm = this.pad2((new Date(this.closeTime.getTime())).getHours() - 12) + ':' + closeMinute + " PM";
      }else if ((new Date(this.closeTime.getTime())).getHours() == 0) {
        close_am_pm = this.pad2((new Date(this.closeTime.getTime())).getHours() + 12) + ':' + closeMinute + " AM";
      } else {
        close_am_pm = closeHour + ':' + closeMinute + " AM";
      }

      this.store_time_error = 0
      let open = new Date(new Date().setHours(Number(openHour),Number(openMinute),0,0)).getTime();
      let close = new Date(new Date().setHours(Number(closeHour),Number(closeMinute),0,0)).getTime();

      this._surge_hours[index].day_time.forEach(time => {
        let open_time = time.start_time.split(" ");
        let close_time = time.end_time.split(" ");
        let open_time1 = open_time[0].split(':')
        let close_time1 = close_time[0].split(':')
        let start_hour = this.convertTo24Hour(open_time1, open_time[1]);;
        let close_hour = this.convertTo24Hour(close_time1, close_time[1]);
        let start_min = Number(open_time1[1])
        let close_min = Number(close_time1[1])

        let open_date_time = new Date(new Date().setHours(start_hour,start_min,0,0)).getTime();
        let close_date_time = new Date(new Date().setHours(close_hour,close_min,0,0)).getTime();

        if ((open_date_time <= open) && (open <= close_date_time)) {
          console.log("1");
          this.store_time_error = 1
        } else if (open <= open_date_time && close_date_time <= close) {
          console.log("2");
          this.store_time_error = 1
        } else if ((open_date_time <= close) && (close <= close_date_time)) {
          console.log("3");
          this.store_time_error = 1
        }
      });
      if(!this.store_time_error){
        this.addSlotTime[index] = false;
        this._surge_hours[index].day_time.push({ 
          start_time: open_am_pm, 
          end_time: close_am_pm, 
          surge_multiplier: this.surge_multiplier 
        })
        this.openTime = undefined;
        this.closeTime = undefined;
      }
    }
  }

  convertTo24Hour(time, period) {
    let hour = Number(time[0]);
    if (period === "AM") {
        return hour === 12 ? 0 : hour;
    } else { // PM case
        return hour === 12 ? 12 : hour + 12;
    }
}

  addNewSlot(index){
    this.errorStartTime = false;
    this.errorEndTime = false;
    this.errorSurgeMultiplier = false;
    this.addSlotTime = {
      0: false,
      1: false,
      2: false,
      3: false,
      4: false,
      5: false,
      6: false
    }
    if (this.addSlotTime[index] === true) {
      this.addSlotTime[index] = false
    } else {
      this.addSlotTime[index] = true
    }
  }

  onClose() {
    this.openTime=null;
    this.closeTime=null;
    this.surge_multiplier=1;
    this.errorStartTime = false;
    this.errorEndTime = false;
    this.errorSurgeMultiplier = false;
    this.modalRef.hide();
  }

  onCheck(slot, slot_index, day_index, event) {
    this._surge_hours[day_index].day_time.splice(slot_index, 1);
  }

  onSave(){
    let array = [];
    this._surge_hours.forEach((time) => {
      array.push({
        "is_surge_hours": time.is_surge_hours,
        "is_surge_on_night": (this.delivery_type == this._helper.DELIVERY_TYPE_CONSTANT.TAXI) ? false : time.is_surge_on_night,
        "day": time.day,
        "day_time": []
      });
      time.day_time.forEach((day_time) => {
        let open_am_pm = day_time.start_time.split(" ");
        let close_am_pm = day_time.end_time.split(" ");
        let open_minute = open_am_pm[0].split(":");
        let close_minute = close_am_pm[0].split(":");

        if (open_am_pm[1] == "PM" && Number(open_minute[0]) != 12) {
          open_minute[0] = Number(open_minute[0]) + 12
        }
        if(open_am_pm[1] == "AM" && Number(open_minute[0]) == 12) {
          open_minute[0] = Number(open_minute[0]) - 12
        }
        if (close_am_pm[1] == "PM" && Number(close_minute[0]) != 12) {
          close_minute[0] = Number(close_minute[0]) + 12
        }
        if (close_am_pm[1] == "AM" && Number(close_minute[0]) == 12) {
          close_minute[0] = Number(close_minute[0]) - 12
        }

        array[array.length - 1].day_time.push({
          "start_time": (Number(open_minute[0]) * 60 + Number(open_minute[1])),
          "end_time": (Number(close_minute[0]) * 60 + Number(close_minute[1])),
          "surge_multiplier": day_time.surge_multiplier
        });

      })
    })
    this.onClose();
    this.updateStoreTime.emit(array)
  }

}
