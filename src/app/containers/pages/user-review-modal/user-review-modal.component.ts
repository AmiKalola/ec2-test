import { Component, TemplateRef,  ViewChild } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { environment } from 'src/environments/environment';
import { DEFAULT_IMAGE_PATH } from 'src/app/views/app/constant';
import { Helper } from 'src/app/shared/helper';


export interface ReviewDetail {
  user_name:string,
  user_phone:string,
  user_image:string,
  store_name:string,
  store_phone:string,
  store_image:string,
  provider_name:string,
  provider_phone:string,
  provider_image:string,
  order_id:string,
  order_comment:string,
  order_rating:number,
  created_date:Date,
  provider_comment:string,
  provider_rating:number,
  number_of_users_dislike_store_comment:string,
  number_of_users_like_store_comment:string,
  
  user_rating_to_provider: string,
  user_rating_to_store: string,
  store_rating_to_user:string,
  store_rating_to_provider: string,
  provider_rating_to_user: string,
  provider_rating_to_store: string,

  user_review_to_provider:string,
  user_review_to_store:string,
  store_review_to_provider:string,
  store_review_to_user:string,
  provider_review_to_store: string,
  provider_review_to_user: string,

}

@Component({
  selector: 'app-user-review-modal',
  templateUrl: './user-review-modal.component.html',
  styleUrls: ['./user-review-modal.component.scss']
})
export class UserReviewModalComponent  {
  modalRef: BsModalRef;
  config = {backdrop: true,ignoreBackdropClick: true,class: 'modal-right'};

  reviewDetail:ReviewDetail;
  IMAGE_URL = environment.imageUrl;
  USER_DEFAULT_IMAGE = DEFAULT_IMAGE_PATH.USER
  timezone_for_display_date:string = '';

  @ViewChild('template', { static: true }) template: TemplateRef<any>;

  constructor(private modalService: BsModalService,public _helper:Helper) {
    this._helper.display_date_timezone.subscribe(data => {
      this.timezone_for_display_date = data;
    })
  }


  show(reviewDetail:ReviewDetail): void {
    this.reviewDetail = reviewDetail
    this.modalRef = this.modalService.show(this.template, this.config);
  }

  onClose(){
    this.modalRef.hide();
  }
}

