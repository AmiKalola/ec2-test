import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GoogleCaptchaSettingModalComponent } from './google-captcha-setting-modal.component';

describe('GoogleCaptchaSettingModalComponent', () => {
  let component: GoogleCaptchaSettingModalComponent;
  let fixture: ComponentFixture<GoogleCaptchaSettingModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GoogleCaptchaSettingModalComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(GoogleCaptchaSettingModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
