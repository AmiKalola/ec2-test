import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { UntypedFormGroup, UntypedFormControl } from '@angular/forms';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { AdminSettingService } from 'src/app/services/admin-setting.service';

@Component({
  selector: 'app-google-captcha-setting-modal',
  templateUrl: './google-captcha-setting-modal.component.html',
})
export class GoogleCaptchaSettingModalComponent implements OnInit {

  modalRef: BsModalRef;
  config = {
    backdrop: true,
    ignoreBackdropClick: true,
    class: 'modal-right'
  };

  google_captcha_key_settings:UntypedFormGroup;

  @ViewChild('template', { static: true }) template: TemplateRef<any>;

  constructor(private modalService: BsModalService,private _adminSettingService: AdminSettingService) { }

  ngOnInit(): void {
    this._initForm()
  }

  show(google_captcha_key_settings=null): void {
    this.google_captcha_key_settings.patchValue(google_captcha_key_settings)
    this.modalRef = this.modalService.show(this.template, this.config);
  }

  _initForm() {
    this.google_captcha_key_settings = new UntypedFormGroup({
      captcha_site_key_for_web: new UntypedFormControl(null),
      captcha_secret_key_for_web: new UntypedFormControl(null),
      captcha_site_key_for_android: new UntypedFormControl(null),
      captcha_secret_key_for_android: new UntypedFormControl(null),
      captcha_site_key_for_ios: new UntypedFormControl(null),
      captcha_secret_key_for_ios: new UntypedFormControl(null),
    })
  }

  onSave(){
    this._adminSettingService.updateAdminSetting(this.google_captcha_key_settings.value)
    this.onClose()
  }

  onClose(){
    this.modalRef.hide()
  }
}
