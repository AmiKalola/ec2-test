import { Component, EventEmitter, OnInit, Output, TemplateRef,  ViewChild } from '@angular/core';
import { UntypedFormArray, UntypedFormControl, UntypedFormGroup, NgForm, Validators } from '@angular/forms';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { CategoryService } from 'src/app/services/category.service';
import { SubcategoryService } from 'src/app/services/subcategory.service';
import { CommonService } from 'src/app/services/common.service';
import { LangService } from 'src/app/shared/lang.service';
import { environment } from 'src/environments/environment';
import { ImageSetting } from 'src/app/models/image_setting.model';
import { ImageCropModelComponent } from 'src/app/containers/pages/image-crop-model/image-crop-model.component';
import { Helper } from 'src/app/shared/helper';
import { DELIVERY_TYPE } from 'src/app/views/app/constant';
import { NotifiyService } from 'src/app/services/notifier.service';

export interface AddCategory{
  _id:any,
  name:any,
  sequence_number:number,
  product_ids:any,
}

@Component({
  selector: 'app-add-new-category-modal',
  templateUrl: './add-new-category-modal.component.html',
  styles: []
})
export class AddNewCategoryModalComponent implements OnInit  {
  modalRef: BsModalRef;
  config = {
    backdrop: true,
    ignoreBackdropClick: true,
    class: 'modal-right'
  };
  category_time: any
  time: any = [
    {
      "is_category_open": true,
      "is_category_open_full_time": true,
      "day": 0,
      "day_time": []
    },
    {
      "is_category_open": true,
      "is_category_open_full_time": true,
      "day": 1,
      "day_time": []
    },
    {
      "is_category_open": true,
      "is_category_open_full_time": true,
      "day": 2,
      "day_time": []
    },
    {
      "is_category_open": true,
      "is_category_open_full_time": true,
      "day": 3,
      "day_time": []
    },
    {
      "is_category_open": true,
      "is_category_open_full_time": true,
      "day": 4,
      "day_time": []
    },
    {
      "is_category_open": true,
      "is_category_open_full_time": true,
      "day": 5,
      "day_time": []
    }, {
      "is_category_open": true,
      "is_category_open_full_time": true,
      "day": 6,
      "day_time": []
    }
  ];
  addSlotTime = {
    0: false,
    1: false,
    2: false,
    3: false,
    4: false,
    5: false,
    6: false
  }
  subcategories = [];
  categoryForm:UntypedFormGroup;
  is_edit:boolean = false;
  edit_id:string = null;
  imagefile;
  imageurl:any = this._helper.DEFAULT_IMAGE_PATH.CATEGORY;
  image_settings:ImageSetting;

  openTime: any;
  closeTime: any;
  category_time_error = 0;
  openTimeValidate: boolean = false;
  closeTimeValidate: boolean = false;

  @Output() updateData = new EventEmitter<any>();
  @ViewChild('cropModel', { static: true }) cropModel: ImageCropModelComponent;
  @ViewChild('template', { static: true }) template: TemplateRef<any>;
  @ViewChild('form') form: NgForm;

  constructor(private modalService: BsModalService,
    private _subcategoryService:SubcategoryService,
    private _commonService:CommonService,
    private _helper:Helper,
    private _lang:LangService,
    private _categoryService:CategoryService,
    private notifications: NotifiyService,
    ) { }

  ngOnInit(){
    this._initForm();
  }

 

  _initForm(){
    this.categoryForm = new UntypedFormGroup({
      name:new UntypedFormArray([]),
      product_ids:new UntypedFormControl([],Validators.required),
      sequence_number:new UntypedFormControl(null,Validators.required),
      is_visible_in_store: new UntypedFormControl(false)
    })
    this._lang.supportedLanguages.forEach(_language=>{
      this.name.push(new UntypedFormControl(null,_language.code === 'en' ? Validators.required : null))
    })
  }

  onSelectImageFile(event) {
    let files = event.target.files;
    if (files.length === 0){
      return;
    }

    const mimeType = files[0].type;
    let fileType=this._helper.uploadFile.filter((element)=> {
      return mimeType==element;
    })
    if (mimeType != fileType) {
      this.notifications.showNotification('error',this._helper._trans.instant('validation-title.invalid-image-format'));
      return;
    }
    let aspectRatio = this.image_settings.product_image_ratio;
    let resizeToWidth = this.image_settings.product_image_max_width;
    this.cropModel.imageChangedEvent = event;
    this.cropModel.show(aspectRatio,resizeToWidth);
  }
  get name() {
    return this.categoryForm.get('name') as UntypedFormArray;
  }
  imageCropped(event) {
    this.imagefile = event
    const reader = new FileReader();
    reader.readAsDataURL(this.imagefile);
    reader.onload = (_event) => {
      this.imageurl = reader.result;
    }
  }

  show(is_edit,edit_id,next_sequence_number = 0): void {

    this.is_edit  = is_edit;
    this.edit_id  = edit_id;
    this.category_time = this.time
    this.addSlotTime = {
      0: false,
      1: false,
      2: false,
      3: false,
      4: false,
      5: false,
      6: false
    }
    this._initData();
    if(is_edit && edit_id !== null){
      this._categoryService.fetch(this.edit_id).then(data=>{
        if(data.success){
          let editData = data.product_group;
          if (data.product_group.category_time) {
            this.category_time = data.product_group.category_time;
            this.calcategoryTime(this.category_time)
          }
          if (this._helper.delivery_type === DELIVERY_TYPE.SERVICE || this._helper.delivery_type === DELIVERY_TYPE.APPOINTMENT) {
            this.edit_id = editData._id;
          } else {
            this.edit_id = edit_id;
          }          
          this.name.patchValue(editData.name)
          this.categoryForm.patchValue({
            sequence_number: editData.sequence_number,
            product_ids : editData.product_ids,
            is_visible_in_store : editData.is_visible_in_store
          })
          this.imageurl = environment.imageUrl + editData.image_url;
          this.modalRef = this.modalService.show(this.template, this.config);
        }
      })
    }else{
      this.resetForm()
      this.categoryForm.patchValue({
        sequence_number:next_sequence_number
      })
      this.modalRef = this.modalService.show(this.template, this.config);
    }
  }

  _initData(){
    this._subcategoryService.list(this._helper.selected_store_id).then(res_data=>{
      if(res_data.success){
        this.subcategories = res_data.product_array
      }
    })
    this._commonService.fetch_image_settings().then(res_data=>{
      if(res_data.success){
        this.image_settings  = res_data.image_setting
      }
    })
  }
  addNewSlot(index) {
    this.openTimeValidate = false;
    this.closeTimeValidate = false;
    this.addSlotTime = {
      0: false,
      1: false,
      2: false,
      3: false,
      4: false,
      5: false,
      6: false
    }
    if (this.addSlotTime[index] === true) {
      this.addSlotTime[index] = false
    } else {
      this.addSlotTime[index] = true
    }
  }

  saveSlot(index) {
    if (!this.openTime || !this.closeTime) {
      if (!this.openTime) {
        this.openTimeValidate = true;
      }
      if (!this.closeTime) {
        this.closeTimeValidate = true;
      }
      return;
    } else {
      this.openTimeValidate = false;
      this.closeTimeValidate = false;
    }
    
    if ((this.openTime < this.closeTime) && this.closeTime && this.openTime) {
      
      let openHour = this.pad2((new Date(this.openTime.getTime())).getHours());
      let closeHour = this.pad2((new Date(this.closeTime.getTime())).getHours());
      let openMinute = this.pad2((new Date(this.openTime.getTime())).getMinutes());
      let closeMinute = this.pad2((new Date(this.closeTime.getTime())).getMinutes());
      let open_am_pm = ""
      let close_am_pm = ""

      if ((new Date(this.openTime.getTime())).getHours() == 12) {
        open_am_pm = this.pad2((new Date(this.openTime.getTime())).getHours()) + ':' + openMinute + " PM";
      } else if ((new Date(this.openTime.getTime())).getHours() > 12) {
        open_am_pm = this.pad2((new Date(this.openTime.getTime())).getHours() - 12) + ':' + openMinute + " PM";
      } else if ((new Date(this.openTime.getTime())).getHours() == 0) {
        open_am_pm = this.pad2((new Date(this.openTime.getTime())).getHours() + 12) + ':' + openMinute + " AM";
      } else {
        open_am_pm = openHour + ':' + openMinute + " AM";
      }
      if ((new Date(this.closeTime.getTime())).getHours() == 12) {
        close_am_pm = this.pad2((new Date(this.closeTime.getTime())).getHours()) + ':' + closeMinute + " PM";
      } else if ((new Date(this.closeTime.getTime())).getHours() > 12) {
        close_am_pm = this.pad2((new Date(this.closeTime.getTime())).getHours() - 12) + ':' + closeMinute + " PM";
      } else if ((new Date(this.closeTime.getTime())).getHours() == 0) {
        close_am_pm = this.pad2((new Date(this.closeTime.getTime())).getHours() + 12) + ':' + closeMinute + " AM";
      } else {
        close_am_pm = closeHour + ':' + closeMinute + " AM";
      }

      this.category_time_error = 0
      let open = new Date(new Date().setHours(Number(openHour), Number(openMinute), 0, 0)).getTime();
      let close = new Date(new Date().setHours(Number(closeHour), Number(closeMinute), 0, 0)).getTime();

      this.category_time[index].day_time.forEach(time => {
        let open_time = time.category_open_time.split(" ");
        let close_time = time.category_close_time.split(" ");
        let open_time1 = open_time[0].split(':')
        let close_time1 = close_time[0].split(':')

        let start_hour;
        if (open_time[1] === "AM") {
          if (open_time1[0] === 12) {
            start_hour = 0;
          } else {
            start_hour = open_time1[0];
          }
        } else {
          if (open_time1[0] === 12) {
            start_hour = 12;
          } else {
            start_hour = open_time1[0] + 12;
          }
        }

        let close_hour;
        if (close_time[1] === "AM") {
          if (close_time1[0] === 12) {
            close_hour = 0;
          } else {
            close_hour = close_time1[0];
          }
        } else {
          if (close_time1[0] === 12) {
            close_hour = 12;
          } else {
            close_hour = close_time1[0] + 12;
          }
        }

        let start_min = Number(open_time1[1]);
        let close_min = Number(close_time1[1])

        let open_date_time = new Date(new Date().setHours(start_hour, start_min, 0, 0)).getTime();
        let close_date_time = new Date(new Date().setHours(close_hour, close_min, 0, 0)).getTime();

        if ((open_date_time <= open) && (open <= close_date_time)) {
          this.category_time_error = 1
          console.log("ef");
        } else if (open <= open_date_time && close_date_time <= close) {
          console.log("esf");
          this.category_time_error = 1
        } else if ((open_date_time <= close) && (close <= close_date_time)) {
          console.log("efdf");
          this.category_time_error = 1
        }
      });

      if (!this.category_time_error) {
        this.addSlotTime[index] = false;
        this.category_time[index].day_time.push({ category_open_time: open_am_pm, category_close_time: close_am_pm })
        this.openTime = undefined;
        this.closeTime = undefined;
      }
    }
  }
  pad2(number) {
    return (number < 10 ? '0' : '') + number
  }
  onCheck(slot, slot_index, day_index, event) {
    this.category_time[day_index].day_time.splice(slot_index, 1);
  }
 async onSubmit(){
  this._helper.checkAndCleanFormValues(this.categoryForm);
  let delivery_type = this._helper.delivery_type
  let slot_invalid:boolean = false;
  this.category_time.forEach((day) => {
    if(!day.is_category_open_full_time && day.day_time?.length == 0){
      slot_invalid = true;
    }
  })
  if(slot_invalid){
    this.notifications.showNotification("error",this._helper._trans.instant('validation-title.please-add-at-least-one-slot-time'))
    return
  }
   this.categoryForm.markAllAsTouched();
   let array: any = [];
   this.category_time.forEach((time) => {
     array.push({
       "is_category_open": !!time.is_category_open,
       "is_category_open_full_time": !!time.is_category_open_full_time,
       "day": time.day,
       "day_time": []
     });
     time.day_time.forEach((day_time) => {
       let open_am_pm = day_time.category_open_time.split(" ");
       let close_am_pm = day_time.category_close_time.split(" ");
       let open_minute = open_am_pm[0].split(":");
       let close_minute = close_am_pm[0].split(":");

       if (open_am_pm[1] == "PM" && Number(open_minute[0]) != 12) {
         open_minute[0] = Number(open_minute[0]) + 12
       }
       if (open_am_pm[1] == "AM" && Number(open_minute[0]) == 12) {
         open_minute[0] = Number(open_minute[0]) - 12
       }
       if (close_am_pm[1] == "PM" && Number(close_minute[0]) != 12) {
         close_minute[0] = Number(close_minute[0]) + 12
       }
       if (close_am_pm[1] == "AM" && Number(close_minute[0]) == 12) {
         close_minute[0] = Number(close_minute[0]) - 12
       }

       array[array.length - 1].day_time.push({
         "category_open_time": (Number(open_minute[0]) * 60 + Number(open_minute[1])),
         "category_close_time": (Number(close_minute[0]) * 60 + Number(close_minute[1]))
       });

     })
   })
  if(this.categoryForm.invalid){
    this.categoryForm.markAllAsTouched();
    return;
  }
    if(this.categoryForm.valid){
      let formData = new FormData();
      if(this.categoryForm.value.is_visible_in_store == null){
        this.categoryForm.value.is_visible_in_store = false
      }
      formData.append('delivery_type', delivery_type.toString());
      formData.append('name',JSON.stringify(this.categoryForm.value.name))
      formData.append('product_ids',this.categoryForm.value.product_ids.toString())
      formData.append('sequence_number',this.categoryForm.value.sequence_number.toString())
      formData.append('category_time', JSON.stringify(array));
      formData.append('is_visible_in_store',this.categoryForm.value.is_visible_in_store)
      if(this.imagefile){
        formData.append('image_url',this.imagefile)
      }
      if(this.is_edit){
        formData.append('product_group_id',this.edit_id.toString())
        await this._categoryService.update(formData)
        this.updateData.emit()
      }else{
        await this._categoryService.add(formData)
        this.updateData.emit()
      }
      this.closeCategoryPopup()
      this.resetForm()
    }else{
      this.categoryForm.markAllAsTouched()
    }
  }
  calcategoryTime(data) {
    this.category_time = []
    data.forEach((category_time) => {
      this.category_time.push({
        "is_category_open": category_time.is_category_open,
        "is_category_open_full_time": category_time.is_category_open_full_time,
        "day": category_time.day,
        "day_time": [],
      });
      category_time.day_time.forEach((day_time) => {

        let category_open_string = this.pad2(Math.floor(day_time.category_open_time / 60)) + ":" + this.pad2(Math.floor(day_time.category_open_time % 60));
        let category_close_string = this.pad2(Math.floor(day_time.category_close_time / 60)) + ":" + this.pad2(Math.floor(day_time.category_close_time % 60));
        if (Math.floor(day_time.category_open_time / 60) > 12) {
          category_open_string = this.pad2(Number(category_open_string.split(":")[0]) - 12) + ':' + category_open_string.split(":")[1] + ' PM';
        } else if (Math.floor(day_time.category_open_time / 60) == 12) {
          category_open_string = this.pad2(Number(category_open_string.split(":")[0])) + ':' + category_open_string.split(":")[1] + ' PM';
        } else if (Math.floor(day_time.category_open_time / 60) == 0) {
          category_open_string = this.pad2(Number(category_open_string.split(":")[0]) + 12) + ':' + category_open_string.split(":")[1] + ' AM';
        } else {
          category_open_string = category_open_string + ' AM';
        }
        if (Math.floor(day_time.category_close_time / 60) > 12) {
          category_close_string = this.pad2(Number(category_close_string.split(":")[0]) - 12) + ':' + category_close_string.split(":")[1] + ' PM';
        } else if (Math.floor(day_time.category_close_time / 60) == 12) {
          category_close_string = this.pad2(Number(category_close_string.split(":")[0])) + ':' + category_close_string.split(":")[1] + ' PM';
        } else if (Math.floor(day_time.category_close_time / 60) == 0) {
          category_close_string = this.pad2(Number(category_close_string.split(":")[0]) + 12) + ':' + category_close_string.split(":")[1] + ' AM';
        } else {
          category_close_string = category_close_string + ' AM';
        }

        this.category_time[this.category_time.length - 1].day_time.push({
          "category_open_time": category_open_string,
          "category_close_time": category_close_string
        });
      })
    })
  }
  resetForm(){
    this.imagefile = undefined;
    this.categoryForm.reset()
    this.imageurl = this._helper.DEFAULT_IMAGE_PATH.CATEGORY;
    this.category_time = this.time;
  }

  closeCategoryPopup(){
    this.modalRef.hide();
    this.category_time = null;
  }

  onChangeSlotTime(item){
    item.is_category_open_full_time = !item.is_category_open;
  }

  onChangeOpenFullDay(item){
    item.is_category_open = !item.is_category_open_full_time;
  }

}

