import { Component, EventEmitter, Output, TemplateRef, ViewChild } from '@angular/core';
import { UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { CommonService } from 'src/app/services/common.service';
import { CHARGES_TYPE } from 'src/app/shared/constant';
import { Helper } from '../../../shared/helper';

@Component({
    selector:'delivery-price-not-set-modal',
    templateUrl:'./delivery-price-not-set-modal.component.html'
})
export class DeliveryPriceNotSetModal {

    modalRef: BsModalRef;
    config = {
      backdrop: true,
      ignoreBackdropClick: false,
      class: 'modal-popup'
    };
    city: '';
    delivery: '';

    CHARGES_TYPE = CHARGES_TYPE
    serviceForm: UntypedFormGroup;
    is_edit: boolean = false;
    edit_service_charge_id;
    fromState = [
      { "id": 11, "title": "button-title.accepted" },
      { "id": 13, "title": "button-title.coming" },
      { "id": 15, "title": "button-title.arrived" },
      { "id": 19, "title": "button-title.started" }
    ]
  
    toState = [
      { "id": 11, "title": "button-title.accepted" },
      { "id": 13, "title": "button-title.coming" },
      { "id": 15, "title": "button-title.arrived" },
      { "id": 19, "title": "button-title.started" }
    ]
    temptoStoreStatues = this.toState
    tempfromStoreStatues = this.fromState

    @ViewChild('template', { static: true }) template: TemplateRef<any>;

    @Output() amount = new EventEmitter<any>();
  route: any;
  serviceData:any;

  constructor(private modalService: BsModalService, private _commonService: CommonService, private _router: Router, public _helper: Helper) { }

  _initForm(data){
    this.serviceForm = new UntypedFormGroup({
        city_id: new UntypedFormControl(data.city._id, Validators.required),
        delivery_id: new UntypedFormControl(data.delivery._id, Validators.required),
        country_id: new UntypedFormControl(data.city.country_id, Validators.required),
        admin_profit_type: new UntypedFormControl(1, Validators.required),
        admin_profit: new UntypedFormControl(0, Validators.required),
        cancellation_charge_type: new UntypedFormControl(1,Validators.required),
        cancellation_charge: new UntypedFormControl(0, Validators.required),
        cancellation_charge_apply_from: new UntypedFormControl(11, Validators.required),
        cancellation_charge_apply_till: new UntypedFormControl(19, Validators.required)
    })
  }

  show(data){
    this.serviceData = data;
    this.route = data.route
    this._initForm(data)
    this._commonService.getServiceCharges({ city_id: data.city._id, delivery_id: data.delivery._id }).then(result => {
      if (result.success) {
        this.is_edit = true
        this.edit_service_charge_id = result.service_charge._id
        this.serviceForm.patchValue({
          ...result.service_charge
        })
      } else {
        this.is_edit = false;
      }
      this.city = data.city.city_name
      this.delivery = data.delivery.delivery_details.delivery_name[0]
    })
    this.modalRef = this.modalService.show(this.template, this.config)
  }

  onFromStateSelect(event){
    this.toState = this.temptoStoreStatues.filter(x => x.id > event.id)
  }
  
  onToStateSelect(event){
    this.fromState = this.tempfromStoreStatues.filter(x => x.id < event.id)
  }

  confirm(){
    if(!this.is_edit){
      this._commonService.addServiceCharges(this.serviceForm.value).then(result => {
        if(result.success){
          this.modalRef.hide()
          this.serviceForm.reset()
        }
      })
    } else {
      let json = {
        _id: this.edit_service_charge_id,
        ...this.serviceForm.value
      }
      this._commonService.updateServiceCharges(json).then(result => {
        if(result.success){
          this.modalRef.hide();
          this.serviceForm.reset();
        }
      })
    }
  }

  setPrice(){
    this._helper.devliverySetfees = this.serviceData.delivery;
    this._router.navigate(['/app/delivery-info/admin-service'])
   
    this.modalRef.hide();
  }

}
