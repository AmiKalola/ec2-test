import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AddNewCancellationReasonComponent } from './add-new-cancellation-reason-modal.component';

describe('AddNewCancellationReasonComponent', () => {
  let component: AddNewCancellationReasonComponent;
  let fixture: ComponentFixture<AddNewCancellationReasonComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AddNewCancellationReasonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddNewCancellationReasonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
