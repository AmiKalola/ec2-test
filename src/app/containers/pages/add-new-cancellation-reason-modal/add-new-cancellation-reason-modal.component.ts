import { Component, TemplateRef, ViewChild } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { UntypedFormArray, UntypedFormGroup, UntypedFormControl, Validators } from '@angular/forms';
import { CommonService } from '../../../services/common.service';
import { CancellationReasonService } from '../../../services/cancellation-reason-service';
import { Helper } from 'src/app/shared/helper';
import { LangService } from 'src/app/shared/lang.service';

@Component({
  selector: 'app-add-new-cancellation-reason-modal',
  templateUrl: './add-new-cancellation-reason-modal.component.html',
  styleUrls: ['./add-new-cancellation-reason-modal.component.scss']
})
export class AddNewCancellationReasonComponent {
  modalRef: BsModalRef;
  config = {
    backdrop: true,
    ignoreBackdropClick: true,
    class: 'modal-right'
  };

  cancellationReasonForm: UntypedFormGroup
  formData: FormData;
  is_edit: boolean = false

  user_type: any = this._helper.ADMIN_DATA_ID.USER;
  deliveries_type_list: any = [];

  @ViewChild('template', { static: true }) template: TemplateRef<any>;

  constructor(private modalService: BsModalService,
    private _commonService: CommonService,
    private _cancellationReasonService: CancellationReasonService,
    public _helper: Helper,
    public _lang: LangService) { }


  show(reason_id = null, user_type = null): void {
    this.modalRef = this.modalService.show(this.template, this.config);
    this.formData = new FormData()
    this.user_type = user_type;
    this.deliveries_type_list = JSON.parse(JSON.stringify(this._helper.DELIVERY_TYPE));
    this._initForm()
    if (reason_id) {
      this.is_edit = true
      this.getCancellationReasons(reason_id)
    }
    if(this.is_edit){
      if(!this._helper.has_permission(this._helper.PERMISSION.EDIT)){
        this.cancellationReasonForm.disable();
      }
    }else{
      this.cancellationReasonForm.enable();
    }
  }

  _initForm() {
    this.cancellationReasonForm = new UntypedFormGroup({
      reason: new UntypedFormArray([]),
      reason_id: new UntypedFormControl(null),
      user_type: new UntypedFormControl(this.user_type),
      delivery_type: new UntypedFormControl(null, [Validators.required]),
    })

    this.cancellationReasonForm.value.user_type = this.user_type;
    this._lang.supportedLanguages.forEach(_language => {
      if (_language.code == "en") {
        this.reason.push(new UntypedFormControl("", Validators.required))
      } else {
        this.reason.push(new UntypedFormControl(null))
      }
    })

  }

  get reason() {
    return this.cancellationReasonForm.get('reason') as UntypedFormArray;
  }

  save() {
    this._helper.checkAndCleanFormValues(this.cancellationReasonForm)
    if (this.cancellationReasonForm.valid) {

      if (!this.cancellationReasonForm.value.reason_id) {
        this._cancellationReasonService.addCancellationReason(this.cancellationReasonForm.value)
      } else {
        this.formData.append('reason_id', this.cancellationReasonForm.value.reason_id)
        this._cancellationReasonService.updateCancellationReason(this.cancellationReasonForm.value)
      }

      this.modalRef.hide()
      this.cancellationReasonForm.reset()
    }else{
      this.cancellationReasonForm.markAllAsTouched();
    }
  }

  getCancellationReasons(reason_id) {
    this._cancellationReasonService.getCancellationReasonDetail({ reason_id }).then(res => {
      if (res.success) {
        this.cancellationReasonForm.patchValue({
          ...res.cancellation_reason,
          reason_id: res.cancellation_reason._id,
        })
      }
    })
  }

  close() {
    this.is_edit = false
    this.modalRef.hide()
    this.cancellationReasonForm.reset()
  }
}
