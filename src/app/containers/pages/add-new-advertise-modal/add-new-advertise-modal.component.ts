import { Component, TemplateRef, ViewChild } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { CountryService } from 'src/app/services/country.service';
import { DeliveryFeesService } from 'src/app/services/delivery-fees.service';
import { StoreService } from 'src/app/services/store.service';
import { environment } from 'src/environments/environment';
import { UntypedFormGroup, UntypedFormControl, Validators } from '@angular/forms';
import { ImageCropModelComponent } from '../image-crop-model/image-crop-model.component';
import { CommonService } from '../../../services/common.service';
import { AdvertiseService } from '../../../services/advertise.service';
import { Helper } from 'src/app/shared/helper';
import { LangService } from 'src/app/shared/lang.service';
import { DeliveryService } from 'src/app/services/delivery.service';
import { NotifiyService } from 'src/app/services/notifier.service';

@Component({
  selector: 'app-add-new-advertise-modal',
  templateUrl: './add-new-advertise-modal.component.html',
  styleUrls: ['./add-new-advertise-modal.component.scss']
})
export class AddNewAdvertiseModalComponent  {
  modalRef: BsModalRef;
  config = {
    backdrop: true,
    ignoreBackdropClick: true,
    class: 'modal-right'
  };

  delivery_list = []
  delivery_type_list = []
  country_list = []
  city_list = []
  store_list = []
  temp_store_list = []
  advertiseForm: UntypedFormGroup
  image_settings: any
  formData: FormData;
  date = new Date()
  is_edit: boolean = false
  image_url = environment.imageUrl
  default_image = '/assets/img/details/1.jpg'
  todayDate = new Date()

  @ViewChild('template', { static: true }) template: TemplateRef<any>;
  @ViewChild('cropModel', { static: true }) cropModel: ImageCropModelComponent;

  constructor(private modalService: BsModalService,
    private _deliveryService: DeliveryService,
    private _countryService: CountryService,
    private _deliveryFeesService: DeliveryFeesService,
    private _storeService: StoreService,
    private _commonService: CommonService,
    private _advertiseService: AdvertiseService,
    public _helper: Helper,
    public _lang: LangService,
    private notifications: NotifiyService
    ) { }

  show(edit_advertise_id = null): void {
    this.modalRef = this.modalService.show(this.template, this.config);
    this.formData = new FormData()
    this.country_list = []
    this.city_list = []
    this.store_list = []
    this._initForm()
    this.getCountryList()
    this.fetch_image_settings()
    if (edit_advertise_id) {
      this.is_edit = true
      this.getAdvertise(edit_advertise_id)
    }
    if(this.is_edit){
      if(!this._helper.has_permission(this._helper.PERMISSION.EDIT)){
        this.advertiseForm.disable();
      }
    }else{
      this.advertiseForm.enable();
    }
  }

  _initForm() {
    this.advertiseForm = new UntypedFormGroup({
      advertise_id: new UntypedFormControl(null),
      image_url: new UntypedFormControl(null),
      country_id: new UntypedFormControl(null, Validators.required),
      ads_detail: new UntypedFormControl(null, Validators.required),
      city_id: new UntypedFormControl(null, Validators.required),
      delivery_type: new UntypedFormControl(null, Validators.required),
      delivery_type_id: new UntypedFormControl([], Validators.required),
      is_ads_visible: new UntypedFormControl(false, Validators.required),
      is_ads_approve_by_admin: new UntypedFormControl(true, Validators.required),
      is_ads_have_expiry_date: new UntypedFormControl(false, Validators.required),
      expiry_date: new UntypedFormControl(this.todayDate),
      is_ads_redirect_to_store: new UntypedFormControl(false, Validators.required),
      store_id: new UntypedFormControl()
    })

    this.advertiseForm.get('is_ads_redirect_to_store').valueChanges
    .subscribe(value => {
      if(value === true) {
        this.advertiseForm.get('store_id').setValidators(Validators.required)
        this.advertiseForm.get('store_id').updateValueAndValidity();
      } else {
        this.advertiseForm.get('store_id').setValidators(null);
        this.advertiseForm.get('store_id').updateValueAndValidity();
      }
    }
);
}

  onChange()
  {
    if(this.advertiseForm.value.is_ads_redirect_to_store){
      this.advertiseForm.get('store_id').setValidators([null,Validators.required])
    }
  }

  save() {
    this._helper.checkAndCleanFormValues(this.advertiseForm);
    if(this.advertiseForm.invalid){
      this.advertiseForm.markAllAsTouched();
      return;
    }
    if (this.advertiseForm.valid) {
      if (this.advertiseForm.value.country_id) {
        this.formData.append('country_id', this.advertiseForm.getRawValue().country_id)
      }
      if (this.advertiseForm.value.city_id) {
        this.formData.append('city_id', this.advertiseForm.getRawValue().city_id)
      }
      if (this.advertiseForm.value.delivery_type_id) {
        this.formData.append('delivery_type_id', this.advertiseForm.getRawValue().delivery_type_id)
      }
      this.formData.append('ads_detail', this.advertiseForm.value.ads_detail)
      this.formData.append('delivery_type', this.advertiseForm.getRawValue().delivery_type)
      this.formData.append('is_ads_visible', this.advertiseForm.value.is_ads_visible)
      this.formData.append('is_ads_approve_by_admin', this.advertiseForm.value.is_ads_approve_by_admin)
      this.formData.append('is_ads_have_expiry_date', this.advertiseForm.value.is_ads_have_expiry_date)
      if (this.advertiseForm.value.is_ads_have_expiry_date) {
        let expiry_date = new Date(this.advertiseForm.value.expiry_date)
        let date_string = (expiry_date.getMonth() + 1) + '-' + expiry_date.getDate() + '-' + expiry_date.getFullYear()
        this.formData.append('expiry_date', date_string)
      }
      this.formData.append('is_ads_redirect_to_store', this.advertiseForm.value.is_ads_redirect_to_store)
      if (this.advertiseForm.value.is_ads_redirect_to_store) {
        this.formData.append('store_id', this.advertiseForm.value.store_id)
      }
      if (!this.advertiseForm.value.advertise_id) {
        this._advertiseService.addAdvertise(this.formData)
      } else {
        this.formData.append('advertise_id', this.advertiseForm.value.advertise_id)
        this._advertiseService.updateAdvertise(this.formData)
      }
      this.modalRef.hide()
      this.advertiseForm.reset()
    }
  }

  changeStatus() {
    this.formData.append('advertise_id', this.advertiseForm.value.advertise_id)
    this.formData.append('is_ads_approve_by_admin', String((!this.advertiseForm.value.is_ads_approve_by_admin)))
    this._advertiseService.updateAdvertise(this.formData)
    this.modalRef.hide()
    this.advertiseForm.reset()
  }

  getDeliveryList(){
    this._deliveryService.get_delivery_list().then(res_data => {
      if(res_data.success){
        this.delivery_type_list = res_data.deliveries
      }
    })
  }

  getCountryList() {
    this._countryService.fetch().then(res => {
      if (res.success) {
        this.country_list = res.countries
      }
    })
  }

  getCityList() {
    if(!this.is_edit){
      this.delivery_list = [];
      this.delivery_type_list = []
    }
    if (!this.advertiseForm.value.advertise_id && !this.is_edit) {
      this.advertiseForm.patchValue({
        delivery_type: null,
        delivery_type_id: [],
      })
    }
    this._deliveryFeesService.get_city_lists(this.advertiseForm.value.country_id).then(res => {
      this.city_list = []
      if (res.success) {
        this.city_list = res.cities;
        if(!this.is_edit){
        this.advertiseForm.patchValue({
          city_id: "000000000000000000000000"
        })
        }
        this.delivery_list = [
          { value: 1, title: 'label-title.store' },
          { value: 5, title: 'label-title.service' },
          { value: 6, title: 'label-title.appoinment' }
        ]
      }else{
        this.advertiseForm.patchValue({
          city_id: null
        })
      }
    })
  }


  getStoreListForCountry() {
    if (!this.advertiseForm.value.advertise_id) {
      this.advertiseForm.patchValue({
        store_id: null
      })
    }
    this._storeService.getStoreListForCountry({ country_id: this.advertiseForm.value.country_id }).then(res => {
      this.store_list = []
      if (res.success) {
        res.stores_all.map((data) => {
          if (data.delivery_type !== 4) {
            this.store_list.push(data)
          }
        })
        this.temp_store_list = this.store_list; 
      }
    })
  }

  getStoreListForCity() {
    if(!this.is_edit){
    this.advertiseForm.patchValue({
      store_id: null,
      delivery_type: null,
      delivery_type_id: [],
    })
    }
    if (!this.advertiseForm.value.advertise_id) {
      this.advertiseForm.patchValue({
        store_id: null
      })
    }
    this._storeService.getStoreListForCity({ city_id: this.advertiseForm.value.city_id }).then(res => {
      this.store_list = []
      if (res.success) {
        res.stores.map((data) => {
          if (data.delivery_type !== 4) {
            this.store_list.push(data)
          }
        })
        this.temp_store_list = this.store_list; 
      }
      if (this.advertiseForm.value.advertise_id) {
        this.advertiseForm.controls.country_id.disable()
        this.advertiseForm.controls.city_id.disable()
        this.advertiseForm.controls.delivery_type.disable()
        this.advertiseForm.controls.delivery_type_id.disable()
      }
    })
  }
  
  getAdvertise(advertise_id) {
    this._advertiseService.getAdvertiseDetails({ advertise_id }).then(res => {
      if (res.success) {
        this.advertiseForm.patchValue({
          ...res.advertise,
          advertise_id: res.advertise._id,
          image_url: this.image_url + res.advertise.image_url
        })
        if(res.advertise.delivery_type == 0){
          this.advertiseForm.patchValue({ delivery_type: '000000000000000000000000' }) 
        }
        if (res.advertise.expiry_date) {
          this.advertiseForm.patchValue({
            expiry_date: new Date(res.advertise.expiry_date)
          })
        }
        this.getCityList()
        this.getStoreListForCountry()
        this.getStoreListForCity()
        let json = { country_id: this.advertiseForm.value.country_id, city_id: this.advertiseForm.value.city_id, delivery_type: this.advertiseForm.value.delivery_type }
        this._storeService.getStoreListForAds(json).then(res => {
          if(!this.is_edit){
            this.advertiseForm.patchValue({ delivery_type_id: ["000000000000000000000000"] }) 
          }
          this.delivery_type_list = res?.delivery;
        })
        this.advertiseForm.patchValue({
          store_id: res?.advertise?.store_details[0]?._id
        })
      }
    })
  }
  
  fetch_image_settings() {
    this._commonService.fetch_image_settings().then(res_data => {
      if (res_data.success) {
        this.image_settings = res_data.image_setting
      }
    })
  }
  
  onSelectImageFile(event) {
    let files = event.target.files;
    if (files.length === 0)
    return;
  
  const mimeType = files[0].type;
  let fileType = this._helper.uploadFile.filter((element)=> {
    return mimeType==element;
  })
  if (mimeType != fileType) {
    this.notifications.showNotification('error',this._helper._trans.instant('validation-title.invalid-image-format'));
    return;
  }
  
  const aspectRatio = this.image_settings.ads_banner_image_ratio;
  const resizeToWidth = this.image_settings.ads_banner_image_max_width;
  this.cropModel.imageChangedEvent = event;
  this.cropModel.show(aspectRatio, resizeToWidth);
}

imageCropped(event) {
  const reader = new FileReader();
  reader.readAsDataURL(event);
  reader.onload = (_event) => {
    this.advertiseForm.patchValue({ image_url: reader.result })
  }
  this.formData.append('image_url', event)
}

close() {
  this.is_edit = false
  this.modalRef.hide()
  this.advertiseForm.reset()
  this.delivery_list = []
}

resetDeliveryTypeId() {
  if (this.advertiseForm.get('delivery_type').value) {
    this.advertiseForm.patchValue({ delivery_type_id: ["000000000000000000000000"] })
  } else {
    this.advertiseForm.patchValue({ delivery_type_id: [] })
  }
  this._storeService.getStoreListForAds({ country_id: this.advertiseForm.value.country_id, city_id: this.advertiseForm.value.city_id, delivery_type: this.advertiseForm.value.delivery_type }).then(res => {
    if(res.success && res?.delivery.length>0){
      this.advertiseForm.patchValue({ delivery_type_id: ["000000000000000000000000"] }) 
      this.delivery_type_list = res?.delivery;
    }else{
      this.advertiseForm.patchValue({ delivery_type_id: [] }) 
    }
  })
}

selectDeliveryTypeId(){
  let idx = this.advertiseForm.get('delivery_type_id').value.findIndex(i => i == "000000000000000000000000")
  if (idx != -1) {
    this.advertiseForm.patchValue({ delivery_type_id: ["000000000000000000000000"] })
  }
}

}
