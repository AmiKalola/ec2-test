import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AddNewAdvertiseModalComponent } from './add-new-advertise-modal.component';

describe('AddNewAdvertiseModalComponent', () => {
  let component: AddNewAdvertiseModalComponent;
  let fixture: ComponentFixture<AddNewAdvertiseModalComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AddNewAdvertiseModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddNewAdvertiseModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
