import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DeliverymanEarningDetailComponent } from './deliveryman-earning-detail.component';

describe('DeliverymanEarningDetailComponent', () => {
  let component: DeliverymanEarningDetailComponent;
  let fixture: ComponentFixture<DeliverymanEarningDetailComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ DeliverymanEarningDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeliverymanEarningDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
