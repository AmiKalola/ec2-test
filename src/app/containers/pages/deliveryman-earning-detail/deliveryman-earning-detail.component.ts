import { Component, TemplateRef,  ViewChild } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { EarningService } from 'src/app/services/earning.service';
import { Helper } from 'src/app/shared/helper';

@Component({
  selector: 'app-deliveryman-earning-detail',
  templateUrl: './deliveryman-earning-detail.component.html',
})
export class DeliverymanEarningDetailComponent  {
  modalRef: BsModalRef;
  config = {
    backdrop: true,
    ignoreBackdropClick: true,
    class: 'modal-right'
  };
  orderPayment;
  start_date;
  end_date;
  provider_name;
  provider:any;
  @ViewChild('template', { static: true }) template: TemplateRef<any>;

  constructor(private modalService: BsModalService,private _earningService:EarningService,public _helper:Helper) { }


  show(provider_id,start_date=null,end_date=null): void {
    this.start_date = start_date;
    this.end_date = end_date;
    this._earningService.fetch_deliveryman_earning({provider_id:provider_id,start_date:start_date,end_date:end_date,page:1,search_field:'',search_value:'',type:1}).then(data=>{
      if(data['success']){
        this.provider = data['provider'];
        this.orderPayment = data['order_total'];
        this.provider_name = data['provider'].first_name + " " + data['provider'].last_name;
        this.modalRef = this.modalService.show(this.template, this.config);
      }
    })
  }


  closeModel(){
    this.modalRef.hide();
  }

}
