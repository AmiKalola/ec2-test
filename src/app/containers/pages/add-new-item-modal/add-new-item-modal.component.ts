import { Component, ElementRef, Input,  TemplateRef, ViewChild } from '@angular/core';
import { UntypedFormArray, UntypedFormControl, UntypedFormGroup, NgForm, Validators } from '@angular/forms';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { ImageSetting } from 'src/app/models/image_setting.model';
import { CommonService } from 'src/app/services/common.service';
import { ItemService } from 'src/app/services/item.service';
import { ModifierService } from 'src/app/services/modifier.service';
import { SubcategoryService } from 'src/app/services/subcategory.service';
import { LangService } from 'src/app/shared/lang.service';
import { environment } from 'src/environments/environment';
import { ImageCropModelComponent } from '../image-crop-model/image-crop-model.component';
import { Helper } from 'src/app/shared/helper';
import { SpecificationWarningModelComponent } from '../specification-warning-modal/specification-warning-modal.component';
import { CONSTANT, DELIVERY_TYPE } from '../../../views/app/constant'
import { NotifiyService } from 'src/app/services/notifier.service';
import { WizardComponent as ArcWizardComponent } from 'angular-archwizard';

@Component({
  selector: 'app-add-new-item-modal',
  templateUrl: './add-new-item-modal.component.html',
  styleUrls: ["./add-new-item-modal.component.scss"]
})
export class AddNewItemModalComponent {
  modalRef: BsModalRef;
  config = {
    backdrop: true,
    ignoreBackdropClick: true,
    class: 'modal-right'
  }; 
  @ViewChild('form') form: NgForm;
  @ViewChild('wizard') wizard: ArcWizardComponent;

  isAddModifier = false;
  isHideMaxRange = true;
  isModifierSelected = false;
  edit_id_item_modifier = null;
  modifierType: number = 1;
  isModifierError: boolean = false
  isModifierValid = true;
  edit_modifier_name = '';
  minimum_user_modifier_selected: boolean = true;
  isFormValid = true;
  update_disabled: boolean = false;
  is_courier_service:boolean=false;

  addedSpecifications: any = [];
  subcategories = [];
  modifiers = [];
  is_edit: boolean = false;
  edit_id: string = null;
  upload_imageurl = [];
  imageurl = [];
  imagefile = [];
  IMAGE_URL = environment.imageUrl;
  image_settings: ImageSetting;
  itemForm: UntypedFormGroup;
  isAddNewModifier = false;
  filter_modifiers: any = [];
  item_taxes = []
  taxes = []
  filteredAssociates: any = [];
  isAssociatedModifierAvailable: boolean = false;
  is_associated: boolean = false
  associate_modifier_group = {
    list: [],
    _id: '',
    name: ''
  }
  isAllChecked: boolean = false
  selectedModifierId: any;
  associate_modifier: any;
  isEditSpecification: boolean = false
  DELIVERY_TYPE = DELIVERY_TYPE;
  delivery_type: number = 1;
  formForwardNavStatus: string = "deny";
  editData: any;
  user_can_add_specification_quantity: boolean;
  country_currency_sign:any;
  @Input() item_sequence_number:any
  @ViewChild('cropModel', { static: true }) cropModel: ImageCropModelComponent;
  @ViewChild('inputGroupFile02', { static: true }) inputGroupFile02: ElementRef;
  @ViewChild('warningModel', { static: true }) warningModel: SpecificationWarningModelComponent;
  @ViewChild('template', { static: true }) template: TemplateRef<any>;

  constructor(private modalService: BsModalService,
    private _commonService: CommonService,
    public _helper: Helper,
    private _lang: LangService,
    private _subcategoryService: SubcategoryService,
    private _modifierService: ModifierService,
    private _itemService: ItemService,
    private _notifierService: NotifiyService,
    ) { }

  ngOnInit() {
    this._initForm();
  }

  _initData() {
    this._commonService.fetch_image_settings().then(res_data => {
      if (res_data.success) {
        this.image_settings = res_data.image_setting
      }
    })
  }

  onWizardStepClick(){
    this.delivery_type = this._helper.delivery_type;
    this._modifierService.list_group().then(data => {
      if (data.success) {
        this.modifiers = data.specification_group
      } else {
        this.modifiers = [];
      }
    })
  }

  _initForm() {
    this.itemForm = new UntypedFormGroup({
      name: new UntypedFormArray([]),
      details: new UntypedFormArray([]),
      price: new UntypedFormControl("0", [Validators.required, Validators.min(0)]),
      sequence_number: new UntypedFormControl(0, Validators.required),
      is_item_in_stock: new UntypedFormControl(true, Validators.required),
      is_visible_in_store: new UntypedFormControl(true, Validators.required),
      is_most_popular: new UntypedFormControl(false, Validators.required),
      product_id: new UntypedFormControl(null, Validators.required),
      item_taxes: new UntypedFormControl([]),
      main_item_id: new UntypedFormControl(null)
    })
    this._lang.supportedLanguages.forEach(_language => {
      if (_language.code == "en") {
        this.name.push(new UntypedFormControl("", Validators.required))
        this.details.push(new UntypedFormControl(null))
      } else {
        this.name.push(new UntypedFormControl(null))
        this.details.push(new UntypedFormControl(null))
      }
    })
  }

  async loadSubcategoryList() {
    let data = await this._subcategoryService.list(this._helper.selected_store_id)
    if (data.success) {
      this.subcategories = data.product_array;
      if(this.subcategories.length==0){
        setTimeout(() => {
          this.itemForm.patchValue({product_id:''});
        }, 50);
      }
      this.taxes = data.store_taxes
    }
  }

  onTaxSelected(event) {
    this.item_taxes = event
    this.itemForm.patchValue({ item_taxes: event })
  }

  get details() {
    return this.itemForm.get('details') as UntypedFormArray;
  }

  get name() {
    return this.itemForm.get('name') as UntypedFormArray;
  }

  async show(is_edit, edit_id, product_id, copyData = null, is_courier_service=false) {
    this.is_courier_service = is_courier_service;

    this._initData();
    if(this._helper.selected_store_id){
      await this.loadSubcategoryList();
    }

    this.upload_imageurl = []
    this._initForm()
    this.itemForm.patchValue({
      product_id: product_id
      
    })
    this.edit_id = edit_id;
    this.is_edit = is_edit;
    this.isModifierSelected = true;
    this.isAddModifier = false;
    this.isModifierSelected = false;
    this.imageurl = [];
    this.isModifierValid = true

    if (is_edit) {
      
      this._itemService.fetch(edit_id).then(data => {
        if (data.success) {
          this.editData = data.item;
          
          if(this.editData?.country_details){
            this.country_currency_sign =this.editData?.country_details[0]?.currency_sign;
          }   
          this.name.patchValue(this.editData.name)
          this.details.patchValue(this.editData.details)

          this.edit_id = (this._helper.delivery_type === DELIVERY_TYPE.SERVICE || this._helper.delivery_type === DELIVERY_TYPE.APPOINTMENT) ? this.editData._id : edit_id;

          this.itemForm.patchValue({
            ...this.editData
          })
          this.taxes = this.editData.country_tax_details
          this.item_taxes = this.editData.item_taxes

          this.addedSpecifications = this.editData.specifications;
          this.editData.image_url.forEach(element => {
            this.imageurl.push({ image: element, checked: false })
          });
          this.modalRef = this.modalService.show(this.template, this.config);
        }
      })
      this.formForwardNavStatus = "allow";
    } else {
      this.country_currency_sign = null;
      if (copyData) {
        this.name.patchValue(copyData.name)
        this.details.patchValue(copyData.details)
        this.itemForm.patchValue({ ...copyData })
        this.addedSpecifications = copyData.specifications;
      }
      this.modalRef = this.modalService.show(this.template, this.config);
      this.formForwardNavStatus = "deny";
    }
  }

  async onSubmitItem() {
    if (this.itemForm.valid || this.is_courier_service) {
      this.itemForm.value._id = this.edit_id;
      this.itemForm.value.specifications = this.addedSpecifications
      if (this.is_edit) {
        let json
          json = {
            ...this.itemForm.value
          }
        // }
        await this._itemService.update(json).then(data =>{
        })
        this.updateImages()
        this.onClose()
      } else {
        await this._itemService.add(this.itemForm.value).then(data => {
          this.edit_id = data.item._id;
          this.is_edit = true;
          this.updateImages()
          this.onClose()
        })
      }
      this.addedSpecifications = []
    } else {
      this.itemForm.markAllAsTouched();
    }

  }

  onSelectImageFile(event) {
    let files = event.target.files;
    if (files.length === 0) return;
    
    const mimeType = files[0].type;
    const fileType = this._helper.uploadFile.some(element => mimeType === element);
    
    if (!fileType) {
      this._notifierService.showNotification('error', this._helper._trans.instant('validation-title.invalid-image-format'));
      return;
    }

    const aspectRatio = this.image_settings.item_image_ratio;
    const resizeToWidth = this.image_settings.item_image_max_width;
    this.cropModel.imageChangedEvent = event;
    this.cropModel.show(aspectRatio, resizeToWidth);
  }

  imageCropped(event) {
    this.imagefile.push(event);
    const reader = new FileReader();
    reader.readAsDataURL(event);
    reader.onload = (_event) => {
      this.upload_imageurl.push({
        image: reader.result,
        checked: false,
      });
    }
    document.getElementById('inputGroupFile02')['value'] = ""
  }

  updateImages() {
    if (this.imagefile.length) {
      let formData = new FormData();
      formData.append('item_id', this.edit_id)
      this.imagefile.forEach((element, idx) => {
        if (element != null) {
          formData.append(idx.toString(), element)
        }
      })
      this._itemService.update_images(formData)
      this.imagefile = []
      this.upload_imageurl = []
    }
  }

  addModifier() {
    this.filter_modifiers = [];
    this.is_associated = false;
    this.isEditSpecification = false;   
    this.associate_modifier_group = { // associateModifierGroup
      list: [],
      _id: '',
      name: ''
    }
    this.associate_modifier = ''
    let allModifiers = JSON.parse(JSON.stringify(this.modifiers))
    let index = this.addedSpecifications.findIndex(_x => _x.is_parent_associate)
    if (index !== -1) {
      allModifiers.forEach(element => {
        if(element.is_visible_in_store === true){
        let idx = this.addedSpecifications.findIndex(_m => _m.unique_id === element.unique_id)
        if (idx !== -1) {
          if (this.addedSpecifications[idx].is_associated) {
            let counter = 0;
            this.addedSpecifications.forEach(modifierGroup => {
              if (modifierGroup._id === this.addedSpecifications[idx]._id) {
                counter++;
              }
            })
            if (counter <= this.addedSpecifications[index].list.length) {
              this.filter_modifiers.push(element)
            }
          } else if (!this.addedSpecifications[idx].is_parent_associate) {
            this.filter_modifiers.push(element)
          }
        } else {
          this.filter_modifiers.push(element)
        }
      }
      });
    } else {
      this.filter_modifiers = allModifiers.filter(m=>m.is_visible_in_store === true);
      this.filter_modifiers.forEach(m =>{
        m.list = m.list.filter(fm=>fm.is_visible_in_store === true)
      })
      this.filter_modifiers = this.filter_modifiers.filter(fm=>fm.list.length > 0)
      this.addedSpecifications.forEach(spec => {
        let index = this.filter_modifiers.findIndex(_x => _x._id === spec._id)
        if (index != -1) {
          this.filter_modifiers.splice(index, 1);
        }
      })

    }
    this.isAddModifier = true;
    this.isAddNewModifier = true
    this.onModifierItemChange(true)
  }

  selectModifier(modifier) {
    this.isModifierSelected = true;
    this.filteredAssociates = []
   
    this.addedSpecifications.push({
      ...modifier,
      max_range: 1,
      range: 1,
      type: 1,
      is_required: true,
      modifier_id: null, // modifierId
      modifier_group_id: null, // modifierGroupId
      isLatest: true
    })
    this.associate_modifier_group = { // associateModifierGroup
      list: [],
      _id: '',
      name: ''
    }
    this.isAllChecked = false;
    this.isAssociatedModifierAvailable = true
    this.selectedModifierId = modifier._id
    this.edit_modifier_name = modifier.name[this._lang.selectedlanguageIndex] ? modifier.name[this._lang.selectedlanguageIndex] : modifier.name[0];
    this.edit_id_item_modifier = this.addedSpecifications.findIndex(_a => _a._id.toString() === modifier._id.toString() && _a.modifier_id === null && _a.modifier_group_id === null);
    this.addedSpecifications[this.edit_id_item_modifier].list = this.addedSpecifications[this.edit_id_item_modifier].list.filter(i=>i.is_visible_in_store === true)
    this.onModifierItemChange(false);
    this.associateModifierFilter();
  }

  editSpecificationFromItem(id, index) {
    this.isEditSpecification = true
    this.filteredAssociates = []
    let idx = this.modifiers.findIndex(x => x._id === id)
    if (idx !== -1) {
      let i = this.editData.specifications.findIndex(x=> x._id === id)
      this.user_can_add_specification_quantity = this.editData.specifications[i].user_can_add_specification_quantity
      this.modifiers[idx].list.forEach(_modifier => {
        const midx = this.addedSpecifications[index].list.findIndex(_t => _t._id.toString() === _modifier._id.toString());
        if (midx === -1 && _modifier.is_visible_in_store === true) {
          this.addedSpecifications[index].list.push(_modifier);
        }
      })
    }

    if (this.addedSpecifications[index].max_range != 0) {
      this.modifierType = 2
    }
    this.isAddNewModifier = false;
    this.isModifierSelected = true;
    this.isAddModifier = true;
    this.is_associated = this.addedSpecifications[index].is_associated
    this.edit_id_item_modifier = index;
    let modifierGroupIndex = this.addedSpecifications.findIndex(_x => _x._id === this.addedSpecifications[index].modifier_group_id)
    if (modifierGroupIndex !== -1) {
      let i = this.editData.specifications.findIndex(x=> x._id === this.addedSpecifications[index].modifier_group_id)
      this.user_can_add_specification_quantity = this.editData.specifications[i].user_can_add_specification_quantity
      let modifierIndex = this.addedSpecifications[modifierGroupIndex].list.findIndex(_x => _x._id === this.addedSpecifications[index].modifier_id)
      this.associate_modifier = this.addedSpecifications[modifierGroupIndex].list[modifierIndex]
      this.associate_modifier_group = this.addedSpecifications[modifierGroupIndex]
      this.filteredAssociates.push(this.associate_modifier_group)
    }
    let modifier = JSON.parse(JSON.stringify(this.addedSpecifications[this.edit_id_item_modifier]))
    this.edit_modifier_name = modifier.name[this._lang.selectedlanguageIndex] ? modifier.name[this._lang.selectedlanguageIndex] : modifier.name[0];
    this.onModifierItemChange(true)
    if (this.addedSpecifications[index].type === 2) {
      this.isHideMaxRange = false;
    } else {
      this.isHideMaxRange = true;
    }

  }

  deleteSpecificationFromItem(idx) {
    if (this.addedSpecifications[idx].is_parent_associate) {
      this.warningModel.show(idx, CONSTANT.DELETE);
    } else {
      this.addedSpecifications.splice(idx, 1);
    }
  }

  onWarning(event) {
    if (event.confirmation) {
      let parentSPecificationId = this.addedSpecifications[event.index]._id;
      if (event.type === CONSTANT.DELETE) {
        this.addedSpecifications.splice(event.index, 1);
      }
      let availableSpecification = true;
      while (availableSpecification) {
        let index = this.addedSpecifications.findIndex(_x => _x.modifier_group_id === parentSPecificationId);
        if (index !== -1) {
          this.addedSpecifications.splice(index, 1);
        } else {
          availableSpecification = false;
        }
      }
      this.isModifierSelected = false;
      this.isAddModifier = false;
      this.isAddNewModifier = false;
      this.isEditSpecification = false;
      this.addedSpecifications[event.index].is_parent_associate = false;
    }
  }

  deleteImages() {
    this.upload_imageurl.forEach((_img, idx) => {
      if (_img.checked === true) {
        this.upload_imageurl[idx] = null
        this.imagefile[idx] = null
      }
    });

    let database_delete_images = this.imageurl.filter(_img => _img.checked === true);
    let delete_image_array = []
    database_delete_images.forEach((element, idx) => {
      delete_image_array.push(element.image)
    })
    if (delete_image_array.length) {
      this._itemService.delete_images(this.edit_id, delete_image_array).then(() => {
        this.imageurl = this.imageurl.filter(_img => _img.checked === false);
      })
    }
  }

  onUserSelected(index, event) {
    this.addedSpecifications[this.edit_id_item_modifier].list[index].is_user_selected = event.target.checked
    this.onModifierItemChange(true)
  }

  updateItemModifier(form) {
  if(form.invalid || this.isModifierError){
    form.submitted=true
    return
  }
    if (this.is_associated) {
      this.addedSpecifications[this.edit_id_item_modifier].modifier_id = this.associate_modifier._id
      this.addedSpecifications[this.edit_id_item_modifier].modifier_group_id = this.associate_modifier_group._id
      this.addedSpecifications[this.edit_id_item_modifier].modifier_group_name = this.associate_modifier_group.name[0]
      this.addedSpecifications[this.edit_id_item_modifier].modifier_name = this.associate_modifier.name[0]
      this.addedSpecifications[this.edit_id_item_modifier].is_associated = this.is_associated
    } else {
      let list = this.addedSpecifications.filter((x)=>(x._id==this.addedSpecifications[this.edit_id_item_modifier]._id && !x.is_associated))
      if(list.length>1){
        this._notifierService.showNotification('error', this._helper._trans.instant('label-title.data-already-added'));
        return;
      }
    }

    // this.addedSpecifications[this.edit_id_item_modifier].user_can_add_specification_quantity = this.user_can_add_specification_quantity
    this.addedSpecifications[this.edit_id_item_modifier].list = this.addedSpecifications[this.edit_id_item_modifier].list.filter(_t => _t.is_user_selected);
    if (Number(this.addedSpecifications[this.edit_id_item_modifier].range) > 0) {
      this.addedSpecifications[this.edit_id_item_modifier].is_required = true;
    } else {
      this.addedSpecifications[this.edit_id_item_modifier].is_required = false;
    }
    if (Number(this.addedSpecifications[this.edit_id_item_modifier].range) === 1 && Number(this.addedSpecifications[this.edit_id_item_modifier].max_range) === 0) {
      this.addedSpecifications[this.edit_id_item_modifier].type = 1;
    } else {
      this.addedSpecifications[this.edit_id_item_modifier].type = 2;
      if (this.addedSpecifications[this.edit_id_item_modifier].is_parent_associate) {
        this.warningModel.show(this.edit_id_item_modifier, CONSTANT.EDIT)
        return;
      }
    }
    this.isModifierSelected = this.isAddModifier = false
    let addedSpecificationsIndex = this.addedSpecifications.findIndex(_x => _x._id === this.associate_modifier_group._id)
    if (addedSpecificationsIndex !== -1) {
      this.addedSpecifications[addedSpecificationsIndex].is_parent_associate = true
    }
    delete this.addedSpecifications[this.edit_id_item_modifier].isLatest
    this.modifierType = 1;
  }

  navigateToList() {
    this.addedSpecifications[this.edit_id_item_modifier].list.forEach((spec, index) => {
      if(!spec.is_user_selected){
        this.addedSpecifications[this.edit_id_item_modifier].list.splice(index, 1)
      }
    })
    if (this.isAddNewModifier) {
      this.deleteSpecificationFromItem(this.edit_id_item_modifier)
    }
    this.isModifierSelected = false
    this.isEditSpecification = false
    this.isAddModifier = false
    this.isAddNewModifier = false
    this.is_associated = false;
    this.associate_modifier_group = { // associateModifierGroup
      list: [],
      _id: '',
      name: ''
    }
    this.associate_modifier = ''
    this.modifierType = 1;
  }

  onModifierItemChange(boolean) {

    if (this.addedSpecifications[this.edit_id_item_modifier]) {
      if (!boolean) {
        if (this.modifierType === 2) {
          this.addedSpecifications[this.edit_id_item_modifier].range = 0
          this.addedSpecifications[this.edit_id_item_modifier].max_range = 1
        }
      }
      let userSelected = 0;
      let is_user_selected_count = 0;
      this.addedSpecifications[this.edit_id_item_modifier].list.forEach(item => {
        if (item.is_default_selected) {
          userSelected++;
        }
        if (item.is_user_selected) {
          is_user_selected_count++;
        }
      });
      if (is_user_selected_count === 0) {
        this.minimum_user_modifier_selected = false
      } else {
        this.minimum_user_modifier_selected = true
      }


      let max_range = 0;

      if (this.modifierType == 1) {
        max_range = JSON.parse(JSON.stringify(this.addedSpecifications[this.edit_id_item_modifier].range))
        this.addedSpecifications[this.edit_id_item_modifier].max_range = 0
        if (this.addedSpecifications[this.edit_id_item_modifier].range === 1) {
          this.addedSpecifications[this.edit_id_item_modifier].type = 1
          this.addedSpecifications[this.edit_id_item_modifier].is_required = true
        } else if (this.addedSpecifications[this.edit_id_item_modifier].range > 1) {
          this.addedSpecifications[this.edit_id_item_modifier].type = 2
          this.addedSpecifications[this.edit_id_item_modifier].is_required = true
        }
      } else {
        max_range = JSON.parse(JSON.stringify(this.addedSpecifications[this.edit_id_item_modifier].max_range))
        this.addedSpecifications[this.edit_id_item_modifier].type = 2
      }
      if (userSelected < max_range) {
        if (is_user_selected_count >= max_range) {
          this.isModifierValid = true
        } else {
          this.isModifierValid = false
        }
      } else {
        this.isModifierValid = false
      }
      if (userSelected > max_range) {
        this.isModifierError = true
      } else {
        if (is_user_selected_count < max_range) {
          this.isModifierError = true
        } else {
          this.isModifierError = false
        }
      }
      if (this.modifierType === 2) {
        this.isFormValid = this.addedSpecifications[this.edit_id_item_modifier].max_range >= userSelected;
      }
      if (!this.isFormValid || this.isModifierError || !this.minimum_user_modifier_selected) {
        this.update_disabled = true
      } else {
        this.update_disabled = false
      }
    }

  }

  onRangeChange() {
    if (this.addedSpecifications[this.edit_id_item_modifier].range > this.addedSpecifications[this.edit_id_item_modifier].list.length) {
      this.addedSpecifications[this.edit_id_item_modifier].range = this.addedSpecifications[this.edit_id_item_modifier].list.length;
    }
    if (this.addedSpecifications[this.edit_id_item_modifier].max_range <= this.addedSpecifications[this.edit_id_item_modifier].range) {
      this.addedSpecifications[this.edit_id_item_modifier].max_range = this.addedSpecifications[this.edit_id_item_modifier].range + 1;
      this.onMaxRangeChange()
    }
    this.onModifierItemChange(true)
  }

  onMaxRangeChange() {
    if (this.addedSpecifications[this.edit_id_item_modifier].max_range > this.addedSpecifications[this.edit_id_item_modifier].list.length) {
      this.addedSpecifications[this.edit_id_item_modifier].max_range = this.addedSpecifications[this.edit_id_item_modifier].list.length
    }
    this.onModifierItemChange(true)
  }

  associateModifierFilter() {
    this.filteredAssociates = []
    let allAddedSpecifications = JSON.parse(JSON.stringify(this.addedSpecifications))
    allAddedSpecifications.forEach(specificationGroup => {
        if (specificationGroup._id !== this.selectedModifierId && specificationGroup.type === 1 && !specificationGroup.modifier_id && !specificationGroup.modifier_group_id  && !specificationGroup.isLatest){ 
        this.filteredAssociates.push(specificationGroup)
      } else if (specificationGroup.modifier_id && specificationGroup.modifier_group_id && (specificationGroup._id === this.selectedModifierId)) {
        let filteredAssociatedIndex = this.filteredAssociates.findIndex(x => x._id === specificationGroup.modifier_group_id)
        if (filteredAssociatedIndex !== -1) {
          let filteredAssociatedListIndex = this.filteredAssociates[filteredAssociatedIndex].list.findIndex(x => x._id === specificationGroup.modifier_id)
          if (filteredAssociatedListIndex !== -1) {
            this.filteredAssociates[filteredAssociatedIndex].list.splice(filteredAssociatedListIndex, 1)
            if (this.filteredAssociates[filteredAssociatedIndex].list.length === 0) {
              this.filteredAssociates.splice(filteredAssociatedIndex, 1)
            }
          }
        }
      }
    })
    let specificationIndex = allAddedSpecifications.findIndex(_x => _x._id === this.selectedModifierId && _x.modifier_group_id !== null)
    if(specificationIndex != -1){
      let index = allAddedSpecifications.findIndex(_x => _x._id === allAddedSpecifications[specificationIndex].modifier_group_id) 
      if(index !== -1){
        this.filteredAssociates = [];
        this.filteredAssociates.push(allAddedSpecifications[index])
      }
    }

    if (this.filteredAssociates.length === 0) {
      this.isAssociatedModifierAvailable = false
      this.is_associated = false
    } else {
      this.isAssociatedModifierAvailable = true
    }
  }

  onAssociate(event) {
    if (event.target.checked) {
      this.associateModifierFilter();
    } else {
      this.associate_modifier = null;
      this.associate_modifier_group = {
        list: [],
        _id: '',
        name: ''
      }
    }
  }

  onSelect(event) {
    this.isAllChecked = event.target.checked
    this.addedSpecifications[this.edit_id_item_modifier].list.forEach(modifier => {
      modifier.is_user_selected = event.target.checked
    })
    this.onModifierItemChange(true)
  }

  onClose() {
    this.modalRef.hide()
    setTimeout(() => {
      this.addedSpecifications = []
      this.itemForm.reset()
      this.country_currency_sign = null;
    }, 500);
  }

  nextPage(wizard){
    this._helper.checkAndCleanFormValues(this.itemForm)
    if(this.itemForm.invalid){
      this.itemForm.markAllAsTouched();
    }else{
      wizard.goToNextStep();
    }    
  }

  onChangeSpecGroup() {
    this.associate_modifier = null
  }
}
