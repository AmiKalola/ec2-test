import {  Component , Input, OnInit,  TemplateRef,  ViewChild } from '@angular/core';
import { UntypedFormArray, UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { SubcategoryService,SubcategoryModel } from 'src/app/services/subcategory.service';
import { Helper } from 'src/app/shared/helper';
import { LangService } from 'src/app/shared/lang.service';
import { DELIVERY_TYPE } from 'src/app/views/app/constant';

@Component({
  selector: 'app-add-new-sub-category-modal',
  templateUrl: './add-new-sub-category-modal.component.html',
  styles: []
})
export class AddNewSubCategoryModalComponent implements OnInit {
  modalRef: BsModalRef;
  config = {
    backdrop: true,
    ignoreBackdropClick: true,
    class: 'modal-right'
  };
  selectedPeople = [{ name: 'Karyn Wright' }];


  addSubcategory:SubcategoryModel = {
    _id:null,
    name:[],
    sequence_number:0
  }
  subcategoryForm:UntypedFormGroup;
  is_edit:boolean = false;
  edit_id:string = null;
  sequence_number = 0;
  DELIVERY_TYPE = DELIVERY_TYPE

  @Input() subcategories:any = [];
 
  @ViewChild('template', { static: true }) template: TemplateRef<any>;

  constructor(private modalService: BsModalService,private _subcategoryService:SubcategoryService,private _helper:Helper,private _lang:LangService) { }

  ngOnInit(){
    this._initForm()
  }

  _initForm(){
    this.subcategoryForm = new UntypedFormGroup({
      name:new UntypedFormArray([]),
      sequence_number:new UntypedFormControl(0,Validators.required),
      is_visible_in_store: new UntypedFormControl(true)
    })
    this._lang.supportedLanguages.forEach(_language=>{
      if(_language.code=="en"){
        this.name.push(new UntypedFormControl(null,Validators.required))
      }else{
        this.name.push(new UntypedFormControl(null))
      }
    })
  }

  get name() {
    return this.subcategoryForm.get('name') as UntypedFormArray; 
  }

  

  show(is_edit,edit_id): void {
    this.is_edit = is_edit
    this.edit_id = edit_id
    this.sequence_number =  Math.max.apply(Math, this.subcategories.map(function(o) { return o.sequence_number; })) + 1
    this.subcategoryForm.reset()
    if(is_edit && edit_id !== null){
      this._subcategoryService.fetch(this.edit_id).then(data=>{
        if(data.success){
          let editData = data.product;
          this.name.patchValue(editData.name)
          this.subcategoryForm.patchValue({...editData})
          if(this._helper.delivery_type === DELIVERY_TYPE.SERVICE || this._helper.delivery_type === DELIVERY_TYPE.APPOINTMENT){
            this.edit_id = editData._id
          }
          this.modalRef = this.modalService.show(this.template, this.config);
        }
      })
    }else{
      this.addSubcategory = {
        _id:null,
        name:[],
        sequence_number:this.sequence_number    
      }
      this.subcategoryForm.patchValue({
        sequence_number:this.sequence_number
      })
      this.modalRef = this.modalService.show(this.template, this.config);
    }
  }

  async onSubmit(){
    this._helper.checkAndCleanFormValues(this.subcategoryForm)
    this.subcategoryForm.value.delivery_type = this._helper.delivery_type 
    if(this.subcategoryForm.valid){
      if(this.is_edit){
        this.subcategoryForm.value._id = this.edit_id;
       await this._subcategoryService.update(this.subcategoryForm.value)
      }else{
        await this._subcategoryService.add(this.subcategoryForm.value)
      }
      this.modalRef.hide();
    }else{
      this.subcategoryForm.markAllAsTouched();
    }
  }


}
