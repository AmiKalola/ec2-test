import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AddAppPushKeyDetalisComponent } from './add-app-push-key-detalis.component';

describe('AddAppPushKeyDetalisComponent', () => {
  let component: AddAppPushKeyDetalisComponent;
  let fixture: ComponentFixture<AddAppPushKeyDetalisComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AddAppPushKeyDetalisComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddAppPushKeyDetalisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
