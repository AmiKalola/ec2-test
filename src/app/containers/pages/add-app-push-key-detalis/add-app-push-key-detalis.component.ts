import { Component, TemplateRef, ViewChild } from '@angular/core';
import { UntypedFormControl, UntypedFormGroup } from '@angular/forms';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { AdminSettingService } from 'src/app/services/admin-setting.service';
@Component({
  selector: 'app-add-app-push-key-detalis',
  templateUrl: './add-app-push-key-detalis.component.html',
})
export class AddAppPushKeyDetalisComponent {
  modalRef: BsModalRef;
  config = {
    backdrop: true,
    ignoreBackdropClick: true,
    class: 'modal-right'
  };

  app_push_key_details: UntypedFormGroup;
  push_p8_file:any
  formData = new FormData();

  @ViewChild('template', { static: true }) template: TemplateRef<any>;

  constructor(private modalService: BsModalService,private _adminSettingService: AdminSettingService) { }

  ngOnInit(): void {
    this._initForm()
  }

  show(app_push_key_details=null): void {
    this.app_push_key_details.patchValue(app_push_key_details)
    this.modalRef = this.modalService.show(this.template, this.config);
  }

  _initForm() {
    this.app_push_key_details = new UntypedFormGroup({
      android_provider_app_gcm_key: new UntypedFormControl(null),
      android_store_app_gcm_key: new UntypedFormControl(null),
      android_user_app_gcm_key: new UntypedFormControl(null),
      team_id: new UntypedFormControl(null),
      key_id: new UntypedFormControl(null),
      ios_push_certificate_path: new UntypedFormControl(null)
    })
  }

  onSelectFile(event){
    const files = event.target.files;
    if (files.length === 0)
      return;

    const fileType = files[0].type;
    
    if (fileType !== '' && fileType !== "application/pkcs8") {
      alert("Please Select Valid File With .p8 extension.")
      return;
    }

    this.push_p8_file = files[0]
  }

  onRemoveSelectedFile(){
    this.push_p8_file = null
    let fileInput: any = document.getElementById("inputGroupFile02")
    fileInput.value = "";
  }

  onSave(){
    this.formData = new FormData();
    if(this.push_p8_file){
      this.formData.append('push_p8_file',this.push_p8_file)
    }
    this.formData.append('android_provider_app_gcm_key',this.app_push_key_details.value.android_provider_app_gcm_key)
    this.formData.append('android_store_app_gcm_key',this.app_push_key_details.value.android_store_app_gcm_key)
    this.formData.append('android_user_app_gcm_key',this.app_push_key_details.value.android_user_app_gcm_key)
    this.formData.append('team_id',this.app_push_key_details.value.team_id)
    this.formData.append('key_id',this.app_push_key_details.value.key_id)
    this.formData.append('ios_push_certificate_path',this.app_push_key_details.value.ios_push_certificate_path)

    this._adminSettingService.updatePushNotificationSetting(this.formData)
    this.close()
  }

  close(){
    this.onRemoveSelectedFile()
    this.modalRef.hide()
  }
}
