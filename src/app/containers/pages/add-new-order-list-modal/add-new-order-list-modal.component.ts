import { Component, TemplateRef,  ViewChild } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { OrderService } from 'src/app/services/order.service';
import { Helper } from 'src/app/shared/helper';
import { LangService } from 'src/app/shared/lang.service';
import { DELIVERY_TYPE_CONSTANT, PRICE_FORMULA } from 'src/app/shared/constant';
import * as html2pdf from 'html2pdf.js';
@Component({
  selector: 'app-add-new-order-list-modal',
  templateUrl: './add-new-order-list-modal.component.html',
  styleUrls: ['./add-new-order-list-modal.component.scss']
})
export class AddNewOrderListModalComponent  {
  PRICE_FORMULA = PRICE_FORMULA;

  modalRef: BsModalRef;
  config = {
    backdrop: true,
    ignoreBackdropClick: true,
    class: 'modal-right'
  };
  order;
  is_order_cancle: boolean = false
  currency_sign: any;
  DELIVERY_TYPE_CONSTANT = DELIVERY_TYPE_CONSTANT;
  timezone_for_display_date:string = '';
  confirmModelRef: BsModalRef;
  confirmatiomConfig = {
    backdrop: true,
    ignoreBackdropClick: true,
  };
  isHistory = false
  @ViewChild('template', { static: true }) template: TemplateRef<any>;
  @ViewChild('confirmationTemplate', { static: true }) confirmationTemplate: TemplateRef<any>;
  buttonDisabled: boolean = false;
  isInvoice = true
  constructor(private modalService: BsModalService,
    private _orderService:OrderService,
    public _helper:Helper,
    public _langService: LangService) {
      this._helper.display_date_timezone.subscribe(data => {
        this.timezone_for_display_date = data;
      })
    }

  show(orderid, type = null, isHistory = false ,new_store_id=null): void {
    this.isInvoice = true
    this.isHistory = isHistory
    if(type && type === 1){
      this.is_order_cancle = true
    }
    this._orderService.get_order_detail(orderid, new_store_id).then(data => {
      if (data.success) {
        this.modalRef = this.modalService.show(this.template, this.config);
        this.order = data['data'];
        this.currency_sign = this.order.country_currency_sign
        if (this.order && this.order.request_date_time) {
          this.order.request_date_time.forEach((request) => {
            const isStatusPresent = this.order.date_time.some(
              (dateTime) => dateTime.status === request.status
            );
            if (!isStatusPresent) {
              this.order.date_time.push(request);
            }
          });

          this.order.date_time.sort((a, b) => {
            const dateA = new Date(a.date).getTime();
            const dateB = new Date(b.date).getTime();
            return dateA - dateB;
          });
        }

      }
    })
    
  }

  onCancle(){
    this.confirmModelRef = this.modalService.show(this.confirmationTemplate, this.confirmatiomConfig);
  }

  confirm(){
    let json = {
      cancel_reason: 'Admin Cancelled',
      order_id: this.order._id,
      user_id: this.order.user_id,
      type: 1
    }
    this._orderService.user_cancle_order(json)
    this.cancel();
    this.onClose()
  }

  cancel(){
    this.confirmModelRef.hide();
  }

  onClose(){
    this.modalRef.hide();
  }

  downloadPdf(){    

    this.buttonDisabled = true;

    const element = document.getElementById('htmlContent');
    const clonedElement = element.cloneNode(true) as HTMLElement;


    const targetDiv = clonedElement.querySelector('#test_order');
    const additionalContentElement = document.createElement('div');

    additionalContentElement.innerHTML = `

    <div class="row d-flex flex-row justify-content-start justify-content-around mb-3">

    <div class="text-center ">

    <p class="text-center mb-1" style="font-size: large;font-weight: bold;">Order ID: `+this.order.id+`</p>

    </div>

    </div>

    `;

    // Append the new HTML content to the cloned element

    targetDiv.appendChild(additionalContentElement);
    const date = Math.floor(new Date().getTime() / 1000);
    const options = {
      filename: date + this.order.id + '.pdf',
      margin: [10, 20, 10, 20] ,    
    };

    html2pdf().set(options).from(clonedElement).toPdf().save().then(() => {
      this.buttonDisabled = false;
    }).catch((error) => {
      console.error('Error generating PDF', error);
    });

  }

  onSelectTab(isTrue) {
    this.isInvoice = isTrue
  }

}
