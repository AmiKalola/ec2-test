import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AddNewWalletRequestComponent } from './add-new-wallet-request.component';

describe('AddNewWalletRequestComponent', () => {
  let component: AddNewWalletRequestComponent;
  let fixture: ComponentFixture<AddNewWalletRequestComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AddNewWalletRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddNewWalletRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
