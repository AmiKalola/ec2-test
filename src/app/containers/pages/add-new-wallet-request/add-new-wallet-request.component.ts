import { Component, TemplateRef,  ViewChild } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { Lightbox } from 'ngx-lightbox';
import { WalletService } from 'src/app/services/wallet.service';
import { Helper } from 'src/app/shared/helper';

@Component({
  selector: 'app-add-new-wallet-request',
  templateUrl: './add-new-wallet-request.component.html',
})
export class AddNewWalletRequestComponent  {
  modalRef: BsModalRef;
  config = {
    backdrop: true,
    ignoreBackdropClick: true,
    class: 'modal-right'
  };
  request: any;
  bank_detail: any;
  album: Array<any> = [
    {
      src: '/assets/img/products/marble-cake.jpg',
      thumb: '/assets/img/products/marble-cake-thumb.jpg'
    },
    {
      src: '/assets/img/products/parkin.jpg',
      thumb: '/assets/img/products/parkin-thumb.jpg'
    },
    {
      src: '/assets/img/products/fruitcake.jpg',
      thumb: '/assets/img/products/fruitcake-thumb.jpg'
    },
    {
      src: '/assets/img/products/tea-loaf.jpg',
      thumb: '/assets/img/products/tea-loaf-thumb.jpg'
    },
    {
      src: '/assets/img/products/napoleonshat.jpg',
      thumb: '/assets/img/products/napoleonshat-thumb.jpg'
    },
    {
      src: '/assets/img/products/magdalena.jpg',
      thumb: '/assets/img/products/magdalena-thumb.jpg'
    }
  ]

  @ViewChild('template', { static: true }) template: TemplateRef<any>;

  constructor(private modalService: BsModalService, private _walletService: WalletService, private lightBox: Lightbox,public _helper:Helper) { }

  get_bank_detail(id){
    this._walletService.get_bank_details({bank_detail_id: id}).then(res_data => {
      if(res_data.success){
        this.bank_detail = res_data.bank_detail
      }
    })
  }

  show(id, wallet_request): void {
    this.request = wallet_request
    this.get_bank_detail(id)
    this.modalRef = this.modalService.show(this.template, this.config);
  }

  onLightBox(){
    this.onClose()
    this.lightBox.open(this.album, 0, {centerVertically: true, positionFromTop: 0, disableScrolling: true, wrapAround: true});
  }

  onClose(){
    this.modalRef.hide()
  }
}
