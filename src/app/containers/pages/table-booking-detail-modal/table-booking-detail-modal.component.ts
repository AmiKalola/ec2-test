import { Component, TemplateRef, ViewChild} from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { OrderService } from 'src/app/services/order.service';
import { Helper } from 'src/app/shared/helper';
import * as html2pdf from 'html2pdf.js';
@Component({
    selector: 'table-booking-detail-modal',
    templateUrl: './table-booking-detail-modal.component.html',
    styleUrls: ['./table-booking-detail-modal.component.scss']
})
export class TableOrderDetailModalComponent {
    callback: (value: any) => void;
    value: any;
    tax_string: string;
    modalRef: BsModalRef;
    config = {
        backdrop: true,
        ignoreBackdropClick: true,
        class: 'modal-right'
    };
    @ViewChild('template', { static: true }) template: TemplateRef<any>;
    order: any;
    currency_sign: any;
    timezone_for_display_date:string = '';
    buttonDisabled: boolean = false;
    isInvoice = true;

    constructor(private modalService: BsModalService,
        private _orderService: OrderService,
        public _helper: Helper) {
          this._helper.display_date_timezone.subscribe(data => {
            this.timezone_for_display_date = data;
          })
        }

    show(order_id): void {
        this._orderService.get_order_detail(order_id).then(data=>{
            if(data.success){
              this.modalRef = this.modalService.show(this.template, this.config);
              this.order = data['data'];
              this.currency_sign = this.order.country_currency_sign
              if(this.order && this.order.request_date_time){
                this.order.date_time.push(...this.order.request_date_time)  
                
                let unique = [];
                let distinct = [];
                for(const element of this.order.date_time){
                  if(!unique[element.status]){
                    distinct.push(element);
                    unique[element.status] = 1;
                  }
                }
      
                this.order.date_time = distinct;
                this.order.date_time.sort((a, b) => parseFloat(a.status) - parseFloat(b.status))
      
              }
            }
          })
    }

    onClose(){
      this.isInvoice = true
      this.modalRef.hide()
    }

    downloadPdf(){    

      this.buttonDisabled = true;
  
  
      const element = document.getElementById('htmlContent');
      const clonedElement = element.cloneNode(true) as HTMLElement;
  
      const targetDiv = clonedElement.querySelector('#test_order');
  
      const additionalContentElement = document.createElement('div');
  
      additionalContentElement.innerHTML = `
  
      <div class="row d-flex flex-row justify-content-start justify-content-around mb-3">
  
      <div class="text-center ">
  
      <p class="text-center mb-1" style="font-size: large;font-weight: bold;">Order ID: `+this.order.id+`</p>
  
      </div>
  
      </div>
  
      `;
  
      // Append the new HTML content to the cloned element
  
      targetDiv.appendChild(additionalContentElement);
      const date = Math.floor(new Date().getTime() / 1000);
  
      const options = {
        filename: date + this.order.id + '.pdf',
        margin: [10, 20, 10, 20] ,     
      };
      html2pdf().set(options).from(clonedElement).toPdf().save().then(() => {

        this.buttonDisabled = false;
  
      }).catch((error) => {
        console.error('Error generating PDF', error);
      });
  
    }

    onSelectTab(isTrue) {
      this.isInvoice = isTrue
    }
}