import { Component, TemplateRef, ViewChild } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { ImageCropModelComponent } from '../image-crop-model/image-crop-model.component';
import { CommonService } from '../../../services/common.service';
import { VehicleService } from '../../../services/vehicle.service'
import { environment } from 'src/environments/environment';
import { Helper } from 'src/app/shared/helper';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { NotifiyService } from 'src/app/services/notifier.service';


@Component({
  selector: 'app-add-new-vehicle',
  templateUrl: './add-new-vehicle.component.html',
  styleUrls: ['./add-new-vehicle.component.scss']
})
export class AddNewVehicleComponent {
  modalRef: BsModalRef;
  config = {
    backdrop: true,
    ignoreBackdropClick: true,
    class: 'modal-right'
  };
  name: any = '';
  description: any = '';
  is_business: any = false;
  image_settings: any;
  vehicle_imagefile: any;
  mappin_imagefile: any;
  upload_vehicle_imageurl: any = '';
  upload_mappin_imageurl: any = '';
  image_type: number = 1;
  form_data: FormData;
  IMAGE_URL = environment.imageUrl
  is_edit: boolean = false;
  is_valid: boolean = true;
  vehicle_list: any = [];
  error:any = false
  @ViewChild('template', { static: true }) template: TemplateRef<any>;
  @ViewChild('cropModel', { static: true }) cropModel: ImageCropModelComponent;
  
  size_type:number=this._helper.SIZE.METER;
  weight_type:number=this._helper.WEIGHT.KG;
  length:number=null;
  width:number=null;
  height:number=null;
  min_weight:number=null;
  max_weight:number=null;
  minmaxWeightValueInvalid:boolean = false;
  vehicleForm: FormGroup;

  constructor(private modalService: BsModalService,
    private _commonService: CommonService,
    private _vehicelService: VehicleService,
    private _helper: Helper,
    private _notifierService:NotifiyService
    ) { }

  show(vehicle): void {
    this.initForm();
    this.form_data = new FormData();

    if (vehicle != '') {
      let json = {
        vehicle_id: vehicle._id
      }
      this._vehicelService.get_vehicle_details(json).then(res_data => {
        if (res_data.success) {
          let vehicle_detail = res_data.vehicle
          this.is_edit = true;
          this.vehicleForm.patchValue({ name: vehicle_detail.vehicle_name,
            description:vehicle_detail.description,
            is_business:vehicle_detail.is_business,
            size_type:vehicle_detail.size_type,
            weight_type : vehicle_detail.weight_type,
            length : vehicle_detail.length,
            width : vehicle_detail.width,
            height : vehicle_detail.height,
            min_weight : vehicle_detail.min_weight,
            max_weight : vehicle_detail.max_weight
          });

          if(vehicle_detail.image_url===''){this.upload_vehicle_imageurl = vehicle_detail.image_url}
          if(vehicle_detail.image_url!==''){this.upload_vehicle_imageurl = this.IMAGE_URL+vehicle_detail.image_url}
          if(vehicle_detail.map_pin_image_url===''){this.upload_mappin_imageurl = vehicle_detail.map_pin_image_url}
          if(vehicle_detail.map_pin_image_url!==''){this.upload_mappin_imageurl = this.IMAGE_URL+vehicle_detail.map_pin_image_url}
          this.form_data.append('vehicle_id', vehicle._id)
          this.minMaxValueValidation();
        }
      })
    } else {
      this.is_edit = false
      this.name = ''
      this.description = ''
      this.is_business = false
      this.upload_vehicle_imageurl = ''
      this.upload_mappin_imageurl = ''
    }
    this.modalRef = this.modalService.show(this.template, this.config);
    this.fetch_image_settings()
  }

  fetch_image_settings() {
    this._commonService.fetch_image_settings().then(res_data => {
      if (res_data.success) {
        this.image_settings = res_data.image_setting
      }
    })
  }

  initForm() {
    this.vehicleForm = new FormGroup({
      name: new FormControl('', Validators.required),
      description: new FormControl(''),
      length: new FormControl('', Validators.required),
      width: new FormControl('', Validators.required),
      height: new FormControl('', Validators.required),
      size_type: new FormControl(this._helper.SIZE.METER, Validators.required),
      min_weight: new FormControl('', Validators.required),
      max_weight: new FormControl('', Validators.required),
      weight_type: new FormControl(this._helper.WEIGHT.KG, Validators.required),
      is_business: new FormControl(false)
    });
  }

  //min max weight value validation error 
  minMaxValueValidation() {
    if (this.vehicleForm.value.min_weight > this.vehicleForm.value.max_weight ) {
      this.minmaxWeightValueInvalid = true ; 
    }else{
      this.minmaxWeightValueInvalid = false;
    }
  }

  onSubmit() {
    this._helper.checkAndCleanFormValues(this.vehicleForm)
    if(this.minmaxWeightValueInvalid){
      return;
    }
    if(this.vehicleForm.invalid){
      this.vehicleForm.markAllAsTouched();
      return
    }
    this.form_data.append('vehicle_name', this.vehicleForm.value.name)
    this.form_data.append('description', this.vehicleForm.value.description)
    this.form_data.append('is_business', this.vehicleForm.value.is_business)
    this.form_data.append('size_type', this.vehicleForm.value.size_type.toString())
    this.form_data.append('weight_type', this.vehicleForm.value.weight_type.toString())
    this.form_data.append('length', this.vehicleForm.value.length.toString())
    this.form_data.append('height', this.vehicleForm.value.height.toString())
    this.form_data.append('width', this.vehicleForm.value.width.toString())
    this.form_data.append('min_weight', this.vehicleForm.value.min_weight.toString())
    this.form_data.append('max_weight', this.vehicleForm.value.max_weight.toString())
    
    this._vehicelService.add_vehicle_data(this.form_data)
    this.onClose()
  }

  get_vehicle_list(event:any){
    let vehicle = (document.getElementById("vehicle") as HTMLInputElement).value
    
    this._vehicelService.get_vehicle_list().then(res_data => {
      this.vehicle_list = res_data.vehicles
      
      for(const element of this.vehicle_list){
        if(vehicle.toLowerCase().trim() == element.vehicle_name.toLowerCase().trim()){
          this.error = true
        }
        else{
          this.error = false
        }
      }
    })
  }

  onUpdate() {
    this._helper.checkAndCleanFormValues(this.vehicleForm)
    if(this.minmaxWeightValueInvalid){
      return;
    }
    if(this.vehicleForm.invalid){
      this.vehicleForm.markAllAsTouched();
      return
    }
    this.form_data.append('vehicle_name', this.vehicleForm.value.name)
    this.form_data.append('description', this.vehicleForm.value.description)
    this.form_data.append('is_business', this.vehicleForm.value.is_business)
    this.form_data.append('size_type', this.vehicleForm.value.size_type.toString())
    this.form_data.append('weight_type', this.vehicleForm.value.weight_type.toString())
    this.form_data.append('length', this.vehicleForm.value.length.toString())
    this.form_data.append('height', this.vehicleForm.value.height.toString())
    this.form_data.append('width', this.vehicleForm.value.width.toString())
    this.form_data.append('min_weight', this.vehicleForm.value.min_weight.toString())
    this.form_data.append('max_weight', this.vehicleForm.value.max_weight.toString())

    this._vehicelService.update_vehicle(this.form_data)
    this.onClose()
  }

  onSelectImageFile(event, type) {  //1: Vehicle, 2: Map-pin
    this.image_type = type
    let files = event.target.files;
    if (files.length === 0)
      return;
    const mimeType = files[0].type;
    let fileType = this._helper.uploadFile.filter((element)=> {
      return mimeType==element;
    })
    if (mimeType != fileType) {
      this._notifierService.showNotification('error', this._helper._trans.instant('validation-title.invalid-image-format'));
      return
    }
    let aspectRatio = this.image_settings.item_image_ratio;
    let resizeToWidth = this.image_settings.item_image_max_width;
    this.cropModel.imageChangedEvent = event;
    this.cropModel.show(aspectRatio, resizeToWidth);
  }

  imageCropped(event) {
    const reader = new FileReader();
    reader.readAsDataURL(event);
    if (this.image_type === 1) {
      this.vehicle_imagefile = event;
      this.form_data.append('image_url', this.vehicle_imagefile)
      reader.onload = (_event) => {
        this.upload_vehicle_imageurl = reader.result
      }
    } else {
      reader.onload = (_event) => {
        this.mappin_imagefile = event;
        this.form_data.append('map_pin_image_url', this.mappin_imagefile)
        this.upload_mappin_imageurl = reader.result
      }
    }
  }

  onClose() {
    this.modalRef.hide()
    this.form_data = new FormData();
    this.is_edit = false
    this.name = ''
    this.description = ''
    this.is_business = ''
    this.upload_vehicle_imageurl = ''
    this.upload_mappin_imageurl = ''
  }

}
