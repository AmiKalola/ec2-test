import { Component, OnInit, TemplateRef,  ViewChild } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { Helper } from 'src/app/shared/helper';
import { LangService } from 'src/app/shared/lang.service';

@Component({
  selector: 'app-referral-code-modal',
  templateUrl: './referral-code-modal.component.html',
})
export class ReferralCodeModalComponent implements OnInit {

  modalRef: BsModalRef;
  config = {
    backdrop: true,
    ignoreBackdropClick: true,
    class: 'modal-right'
  };
  referral: any;
  type: string;
  timezone_for_display_date:string = '';

  @ViewChild('template', { static: true }) template: TemplateRef<any>;

  constructor(private modalService: BsModalService, public _lang: LangService,public _helper:Helper) { }

  ngOnInit(){
    this._helper.display_date_timezone.subscribe(data => {
      this.timezone_for_display_date = data;
    })
  }

  show(referral, type): void {
    this.modalRef = this.modalService.show(this.template, this.config);
    this.referral = referral;
    this.type = type;
  }

}
