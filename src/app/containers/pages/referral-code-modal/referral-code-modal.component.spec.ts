import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ReferralCodeModalComponent } from './referral-code-modal.component';

describe('ReferralCodeModalComponent', () => {
  let component: ReferralCodeModalComponent;
  let fixture: ComponentFixture<ReferralCodeModalComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ReferralCodeModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReferralCodeModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
