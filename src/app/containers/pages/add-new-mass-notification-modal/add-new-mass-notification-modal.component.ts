import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { CountryService } from 'src/app/services/country.service';
import { DeliveryFeesService } from 'src/app/services/delivery-fees.service';
import { DeliveryService } from 'src/app/services/delivery.service';
import { MassNotificationService } from 'src/app/services/mass-notification.service';
import { USER_TYPE } from 'src/app/shared/constant';
import { Helper } from 'src/app/shared/helper';

@Component({
  selector: 'app-add-new-mass-notification-modal',
  templateUrl: './add-new-mass-notification-modal.component.html',
})
export class AddNewMassNotificationModalComponent implements OnInit {
  modalRef: BsModalRef;
  config = {
    backdrop: true,
    ignoreBackdropClick: true,
    class: 'modal-right'
  };

  USER_TYPE = USER_TYPE
  notificationForm: UntypedFormGroup
  country_list = []
  city_list = []
  delivery_list = []

  @ViewChild('template', { static: true }) template: TemplateRef<any>;

  constructor(private modalService: BsModalService,
    private _countryService: CountryService,
    private _deliveryService: DeliveryService,
    private _massNotificationService: MassNotificationService,
    private helper : Helper,
    private _deliveryFeesService: DeliveryFeesService) { }

  ngOnInit(): void {
    this._initForm()
  }

  show(): void {
    this.modalRef = this.modalService.show(this.template, this.config);
    this._initForm()
    this.getCountryList()
    this.getDeliveryList()
  }

  _initForm() {
    this.notificationForm = new UntypedFormGroup({
      user_type: new UntypedFormControl(USER_TYPE.USER,Validators.required),
      device_type: new UntypedFormControl('both',Validators.required),
      delivery: new UntypedFormControl(null),
      country: new UntypedFormControl('all',Validators.required),
      city: new UntypedFormControl('all',Validators.required),
      message: new UntypedFormControl(null,Validators.required),
    })
  }

  send() {
    this.helper.checkAndCleanFormValues(this.notificationForm);
    if(this.notificationForm.valid){
      this._massNotificationService.sendMassNotification({...this.notificationForm.value})
      this.notificationForm.reset()
      this.onClose()
    }else{
      this.notificationForm.markAllAsTouched();
    }
  }

  getCountryList() {
    this._countryService.fetch().then(res => {
      if (res.success) {
        this.country_list = res.countries
      }
    })
  }

  getCityList() {
    if(this.notificationForm.value.country != 'all'){
    this._deliveryFeesService.get_city_lists(this.notificationForm.value.country).then(res => {
      this.city_list = []
      this.notificationForm.patchValue({city : 'all'});
      if (res.success) {
        this.city_list = res.cities
      }
    })
    }else{
      this.city_list = []
      this.notificationForm.patchValue({city : 'all'});
    }
  }

  getDeliveryList() {
    this._deliveryService.get_delivery_list().then(res => {
      if (res.success) {
        this.delivery_list = res.deliveries
      }
    })
  }

  onClose(){
    this.country_list = []
    this.city_list = []
    this.delivery_list = []
    this.modalRef.hide();
  }
}
