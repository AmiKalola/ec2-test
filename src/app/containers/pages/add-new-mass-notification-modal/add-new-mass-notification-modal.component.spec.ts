import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AddNewMassNotificationModalComponent } from './add-new-mass-notification-modal.component';

describe('AddNewMassNotificationModalComponent', () => {
  let component: AddNewMassNotificationModalComponent;
  let fixture: ComponentFixture<AddNewMassNotificationModalComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AddNewMassNotificationModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddNewMassNotificationModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
