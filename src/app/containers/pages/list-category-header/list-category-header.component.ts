import { Component,  ViewChild, EventEmitter, Output, Input } from '@angular/core';
import { Helper } from 'src/app/shared/helper';
import { DELIVERY_TYPE } from 'src/app/views/app/constant';

@Component({
  selector: 'app-list-category-header',
  templateUrl: './list-category-header.component.html'
})
export class ListCategoryHeaderComponent {
  displayOptionsCollapsed = false;

  @Input() showOrderBy = true;
  @Input() showSearch = true;
  @Input() showItemsPerPage = true;
  @Input() showDisplayMode = true;
  @Input() showCheckboxMode = false;
  @Input() showCheckboxModeList = [];
  @Input() displayMode = 'list';
  @Input() selectAllState = '';
  @Input() itemsPerPage = 10;
  @Input() itemOptionsPerPage = [5, 10, 20];
  @Input() itemOrder = { label: 'Product Name', value: 'title' };
  @Input()  itemOptionsOrders = [
    { label: 'Product Name', value: 'title' },
    { label: 'Category', value: 'category' },
    { label: 'Status', value: 'status' }];

  @Output() changeDisplayMode: EventEmitter<string> = new EventEmitter<string>();
  @Output() addNewItem: EventEmitter<any> = new EventEmitter();
  @Output() selectAllChange: EventEmitter<any> = new EventEmitter();
  @Output() searchKeyUp: EventEmitter<any> = new EventEmitter();
  @Output() itemsPerPageChange: EventEmitter<any> = new EventEmitter();
  @Output() changeOrderBy: EventEmitter<any> = new EventEmitter();
  @Output() onChangeStore: EventEmitter<any> = new EventEmitter();
  is_store_can_add_category: boolean = false
  DELIVERY_TYPE = DELIVERY_TYPE

  @ViewChild('search') search: any;
  constructor(public _helper: Helper) {
    this._helper.changeSelectedStore.subscribe(() => {
      if(this._helper.selected_store){
        this.is_store_can_add_category = this._helper.selected_store.delivery_details?.is_store_can_create_group
      }
    })
  }



  onSelectDisplayMode(mode: string): void {
    this.changeDisplayMode.emit(mode);
  }
  onAddNewItem(): void {
    this.addNewItem.emit(null);
  }
  selectAll(event): void  {
    this.selectAllChange.emit(event);
  }
  onChangeItemsPerPage(item): void  {
    this.itemsPerPageChange.emit(item);
  }

  onChangeOrderBy(item): void  {
    this.itemOrder = item;
    this.changeOrderBy.emit(item);
  }

  onSearchKeyUp($event): void {
    this.searchKeyUp.emit($event);
  }

  on_change_store(){
    this._helper.selected_store_id =  null
    this.onChangeStore.emit()
  }
}
