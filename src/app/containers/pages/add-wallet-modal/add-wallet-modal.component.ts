import { Component, EventEmitter, Input, Output, TemplateRef, ViewChild } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Helper } from 'src/app/shared/helper';

@Component({
    selector:'app-add-wallet-modal',
    templateUrl:'./add-wallet-modal.component.html'
})
export class AddWalletModelComponent {

    modalRef: BsModalRef;
    config = {
      backdrop: false,
      ignoreBackdropClick: false,
      class: 'modal-popup'
    };
    wallet_amount: any = 0
    isError:boolean = false
    max_number = 9999

    @ViewChild('template', { static: true }) template: TemplateRef<any>;

    @Output() amount = new EventEmitter<any>();
    @Input() pattern: string | RegExp


  constructor(private modalService: BsModalService, public _helper: Helper) { }


  show(){
      this.wallet_amount = 0;
      this.modalRef = this.modalService.show(this.template, this.config)
  }

  
  confirm(){
    if(!isNaN(this.wallet_amount)){
      if(Number.isInteger(this.wallet_amount) === false)
      {
        this.isError = true
        return false  
      }
      else{
        this.isError = false
        this.amount.emit(this.wallet_amount)
        this.modalRef.hide()
      }
    } else {
      this.isError = true
    }
  }
  close_modal(){
    this.modalRef.hide();
    this.amount.emit('close_modal')
  }

  wallet_amount_validator(event){
    if(this.wallet_amount > this.max_number){
      this.isError = true
      return false  
    } else {
      this.isError = false
    }
  }

}
