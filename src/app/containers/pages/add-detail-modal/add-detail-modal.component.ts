import { Component, TemplateRef, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { AddDetailsService } from 'src/app/services/add-detail.service'
import { Helper } from 'src/app/shared/helper';
import { LangService } from 'src/app/shared/lang.service';

@Component({
  selector: 'app-add-detail-modal',
  templateUrl: './add-detail-modal.component.html',
})
export class AddDetailModalComponent  {
  user = true;
  data;
  modalRef: BsModalRef;
  config = {
      backdrop: true,
      ignoreBackdropClick: false,
      class: 'modal-right'
  };
  countries: any = [];
  deliveries: any = [];
  providers: any = [];
  cities: any = [];
  stores: any = [];
  selectedCountry: any;
  copy_store: any;
  paste_store: any;
  type: any;
  city_id: any;
  delivery: any;
  store: any;
  provider: any;
  user_detail: any;
  email: any = '';
  phone: any;
  name: any;
  userForm: NgForm


  @ViewChild('template', { static: true }) template: TemplateRef<any>;

  constructor(private modalService: BsModalService, 
    private _addDetailService: AddDetailsService, 
    private _helper: Helper,
    private _lang: LangService) { }

  show(): void {
    this.get_data()
    this.modalRef = this.modalService.show(this.template, this.config);
  }

  get_data(){
    this._addDetailService.get_country_list().then(res_data => {
      if(res_data.success){
        this.countries = res_data.countries
      }
    })
    this._addDetailService.get_delivery_list().then(res_data => {
      if(res_data.success){
        this.deliveries = res_data.deliveries
        let index = this.deliveries.findIndex(x =>  x.delivery_type === 2)
        if(index != -1){
          this.deliveries.splice(index, 1)
        }
      }
    })
  }

  makeUser(){
    this.user = true;
    this.data = false;
  }

  copyData(){
    this.user = false;
    this.data = true;
  }
  
  onCountrySelected(event){
    this.selectedCountry = event
    this._addDetailService.get_city_list({country_id: this.selectedCountry}).then(res_data => {
      this.cities = res_data.cities
    })
  }

  get_store_list(event){
    this._addDetailService.get_store_list_by_delivery({delivery_id: event}).then(res_data => {
      this.stores = res_data.stores
    })
  }

  onMakeUser(){
    let json = {
      city_id: this.city_id,
      country_id: this.selectedCountry,
      store_delivery_id: this.delivery,
      type: this.type,
      email: this.email,
      phone: this.phone,
      name: this.name
    }
    this.type = Number(this.type)

    if(this.type === 2 || this.type === 0){
      this._addDetailService.add_new_store(json).then(res_data => {
        if(res_data.success){
          this.store = res_data.store
        }
      })
    }
    if (this.type === 7 || this.type === 0){
      this._addDetailService.add_new_user(json).then(res_data => {
        if(res_data.success){
          this.user_detail = res_data.user
        }
      })
    }
    if (this.type === 8 || this.type === 0){
      this._addDetailService.add_new_provider(json).then(res_data => {
        if(res_data.success){
          this.provider = res_data.provider
          this.AddProviderVehicleData(res_data.provider._id);
        }
      })
    }
    this.city_id = null;
    this.selectedCountry = null;
    this.delivery = null;
    this.type = null
  }

  AddProviderVehicleData(providerid){
      let json = {
        provider_id: providerid
      }

      this._addDetailService.add_provider_vehicle_data(json).then(res_data => {
      })
  }

  onCopyStore(){
    let json = {
      present_store_id: this.copy_store,
      new_store_id: this.paste_store
    }

    this._addDetailService.update_database(json).then(res_data => {
      if(res_data.success){
        setTimeout(() => {
          this._addDetailService.update_new_item_database(json).then(res_data => {
          })
        }, 500)
        this.copy_store = null
        this.paste_store = null
      }
    })
  }

  onClose() {
    this.modalRef.hide()
  }
}
