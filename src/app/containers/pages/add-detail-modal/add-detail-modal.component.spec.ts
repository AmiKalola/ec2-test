import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AddDetailModalComponent } from './add-detail-modal.component';

describe('AddDetailModalComponent', () => {
  let component: AddDetailModalComponent;
  let fixture: ComponentFixture<AddDetailModalComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AddDetailModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddDetailModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
