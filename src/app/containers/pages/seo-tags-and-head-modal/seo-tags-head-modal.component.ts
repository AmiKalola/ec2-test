import { Component, TemplateRef, ViewChild } from '@angular/core';
import { UntypedFormArray, UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { AdminSettingService } from 'src/app/services/admin-setting.service';
import { Helper } from 'src/app/shared/helper';
import { LangService } from 'src/app/shared/lang.service';
@Component({
  selector: 'seo-tags-head-modal',
  templateUrl: './seo-tags-head-modal.component.html',
  styleUrls: ["./seo-tags-head-modal.component.scss"]
})
export class SeoTagsHeadModalComponent {
  modalRef: BsModalRef;
  config = {
    backdrop: true,
    ignoreBackdropClick: true,
    class: 'modal-right'
  };
  seoForm: UntypedFormGroup
  tabType: number
  tags= [{id:'h1', name:'H1'}, {id:'h2', name:'H2'}, {id:'h3', name:'H3'}, {id:'h4', name:'H4'}, {id:'h5', name:'H5'}, {id:'h6', name:'H6'}]

  @ViewChild('template', { static: true }) template: TemplateRef<any>;

  constructor(private modalService: BsModalService,
    private _lang: LangService,
    private helper : Helper,
    private _adminSettingsService: AdminSettingService) { }


  show(seoData, tabType): void {
    this.tabType = tabType
    this._initForm()
    if(seoData){
      if(tabType === 1){
        this.deliveryListTitle.patchValue(seoData.deliveryListTitle)
        this.nearStoreList.patchValue(seoData.nearStoreList)
        this.offerList.patchValue(seoData.offerList)
        this.deliveryListTitle.patchValue(seoData.deliveryListTitle)
        this.adsTitle.patchValue(seoData.adsTitle)
        this.nearStoreList.patchValue(seoData.nearStoreList)
        this.offerList.patchValue(seoData.offerList)
      } else if(tabType === 2) {
        this.menuTitle.patchValue(seoData.menuTitle)
        this.addressTitle.patchValue(seoData.addressTitle)
        this.hoursTitle.patchValue(seoData.hoursTitle)
      } else {
        this.homeTitle.patchValue(seoData.homeTitle)
        this.homeDescription.patchValue(seoData.homeDescription)
        this.storeTitle.patchValue(seoData.storeTitle)
        this.storeDescription.patchValue(seoData.storeDescription)
        this.providerTitle.patchValue(seoData.providerTitle)
        this.providerDescription.patchValue(seoData.providerDescription)
      }

      this.seoForm.patchValue({
        metaRobotsIndex: seoData.metaRobotsIndex,
        metaRobotsFollow: seoData.metaRobotsFollow,
        seoTitleTag: seoData.seoTitleTag,
        seoDescription: seoData.seoDescription,
        seoConicalUrl: seoData.seoConicalUrl,
        deliveryListHtmlTag: seoData.deliveryListHtmlTag || 'h3',
        advertiseHtmlTag: seoData.advertiseHtmlTag || 'h3',
        nearStoreListHtmlTag: seoData.nearStoreListHtmlTag || 'h3',
        offerTitleHtmlTag: seoData.offerTitleHtmlTag || 'h3',
        hoursTitleHtmlTag: seoData.hoursTitleHtmlTag || 'h3',
        menuTitleHtmlTag: seoData.menuTitleHtmlTag || 'h3',
        addressTitleHtmlTag: seoData.addressTitleHtmlTag || 'h3',
        homeDescriptionHtmlTag: seoData.homeDescriptionHtmlTag || 'h3',
        homeTitleHtmlTag: seoData.homeTitleHtmlTag || 'h3',
        providerDescriptionHtmlTag: seoData.providerDescriptionHtmlTag || 'h3',
        providerTitleHtmlTag: seoData.providerTitleHtmlTag || 'h3',
        storeDescriptionHtmlTag: seoData.storeDescriptionHtmlTag || 'h3',
        storeTitleHtmlTag: seoData.storeTitleHtmlTag || 'h3',
      })
    }
    this.modalRef = this.modalService.show(this.template, this.config);
  }

  _initForm(){
    this.seoForm = new UntypedFormGroup({
      metaRobotsIndex: new UntypedFormControl(false, Validators.required),
      metaRobotsFollow: new UntypedFormControl(false, Validators.required),
      seoTitleTag: new UntypedFormControl(null, Validators.required),
      seoDescription: new UntypedFormControl(null, Validators.required),
      seoConicalUrl: new UntypedFormControl(null),
      deliveryListTitle: new UntypedFormArray([]),
      deliveryListHtmlTag: new UntypedFormControl(null, Validators.required),
      adsTitle: new UntypedFormArray([]),
      advertiseHtmlTag: new UntypedFormControl(null, Validators.required),
      nearStoreList: new UntypedFormArray([]),
      nearStoreListHtmlTag: new UntypedFormControl(null, Validators.required),
      offerList: new UntypedFormArray([]),
      offerTitleHtmlTag: new UntypedFormControl(null, Validators.required),
      menuTitle: new UntypedFormArray([]),
      menuTitleHtmlTag: new UntypedFormControl(null, Validators.required),
      addressTitle: new UntypedFormArray([]),
      addressTitleHtmlTag: new UntypedFormControl(null, Validators.required),
      hoursTitle: new UntypedFormArray([]),
      hoursTitleHtmlTag: new UntypedFormControl(null, Validators.required),
      homeTitle: new UntypedFormArray([]),
      homeTitleHtmlTag: new UntypedFormControl(null, Validators.required),
      homeDescription: new UntypedFormArray([]),
      homeDescriptionHtmlTag: new UntypedFormControl(null, Validators.required),
      storeTitle: new UntypedFormArray([]),
      storeTitleHtmlTag: new UntypedFormControl(null, Validators.required),
      storeDescription: new UntypedFormArray([]),
      storeDescriptionHtmlTag: new UntypedFormControl(null, Validators.required),
      providerTitle: new UntypedFormArray([]),
      providerTitleHtmlTag: new UntypedFormControl(null, Validators.required),
      providerDescription: new UntypedFormArray([]),
      providerDescriptionHtmlTag: new UntypedFormControl(null, Validators.required),
    })
    this._lang.supportedLanguages.forEach(_language=>{
      this.deliveryListTitle.push(new UntypedFormControl(_language.code === 'en' ? "Delivery List" : "",_language.code === 'en' ? Validators.required : null))
      this.adsTitle.push(new UntypedFormControl(_language.code === 'en' ? "Advertise" : "",_language.code === 'en' ? Validators.required : null))
      this.nearStoreList.push(new UntypedFormControl(_language.code === 'en' ? "Stores Near You" : "",_language.code === 'en' ? Validators.required : null))
      this.offerList.push(new UntypedFormControl(_language.code === 'en' ? "Promo Codes" : "",_language.code === 'en' ? Validators.required : null))
      this.homeTitle.push(new UntypedFormControl(_language.code === 'en' ? "Unexpected Guest?" : "",_language.code === 'en' ? Validators.required : null))
      this.homeDescription.push(new UntypedFormControl(_language.code === 'en' ? "Order Food From Favorite Address Near You" : "",_language.code === 'en' ? Validators.required : null))
      this.menuTitle.push(new UntypedFormControl(_language.code === 'en' ? "Menu" : "",_language.code === 'en' ? Validators.required : null))
      this.addressTitle.push(new UntypedFormControl(_language.code === 'en' ? "Address" : "",_language.code === 'en' ? Validators.required : null))
      this.hoursTitle.push(new UntypedFormControl(_language.code === 'en' ? "Hours" : "",_language.code === 'en' ? Validators.required : null))
      this.storeTitle.push(new UntypedFormControl(_language.code === 'en' ? "Restaurants In Your Pocket" : "",_language.code === 'en' ? Validators.required : null))
      this.storeDescription.push(new UntypedFormControl(_language.code === 'en' ? "Order Food From Restaurant & Track On The Go" : "",_language.code === 'en' ? Validators.required : null))
      this.providerTitle.push(new UntypedFormControl(_language.code === 'en' ? "Restaurants In Your Pocket" : "",_language.code === 'en' ? Validators.required : null))
      this.providerDescription.push(new UntypedFormControl(_language.code === 'en' ? "Order Food From Restaurant & Track On The Go" : "",_language.code === 'en' ? Validators.required : null))
    })
  }

  get providerTitle(){
    return this.seoForm.get('providerTitle') as UntypedFormArray
  }

  get providerDescription(){
    return this.seoForm.get('providerDescription') as UntypedFormArray
  }

  get storeDescription(){
    return this.seoForm.get('storeDescription') as UntypedFormArray
  }

  get storeTitle(){
    return this.seoForm.get('storeTitle') as UntypedFormArray
  }

  get menuTitle(){
    return this.seoForm.get('menuTitle') as UntypedFormArray
  }

  get addressTitle(){
    return this.seoForm.get('addressTitle') as UntypedFormArray
  }

  get hoursTitle(){
    return this.seoForm.get('hoursTitle') as UntypedFormArray
  }

  get homeDescription(){
    return this.seoForm.get('homeDescription') as UntypedFormArray
  }

  get homeTitle(){
    return this.seoForm.get('homeTitle') as UntypedFormArray
  }

  get deliveryListTitle(){
    return this.seoForm.get('deliveryListTitle') as UntypedFormArray
  }

  get adsTitle(){
    return this.seoForm.get('adsTitle') as UntypedFormArray
  }

  get nearStoreList(){
    return this.seoForm.get('nearStoreList') as UntypedFormArray
  }

  get offerList(){
    return this.seoForm.get('offerList') as UntypedFormArray
  }

  updateSeoTags(){
    this.helper.checkAndCleanFormValues(this.seoForm)
    if(this.seoForm.invalid){
      this.seoForm.markAllAsTouched();
      return
    }
    this._adminSettingsService.updateSeoTags({type: this.tabType, ...this.seoForm.value}).then(result => {
      this.onClose();
      this._adminSettingsService._seoChanges.next(null);
    })
  }

  onClose(){
    this.modalRef.hide();
  }

}
