import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AddAppCertificateSettingComponent } from './add-app-certificate-setting.component';

describe('AddAppCertificateSettingComponent', () => {
  let component: AddAppCertificateSettingComponent;
  let fixture: ComponentFixture<AddAppCertificateSettingComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AddAppCertificateSettingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddAppCertificateSettingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
