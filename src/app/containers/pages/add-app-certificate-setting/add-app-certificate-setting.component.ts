import { Component, TemplateRef, ViewChild } from '@angular/core';
import { UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { AdminSettingService } from 'src/app/services/admin-setting.service';
@Component({
  selector: 'app-add-app-certificate-setting',
  templateUrl: './add-app-certificate-setting.component.html',
})
export class AddAppCertificateSettingComponent {
  modalRef: BsModalRef;
  config = {
    backdrop: true,
    ignoreBackdropClick: true,
    class: 'modal-right'
  };
  app_certificate_settings: UntypedFormGroup;

  @ViewChild('template', { static: true }) template: TemplateRef<any>;

  constructor(private modalService: BsModalService,private _adminSettingService: AdminSettingService) { }

  ngOnInit(): void {
    this._initForm()
  }

  show(app_certificate_settings=null): void {
    this.app_certificate_settings.patchValue(app_certificate_settings)
    this.modalRef = this.modalService.show(this.template, this.config);
  }

  _initForm() {
    this.app_certificate_settings = new UntypedFormGroup({
      user_certificate_mode: new UntypedFormControl(null,Validators.required),
      user_bundle_id: new UntypedFormControl(null),
      provider_certificate_mode: new UntypedFormControl(null,Validators.required),
      provider_bundle_id: new UntypedFormControl(null),
      store_certificate_mode: new UntypedFormControl(null,Validators.required),
      store_bundle_id: new UntypedFormControl(null)
    })
  }

  onSave(){
    this._adminSettingService.updateInstallationSetting(this.app_certificate_settings.value)
    this.modalRef.hide()
  }

}
