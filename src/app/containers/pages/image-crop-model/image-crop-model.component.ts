import { Component, EventEmitter, Output, TemplateRef, ViewChild } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { ImageCroppedEvent } from 'ngx-image-cropper';

@Component({
    selector:'app-image-crop-model',
    templateUrl:'./image-crop-model.component.html'
})
export class ImageCropModelComponent {

    modalRef: BsModalRef;
    config = {
      backdrop: false,
      ignoreBackdropClick: false,
      class: 'modal-popup'
    };

    @ViewChild('template', { static: true }) template: TemplateRef<any>;

    imageChangedEvent:Event;
    aspectRatio:number;
    resizeToWidth:number;
    @Output() updateImage = new EventEmitter<any>();

    image_file;

  constructor(private modalService: BsModalService) { }


  imageCropped(event: ImageCroppedEvent) {
    this.image_file = dataURLtoFile(event.base64,'file.png')
  }

  show(aspectRatio,resizeToWidth){
      this.aspectRatio = aspectRatio;
      this.resizeToWidth = resizeToWidth;
      this.modalRef = this.modalService.show(this.template, this.config)
  }

  confirm(){
    this.updateImage.emit(this.image_file)
    this.onClose()
  }

  onClose(){
    this.modalRef.hide()
  }

}

function dataURLtoFile(dataurl, filename) {

  let arr = dataurl.split(',');
  let mime = getFileTypes(arr);
  
  let bstr = atob(arr[1]);
  let n = bstr.length;
  let u8arr = new Uint8Array(n);

  while (n--) {
    u8arr[n] = bstr.charCodeAt(n);
  }

  return new File([u8arr], filename, { type: mime });
}


function getFileTypes(params: any): string | null {
  if (!Array.isArray(params) || typeof params[0] !== 'string') {
    return null;
  }
  const parts = params[0].split(';');
  // Check if it's a data URI with a valid MIME type
  if (parts.length > 1 && parts[0].startsWith('data:')) {
    const mimeType = parts[0].split(':')[1].split(',')[0]; // Extract MIME type from data URI
    return mimeType.trim().toLowerCase(); // Normalize MIME type
  } else {
    // Handle non-data URIs (extract MIME type from colon-separated string)
    return parts.length > 1 ? parts[0].trim().toLowerCase() : null; // Normalize MIME type
  }
}
