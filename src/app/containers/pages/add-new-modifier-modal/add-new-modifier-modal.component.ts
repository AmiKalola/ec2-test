import { Component, OnInit, TemplateRef, Input, ViewChild, Output, EventEmitter } from '@angular/core';
import { UntypedFormArray, UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { ModifierService , ModifierModel } from 'src/app/services/modifier.service';
import { Helper } from 'src/app/shared/helper';
import { LangService } from 'src/app/shared/lang.service';
@Component({
  selector: 'app-add-new-modifier-modal',
  templateUrl: './add-new-modifier-modal.component.html',
  styleUrls: ["./add-new-modifier-modal.component.scss"]
})
export class AddNewModifierModalComponent implements OnInit  {
  modalRef: BsModalRef;
  config = {
    backdrop: true,
    ignoreBackdropClick: true,
    class: 'modal-right'
  };
  @Input() delivery_type = this._helper.DELIVERY_TYPE_CONSTANT.STORE;

  isAddNewModifierItem=false
  is_edit:boolean = false;
  edit_id:string = null;
  modifierGroupForm:UntypedFormGroup;
  edit_modifierGroup_specifications = [];

  modifierForm:UntypedFormGroup;
  is_edit_modifier:boolean = false;
  edit_id_modifier:any = null

  @Output() updateData = new EventEmitter<any>();
  @ViewChild('template', { static: true }) template: TemplateRef<any>;

  constructor(private modalService: BsModalService,private modifierService:ModifierService,private _helper:Helper,private _lang:LangService) { }

  ngOnInit(){
    this._initForm()
  }

  _initForm(){
    this.modifierGroupForm = new UntypedFormGroup({
      sequence_number:new UntypedFormControl(0,Validators.required),
      specification_group_name:new UntypedFormArray([]),
      is_visible_in_store: new UntypedFormControl(true),
    })
    this.modifierForm = new UntypedFormGroup({
      name:new UntypedFormArray([]),
      price:new UntypedFormControl(0,Validators.required),
      sequence_number:new UntypedFormControl(0,Validators.required),
      is_visible_in_store: new UntypedFormControl(true),
    })
    this._lang.supportedLanguages.forEach(_language=>{
      this.specification_group_name.push(new UntypedFormControl(null,_language.code === 'en' ? Validators.required : null))
      this.name.push(new UntypedFormControl(null,_language.code === 'en' ? Validators.required : null))
    })
  }

  show(is_edit,edit_id): void {
    this._initForm()
    this.is_edit = is_edit
    this.edit_id = edit_id
    if(is_edit && edit_id !== null){
      this.loadModifierGroupData()
      this.modalRef = this.modalService.show(this.template, this.config);
    }else{
      this.edit_modifierGroup_specifications = [];
      this.is_edit_modifier = false;
      this.edit_id_modifier = null;
      this.modalRef = this.modalService.show(this.template, this.config);
    }
    this.isAddNewModifierItem = false
  }

  loadModifierGroupData(){
    this.modifierService.fetch(this.edit_id).then(data=>{
      if(data.success){
        let editData = data.specification_list;
        this.specification_group_name.patchValue(editData.name)
        this.modifierGroupForm.patchValue({
          sequence_number:editData.sequence_number,
          is_visible_in_store:editData.is_visible_in_store,
        })
        this.edit_modifierGroup_specifications  = editData.specifications
      }
    })
  }

  addNewModifierItem(is_edit_modifier,edit_id_modifier){
    this.is_edit_modifier = is_edit_modifier;
    this.edit_id_modifier = edit_id_modifier;
    this.isAddNewModifierItem = true;
    if(this.is_edit_modifier && this.edit_id_modifier !== null){
      this.name.patchValue(edit_id_modifier.name)
      this.modifierForm.patchValue({
        price:edit_id_modifier.price,
        sequence_number:edit_id_modifier.sequence_number,
        is_visible_in_store:edit_id_modifier.is_visible_in_store,
      })
    }
  }

  async onSubmitModifierGroup(){
    this._helper.checkAndCleanFormValues(this.modifierGroupForm);
    // if(this.mode)
    if(this.modifierGroupForm.valid){
      if(this.is_edit){
        let editModifierGroup = {
          ...this.modifierGroupForm.value
        };
        editModifierGroup._id = this.edit_id;
        editModifierGroup['name'] = editModifierGroup.specification_group_name
        delete editModifierGroup.specification_group_name;
        await this.modifierService.update_group(editModifierGroup)
        this.updateData.emit()
        this._initForm()

      }else{
          let specification_group_name = this.modifierGroupForm.value.specification_group_name;
          this.modifierGroupForm.value.specification_group_name = []
          this.modifierGroupForm.value.specification_group_name[0] = specification_group_name;
         await this.modifierService.add_group(this.modifierGroupForm.value)
         this.updateData.emit()
          this._initForm()

      }
    }else{
      this.modifierGroupForm.markAllAsTouched();
      return;
    }
    this.modalRef.hide()
  }

  onSubmitModifier(){
    this._helper.checkAndCleanFormValues(this.modifierForm);
    if(this.modifierForm.invalid){
      this.modifierForm.markAllAsTouched();
      return;
    }
    if(this.is_edit_modifier){
      let editModifier: ModifierModel = {
        _id:this.edit_id_modifier._id,
        specification_group_id:this.edit_id,
        specification_name:[{...this.modifierForm.value}],
      }
      this.modifierService.update_modifier(editModifier).then(()=>{
        this.loadModifierGroupData()
        this.isAddNewModifierItem = false;
        this.modifierForm.reset()
        this._initForm()
      })
    }else{
      let addModifier:ModifierModel = {
        _id:null,
        specification_group_id:this.edit_id,
        specification_name:[{...this.modifierForm.value}]
      }
      this.modifierService.add_modifier(addModifier).then(()=>{
        this.loadModifierGroupData()
        this.isAddNewModifierItem = false;
        this.modifierForm.reset()
        this._initForm()
      })
    }

  }

  get specification_group_name() {
    return this.modifierGroupForm.get('specification_group_name') as UntypedFormArray;
  }

  get name() {
    return this.modifierForm.get('name') as UntypedFormArray;
  }

  onModelClose(){
        this.modifierForm.reset()
        this.modifierGroupForm.reset()
        this.modalRef.hide()
  }

  onBack(){
    this.modifierForm.reset()
    this.isAddNewModifierItem=false
  }

}
