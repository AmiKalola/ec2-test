import { Component, EventEmitter, Output, TemplateRef, ViewChild } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { CommonService } from '../../../services/common.service';
import { VehicleService } from '../../../services/vehicle.service'
import { UntypedFormControl, UntypedFormGroup, NgForm, Validators } from '@angular/forms';
import { Helper } from 'src/app/shared/helper';
import { NotifiyService } from 'src/app/services/notifier.service';

@Component({
  selector: 'app-add-language-modal',
  templateUrl: './add-language-modal.component.html',
  styleUrls: ['./add-language-modal.component.scss']
})
export class AddNewLanguageComponent {
  modalRef: BsModalRef;
  config = {
    backdrop: true,
    ignoreBackdropClick: true,
    class: 'modal-right'
  };
  languageForm: UntypedFormGroup;
  admin_file: any;
  store_file: any;
  user_file: any;
  name: any = '';
  code: any = '';
  is_lang_rtl: boolean = true;
  urls: any;
  admin_url: any = '';
  store_url: any = '';
  user_url: any = '';
  langForm: NgForm;
  is_invalid: boolean = true;
  editLanguage:any;
  form_data: FormData;
  show_file_error: boolean = false;
  is_edit : boolean = false;

  @Output() onCloseModal: EventEmitter<any> = new EventEmitter();
  @Output() lang_data = new EventEmitter<any>();
  

  @ViewChild('template', { static: true }) template: TemplateRef<any>;


  constructor(private modalService: BsModalService, private _commonService: CommonService, private _vehicelService: VehicleService, private _helper: Helper,
     private _notifierService: NotifiyService) { }



  ngOnInit(): void {
    this.languageForm = new UntypedFormGroup({
      name: new UntypedFormControl(null, Validators.required),
      code: new UntypedFormControl(null, Validators.required),
      is_lang_rtl: new UntypedFormControl(false),
      files: new UntypedFormControl(null, Validators.required)
    })
  }

  show(): void {   
    this.languageForm = new UntypedFormGroup({
    name: new UntypedFormControl(null, Validators.required),
    code: new UntypedFormControl(null, Validators.required),
    is_lang_rtl: new UntypedFormControl(false),
    files: new UntypedFormControl(null, Validators.required)
  })
    this.modalRef = this.modalService.show(this.template, this.config);
    this.form_data = new FormData
  }

  onSelectImageFile(event, type) {
    const files = event.target.files;
    if (files.length === 0)
      return;
    const mimeType = files[0].type;
    if (mimeType.match(/json\/*/) == null) {
      this._notifierService.showNotification('error', this._helper._trans.instant('validation-title.only-JSON-are-supported'));
      return;
    }
    const fileReader = new FileReader();
    this.admin_url = files[0].name
    fileReader.readAsText(files[0], "UTF-8");
    fileReader.onload = () => {
      this.form_data.append('files', files[0]);
      this.show_file_error = false;
    }
    fileReader.onerror = (error) => {
    }
  }
  edit(language): void {
    this.is_edit = true
    this.languageForm = new UntypedFormGroup({
      name: new UntypedFormControl(language.name, Validators.required),
      code: new UntypedFormControl(language.code, Validators.required),
      is_lang_rtl: new UntypedFormControl(language.is_lang_rtl),
      files: new UntypedFormControl(null)
    })
    this.modalRef = this.modalService.show(this.template, this.config);
    this.form_data = new FormData
    this.form_data.append('_id', language._id)
  }


  closeModal() {
    this.modalRef.hide();
    this.admin_url = '';
    setTimeout(() => {
      this.languageForm.reset();
      this.is_edit = false;
    }, 500);
  }
  
  submit() {
    if (this.languageForm.invalid) {
      this.languageForm.markAllAsTouched();
    }
    if (this.languageForm.valid) {
      this.form_data.append('name', this.languageForm.value.name)
      this.form_data.append('code', this.languageForm.value.code)
      this.form_data.append('is_lang_rtl', this.languageForm.value.is_lang_rtl)
      if (this.is_edit) {
        this._commonService.edit_language(this.form_data).then(res => {
          this.form_data = new FormData;
          this.closeModal();
          this.show_file_error = false;
          if (res.success) {
            this.lang_data.emit();
            window.location.reload()
          }
        })
      } else {
        if (this.form_data.get('files')) {
          this._commonService.add_language(this.form_data).then(res => {
            this.form_data = new FormData;
            this.closeModal();
            this.show_file_error = false;
            if (res.success) {
              this.lang_data.emit();
              window.location.reload()
            }
          })
        }else{
          this.show_file_error = true;
        }
      }
    }
  }

}