import { Component, TemplateRef,  ViewChild } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { EarningService } from 'src/app/services/earning.service';
import { Helper } from 'src/app/shared/helper';

@Component({
  selector: 'app-store-earning-detail',
  templateUrl: './store-earning-detail.component.html',
})
export class StoreEarningDetailComponent  {
  modalRef: BsModalRef;
  config = {
    backdrop: true,
    ignoreBackdropClick: true,
    class: 'modal-right'
  };
  orderPayment;
  store;
  start_date;
  end_date;

  @ViewChild('template', { static: true }) template: TemplateRef<any>;

  constructor(private modalService: BsModalService,private _earningService:EarningService,public _helper:Helper) { }


  show(store_id,start_date=null,end_date=null): void {

    this.start_date = start_date;
    this.end_date = end_date;
    this._earningService.fetch_store_earning({store_id:store_id,start_date:start_date,end_date:end_date,page:1,search_field:'',search_value:'',type:1}).then(data=>{
      if(data['success']){
        this.orderPayment = data['order_total'];
        this.store = data['store'];
        this.modalRef = this.modalService.show(this.template, this.config);
      }
    })
  }


  closeModel(){
    this.modalRef.hide();
  }

}
