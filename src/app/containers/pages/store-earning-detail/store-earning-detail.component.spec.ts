import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { StoreEarningDetailComponent } from './store-earning-detail.component';

describe('StoreEarningDetailComponent', () => {
  let component: StoreEarningDetailComponent;
  let fixture: ComponentFixture<StoreEarningDetailComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ StoreEarningDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StoreEarningDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
