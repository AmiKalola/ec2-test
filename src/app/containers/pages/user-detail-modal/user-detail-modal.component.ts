import { Component, EventEmitter, Output, TemplateRef,  ViewChild } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { User } from 'src/app/models/user.model';
import { UserService, user_page_type } from '../../../services/user.service'
import { Helper } from 'src/app/shared/helper';
import { ImageCropModelComponent } from '../image-crop-model/image-crop-model.component';
import { CommonService } from 'src/app/services/common.service';
import { NgForm } from '@angular/forms';
import { AddWalletModelComponent } from '../add-wallet-modal/add-wallet-modal.component';
import { NotifiyService } from 'src/app/services/notifier.service';

@Component({
  selector: 'user-detail-modal',
  templateUrl: './user-detail-modal.component.html',
  styleUrls: ["./user-detail-modal.component.scss"]
})

export class UserDetailModalComponent  {
  @ViewChild('userForm', { static: true }) userForm: NgForm;
  showPassword: boolean = false; // password eye icon boolean

  base64Image: any;
  fiveRate=5;
  modalRef: BsModalRef;
  confirmModelRef: BsModalRef;
  config = {
    backdrop: true,
    ignoreBackdropClick: true,
    class: 'modal-right'
  };
  confirmationModalConfig = {
    backdrop: true,
    ignoreBackdropClick: true,
  };
  isChatTab = false;
  image_type: number;
  todayDate = new Date()
  user_detail: User = {
    user_id: '',
    first_name: '',
    last_name: '',
    phone: '',
    country_phone_code: '',
    email: '',
    image_url: '',
    password: '',
    is_use_wallet: false,
    wallet: 0,
    wallet_currency_code: '',
    created_at: '',
    app_version: 'any',
    device_type: '',
    referral_code: '',
    store_rate: 0,
    provider_rate: 0,
    comment: '',
    is_email_verified: false,
    is_phone_verified: false,
    unique_id:0
  };
  user_page_type: any = user_page_type;
  documents: any[] = [];
  referral_history: any[] = [];
  review_list: any[] = [];
  image_settings: any;
  user_image: any;
  upload_user_image: any = '';
  upload_document_image: any = ''
  form_data: FormData;
  document_form_data: FormData;
  selectedDocumentIndex: any;
  is_edit: boolean = false
  show_review_type = 1;
  DOCUMENT_DEFAULT_IMAGE: any;
  minimum_length: number;
  maximum_length: number;
  timezone_for_display_date:string = '';
  approveModalConfig = {
    backdrop: true,
    ignoreBackdropClick: true,
  };
  approveModelRef: BsModalRef;
  selectedUser : any ;
  approvalStatus : any ;
  unique_id:number;
  
  @Output() updatedData = new EventEmitter<any>();
  
  @ViewChild('approveTemplate', { static: true }) approveTemplate: TemplateRef<any>;
  @ViewChild('cropModel', { static: true }) cropModel: ImageCropModelComponent;
  @ViewChild('template', { static: true }) template: TemplateRef<any>;
  @ViewChild('confirmationTemplate', { static: true }) confirmationTemplate: TemplateRef<any>;
  @ViewChild('walletAmount', {static: true}) walletAmount: AddWalletModelComponent

  constructor(public userService: UserService, 
    private modalService: BsModalService, 
    public _helper: Helper,
    private _commonService: CommonService,
    private _notifierService: NotifiyService
    ) { 
      this.DOCUMENT_DEFAULT_IMAGE = this._helper.DEFAULT_IMAGE_PATH.DOCUMENT
      this._helper.display_date_timezone.subscribe(data => {
        this.timezone_for_display_date = data;
      })
    }


  show(user): void {
    this.showPassword =false;
    this.form_data = new FormData;
    this.document_form_data = new FormData;
    this.unique_id = user.unique_id;
    this.get_user_detail(user._id)
    this.fetch_image_settings()
    this.modalRef = this.modalService.show(this.template, this.config);
    this.subadmineditpermission();
  }

  //sub admin edit permission is false then profile form disable
  subadmineditpermission(){
    setTimeout(() => {
      if(!this._helper.has_permission(this._helper.PERMISSION.EDIT)){
        let elements : any = document.getElementsByTagName('form')[0]?.elements;
        for(const element of elements){
          if(element.tagName != 'BUTTON'){
            element.setAttribute('disabled' , 'true')
          }
        }
      }
    }, 300);
  }

  fetch_image_settings() {
    this._commonService.fetch_image_settings().then(res_data => {
      if (res_data.success) {
        this.image_settings = res_data.image_setting
      }
    })
  }

  get_user_detail(user_id){
    this.userService.getUserDetail({user_id: user_id}).then(res_data => {
      this.user_detail = res_data.user;
      this.user_detail.user_id = res_data.user._id;
      this.user_detail.password = '';
      this.minimum_length = res_data.minimum_phone_number_length;
      this.maximum_length = res_data.maximum_phone_number_length;
      this.upload_user_image = this._helper.image_url + this.user_detail.image_url
    })
  }

  approveDecline(user, type){
    this.selectedUser = user ;
    this.approvalStatus = type ;
    this.onClose();
    this.approveModelRef = this.modalService.show(this.approveTemplate, this.approveModalConfig);
  }

  approve(){
    let data = {
      "user_page_type": this.approvalStatus === user_page_type.approved ? 1 : 2,
      user_id: this.selectedUser._id
    }
    this.userService.approveDeclineUser(data).then(res_data => {
      this.onClose();
      this.approveModelRef.hide();
    })
  }

  cancelAprroveModal(){
    this.approveModelRef.hide();
    this.modalRef = this.modalService.show(this.template, this.config);
    this.subadmineditpermission();
  }

  updateUser(user_id,userForm){
    this.user_detail.last_name = this.user_detail.last_name.toString().trim();
    this.user_detail.first_name = this.user_detail.first_name.toString().trim();
    if(!this.user_detail.first_name || !this.user_detail.last_name){
       userForm.submitted = true;
       return
    }

    if (userForm.invalid) {
      userForm.submitted = true;
      return;
    }
    this.form_data.append('user_id', this.user_detail.user_id)
    this.form_data.append('first_name', this.user_detail.first_name)
    this.form_data.append('last_name', this.user_detail.last_name)
    this.form_data.append('phone', this.user_detail.phone)
    this.form_data.append('country_phone_code', this.user_detail.country_phone_code)
    this.form_data.append('email', this.user_detail.email)
    this.form_data.append('password', this.user_detail.password)
    this.form_data.append('is_use_wallet', String(this.user_detail.is_use_wallet))
    this.form_data.append('wallet', String(this.user_detail.wallet))
    this.form_data.append('wallet_currency_code', this.user_detail.wallet_currency_code)
    this.form_data.append('created_at', this.user_detail.created_at)
    this.form_data.append('app_version', this.user_detail.app_version)
    this.form_data.append('device_type', this.user_detail.device_type)
    this.form_data.append('referral_code', this.user_detail.referral_code)
    this.form_data.append('store_rate', String(this.user_detail.store_rate))
    this.form_data.append('provider_rate', String(this.user_detail.provider_rate))
    this.form_data.append('comment', this.user_detail.comment)
    this.form_data.append('is_email_verified', String(this.user_detail.is_email_verified))
    this.form_data.append('is_phone_verified', String(this.user_detail.is_phone_verified))
    this.userService.updateUserDetail(this.form_data).then(res_data => {
      this.onClose();
      this.form_data = new FormData
      userForm.submitted = false;
    })
  }


  get_document_list(){
    this.is_edit=false;

    let data = {
      id: this.user_detail.user_id,
      type: this._helper.USER_TYPE.USER
    }
    this.userService.getDocumentList(data).then(res_data => {
      if(res_data.success){

        this.documents = res_data.documents;
        this.documents.forEach(document => {
          if(document.expired_date){
            document.expired_date = new Date(document.expired_date)
          }
          document.image_url = this._helper.image_url + document.image_url
        })
      } else {
        this.documents = [];
      }
    })
  }

  update_document(document){
    
    if(((document.is_expired_date && !document.expired_date) || (document.is_unique_code && !document.unique_code) )){
      document.dataRequired = true;
      return;
    }
    if((document.image_url == this._helper.image_url) && document.is_mandatory){
      this._notifierService.showNotification('error', this._helper._trans.instant('validation-title.please_upload_image'));
      return;
    }
    
    if(document.expired_date != null){
      this.document_form_data.append('expired_date',document.expired_date)
    }
    this.document_form_data.append('id',this.user_detail.user_id)
    this.document_form_data.append('type',String(this._helper.USER_TYPE.USER))
    this.document_form_data.append('unique_code',document.unique_code)
    this.document_form_data.append('document_id',document._id)
    this.userService.updateDocumentDetail(this.document_form_data).then(res_data => {
      document.is_edit = false;
      this.selectedDocumentIndex = '';
      this.is_edit = false
      document.dataRequired = false;
      this.document_form_data = new FormData;
    })
  }

  get_referral_usage_list(){
    let data = {
      id: this.user_detail.user_id,
      type: this._helper.USER_TYPE.USER
    }
    this.userService.getReferralUsageList(data).then(res_data => {
      this.referral_history = res_data.referral_history;
    })
  }

  get_review_usage_list(){
    let data = {
      user_id: this.user_detail.user_id
    }
    this.userService.getReviewList(data).then(res_data => {
      this.review_list = res_data.review_list;
    })
  }

  onSelectImageFile(event, type) {  //1: profile, 2: document
    this.image_type = type
    let files = event.target.files;
    if (files.length === 0){
      return;
    }
    const mimeType = files[0].type;
    let fileType;

    if(type==1 || type !== 1) {
      fileType = this._helper.uploadFile.filter((element) => {
        return mimeType == element;
      })
    }else {
      fileType = this._helper.uploadDocFile.filter((element) => {
        return mimeType == element;
      })
    }

    if (fileType != mimeType) {
      if(type==1 || type !== 1){
        this._notifierService.showNotification('error',this._helper._trans.instant('validation-title.invalid-image-format'));
        return;
      }
    }else{
      this.imageCropped(files[0])
    }

  }

  imageCropped(event) {
    const reader = new FileReader();
    reader.readAsDataURL(event);
    this.user_image = event;
    if(this.image_type === 1){
      this.form_data.append('image_url', this.user_image)
    } else {
      this.document_form_data.append('image_url', this.user_image)
    }
    reader.onload = (_event) => {
      if(this.image_type === 1){
        this.upload_user_image = reader.result
      } else {
        this.documents[this.selectedDocumentIndex].image_url = reader.result
      }
    }
  }

  onEdit(document, index){
    this.selectedDocumentIndex = index
    document.is_edit=true
    this.is_edit = true
  }

  onAddWallet(){
    this.onClose()
    this.walletAmount.show()
  }

  addWalletAmount(amount){
    if(amount != 'close_modal'){
    let json = {
      type: this._helper.USER_TYPE.USER,
      user_id: this.user_detail.user_id,
      wallet: amount
    }
    this._commonService.add_wallet_amount(json)
    }else{
      this.modalRef = this.modalService.show(this.template, this.config)
      this.subadmineditpermission();
    }
  }

  // changes starts from here........

  downloadImage(value,imageName) {
    let imageUrl = value;
    let split_image_url = imageUrl.split(this._helper.image_url)
    if(split_image_url[1] != ''){
      // window.open(image_url)
    }
    this._helper.downloadUrl(imageUrl)
    .subscribe({
      next :imgData =>{
        this._helper.downloadImage(imgData, imageName)
      },
      error :err => console.log(err)
    });
  }

  // changes end here..........

  deleteAccount(){
    this.onClose()
    this.confirmModelRef = this.modalService.show(this.confirmationTemplate, this.confirmationModalConfig)
  }

  cancel(){
    this.confirmModelRef.hide()
    this.modalRef = this.modalService.show(this.template, this.config)
    this.subadmineditpermission();
  }

  async confirm(){
    await this.userService.deleteAccount({user_id: this.user_detail.user_id})
    this.confirmModelRef.hide()
    this.updatedData.emit()
  }

  onClose() {
    this.modalRef.hide()
    this.footerHide= false;
  }

  footerHide:boolean=false;

  onSelectTab(boolean){
    if(boolean === true){
      this.footerHide= true;
    }else{
      this.footerHide= false;
    }
  }
}
