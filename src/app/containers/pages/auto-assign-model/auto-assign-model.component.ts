import { Component, EventEmitter, Input, Output, TemplateRef, ViewChild } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { OrderService } from 'src/app/services/order.service';
import { Helper } from '../../../shared/helper'
import { DELIVERY_TYPE_CONSTANT } from '../../../shared/constant';

@Component({
    selector:'app-auto-assign-model',
    templateUrl:'./auto-assign-model.component.html',
    styleUrls:['./auto-assign-model.component.css']
})
export class AutoAssignModelComponent {

    modalRef: BsModalRef;
    config = {
      backdrop: true,
      ignoreBackdropClick: false,
      class: 'modal-popup modal-sm'
    };

    @ViewChild('template', { static: true }) template: TemplateRef<any>;
    @Input() isEstimateTime:boolean = false;
    @Output() assignType = new EventEmitter<any>();

    estimateTime:number = 1;
    type: number = 1;
    isShowVehicle = false;
    vehicles = [];
    providers = [];
    isShowProviders = false;
    orderId = null;
    selectedVehicle;
    selectedProvider;
    delivery_type:number;
    is_admin_services:boolean=false;
   selected_vehicle: any[];
   DELIVERY_TYPE_CONSTANT = DELIVERY_TYPE_CONSTANT;


  constructor(private modalService: BsModalService,private _helper:Helper,private orderservice:OrderService) { }

  show(orderId=null,isEstimateTime = false, vehicles_list = [], delivery_type = 1, is_admin_services = false){
    this.delivery_type = delivery_type;
    this.is_admin_services = is_admin_services;
    this.onReset();
    this.isEstimateTime = isEstimateTime;
    this.orderId = orderId;
    this.vehicles = vehicles_list
    if(this.orderId.delivery_type == 2 || this.orderId.delivery_type == 4){
      this.orderservice.get_request_data({request_id:this.orderId.request_id}).then((result)=>{
        this.selected_vehicle = this.vehicles.filter((value)=>{
           return value._id == result.request.vehicle_id
         })
         this.selectedVehicle = this.selected_vehicle[0]._id
       })
    }
    this.modalRef = this.modalService.show(this.template, this.config)
  }

  onSelectDriver(){
  }

  onAssign(type = 1){
    this.type = type;
    let data = {
      type, 
      selectedVehicle: this.selectedVehicle,
      is_admin_services: this.is_admin_services,
    }
    this.assignType.emit(data)
    this.modalRef.hide()
  }

  onReset(){
    this.isEstimateTime = false;
    this.isShowProviders = false;
    this.isShowVehicle = false;
    this.orderId = null;
    this.selectedVehicle = null;
    this.selectedProvider = null;
    this.estimateTime = 1;
  }

  onClose() {
    this.modalRef.hide()
    this.onReset();
  }

}
