import { Component, TemplateRef,  ViewChild } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { CityService } from 'src/app/services/city.service';
import { CountryService } from 'src/app/services/country.service';
import { DeliveryFeesService } from 'src/app/services/delivery-fees.service';
import { StoreService } from 'src/app/services/store.service';
import { DELIVERY_TYPE_CONSTANT } from 'src/app/shared/constant';
import { Helper } from '../../../shared/helper';
@Component({
  selector: 'set-store-id-model',
  templateUrl: './set-store-id-model.component.html',
  styleUrls: ['./set-store-id-model.component.scss'],
  styles: [`
      ul.category-dropdown{
        max-height: 30vh;
        overflow: auto;
      }
      .dropdown-menu-right{
          right: 0 !important;
      }
      .category-input{
        border-top-left-radius: 100px; border-bottom-left-radius: 100px
      }
  `]
})
export class SetStoreModalComponent  {
  modalRef: BsModalRef;
  stores = [];
  selectedStore;
  config = {
    backdrop: true,
    ignoreBackdropClick: true,
    class: 'modal-right'
  };
  delivery_data: any = []
  deliveries_list: any = []
  store_search_text: any = ''
  selected_delivery: any
  temp_stores: any = [];
  business_type: any = [
    { title: 'Delivery Merchant', id: 1 },
    // { title: 'Admin Services', id: 5 },
    { title: 'Service Merchant', id: 6 }
  ]
  selected_business_type: number= 1;
  temp_delivery_list: any = [];
  country_list: any = [];
  selectedCountryId: string;
  business_type_list: any = [];
  city_list: any = [];
  selectedCityId: string = "all";

  selected_country:any = '000000000000000000000000';
  country_name: any = '';
  city_name: any = '';
  selected_city = '000000000000000000000000';

  @ViewChild('template', { static: true }) template: TemplateRef<any>;

  constructor(private modalService: BsModalService,private _storeService:StoreService,private _helper:Helper , private _deliverFeesServcise: DeliveryFeesService,  private _cityService: CityService, private _deliveryFeesService: DeliveryFeesService, private _countryService: CountryService, ) { }

  show(): void {
    this._helper.isStoremodelOpen = true;
    this._storeService.getStoreListForMap().then(data=>{
        if(data.success){
            this.stores = data.stores;
            this.temp_stores = this.stores
            this.temp_delivery_list = data.deliveries;
            this.onBusinessChange(this.selected_business_type)
            this.selected_delivery = this.deliveries_list[0]
            this.onBusinessChange(this.selected_business_type)
            if(this.stores && this.stores.length){
                this.selectedStore = this.stores[0]._id;
            }
        }
    })
    this.modalRef = this.modalService.show(this.template, this.config);
    this.getCountryList();
    if(this.selected_country !== '000000000000000000000000'){
    this._deliveryFeesService.get_city_lists(this.selected_country).then(res => {
      this.city_list = []
      if (res && res.success) {
        this.city_list = res.cities
        if (this.city_list.length == 1) {
          this.selected_city = this.city_list[0]._id
          this.city_name = this.city_list[0].city_name;
        }
      }
    })
    }
  }

  filterStores(){
    this.stores = this.temp_stores.filter(x => x.store_delivery_id === this.selected_delivery._id)
    this.filter();
  }

  selectStore(store, type){
    this._helper.selected_city = {
      _id: null,
      city_name: []
    };
      this._helper.changeSelectStore(store, type)
      this.onClose()
  }

  ondeliveryChange(delivery){
    this.selected_delivery = delivery
    this.filterStores()
  }

  onClose(){
    this.store_search_text = ''
    this.modalRef.hide()
    this._helper.isStoremodelOpen = false;
    this.selected_country = '000000000000000000000000';
    this.selected_city = '000000000000000000000000';
    this.city_list = [];
  }

  onBusinessChange(event){
    if(event === DELIVERY_TYPE_CONSTANT.STORE){
      this.deliveries_list = this.temp_delivery_list.filter(x => x.delivery_type === DELIVERY_TYPE_CONSTANT.STORE)
    } else {
      this.deliveries_list = this.temp_delivery_list.filter(x => x.delivery_type === DELIVERY_TYPE_CONSTANT.SERVICE || x.delivery_type === DELIVERY_TYPE_CONSTANT.APPOINMENT)
    }
    this.selected_delivery = this.deliveries_list[0]
    this.filterStores()
  }

  onChangeCountry(country): void {
    if(this.selected_country != '000000000000000000000000'){
      if(this.selected_country=='000000000000000000000000'){
        this.country_name = '';
        this.city_list = [];
        this.city_name = ''
        this.selected_city = '000000000000000000000000'
      } else {
        let index = this.country_list.findIndex(x => x._id === this.selected_country)
        this.country_name = this.country_list[index].country_name
        this.getCityList()
      }
      this.filterStores()
      this.filter()
    }
  }

  onChangeCity(city): void {
    if(this.selected_city != '000000000000000000000000'){
      let index = this.city_list.findIndex(x => x._id === this.selected_city)
      if(index != -1){
        this.city_name = this.city_list[index].city_name
      }
      this.filterStores()
     this.filter()
    }
  }

  getCountryList() {
    this._countryService.fetch().then(res => {
      if (res.success) {
        this.country_list = res.countries
        this.selected_country = '000000000000000000000000'
        this.country_name = ''
        if(this.country_list.length==1){
          this.selected_country=this.country_list[0]._id
          this.country_name=this.country_list[0].country_name;
        }
        this.getCityList()
      }
    })
  }

  getCityList() {
    this.city_name = ''
    this.selected_city = '000000000000000000000000'
    if (this.selected_country != '000000000000000000000000') {
      this._deliveryFeesService.get_city_lists(this.selected_country).then(res => {
        this.city_list = []
        if (res && res.success) {
          this.city_list = res.cities
          if (this.city_list.length == 1) {
            this.selected_city = this.city_list[0]._id
            this.city_name = this.city_list[0].city_name;
          }
        }
      })
    }
  }

  filter() {
    this._helper.ngZone.run(() => {
      this.stores = this.stores.filter((ads) => {
        if ((ads.country_id == this.selected_country || this.selected_country == '000000000000000000000000') && (ads.city_id == this.selected_city || this.selected_city == '000000000000000000000000')) {
          return true;
        }
      })
      this.stores = this.stores.filter((ads) => {
        if ((ads.country_id == this.selected_country || this.selected_country == '000000000000000000000000') && (ads.city_id == this.selected_city || this.selected_city == '000000000000000000000000')) {
          return true;
        }
      })
    })
  }

}
