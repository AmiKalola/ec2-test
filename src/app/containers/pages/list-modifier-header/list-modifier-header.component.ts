import { Component,  ViewChild, EventEmitter, Output, Input } from '@angular/core';
import { Helper } from 'src/app/shared/helper';
import { DELIVERY_TYPE } from 'src/app/views/app/constant';

@Component({
  selector: 'app-list-modifier-header',
  templateUrl: './list-modifier-header.component.html'
})
export class ListModifierHeaderComponent {
  displayOptionsCollapsed = false;
  DELIVERY_TYPE = DELIVERY_TYPE

  @Input() showSearch = true;
  @Input() displayMode = 'list';
  @Input() delivery_type = this._helper.DELIVERY_TYPE_CONSTANT.STORE;

  @Output() changeDisplayMode: EventEmitter<string> = new EventEmitter<string>();
  @Output() addNewItem: EventEmitter<any> = new EventEmitter();
  @Output() selectAllChange: EventEmitter<any> = new EventEmitter();
  @Output() searchKeyUp: EventEmitter<any> = new EventEmitter();
  @Output() itemsPerPageChange: EventEmitter<any> = new EventEmitter();
  @Output() changeOrderBy: EventEmitter<any> = new EventEmitter();
  @Output() onChangeStore: EventEmitter<any> = new EventEmitter();

  @ViewChild('search') search: any;
  constructor(public _helper: Helper) { }



  onSelectDisplayMode(mode: string): void {
    this.changeDisplayMode.emit(mode);
  }
  onAddNewItem(): void {
    this.addNewItem.emit(null);
  }
  selectAll(event): void  {
    this.selectAllChange.emit(event);
  }
  onChangeItemsPerPage(item): void  {
    this.itemsPerPageChange.emit(item);
  }

  onChangeOrderBy(item): void  {
    this.changeOrderBy.emit(item);
  }

  onSearchKeyUp($event): void {
    this.searchKeyUp.emit($event);
  }

  on_change_store(){
    this._helper.selected_store_id =  null
    this.onChangeStore.emit()
  }
}
