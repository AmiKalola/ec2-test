import { Component, TemplateRef, ViewChild } from '@angular/core';
import { UntypedFormArray, UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { AdminSettingService } from 'src/app/services/admin-setting.service';
import { Helper } from 'src/app/shared/helper';
import { LangService } from 'src/app/shared/lang.service';
@Component({
  selector: 'twitter-graph-modal',
  templateUrl: './twitter-graph-modal.component.html',
  styleUrls: ["./twitter-graph-modal.component.scss"]
})
export class TwitterGraphModalComponent {
  modalRef: BsModalRef;
  twitterGraphForm: UntypedFormGroup
  tabType: boolean = false
  formData: FormData
  config = {
    backdrop: true,
    ignoreBackdropClick: true,
    class: 'modal-right'
  };
  twitterImageUrlFile: any;

  @ViewChild('template', { static: true }) template: TemplateRef<any>;

  constructor(private modalService: BsModalService,
    private _lang: LangService,
    public _helper: Helper,
    public _adminSettingsService: AdminSettingService) { }


  show(seoData, tabType): void {
    this.twitterImageUrlFile = null
    this.tabType = tabType
    this.twitterGraphForm = new UntypedFormGroup({
      twitterTitle: new UntypedFormArray([]),
      twitterDescription: new UntypedFormArray([]),
      twitterUrl: new UntypedFormControl(seoData.twitterUrl || "", Validators.required),
      twitterCard: new UntypedFormControl(seoData.twitterCard || "", Validators.required),
      twitterImage: new UntypedFormControl(null, Validators.required),
    })
    this._lang.supportedLanguages.forEach((_language, index) => {
      this.twitterTitle.push(new UntypedFormControl(seoData.twitterTitle[index] || "",_language.code === 'en' ? Validators.required : null))
      this.twitterDescription.push(new UntypedFormControl(seoData.twitterDescription[index] || "",_language.code === 'en' ? Validators.required : null))
    })
    this.modalRef = this.modalService.show(this.template, this.config);
  }

  get twitterTitle(){
    return this.twitterGraphForm.get('twitterTitle') as UntypedFormArray;
  }

  get twitterDescription(){
    return this.twitterGraphForm.get('twitterDescription') as UntypedFormArray;
  }

  updateTwitterTags(){
    this.formData = new FormData()
    this.formData.append('type', String(this.tabType))

    this.twitterGraphForm.value.twitterTitle.forEach(element => {
      this.formData.append('twitterTitle', element)
    });
    this.twitterGraphForm.value.twitterDescription.forEach(element => {
      this.formData.append('twitterDescription', element)
    });
    this.formData.append('twitterUrl', this.twitterGraphForm.value.twitterUrl)
    this.formData.append('twitterCard', this.twitterGraphForm.value.twitterCard)
    if (this.twitterImageUrlFile) {
      this.formData.append('twitterImage', this.twitterImageUrlFile)
    }
    this._adminSettingsService.updateSeoTags(this.formData).then(result => {
      this.onClose();
      this._adminSettingsService._seoChanges.next(null);
    })
  }

  onSelectFile(event) {
    const files = event.target.files;
    if (files.length === 0)
      return;

    const fileType = files[0].type;

    if (fileType.match(/image\/*/) == null) {
      alert("Only images are supported.")
      return;
    }

    this.twitterImageUrlFile = files[0]
  }

  onRemoveSelectedFile() {
    this.twitterImageUrlFile = null
    let fileInput: any = document.getElementById("inputGroupFile02")
    fileInput.value = "";
  }

  onClose() {
    this.modalRef.hide()
  }

}
