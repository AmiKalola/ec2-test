import { Component, EventEmitter, Output, TemplateRef, ViewChild } from '@angular/core';
import { AbstractControl, UntypedFormArray, UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { CountryService } from 'src/app/services/country.service';
import { Helper } from 'src/app/shared/helper';
import { LangService } from 'src/app/shared/lang.service';
@Component({
  selector: 'add-new-tax-modal',
  templateUrl: './add-new-tax-modal.component.html',
})
export class AddNewTaxModalComponent {
  modalRef: BsModalRef;
  config = {
    backdrop: true,
    ignoreBackdropClick: true,
    class: 'modal-right'
  };
  taxForm: UntypedFormGroup
  is_edit: boolean = false
  country_id: any
  tax_id: any;

  @ViewChild('template', {static: true}) template: TemplateRef<any>
  @Output() addTax = new EventEmitter<any>()

  constructor(private modalService: BsModalService, private _lang: LangService, public _helper: Helper, private _countryService: CountryService) { }

  show(tax , country_id): void {
    if(!tax){
      tax = null
    }
    this.country_id = country_id
    this.taxForm = new UntypedFormGroup({
      tax: new UntypedFormControl(null,[Validators.required,this.profitValueValidation]),
      is_tax_visible: new UntypedFormControl(true),
      tax_name: new UntypedFormArray([]),
    })
    this._lang.supportedLanguages.forEach(_language=>{
      this.tax_name.push(new UntypedFormControl("",_language.code === 'en' ? Validators.required : null))
    })
    if(tax){
      this.is_edit = true
      this.tax_id = tax._id
      
      this.taxForm.patchValue({
        tax: tax.tax,
        is_tax_visible: tax.is_tax_visible,
        tax_name: tax.tax_name
      })
    } else {
      this.is_edit = false
    }
    
    this.modalRef = this.modalService.show(this.template, this.config);
  }

  get tax_name(){
    return this.taxForm.get('tax_name') as UntypedFormArray
  }

  onSubmit(){
    if(this.taxForm.invalid){
      this.taxForm.markAllAsTouched();
      return;
    }
    let json = {
      tax_name: this.taxForm.value.tax_name,
      tax: this.taxForm.value.tax,
      is_tax_visible: this.taxForm.value.is_tax_visible,
      country_id: this.country_id
    }
    if(this.is_edit){
      let json_data = {
        _id: this.tax_id,
        ...json
      }
      this._countryService.edit_tax(json_data).then(res_data => {
        if(res_data.success){
          this.addTax.emit(res_data.res_data)
        }
      })
    } else {
      this._countryService.add_tax(json).then(res_data => {
        if(res_data.success){
          this.addTax.emit(res_data.res_data)
        }
      })
    }

    this.modalRef.hide()
    this.taxForm.reset()
  }
  
  profitValueValidation(control: AbstractControl): any {
    if(control.value <= 0){
      return { minValue: true }
    }
    if (control.value > 100) {
      return { maxValue: true }
    }
    return null
  }


  handleKeyPress(event: KeyboardEvent) {
    const input = event.target as HTMLInputElement;
    const value = input.value;

    // Regular expression to match the input format: digits followed by up to 3 digits after a decimal point
    const regex = /^\d*\.?\d{0,2}$/;

    // Check if the input value matches the regular expression
    if (parseFloat(value) > 100 || !regex.test(value)) {
      event.preventDefault(); // Prevent further input
  }
}


  onClose(){
    this.modalRef.hide()
  }

}
