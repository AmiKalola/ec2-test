import { Component, TemplateRef,  ViewChild } from '@angular/core';
import { UntypedFormArray, UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { NotifiyService } from 'src/app/services/notifier.service';
import { CityService } from 'src/app/services/city.service';
import { CountryService } from 'src/app/services/country.service';
import { SubAdminService } from 'src/app/services/sub-admin.service';
import { Helper } from 'src/app/shared/helper';
import { SubAdminUrl } from 'src/app/shared/sub-admin-url';
@Component({
  selector: 'app-add-sub-admin-modal',
  templateUrl: './add-sub-admin-modal.component.html',
  styleUrls: ['./add-sub-admin-modal.component.scss']
})
export class AddSubAdminModalComponent  {
  showPassword: boolean = false; // password eye icon boolean

  searchText = '';
  modalRef: BsModalRef;
  config = {
    backdrop: true,
    ignoreBackdropClick: true,
    class: 'modal-right',
    keyboard: false
  };
  admin_urls: any = [];
  subAdminForm: UntypedFormGroup;
  adminUrls: any = [];
  isEdit: boolean = false;
  editAdmin: any;
  is_sub_admin: boolean = false;
  country_list: any = [];
  city_list: any = [];
  selectedCountries: any[] = [];
  selectedCities: any[] = [];
  select_country_error:boolean = false;
  select_city_error:boolean = false;
  
  @ViewChild('template', { static: true }) template: TemplateRef<any>;

  constructor(private _notifierService: NotifiyService,private modalService: BsModalService, private _helper: Helper,private _countryService: CountryService, private _cityService: CityService, private _subAdminService: SubAdminService,private _subAdminUrl:SubAdminUrl) {
    this.admin_urls = this._subAdminUrl.ADMIN_URL
  }

  show(admin): void {
    this.showPassword =false;
    this._initForm()
    this.getCountryList(admin);
  }
  _initForm(){
    this.subAdminForm = new UntypedFormGroup({
      username: new UntypedFormControl(null, Validators.required),
      email: new UntypedFormControl(null, [Validators.email ,Validators.required]),
      update_admin_id: new UntypedFormControl(''),
      admin_type: new UntypedFormControl(null, Validators.required),
      password: new UntypedFormControl(null, [Validators.minLength(6), Validators.required]),
      is_show_email : new UntypedFormControl(false),
      is_show_phone : new UntypedFormControl(false),
      urls: new UntypedFormArray([]),
      is_country_based_access_control_enabled : new UntypedFormControl(false),
      is_city_based_access_control_enabled : new UntypedFormControl(false),
      allowed_countries: new UntypedFormArray([]),
      allowed_cities: new UntypedFormArray([])
    })
  }

  _patchForm(){
    this.subAdminForm.patchValue({
      username: this.editAdmin.username,
      email: this.editAdmin.email,
      update_admin_id: this.editAdmin._id,
      urls: this.editAdmin.urls,
      admin_type: this.editAdmin.admin_type,
      is_show_email : this.editAdmin.is_show_email,
      is_show_phone : this.editAdmin.is_show_phone,
      allowed_countries: this.editAdmin.allowed_countries,
      allowed_cities: this.editAdmin.allowed_cities,
      is_country_based_access_control_enabled: this.editAdmin.is_country_based_access_control_enabled,
      is_city_based_access_control_enabled: this.editAdmin.is_city_based_access_control_enabled,
    })
    this.subAdminForm.controls['password'].setValidators(null);
    this.subAdminForm.controls['password'].setErrors(null);
    if(this.editAdmin.admin_type === 3){
      this.is_sub_admin = true
    } else {
      this.is_sub_admin = false
    }
    this.adminUrls = this.editAdmin.urls;
    this.selectedCountries = this.editAdmin.allowed_countries;
    this.getCityList();
    this.selectedCities = this.editAdmin.allowed_cities;

    this.country_list.forEach((country) => {
      this.selectedCountries.forEach((selected_country) => {
        if(country._id == selected_country){
          country.is_country_checked = true;
        }
      })
    })
  }

  onUrlCheck(value, event){
    let url_to_check = this.admin_urls.filter(url => url.value == value);
    let index = url_to_check[0].index;
    let admin_url_index = this.admin_urls.findIndex((x) => x.index == index);
    if(event.target.checked){
      if(admin_url_index != -1){
        this.adminUrls.push({
          route:this.admin_urls[admin_url_index].route,
          url: this.admin_urls[admin_url_index].value,
          permission: '10000'
        })
      }
    } else {
      let urlIndex = this.adminUrls.findIndex(x => x.url === this.admin_urls[admin_url_index].value)
      this.adminUrls.splice(urlIndex, 1)
    }
  }

  is_checked(url, type){
    let index = this.adminUrls.findIndex((x)=>x.url==url);
    if(index!==-1){
      let permission = this.adminUrls[index].permission.split('');
      if(permission[Number(type)]=='1'){
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  change_permission(url, type){
    let index = this.adminUrls.findIndex((x)=>x.url==url);
    if(index!==-1){
      let permission = this.adminUrls[index].permission.split('');
      if(permission[Number(type)]=='1'){
        permission[Number(type)] = '0';
      } else {
        permission[Number(type)] = '1';
      }
      this.adminUrls[index].permission = permission.join('')
    }
  }

  onAddAdmin(){
    this._helper.checkAndCleanFormValues(this.subAdminForm)
    document.getElementById("scrollTop").scrollIntoView({
      behavior: 'smooth',
      block : 'end',
      inline: "nearest"  
    });
    
    this.subAdminForm.markAllAsTouched()
    if(this.subAdminForm.value.is_country_based_access_control_enabled === true && this.selectedCountries.length == 0){
      this.select_country_error = true;
    }else{
      this.select_country_error = false;
    }
    if(this.subAdminForm.value.is_city_based_access_control_enabled === true && this.selectedCities.length == 0){
      this.select_city_error = true;
    }else{
      this.select_city_error = false;
    }
    if((this.subAdminForm.value.is_country_based_access_control_enabled === true && this.selectedCountries.length == 0) || (this.subAdminForm.value.is_city_based_access_control_enabled === true && this.selectedCities.length == 0)){
      return
    }
    if(this.subAdminForm.valid){
      if(this.subAdminForm.value.is_country_based_access_control_enabled === true){
        this.subAdminForm.value.allowed_countries  = this.selectedCountries;
      }else{
        this.subAdminForm.value.allowed_countries  = [];
      }
      if(this.subAdminForm.value.is_city_based_access_control_enabled === true){
        this.subAdminForm.value.allowed_cities  = this.selectedCities;
      }else{
        this.subAdminForm.value.allowed_cities  = [];
      }
      if(this.subAdminForm.value.admin_type != 1){
        if(this.adminUrls.length>0){
          this.subAdminForm.value.urls  = this.adminUrls
        }else{
          this._notifierService.showNotification('error', this._helper._trans.instant('label-title.please-select-atleast-one-permission'));
          return
        }
        this._subAdminService.addAdmin(this.subAdminForm.value).then((res:any) =>{
          if(res.success){
            this.onClose()
          }
        })
      } else {
        this.subAdminForm.value.urls  = []
        this._subAdminService.addAdmin(this.subAdminForm.value).then((res:any) =>{
          if(res.success){
            this.onClose()
          }
        })
      }
    }
  }

  onUpdateAdmin(){
    this._helper.checkAndCleanFormValues(this.subAdminForm)
    this.subAdminForm.markAllAsTouched()
    if(this.subAdminForm.value.is_country_based_access_control_enabled === true && this.selectedCountries.length == 0){
      this.select_country_error = true;
    }else{
      this.select_country_error = false;
    }
    if(this.subAdminForm.value.is_city_based_access_control_enabled === true && this.selectedCities.length == 0){
      this.select_city_error = true;
    }else{
      this.select_city_error = false;
    }
    if((this.subAdminForm.value.is_country_based_access_control_enabled === true && this.selectedCountries.length == 0) || (this.subAdminForm.value.is_city_based_access_control_enabled === true && this.selectedCities.length == 0)){
      return
    }
    if(this.subAdminForm.valid){
      if(this.subAdminForm.value.is_country_based_access_control_enabled === true){
        this.subAdminForm.value.allowed_countries  = this.selectedCountries;
      }else{
        this.subAdminForm.value.allowed_countries  = [];
      }
      if(this.subAdminForm.value.is_city_based_access_control_enabled === true){
        this.subAdminForm.value.allowed_cities  = this.selectedCities;
      }else{
        this.subAdminForm.value.allowed_cities  = [];
      }
      if(this.subAdminForm.value.admin_type == 3){
        if(this.adminUrls.length>0){
          this.subAdminForm.value.urls  = this.adminUrls
        }else{
          this._notifierService.showNotification('error', this._helper._trans.instant('label-title.please-select-atleast-one-permission'));
          return
        }
      }
      this._subAdminService.updateAdmin(this.subAdminForm.value).then((res:any) =>{
        if(res.success){
          this.onClose()
        }
      })
    }
  }

  onClose(){
    this.modalRef.hide()
    setTimeout(() => {
      this.subAdminForm.reset()
      this.adminUrls.length = 0
      this.isEdit = false
      this.searchText = '';
      this.is_sub_admin = false;
      this.country_list = [];
      this.city_list = [];
      this.selectedCountries = [];
      this.selectedCities = [];
      this.select_country_error = false;
      this.select_city_error = false;
    }, 400);
  }

  onAdminType(event){
    if(event === 3){
      this.is_sub_admin = true
    } else {
      this.is_sub_admin = false
    }
  }

  // get CountryList
  getCountryList(admin) {
    this._countryService.fetch().then(res => {
      if (res.success) {
        this.country_list = res.countries;
        this.country_list.forEach((country) => {
          country.is_country_checked = false;
        })
      } else {
        this.country_list = []
      }
      this._initForm()
      if (admin != '') {
        this.isEdit = true
        this.editAdmin = admin
        this.editAdmin.urls = JSON.parse(JSON.stringify(this.editAdmin.urls))
        this._patchForm()
      }
      this.modalRef = this.modalService.show(this.template, this.config);
    })
  }

  selectCountry(data) {
    if (data.is_country_checked) {
      this.selectedCountries.push(data._id);
      this.getCityList();
    } else {
      this.selectedCountries = this.selectedCountries.filter(item => item !== data._id);
      this.getCityList();
    }
  }

  // get CityList
  getCityList() {
    let json: any = { country_ids: this.selectedCountries };
    this._cityService.getDestinationCityList(json).then(city => {

      if (city.success) {
        this.city_list = city.destination_list;
        this.city_list.forEach(city => {
          city.is_city_checked = false;
        })
        if (this.isEdit) {
          this.city_list.forEach((city) => {
            this.selectedCities.forEach((selected_city) => {
              if (city._id == selected_city) {
                city.is_city_checked = true;
              }
            })
          })
        }

        const citiesNotInList = this.selectedCities.filter(selected_city => !this.city_list.some(city => city._id == selected_city));
        if (citiesNotInList.length > 0) {
          citiesNotInList.forEach(city => {
            if (this.selectedCities.includes(city)) {
              this.selectedCities.splice(city, 1);
            }
          })
        }
      }
    })
  }

  selectCity(data) {
    if (data.is_city_checked) {
      this.selectedCities.push(data._id);
    } else {
      this.selectedCities = this.selectedCities.filter(item => item !== data._id);
    }
  }

  changeCountryAccess() {
    if (this.subAdminForm.value.is_country_based_access_control_enabled === false) {
      this.subAdminForm.patchValue({ is_city_based_access_control_enabled: false })
    }
  }

  changeCityAccess() {
    if (this.subAdminForm.value.is_city_based_access_control_enabled === true) {
      this.subAdminForm.patchValue({ is_country_based_access_control_enabled: true })
    }
  }
}
