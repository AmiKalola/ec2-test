import { Component, EventEmitter, Output, TemplateRef, ViewChild } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { CONSTANT } from '../../../../app/views/app/constant';

@Component({
    selector:'specification-warning-modal',
    templateUrl:'./specification-warning-modal.component.html'
})
export class SpecificationWarningModelComponent {

    modalRef: BsModalRef;
    config = {
        backdrop: true,
        ignoreBackdropClick: false,
        class: 'modal-popup'
    };
    specificationIndex: number
    type: number
    typeConstant = CONSTANT

    @ViewChild('template', { static: true }) template: TemplateRef<any>;

    @Output() onSubmit = new EventEmitter<any>();


    constructor(private modalService: BsModalService) { }

    show(index, type) {
        this.specificationIndex = index
        this.type = type;
        this.modalRef = this.modalService.show(this.template, this.config)
    }

    confirm() {
        this.modalRef.hide()
        this.onSubmit.emit({confirmation: true, index: this.specificationIndex, type: this.type})
    }

    close(){
        this.modalRef.hide()
        this.onSubmit.emit({confirmation: false, index: this.specificationIndex, type: this.type})
    }

}
