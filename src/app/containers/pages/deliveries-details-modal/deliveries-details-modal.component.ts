import { Component, NgZone, TemplateRef,  ViewChild } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { DeliveriesService } from 'src/app/services/deliveries.service';
import { Helper } from 'src/app/shared/helper';
import { environment } from 'src/environments/environment';
import { AddNewOrderListModalComponent } from '../add-new-order-list-modal/add-new-order-list-modal.component';
declare let google;

@Component({
  selector: 'deliveries-details-modal',
  templateUrl: './deliveries-details-modal.component.html',
  styleUrls: ['./deliveries-details-modal.component.scss']
})
export class DeliveriesDetailModal  {
  modalRef: BsModalRef;
  config = {
    backdrop: true,
    ignoreBackdropClick: true,
    class: 'modal-right'
  };
  order;
  is_order_cancle: boolean = false
  destination_address: any = {address: '', location: []};
  @ViewChild('template', { static: true }) template: TemplateRef<any>;
  map: any;
  bounds: any;
  public directionsDisplay = new google.maps.DirectionsRenderer;
  provider_detail: any = ''
  pickup_address: any = {}
  destination_addresses: any[] = []
  IMAGE_BASE_URL = environment.imageUrl
  store_marker: any
  user_marker: any
  provider_marker: any;
  order_id: any;
  order_detail:any;

  constructor(private modalService: BsModalService,
    private _deliveriesService: DeliveriesService,
    public _helper:Helper,
    public ngZone: NgZone) { }

  @ViewChild('OrderDetailModal', { static: true }) OrderDetailModal: AddNewOrderListModalComponent;

  show(orderid, provider_id): void {
    let json = {
      order_detail_id: orderid,
      provider_id
    }
    this._deliveriesService.admin_requests_datail(json).then(res_data => {
      if(res_data.success){
      this.modalRef = this.modalService.show(this.template, this.config);
      this.order_id = res_data.order_detail._id
      this.order_detail = res_data.order_detail;
      if(res_data.provider_detail){
        this.provider_detail = res_data.provider_detail
      }
      this.destination_address = res_data.request_detail.destination_addresses[0]
      this.destination_addresses = res_data.request_detail.destination_addresses;
      this.pickup_address = res_data.request_detail.pickup_addresses[0]
      this.ngZone.run(() => {
        let theme  = localStorage.getItem('vien-themecolor');
        if(theme.includes('dark')){
          this.map = new google.maps.Map(document.getElementById('map'), {
            zoom: 10,
            center: { lat: 22.290268653053918, lng: 70.7748018100878 },
            draggable: false,
            zoomControl: true,
            scrollwheel: false,
            disableDoubleClickZoom: false,
            fullscreenControl: true,
            styles: [
              { elementType: "geometry", stylers: [{ color: "#242f3e" }] },
              { elementType: "labels.text.stroke", stylers: [{ color: "#242f3e" }] },
              { elementType: "labels.text.fill", stylers: [{ color: "#746855" }] },
              {
                featureType: "administrative.locality",
                elementType: "labels.text.fill",
                stylers: [{ color: "#d59563" }],
              },
              {
                featureType: "poi",
                elementType: "labels.text.fill",
                stylers: [{ color: "#d59563" }],
              },
              {
                featureType: "poi.park",
                elementType: "geometry",
                stylers: [{ color: "#263c3f" }],
              },
              {
                featureType: "poi.park",
                elementType: "labels.text.fill",
                stylers: [{ color: "#6b9a76" }],
              },
              {
                featureType: "road",
                elementType: "geometry",
                stylers: [{ color: "#38414e" }],
              },
              {
                featureType: "road",
                elementType: "geometry.stroke",
                stylers: [{ color: "#212a37" }],
              },
              {
                featureType: "road",
                elementType: "labels.text.fill",
                stylers: [{ color: "#9ca5b3" }],
              },
              {
                featureType: "road.highway",
                elementType: "geometry",
                stylers: [{ color: "#746855" }],
              },
              {
                featureType: "road.highway",
                elementType: "geometry.stroke",
                stylers: [{ color: "#1f2835" }],
              },
              {
                featureType: "road.highway",
                elementType: "labels.text.fill",
                stylers: [{ color: "#f3d19c" }],
              },
              {
                featureType: "transit",
                elementType: "geometry",
                stylers: [{ color: "#2f3948" }],
              },
              {
                featureType: "transit.station",
                elementType: "labels.text.fill",
                stylers: [{ color: "#d59563" }],
              },
              {
                featureType: "water",
                elementType: "geometry",
                stylers: [{ color: "#17263c" }],
              },
              {
                featureType: "water",
                elementType: "labels.text.fill",
                stylers: [{ color: "#515c6d" }],
              },
              {
                featureType: "water",
                elementType: "labels.text.stroke",
                stylers: [{ color: "#17263c" }],
              },
            ],
          });
        }else{
          this.map = new google.maps.Map(document.getElementById('map'), {
            zoom: 10,
            center: { lat: 22.290268653053918, lng: 70.7748018100878 },
            draggable: false,
            zoomControl: true,
            scrollwheel: false,
            disableDoubleClickZoom: false,
            fullscreenControl: true
          });
        }
        this.bounds = new google.maps.LatLngBounds();
        this.directionsDisplay.setMap(this.map);
        let map_pin = document.getElementById('map_pin');
        this.map.controls[google.maps.ControlPosition.BOTTOM_CENTER].push(map_pin);
        this.bounds.extend(new google.maps.LatLng(this.pickup_address.location[0], this.pickup_address.location[1]));
        this.store_marker = new google.maps.Marker({
          position: { lat: this.pickup_address.location[0], lng: this.pickup_address.location[1] },
          map: this.map,
          icon: this.IMAGE_BASE_URL + 'map_pin_images/Deliveryman/deliveryman_online_with_orders.png'
        });
        let store_contentString =
          '<b>Name</b>: ' + this.pickup_address.user_details.name +
          '<br><b>Email</b>: ' + this.pickup_address.user_details.email +
          '<br><b>Phone</b>: ' + this.pickup_address.user_details.country_phone_code + this.pickup_address.user_details.phone;
        let store_message = new google.maps.InfoWindow({
          content: store_contentString,
          maxWidth: 320        
        });
        console.log(store_message);
      
        this.destination_addresses.forEach((element, index) => {
          this.bounds.extend(new google.maps.LatLng(element.location[0], element.location[1]));
  
          this.user_marker = new google.maps.Marker({
            position: { lat: element.location[0], lng: element.location[1] },
            map: this.map,
            label: (index+1).toString()
         
          });
        });
      
        if(this.provider_detail != ''){
          this.provider_marker = new google.maps.Marker({
            position: { lat: this.provider_detail.location[0], lng: this.provider_detail.location[1] },
            map: this.map,
            icon: this.IMAGE_BASE_URL + 'map_pin_images/Deliveryman/deliveryman_online.png'
          });
        }
        this.map.fitBounds(this.bounds);
      });
    }
    })
  }

  onClose(){
    this.provider_detail = {}
    this.modalRef.hide()
    this.store_marker.setMap(null);
    if(this.provider_marker){
      this.provider_marker.setMap(null);
    }
    this.user_marker.setMap(null);

  }

  onDetails(){
    this.modalRef.hide()
    setTimeout(() => {
      this.OrderDetailModal.show(this.order_id)
    }, 100);
  }
  
}
