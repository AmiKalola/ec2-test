import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AddNewOrderListModalComponent } from './deliveries-details-modal.component';

describe('AddNewOrderListModalComponent', () => {
  let component: AddNewOrderListModalComponent;
  let fixture: ComponentFixture<AddNewOrderListModalComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AddNewOrderListModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddNewOrderListModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
