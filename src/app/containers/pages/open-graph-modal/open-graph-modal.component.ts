import { Component, TemplateRef, ViewChild } from '@angular/core';
import { UntypedFormArray, UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { AdminSettingService } from 'src/app/services/admin-setting.service';
import { LangService } from 'src/app/shared/lang.service';
@Component({
  selector: 'open-graph-modal',
  templateUrl: './open-graph-modal.component.html',
  styleUrls: ["./open-graph-modal.component.scss"]
})
export class OpenGraphModalComponent {
  modalRef: BsModalRef;
  openGraphForm: UntypedFormGroup
  tabType: number
  formData: FormData
  config = {
    backdrop: true,
    ignoreBackdropClick: true,
    class: 'modal-right'
  };
  ogImageUrlFile: any;

  @ViewChild('template', { static: true }) template: TemplateRef<any>;

  constructor(private modalService: BsModalService,
    private _lang: LangService,
    private _adminSettingsService: AdminSettingService) { }


  show(seoData, tabType): void {
    this.ogImageUrlFile = null
    this.tabType = tabType
    this.openGraphForm = new UntypedFormGroup({
      ogTitle: new UntypedFormArray([]),
      ogDescription: new UntypedFormArray([]),
      ogType: new UntypedFormControl(tabType, Validators.required),
      ogUrl: new UntypedFormControl(null, Validators.required),
      ogImage: new UntypedFormControl(null, Validators.required),
      ogUrlIsManual: new UntypedFormControl(false, Validators.required),
      ogImageUrlIsManual: new UntypedFormControl(false, Validators.required)
    })
    this._lang.supportedLanguages.forEach((_language,index)=>{
      this.ogTitle.push(new UntypedFormControl(seoData.ogTitle[index] || "", _language.code === 'en' ? Validators.required : null))
      this.ogDescription.push(new UntypedFormControl(seoData.ogDescription[index] || "", _language.code === 'en' ? Validators.required : null))
    })

    this.openGraphForm.patchValue({
      ogUrlIsManual: seoData.ogUrlIsManual || false,
      ogImageUrlIsManual: seoData.ogImageUrlIsManual || false
    })

    if (seoData.ogUrlIsManual) {
      this.openGraphForm.patchValue({
        ogUrl: seoData.ogUrl
      })
    }

    if (seoData.ogImageUrlIsManual) {
      this.openGraphForm.patchValue({
        ogImage: seoData.ogImage
      })
    }
    this.modalRef = this.modalService.show(this.template, this.config);
  }

  get ogTitle(){
    return this.openGraphForm.get('ogTitle') as UntypedFormArray;
  }

  get ogDescription(){
    return this.openGraphForm.get('ogDescription') as UntypedFormArray;
  }

  urlTypeChange(type){
    if(type === 1){
      this.openGraphForm.patchValue({ogUrlIsManual: false})
    } else {
      this.openGraphForm.patchValue({ogUrlIsManual: true})
    }
  }

  imageUrlTypeChange(type){
    if(type === 1){
      this.openGraphForm.patchValue({ogImageUrlIsManual: false})
    } else {
      this.openGraphForm.patchValue({ogImageUrlIsManual: true})
    }
  }

  updateOGTags(){
    this.formData = new FormData()
    this.formData.append('type', String(this.tabType))
    this.openGraphForm.value.ogTitle.forEach(element => {
      this.formData.append('ogTitle', element)
    });
    this.openGraphForm.value.ogDescription.forEach(element => {
      this.formData.append('ogDescription', element)
    });
    this.formData.append('ogType', this.openGraphForm.value.ogType)
    this.formData.append('ogUrl', this.openGraphForm.value.ogUrl)
    this.formData.append('ogImage', this.openGraphForm.value.ogImage)
    this.formData.append('ogUrlIsManual', this.openGraphForm.value.ogUrlIsManual)
    this.formData.append('ogImageUrlIsManual', this.openGraphForm.value.ogImageUrlIsManual)
    if (this.ogImageUrlFile) {
      this.formData.append('ogImageUrlFile', this.ogImageUrlFile)
    }
    this._adminSettingsService.updateSeoTags(this.formData).then(result => {
      this.onClose();
      this._adminSettingsService._seoChanges.next(null);
    })
  }

  onSelectFile(event){
    const files = event.target.files;
    if (files.length === 0)
      return;

    const fileType = files[0].type;

    if (fileType.match(/image\/*/) == null) {
      alert("Only images are supported.")
      return;
    }

    this.ogImageUrlFile = files[0]
  }

  onRemoveSelectedFile(){
    this.ogImageUrlFile = null
    let fileInput:any = document.getElementById("inputGroupFile02")
    fileInput.value = "";
  }

  onClose() {
    this.modalRef.hide()
  }

}
