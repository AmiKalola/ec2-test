import { Component, EventEmitter, Output, TemplateRef,  ViewChild } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { Store } from 'src/app/models/store.model';
import { StoreService, store_page_type } from '../../../services/store.service'
import { Helper } from 'src/app/shared/helper';
import { LangService, Language } from 'src/app/shared/lang.service';
import { DELIVERY_TYPE_CONSTANT } from 'src/app/shared/constant';
import { ImageCropModelComponent } from '../image-crop-model/image-crop-model.component';
import { CommonService } from 'src/app/services/common.service';
import { AddWalletModelComponent } from '../add-wallet-modal/add-wallet-modal.component';
import { Lightbox } from 'ngx-lightbox';
import { NotifiyService } from 'src/app/services/notifier.service';

@Component({
  selector: 'app-add-new-store-info-modal',
  templateUrl: './add-new-store-info-modal.component.html',
  styleUrls: ['./add-new-store-info-modal.component.scss']
})
export class AddNewStoreInfoModalComponent {
  showPassword: boolean = false; // password eye icon boolean

  base64Image:any;
  fiveRate=5;
  is_seo: boolean = false; 
  modalRef: BsModalRef;
  confirmModelRef: BsModalRef;
  DELIVERY_TYPE_CONSTANT = DELIVERY_TYPE_CONSTANT;
  config = {
    backdrop: true,
    ignoreBackdropClick: true,
    class: 'modal-right'
  };
  confirmationModalConfig = {
    backdrop: true,
    ignoreBackdropClick: true,
  };
  isChatTab = false;
  isCollapsedAnimated = false;
  isCollapsedAnimated1 = false;
  user_detail: Store = {
    store_id: '',
    name: [],
    phone: '',
    country_phone_code: '',
    email: '',
    image_url: '',
    password: '',
    is_use_wallet: false,
    wallet: 0,
    wallet_currency_code: '',
    created_at: '',
    app_version: 'any',
    device_type: '',
    referral_code: '',
    user_rate: 0,
    provider_rate: 0,
    comment: '',
    is_email_verified: false,
    is_phone_verified: false,
    website_url: '',
    location: [],
    address: '',
    is_store_can_complete_order: false,
    is_store_can_add_provider: false,
    slogan: '',
    admin_profit_mode_on_store: null,
    admin_profit_value_on_store: null,
    unique_id:0
  };
  store_page_type: any = store_page_type;
  documents: any[] = [];
  referral_history: any[] = [];
  review_list: any[] = [];
  languages: Language[];
  show_review_type: number = 1;
  selectedDocumentIndex: number;

  todayDate = new Date()

  selected_document: any;
  image_settings: any;
  image_type: any;
  document_image: any;
  vehicle_document_form_data: any;
  store_form_data: FormData;
  store_document_form_data: FormData;
  upload_profile_image: any = null;
  document_display_image: any = null;
  minimum_length: number= 8;
  maximum_length: number= 12;
  timezone_for_display_date:string = '';
  approveModalConfig = {
    backdrop: true,
    ignoreBackdropClick: true,
  };
  approveModelRef: BsModalRef;
  selectedUser : any ;
  approvalStatus : any ;
  unique_id:number;
  merchantName:boolean=false;

  @Output() updateData = new EventEmitter <any>();
  @ViewChild('approveTemplate', { static: true }) approveTemplate: TemplateRef<any>;
  @ViewChild('template', { static: true }) template: TemplateRef<any>;
  @ViewChild('cropModel', { static: true }) cropModel: ImageCropModelComponent;
  @ViewChild('confirmationTemplate', { static: true }) confirmationTemplate: TemplateRef<any>;
  @ViewChild('walletAmount', {static: true}) walletAmount: AddWalletModelComponent;

  constructor(public storeService: StoreService,
    private modalService: BsModalService,
    public _helper: Helper,
    private langService: LangService,
    private _commonService: CommonService,
    private lightBox: Lightbox,
    private _notifierService: NotifiyService
    ) {
    this.languages = this.langService.supportedLanguages;
    this._helper.display_date_timezone.subscribe(data => {
      this.timezone_for_display_date = data;
    })
  }


  show(store): void {
    this.showPassword =false;
    this.store_form_data = new FormData;
    this.unique_id = store.unique_id;
    this.get_store_detail(store._id)
    this.modalRef = this.modalService.show(this.template, this.config);
    this.fetch_image_settings()
    this.store_document_form_data = new FormData
    this.subadmineditpermission();
  }
  
  //sub admin edit permission is false then profile form disable
  subadmineditpermission(){
    if(!this._helper.has_permission(this._helper.PERMISSION.EDIT)){
    setTimeout(() => {
        let elements : any = document.getElementsByTagName('form')[0]?.elements;
        for(const element of elements){
          if(element.tagName != 'BUTTON'){
            element.setAttribute('disabled' , 'true')
          }
        }
      }, 300);
    }
  }

  fetch_image_settings() {
    this._commonService.fetch_image_settings().then(res_data => {
      if (res_data.success) {
        this.image_settings = res_data.image_setting
      }
    })
  }

  get_store_detail(store_id){
    this.storeService.getStoreDetail({store_id: store_id}).then(res_data => {
      this.user_detail = res_data.store;
      this.user_detail.store_id = res_data.store._id;
      this.user_detail.password = '';
      this.upload_profile_image =  this._helper.image_url +  this.user_detail.image_url
      this.minimum_length = res_data.minimum_phone_number_length
      this.maximum_length = res_data.maximum_phone_number_length
    })
  }

  approveDecline(user, type){
    this.selectedUser = user ;
    this.approvalStatus = type ;
    this.modalRef.hide();
    this.approveModelRef = this.modalService.show(this.approveTemplate, this.approveModalConfig);
  }

  approve(){
    let page_type;
    switch(this.approvalStatus){
      case store_page_type.approved:
        page_type = 1;
        break;
      case store_page_type.blocked:
        page_type = 2;
        break;
      case store_page_type.business_off:
        page_type = 3;
        break;
    }
    let data = {
      "store_page_type": page_type,
      store_id: this.selectedUser._id
    }
    this.storeService.approveDeclineStore(data).then(res_data => {
      this.modalRef.hide();
      this.approveModelRef.hide();
    })
  }

  cancelAprroveModal(){
    this.approveModelRef.hide();
    this.modalRef = this.modalService.show(this.template, this.config);
    this.subadmineditpermission();
  }

  store_can_add_provider(){
    if(!this.user_detail.is_store_can_add_provider){
      this.user_detail.is_store_can_complete_order = false;
    }
  }

  updateUser(store_id){
    this.user_detail.name = this.user_detail.name.map(name => name.trim());
    if(!this.user_detail.name[0]){
      return
    }
    
    if(this.user_detail.name==''){
      this.merchantName=true;
      return
    }
    
    this.store_form_data.append('store_id', this.user_detail.store_id)
    this.store_form_data.append('name', JSON.stringify(this.user_detail.name))
    this.store_form_data.append('phone', this.user_detail.phone)
    this.store_form_data.append('country_phone_code', this.user_detail.country_phone_code)
    this.store_form_data.append('email', this.user_detail.email)
    this.store_form_data.append('password', this.user_detail.password)
    this.store_form_data.append('is_use_wallet', String(this.user_detail.is_use_wallet))
    this.store_form_data.append('wallet', String(this.user_detail.wallet))
    this.store_form_data.append('wallet_currency_code', this.user_detail.wallet_currency_code)
    this.store_form_data.append('created_at', this.user_detail.created_at)
    this.store_form_data.append('app_version', this.user_detail.app_version)
    this.store_form_data.append('device_type', this.user_detail.device_type)
    this.store_form_data.append('referral_code', this.user_detail.referral_code)
    this.store_form_data.append('user_rate', String(this.user_detail.user_rate))
    this.store_form_data.append('provider_rate', String(this.user_detail.provider_rate))
    this.store_form_data.append('comment', this.user_detail.comment)
    this.store_form_data.append('is_email_verified', String(this.user_detail.is_email_verified))
    this.store_form_data.append('is_phone_verified', String(this.user_detail.is_phone_verified))
    this.store_form_data.append('website_url', this.user_detail.website_url)
    this.store_form_data.append('location', JSON.stringify(this.user_detail.location))
    this.store_form_data.append('address', this.user_detail.address)
    this.store_form_data.append('is_store_can_complete_order', String(this.user_detail.is_store_can_complete_order))
    this.store_form_data.append('is_store_can_add_provider', String(this.user_detail.is_store_can_add_provider))
    this.store_form_data.append('slogan', this.user_detail.slogan)
    this.store_form_data.append('admin_profit_mode_on_store', String(this.user_detail.admin_profit_mode_on_store))
    this.store_form_data.append('admin_profit_value_on_store', String(this.user_detail.admin_profit_value_on_store))
    this.storeService.updateStoreDetail(this.store_form_data).then(res_data => {
      this.modalRef.hide();
    })
  }


  get_document_list(){
    let data = {
      id: this.user_detail.store_id,
      type: this._helper.USER_TYPE.STORE
    }
    this.storeService.getDocumentList(data).then(res_data => {
      this.documents = res_data.documents;
      this.documents.forEach(document => {
        if(document.image_url != ''){
          document.image_url = this._helper.image_url + document.image_url
        }
        if(document.expired_date){
          document.expired_date = new Date(document.expired_date);
        }
      })
    })
  }

  update_document(document){
    if(((document.is_expired_date && !document.expired_date) || (document.is_unique_code && !document.unique_code) )){
      document.dataRequired = true;
      return;
    }
    if(!document.image_url && !this.store_document_form_data.get('image_url') && document.is_mandatory){
      this._notifierService.showNotification('error', this._helper._trans.instant('validation-title.please_upload_image'));
      return;
    }
    
    this.store_document_form_data.append('id', this.user_detail.store_id)
    this.store_document_form_data.append('type', String(this._helper.USER_TYPE.STORE))
    this.store_document_form_data.append('unique_code', document.unique_code)
    this.store_document_form_data.append('expired_date', document.expired_date)
    this.store_document_form_data.append('document_id', document._id)
    this.storeService.updateDocumentDetail(this.store_document_form_data).then(res_data => {
      document.is_edit = false;
      document.dataRequired = false;
      this.store_document_form_data = new FormData()
      this.get_document_list()
    })
  }

  get_referral_usage_list(){
    let data = {
      id: this.user_detail.store_id,
      type: this._helper.USER_TYPE.STORE
    }
    this.storeService.getReferralUsageList(data).then(res_data => {
      this.referral_history = res_data.referral_history;
    })
  }

  get_review_usage_list(){
    let data = {
      store_id: this.user_detail.store_id
    }
    this.storeService.getReviewList(data).then(res_data => {
      this.review_list = res_data.review_list;
    })
  }

  onMenuClick(){
    this._helper.changeSelectStore(this.user_detail, 1)
    this._helper.selected_store_id = this.user_detail.store_id;
    this.modalRef.hide();
    if(this._helper.has_permission(this._helper.PERMISSION.VIEW, 'category')){
      this._helper.router.navigate(['/app/menu/category'])
    }else if(this._helper.has_permission(this._helper.PERMISSION.VIEW, 'item-list')){
      this._helper.router.navigate(['/app/menu/item-list'])
    }else if(this._helper.has_permission(this._helper.PERMISSION.VIEW, 'modifier-group')){
      this._helper.router.navigate(['/app/menu/modifier-group'])
    }
  }

  onSelectImageFile(event, document, type) {  //1: store profile, 2: document,
    this.selected_document = document
    this.image_type = type
    let files = event.target.files;
    if (files.length === 0)
      return;
    const mimeType = files[0].type;
    let fileType : any ;
    if(type==1 || type !==1) {
      fileType = this._helper.uploadFile.filter((element) => {
        return mimeType == element;
      })
    }else {
      fileType = this._helper.uploadDocFile.filter((element) => {
        return mimeType == element;
      })
    }

    if (fileType != mimeType) {
      if(type==1 || type !==1){
        this._notifierService.showNotification('error',this._helper._trans.instant('validation-title.invalid-image-format'));
        return;
      }
    }else{
      this.imageCropped(files[0])
    }

  }

  imageCropped(event) {
    const reader = new FileReader();
    reader.readAsDataURL(event);
    this.document_image = event;
    if(this.image_type === 1){
      this.store_form_data.append('image_url', this.document_image)
    } else {
      this.store_document_form_data.append('image_url', this.document_image)
    }
    reader.onload = (_event) => {
      if(this.image_type === 1){
        this.upload_profile_image = reader.result
      } else {
        this.documents[this.selected_document].image_url = reader.result
      }
    }
  }

  onAddWallet(){
    this.modalRef.hide();
    this.walletAmount.show()
  }

  addWalletAmount(amount){
    if(amount != 'close_modal'){
      let json = {
        type: this._helper.USER_TYPE.STORE,
        store_id: this.user_detail.store_id,
        wallet: amount
      }
      this._commonService.add_wallet_amount(json)
    }else{
      this.modalRef = this.modalService.show(this.template, this.config)
      this.subadmineditpermission();
    }
  }

  onLightBox(document){
    if(document.image_url != ''){
      this.onClose()
      let src = document.image_url
      this.lightBox.open([{ src, thumb: '', downloadUrl: '' }], 0, { centerVertically: true, positionFromTop: 0, disableScrolling: true, wrapAround: true });
    }
  }

// changes starts from here........

downloadImage(value,imageName) {
  let imageUrl = value;
  let split_image_url = imageUrl.split(this._helper.image_url)
  if(split_image_url[1] != ''){
    // window.open(image_url)
  }
  this._helper.downloadUrl(imageUrl)
  .subscribe({
   next : imgData =>{
      this._helper.downloadImage(imgData, imageName)
    },
    error : err => console.log(err)}
  );
}

// changes end here..........  

  deleteAccount(){
    this.modalRef.hide()
    this.confirmModelRef = this.modalService.show(this.confirmationTemplate, this.confirmationModalConfig)
  }

  cancel(){
    this.confirmModelRef.hide()
    this.modalRef = this.modalService.show(this.template, this.config)
    this.subadmineditpermission();
  }

  async confirm(){
    await this.storeService.deleteAccount({store_id: this.user_detail.store_id})
    this.confirmModelRef.hide()
    this.updateData.emit()
  }


  onClose() {
    this.modalRef.hide()
  }
}
