import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AddNewStoreInfoModalComponent } from './add-new-store-info-modal.component';

describe('AddNewStoreInfoModalComponent', () => {
  let component: AddNewStoreInfoModalComponent;
  let fixture: ComponentFixture<AddNewStoreInfoModalComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AddNewStoreInfoModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddNewStoreInfoModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
