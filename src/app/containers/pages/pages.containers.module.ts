import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LightboxModule } from 'ngx-lightbox';
import { SharedModule } from 'src/app/shared/shared.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { LayoutContainersModule } from '../layout/layout.containers.module';
import { AddNewCategoryModalComponent } from './add-new-category-modal/add-new-category-modal.component';
import { AddNewSubCategoryModalComponent } from './add-new-sub-category-modal/add-new-sub-category-modal.component';
import { AddNewItemModalComponent } from './add-new-item-modal/add-new-item-modal.component';
import { AddNewModifierModalComponent } from './add-new-modifier-modal/add-new-modifier-modal.component';
import { TodayOrderHeaderComponent } from './today-order-header/today-order-header.component';
import { StoreTimeSettingsModalComponent } from './store-time-settings-modal/store-time-settings-modal.component'
import { MailConnectModalComponent } from './mail-connect-modal/mail-connect-modal.component'
import { SmsConnectModalComponent } from './sms-connect-modal/sms-connect-modal.component'
import { GoogleApiKeyModalComponent } from './google-api-key-modal/google-api-key-modal.component'
import { AddSubAdminModalComponent } from './add-sub-admin-modal/add-sub-admin-modal.component'
import { UserReviewModalComponent } from './user-review-modal/user-review-modal.component'
import { UserDetailModalComponent } from './user-detail-modal/user-detail-modal.component';
import { ListCategoryHeaderComponent } from './list-category-header/list-category-header.component';
import { ListItemHeaderComponent } from './list-item-header/list-item-header.component';
import { ListModifierHeaderComponent } from './list-modifier-header/list-modifier-header.component';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { RatingModule } from 'ngx-bootstrap/rating';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ArchwizardModule } from 'angular-archwizard';
import { DropzoneModule } from 'ngx-dropzone-wrapper';
import { TimepickerModule } from 'ngx-bootstrap/timepicker';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { AddNewDInfoModalComponent } from './add-new-d-info-modal/add-new-d-info-modal.component';
import { ModalModule } from 'ngx-bootstrap/modal';
import { AddNewVehicleComponent } from './add-new-vehicle/add-new-vehicle.component';
import { AddDManInfoModalComponent } from './add-d-man-info-modal/add-d-man-info-modal.component';
import { AddNewStoreInfoModalComponent } from './add-new-store-info-modal/add-new-store-info-modal.component';
import { AddOfferAdvanceSettingModalComponent } from './add-offer-advance-setting-modal/add-offer-advance-setting-modal.component';
import { AddNewAdvertiseModalComponent } from './add-new-advertise-modal/add-new-advertise-modal.component';
import { StoreEarningDetailComponent } from './store-earning-detail/store-earning-detail.component';
import { DeliverymanEarningDetailComponent } from './deliveryman-earning-detail/deliveryman-earning-detail.component';
import { PipeModule } from 'src/app/pipes/pipe.module';
import { AddNewWalletRequestComponent } from './add-new-wallet-request/add-new-wallet-request.component';
import { AddNewOrderListModalComponent } from './add-new-order-list-modal/add-new-order-list-modal.component';
import { EditAdminDetailsModalComponent } from './edit-admin-details-modal/edit-admin-details-modal.component';
import { TranslateModule } from '@ngx-translate/core';
import { ImageCropModelComponent } from './image-crop-model/image-crop-model.component'
import { ImageCropperModule } from 'ngx-image-cropper';
import { AddAppPushKeyDetalisComponent } from './add-app-push-key-detalis/add-app-push-key-detalis.component';
import { AddAppCertificateSettingComponent } from './add-app-certificate-setting/add-app-certificate-setting.component';
import { AddNewMassNotificationModalComponent } from './add-new-mass-notification-modal/add-new-mass-notification-modal.component';
import { AddDeliveryTagModalComponent } from './add-delivery-tag-modal/add-delivery-tag-modal.component'
import { SetStoreModalComponent } from './set-store-id-model/set-store-id-model.component';
import { AddWalletModelComponent } from './add-wallet-modal/add-wallet-modal.component'
import { AutoAssignModelComponent } from './auto-assign-model/auto-assign-model.component';
import { AddNewLanguageComponent } from './add-language-modal/add-language-modal.component';
import { DeliveriesDetailModal } from './deliveries-details-modal/deliveries-details-modal.component';
import { AddDetailModalComponent } from './add-detail-modal/add-detail-modal.component'
import { AddNewTaxModalComponent } from './add-new-tax-modal/add-new-tax-modal.component'
import { AddNewPromoInfoModalComponent } from './add-new-promo-info-modal/add-new-promo-info-modal.component';
import { ActivityLogsModalComponent } from './activity-logs-modal/activity-logs-modal.component'
import {TableOrderDetailModalComponent} from './table-booking-detail-modal/table-booking-detail-modal.component'
import { SeoTagsHeadModalComponent } from './seo-tags-and-head-modal/seo-tags-head-modal.component'
import { OpenGraphModalComponent } from './open-graph-modal/open-graph-modal.component'
import { TwitterGraphModalComponent } from './twitter-graph-modal/twitter-graph-modal.component'
import { AddNewCancellationReasonComponent } from './add-new-cancellation-reason-modal/add-new-cancellation-reason-modal.component';
import { ScriptTagsModalComponent } from './script-tags-modal/script-tags-modal.component';
import { SpecificationWarningModelComponent } from './specification-warning-modal/specification-warning-modal.component'
import { DeliveryPriceNotSetModal } from './add-charges-modal/delivery-price-not-set-modal.component';
import { GoogleCaptchaSettingModalComponent } from './google-captcha-setting-modal/google-captcha-setting-modal.component';
import { ReferralCodeModalComponent } from './referral-code-modal/referral-code-modal.component';
import { DirectivesModule } from 'src/app/directives/directives.module';
import { EditDeliveryTypeComponent } from './edit-delivery-type/edit-delivery-type.component';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { AddBannerModalComponent } from './add-banner-modal/add-banner-modal.component';

@NgModule({
  declarations: [
    TableOrderDetailModalComponent,
    ActivityLogsModalComponent,
    AddNewPromoInfoModalComponent,
    TodayOrderHeaderComponent,
    AddNewCategoryModalComponent,
    AddNewSubCategoryModalComponent,
    AddNewItemModalComponent,
    AddNewModifierModalComponent,
    UserDetailModalComponent,
    ListCategoryHeaderComponent,
    ListItemHeaderComponent,
    ListModifierHeaderComponent,
    StoreTimeSettingsModalComponent,
    MailConnectModalComponent,
    SmsConnectModalComponent,
    GoogleApiKeyModalComponent,
    AddSubAdminModalComponent,
    UserReviewModalComponent,
    SetStoreModalComponent,
    AddNewDInfoModalComponent,
    AddNewVehicleComponent,
    EditAdminDetailsModalComponent,
    AddDManInfoModalComponent,
    AddNewStoreInfoModalComponent,
    AddOfferAdvanceSettingModalComponent,
    AddNewAdvertiseModalComponent,
    AddNewCancellationReasonComponent,
    StoreEarningDetailComponent,
    DeliverymanEarningDetailComponent,
    AddNewWalletRequestComponent,
    AddNewOrderListModalComponent,
    ImageCropModelComponent,
    AddAppPushKeyDetalisComponent,
    AddAppCertificateSettingComponent,
    AddNewMassNotificationModalComponent,
    AddDeliveryTagModalComponent,
    AddWalletModelComponent,
    AutoAssignModelComponent,
    AddNewLanguageComponent,
    DeliveriesDetailModal,
    AddDetailModalComponent,
    AddNewTaxModalComponent,
    SeoTagsHeadModalComponent,
    OpenGraphModalComponent,
    TwitterGraphModalComponent,
    ScriptTagsModalComponent,
    SpecificationWarningModelComponent,
    DeliveryPriceNotSetModal,
    GoogleCaptchaSettingModalComponent,
    ReferralCodeModalComponent,
    EditDeliveryTypeComponent,
    AddBannerModalComponent
  ],
  imports: [
    TranslateModule,
    CommonModule,
    PipeModule,
    SharedModule,
    RouterModule,
    CollapseModule,
    FormsModule,
    ReactiveFormsModule,
    LayoutContainersModule,
    NgSelectModule,
    LightboxModule,
    RatingModule.forRoot(),
    TabsModule.forRoot(),
    BsDatepickerModule.forRoot(),
    AccordionModule.forRoot(),
    BsDropdownModule.forRoot(),
    TimepickerModule.forRoot(),
    ModalModule.forRoot(),
    TooltipModule,
    ArchwizardModule,
    DropzoneModule,
    ImageCropperModule,
    DirectivesModule
  ],
  exports: [
    TableOrderDetailModalComponent,
    ActivityLogsModalComponent,
    AddNewPromoInfoModalComponent,
    TodayOrderHeaderComponent,
    StoreEarningDetailComponent,
    DeliverymanEarningDetailComponent,
    AddNewCategoryModalComponent,
    AddNewSubCategoryModalComponent,
    AddNewItemModalComponent,
    AddNewModifierModalComponent,
    SetStoreModalComponent,
    UserDetailModalComponent,
    ListCategoryHeaderComponent,
    ListItemHeaderComponent,
    ListModifierHeaderComponent,
    StoreTimeSettingsModalComponent,
    MailConnectModalComponent,
    SmsConnectModalComponent,
    GoogleApiKeyModalComponent,
    AddSubAdminModalComponent,
    UserReviewModalComponent,
    EditAdminDetailsModalComponent,
    AddNewDInfoModalComponent,
    AddNewVehicleComponent,
    AddDManInfoModalComponent,
    AddNewStoreInfoModalComponent,
    AddOfferAdvanceSettingModalComponent,
    AddNewAdvertiseModalComponent,
    AddNewCancellationReasonComponent,
    AddNewWalletRequestComponent,
    AddNewOrderListModalComponent,
    ImageCropModelComponent,
    AddAppPushKeyDetalisComponent,
    AddAppCertificateSettingComponent,
    AddNewMassNotificationModalComponent,
    AddDeliveryTagModalComponent,
    AddWalletModelComponent,
    AutoAssignModelComponent,
    AddNewLanguageComponent,
    DeliveriesDetailModal,
    AddDetailModalComponent,
    AddNewTaxModalComponent,
    SeoTagsHeadModalComponent,
    OpenGraphModalComponent,
    TwitterGraphModalComponent,
    ScriptTagsModalComponent,
    SpecificationWarningModelComponent,
    DeliveryPriceNotSetModal,
    GoogleCaptchaSettingModalComponent,
    ReferralCodeModalComponent,
    EditDeliveryTypeComponent,
    AddBannerModalComponent
  ]
})
export class PagesContainersModule { }
