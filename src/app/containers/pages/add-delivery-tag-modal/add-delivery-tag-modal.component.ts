import { Component, TemplateRef, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { UntypedFormArray, UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { AdminSettingService } from 'src/app/services/admin-setting.service';
import { Helper } from 'src/app/shared/helper';
import { LangService } from 'src/app/shared/lang.service';

@Component({
    selector: 'app-add-delivery-tag-modal',
    templateUrl: './add-delivery-tag-modal.component.html',
})
export class AddDeliveryTagModalComponent {
    isDriver = true;
    tagForm: UntypedFormGroup;
    languages: any = [];
    @Input() value: any = '';
    @Output() addTag = new EventEmitter<any>()

    test: number = 1

    modalRef: BsModalRef;
    config = {
        backdrop: true,
        ignoreBackdropClick: false,
        class: 'modal-right'
    };
    tag: any;
    @ViewChild('template', { static: true }) template: TemplateRef<any>;

    constructor(private modalService: BsModalService, private _adminSettingService: AdminSettingService, private _lang: LangService,private _helper:Helper) { }

    show(data): void {
        this._intiForm()
        this.get_admin_setting(data)
        this.modalRef = this.modalService.show(this.template);
    }

    _intiForm(){
        this.tagForm = new UntypedFormGroup({
            tag: new UntypedFormArray([])
        })
    }

    get tags(){
        return this.tagForm.get('tag') as UntypedFormArray
    }

    get_admin_setting(data){
      let notification = true
        this._adminSettingService.fetchAdminSetting(notification).then(res_data => {
          if(res_data.success){
            this.languages = res_data.setting.lang
            this.languages.forEach((language, index) => {
              this.tags.push(new UntypedFormControl((data && data[index]) ? data[index] : '',language.code === 'en' ? Validators.required : null))
            });
          }
        })
      }

    onSave(){
        this.addTag.emit(this.tagForm.value)
        this.onClose()
    }

    onClose(){
      this.modalRef.hide()
    }
}
