import { Component, TemplateRef, ViewChild } from '@angular/core';
import {  UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { AdminSettingService } from 'src/app/services/admin-setting.service';

@Component({
  selector: 'scrip-tags-modal',
  templateUrl: './script-tags-modal.component.html',
  styleUrls: ["./script-tags-modal.component.scss"]
})
export class ScriptTagsModalComponent {
  modalRef: BsModalRef;
  scriptForm: UntypedFormGroup
  config = {
    backdrop: true,
    ignoreBackdropClick: true,
    class: 'modal-right'
  }

  @ViewChild('template', { static: true }) template: TemplateRef<any>;

  constructor(private modalService: BsModalService, private _adminSettingsService: AdminSettingService) { }

  show(): void {
    this._initForm()
    this.modalRef = this.modalService.show(this.template, this.config);
  }

  _initForm(){
    this.scriptForm = new UntypedFormGroup({
      title: new UntypedFormControl(null, Validators.required),
      htmlData: new UntypedFormControl(null),
      src: new UntypedFormControl(null, Validators.required)
    })
  }

  onScriptFormSubmit(){
    if(this.scriptForm.valid){
      this._adminSettingsService.addScriptTags(this.scriptForm.value).then(result => {
      })
    }else{
      this.scriptForm.markAllAsTouched()
    }
  }

  onClose(){
    this.scriptForm.reset()
    this.modalRef.hide()
  }

}
