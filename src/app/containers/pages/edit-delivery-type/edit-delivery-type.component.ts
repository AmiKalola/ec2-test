import { Component, TemplateRef, ViewChild} from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { ImageCropModelComponent } from '../image-crop-model/image-crop-model.component';
import { CommonService } from '../../../services/common.service';
import { environment } from 'src/environments/environment';
import { Helper } from 'src/app/shared/helper';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { NotifiyService } from 'src/app/services/notifier.service';
import { DeliveryService } from 'src/app/services/delivery.service';

@Component({
  selector: 'app-edit-delivery-type',
  templateUrl: './edit-delivery-type.component.html',
})
export class EditDeliveryTypeComponent{
  modalRef: BsModalRef;
  config = {
    backdrop: true,
    ignoreBackdropClick: true,
    class: 'modal-right'
  };
  name: any = '';
  image_settings: any;
  delivery_imagefile: any;
  upload_delivery_imageurl: any = '';
  image_type: number = 1;
  form_data: FormData;
  IMAGE_URL = environment.imageUrl
  is_edit: boolean = false
  delivery_type:any;
  @ViewChild('template', { static: true }) template: TemplateRef<any>;
  @ViewChild('cropModel', { static: true }) cropModel: ImageCropModelComponent;

  constructor(private modalService: BsModalService,
    private _commonService: CommonService,
    private _deliveryService: DeliveryService,
    private _helper: Helper,
    private _notifierService:NotifiyService
    ) { }

  show(deliverytype): void {
    this.initForm();
    this.form_data = new FormData();
    this.delivery_type = deliverytype.unique_id
    if (deliverytype != '') {
          this.is_edit = true;
          this.deliverytypeForm.patchValue({ name: deliverytype.name,});
          if(deliverytype.image_url===''){this.upload_delivery_imageurl = deliverytype.image_url}
          if(deliverytype.image_url!==''){this.upload_delivery_imageurl = this.IMAGE_URL+deliverytype.image_url}
          this.form_data.append('delivery_type_id', deliverytype._id)
    } 
    this.modalRef = this.modalService.show(this.template, this.config);
    this.fetch_image_settings()
  }

  fetch_image_settings() {
    this._commonService.fetch_image_settings().then(res_data => {
      if (res_data.success) {
        this.image_settings = res_data.image_setting
      }
    })
  }
  deliverytypeForm: FormGroup;

  initForm() {
    this.deliverytypeForm = new FormGroup({
      name: new FormControl('', Validators.required),
    });
  }

  onUpdate() {
    if(this.deliverytypeForm.invalid){
      this.deliverytypeForm.markAllAsTouched();
      return
    }
    this.form_data.append('name', this.deliverytypeForm.value.name)
    this._deliveryService.update_delivery_type(this.form_data)
    this.onClose()
  }

  onSelectImageFile(event, type) {  //1: Vehicle, 2: Map-pin
    this.image_type = type
    let files = event.target.files;
    if (files.length === 0)
      return;
    const mimeType = files[0].type;
    let fileType=this._helper.uploadFile.filter((element)=> {
      return mimeType==element;
    })
    if (mimeType != fileType) {
      this._notifierService.showNotification('error', this._helper._trans.instant('validation-title.invalid-image-format'));
      return
    }
    let aspectRatio = this.image_settings.item_image_ratio;
    let resizeToWidth = this.image_settings.item_image_max_width;
    this.cropModel.imageChangedEvent = event;
    this.cropModel.show(aspectRatio, resizeToWidth);
  }

  imageCropped(event) {
    const reader = new FileReader();
    reader.readAsDataURL(event);
    if (this.image_type === 1) {
      this.delivery_imagefile = event;
      this.form_data.append('image_url', this.delivery_imagefile)
      reader.onload = (_event) => {
        this.upload_delivery_imageurl = reader.result
            
      }
    } 
  }

  onClose() {
    this.modalRef.hide()
    this.form_data = new FormData();
    this.is_edit = false
    this.name = ''
    this.upload_delivery_imageurl = ''
  }

}
