import { Component, TemplateRef,  ViewChild, OnInit } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { UntypedFormGroup, UntypedFormControl, Validators } from '@angular/forms';
import { AdminSettingService } from '../../../services/admin-setting.service';
import { CountryService } from 'src/app/services/country.service';
import { Helper } from 'src/app/shared/helper';
@Component({
  selector: 'app-edit-admin-details-modal',
  templateUrl: './edit-admin-details-modal.component.html',
  styleUrls: ['./edit-admin-details-modal.component.scss']
})
export class EditAdminDetailsModalComponent implements OnInit {

  modalRef: BsModalRef;
  config = {
    backdrop: true,
    ignoreBackdropClick: true,
    class: 'modal-right'
  };

  admin_details:UntypedFormGroup;
  all_countries = []
  timezone_list = []
  priorities = [
    {value: 1, name: 'label-title.high'},
    {value: 2, name: 'label-title.medium'},
    {value: 3, name: 'label-title.low'},
  ]
  maxNumberRange: Array<any> = []
  minNumberRange: Array<any> = [7, 8, 9, 10, 11, 12, 13, 14, 15]

  @ViewChild('template', { static: true }) template: TemplateRef<any>;

  constructor(public _helper:Helper,private modalService: BsModalService,private _adminSettingService: AdminSettingService,private _countryService:CountryService) { }

  ngOnInit(): void {
    this._initForm()
    this._countryService.getAllCountry().then(res=>{
      this.all_countries = res.country_list;
    })
    this._countryService.getTimezoneList().then(res=>{
      this.timezone_list = res
    })
  }

  show(admin_details=null): void {
    this.admin_details.patchValue(admin_details)
    if(this.admin_details.value.admin_country){
      this.admin_details.controls.admin_country.disable()
    }
    let minimum_phone_number_length = admin_details.minimum_phone_number_length
    this.maxNumberRange = this.minNumberRange.filter(d=>d >= minimum_phone_number_length)

    this.modalRef = this.modalService.show(this.template, this.config);
    if(this.admin_details.value.minimum_phone_number_length){
      this.onMinSelect(this.admin_details.value.minimum_phone_number_length);
    }
  }

  _initForm() {
    this.admin_details = new UntypedFormGroup({
      app_name: new UntypedFormControl(null,Validators.required),
      admin_email: new UntypedFormControl(null,[Validators.required, Validators.email]),
      admin_phone_number: new UntypedFormControl(null,Validators.required),
      admin_contact_email: new UntypedFormControl(null,[Validators.required, Validators.email]),
      admin_contact_phone_number: new UntypedFormControl(null,Validators.required),
      latitude: new UntypedFormControl(0, Validators.required),
      longitude: new UntypedFormControl(0, Validators.required),
      admin_panel_timezone: new UntypedFormControl(null,Validators.required),
      provider_timeout: new UntypedFormControl(null,Validators.required),
      service_provider_timeout: new UntypedFormControl(null,Validators.required),
      default_search_radius: new UntypedFormControl(null,Validators.required),
      no_of_loop_for_send_request: new UntypedFormControl(1, [Validators.required, Validators.min(1), Validators.max(1000)]),
      admin_country: new UntypedFormControl(null,Validators.required),
      admin_currency_code: new UntypedFormControl(null,Validators.required),
      admin_currency: new UntypedFormControl(null,Validators.required),
      activity_log_priority: new UntypedFormControl([], Validators.required),
      maximum_phone_number_length: new UntypedFormControl(12, Validators.required),
      minimum_phone_number_length: new UntypedFormControl(7, Validators.required)
    })
  }

  onChangeCountry(){
    if(this.admin_details.value.admin_country){
      this._countryService.getCountryDetailForAdmin({countryname:this.admin_details.value.admin_country}).then(res=>{
        this.admin_details.patchValue({
          admin_currency_code:res.lookup.currencies[0],
          admin_currency:res.currency_symbol
        })
      })
    }
  }

  onMinSelect(event){
    this.maxNumberRange = []
    this.minNumberRange.forEach(number => {
      if(number >= event){
        this.maxNumberRange.push(number)
      }
    })
  }

  onSave(){
    if(this.admin_details.invalid){
      this.admin_details.markAllAsTouched();
      return;
    }    
    this._adminSettingService.updateAdminSetting(this.admin_details.value)
    this.modalRef.hide()
  }
}
