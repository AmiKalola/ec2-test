import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AddOfferAdvanceSettingModalComponent } from './add-offer-advance-setting-modal.component';

describe('AddOfferAdvanceSettingModalComponent', () => {
  let component: AddOfferAdvanceSettingModalComponent;
  let fixture: ComponentFixture<AddOfferAdvanceSettingModalComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AddOfferAdvanceSettingModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddOfferAdvanceSettingModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
