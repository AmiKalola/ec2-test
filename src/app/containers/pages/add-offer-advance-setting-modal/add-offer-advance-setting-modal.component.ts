import { Component, TemplateRef, ViewChild } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { PromocodeService } from '../../../services/promocode.service';
import { ImageCropModelComponent } from 'src/app/containers/pages/image-crop-model/image-crop-model.component'
import { Helper } from 'src/app/shared/helper';
import { CommonService } from 'src/app/services/common.service';
import { LangService } from 'src/app/shared/lang.service';
import { NotifiyService } from 'src/app/services/notifier.service';
import { DeliveryFeesService } from 'src/app/services/delivery-fees.service';

@Component({
  selector: 'app-add-offer-advance-setting-modal',
  templateUrl: './add-offer-advance-setting-modal.component.html',
})
export class AddOfferAdvanceSettingModalComponent {
  todayDate:Date= new Date();
  modalRef: BsModalRef;
  promoImage: any;
  upload_promo_image: any;
  form_data: FormData;
  image_settings: any;
  config = {
    backdrop: true,
    ignoreBackdropClick: true,
    class: 'modal-right'
  };
  country_list: any = [];
  delivery_list: any = [];
  city_list: any = [];
  is_edit: boolean = false;
  item_product_array: any = [];
  selectedPromoApply: any;
  admin_data: any;
  user_search_string: string = "";
  promo_code_detail: any = {
    country_id: '',
    city_id: '',
    admin_loyalty: 0,
    admin_loyalty_type: 1,
    promo_code_name: '',
    promo_code_type: 1,
    promo_code_value: 0,
    promo_code_details:'',
    promo_for: '',
    is_active: false,
    is_promo_have_date: false,
    promo_start_time: '',
    promo_end_time: '',
    promo_code_apply_on_minimum_item_count: 0,
    promo_apply_after_completed_order: 0,
    promo_code_apply_on_minimum_amount: 0,
    promo_code_max_discount_amount: 0,
    promo_code_uses: 0,
    days: [],
    weeks: [],
    months: [],
    is_promo_apply_on_completed_order: false,
    is_promo_required_uses: false,
    is_promo_have_minimum_amount_limit: false,
    is_promo_have_item_count_limit: false,
    is_promo_have_max_discount_limit: false,
  };
  bsRangeValue: any;
  approveModalConfig = {
    backdrop: true,
    ignoreBackdropClick: true,
  };
  approveModelRef: BsModalRef;
  selected_country:any;

  @ViewChild('approveTemplate', { static: true }) approveTemplate: TemplateRef<any>;
  @ViewChild('cropModel', { static: true }) cropModel: ImageCropModelComponent;
  @ViewChild('template', { static: true }) template: TemplateRef<any>;

  constructor(public offerService: PromocodeService,
    public _helper: Helper,
    private modalService: BsModalService,
    private _commonService: CommonService,
    private _lang: LangService,
    private _notifierService: NotifiyService,
    private _deliveryFeesService:DeliveryFeesService,
    ) { }

  async show(promo_id): Promise<void> {
    this.promo_code_detail = {
      country_id: '',
      city_id: '',
      admin_loyalty: 0,
      admin_loyalty_type: 1,
      promo_code_name: '',
      promo_code_type: 1,
      promo_code_details:'',
      promo_code_value: 0,
      promo_for: '',
      is_active: false,
      is_promo_have_date: false,
      promo_start_time: '',
      promo_end_time: '',
      promo_code_apply_on_minimum_item_count: 0,
      promo_apply_after_completed_order: 0,
      promo_code_apply_on_minimum_amount: 0,
      promo_code_max_discount_amount: 0,
      promo_code_uses: 0,
      days: [],
      weeks: [],
      months: [],
      is_promo_apply_on_completed_order: false,
      is_promo_required_uses: false,
      is_promo_have_minimum_amount_limit: false,
      is_promo_have_item_count_limit: false,
      is_promo_have_max_discount_limit: false
    }
    this.is_edit = false;
    this.admin_data = JSON.parse(localStorage.getItem('adminData'))
    this.form_data = new FormData();
    this.fetch_image_settings()
    this.get_country_list()
    await this.get_delivery_list()
    this.modalRef = this.modalService.show(this.template, this.config);
    if (promo_id) {
      this.is_edit = true
      this.offerService.fetch(promo_id).then(async res_data => {
        if (res_data.success) {
          let start_data = [0,0]
          let end_data = [0,0]
          this.promo_code_detail = res_data.promo_code_detail;
          if(this.promo_code_detail.promo_start_date && this.promo_code_detail.promo_expire_date){
            this.bsRangeValue = [new Date(this.promo_code_detail.promo_start_date), new Date(this.promo_code_detail.promo_expire_date)]
          }

          if(this.promo_code_detail.promo_start_time){
            start_data = this.promo_code_detail.promo_start_time.split(":");
          }
          if(this.promo_code_detail.promo_start_time){
            end_data = this.promo_code_detail.promo_end_time.split(":");
          }
          let start_time = new Date(new Date().setHours(Number(start_data[0]), Number(start_data[1]), 0));
          let end_time = new Date(new Date().setHours(Number(end_data[0]), Number(end_data[1]), 0));
          this.promo_code_detail.promo_start_time = start_time
          this.promo_code_detail.promo_end_time = end_time
          this.promo_code_detail.promo_id = res_data.promo_code_detail._id;
          this.promo_code_detail.created_id = res_data.promo_code_detail.created_id
          this.onCountrySelected(this.promo_code_detail.country_id)
          if(this.promo_code_detail.promo_for == this._helper.ADMIN_DATA_ID.USER){
            this.onPromoForUser(this.promo_code_detail.promo_apply_on)
          }else{
           await this.onPromoFor(this.promo_code_detail.promo_for, false)
            this.onItemproductList(this.promo_code_detail.promo_apply_on)
          }
          this.promo_code_detail.store_ids = JSON.stringify(this.promo_code_detail.store_ids)
          this.upload_promo_image = this._helper.image_url + this.promo_code_detail.image_url
          promo_id = null
        }
      });
    } else {
      this.promo_code_detail.created_id = this.admin_data._id
    }
    if(this.is_edit){
      if(!this._helper.has_permission(this._helper.PERMISSION.EDIT)){
        let elements : any = document.getElementsByTagName('form')[0]?.elements;
        for(const element of elements){
          if(element.tagName != 'BUTTON'){
            element.setAttribute('disabled' , 'true')
          }
        }
      }
    }
  }

  get_country_list(){
    this._deliveryFeesService.get_server_country_list().then(res_data => {
      this.country_list = res_data.countries
    })
  }

  async get_delivery_list(){
   await this.offerService.get_delivery_list.then(res_data => {
     this.delivery_list = res_data?.deliveries;
    })
  }

  onCountrySelected(country_id){
    let country = this.country_list.filter(value =>  country_id == value._id)
    this.selected_country= country[0];

    this.city_list = []
    if(this.is_edit === false){
      this.promo_code_detail.city_id = null;
    }
    this._deliveryFeesService.get_city_lists(country_id).then(res_data => {
      this.city_list = res_data.cities
    })
  }

  onCitySelected(city_id){
    if(city_id == '000000000000000000000000' && (this.promo_code_detail.promo_for == 21 || this.promo_code_detail.promo_for == 22)){
      this.promo_code_detail.promo_for = ''
    }
    let boolean = false
    if(this.promo_code_detail.promo_for){
      boolean = true
    }
    this.onPromoFor(this.promo_code_detail.promo_for, boolean)
  }

 async onPromoFor(promo_for, boolean){ // 0: Deliveries, 2: store, 20: service, 21: product, 22: item
    this.item_product_array = []
    if(boolean){
      this.promo_code_detail.promo_apply_on = []
    }
    let json = {
      city_id: this.promo_code_detail.city_id
    }
    if(promo_for === 21){
      this.offerService.product_for_city_list(json).then(res_data => {
        let city = res_data.city
        city.forEach(city => {
          if(city._id == this.promo_code_detail.city_id){
            city.product_detail.forEach(product => {
              product.store_name = city.store_detail.name ? city.store_detail.name : city.store_detail.delivery_name
              this.item_product_array.push(product)
            });
          }
        });
      })
    } else if (promo_for === 22){
      this.offerService.item_for_city_list(json).then(res_data => {
        let city = res_data.city
        city.forEach(city => {
          if(city._id === this.promo_code_detail.city_id){
            city.item_detail.forEach(item => {
              item.store_name = city.store_detail.name ? city.store_detail.name : city.store_detail.delivery_name
              this.item_product_array.push(item)
            });
          }
        });
      })
    } else if (promo_for === 2){
      this.offerService.get_store_list_for_city(json).then(res_data => {
        this.item_product_array = res_data.stores
      })
    } else if (promo_for === 0) {
      await this.delivery_list.forEach(delivery => {
          this.item_product_array.push({name: delivery.delivery_name, _id: delivery._id,delivery_type:delivery.delivery_type})
      })
    } 
  }

  search_user(event){
    event.stopPropagation();
    let json = {
      search_string: this.user_search_string,
      wallet_currency_code : this.selected_country.currency_code,
    }
    if(this._helper.has_permission(this._helper.PERMISSION.EDIT) || this._helper.has_permission(this._helper.PERMISSION.ADD)){
      this.offerService.get_user_list(json).then(res_data => {
        this.item_product_array = res_data.users
      })
    }
  }
  
  onPromoForUser(promo_apply_on){ // 0: Deliveries, 2: store, 20: service, 21: product, 22: item
    let json = {
      search_string: this.user_search_string,
      promo_apply_on: promo_apply_on
    }
    this.offerService.get_user_list(json).then(res_data => {
      this.item_product_array = res_data.users
    })
  }

  fetch_image_settings() {
    this._commonService.fetch_image_settings().then(res_data => {
      if (res_data.success) {
        this.image_settings = res_data.image_setting
      }
    })
  }

  updatePromo(form:any) {
    this._helper.checkAndCleanFormValues(form);

    if(form.invalid){
      form.submitted = true ;
      return ;
    }
    if(this.bsRangeValue){
      this.promo_code_detail.promo_start_date = this.bsRangeValue[0]
      this.promo_code_detail.promo_expire_date = this.bsRangeValue[1]
    }

    let promo_start_time = this.promo_code_detail.promo_start_time
    let promo_end_time = this.promo_code_detail.promo_end_time

    if (promo_start_time) {
      this.promo_code_detail.promo_start_time = this.updateTimetoTwoDigit(promo_start_time.getHours()) + ":" + this.updateTimetoTwoDigit(promo_start_time.getMinutes());
    }
    if (promo_end_time) {
      this.promo_code_detail.promo_end_time = this.updateTimetoTwoDigit(promo_end_time.getHours()) + ":" + this.updateTimetoTwoDigit(promo_end_time.getMinutes());
    }
    this.promo_code_detail.store_ids = []


    Object.keys(this.promo_code_detail).forEach(_key=>{
      if(_key == 'promo_apply_on'){
        this.form_data.append(_key,this.promo_code_detail.promo_apply_on.toString());
      }else if(_key !== 'created_id'){
        this.form_data.append(_key,this.promo_code_detail[_key]);
      }
    })

    this.promo_code_detail.is_active = this.promo_code_detail.is_active ? this.promo_code_detail.is_active : false;
    this.promo_code_detail.is_promo_have_date = this.promo_code_detail.is_promo_have_date ? this.promo_code_detail.is_promo_have_date : false;
    this.promo_code_detail.is_promo_apply_on_completed_order = this.promo_code_detail.is_promo_apply_on_completed_order ? this.promo_code_detail.is_promo_apply_on_completed_order : false;
    this.promo_code_detail.is_promo_required_uses = this.promo_code_detail.is_promo_required_uses ? this.promo_code_detail.is_promo_required_uses : false;
    this.promo_code_detail.is_promo_have_minimum_amount_limit = this.promo_code_detail.is_promo_have_minimum_amount_limit ? this.promo_code_detail.is_promo_have_minimum_amount_limit : false;
    this.promo_code_detail.is_promo_have_item_count_limit = this.promo_code_detail.is_promo_have_item_count_limit ? this.promo_code_detail.is_promo_have_item_count_limit : false;
    this.promo_code_detail.is_promo_have_max_discount_limit = this.promo_code_detail.is_promo_have_max_discount_limit ? this.promo_code_detail.is_promo_have_max_discount_limit : false;


    if (this.promo_code_detail.promo_id) {
      this.offerService.update(this.form_data).then(res_data => {
        this.modalRef.hide();
        if (res_data.success) {
          this.upload_promo_image = ''
          setTimeout(() => {
            this.promo_code_detail = {}
          }, 200);
        }
        this.is_edit = false
        this.form_data = new FormData
      });
    } else {
      this.offerService.add(this.form_data).then(res_data => {
        this.modalRef.hide();
        if (res_data.success) {
          this.upload_promo_image = ''
          setTimeout(() => {
            this.promo_code_detail = {}
          }, 200);
        }
        this.is_edit = false
        this.form_data = new FormData
      });
    }
  }

  unapprove:boolean;
  onApprovePromo(boolean){
    this.unapprove = boolean;
    this.modalRef.hide();
    this.approveModelRef = this.modalService.show(this.approveTemplate, this.approveModalConfig);
  }

  approve(){
    this.promo_code_detail.is_approved = this.unapprove;
    this.promo_code_detail.store_ids = []

    this.offerService.update(this.promo_code_detail).then(res_data => {
      if (res_data.success) {
        this.modalRef.hide();
        this.approveModelRef.hide();
        this.form_data = new FormData
      }
    });
  }

  cancelAprroveModal(){
    this.approveModelRef.hide();
    this.modalRef = this.modalService.show(this.template, this.config);
  }

  onSelectImageFile(event) {
    let files = event.target.files;
    if (files.length === 0)
      return;
    const mimeType = files[0].type;
    let fileType=this._helper.uploadFile.filter((element)=> {
      return mimeType==element;
    })
    if (mimeType != fileType) {
      this._notifierService.showNotification('error',this._helper._trans.instant('validation-title.invalid-image-format'));
      return;
    }
    let aspectRatio = this.image_settings.item_image_ratio;
    let resizeToWidth = this.image_settings.item_image_max_width;
    this.cropModel.imageChangedEvent = event;
    this.cropModel.show(aspectRatio, resizeToWidth);
  }

  imageCropped(event) {
    const reader = new FileReader();
    reader.readAsDataURL(event);
    this.promoImage = event;
    this.form_data.append('image_url', this.promoImage)
    reader.onload = (_event) => {
      this.upload_promo_image = reader.result
    }
  }

  onClose(){
    this.is_taxi_courier_business = false;
    this.modalRef.hide()
    this.upload_promo_image = ''
    this.is_edit = false
    this.promo_code_detail = {
      country_id: '',
      city_id: '',
      admin_loyalty: 0,
      admin_loyalty_type: 1,
      promo_code_name: '',
      promo_code_type: 1,
      promo_code_details:'',
      promo_code_value: 0,
      promo_for: '',
      is_active: false,
      is_promo_have_date: false,
      promo_start_time: '',
      promo_end_time: '',
      promo_code_uses: 0,
      days: [],
      weeks: [],
      months: [],
      is_promo_apply_on_completed_order: false,
      is_promo_required_uses: false,
      is_promo_have_minimum_amount_limit: false,
      is_promo_have_item_count_limit: false,
      is_promo_have_max_discount_limit: false,
    };
  }

  onPromoRecursion(event){
  }

  updateTimetoTwoDigit(value) {
    return Number(value) < 10 ? "0" + value : value;
  }
  
  is_taxi_courier_business = false;

  onItemproductList(evt = []){
    if (evt.length === 0 || evt.length >= 2) {
      this.is_taxi_courier_business = false;
      return;
    }
    if (evt.length == 1) {
      for (const item of this.item_product_array) {
        if (evt.includes(item._id) && (item.delivery_type === 2 || item.delivery_type === 4)) {
          this.is_taxi_courier_business = true;
          return;
        }
      }
      this.is_taxi_courier_business = false;
    } else if (evt.length == 2) {
      let validItemCount = 0;
      evt.forEach(id => {
        const item = this.item_product_array.find(d => d._id === id);
        if (item && (item.delivery_type === 2 || item.delivery_type === 4)) {
          validItemCount++;
        }
      });
      this.is_taxi_courier_business = (validItemCount === 2);
      return this.is_taxi_courier_business
    }
  }

  
}


// NO_RECURSION: 0,
//     DAILY_RECURSION: 1,
//     WEEKLY_RECURSION: 2,
//     MONTHLY_RECURSION: 3,
//     ANNUALLY_RECURSION: 4
