import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AddDManInfoModalComponent } from './add-d-man-info-modal.component';

describe('AddDManInfoModalComponent', () => {
  let component: AddDManInfoModalComponent;
  let fixture: ComponentFixture<AddDManInfoModalComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AddDManInfoModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddDManInfoModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
