import { Component, ElementRef, EventEmitter, OnInit, Output, TemplateRef,  ViewChild } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { Provider } from 'src/app/models/provider.model';
import { ProviderService, provider_page_type } from '../../../services/delivery-boy.service'
import { Helper } from 'src/app/shared/helper';
import { UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { ImageCropModelComponent } from '../image-crop-model/image-crop-model.component';
import { CommonService } from 'src/app/services/common.service';
import { AddWalletModelComponent } from '../add-wallet-modal/add-wallet-modal.component';
import { Lightbox } from 'ngx-lightbox';
import { LangService } from 'src/app/shared/lang.service';
import { NotifiyService } from 'src/app/services/notifier.service';

@Component({
  selector: 'app-add-d-man-info-modal',
  templateUrl: './add-d-man-info-modal.component.html',
  styleUrls: ['./add-d-man-info-modal.component.scss']
})
export class AddDManInfoModalComponent implements OnInit {
  showPassword: boolean = false; // password eye icon boolean

  fiveRate=5;
  modalRef: BsModalRef;
  confirmModelRef: BsModalRef;
  config = {
    backdrop: true,
    ignoreBackdropClick: true,
    class: 'modal-right'
  };
  confirmationModalConfig = {
    backdrop: true,
    ignoreBackdropClick: true,
  };
  todayDate = new Date()

  isChatTab = false;
  isCollapsedAnimated = true;
  isCollapsedAnimated1 = false;
  user_detail: Provider = {
    provider_id: '',
    first_name: '',
    last_name: '',
    phone: '',
    country_phone_code: '',
    email: '',
    image_url: '',
    password: '',
    is_use_wallet: false,
    is_active_for_job: false,
    wallet: 0,
    wallet_currency_code: '',
    created_at: '',
    app_version: 'any',
    device_type: '',
    referral_code: '',
    store_rate: 0,
    user_rate:0,
    provider_rate: 0,
    comment: '',
    is_email_verified: false,
    is_phone_verified: false,
    server_token: '',
    unique_id:0,
    provider_type_id:'',
    is_approved: false,
    service_delivery_type: 1,
    delivery_types: [],
    service_id: []
  };
  
  deliveryType1Buttons = [
    { id: this._helper.DELIVERY_TYPE_CONSTANT.STORE, label: 'label-title.store' },
    { id: this._helper.DELIVERY_TYPE_CONSTANT.COURIER, label: 'label-title.courier' },
    { id: this._helper.DELIVERY_TYPE_CONSTANT.TAXI, label: 'label-title.taxi' },
    { id: this._helper.DELIVERY_TYPE_CONSTANT.ECOMMERCE, label: 'label-title.ecommerce' }
  ];

  deliveryType2Buttons = [
    { id: this._helper.DELIVERY_TYPE_CONSTANT.SERVICE, label: 'label-title.service' },
    { id: this._helper.DELIVERY_TYPE_CONSTANT.APPOINMENT, label: 'label-title.appointment' }
  ];

  selectedButtons1: any = [];
  selectedButtons2: any = [];
  business_type_list: any = [];
  selectedBusinessId: string = null;

  provider_page_type: any = provider_page_type;
  documents: any[] = [];
  referral_history: any[] = [];
  review_list: any[] = [];
  vehicle_list: any = [];
  is_edit: boolean = false;
  is_edit_doc:boolean=false;

  show_review_type: number = 1;

  vehicleForm: UntypedFormGroup;
  service_list: any = [];
  provider_id: any;
  vehicle_id: any;
  image_settings: any;
  document_image: any = '';
  document_display_image: any = '';
  vehicle_document_form_data: FormData;
  DOCUMENT_DEFAULT_IMAGE: any;
  selected_document: any;
  provider_document_form_data: any;
  provider_form_data: any;
  provider
  image_type: number;
  base64Image: any;
  upload_provider_image: any = '';
  is_vehicle_edit: boolean = false;
  isCollapsed: boolean = false;
  minimum_length: number= 8;
  maximum_length: number= 12;
  timezone_for_display_date:string = '';
  approveModalConfig = {
    backdrop: true,
    ignoreBackdropClick: true,
  };
  approveModelRef: BsModalRef;
  selectedUser : any ;
  approvalStatus : any ;
  unique_id:number;
  is_editVehicelDoc:boolean = false
  deliveries_list:any=[];
  @Output() updatedData = new EventEmitter<any>();
  @ViewChild('approveTemplate', { static: true }) approveTemplate: TemplateRef<any>;
  @ViewChild('cropModel', { static: true }) cropModel: ImageCropModelComponent;
  @ViewChild('template', { static: true }) template: TemplateRef<any>;
  @ViewChild('confirmationTemplate', { static: true }) confirmationTemplate: TemplateRef<any>;
  @ViewChild('walletAmount', {static: true}) walletAmount: AddWalletModelComponent;
  @ViewChild('deliveryContainer') deliveryContainer: ElementRef;
  constructor(public providerService: ProviderService,
    private modalService: BsModalService,
    public _helper: Helper,
    private _commonService:CommonService,
    private lightBox: Lightbox,
    public _langService: LangService,
    private _notifierService: NotifiyService
    ) {
    this.DOCUMENT_DEFAULT_IMAGE = _helper.DEFAULT_IMAGE_PATH.DOCUMENT
  }

  ngOnInit(){
    this._helper.display_date_timezone.subscribe(data => {
      this.timezone_for_display_date = data;
    })
  }


  show(provider, deliveries): void {
    this.deliveries_list = deliveries;
    this.showPassword =false;
    this.vehicle_document_form_data = new FormData;
    this.provider_document_form_data = new FormData;
    this.provider_form_data = new FormData;
    this.provider_id = provider._id;
    this.unique_id = provider.unique_id;
    this.vehicleForm = new UntypedFormGroup({
      vehicle_name: new UntypedFormControl(null, Validators.required),
      is_approved: new UntypedFormControl(false, Validators.required),
      vehicle_plate_no: new UntypedFormControl(null, Validators.required),
      vehicle_model: new UntypedFormControl(null, Validators.required),
      vehicle_year: new UntypedFormControl(null, Validators.required),
      vehicle_color: new UntypedFormControl(null, Validators.required),
      vehicle_type: new UntypedFormControl(null, Validators.required)
    })
    this.get_provider_detail(provider._id)
    this.modalRef = this.modalService.show(this.template, this.config);
    this.getVehicleList()
    this.fetch_image_settings()
    this.subadmineditpermission();
  }

  //sub admin edit permission is false then profile form disable
  subadmineditpermission(){
    if(!this._helper.has_permission(this._helper.PERMISSION.EDIT)){
      setTimeout(() => {
        let elements :any = document.getElementsByTagName('form')[0]?.elements;
        for(const element of elements){
          if(element.tagName != 'BUTTON'){
            element.setAttribute('disabled' , 'true')
          }
        }
      }, 300);
      this.vehicleForm.disable();
    }
  }

  fetch_image_settings() {
    this._commonService.fetch_image_settings().then(res_data => {
      if (res_data.success) {
        this.image_settings = res_data.image_setting
      }
    })
  }

  get_provider_detail(provider_id){
    this.providerService.getProviderDetail({provider_id: provider_id}).then(res_data => {
      this.user_detail = res_data.provider;
      this.user_detail.provider_id = res_data.provider._id;
      this.user_detail.password = '';
      this.upload_provider_image = this._helper.image_url + this.user_detail.image_url
      this.minimum_length = res_data.minimum_phone_number_length
      this.maximum_length = res_data.maximum_phone_number_length
      if (!this.user_detail.provider_type_id && this.user_detail.is_approved) { // for admin provider 
        if (this.user_detail.service_delivery_type === 1) {
          this.selectedButtons1 = [...this.user_detail.delivery_types];
        } else if (this.user_detail.service_delivery_type === 2) {
          this.selectedButtons2 = [...this.user_detail.delivery_types];
          if (this.selectedButtons2[0]) {
            this.business_type_list = this.deliveries_list.filter((x) => x.delivery_type == this.selectedButtons2[0] && x.is_business === true);
            this.selectedBusinessId = this.user_detail?.service_id[0] ? this.user_detail?.service_id[0] : null;
            setTimeout(() => {
              const selectedButton = document.getElementById(this.selectedBusinessId);
              if (selectedButton) {
                selectedButton.scrollIntoView({ behavior: 'smooth', block: 'end' });
              }
            }, 100);
          }
        }
      }
    })
  }



  getVehicleList() {
    let json = {
      provider_id: this.provider_id
    }
    this.providerService.providerVehicleList(json).then(res_data => {
      this.vehicle_list = res_data.provider_vehicles
      this.service_list = res_data.vehicles
      
      if (this.vehicle_list) {
        this.vehicle_list.forEach(vehicle => {
          vehicle.document_list.forEach(document => {
            if (document.expired_date) {
              document.expired_date = new Date(document.expired_date)
            }
          });
          vehicle.is_edit = false
          vehicle.is_disable = true
        });
      }
    })
  }

  approveDecline(user, type){
    this.selectedUser = user ;
    this.approvalStatus = type ;
    this.modalRef.hide();
    this.approveModelRef = this.modalService.show(this.approveTemplate, this.approveModalConfig);
  }

  approve(){
    let page_type;
    switch(this.approvalStatus){
      case provider_page_type.online:
        page_type = 1;
        break;
      case provider_page_type.approved:
        page_type = 2;
        break;
      case provider_page_type.blocked:
        page_type = 3;
        break;
    }
    let data = {
      "provider_page_type": page_type,
      provider_id: this.selectedUser._id
    }
    this.providerService.approveDeclineProvider(data).then(res_data => {
      this.modalRef.hide();
      this.approveModelRef.hide();
    })
  }

  cancelAprroveModal(){
    this.approveModelRef.hide();
    this.modalRef = this.modalService.show(this.template, this.config);
    this.subadmineditpermission();
  }

  onEdit(vehicle){
    vehicle.is_edit = !vehicle.is_edit
    vehicle.is_disable = !vehicle.is_disable
    this.is_edit = !this.is_edit
    this.isCollapsed = !this.isCollapsed;
    this.vehicleForm.patchValue({
      vehicle_name: vehicle.vehicle_name,
      vehicle_plate_no: vehicle.vehicle_plate_no,
      vehicle_model: vehicle.vehicle_model,
      vehicle_color: vehicle.vehicle_color,
      vehicle_year: vehicle.vehicle_year,
      vehicle_detail: vehicle.vehicle_detail.vehicle_name,
      vehicle_type: vehicle.admin_vehicle_id,
      is_approved: vehicle.is_approved
    })
    this.vehicle_id = vehicle._id
  }
  
  updateProvider(provider_detail_id,userForm){
    this.user_detail.last_name = this.user_detail.last_name.toString().trim();
    this.user_detail.first_name = this.user_detail.first_name.toString().trim();
    if(!this.user_detail.first_name || !this.user_detail.last_name){
       userForm.submitted = true;
       return;
    }

    if (userForm.invalid ) {
      userForm.submitted = true;
      return;
    }
    if(!this.user_detail.provider_type_id && this.user_detail.is_approved){ // for admin provider 
      if(this.user_detail.service_delivery_type == 2){
        if(this.selectedButtons2.length==0){
          this._notifierService.showNotification('error', this._helper._trans.instant('label-title.please-select-service'));
          return
        }else if(!this.selectedBusinessId){
          this._notifierService.showNotification('error', this._helper._trans.instant('label-title.please-select-service-type'));
          return
        }
        this.provider_form_data.append('delivery_types', JSON.stringify(this.selectedButtons2))
        this.provider_form_data.append('service_id', this.selectedBusinessId)
      }else if(this.user_detail.service_delivery_type == 1){
        if(this.selectedButtons1.length==0){
          this._notifierService.showNotification('error', this._helper._trans.instant('label-title.please-select-service'));
          return
        }
        this.provider_form_data.append('delivery_types', JSON.stringify(this.selectedButtons1))
      }
    }
    this.provider_form_data.append('service_delivery_type', this.user_detail.service_delivery_type)
    this.provider_form_data.append('provider_id', this.user_detail.provider_id)
    this.provider_form_data.append('first_name', this.user_detail.first_name)
    this.provider_form_data.append('last_name', this.user_detail.last_name)
    this.provider_form_data.append('phone', this.user_detail.phone)
    this.provider_form_data.append('country_phone_code', this.user_detail.country_phone_code)
    this.provider_form_data.append('email', this.user_detail.email)
    this.provider_form_data.append('image_url', this.user_detail.image_url)
    this.provider_form_data.append('password', this.user_detail.password)
    this.provider_form_data.append('is_use_wallet', this.user_detail.is_use_wallet)
    this.provider_form_data.append('is_active_for_job', this.user_detail.is_active_for_job)
    this.provider_form_data.append('wallet', this.user_detail.wallet)
    this.provider_form_data.append('wallet_currency_code', this.user_detail.wallet_currency_code)
    this.provider_form_data.append('created_at', this.user_detail.created_at)
    this.provider_form_data.append('app_version', this.user_detail.app_version)
    this.provider_form_data.append('device_type', this.user_detail.device_type)
    this.provider_form_data.append('referral_code', this.user_detail.referral_code)
    this.provider_form_data.append('store_rate', this.user_detail.store_rate)
    this.provider_form_data.append('user_rate', this.user_detail.user_rate)
    this.provider_form_data.append('provider_rate', this.user_detail.provider_rate)
    this.provider_form_data.append('comment', this.user_detail.comment)
    this.provider_form_data.append('is_email_verified', this.user_detail.is_email_verified)
    this.provider_form_data.append('is_phone_verified', this.user_detail.is_phone_verified)
    this.provider_form_data.append('server_token', this.user_detail.server_token)
    this.providerService.updateProviderDetail(this.provider_form_data).then((res_data:any) => {
      if(res_data.success){
        this.onClose();
        userForm.submitted =false;
      }
    })
  }

  onVehicleApprove(event, vehicle){
    this.vehicleForm.patchValue({is_approved: event.target.checked})
  }

  vehicleType(event){
    let index = this.service_list.findIndex(x => x._id === event)
    this.vehicleForm.patchValue({vehicle_type: this.service_list[index]._id})
    if(this.vehicle_list.length === 1){
      this.vehicleForm.patchValue({is_approved: true})

    }
  }

  get_document_list(){
    this.is_edit_doc=false;
    let data = {
      id: this.provider_id,
      type: this._helper.USER_TYPE.PROVIDER
    }
    this.providerService.getDocumentList(data).then(res_data => {
      this.documents = res_data.documents;
      this.documents.forEach(document => {
        if(document.expired_date){
          document.expired_date = new Date(document.expired_date)
        } else {
          document.expired_date = ''
        }
      })
    })
  }

  update_document(document){
      
    if(((document.is_expired_date && !document.expired_date) || (document.is_unique_code && !document.unique_code) )){
      document.dataRequired = true;
      return;
    }
    if(!document.image_url && !this.provider_document_form_data.get('image_url') && document.is_mandatory){
      this._notifierService.showNotification('error', this._helper._trans.instant('validation-title.please_upload_image'));
      return;
    }
    
    if(document.expired_date === null){
      document.expired_date = ''
    }
    this.provider_document_form_data.append('id', this.user_detail.provider_id)
    this.provider_document_form_data.append('type', this._helper.USER_TYPE.PROVIDER)
    this.provider_document_form_data.append('unique_code', document.unique_code)
    this.provider_document_form_data.append('expired_date', document.expired_date)
    this.provider_document_form_data.append('document_id', document._id)

    this.providerService.updateDocumentDetail(this.provider_document_form_data).then(res_data => {
      document.is_edit = false;
      this.is_edit_doc=false;
      document.dataRequired = false;
      this.provider_document_form_data = new FormData
      this.document_display_image = ''
      document.image_url = ''
      this.getVehicleList()
      this.get_document_list()
    })
  }

  get_referral_usage_list(){
    let data = {
      id: this.user_detail.provider_id,
      type: this._helper.USER_TYPE.PROVIDER
    }
    this.providerService.getReferralUsageList(data).then(res_data => {
      this.referral_history = res_data.referral_history;
    })
  }

  get_review_usage_list(){
    let data = {
      provider_id: this.user_detail.provider_id
    }
    this.providerService.getReviewList(data).then(res_data => {
      this.review_list = res_data.review_list;
      let review_avg = 0
      this.review_list.forEach(review => {
        review_avg = review_avg + review.user_rating_to_provider
      })
    })
  }

  updateVehicle(vehicle){
    this._helper.checkAndCleanFormValues(this.vehicleForm)
    if(this.vehicleForm.invalid){
      this.vehicleForm.markAllAsTouched()
      return;
    }
    let json = {
      _id: this.vehicle_id,
      admin_vehicle_id: this.vehicleForm.value.vehicle_type,
      is_approved: this.vehicleForm.value.is_approved,
      provider_id: this.provider_id,
      vehicle_color: this.vehicleForm.value.vehicle_color,
      vehicle_model: this.vehicleForm.value.vehicle_model,
      vehicle_name: this.vehicleForm.value.vehicle_name,
      vehicle_plate_no: this.vehicleForm.value.vehicle_plate_no,
      vehicle_year: this.vehicleForm.value.vehicle_year
    }
    this.providerService.providerVehicelUpdate(json).then(res_data => {
      this.getVehicleList();
      this.is_edit=false;
      this.document_display_image = '';
      vehicle.is_edit = false;
      this.isCollapsed = false;
      this.is_editVehicelDoc=false;
    })
  }

  onSelectImageFile(event, document, type) {  //1: vehicle, 2: document, 3: Profile
    this.selected_document = document
    this.image_type = type
    let files = event.target.files;
    if (files.length === 0)
      return;
    const mimeType = files[0].type;
    let fileType : any;
    if (type == 3 || type !== 3) {
      fileType = this._helper.uploadFile.filter((element) => {
        return mimeType == element;
      })
    } else {
      fileType = this._helper.uploadDocFile.filter((element) => {
        return mimeType == element;
      })
    }
    if (mimeType != fileType) {
      if (type == 3 || type !== 3) {
        this._notifierService.showNotification('error', this._helper._trans.instant('validation-title.invalid-image-format'));
      }
    } else {
      this.imageCropped(files[0])
    }
  }

  imageCropped(event) {
    const reader = new FileReader();
    reader.readAsDataURL(event);
    this.document_image = event;
    if(this.image_type === 1){
      this.vehicle_document_form_data.append('image_url', this.document_image)
    } else if(this.image_type === 2) {
      this.provider_document_form_data.append('image_url', this.document_image)
    } else {
      this.provider_form_data.append('image_url', this.document_image)
    }
    reader.onload = (_event) => {
      if(this.image_type === 3){
        this.upload_provider_image = reader.result
      } else {
        this.document_display_image = reader.result
      }
    }
  }

  onVehicelDocumentEdit(document){
    this.is_editVehicelDoc = true
    document.is_edit = true
  }

  onVehicelDocumentUpdate(document){
    if(((document.document_details.is_expired_date && !document.expired_date) || (document.document_details.is_unique_code && !document.unique_code) )){
      document.dataRequired = true;
      return;
    }
    if(!document.image_url && !this.vehicle_document_form_data.get('image_url') && document.document_details.is_mandatory){
      this._notifierService.showNotification('error', this._helper._trans.instant('validation-title.please_upload_image'));
      return;
    }
    if(!document.expired_date){
      document.expired_date = ''
    }
    this.dataAppendOnFormData(this.vehicle_document_form_data, 'id', document.user_id);
    this.dataAppendOnFormData(this.vehicle_document_form_data, 'document_id', document._id);
    this.dataAppendOnFormData(this.vehicle_document_form_data, 'expired_date', document.expired_date);
    this.dataAppendOnFormData(this.vehicle_document_form_data, 'unique_code', String(document.unique_code));
    this.dataAppendOnFormData(this.vehicle_document_form_data, 'type', document.document_for);
    
    this.providerService.upload_documents(this.vehicle_document_form_data).then(res_data => {
      document.is_edit = false
      this.document_display_image = ''
      this.is_editVehicelDoc = false
      document.dataRequired = false;
      this.isCollapsed = false
      this.getVehicleList()
      this.vehicle_document_form_data = new FormData;
    })
  }

  dataAppendOnFormData(formData, key, value) {
    formData.append(key, value);
  }

  onAddWallet(){
    this.modalRef.hide();
    this.walletAmount.show()
  }

  addWalletAmount(amount){
    if(amount != 'close_modal'){
      let json = {
        type: this._helper.USER_TYPE.PROVIDER,
        provider_id: this.user_detail.provider_id,
        wallet: amount
      }
      
      this._commonService.add_wallet_amount(json)
    }else{
      this.modalRef = this.modalService.show(this.template, this.config)
      this.subadmineditpermission();
    }
  }

  onLightBox(document){
    if(document.image_url != ''){
      let src = this._helper.image_url + document.image_url
      this.lightBox.open([{ src, thumb: '', downloadUrl: '' }], 0, { centerVertically: true, positionFromTop: 0, disableScrolling: true, wrapAround: true });
    }
  }

  onClose(){
    this.is_editVehicelDoc = false
    this.isCollapsed = false
    this.is_edit = false
    this.modalRef.hide()
    this.footerHide= false;
    this.user_detail.provider_type_id = null;
    this.business_type_list = [];
    this.selectedButtons1 = [];
    this.selectedButtons2 = [];
    this.selectedBusinessId = null;
  }

  // changes starts from here........

  downloadImage(value,imageName) {
    let imageUrl = value;
    let split_image_url = imageUrl.split(this._helper.image_url)
    if(split_image_url[1] != ''){
      // window.open(image_url)
    }
    this._helper.downloadUrl(imageUrl)
      .subscribe({
        next: (imgData) => {
          this._helper.downloadImage(imgData, imageName)
        },
        error: err => console.log(err)
      });
  }

  // changes end here..........

  deleteAccount(){
    this.modalRef.hide()
    this.confirmModelRef = this.modalService.show(this.confirmationTemplate, this.confirmationModalConfig)
  }

  cancel(){
    this.confirmModelRef.hide()
    this.modalRef = this.modalService.show(this.template, this.config)
    this.subadmineditpermission();
  }

  async confirm(){
    await this.providerService.deleteAccount({provider_id: this.provider_id})
    this.confirmModelRef.hide()
    this.updatedData.emit()
  }

  editDoc(document, i) {
    this.is_edit_doc = true;
    document.is_edit = true;
  }

  footerHide:boolean=false;

  onSelectTab(boolean){
    if(boolean === true){
      this.footerHide= true;
    }else{
      this.footerHide= false;
    }
  }

  provider_filter(id: number): void {
     if (this.user_detail.service_delivery_type === 1) {
      const index = this.selectedButtons1.indexOf(id);
      if (index === -1) {
        this.selectedButtons1.push(id);
      } else {
        this.selectedButtons1.splice(index, 1);
      }
    } else if (this.user_detail.service_delivery_type === 2) { 
      this.selectedBusinessId = null;   
      this.selectedButtons2 = [id];
      this.business_type_list = this.deliveries_list.filter((x) => x.delivery_type == this.selectedButtons2[0] && x.is_business === true);
    }
  }

  get_click_event(id: number): boolean {
    if((this.user_detail.service_delivery_type === 1)){
      return this.selectedButtons1.includes(id);
    }else{
      return this.selectedButtons2.includes(id);
    }
  }

  onSelectBusiness(businessId: string): void {
    this.selectedBusinessId = businessId;
  }
  
}