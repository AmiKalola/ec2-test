import { Component, TemplateRef,  ViewChild } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { PromocodeService } from '../../../services/promocode.service'
import { Helper } from '../../../shared/helper';
@Component({
  selector: 'app-add-new-promo-info-modal',
  templateUrl: './add-new-promo-info-modal.component.html',
})
export class AddNewPromoInfoModalComponent{
  modalRef: BsModalRef;
  config = {
    backdrop: true,
    ignoreBackdropClick: true,
    class: 'modal-right'
  };

  promo_code_detail: any = {

  };
  order_list: any[] = [];

  @ViewChild('template', { static: true }) template: TemplateRef<any>;

  constructor(public offerService: PromocodeService, public _helper: Helper, private modalService: BsModalService) { }

  show(promo_id): void {
    this.modalRef = this.modalService.show(this.template, this.config);
    this.offerService.get_promo_uses_detail(promo_id).then(res_data => {
      if(res_data.success){
        this.promo_code_detail = res_data.promo_code_detail;
        this.order_list = res_data.order_list;
      }
    });
  }

}
