import { Component, ViewChild, EventEmitter, Output, Input, SimpleChanges } from '@angular/core';
import { Helper } from 'src/app/shared/helper';
import { PER_PAGE_LIST } from '../../../shared/constant';

@Component({
    selector: 'today-order-header',
    templateUrl: './today-order-header.component.html'
})
export class TodayOrderHeaderComponent {
    displayOptionsCollapsed = false;
    @Input() searchData:any;
    @Input() showOrderBy = true;
    @Input() showSearch = true;
    @Input() showItemsPerPage = true;
    @Input() showDisplayMode = true;
    @Input() showCheckboxMode = false;
    @Input() showCheckboxModeList = [];
    @Input() displayMode = 'list';
    @Input() selectAllState = '';
    @Input() itemsPerPage = 20;
    @Input() itemOptionsPerPage = PER_PAGE_LIST;
    @Input() itemOrder = { label: 'Product Name', value: 'title' };
    @Input() itemOptionsOrders = [
        { label: 'Product Name', value: 'title' },
        { label: 'Category', value: 'category' },
        { label: 'Status', value: 'status' }];

    @Output() changeDisplayMode: EventEmitter<string> = new EventEmitter<string>();
    @Output() addNewItem: EventEmitter<any> = new EventEmitter();
    @Output() selectAllChange: EventEmitter<any> = new EventEmitter();
    @Output() searchKeyUp: EventEmitter<any> = new EventEmitter();
    @Output() itemsPerPageChange: EventEmitter<any> = new EventEmitter();
    @Output() changeOrderBy: EventEmitter<any> = new EventEmitter();
    @Output() onExport: EventEmitter<any> = new EventEmitter();

    @ViewChild('search') search: any;

    search_value = '';

    constructor(public _helper: Helper) { }
    ngOnChanges(changes: SimpleChanges): void {
      this.search_value=this.searchData;
      
      if(this.itemOrder){
        this.assignOrder(this.itemOrder);
      }
    }

    //for gitlab code optimize
    assignOrder(itemOrder:any){
        this.itemOrder = itemOrder;
    }

    onSelectDisplayMode(mode: string): void {
        this.changeDisplayMode.emit(mode);
    }
    onAddNewItem(): void {
        this.addNewItem.emit(null);
    }
    selectAll(event): void {
        this.selectAllChange.emit(event);
    }
    onChangeItemsPerPage(item): void {
        this.itemsPerPageChange.emit(item);
    }

    onChangeOrderBy(item): void {
        this.itemOrder = item;
        this.changeOrderBy.emit(item);
    }

    onSearchKeyUp($event): void {
        this.searchKeyUp.emit(this.search_value);
    }

    clickonExport(){
        this.onExport.emit()
    }
}
