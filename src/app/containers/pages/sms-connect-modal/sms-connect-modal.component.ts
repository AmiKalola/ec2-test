import { Component, OnInit, TemplateRef,  ViewChild } from '@angular/core';
import { UntypedFormControl, UntypedFormGroup } from '@angular/forms';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { SmsService } from 'src/app/services/sms.service';
@Component({
  selector: 'app-sms-connect-modal',
  templateUrl: './sms-connect-modal.component.html',
  styleUrls: ['./sms-connect-modal.component.scss']
})
export class SmsConnectModalComponent implements OnInit {
  modalRef: BsModalRef;
  config = {
    backdrop: true,
    ignoreBackdropClick: true,
    class: 'modal-right'
  };

  sms_credentials_form:UntypedFormGroup;
  sms_gateway_detail:any

  @ViewChild('template', { static: true }) template: TemplateRef<any>;

  constructor(private modalService: BsModalService,private _smsService: SmsService) { }
  ngOnInit(): void {
    this._initForm()
  }

  show(): void {
    this.modalRef = this.modalService.show(this.template, this.config);
    this.getGatewayDetail()
  }

  _initForm(){
    this.sms_credentials_form = new UntypedFormGroup({
      sms_auth_id:new UntypedFormControl(null),
      sms_auth_token:new UntypedFormControl(null),
      sms_number:new UntypedFormControl(null),
      twiml_url: new UntypedFormControl(null),
    })
  }

  getGatewayDetail() {
    this._smsService.getSmsGatewayDetail().then(res=>{
      if(res.success){
        this.sms_credentials_form.patchValue(res.sms_gateway_detail)
      }
    })
  }

  save() {
    this._smsService.updateSmsConfiguration({...this.sms_credentials_form.value})
    this.onClose()
  }

  onClose() {
    this.modalRef.hide()
  }
}
