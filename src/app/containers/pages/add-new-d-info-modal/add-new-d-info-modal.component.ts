import { Component, TemplateRef, ViewChild } from '@angular/core';
import { UntypedFormArray, UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { AdminSettingService } from 'src/app/services/admin-setting.service';
import { CommonService } from 'src/app/services/common.service';
import { LangService } from 'src/app/shared/lang.service';
import { AddDeliveryTagModalComponent } from '../add-delivery-tag-modal/add-delivery-tag-modal.component';
import { ImageCropModelComponent } from '../image-crop-model/image-crop-model.component';
import { DELIVERY_TYPE, DELIVERY_TYPE_CONSTANT } from 'src/app/shared/constant'
import { DeliveryService } from 'src/app/services/delivery.service';
import { Helper } from 'src/app/shared/helper';
@Component({
  selector: 'app-add-new-d-info-modal',
  templateUrl: './add-new-d-info-modal.component.html',
  styleUrls: ['./add-new-d-info-modal.component.scss']
})
export class AddNewDInfoModalComponent {
  modalRef: BsModalRef;
  config = {
    backdrop: true,
    ignoreBackdropClick: true,
    class: 'modal-right'
  };
  deliveryInfoForm: UntypedFormGroup;
  languages: any = [];
  image_settings: any;
  logo: 1;
  image_type: number = 1;
  delivery_imagefile: any;
  banner_imagefile: any;
  icon_imagefile: any;
  upload_icon_imageurl: any = '';
  upload_banner_imageUrl:any='';
  upload_delivery_imageurl: any = '';
  upload_default_imageurl: any = '';
  form_data: FormData;
  tags: any = [];
  delivery_type = DELIVERY_TYPE
  DELIVERY_TYPE_CONSTANT=DELIVERY_TYPE_CONSTANT;
  selectedLang: number;


  @ViewChild('template', { static: true }) template: TemplateRef<any>;
  @ViewChild('cropModel', { static: true }) cropModel: ImageCropModelComponent;
  @ViewChild('deliveryTagModal', { static: true }) deliveryTagModal: AddDeliveryTagModalComponent
  default_imagefile: any;
  constructor(private modalService: BsModalService,
    private _adminSettingService: AdminSettingService,
    private _lang: LangService,
    private _commonService: CommonService,
    private _deliveryService: DeliveryService,
    private _helper: Helper) { }

  show(delivery_type): void {
    this.delivery_type = delivery_type;
    this.selectedLang = this._lang.selectedlanguageIndex
    this.deliveryInfoForm = new UntypedFormGroup({
      delivery_type: new UntypedFormControl(1, Validators.required),
      sequence_number: new UntypedFormControl(0, Validators.required),
      is_store_can_create_group: new UntypedFormControl(false),
      is_store_can_edit_order: new UntypedFormControl(false),
      is_business: new UntypedFormControl(false),
      is_provide_table_bookings: new UntypedFormControl(false),
      delivery_name: new UntypedFormArray([]),
      description: new UntypedFormArray([]),
      famous_products_tags: new UntypedFormControl([]),
      deleted_product_tag_array: new UntypedFormControl([]),
      is_admin_service_allow: new UntypedFormControl(false),
      theme_number: new UntypedFormControl('1', Validators.required)
    })
    this.modalRef = this.modalService.show(this.template, this.config);
    this.get_admin_setting()
    this.fetch_image_settings()
    this.form_data = new FormData();
  }

  closeModal(){
    this.form_data = new FormData();
    this.upload_delivery_imageurl=''
    this.upload_default_imageurl=''
    this.upload_icon_imageurl=''
    this.upload_banner_imageUrl=''
  }

  fetch_image_settings() {
    this._commonService.fetch_image_settings().then(res_data => {
      if (res_data.success) {
        this.image_settings = res_data.image_setting
      }
    })
  }

  get_admin_setting() {
    let notification = true
    this._adminSettingService.fetchAdminSetting(notification).then(res_data => {
      if (res_data.success) {
        this.languages = res_data.setting.lang
        this.languages.forEach(language => {
          this.delivery_name.push(new UntypedFormControl('', language.code === 'en' ? Validators.required : null))
          this.description.push(new UntypedFormControl())
        });
      }
    })
  }

  showAddNewModal(): void {
    this.deliveryTagModal.show(null)
  }

  get delivery_name() {
    return this.deliveryInfoForm.get('delivery_name') as UntypedFormArray;
  }

  get description() {
    return this.deliveryInfoForm.get('description') as UntypedFormArray;
  }

  onSave() {
  }

  onSelectImageFile(event, type) {  //1: Vehicle, 2: Map-pin
    this.image_type = type
    const files = event.target.files;
    if (files.length === 0)
      return;
    const mimeType = files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      alert("Only images are supported.")
      return;
    }
    let aspectRatio : number;
    let resizeToWidth : any;
    if(this.image_type == 3)
    {
      aspectRatio = 3.2;
      resizeToWidth = this.image_settings.ads_fullscreen_image_max_width;
      this.cropModel.imageChangedEvent = event;
      this.cropModel.show(aspectRatio, resizeToWidth);
    }
    else{
    aspectRatio = 1;
    resizeToWidth = this.image_settings.item_image_max_width;
    this.cropModel.imageChangedEvent = event;
    this.cropModel.show(aspectRatio, resizeToWidth);
    }
  }

  imageCropped(event) {
    const reader = new FileReader();
    reader.readAsDataURL(event);
    if (this.image_type === 1) {
      this.delivery_imagefile = event;
      this.form_data.append('image_url', this.delivery_imagefile)
      reader.onload = (_event) => {
        this.upload_delivery_imageurl = reader.result
      }
    }else if(this.image_type === 3){
      this.banner_imagefile = event;
      this.form_data.append('banner_url', this.banner_imagefile)
      reader.onload = (_event) => {
        this.upload_banner_imageUrl = reader.result
      }
    }else if (this.image_type === 4) {
      this.default_imagefile = event;
      this.form_data.append('default_image_url', this.default_imagefile)
      reader.onload = (_event) => {
        this.upload_default_imageurl = reader.result
      }
    } else {
      reader.onload = (_event) => {
        this.icon_imagefile = event;
        this.form_data.append('icon_url', this.icon_imagefile)
        this.upload_icon_imageurl = reader.result
      }
    }
  }

  tagAdded(event) {
    this.tags.push(event.tag)
    this.deliveryInfoForm.value.famous_products_tags.push(event.tag)
  }

  onSubmit() {
    this._helper.checkAndCleanFormValues(this.deliveryInfoForm);
    if(this.deliveryInfoForm.invalid){
      this.deliveryInfoForm.markAllAsTouched();
      return;
    }
    this.deliveryInfoForm.value.delivery_name?.forEach((delivery,index) => {
      delivery = delivery.toString().trim()
      this.deliveryInfoForm.value.delivery_name[index] = delivery;
    })

    this.form_data.append('delivery_name', JSON.stringify(this.deliveryInfoForm.value.delivery_name))
    this.form_data.append('description', JSON.stringify(this.deliveryInfoForm.value.description))
    this.form_data.append('is_business', this.deliveryInfoForm.value.is_business)
    this.form_data.append('is_provide_table_booking', this.deliveryInfoForm.value.is_provide_table_bookings)
    this.form_data.append('is_store_can_create_group', this.deliveryInfoForm.value.is_store_can_create_group)
    this.form_data.append('is_store_can_edit_order', this.deliveryInfoForm.value.is_store_can_edit_order)
    this.form_data.append('delivery_type', this.deliveryInfoForm.value.delivery_type)
    this.form_data.append('famous_products_tags', JSON.stringify(this.deliveryInfoForm.value.famous_products_tags))
    this.form_data.append('deleted_product_tag_array', JSON.stringify(this.deliveryInfoForm.value.deleted_product_tag_array))
    this.form_data.append('sequence_number', this.deliveryInfoForm.value.sequence_number)
    this.form_data.append('theme_number', this.deliveryInfoForm.value.theme_number)
    this.form_data.append('is_admin_service_allow'  , this.deliveryInfoForm.value.is_admin_service_allow)
    this._deliveryService.add_delivery_data(this.form_data).then(res_data => {
      this.modalRef.hide();
    });
    this.tags = []
    this.form_data = new FormData();
    this.upload_delivery_imageurl=''
    this.upload_default_imageurl=''
    this.upload_icon_imageurl=''
    this.upload_banner_imageUrl=''
  }

  onClose(){
    this.modalRef.hide();
  }

}
