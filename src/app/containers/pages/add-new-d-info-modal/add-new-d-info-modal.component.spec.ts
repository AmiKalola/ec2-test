import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AddNewDInfoModalComponent } from './add-new-d-info-modal.component';

describe('AddNewDInfoModalComponent', () => {
  let component: AddNewDInfoModalComponent;
  let fixture: ComponentFixture<AddNewDInfoModalComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AddNewDInfoModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddNewDInfoModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
