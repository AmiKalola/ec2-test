import { Component, TemplateRef, ViewChild } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
@Component({
  selector: 'activity-logs-modal',
  templateUrl: './activity-logs-modal.component.html',
  styleUrls: ['./activity-logs-modal.component.scss']
})
export class ActivityLogsModalComponent {
  modalRef: BsModalRef;
  config = {
    backdrop: true,
    ignoreBackdropClick: true,
    class: 'modal-right'
  };
  api_name: ''
  activity_log: any;

  @ViewChild('template', { static: true }) template: TemplateRef<any>;

  constructor(private modalService: BsModalService) { }

  show(data): void {
      this.activity_log = data
      let api = data.name.split('/')
      this.api_name = api[api.length - 1]
    this.modalRef = this.modalService.show(this.template, this.config);
  }

  onSave(){
    this.modalRef.hide()
  }

}
