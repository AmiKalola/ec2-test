import { Component, TemplateRef, ViewChild } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { UntypedFormGroup, UntypedFormControl } from '@angular/forms';
import { AdminSettingService } from '../../../services/admin-setting.service';
import { SETTING_TYPE } from 'src/app/shared/constant';
@Component({
  selector: 'app-google-api-key-modal',
  templateUrl: './google-api-key-modal.component.html',
  styleUrls: ['./google-api-key-modal.component.scss'],
})
export class GoogleApiKeyModalComponent  {
  modalRef: BsModalRef;
  config = {
    backdrop: true,
    ignoreBackdropClick: true,
    class: 'modal-right',
  };

  SETTING_TYPE = SETTING_TYPE;

  google_api_key_settings: UntypedFormGroup;
  settings_type: any;
  modal_heading : any ;

  @ViewChild('template', { static: true }) template: TemplateRef<any>;

  constructor(
    private modalService: BsModalService,
    private _adminSettingService: AdminSettingService
  ) {}

  show(google_api_key_settings, type): void {
    if (!google_api_key_settings) {
      google_api_key_settings = null
    }
    this.settings_type = type;
    this._initForm();
    this._patchForm(type, google_api_key_settings);
    this.modalRef = this.modalService.show(this.template, this.config);
  }

  _initForm() {
    
    if(this.settings_type == SETTING_TYPE.WEB_PANEL){
      this.google_api_key_settings = new UntypedFormGroup({
        admin_panel_google_key: new UntypedFormControl(null),
        store_panel_google_key: new UntypedFormControl(null),
        branch_io_key: new UntypedFormControl(null),
        api_google_key_for_server: new UntypedFormControl(null)
      })
    }else{
      this.google_api_key_settings = new UntypedFormGroup({
        google_map_key: new UntypedFormControl(null),
        google_placeautocomplate_key: new UntypedFormControl(null),
        google_geocoding_key: new UntypedFormControl(null),
        google_distance_matrix_key: new UntypedFormControl(null),
        google_direction_matrix_key: new UntypedFormControl(null),
      });
    }
  }

  _patchForm(type, setting_details) {
    switch (type) {
      case SETTING_TYPE.ANDROID_USER:
        this.google_api_key_settings.patchValue({
          google_map_key: setting_details.android_customer_app_google_map_key,
          google_placeautocomplate_key:
            setting_details.android_customer_app_google_places_autocomplete_key,
          google_geocoding_key:
            setting_details.android_customer_app_google_geocoding_key,
          google_distance_matrix_key:
            setting_details.android_customer_app_google_distance_matrix_key,
          google_direction_matrix_key:
            setting_details.android_customer_app_google_direction_matrix_key,
        });
        this.modal_heading = 'heading-title.andriod-user-app'
        break;

      case SETTING_TYPE.ANDROID_PARTNER:
        this.google_api_key_settings.patchValue({
          google_map_key: setting_details.android_partner_app_google_map_key,
          google_placeautocomplate_key:
            setting_details.android_partner_app_google_places_autocomplete_key,
          google_geocoding_key:
            setting_details.android_partner_app_google_geocoding_key,
          google_distance_matrix_key:
            setting_details.android_partner_app_google_distance_matrix_key,
          google_direction_matrix_key:
            setting_details.android_partner_app_google_direction_matrix_key,
        });
        this.modal_heading = 'heading-title.andriod-partner-app'
        break;

      case SETTING_TYPE.ANDROID_STORE:
        this.google_api_key_settings.patchValue({
          google_map_key: setting_details.android_merchant_app_google_map_key,
          google_placeautocomplate_key:
            setting_details.android_merchant_app_google_places_autocomplete_key,
          google_geocoding_key:
            setting_details.android_merchant_app_google_geocoding_key,
          google_distance_matrix_key:
            setting_details.android_merchant_app_google_distance_matrix_key,
          google_direction_matrix_key:
            setting_details.android_merchant_app_google_direction_matrix_key,
        });
        this.modal_heading = 'heading-title.andriod-store-app'
        break;

      case SETTING_TYPE.IOS_USER:
        this.google_api_key_settings.patchValue({
          google_map_key: setting_details.ios_customer_app_google_map_key,
          google_placeautocomplate_key:
            setting_details.ios_customer_app_google_places_autocomplete_key,
          google_geocoding_key:
            setting_details.ios_customer_app_google_geocoding_key,
          google_distance_matrix_key:
            setting_details.ios_customer_app_google_distance_matrix_key,
          google_direction_matrix_key:
            setting_details.ios_customer_app_google_direction_matrix_key,
        });
        this.modal_heading = 'heading-title.ios-user-app'
        break;

      case SETTING_TYPE.IOS_PARTNER:
        this.google_api_key_settings.patchValue({
          google_map_key: setting_details.ios_partner_app_google_map_key,
          google_placeautocomplate_key:
            setting_details.ios_partner_app_google_places_autocomplete_key,
          google_geocoding_key:
            setting_details.ios_partner_app_google_geocoding_key,
          google_distance_matrix_key:
            setting_details.ios_partner_app_google_distance_matrix_key,
          google_direction_matrix_key:
            setting_details.ios_partner_app_google_direction_matrix_key,
        });
        this.modal_heading = 'heading-title.ios-partner-app'
        break;

      case SETTING_TYPE.IOS_STORE:
        this.google_api_key_settings.patchValue({
          google_map_key: setting_details.ios_merchant_app_google_map_key,
          google_placeautocomplate_key:
            setting_details.ios_merchant_app_google_places_autocomplete_key,
          google_geocoding_key:
            setting_details.ios_merchant_app_google_geocoding_key,
          google_distance_matrix_key:
            setting_details.ios_merchant_app_google_distance_matrix_key,
          google_direction_matrix_key:
            setting_details.ios_merchant_app_google_direction_matrix_key,
        });
        this.modal_heading = 'heading-title.ios-store-app'
        break;

      case SETTING_TYPE.WEB_PANEL:
        this.google_api_key_settings.patchValue({
          admin_panel_google_key: setting_details.admin_panel_google_key,
          store_panel_google_key: setting_details.store_panel_google_key,
          branch_io_key: setting_details.branch_io_key,
          api_google_key_for_server: setting_details?.api_google_key_for_server
        });
        this.modal_heading = 'heading-title.web-panel'
        break;

      default:
        break;
    }
  }

  onSave() {
    let json : any ;
    switch (this.settings_type) {
      case SETTING_TYPE.ANDROID_USER:
        json = {
          android_customer_app_google_map_key : this.google_api_key_settings.value.google_map_key ,
          android_customer_app_google_places_autocomplete_key : this.google_api_key_settings.value.google_placeautocomplate_key ,
          android_customer_app_google_geocoding_key : this.google_api_key_settings.value.google_geocoding_key ,
          android_customer_app_google_distance_matrix_key : this.google_api_key_settings.value.google_distance_matrix_key ,
          android_customer_app_google_direction_matrix_key : this.google_api_key_settings.value.google_direction_matrix_key ,
        }
        break;

      case SETTING_TYPE.ANDROID_PARTNER:
        json = {
          android_partner_app_google_map_key : this.google_api_key_settings.value.google_map_key ,
          android_partner_app_google_places_autocomplete_key : this.google_api_key_settings.value.google_placeautocomplate_key ,
          android_partner_app_google_geocoding_key : this.google_api_key_settings.value.google_geocoding_key ,
          android_partner_app_google_distance_matrix_key : this.google_api_key_settings.value.google_distance_matrix_key ,
          android_partner_app_google_direction_matrix_key : this.google_api_key_settings.value.google_direction_matrix_key ,
        }
        break;

      case SETTING_TYPE.ANDROID_STORE:
        json = {
          android_merchant_app_google_map_key : this.google_api_key_settings.value.google_map_key ,
          android_merchant_app_google_places_autocomplete_key : this.google_api_key_settings.value.google_placeautocomplate_key ,
          android_merchant_app_google_geocoding_key : this.google_api_key_settings.value.google_geocoding_key ,
          android_merchant_app_google_distance_matrix_key : this.google_api_key_settings.value.google_distance_matrix_key ,
          android_merchant_app_google_direction_matrix_key : this.google_api_key_settings.value.google_direction_matrix_key ,
        }
        break;

      case SETTING_TYPE.IOS_USER:
        json = {
          ios_customer_app_google_map_key : this.google_api_key_settings.value.google_map_key ,
          ios_customer_app_google_places_autocomplete_key : this.google_api_key_settings.value.google_placeautocomplate_key ,
          ios_customer_app_google_geocoding_key : this.google_api_key_settings.value.google_geocoding_key ,
          ios_customer_app_google_distance_matrix_key : this.google_api_key_settings.value.google_distance_matrix_key ,
          ios_customer_app_google_direction_matrix_key : this.google_api_key_settings.value.google_direction_matrix_key ,
        }
        break;

      case SETTING_TYPE.IOS_PARTNER:
        json = {
          ios_partner_app_google_map_key : this.google_api_key_settings.value.google_map_key ,
          ios_partner_app_google_places_autocomplete_key : this.google_api_key_settings.value.google_placeautocomplate_key ,
          ios_partner_app_google_geocoding_key : this.google_api_key_settings.value.google_geocoding_key ,
          ios_partner_app_google_distance_matrix_key : this.google_api_key_settings.value.google_distance_matrix_key ,
          ios_partner_app_google_direction_matrix_key : this.google_api_key_settings.value.google_direction_matrix_key ,
        }
        break;

      case SETTING_TYPE.IOS_STORE:
        json = {
          ios_merchant_app_google_map_key : this.google_api_key_settings.value.google_map_key ,
          ios_merchant_app_google_places_autocomplete_key : this.google_api_key_settings.value.google_placeautocomplate_key ,
          ios_merchant_app_google_geocoding_key : this.google_api_key_settings.value.google_geocoding_key ,
          ios_merchant_app_google_distance_matrix_key : this.google_api_key_settings.value.google_distance_matrix_key ,
          ios_merchant_app_google_direction_matrix_key : this.google_api_key_settings.value.google_direction_matrix_key ,
        }
        break;

      case SETTING_TYPE.WEB_PANEL:
        json = {
          admin_panel_google_key : this.google_api_key_settings.value.admin_panel_google_key,
          store_panel_google_key : this.google_api_key_settings.value.store_panel_google_key,
          branch_io_key : this.google_api_key_settings.value.branch_io_key,
        }
        break;

      default:
        break;
    }

    this._adminSettingService.updateInstallationSetting(
      json
    );
    this.onClose();
  }

  onClose() {
    this.modalRef.hide();
  }
}
