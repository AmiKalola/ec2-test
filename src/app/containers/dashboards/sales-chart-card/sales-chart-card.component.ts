import { Component,OnInit, ViewChild } from '@angular/core';
import { LineChartComponent } from 'src/app/components/charts/line-chart/line-chart.component';
import { AdminSettingService } from 'src/app/services/admin-setting.service';
import { Helper } from 'src/app/shared/helper';
import { ChartService } from '../../../components/charts/chart.service';
import { lineChartData, pieChartData } from '../../../data/charts';

@Component({
  selector: 'app-sales-chart-card',
  templateUrl: './sales-chart-card.component.html'
})
export class SalesChartCardComponent implements OnInit {
  
  chartDataConfig: any;
  lineChartData = lineChartData;
  pieChartData = pieChartData
  line_chart_item_bsRangeValue = [];
  week_days = [];
  selected_end_month: any;
  selected_start_month: any;
  created_at: Date = null;

  @ViewChild('lineChartComponent', { static: true }) lineChartComponent: LineChartComponent;

  constructor(private chartService: ChartService,
    public _adminSettingService: AdminSettingService, public _helper:Helper) {
    this.chartDataConfig = this.chartService;
  }

  ngOnInit(){
    this.configMonthList()
  }
  configMonthList() {
    this._adminSettingService.getSettingDetail().then((response) => {
      if (response.success && response.setting) {
        this.created_at = response.setting.created_at;
        if (this.created_at) {
          this.createdDateSetting()
        } else{
          this.nonCreatedDate()
        }
      }
    })
  }

  
  changeMonts(selected_start_month, selected_end_month) {
    this.selected_start_month = selected_start_month;
    const networkPayloadDate = new Date(selected_end_month);
    networkPayloadDate.setUTCDate(networkPayloadDate.getUTCDate() - 1);
    const formattedUTCDate = networkPayloadDate.toISOString();
    this.selected_end_month = formattedUTCDate;
    this.line_chart_item_bsRangeValue[0] = new Date(this.selected_start_month);
    this.line_chart_item_bsRangeValue[1] = new Date(this.selected_end_month);
    this.lineChartComponent.parentCalled();
  }
  
  createdDateSetting() {
    this.week_days = this._helper.getSixMonthDifference(this.created_at);
    this.week_days?.reverse();
    this.selected_start_month = this.week_days[0][0];
    const networkPayloadDate = new Date(this.week_days[0][1]);
    networkPayloadDate.setUTCDate(networkPayloadDate.getUTCDate() - 1);
    const formattedUTCDate = networkPayloadDate.toISOString();
    this.selected_end_month = formattedUTCDate;
    this.line_chart_item_bsRangeValue[0] = new Date(this.selected_start_month);
    this.line_chart_item_bsRangeValue[1] = new Date(this.selected_end_month);
    this.lineChartComponent.parentCalled();
  }

  nonCreatedDate(){
    let months;
    let d2 = new Date();
    let d1 = new Date(this.created_at);
    months = (d2.getFullYear() - d1.getFullYear()) * 12;
    months -= d1.getMonth();
    months += d2.getMonth();
    let bet_months = months <= 0 ? 0 : months;
    let months_between = Math.ceil(bet_months/6) + 1;

    for (let index = 0; index < months_between; index++) {
      let date = this.week_days[index - 1] ? this.week_days[index - 1] : new Date();
      this.week_days.push(this._helper.getMonthDay(date, index).toString());
    }
    if (this.week_days.length == 8) {
      this.week_days = [...new Set(this.week_days)];
      this.selected_start_month = this.week_days[0];
      this.selected_end_month = this.week_days[1];
      this.line_chart_item_bsRangeValue[0] = new Date(this.selected_start_month);
      this.line_chart_item_bsRangeValue[1] = new Date(this.selected_end_month);
      this.lineChartComponent.parentCalled();
    }
    this.selected_start_month = this.week_days[1];
    this.selected_end_month = this.week_days[0];
    this.line_chart_item_bsRangeValue[0] = new Date(this.selected_start_month);
    this.line_chart_item_bsRangeValue[1] = new Date(this.selected_end_month);
    this.lineChartComponent.parentCalled();
  }

}
