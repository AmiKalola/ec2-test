import { NgModule } from '@angular/core';
import { SalesChartCardComponent } from './sales-chart-card/sales-chart-card.component';
import { SortableStatisticsRowComponent } from './sortable-statistics-row/sortable-statistics-row.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { ComponentsChartModule } from 'src/app/components/charts/components.charts.module';
import { FormsModule  } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { RatingModule } from 'ngx-bootstrap/rating';
import { ProgressbarModule } from 'ngx-bootstrap/progressbar';
import { ModalModule } from 'ngx-bootstrap/modal';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';

@NgModule({
    declarations: [
        SalesChartCardComponent,
        SortableStatisticsRowComponent,

    ],
    imports: [
        SharedModule,
        ComponentsChartModule,
        RatingModule.forRoot(),
        FormsModule,
        NgSelectModule,
        ProgressbarModule.forRoot(),
        ModalModule.forRoot(),
        BsDropdownModule.forRoot()
    ],
    providers: [ ],
    exports: [
        SalesChartCardComponent,
        SortableStatisticsRowComponent,
    ]
})

export class DashboardsContainersModule { }
