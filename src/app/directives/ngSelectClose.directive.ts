import { Directive, HostListener } from '@angular/core';
import { NgSelectComponent } from '@ng-select/ng-select';

@Directive({
  selector: '[appCloseOnScroll]'
})
export class CloseOnScrollDirective {
  constructor(private ngSelect: NgSelectComponent) {}

  @HostListener('window:scroll', ['$event'])
  onWindowScroll(event) {
    this.ngSelect.close();
  }
  
}