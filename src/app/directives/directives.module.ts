import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClickOutsideDirective} from './clickOutSide.directive';
import { HideIfUnauthorizedDirective} from './hide.directive';
import { InputFocusDirective } from './focus_inputs.directive';
import { DisablePasteDropDirective } from './drop_paste_false.directive';
import { CloseOnScrollDirective } from './ngSelectClose.directive';


@NgModule({
  declarations: [ClickOutsideDirective,HideIfUnauthorizedDirective,DisablePasteDropDirective,InputFocusDirective,CloseOnScrollDirective],
  imports: [
    CommonModule
  ],
  exports:[
    ClickOutsideDirective,
    HideIfUnauthorizedDirective,
    DisablePasteDropDirective,
    InputFocusDirective,
    CloseOnScrollDirective
  ]
})
export class DirectivesModule { }
