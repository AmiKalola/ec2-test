import { environment } from 'src/environments/environment';
import { UserRole } from '../shared/auth.roles';
const adminRoot = environment.adminRoot;

export interface IMenuItem {
  id?: string;
  icon?: string;
  role?: string;
  label: string;
  to: string;
  newWindow?: boolean;
  subs?: IMenuItem[];
  roles?: UserRole[];
}

const data: IMenuItem[] = [
  {
    icon: 'iconsminds-shop-4',
    label: 'menu-title.dashboard',
    to: `${adminRoot}/dashboard`,
    roles: [UserRole.Admin, UserRole.Editor],
    role: '/app/dashboard'
  },
  {
    icon: 'simple-icon-notebook',
    label: 'menu-title.order',
    to: `${adminRoot}/order`,
    subs: [
      {
        icon: 'simple-icon-layers',
        label: 'menu-title.running-order',
        to: `${adminRoot}/order/running-orders`,
        subs: [
          {
            icon: 'simple-icon-basket-loaded',
            label: 'menu-title.orders',
            to: `${adminRoot}/order/running-orders/orders`,
            role: '/admin/orders'
          },
          {
            icon: 'iconsminds-scooter',
            label: 'delivery',
            to: `${adminRoot}/order/running-orders/delivery`,
            role: '/admin/delivery'
          },
          {
            icon: 'iconsminds-notepad',
            label: 'menu-title.table-booking',
            to: `${adminRoot}/order/running-orders/table-booking`,
            role: '/admin/table-booking'
          },
          {
            icon: 'iconsminds-engineering',
            label: 'menu-title.service',
            to: `${adminRoot}/order/running-orders/service`,
            role: '/admin/service'
          },
          {
            icon: 'iconsminds-blackboard',
            label: 'menu-title.appointment',
            to: `${adminRoot}/order/running-orders/appointment`,
            role: '/admin/appointment'
          },
          {
            icon: 'iconsminds-shopping-bag',
            label: 'menu-title.ecommerce',
            to: `${adminRoot}/order/running-orders/ecommerce`,
            role: '/admin/ecommerce'
          },
          {
            icon: 'simple-icon-user-following',
            label: 'menu-title.dispatch',
            to: `${adminRoot}/order/running-orders/dispatch`,
            role: '/admin/dispatch'
          }
        ],
      },
      {
        icon: 'simple-icon-layers',
        label: 'menu-title.history',
        to: `${adminRoot}/order/history`,
        subs: [
          {
            icon: 'iconsminds-receipt-4',
            label: 'menu-title.history',
            to: `${adminRoot}/order/history/history-list`,
            role: '/admin/history-list'
          },
          {
            icon: 'simple-icon-star',
            label: 'menu-title.review',
            to: `${adminRoot}/order/history/reviews`,
            role: '/admin/reviews'
          }
        ],
      },
      {
        icon: 'simple-icon-layers',
        label: 'menu-title.report',
        to: `${adminRoot}/order/reports`,
        subs: [
          {
            icon: 'iconsminds-library',
            label: 'menu-title.report',
            to: `${adminRoot}/order/reports/report`,
            role: '/admin/report'
          }
        ],
      },

    ],
  },
  {
    icon: 'iconsminds-map2',
    label: 'menu-title.map-view',
    to: `${adminRoot}/map-views`,
    subs: [
      {
        icon: 'iconsminds-geo2',
        label: 'menu-title.store-location',
        to: `${adminRoot}/map-views/store-location`,
        role: '/admin/store-location'
      },
      {
        icon: 'iconsminds-location-2',
        label: 'menu-title.provider-location',
        to: `${adminRoot}/map-views/provider-location`,
        role: '/admin/provider-location'
      },
      {
        icon: 'iconsminds-map-marker',
        label: 'menu-title.delivery-man-tracking',
        to: `${adminRoot}/map-views/delivery-man-tracking`,
        role: '/admin/delivery-man-tracking'
      },
      {
        icon: 'iconsminds-road-2',
        label: 'menu-title.business-area',
        to: `${adminRoot}/map-views/business-area`,
        role: '/admin/business-area'
      }
    ],
  },
  {
    icon: 'iconsminds-receipt-4',
    label: 'menu-title.earning',
    to: `${adminRoot}/earning`,
    subs: [
      {
        icon: 'simple-icon-layers',
        label: 'menu-title.earning',
        to: `${adminRoot}/earning/order`,
        subs: [
          {
            icon: 'iconsminds-basket-coins',
            label: 'menu-title.order-earning',
            to: `${adminRoot}/earning/order/order-earning`,
            role: '/admin/order-earning'
          },
          {
            icon: 'iconsminds-money-bag',
            label: 'menu-title.deliveryman-weekly',
            to: `${adminRoot}/earning/order/deliveryman-weekly`,
            role: '/admin/deliveryman-weekly'
          },
          {
            icon: 'iconsminds-shop-4',
            label: 'menu-title.store-earning',
            to: `${adminRoot}/earning/order/store-earning`,
            role: '/admin/store-earning'
          }
        ],
      },
      {
        icon: 'simple-icon-layers',
        label: 'menu-title.wallet',
        to: `${adminRoot}/earning/wallet`,
        subs: [
          {
            icon: 'simple-icon-wallet',
            label: 'menu-title.wallet-history',
            to: `${adminRoot}/earning/wallet/wallet-history`,
            role: '/admin/wallet-history'
          },
          {
            icon: 'iconsminds-wallet',
            label: 'menu-title.wallet-request',
            to: `${adminRoot}/earning/wallet/wallet-request`,
            role: '/admin/wallet-request'
          },
          {
            icon: 'simple-icon-credit-card',
            label: 'menu-title.transaction-history',
            to: `${adminRoot}/earning/wallet/transaction-history`,
            role: '/admin/transaction-history'
          },
          {
            icon: 'simple-icon-badge',
            label: 'menu-title.redeem-history',
            to: `${adminRoot}/earning/wallet/redeem-history`,
            role: '/admin/redeem-history'
          },
        ],
      },
    ],
  },
  {
    icon: 'iconsminds-pantone',
    label: 'menu-title.menu',
    to: `${adminRoot}/menu`,
    subs: [
      {
        icon: 'simple-icon-layers',
        label: 'menu-title.menu',
        to: `${adminRoot}/menu/levels1`,
        subs: [
          {
            icon: 'simple-icon-organization',
            label: 'menu-title.category',
            to: `${adminRoot}/menu/category`,
            role: '/admin/category'
          },
          {
            icon: 'simple-icon-film',
            label: 'menu-title.item',
            to: `${adminRoot}/menu/item-list`,
            role: '/admin/item-list'
          },
        ],
      },
      {
        icon: 'simple-icon-layers',
        label: 'menu-title.modifier',
        to: `${adminRoot}/menu/levels2`,
        subs: [
          {
            icon: 'simple-icon-book-open',
            label: 'menu-title.modifier-group',
            to: `${adminRoot}/menu/modifier-group`,
            role: '/admin/modifier-group'
          }
        ],
      }
    ],
  },
  {
    icon: 'simple-icon-people',
    label: 'menu-title.users',
    to: `${adminRoot}/users`,
    subs: [
      {
        icon: 'simple-icon-people',
        label: 'menu-title.user',
        to: `${adminRoot}/users/user`,
        role: '/admin/user'
      },
      {
        icon: 'iconsminds-handshake',
        label: 'menu-title.delivery-boy-list',
        to: `${adminRoot}/users/delivery-boy-list`,
        role: '/admin/delivery-boy-list'
      },
      {
        icon: 'iconsminds-shop-2',
        label: 'menu-title.store',
        to: `${adminRoot}/users/store`,
        role: '/admin/store'
      },
      {
        icon: 'iconsminds-administrator',
        label: 'menu-title.sub-admin',
        to: `${adminRoot}/users/sub-admin`,
        role: '/admin/sub-admin'
      }
      , {
        icon: 'simple-icon-chart',
        label: 'menu-title.activity-logs',
        to: `${adminRoot}/users/activity-logs`,
        role: '/admin/activity-logs'
      }
    ],
  },
  {
    icon: 'iconsminds-ship',
    label: 'menu-title.delivery-info',
    to: `${adminRoot}/delivery-info`,
    subs: [
      {
        icon: 'iconsminds-box-close',
        label: 'menu-title.delivery',
        to: `${adminRoot}/delivery-info/business`,
        role: '/admin/business'
      },
      {
        icon: 'simple-icon-globe',
        label: 'menu-title.country-city-info',
        to: `${adminRoot}/delivery-info/country-city-info`,
        role: '/admin/country-city-info'
      },
      {
        icon: 'iconsminds-pricing',
        label: 'menu-title.d-fees-info',
        to: `${adminRoot}/delivery-info/d-fees-info`,
        role: '/admin/d-fees-info'
      },
      {
        icon: 'iconsminds-billing',
        label: 'menu-title.admin-service',
        to: `${adminRoot}/delivery-info/admin-service`,
        role: '/admin/admin-service'
      },
      {
        icon: 'iconsminds-gears',
        label: 'menu-title.ecommerce-service',
        to: `${adminRoot}/delivery-info/ecommerce-service`,
        role: '/admin/ecommerce-service'
      }
      ,
      {
        icon: 'simple-icon-list',
        label: 'menu-title.courier-service',
        to: `${adminRoot}/delivery-info/courier-service`,
        role: '/admin/courier-service'
      }
    ]
  },
  {
    icon: 'iconsminds-three-arrow-fork',
    label: 'menu-title.settings',
    to: `${adminRoot}/settings`,
    subs: [
      {
        icon: 'simple-icon-layers',
        label: 'menu-title.basic-settings',
        to: `${adminRoot}/settings/basic-settings`,
        subs: [
          {
            icon: ' iconsminds-administrator',
            label: 'menu-title.admin',
            to: `${adminRoot}/settings/basic-settings/admin`,
            role: '/admin/settings/admin'
          },
          {
            icon: 'iconsminds-files',
            label: 'menu-title.document',
            to: `${adminRoot}/settings/basic-settings/document`,
            role: '/admin/document'
          },
          {
            icon: 'iconsminds-box-with-folders',
            label: 'menu-title.language',
            to: `${adminRoot}/settings/basic-settings/language`,
            role: '/admin/language'
          },
          {
            icon: 'iconsminds-box-with-folders',
            label: 'menu-title.banner',
            to: `${adminRoot}/settings/basic-settings/banner`,
            role: '/admin/banner'
          }

        ],
      },
      {
        icon: 'simple-icon-layers',
        label: 'menu-title.discount',
        to: `${adminRoot}/settings/discount`,
        subs: [
          {
            icon: 'iconsminds-information',
            label: 'menu-title.promo-code',
            to: `${adminRoot}/settings/discount/offer`,
            role: '/admin/offer'
          },
          {
            icon: 'simple-icon-picture',
            label: 'menu-title.advertise',
            to: `${adminRoot}/settings/discount/advertise`,
            role: '/admin/advertise'
          },
          {
            icon: 'iconsminds-gift-box',
            label: 'menu-title.referral-code',
            to: `${adminRoot}/settings/discount/referral`,
            role: '/admin/referral'
          },
        ],
      },
      {
        icon: 'simple-icon-layers',
        label: 'menu-title.template',
        to: `${adminRoot}/settings/template`,
        subs: [
          {
            icon: 'iconsminds-mail-settings',
            label: 'menu-title.email-settings',
            to: `${adminRoot}/settings/template/email-settings`,
            role: '/admin/email-settings'
          },
          {
            icon: 'iconsminds-speach-bubble-dialog',
            label: 'menu-title.sms-settings',
            to: `${adminRoot}/settings/template/sms-settings`,
            role: '/admin/sms-settings'
          },
          {
            icon: 'simple-icon-bell',
            label: 'menu-title.mass-notification',
            to: `${adminRoot}/settings/template/mass-notification`,
            role: '/admin/mass-notification'
          },
          {
            icon: 'iconsminds-letter-open',
            label: 'menu-title.privacy-terms',
            to: `${adminRoot}/settings/template/legal`,
            role: '/admin/legal'
          },
          {
            icon: 'iconsminds-close',
            label: 'menu-title.cancellation-reason',
            to: `${adminRoot}/settings/template/cancellation-reason`,
            role: '/admin/cancellation-reason'
          },
          {
            icon: 'simple-icon-chart',
            label: 'menu-title.logs',
            to: `${adminRoot}/settings/template/logs`,
            role: '/admin/logs'
          }
        ],
      }
    ],
  }, {
    icon: 'simple-icon-layers',
    label: 'menu-title.seo',
    to: `${adminRoot}/seo`,
    subs: [
      {
        icon: 'iconsminds-tag',
        label: 'menu-title.tags',
        to: `${adminRoot}/seo/tags`,
        role: '/admin/seo/tags'
      },
      {
        icon: 'iconsminds-link-2',
        label: 'menu-title.script',
        to: `${adminRoot}/seo/script`,
        role: '/admin/seo/script'
      }
    ],
  },
];

export default data;
