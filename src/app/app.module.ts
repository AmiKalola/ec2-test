import { BrowserModule } from '@angular/platform-browser';
import { APP_INITIALIZER, NgModule } from '@angular/core';
import { AppRoutingModule } from './app.routing';
import { AppComponent } from './app.component';
import { ViewsModule } from './views/views.module';
import { TranslateModule } from '@ngx-translate/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule , HTTP_INTERCEPTORS} from '@angular/common/http';
import { AngularFireModule } from '@angular/fire/compat';
import { environment } from 'src/environments/environment';
import { LayoutContainersModule } from './containers/layout/layout.containers.module';
import { Helper } from '../app/shared/helper';
import { ReqInterceptInterceptor } from 'src/app/interceptor/req-intercept.interceptor';
import { ResInterceptInterceptor } from 'src/app/interceptor/res-intercept.interceptor';
import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';
import { ToastrModule } from 'ngx-toastr';
import { CommonService } from './services/common.service';
import { LoaderModule } from './components/loader/loader.module';

const config: SocketIoConfig = { url: environment.socketUrl, options: {} };

function load(config: CommonService): () => Promise<boolean> {
  return async () => {
    await config._initApp();
    return true;
  };
}
@NgModule({
  imports: [
    BrowserModule,
    ViewsModule,
    AppRoutingModule,
    LayoutContainersModule,
    BrowserAnimationsModule,
    TranslateModule.forRoot(),
    HttpClientModule,
    LoaderModule,
    AngularFireModule.initializeApp(environment.firebase),
    SocketIoModule.forRoot(config),
    ToastrModule.forRoot({
      timeOut: 3000,
      progressBar: true,
      progressAnimation: 'increasing',
      positionClass: 'toast-bottom-right'
    }),
  ],
  declarations: [
    AppComponent
  ],
  providers: [Helper,
    {
			provide: APP_INITIALIZER,
			useFactory: load,
			deps: [CommonService],
			multi: true
		},
    {
    provide:HTTP_INTERCEPTORS,
    useClass:ReqInterceptInterceptor,
    multi:true
  },
  {
    provide:HTTP_INTERCEPTORS,
    useClass:ResInterceptInterceptor,
    multi:true
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
