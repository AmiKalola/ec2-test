export class User {
	user_id: string
    first_name: string
    last_name: string
    phone: string
    country_phone_code: string
    email: string
    image_url: string
    password: string
    is_use_wallet: boolean
    wallet: number
    wallet_currency_code: string
    created_at: any
    app_version: any
    device_type: any
    comment: string
    store_rate: number
    referral_code: string
    provider_rate: number
    is_phone_verified: boolean
    is_email_verified: boolean
    unique_id:number
}