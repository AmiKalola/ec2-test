export interface StatusDateime {
    "status":number,
    "date":Date
}

export interface Modifier {
    "name":string;
    "value":Array<string>;
}

export interface Item {
    "item_name":string,
    "unique_id":number,
    "modifier":Array<Modifier>,
    "item_price":number,
    "note_for_item":string
}

export class OrderDetail {
    "id":string;
    "name":string;
    "address":string;
    "price":number;
    "tax":number;
    "delivery_price":number;
    "discount":number;
    "total_item_price":number;
    "total":number;
    "currency_sign":string;
    "payment_method":string;
    "phone":string;
    "distance":string;
    "time":string;
    "orderedAt":string;
    "deliveryAt":string;
    "country_code":string;
    "delivery_note":string;
    "date_time":Array<StatusDateime>;
    "items":Array<Item>
}