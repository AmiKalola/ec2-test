export class Provider {
	provider_id: string
    first_name: string
    last_name: string
    phone: string
    country_phone_code: string
    email: string
    image_url: string
    password: string
    is_use_wallet: boolean
    is_active_for_job: boolean
    wallet: number
    wallet_currency_code: string
    created_at: any
    app_version: any
    device_type: any
    comment: string
    store_rate: number
    user_rate: number
    referral_code: string
    provider_rate: number
    is_phone_verified: boolean
    is_email_verified: boolean
    server_token: string
    unique_id:number
    is_approved:boolean
    provider_type_id:string;
    service_delivery_type:number
    delivery_types:any
    service_id:any
}