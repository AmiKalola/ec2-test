export interface EarningResponse {
    unique_id:string,
    name:string,
    total_cart_price:string,
    item_tax:number,
    total_delivery_price:number,
    total:number,
    promo_payment:number,
    user_pay_payment:number,
    total_item_count:number,
    currency_sign:string,
    payment_method:string,
    completed_at:Date,
}
