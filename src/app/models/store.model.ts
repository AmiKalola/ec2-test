export class Store {
	store_id: string
    name: any
    phone: string
    country_phone_code: string
    email: string
    image_url: string
    password: string
    is_use_wallet: boolean
    wallet: number
    wallet_currency_code: string
    created_at: any
    app_version: any
    device_type: any
    comment: string
    provider_rate: number
    user_rate: number
    referral_code: string
    is_phone_verified: boolean
    is_email_verified: boolean
    website_url: string
    location: any
    address: string
    is_store_can_complete_order: boolean
    is_store_can_add_provider: boolean
    slogan: string
    admin_profit_mode_on_store: number
    admin_profit_value_on_store: number
    unique_id:number
}