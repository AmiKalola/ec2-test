import { Component} from '@angular/core';
import { Helper } from '../../shared/helper'

@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.css']
})
export class LoaderComponent   {
    constructor(public _helper:Helper) { }

}
