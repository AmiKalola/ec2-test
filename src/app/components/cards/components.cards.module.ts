import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RoundProgressModule } from 'angular-svg-round-progressbar';
import { GradientWithRadialProgressCardComponent } from './gradient-with-radial-progress-card/gradient-with-radial-progress-card.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { PipeModule } from 'src/app/pipes/pipe.module';

@NgModule({
  declarations: [
    GradientWithRadialProgressCardComponent,
  ],
  imports: [
    CommonModule,
    RoundProgressModule,
    SharedModule,
    PipeModule
  ],
  providers: [],
  exports: [
    GradientWithRadialProgressCardComponent,
  ]
})

export class ComponentsCardsModule { }
