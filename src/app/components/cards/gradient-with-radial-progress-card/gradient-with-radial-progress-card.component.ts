import { Component, Input, SimpleChanges } from '@angular/core';
import { Helper } from 'src/app/shared/helper';

@Component({
  selector: 'app-gradient-with-radial-progress-card',
  templateUrl: './gradient-with-radial-progress-card.component.html'
})
export class GradientWithRadialProgressCardComponent {
  @Input() icon = 'icon';
  @Input() title = 'title';
  @Input() detail = 'detail';
  @Input() progressText = '25%';
  @Input() percent = 25;
  @Input() data;

  admin_earn_percentage: number = 0;
  admin_earn_per: number = 0;
  earning_percentage: number = 0
  admin_earnings: number = 0;
  total_percentage: number = 0;
  total_payments: number = 0;
  order_payment: number = 0;
  order_payment_percentage: number = 0;
  store_earning: number = 0
  store_earn_per: number = 0;
  store_payment_pre_earning: number = 0;
  delivery_payment: number = 0;
  delivery_percentage: number = 0;
  deliveryman_earn_percentage: number = 0;
  provider_earning: number = 0;
  provider_payment_pre_earning: number = 0
  provider_earn_per: number = 0;
  currency_sign:any = ''

  constructor(public _helper:Helper) {
  }

  ngOnChanges(changes: SimpleChanges){
    if(this.data){
      this.currency_sign = this._helper.selected_country_id.currency_sign
      const res_data = this.data
      this.total_payments = res_data.total_payments;
      this.admin_earnings = res_data.admin_earning;
      this.admin_earn_per = res_data.admin_earn_per;
      this.earning_percentage = 100;
      this.order_payment = res_data.order_payment;
      this.store_earning = res_data.store_earning;
      this.delivery_payment = res_data.delivery_payment;
      this.provider_earning = res_data.provider_earning;
      if(res_data.total_payments == 0){
        this.earning_percentage = 0;
      }
      if(res_data.store_payment_pre_earning > 100){
        this.store_payment_pre_earning = 100
      } else {
        this.store_payment_pre_earning = res_data.store_payment_pre_earning;
      }
      if(res_data.store_earn_per > 100){
        this.store_earn_per = 100
      } else {
        this.store_earn_per = res_data.store_earn_per;
      }
      if(res_data.provider_payment_pre_earning > 100){
        this.provider_payment_pre_earning = 100
      } else {
        this.provider_payment_pre_earning = res_data.provider_payment_pre_earning;
      }
      if(res_data.provider_earn_per > 100){
        this.provider_earn_per = 100
      } else {
        this.provider_earn_per = res_data.provider_earn_per;
      }
    }

  }
}
