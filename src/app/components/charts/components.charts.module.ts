import { NgModule } from '@angular/core';
import { LineChartComponent } from './line-chart/line-chart.component';
import { BarChartComponent } from './bar-chart/bar-chart.component';
import { PieChartComponent } from './pie-chart/pie-chart.component';
import { PaymentBarChartComponent } from './payment-bar-chart/payment-bar-chart.component'
import { CommonModule } from '@angular/common';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';

@NgModule({
  declarations: [
    LineChartComponent,
    BarChartComponent,
    PieChartComponent,
    PaymentBarChartComponent
  ],
  imports: [
    CommonModule,
    BsDropdownModule.forRoot()
  ],
  providers: [],
  exports: [
    LineChartComponent,
    BarChartComponent,
    PieChartComponent,
    PaymentBarChartComponent
  ]
})

export class ComponentsChartModule { }
