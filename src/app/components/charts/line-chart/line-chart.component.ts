import { Component, Input, ViewChild, ElementRef, OnDestroy, OnInit } from '@angular/core';
import { Chart } from 'chart.js';
import { CommonService } from 'src/app/services/common.service';
import { Helper } from 'src/app/shared/helper';
import { Colors } from '../../../constants/colors.service'

@Component({
  selector: 'app-line-chart',
  templateUrl: './line-chart.component.html'
})
export class LineChartComponent implements OnInit, OnDestroy {

  @Input() shadow = false;
  @Input() options;
  @Input() data;
  @Input() class = 'chart-container';
  @ViewChild('chart', { static: true }) chartRef: ElementRef;
  @Input() line_chart_item_bsRangeValue;
  @Input() line_country_id;
  concatinatedArrray: any = [];
  is_negative: boolean = false
  start_date;
  end_date;
  currency_sign :any =''

  lineChartData =     {
      datasets: [{
        borderColor: Colors.getColors().themeColor1,
        fill: false,
        data: [],
        borderWidth: 2,
        pointBackgroundColor: "white",
        pointBorderColor: Colors.getColors().themeColor1,
        pointBorderWidth: 2,
        pointHoverBackgroundColor: Colors.getColors().themeColor1,
        pointHoverBorderColor: "white",
        pointHoverRadius: 6,
        pointRadius: 4,
        label: 'Admin Earning'
      },
      {
        borderColor: Colors.getColors().themeColor2,
        fill: false,
        data: [],
        borderWidth: 2,
        pointBackgroundColor: "white",
        pointBorderColor: Colors.getColors().themeColor2,
        pointBorderWidth: 2,
        pointHoverBackgroundColor: Colors.getColors().themeColor2,
        pointHoverBorderColor: "white",
        pointHoverRadius: 6,
        pointRadius: 4,
        label: 'Admin Paid'
      },
      {
        borderColor: Colors.getColors().themeColor3,
        fill: false,
        data: [],
        borderWidth: 2,
        pointBackgroundColor: "white",
        pointBorderColor: Colors.getColors().themeColor3,
        pointBorderWidth: 2,
        pointHoverBackgroundColor: Colors.getColors().themeColor3,
        pointHoverBorderColor: "white",
        pointHoverRadius: 6,
        pointRadius: 4,
        label: 'Paid Deliveryman'
      },
      {
        borderColor: Colors.getColors().themeColor4,
        fill: false,
        data: [],
        borderWidth: 2,
        pointBackgroundColor: "white",
        pointBorderColor: Colors.getColors().themeColor4,
        pointBorderWidth: 2,
        pointHoverBackgroundColor: Colors.getColors().themeColor4,
        pointHoverBorderColor: "white",
        pointHoverRadius: 6,
        pointRadius: 4,
        label: 'Paid Store'
      },
      {
        borderColor: Colors.getColors().themeColor5,
        fill: false,
        data: [],
        borderWidth: 2,
        pointBackgroundColor: "white",
        pointBorderColor: Colors.getColors().themeColor5,
        pointBorderWidth: 2,
        pointHoverBackgroundColor: Colors.getColors().themeColor5,
        pointHoverBorderColor: "white",
        pointHoverRadius: 6,
        pointRadius: 4,
        label: 'Total'
      }]
    }

  labels = []

  chart: Chart;
  chartData: any = {
    datasets: [{
      label: "Test",
      data: [ 52, 58, 65, 78, 98, 138, 160],
      borderColor: '#000000',
      fill: false,
      borderWidth: 2,
      pointBackgroundColor: "white",
      pointBorderColor: "#000000",
      pointBorderWidth: 2,
      pointHoverBackgroundColor: "#000000",
      pointHoverBorderColor: "white",
      pointHoverRadius: 6,
      pointRadius: 4,
    }],
      labels: ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"]
    }

  public constructor(private _commonService: CommonService, private _helper: Helper) {
    this._helper.countryId.subscribe((data) => {
      this.parentCalled()
    })

  }
 
  parentCalled(){
    if(this.line_chart_item_bsRangeValue && this.line_chart_item_bsRangeValue.length > 0){
      if(this.chart){
        this.start_date = this.line_chart_item_bsRangeValue[1];
        this.end_date = this.line_chart_item_bsRangeValue[0];
        this.chart.destroy();
        this.chart = null;
        for (const element of this.lineChartData.datasets) {
          element.data = [];
        }
        this.concatinatedArrray = [];
        this.labels = [];
      }
      this.ngOnInit();
    }
  }

  ngOnInit() {

    this.start_date = this.line_chart_item_bsRangeValue[0];
    let date = new Date(this.line_chart_item_bsRangeValue[1]);
    date.setDate(date.getDate() + 1);
    this.end_date = date;
    this.currency_sign = this._helper.selected_country_id.currency_sign
    let json:any = {start_month :this.start_date,end_month:this.end_date, country_id: this._helper.selected_country_id.country_id}
    if(this.start_date && this.end_date){
      this._commonService.last_six_months_earnings(json).then(res_data => {
        if(res_data.success){
          let response_data = res_data.array

          if (this.shadow) {
            Chart.controllers.lineWithShadow = Chart.controllers.line;
            Chart.controllers.lineWithShadow = Chart.controllers.line.extend({
              draw(ease) {
                Chart.controllers.line.prototype.draw.call(this, ease);
                const chartCtx = this.chart.ctx;
                chartCtx.save();
                chartCtx.shadowColor = 'rgba(0,0,0,0.15)';
                chartCtx.shadowBlur = 10;
                chartCtx.shadowOffsetX = 0;
                chartCtx.shadowOffsetY = 10;
                chartCtx.responsive = true;
                chartCtx.stroke();
                Chart.controllers.line.prototype.draw.apply(this, arguments);
                chartCtx.restore();
              }
            });
          }

          response_data.forEach(data => {
            this.lineChartData.datasets[0].data.push(data.admin_earning.toFixed(this._helper.to_fixed_number))
            this.lineChartData.datasets[1].data.push(data.admin_paid.toFixed(this._helper.to_fixed_number))
            this.lineChartData.datasets[2].data.push(data.paid_deliveryman.toFixed(this._helper.to_fixed_number))
            this.lineChartData.datasets[3].data.push(data.paid_store.toFixed(this._helper.to_fixed_number))
            this.lineChartData.datasets[4].data.push(data.total.toFixed(this._helper.to_fixed_number))
            this.labels.push(data._id)
          });
          this.chartData = this.lineChartData
          this.chartData.labels = this.labels
          this.concatinatedArrray = this.lineChartData.datasets[0].data
          this.concatinatedArrray = this.concatinatedArrray.concat(this.lineChartData.datasets[1].data)
          this.concatinatedArrray = this.concatinatedArrray.concat(this.lineChartData.datasets[2].data)
          this.concatinatedArrray = this.concatinatedArrray.concat(this.lineChartData.datasets[3].data)
          this.concatinatedArrray = this.concatinatedArrray.concat(this.lineChartData.datasets[4].data)
          let max_value = Math.max(...this.concatinatedArrray)
          let min_value = Math.min(...this.concatinatedArrray)

          let slot = 10;
          let near_max = this._helper.findnearest(max_value);
          let near_min = this._helper.findnearest(min_value);
          let slot_range = 0;
          if(Number(Math.ceil(min_value)) < 0){
            slot_range = this._helper.findnearest((near_max + near_min) / slot);
          }else{
            slot_range = this._helper.findnearest((near_max - near_min) / slot);
          }

          const chartRefEl = this.chartRef.nativeElement;
          const ctx = chartRefEl.getContext('2d');
          this.chart = new Chart(ctx, {
            type: this.shadow ? 'lineWithShadow' : 'line',
            data: this.chartData,
            options: this.options
          });
          this.chart.options.scales.yAxes[0].ticks.max = near_max
          this.chart.options.scales.yAxes[0].ticks.min = -near_min
          this.chart.options.scales.yAxes[0].ticks.stepSize = slot_range
          this.chart.options.scales.yAxes[0].ticks.callback = (value, index, values) => {
            return this.currency_sign + ' ' + value.toFixed(this._helper.to_fixed_number);
          };
          this.chart.options.tooltips.callbacks.label = (tooltipItem, data) => {
            let label = data.datasets[tooltipItem.datasetIndex].label || '';
            if (label) {
              label += ': ';
            }
            const value = tooltipItem.yLabel;
            label += this.currency_sign + ' ' + value.toFixed(this._helper.to_fixed_number); // Add currency symbol to the tooltip
            return label;
          };
          this.chart.update()
        }
      })
    }
  }


  findtest(value){
    value = Math.abs(Math.ceil(value));
    let length = value.toString().length;

    if(length === 1){
      return length;
    }else{
      let test1 = "1";
      for (let index = 0; index < length -1 ; index++) {
        test1 = test1 + "0";
      }
      let test2 = value % Number(test1);
      let test3 = value - test2;
      let test4:any = test2.toString().length === length - 1 ? Number(test2.toString()[0]) + 1 : 1;
      for (let index = 0; index < length - 2 ; index++) {
        test4 = test4 + "0";
      }
      let final = Number(test3) + Number(test4);
      return final
    }

  }


  ngOnDestroy(): void {
    if (this.chart) {
      this.chart.destroy();
    }
  }
}
