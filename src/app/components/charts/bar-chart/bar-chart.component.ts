import { TranslateService } from '@ngx-translate/core';
import { Component, Input, ViewChild, ElementRef,  OnDestroy, OnInit } from '@angular/core';
import { Chart } from 'chart.js';
import { CommonService } from 'src/app/services/common.service';
import { Helper } from 'src/app/shared/helper';
import { Colors } from '../../../constants/colors.service';

@Component({
  selector: 'app-bar-chart',
  templateUrl: './bar-chart.component.html'
})
export class BarChartComponent implements  OnDestroy, OnInit {

  @Input() shadow = false;
  @Input() options;
  @Input() data;
  @Input() class = 'chart-container';
  @Input() order_chart_item_bsRangeValue;
  @ViewChild('chart', { static: true }) chartRef: ElementRef;

  chart: Chart;
  chartData: any;
  max_number: number;
  min_number: number;
  concatinedArray: any = [];
  start_date;
  end_date;

  barChartData : any;
  labels = []

  public constructor(private _commonService: CommonService, private _helper: Helper, private translate: TranslateService) { }

  parentCalled(){
    if(this.order_chart_item_bsRangeValue && this.order_chart_item_bsRangeValue.length > 0){
      if(this.chart){
        this.start_date = this.order_chart_item_bsRangeValue[1];
        this.end_date = this.order_chart_item_bsRangeValue[0];
        this.chart.destroy();
        this.chart = null;
        for (const element of this.barChartData.datasets) {
          element.data = [];
        }
        this.concatinedArray = [];
        this.labels = [];
      }
      this.ngOnInit();
    }
  }

  ngOnInit() {
    this.start_date = this.order_chart_item_bsRangeValue[0];
    let json:any = {start_month :this.start_date, country_id: this._helper.selected_country_id.country_id}

    if(this.start_date){
      this._commonService.last_fifteen_day_order_detail(json).then(res_data => {
        if (res_data.success) {
          this.barChartData = {
            datasets: [{
              borderColor: Colors.getColors().themeColor1,
              fill: false,
              backgroundColor: Colors.getColors().themeColor1,
              data: [],
              borderWidth: 2,
              pointBackgroundColor: "white",
              pointBorderColor: Colors.getColors().themeColor1,
              pointBorderWidth: 2,
              pointHoverBackgroundColor: Colors.getColors().themeColor1,
              pointHoverBorderColor: "white",
              pointHoverRadius: 6,
              pointRadius: 4,
              label: this.translate.instant('label-title.cancelled-by-admin')
            },
            {
              borderColor: Colors.getColors().themeColor2,
              fill: false,
              backgroundColor: Colors.getColors().themeColor2,
              data: [],
              borderWidth: 2,
              pointBackgroundColor: "white",
              pointBorderColor: Colors.getColors().themeColor2,
              pointBorderWidth: 2,
              pointHoverBackgroundColor: Colors.getColors().themeColor2,
              pointHoverBorderColor: "white",
              pointHoverRadius: 6,
              pointRadius: 4,
              label: this.translate.instant('label-title.cancelled-by-user')
            },
            {
              borderColor: Colors.getColors().themeColor3,
              fill: false,
              backgroundColor: Colors.getColors().themeColor3,
              data: [],
              borderWidth: 2,
              pointBackgroundColor: "white",
              pointBorderColor: Colors.getColors().themeColor3,
              pointBorderWidth: 2,
              pointHoverBackgroundColor: Colors.getColors().themeColor3,
              pointHoverBorderColor: "white",
              pointHoverRadius: 6,
              pointRadius: 4,
              label: this.translate.instant('label-title.completed-order')
            },
            {
              borderColor: Colors.getColors().themeColor4,
              fill: false,
              backgroundColor: Colors.getColors().themeColor4,
              data: [],
              borderWidth: 2,
              pointBackgroundColor: "white",
              pointBorderColor: Colors.getColors().themeColor4,
              pointBorderWidth: 2,
              pointHoverBackgroundColor: Colors.getColors().themeColor4,
              pointHoverBorderColor: "white",
              pointHoverRadius: 6,
              pointRadius: 4,
              label: this.translate.instant('label-title.rejected-by-store')
            },
            {
              borderColor: Colors.getColors().themeColor5,
              fill: false,
              backgroundColor: Colors.getColors().themeColor5,
              data: [],
              borderWidth: 2,
              pointBackgroundColor: "white",
              pointBorderColor: Colors.getColors().themeColor5,
              pointBorderWidth: 2,
              pointHoverBackgroundColor: Colors.getColors().themeColor5,
              pointHoverBorderColor: "white",
              pointHoverRadius: 6,
              pointRadius: 4,
              label: this.translate.instant('label-title.total-deliveries')
            },
            {
              borderColor: Colors.getColors().themeColor6,
              fill: false,
              backgroundColor: Colors.getColors().themeColor6,
              data: [],
              borderWidth: 2,
              pointBackgroundColor: "white",
              pointBorderColor: Colors.getColors().themeColor6,
              pointBorderWidth: 2,
              pointHoverBackgroundColor: Colors.getColors().themeColor6,
              pointHoverBorderColor: "white",
              pointHoverRadius: 6,
              pointRadius: 4,
              label: this.translate.instant('label-title.total-orders')
          }]
                    }
          if (this.shadow) {
            Chart.defaults.global.datasets.barWithShadow = Chart.defaults.global.datasets.bar;
            Chart.defaults.barWithShadow = Chart.defaults.bar;
            Chart.controllers.barWithShadow = Chart.controllers.bar.extend({
              // tslint:disable-next-line:typedef
              draw(ease) {
                Chart.controllers.bar.prototype.draw.call(this, ease);
                const chartCtx = this.chart.ctx;
                chartCtx.save();
                chartCtx.shadowColor = 'rgba(0,0,0,0.2)';
                chartCtx.shadowBlur = 7;
                chartCtx.shadowOffsetX = 5;
                chartCtx.shadowOffsetY = 7;
                chartCtx.responsive = true;
                Chart.controllers.bar.prototype.draw.apply(this, arguments);
                chartCtx.restore();
              }
            });
          }

          res_data.array.forEach(data => {
            this.barChartData.datasets[0].data.push(data.cancelled_by_admin)
            this.barChartData.datasets[1].data.push(data.cancelled_by_user)
            this.barChartData.datasets[2].data.push(data.completed_order)
            this.barChartData.datasets[3].data.push(data.rejected_by_store)
            this.barChartData.datasets[4].data.push(data.total_deliveries)
            this.barChartData.datasets[5].data.push(data.total_orders)
            this.labels.push(data._id)
          });
          this.concatinedArray = this.barChartData.datasets[0].data
          this.concatinedArray = this.concatinedArray.concat(this.barChartData.datasets[1].data)
          this.concatinedArray = this.concatinedArray.concat(this.barChartData.datasets[2].data)
          this.concatinedArray = this.concatinedArray.concat(this.barChartData.datasets[3].data)
          this.concatinedArray = this.concatinedArray.concat(this.barChartData.datasets[4].data)
          this.concatinedArray = this.concatinedArray.concat(this.barChartData.datasets[5].data)
          let max_value = Math.max(...this.concatinedArray)
          let slot_value = Math.ceil(max_value / 10)
          max_value = Math.ceil(max_value) + slot_value

          const chartRefEl = this.chartRef.nativeElement;
          const ctx = chartRefEl.getContext('2d');
          this.chartData = this.barChartData
          this.chartData.labels = this.labels
          this.chart = new Chart(ctx, {
            type: this.shadow ? 'barWithShadow' : 'bar',
            data: this.chartData,
            options: this.options
          });
          this.chart.options.scales.yAxes[0].ticks.max = max_value
          this.chart.options.scales.yAxes[0].ticks.min = 0
          this.chart.options.scales.yAxes[0].ticks.stepSize = slot_value
          this.chart.update()
        }
      })
    }
  }

  ngOnDestroy(): void {
    if (this.chart) {
      this.chart.destroy();
    }
  }
}
