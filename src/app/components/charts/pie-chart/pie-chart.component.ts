import { TranslateService } from '@ngx-translate/core';
import { Component, Input, ViewChild, ElementRef, OnDestroy, OnInit } from '@angular/core';
import { Chart } from 'chart.js';
import { CommonService } from 'src/app/services/common.service';
import { Colors } from '../../../constants/colors.service'
import { Helper } from 'src/app/shared/helper';

@Component({
  selector: 'app-pie-chart',
  templateUrl: './pie-chart.component.html'
})
export class PieChartComponent implements OnDestroy, OnInit {

  @Input() shadow = false;
  @Input() options;
  @Input() data;
  @Input() class = 'chart-container';
  @Input() pie_chart_item_bsRangeValue;

  @ViewChild('chart', { static: true }) chartRef: ElementRef;

  chart: Chart;
  total_wallet_payment: number;
  total_promo_payment: number;
  total_other_payment: number;
  total_cash_payment: number;
  pieChartData : any;
  concatinedArray: any = [];
  start_date;
  end_date;
  labels = []
  currency_sign :any =''

  public constructor(private _commonService: CommonService, private translate: TranslateService,private _helper:Helper) { }
  parentCalled(){
    if(this.pie_chart_item_bsRangeValue && this.pie_chart_item_bsRangeValue.length > 0){
      if(this.chart){
        this.start_date = this.pie_chart_item_bsRangeValue[1];
        this.end_date = this.pie_chart_item_bsRangeValue[0];
        this.chart.destroy();
        this.chart = null;
        for (const element of this.pieChartData.datasets) {
          element.data = [];
        }
        this.concatinedArray = [];
        this.labels = [];
      }
      this.ngOnInit();
    }
  }

  ngOnInit(){
    this.start_date = this.pie_chart_item_bsRangeValue[0];
    let date = new Date(this.pie_chart_item_bsRangeValue[1]);
    date.setDate(date.getDate() + 1);
    this.end_date = date;
    this.currency_sign = this._helper.selected_country_id.currency_sign
    let json:any = {start_month :this.start_date,end_month:this.end_date, country_id: this._helper.selected_country_id.country_id}

    if(this.start_date && this.end_date){
      this._commonService.last_six_month_payment_detail(json).then(res_data => {
        if (res_data.success) {
          let labels_array = [this.translate.instant('label-title.wallet-payment'), this.translate.instant('label-title.promo-payment'), this.translate.instant('label-title.other-payment'), this.translate.instant('label-title.cash-payment')]

          this.pieChartData = {
            labels: labels_array,
            datasets: [{
              backgroundColor: [Colors.getColors().themeColor1_10, Colors.getColors().themeColor2_10, Colors.getColors().themeColor3_10, Colors.getColors().themeColor4_10],
              borderColor: [Colors.getColors().themeColor1, Colors.getColors().themeColor2, Colors.getColors().themeColor3, Colors.getColors().themeColor4],
              borderWidth: 2,
              data: [],
              label: "",
            }]
          }
          this.total_cash_payment = res_data.order_detail.total_cash_payment.toFixed(this._helper.to_fixed_number);
          this.total_other_payment = res_data.order_detail.total_other_payment.toFixed(this._helper.to_fixed_number);
          this.total_promo_payment = res_data.order_detail.total_promo_payment.toFixed(this._helper.to_fixed_number);
          this.total_wallet_payment = res_data.order_detail.total_wallet_payment.toFixed(this._helper.to_fixed_number);

          if (this.shadow) {
            Chart.defaults.pieWithShadow = Chart.defaults.pie;
            Chart.controllers.pieWithShadow = Chart.controllers.pie.extend({
              draw(ease) {
                Chart.controllers.pie.prototype.draw.call(this, ease);
                const chartCtx = this.chart.chart.ctx;
                chartCtx.save();
                chartCtx.shadowColor = 'rgba(0,0,0,0.15)';
                chartCtx.shadowBlur = 10;
                chartCtx.shadowOffsetX = 0;
                chartCtx.shadowOffsetY = 10;
                chartCtx.responsive = true;
                Chart.controllers.pie.prototype.draw.apply(this, arguments);
                chartCtx.restore();
              }
            });
          }
          const originalData = [
            this.total_wallet_payment,
            this.total_promo_payment,
            this.total_other_payment,
            this.total_cash_payment
          ];

          this.pieChartData.datasets[0].data = originalData
          const chartRefEl = this.chartRef.nativeElement;
          const ctx = chartRefEl.getContext('2d');
          this.chart = new Chart(ctx, {
            type: this.shadow ? 'pieWithShadow' : 'pie',
            data: this.pieChartData,
            options: this.options
          });

          const currencyFormatter = (value, index) => {
            return labels_array[index] +': '+ this.currency_sign + ' ' + value;
          };

          this.chart.options.tooltips.callbacks.label = (tooltipItem, data) => {
            const index = tooltipItem.index;
            const value = originalData[index];
            return currencyFormatter(value, index);
          };
          this.chart.update()
        }
      })
    }
  }

  ngOnDestroy(): void {
    if (this.chart) {
      this.chart.destroy();
    }
  }
}
