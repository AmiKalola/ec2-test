import { Component, Input, ViewChild, ElementRef,  OnDestroy, OnInit } from '@angular/core';
import { Chart } from 'chart.js';
import { CommonService } from '../../../services/common.service';
import { Colors } from '../../../constants/colors.service'
import { TranslateService } from '@ngx-translate/core';
import { Helper } from 'src/app/shared/helper';

@Component({
  selector: 'app-payment-bar-chart',
  templateUrl: './payment-bar-chart.component.html'
})
export class PaymentBarChartComponent implements  OnDestroy, OnInit {

  @Input() shadow = false;
  @Input() options;
  @Input() data;
  @Input() class = 'chart-container';
  @Input() bar_chart_item_bsRangeValue;
  @ViewChild('chart', { static: true }) chartRef: ElementRef;

  chart: Chart;
  chartData: any;
  concatinedArray: any = [];
  start_date;
  end_date;
  currency_sign :any =''
  barChartData :any;
  labels = []

  public constructor(private _commonService: CommonService, private _trans: TranslateService,private _helper:Helper) {
  }
  parentCalled(){
    if(this.bar_chart_item_bsRangeValue && this.bar_chart_item_bsRangeValue.length > 0){
      if(this.chart){
        this.start_date = this.bar_chart_item_bsRangeValue[1];
        this.end_date = this.bar_chart_item_bsRangeValue[0];
        this.chart.destroy();
        this.chart = null;
        for (const element of this.barChartData.datasets) {
          element.data = [];
        }
        this.concatinedArray = [];
        this.labels = [];
      }
      this.ngOnInit();
    }
  }
  ngOnInit() {
    this.start_date = this.bar_chart_item_bsRangeValue[0];
    let date = new Date(this.bar_chart_item_bsRangeValue[1]);
    date.setDate(date.getDate() + 1);
    this.end_date = date;
    this.currency_sign = this._helper.selected_country_id.currency_sign
    let json:any = {start_month :this.start_date,end_month:this.end_date, country_id: this._helper.selected_country_id.country_id}
    if(this.start_date && this.end_date){
      this.monthlyPayment(json)
    }
  }

  setData(){
    this.barChartData = {
      datasets: [{
        borderColor: Colors.getColors().themeColor1,
        fill: false,
        backgroundColor: Colors.getColors().themeColor1,
        data: [],
        borderWidth: 2,
        pointBackgroundColor: "white",
        pointBorderColor: Colors.getColors().themeColor1,
        pointBorderWidth: 2,
        pointHoverBackgroundColor: Colors.getColors().themeColor1,
        pointHoverBorderColor: "white",
        pointHoverRadius: 6,
        pointRadius: 4,
        label: this._trans.instant('label-title.cash')
      },
      {
        borderColor: Colors.getColors().themeColor2,
        fill: false,
        backgroundColor: Colors.getColors().themeColor2,
        data: [],
        borderWidth: 2,
        pointBackgroundColor: "white",
        pointBorderColor: Colors.getColors().themeColor2,
        pointBorderWidth: 2,
        pointHoverBackgroundColor: Colors.getColors().themeColor2,
        pointHoverBorderColor: "white",
        pointHoverRadius: 6,
        pointRadius: 4,
        label: this._trans.instant('label-title.promo')
      },
      {
        borderColor: Colors.getColors().themeColor3,
        fill: false,
        backgroundColor: Colors.getColors().themeColor3,
        data: [],
        borderWidth: 2,
        pointBackgroundColor: "white",
        pointBorderColor: Colors.getColors().themeColor3,
        pointBorderWidth: 2,
        pointHoverBackgroundColor: Colors.getColors().themeColor3,
        pointHoverBorderColor: "white",
        pointHoverRadius: 6,
        pointRadius: 4,
        label: this._trans.instant('label-title.wallet')
      },
      {
        borderColor: Colors.getColors().themeColor4,
        fill: false,
        backgroundColor: Colors.getColors().themeColor4,
        data: [],
        borderWidth: 2,
        pointBackgroundColor: "white",
        pointBorderColor: Colors.getColors().themeColor4,
        pointBorderWidth: 2,
        pointHoverBackgroundColor: Colors.getColors().themeColor4,
        pointHoverBorderColor: "white",
        pointHoverRadius: 6,
        pointRadius: 4,
        label: this._trans.instant('label-title.other')
      }]
    }
  }

  monthlyPayment(json){
    this._commonService.monthly_payment_detail(json).then(res_data => {
      if (res_data.success) {
        this.setData()
        if (this.shadow) {
          Chart.defaults.global.datasets.barWithShadow = Chart.defaults.global.datasets.bar;
          Chart.defaults.barWithShadow = Chart.defaults.bar;
          Chart.controllers.barWithShadow = Chart.controllers.bar.extend({
            draw(ease) {
              Chart.controllers.bar.prototype.draw.call(this, ease);
              const chartCtx = this.chart.ctx;
              chartCtx.save();
              chartCtx.shadowColor = 'rgba(0,0,0,0.2)';
              chartCtx.shadowBlur = 7;
              chartCtx.shadowOffsetX = 5;
              chartCtx.shadowOffsetY = 7;
              chartCtx.responsive = true;
              Chart.controllers.bar.prototype.draw.apply(this, arguments);
              chartCtx.restore();
            }
          });
        }


        res_data.array.forEach(data => {
          this.barChartData.datasets[0].data.push(data.cash_payment.toFixed(this._helper.to_fixed_number))
          this.barChartData.datasets[1].data.push(data.promo_payment.toFixed(this._helper.to_fixed_number))
          this.barChartData.datasets[2].data.push(data.wallet_payment.toFixed(this._helper.to_fixed_number))
          this.barChartData.datasets[3].data.push(data.other_payment.toFixed(this._helper.to_fixed_number))
          this.labels.push(data._id)
        });

        this.concatinedArray = this.concatinedArray.concat(this.barChartData.datasets[0].data)
        this.concatinedArray = this.concatinedArray.concat(this.barChartData.datasets[1].data)
        this.concatinedArray = this.concatinedArray.concat(this.barChartData.datasets[2].data)
        this.concatinedArray = this.concatinedArray.concat(this.barChartData.datasets[3].data)
        let max_value = Math.max(...this.concatinedArray)
        let slot_value = Math.ceil(max_value / 10)
        max_value = Math.ceil(max_value) + slot_value


        const chartRefEl = this.chartRef.nativeElement;
        const ctx = chartRefEl.getContext('2d');
        this.chartData = this.barChartData
        this.chartData.labels = this.labels
        this.chart = new Chart(ctx, {
          type: this.shadow ? 'barWithShadow' : 'bar',
          data: this.chartData,
          options: this.options
        });

        this.chart.options.scales.yAxes[0].ticks.max = max_value
        this.chart.options.scales.yAxes[0].ticks.min = 0
        this.chart.options.scales.yAxes[0].ticks.stepSize = slot_value
        this.chart.options.scales.yAxes[0].ticks.callback = (value, index, values) => {
          return this.currency_sign + ' ' + value.toFixed(this._helper.to_fixed_number);
        };
        this.chart.options.tooltips.callbacks.label = (tooltipItem, data) => {
          let label = data.datasets[tooltipItem.datasetIndex].label || '';
          if (label) {
            label += ': ';
          }
          const value = tooltipItem.yLabel;
          label += this.currency_sign + ' ' + value.toFixed(this._helper.to_fixed_number); // Add currency symbol to the tooltip
          return label;
        };

        this.chart.update()
      }
    })
  }

  ngOnDestroy(): void {
    if (this.chart) {
      this.chart.destroy();
    }
  }
}
