import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LangPipe } from './lang.pipe'
import { RoundPipe } from './round.pipe'
import { SearchPipe } from './search.pipe'

@NgModule({
  declarations: [LangPipe,RoundPipe,SearchPipe],
  imports: [CommonModule],
  exports: [LangPipe,RoundPipe,SearchPipe],
})
export class PipeModule {}
