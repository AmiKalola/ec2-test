// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
import { UserRole } from '../app/shared/auth.roles';

export const environment = {
  production: false,
  buyUrl : 'https://1.envato.market/6NV1b',
  SCARF_ANALYTICS : false,
  adminRoot: '/app',

  // ----- local ------------------
  // imageUrl: 'http://localhost:8100/',
  // apiUrl: 'http://localhost:8100/v4',
  // socketUrl: 'http://localhost:8100/',

  // ----- live --------------------
  imageUrl: 'https://apiesuper.elluminatiinc.net/',
  languageUrl: 'https://apiesuper.elluminatiinc.net/',
  apiUrl:  'https://apiesuper.elluminatiinc.net/v4',
  socketUrl: 'https://apiesuper.elluminatiinc.net/',
  // apiUrl: 'http://192.168.0.178:8100/v4',

  // ----- developer --------------
  // imageUrl: 'https://apiesuperdeveloper.elluminatiinc.net/',
  // languageUrl: 'https://apiesuperdeveloper.elluminatiinc.net/',
  // apiUrl: 'https://apiesuperdeveloper.elluminatiinc.net/v4',
  // socketUrl: 'https://apiesuperdeveloper.elluminatiinc.net/',

  defaultMenuType: 'menu-default',
  subHiddenBreakpoint: 1442,
  menuHiddenBreakpoint: 768,
  themeColorStorageKey: 'vien-themecolor',
  isMultiColorActive: true,
  defaultColor: 'light.greenlime',
  isDarkSwitchActive: true,
  defaultDirection: 'ltr',
  themeRadiusStorageKey: 'vien-themeradius',
  isAuthGuardActive: true,
  defaultRole: UserRole.Admin,
  firebase: {
    apiKey: "AIzaSyBaoS3Q37bqIZpScYVuStqRF7MjQPtwsT0",
    authDomain: "esuper-ba26a.firebaseapp.com",
    databaseURL: "https://esuper-ba26a-default-rtdb.firebaseio.com",
    projectId: "esuper-ba26a",
    storageBucket: "esuper-ba26a.appspot.com",
    messagingSenderId: "1002505798469",
    appId: "1:1002505798469:web:520c15de885e0ab71f342c",
    measurementId: "G-L5X87B04P9"
  }
};
