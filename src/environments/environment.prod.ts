// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
import { UserRole } from '../app/shared/auth.roles';

export const environment = {
  production: true,
  buyUrl : "https://1.envato.market/6NV1b",
  SCARF_ANALYTICS : false,
  adminRoot: '/app',
  
  // live
  apiUrl: 'https://apiesuper.elluminatiinc.net/v4',
  imageUrl: 'https://apiesuper.elluminatiinc.net/',
  languageUrl: 'https://apiesuper.elluminatiinc.net/',
  socketUrl: 'https://apiesuper.elluminatiinc.net/',
  
  // development
  // imageUrl: 'https://apiesuperdeveloper.elluminatiinc.net/',
  // languageUrl: 'https://apiesuperdeveloper.elluminatiinc.net/',
  // apiUrl:  'https://apiesuperdeveloper.elluminatiinc.net/v4',
  // socketUrl: 'https://apiesuperdeveloper.elluminatiinc.net/',

  // apiUrl: 'http://192.168.0.160:8100/v4',
  // imageUrl: 'http://192.168.0.160:8100/',
  // socketUrl: 'http://192.168.0.160:8100/',
  
  defaultMenuType: 'menu-default',
  subHiddenBreakpoint: 1442,
  menuHiddenBreakpoint: 768,
  themeColorStorageKey: 'vien-themecolor',
  isMultiColorActive: true,
  /*
  Color Options:
  'light.blueyale', 'light.blueolympic', 'light.bluenavy', 'light.greenmoss', 'light.greenlime', 'light.yellowgranola', 'light.greysteel', 'light.orangecarrot', 'light.redruby', 'light.purplemonster'
  'dark.blueyale', 'dark.blueolympic', 'dark.bluenavy', 'dark.greenmoss', 'dark.greenlime', 'dark.yellowgranola', 'dark.greysteel', 'dark.orangecarrot', 'dark.redruby', 'dark.purplemonster'
  */
  defaultColor: 'light.greenlime',
  isDarkSwitchActive: true,
  defaultDirection: 'ltr',
  themeRadiusStorageKey: 'vien-themeradius',
  isAuthGuardActive: false,
  defaultRole: UserRole.Admin,
  firebase: {
    apiKey: "AIzaSyBaoS3Q37bqIZpScYVuStqRF7MjQPtwsT0",
    authDomain: "esuper-ba26a.firebaseapp.com",
    databaseURL: "https://esuper-ba26a-default-rtdb.firebaseio.com",
    projectId: "esuper-ba26a",
    storageBucket: "esuper-ba26a.appspot.com",
    messagingSenderId: "1002505798469",
    appId: "1:1002505798469:web:520c15de885e0ab71f342c",
    measurementId: "G-L5X87B04P9"
  }
};
